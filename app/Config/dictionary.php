<?php 
if(file_exists(__DIR__.'/database_local.php')){
	Configure::write('ivf_production', 'local'); 
}else{
	Configure::write('ivf_production', 'production'); 
}       
 
Configure::write('PIC_TOPIC', array('攻略'=>"10", '分享'=>"20", '问答'=>'30', '医院'=>'50'));  
Configure::write('OTHER_TYPE', array('染色体异常', '精子问题', '卵巢问题', '子宫问题','输卵管问题','胎停流产','不明原因不孕'));  
Configure::write('KEYWORDS_TYPE', array('首页'=>"10", '远程咨询'=>"20", '精准预约'=>'30', 'VIP尊享'=>'40', '金牌无忧'=>'50', '医院列表'=>'60', '医生列表'=>'70', '社区首页'=>'80', '攻略列表'=>'90', '攻略热门'=>'100', '攻略精华'=>'110', '分享列表'=>'120', '问答列表'=>'130', '最新问题'=>'140', '待回答问题'=>'150', '胖妈妈之声'=>'160', '视频自媒体'=>'170','网站地图'=>'180','联系我们'=>'190','新闻资讯栏目'=>'200','常见问题栏目'=>'210','关于我们'=>'220','活动页面'=>'230','加入我们'=>'240','自定义专题'=>'250'));  
Configure::write('COMMENT_TOPIC', array('攻略'=>"10", '医院'=>"20", '医生'=>'30', '视频'=>'40', '分享'=>'50', '问答'=>'60')); 
Configure::write('AGREE_TOPIC', array('攻略'=>"10", '视频'=>'40', '分享'=>'50', '问答'=>'60')); 
Configure::write('SHARE_TAG', array('10'=>"初次检查", '20'=>"促排阶段", '30'=>'取卵移植', '40'=>'保胎','50'=>'就医经验','60'=>'心情','70'=>'其他')); 
Configure::write('SHARE_TAG_TWO', array('10'=>"全球生殖中心", '20'=>"杰特宁试管婴儿中心", '30'=>'帕亚泰·是拉差', '40'=>'iBaby生殖中心','70'=>'其他')); 
Configure::write('DISEASE_TOPIC', array('10'=>'我想选性别','20'=>'我不清楚我的病症','30'=>'染色体异常','40'=>'精子问题','50'=>'卵巢问题','60'=>'子宫问题','70'=>'输卵管问题','80'=>'胎停流产','90'=>'不明原因不孕'));
Configure::write('ALBUM_CATE', array('1'=>'胖妈妈之声', '2'=>'试管百科', '3'=>'杂七杂八')); 
Configure::write('EDUCATION', array('1'=>'大学专科', '2'=>'大学本科', '3'=>'研究生', '4'=>'硕士', '5'=>'博士', '6'=>'博士后')); 
Configure::write('Experience', array('1'=>'不限','2'=>'应届生', '3'=>'1-3年', '4'=>'3-5年', '5'=>'5-10年', '6'=>'10年以上')); 
Configure::write('HOSPITAL_SCORE', array('1'=>'1分', '2'=>'2分', '3'=>'3分', '4'=>'4分', '5'=>'5分')); 
Configure::write('LOG_ACTION', array('add'=>'添加', 'del'=>'删除', 'edit'=>'编辑', 'batch_del'=>'批量删除'));  
Configure::write('USER_SEX', array('0'=>'保密', '1'=>'男神', '2'=>'女神')); 
Configure::write('WEB_SITE', array('0'=>'ivf52', '1'=>'ivf91', '2'=>'ivf51', '3'=>'yunyao')); 
Configure::write('MENU_ACTION', array('index_manage'=>'首页','user_manage'=>'会员管理','hospital_manage'=>'医院管理', 'news_manage'=>'新闻管理',  'article_manage'=>'攻略管理', 'videos_manage'=>'媒体管理', 'seo_manage'=>'seo管理', 'hr_manage'=>'人事管理', 'roles_manage'=>'权限管理'));    
?>

