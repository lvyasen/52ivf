<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
 
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	// if (is_mobile_request()) {
	// 	Router::connect('/', array('controller' => 'special', 'action' => 'index', 'home'));
	// }else{
		Router::connect('/', array('controller' => 'main', 'action' => 'index', 'home'));
	// }
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
	function is_mobile_request(){
	    $_SERVER['ALL_HTTP'] = isset($_SERVER['ALL_HTTP']) ? $_SERVER['ALL_HTTP'] : '';
	    $mobile_browser = '0';

	    if(preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|iphone|ipad|ipod|android|xoom)/i', strtolower($_SERVER['HTTP_USER_AGENT'])))
	        $mobile_browser++;
	    if((isset($_SERVER['HTTP_ACCEPT'])) and (strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') !== false))
	        $mobile_browser++;
	    if(isset($_SERVER['HTTP_X_WAP_PROFILE']))
	        $mobile_browser++;
	    if(isset($_SERVER['HTTP_PROFILE']))
	        $mobile_browser++;

	    $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'],0,4));
	    $mobile_agents = array(
	        'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
	        'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
	        'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
	        'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
	        'newt','noki','oper','palm','pana','pant','phil','play','port','prox',
	        'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
	        'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
	        'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
	        'wapr','webc','winw','winw','xda','xda-'
	    );

	    if(in_array($mobile_ua, $mobile_agents))
	        $mobile_browser++;
	    if(strpos(strtolower($_SERVER['ALL_HTTP']), 'operamini') !== false)
	        $mobile_browser++;
	    if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows') !== false)
	        $mobile_browser=0;
	    if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows phone') !== false)
	        $mobile_browser++;
	    if($mobile_browser>0)
	        return true;
	    else
	        return false;
	}
