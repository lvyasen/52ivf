<?php $this->Sgwy->addCss('/css/hospital/swiper.min.css');?>
<?php $this->Sgwy->addCss('/css/hospital/info.css');?>
<?php $this->Sgwy->addCss('/css/jquery.mCustomScrollbar.css');?>
<?php $this->Sgwy->addJs('/js/hospital/info.js');?>
<?php $this->Sgwy->addJs('/js/hospital/swiper.min.js');?>
<?php $this->Sgwy->addJs('/js/jquery.mCustomScrollbar.concat.min.js');?>
<?php echo $this->Html->script('/js/mobile/user.js');?>
<?php echo $this->Html->script('/js/page.js');?>
</head>
<body>
<div class="hos_info">
    <div class="wav_title">
        <p>医院详情</p>
        <img class="lazy" onclick="history.go(-1)" src="/images/common/grey.gif"
             data-original="/images/mobile/fanhui.png">
    </div>
    <div class="banner_pc"></div>
    <div class="banner_ph">
        <img src="<?=$info['Hospital']['mb_image']?>" alt="<?=$info['Hospital']['cn_name']?> 泰国试管婴儿攻略 试管无忧">
    </div>
    <div class="contain">
        <div class="grade">
            <a href="/">首页</a> ><a href="/hospital/index/"> 热门医院</a> > 医院详情
        </div>
        <div class="top">
            <div class="top_left">
                <img src="<?=$info['Hospital']['image']?>" alt="<?=$info['Hospital']['cn_name']?> 泰国试管婴儿攻略 试管无忧 ">
            </div>
            <div class="top_center">
                <div class="btitle"><?=$info['Hospital']['cn_name']?><span>(<?=$info['Hospital']['en_name']?>)</span>
                </div>
                <h3><span><?=$info['Hospital']['lever']?></span></h3>
                <p class="koubei">口碑 <span>&nbsp;<?=$info['Hospital']['public_praise']?></span>&nbsp;&nbsp;人气<span>&nbsp;<?=$info['Hospital']['popularity']?></span>&nbsp;&nbsp;活跃<span>&nbsp;<?=$info['Hospital']['active']?></span>&nbsp;&nbsp;
                </p>
                <p class="tishi">温馨提示：本页面医院的项目价格为老版数据，仅供参考，<br>如需获得最新价格咨询，请咨询客服。</p>
                <p class="jianjie"><span>简介：</span><?=$info['Hospital']['introduction']?></p><a class="gengduo">更多</a><a
                    class="shou">收起</a>
                <div class="gd_pro">
                    <div class="gd_con mCustomScrollbar"><?=$info['Hospital']['introduction']?></div>
                </div>
            </div>
            <div class="top_right">
                <a class="t_y"><img src="/images/hospital/bottom.png" alt=""/></a>
            </div>

        </div>
        <div class="top_ph">
            <h1><?=$info['Hospital']['cn_name']?></h1>
            <ul>
                <li>
                    <p><?=$info['Hospital']['appoint_num']?></p>
                    <span>预约</span>
                </li>
                <li>
                    <p><?=$info['Hospital']['case_num']?></p>
                    <span>案例</span>
                </li>
                <li>
                    <p><?=$info['Hospital']['appoint_num']?></p>
                    <span>关注数</span>
                </li>
            </ul>
            <div class="skill">
                <p>特点：</p>
                <span><?=$info['Hospital']['projects']?></span>
            </div>
        </div>
        <?php if(!empty($docList)){
        ?>
            <div class="expert">
                <h5>专家团队</h5>
                <div class="expert_box">
                    
                    <div class="swiper-container swiper-container-team">
                        <div class="swiper-wrapper">
                            <?php foreach($docList as $key=>$value){
                            ?>
                            <div class="swiper-slide">
                                <img src="<?=$value['Doctor']['avatar']?>" alt="">
                                <div class="slide_right">
                                    <p><?=$value['Doctor']['en_name']?></p>
                                    <div class="s_r_text">
                                        <?=$value['Doctor']['description']?>
                                    </div>
                                    <a href="/doctor/info/<?=$value['Doctor']['id']?>.html">查看详情</a>
                                </div>
                            </div>
                            <?php
                        }?>
                        </div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination"></div>
                        <!-- Add Arrows -->
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>



            <div class="con_left">
                <div class="hos_suggest">
                    <h5>医院简介</h5>
                    <p><?=$info['Hospital']['introduction']?></p>
                </div>
                <?php if(sizeof($docList) >0){?>
                <div class="team" style="display: none;">
                    <h5>医生团队</h5>
                    <ul>
                        <?php foreach($docList as $key => $value):?>
                        <li>
                            <img class="lazy" src="<?=$value['Doctor']['avatar']?>"
                                 data-original="<?=$value['Doctor']['avatar']?>">
                            <div>
                                <p class="title"><?=$value["Doctor"]["cn_name"]?><span>（<?=$value["Doctor"]["duty"]?>
                                    ）</span><a href="" title="">认证</a></p>
                                <p class="sc">擅长：</p>
                                <p class="bu"><?=$value["Doctor"]["projects"]?></p>
                                <p class="sc_ph">专长:<span><?=$value["Doctor"]["projects"]?></span></p>
                                <p class="team_xin"><span><?=$value["Doctor"]["appoint_num"]?></span>预约&nbsp;&nbsp;|&nbsp;&nbsp;<span><?=$value["Doctor"]["case_num"]?></span>案例
                                </p>

                            </div>

                        </li>
                        <?php endforeach;?>
                    </ul>
                </div>
                <?php }?>
                <div class="custom">
                    <?=stripslashes($info['Hospital']['custom_text1'])?>
                </div>

                <div class="scene">
                    <h5>医院环境</h5>
                    <div id="www_qpsh_com" style="overflow:hidden;height:170px;width:95%;margin: 20px auto;">
                        <table width="0" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td id="www_qpsh_com1">
                                    <table width="0" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <?php foreach ($info['Hospital']['images'] as $k => $v):?>
                                            <td align="center"><img src="<?php echo $v['Picture']['thumb'];?>"
                                                                    alt="试管无忧 医院环境 泰国试管婴儿攻略"/></td>
                                            <?php endforeach;?>
                                        </tr>
                                    </table>
                                </td>
                                <td id="www_qpsh_com2"></td>
                            </tr>
                        </table>
                    </div>
                    <!-- <div class="swiper-container">
                        <div class="swiper-wrapper">

                            <?php foreach ($info['Hospital']['images'] as $k => $v):?>
                            <div class="swiper-slide sli"><a href="<?php echo $v['Picture']['image'];?>"  rel="projectimg"><img class="lazy"  id="shenqiqi" src="<?php echo $v['Picture']['thumb'];?>" data-original="<?php echo $v['Picture']['thumb'];?>"></a></div>
                            <?php endforeach;?>
                        </div>
                    </div> -->
                </div>
                <div class="process">
                    <h5>试管流程</h5>
                    <ul>
                        <li>
                            <p><img src="/images/hospital/1.png" alt="" style="margin-top:-1px;"><i>01</i><span>国内检查</span></p>
                            <div>国内各方代表服务中心提供前期咨询，患者在国内做检查，并将报告发往泰国医院进行初诊建档</div>
                        </li>
                        <li>
                            <p><img src="/images/hospital/2.png" alt=""><i>02</i><span>促排流程</span></p>
                            <div>在泰国进行约13天的促排后，取卵取精结合胚胎并培育</div>
                        </li>
                        <li>
                            <p><img src="/images/hospital/3.png" alt=""><i>03</i><span>胚胎分析</span></p>
                            <div>待胚胎发育至3-5天进行染色体筛查</div>
                        </li>
                        <li>
                            <p><img src="/images/hospital/4.png" alt=""><i>04</i><span>移植待产</span></p>
                            <div>移植优质胚胎，验孕待产</div>
                        </li>
                    </ul>
                </div>
                <?php if(!empty($info['Hospital']['object'])){?>
                <div class="price">
                    <h5>价格明细</h5>
                    <?=stripslashes($info['Hospital']['object'])?>
                </div>
                <?php }?>
                <div class="custom">
                    <?=stripslashes($info['Hospital']['custom_text2'])?>
                </div>
                <div class="hos_score">
                    <h5>医院评分</h5>
                    <div class="score_cen">
                        <ul>
                            <li>
                                <p>医院环境<span><?=$info['Hospital']['environment_score']?></span></p>
                                <div>
                                    <?php
                                    for ($x=1; $x <= $info['Hospital']['environment_score']; $x++) {
                                      echo "<img src=/images/hospital/wujiaoxing_01.jpg>";
                                    }
                                    ?>
                                </div>
                            </li>
                            <li>
                                <p>医疗技术<span><?=$info['Hospital']['skill_score']?></span></p>
                                <div>
                                    <?php
                                    for ($x=1; $x <= $info['Hospital']['skill_score']; $x++) {
                                      echo "<img src=/images/hospital/wujiaoxing_01.jpg>";
                                    }
                                    ?>
                                </div>
                            </li>
                            <li>
                                <p>服务评分<span><?=$info['Hospital']['service_score']?></span></p>
                                <div>
                                    <?php
                                    for ($x=1; $x <= $info['Hospital']['service_score']; $x++) {
                                      echo "<img src=/images/hospital/wujiaoxing_01.jpg>";
                                    }
                                    ?>
                                </div>
                            </li>
                            <li>
                                <p>综合指数<span><?=$info['Hospital']['composite_score']?></span></p>
                                <div>
                                    <?php
                                    for ($x=1; $x <= $info['Hospital']['composite_score']; $x++) {
                                      echo "<img src=/images/hospital/wujiaoxing_01.jpg>";
                                    }
                                    ?>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="patient" id="loadPage">
                    <h5>患者评价</h5>
                    <div class="text_writ">
                        <textarea name="" id="" placeholder="我来说两句..."></textarea>
                        <div>
                            <a class="publish">发表评价</a>
                        </div>
                    </div>
                    <ul class="pat_list">
                        <?php foreach ($comments as $key => $value):?>
                        <li>
                            <div class="pinglu">
                                <div class="head_pic">
                                    <img src="<?=$value['Comment']['avatar']?>">
                                </div>
                                <div class="conte">
                                    <div class="namec"><?=$value['Comment']['username']?></div>
                                    <div class="contc"><?=$value['Comment']['content']?></div>
                                    <!--  <div class="timec"><?php echo Ivf::formatTime($value['Comment']['comment_time']);?></div> -->
                                </div>
                            </div>
                        </li>
                        <?php endforeach;?>
                    </ul>
                    <?php if(sizeof($comments) >= 5){?>
                    <div class="more"
                         onclick="_page.getPageComment(this,<?=$info['Hospital']['id']?>,'hospital_comment')">
                        <a href="javascript:void(0)" title="">加载更多</a>
                    </div>
                    <?php }?>
                </div>
            </div>
            <div class="con_right">
                <h5>远程咨询往期回顾</h5>
                <ul>
                    <li class="direct_list">
                        <img class="vid_btn" src="/images/hospital/l001.jpg" alt="41岁陈女士 泰国试管婴儿攻略 试管无忧 ">
                        <div class="direct">
                            <span>41岁陈女士(卵巢萎缩)：</span>
                            <!-- <a href="" title="">直播</a> -->
                            <p>赴泰前，与蔡医生远程问诊</p>
                        </div>
                    </li>
                    <li class="direct_list">
                        <img class="vid_btn" src="/images/hospital/l002.jpg" alt="山东夫妇 泰国试管婴儿攻略 试管无忧">
                        <div class="direct">
                            <span>山东夫妇(求龙凤胎)：</span>
                            <!-- <a href="" title="">直播</a> -->
                            <p>远程问诊，确定预约提迪贡医生</p>
                        </div>
                    </li>
                    <li class="direct_list">
                        <img class="vid_btn" src="/images/hospital/l003.jpg" alt="胡女士 泰国试管婴儿攻略 试管无忧">
                        <div class="direct">
                            <span>胡女士(多次流产)：</span>
                            <!-- <a href="" title="">直播</a> -->
                            <p>咨询治疗和护理建议，确定赴泰时间</p>
                        </div>
                    </li>
                </ul>
            </div>
    </div>
    <div class="box_bg">
        <div class="box">
            <div class="close"></div>
            <form class="dataForm" action="/hospital/index" method="post">
                <input type="hidden" name="hid" id="hid" value="<?=$info['Hospital']['id']?>"/>
                <div class="content">
                    <div class="con-1">
                        <label for="">希望就诊医院：</label>
                        <span id="hospitalName"><?=$info['Hospital']['cn_name']?></span>
                    </div>
                    <div class="con-2">
                        <label for="">病情描述：</label>
                        <textarea name="description" value=""></textarea>
                    </div>
                    <div class="con-3">
                        <label for="">您的姓名：</label>
                        <input type="text" placeholder="请输入您的姓名" name="user_name">
                    </div>
                    <div class="con-3">
                        <label for="">您的手机号：</label>
                        <input type="text" placeholder="请输入手机号" id="u_mobile" name="u_mobile" datatype="m"
                               errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！">
                    </div>
                    <div class="con-4">
                        <label for="">验证码：</label>
                        <input type="text" name="u_code" value="" datatype="*6-6" errormsg="请输入6位验证码！"
                               nullmsg="请输入6位验证码！" autocomplete="off">
                        <input class="get_code" type="button" id="send_mc_btn"
                               onclick="_user.sendRcode('send_code_all')" value="获取验证码"/>
                    </div>
                    <div class="con-5">
                        <span>总价约￥<i id="hospitalPrice"><?=intval($info['Hospital']['price'])?></i></span>
                        <button>马上预订</button>
                    </div>
                </div>
                <input type="hidden" name="dosubmit" value="1"/>
            </form>
        </div>
    </div>
</div>
    

