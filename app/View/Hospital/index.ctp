<?php $this->Sgwy->addCss('/css/hospital/index.css');?>
<?php echo $this->Html->script('/js/mobile/user.js');?> 
<?php echo $this->Html->script('/js/page.js');?>  
   <div class="hospital">
       <div class="wav_title">
        <p>全球试管医院</p>
        <img alt="" class="lazy" onclick="window.location.href='/';" src="/images/mobile/fanhui.png"><span>首页</span>
        </div>
        <div class="banner_pc"></div>
        <div class="banner_ph">
            <img alt="" class="lazy" src="/images/common/grey.gif" data-original="/images/hospital/banner_yiyuan_ph.jpg">
        </div>
        <div class="contain">
            <div class="content">
                <div class="nav">
                    <!-- <div class="country">
                        <a class="<?php echo(empty($cid))? "ct_click":"";?>" href="/hospital">全部</a>
                        <?php foreach($districtList as $key => $value):?>
                        <a href="/hospital/index/<?=$value["District"]["id"]?>.html" class="<?php echo !empty($cid) && $value["District"]["id"]==$cid ? "ct_click":"";?>"><?=$value["District"]["country"]?></a>
                    <?php endforeach;?>
                    </div> -->
                    <div class="yiyuan">
                        <a class="yy_click" href="/hospital/index/<?php echo !empty($cid)? $cid.".html":"";?>" title="">找医院</a>
                        <a href="/doctor/index/<?php echo !empty($cid)? $cid.".html":"";?>" title="">找医生</a>
                    </div>
                </div>
                <div class="list" id="loadPage">
                    <ul>
                     <?php foreach($dataList as $key => $value):?>
                        <li>
							<input  type="hidden" id="HN_<?php echo $value["Hospital"]["id"];?>" value="<?php echo $value["Hospital"]["cn_name"];?>"/>
							<input  type="hidden" id="PR_<?php echo $value["Hospital"]["id"];?>" value="<?php echo intval($value["Hospital"]["price"]);?>"/>
                            <a  href="/hospital/info/<?php echo $value["Hospital"]["id"].".html";?>" title="">
                            <div class="list_left">
                                <img alt="<?=$value['Hospital']['cn_name']?> 泰国试管婴儿攻略 试管无忧" class="lazy" src="<?=$value['Hospital']['image']?>" data-original="<?=$value['Hospital']['image']?>">
                            </div>
                            <div class="list_center">
                                <p class="title"><?=$value['Hospital']['cn_name']?><span>（<?=$value['Hospital']['en_name']?>）</span></p>
                                <p class="title_m"><?=$value['Hospital']['cn_name']?></p>
                                <p class="tag">口碑：<span><?=$value['Hospital']['public_praise']?></span>人气：<span><?=$value['Hospital']['popularity']?></span>活跃：<span><?=$value['Hospital']['active']?></span></p>
                                <p class="wenan"><?=$value['Hospital']['introduction']?></p>
                                <p class="te">特点：<span><?=$value['Hospital']['projects']?></span></p>
                                <div class="score">
                                    <div class="score_img">
                                    <?php 
                                        for ($x=1; $x<=$value['Hospital']['score']; $x++) {
                                          echo "<img alt='' src=/images/hospital/quan_1.jpg>";
                                        } 
                                    ?>
                                       <div><?=$value['Hospital']['score']?>/5分&nbsp;&nbsp;还可以</div>
                                    </div>
                                    <div class="score_w">
                                        <p>已有<span><?=$value['Hospital']['rec_num']?></span>位病友推介该医院</p>
                                    </div>
                                </div>
                                <div class="score_m">
                                    <div>
                                       <?php 
                                        for ($x=1; $x<=$value['Hospital']['popularity']; $x++) {
                                          echo "<img alt='' src=/images/hospital/wujiaoxing_01.jpg>";
                                            } 
                                        ?> 
                                    </div>
                                    <span><i>￥</i><?=intval($value['Hospital']['price'])?>起/人</span>
                                </div>
                                <p class="wenan_m"><?=$value['Hospital']['introduction']?></p>
                            </div>
                            </a>
                            <div class="list_right">
                                <p><span><i>￥</i><?=intval($value['Hospital']['price'])?></span>起/人</p>
                                <a class="pc_yu" href="javascript:void(0)" title="" onclick="_user.appoint(<?php echo $value["Hospital"]["id"];?>);_user.showDiv(0);" rel="nofollow">我要预约</a>
                                <a class="ph_yu" href="/mobile/order_hospital/<?php echo $value["Hospital"]["id"].'.html';?>" title="" rel="nofollow">预约</a>
                            </div>
                        </li>
                    <?php endforeach;?> 
                    </ul>
                </div>
                <?php if(sizeof($dataList) >= 6){?>
                <div class="more" onclick="_page.getHospital(this,<?php echo intval($cid);?>)">
                    <a href="javascript:void(0)">加载更多</a>
                </div>
                <?php }?>
            </div>
            <div class="flot_right">
                <div class="right_top">
                    <h4>加我微信&nbsp;沟通更快</h4>
                    <div class="touxiang">
                        <p>NaNa <br>就医顾问</p>
                    </div>                
                </div>
                <div class="right_cen">
                    <h4>无忧承诺</h4>
                    <p>优质医院，实地考察</p>
                    <p>快速预约，直达医院</p>
                    <p>低价保证，服务保障</p>
                </div>
                <div class="right_bot">
                <a href="/site/index/" target="_blank"><img src="/images/hospital/guanggao.jpg" alt="赴泰考察"></a>
                </div>
            </div>
        </div>
        <div class="box_bg">
            <div class="box">
                <div class="close"></div>
				<form  class="dataForm" action="/hospital/index" method="post" >
				<input type="hidden" name="hid" id="hid" value="0"/>   
                <div class="content">
                    <div class="con-1">
                        <label for="">希望就诊医院：</label>
                        <span id="hospitalName"></span>
                    </div>
                    <div class="con-2">
                        <label for="">病情描述：</label>
                        <textarea name="description" value=""></textarea>
                    </div>
                    <div class="con-3">
                        <label for="">您的姓名：</label>
                        <input type="text" placeholder="请输入您的姓名" name="user_name">
                    </div>
                    <div class="con-3">
                        <label for="">您的手机号：</label>
                        <input type="text"  placeholder="请输入手机号" id="u_mobile" name="u_mobile"  datatype="m" errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！">
                    </div>
                    <div class="con-4">
                        <label for="">验证码：</label>
                        <input type="text" name="u_code" value="" datatype="*6-6" errormsg="请输入6位验证码！"  nullmsg="请输入6位验证码！" autocomplete="off">
                        <input class="get_code" type="button"  id="send_mc_btn" onclick="_user.sendRcode('send_code_all')" value="获取验证码"/>
                    </div>
                    <div class="con-5">
                        <span>总价约￥<i id="hospitalPrice">199</i></span>
                        <button>马上预订</button>
                    </div>
                </div>
				<input type="hidden" name="dosubmit" value="1"/>    
			</form>       
            </div>
        </div>
    </div>
	<script type="text/javascript"> 
    $(document).ready(function(){
        $(".close").click(function(){
            $(".box_bg").fadeOut();
        }) 
		$(".dataForm").Validform({
			tiptype:function(msg){
				layer.alert(msg,{icon:0});   
			},
			tipSweep:true,
			ajaxPost:true,
			callback:function(o){
				if (o.status == 200) {
					layer.msg(o.message,{
                        icon:0,
                        skin:'layer_mobile_login'
                    });    
                    $(".box_bg").fadeOut();          
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
				} else {
					layer.msg(o.message,{
                        icon:5,
                        skin:'layer_mobile_login'
                    });    
				} 
			}
		});
    })
        function MM_jumpMenu(targ,selObj,restore){ //v3.0
           eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
           if (restore) selObj.selectedIndex=0;
        }
    </script> 
   