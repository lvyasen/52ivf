<?php echo $this->Html->css('/css/mobile/login.css');?>  
<?php echo $this->Html->script('/js/mobile/user.js');?>   
<?php $this->Sgwy->addJs('/js/mobile/bright.js');?> 
<div class="register">
     <div class="nav_title">
        <p>密码找回</p>
        <img class="lazy" onclick="history.go(-1)" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
    </div>
    <div class="register_form">
        <form  class="dataForm" action="/mobile/retrieval" id="regForm" method="post" style="overflow:hidden;">
            <div class="reg_1">
                <input class="register_phone vals" type="text" placeholder="请输入手机号" id="u_mobile" name="u_mobile"  datatype="m" errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！" autocomplete="off"/> 
            </div>    
            <div class="reg_2">
                <input class="register_code vals" type="text" name="u_code" value="" placeholder="输入6位验证码" datatype="*6-6" errormsg="请输入6位验证码！"  nullmsg="请输入6位验证码！" autocomplete="off"/>     
                <input class="get_code bright" type="button"  id="send_mc_btn" onclick="_user.sendRcode('findpwd_send_code')" value="获取验证码"/>    
            </div>               
            <div class="reg_3">
                <input  class="register_password_01 vals" type="password" placeholder="请输入新密码"  name="u_pwd" datatype="*8-16" errormsg="密码至少8个字符,最多16个字符！"  nullmsg="请输入新密码！" autocomplete="off"/> 
            </div> 
            <div class="reg_4">
                <input  class="register_password_02 vals" type="password" placeholder="请确认新密码" name="u_comfirm_pwd"  type="text"  datatype="*" recheck="u_pwd" errormsg="两次输入的密码不一致！"  nullmsg="请确认新密码！" autocomplete="off"/>              
            </div>    
            <div class="sub">                  
                <input type="submit" class="register_btn" value="找回" /> 
            </div>   
            <input type="hidden" name="dosubmit" value="1"/>    
        </form>             
    </div>     
</div>         
<script type="text/javascript"> 
    $(".dataForm").Validform({
        tiptype:function(msg){
            layer.alert(msg,{icon:0});   
        },
        tipSweep:true,
        ajaxPost:true,
        callback:function(o){
            if (o.status == 200) {
                layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
                setTimeout("window.location.href='/mobile/index/'", 2000);
            } else {
                layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
            } 
        }
    });
    
</script>         
