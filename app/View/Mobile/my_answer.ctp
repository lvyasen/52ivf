<?php echo $this->Html->css('/css/mobile/my_answer.css');?>
<div class="my_answer">
	<div class="nav_title">
		<p><span>我的</span>我的问答</p>
		<img class="lazy" onclick="window.location.href='/mobile/'" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
		<a href="/mobile/release_ans/">发布</a>
	</div>
	<div class="list">
	    <ul class="list_ul">
            <?php foreach ($dataList as $key => $value):?>
	    	<li><a href="/answer/info/<?php echo $value["Share"]["id"].".html";?>" >
	    		<p><?=$value['Share']['description']?></p>
	    		<ul class="list_img"> 
                    <?php foreach ($value['Share']['images'] as $k => $v):?> 
                    <li><img class="lazy"   data-original="<?php echo $v['Picture']['thumb'];?>" src="<?php echo $v['Picture']['thumb'];?>"?>  </li> 
                    <?php endforeach;?> 
	    		</ul>
	    		<div class="Tag">
	    			<div class="time"><i></i><?=date("m-d H:s",strtotime($value['Share']['create_time']))?></div>
	    			<div class="write"><i></i><?=$value['Share']['comment_num']?></div>
	    			<div class="look"><i></i><?=$value['Share']['browse_num']?></div>
	    		</div></a>
	    	</li>
        	<?php endforeach;?> 
	    </ul>
	</div>
</div>
