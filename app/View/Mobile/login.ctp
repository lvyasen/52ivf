
<?php echo $this->Html->css('/css/mobile/login.css');?>  
<div class="login">
    <div class="nav_title">
        <p>帐号登录</p>
        <img class="lazy" onclick="history.go(-1)" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
    </div>
    <div class="login_form"> 
        <form  class="dataForm" action="/mobile/login"  method="post" >
            <div class="inp_1">
                <input class="login_phone" type="text" placeholder="请输入手机号(限11位)" id="u_mobile" name="u_mobile"  datatype="m" errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！" autocomplete="off"/> 
            </div>                   
            <div class="inp_2">
                <input  class="login_password" type="password"  name="u_pwd"  placeholder="密码"  datatype="*8-16" errormsg="密码至少8个字符,最多16个字符！"  nullmsg="请输入密码" autocomplete="off"/>
                <a class="" href="/mobile/retrieval/">忘记密码?</a> 
            </div>
                    
            <div class="sub">                  
                <button class="login_submit">登录</button> 
                <a href="/mobile/register/" class="login_register">注册</a>
            </div>   
        <input type="hidden" name="dosubmit" value="1"/>             
        </form>    
    </div> 
   
</div>         
<div class="foot"></div> 
<script type="text/javascript"> 
    $(".dataForm").Validform({
        tiptype:function(msg){
            layer.alert(msg,{icon:0});   
        },
        tipSweep:true,
        ajaxPost:true,
        callback:function(o){
            if (o.status == 200) {
                layer.msg(o.message,{
                    icon:6,
                    skin:'layer_mobile_login'
                });    
                setTimeout("window.location.href='"+o.data.regerurl+"'", 2000);
            } else {
                layer.msg(o.message,{
                    icon:5,
                    skin:'layer_mobile_login'
                });    
            } 
        }
    });
    //判断键盘是否弹出
    var wHeight = window.innerHeight;   //获取初始可视窗口高度
    $(window).resize(function() {         //监测窗口大小的变化事件
        var hh = window.innerHeight;     //当前可视窗口高度
        if(wHeight > hh ){           //可以作为虚拟键盘弹出事件
            $(".footnav").hide();
        }else{         //可以作为虚拟键盘关闭事件
            $(".footnav").show();
        }
    });
</script>    