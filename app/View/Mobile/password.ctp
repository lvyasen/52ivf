<?php echo $this->Html->css('/css/mobile/password.css');?>
<?php $this->Sgwy->addJs('/js/mobile/bright.js');?>
<div class="password">
	<div class="nav_title">
		<p><span>我的</span>修改密码</p>
		<img class="lazy" onclick="history.go(-1)" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
	</div>
	<div class="ipts">
        <form  class="dataForm" action="/mobile/password" method="post" >
		    <div>
		    	<input class="old_pass" type="password" placeholder="请输入旧密码"  name="u_pwd"  datatype="*8-16" errormsg="密码至少8个字符,最多16个字符！"  nullmsg="请输入密码！">
		    	<img class="lazy" src="/images/common/grey.gif" data-original="/images/mobile/suo.png"> 
		    </div>
		    <div>
		    	<input class="new_pass1" type="password" placeholder="请输入新的密码"  name="new_pwd"  datatype="*8-16" errormsg="密码至少8个字符,最多16个字符！"  nullmsg="请输入密码！">
		    	<img class="lazy" src="/images/common/grey.gif" data-original="/images/mobile/suo.png"> 
		    </div>
		    <div>
		    	<input class="new_pass2" type="password" placeholder="请在此输入新的密码" name="new_pwd_comfirm"  datatype="*" recheck="new_pwd" errormsg="两次输入的密码不一致！"  nullmsg="请再次输入密码！">
		    	<img class="lazy" src="/images/common/grey.gif" data-original="/images/mobile/suo.png"> 
		    </div>

		<div class="submit">
            <input type="submit" class="submit_btn" value="保存" /> 
			<p>温馨提示：密码必须至少8个字符，而且同时包含字母和数字。</p>
		</div>
        <input type="hidden" name="dosubmit" value="1"/>    
		</form>
		
	</div>
</div> 
<script type="text/javascript"> 
    $(".dataForm").Validform({
        tiptype:function(msg){
            layer.alert(msg,{icon:0});   
        },
        tipSweep:true,
        ajaxPost:true,
        callback:function(o){
            if (o.status == 200) {
                layer.msg(o.message,{
                    icon:6,
                    skin:'layer_mobile_login'
                });    
                setTimeout("window.location.href='/mobile/'", 2000);
            } else {
                layer.msg(o.message,{
                    icon:5,
                    skin:'layer_mobile_login'
                });    
            } 
        }
    });
</script>         