<?php echo $this->Html->css('/css/mobile/order_doctor.css');?>  
<?php echo $this->Html->script('/js/mobile/user.js');?> 
<?php $this->Sgwy->addJs('/js/mobile/bright.js');?> 
<div class="order_doctro">
    <div class="nav_title">
        <p>预约医生</p>
        <img class="lazy" onclick="history.go(-1)" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
    </div>
    <div class="od_form">  
        <form  class="dataForm" action="/mobile/order_doctor" method="post" >
            <input type="hidden" name="did" id="did" value="<?php echo $info['Doctor']['id'];?>"/>  
            <div class="d_inp01">
                <?php echo $info['Doctor']['cn_name'];?>
            </div>    
            <div class="d_inp02">
                <input class="vals" type="text" placeholder="请填入您的姓名" name="user_name" autocomplete="off">
            </div>
           <div class="d_inp02">
                <input class="vals" type="text" placeholder="简单描述一下您的症状" name="description" autocomplete="off">
            </div>               
            <div class="d_inp03">
                <input class="vals" type="text"  placeholder="请输入手机号(限11位)" id="u_mobile" name="u_mobile"  datatype="m" errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！" autocomplete="off">
            </div> 
            <div class="d_inp04">
                <input class="vals" type="text" name="u_code" value="" placeholder="输入6位验证码" datatype="*6-6" errormsg="请输入6位验证码！"  nullmsg="请输入6位验证码！" autocomplete="off"> 
                <input class="get_code bright" type="button"  id="send_mc_btn" onclick="_user.sendRcode('send_code_all')" value="获取验证码"/ autocomplete="off">
            </div>   
            <div class="sub">
                <input type="submit" class="or_btn register_btn" value="马上预约" />
            </div>  
            <input type="hidden" name="dosubmit" value="1"/>    
        </form>             
    </div>     
</div>  
<script type="text/javascript"> 
        $(".dataForm").Validform({
            tiptype:function(msg){
                layer.alert(msg,{icon:0});   
            },
            tipSweep:true,
            ajaxPost:true,
            callback:function(o){
                if (o.status == 200) {
                    layer.msg(o.message,{
                        icon:6,
                        skin:'layer_mobile_login'
                    });     
                    window.history.go(-1);
                } else {
                    layer.msg(o.message,{
                        icon:5,
                        skin:'layer_mobile_login'
                    });    
                } 
            }
        }); 
</script>
