
<?php echo $this->Html->css('/css/mobile/person.css');?>

<div class="person">
	<div class="head_img">
		<img class="lazy" src="<?php echo $userInfo['avatar'];?>" data-original="<?php echo $userInfo['avatar'];?>">
        <p><?php echo isset($userInfo['nick_name']) && !empty($userInfo['nick_name'])?$userInfo['nick_name']:"你还没有设置用户名哦";?></p>
        <p><span><em></em><?php echo $sex[$userInfo['sex']];?></span></p>
	</div>
	<div class="means">
		<ul>
			<li class="menu_01"><span></span><a href="/mobile/means/">资料管理<em></em></a></li>
            <li class="menu_02"><span></span><a href="/mobile/password/">密码管理<em></em></a></li>
            <li class="menu_06"><span></span><a href="/mobile/my_plan/">我的攻略<em></em></a></li>
            <!--<li class="menu_03"><span></span><a href="/mobile/my_reservation/">我的预约<em></em></a></li>-->
            <li class="menu_04"><span></span><a href="/mobile/my_share/">我的分享<em></em></a></li>
            <li class="menu_05"><span></span><a href="/mobile/my_answer/">我的问答<em></em></a></li>           
		</ul>
		<a href="/personal/login_out" class="out">退出登录</a>
	</div>

</div>