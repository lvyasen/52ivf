<?php echo $this->Html->css('/css/mobile/login.css');?>  
<?php echo $this->Html->script('/js/mobile/user.js');?>    
<?php $this->Sgwy->addJs('/js/mobile/bright.js');?>
<div class="register">
    <div class="nav_title">
        <p>手机注册</p>
        <img class="lazy" onclick="history.go(-1)" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
    </div>
    <div class="register_form"> 
        <form  class="dataForm" action="/mobile/register" id="regForm" method="post" >
            <div class="reg_1">
                <input class="register_phone" type="text" placeholder="请输入手机号(限11位)" id="u_mobile" name="u_mobile"  datatype="m" errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！" autocomplete="off"/> 
            </div>    
            <div class="reg_2">
                <input class="register_code" type="text" name="u_code" value="" placeholder="输入6位验证码" datatype="*6-6" errormsg="请输入6位验证码！"  nullmsg="请输入6位验证码" autocomplete="off"/>
                <input class="get_code bright" type="button"  id="send_mc_btn" onclick="_user.sendRcode('send_code')" value="获取验证码"/>
            </div>               
            <div class="reg_3">
                <input  class="register_password_01" type="password"  name="u_pwd"  placeholder="输入密码(8~16个数字与字母组合)"  datatype="*8-16" errormsg="密码至少8个字符,最多16个字符！"  nullmsg="请输入密码！" autocomplete="off"/> 
            </div> 
            <div class="reg_4">
                <input  class="register_password_02"  name="u_comfirm_pwd"  type="password" placeholder="确认密码"  datatype="*" recheck="u_pwd" errormsg="两次输入的密码不一致！"  nullmsg="请再次输入密码！" autocomplete="off"/>     
            </div>   
            <div class="agre">
                <input id="agre" type="radio">
                <label for="agre">同意</label>
                <a href="">《用户注册协议》</a>
            </div>  
            <div class="sub">                  
                <input type="submit" class="register_btn bright" value="注册" />
            </div>  
            <input type="hidden" name="dosubmit" value="1"/>    
        </form>             
    </div>     
</div> 
<script type="text/javascript"> 
    $(".dataForm").Validform({
        tiptype:function(msg){
            layer.alert(msg,{icon:0});   
        },
        tipSweep:true,
        ajaxPost:true,
        callback:function(o){
            if (o.status == 200) {
                layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
                setTimeout("window.location.href='/mobile/'", 2000);
            } else {
                layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
            } 
        }
    });
</script>         