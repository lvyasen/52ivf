<?php echo $this->Html->css('/css/mobile/release.css');?>
<?php echo $this->Html->script('/admin/js/plupload/plupload.full.min.js');?>     
<!-- 问答发布 -->
<div class="release">
	<form  class="dataForm" action="/mobile/release_ans" method="post" enctype="multipart/form-data">  
	<div class="top">
		<span><a href="/mobile/my_answer/">取消</a></span>
		<a href="javascript:void(0)" class="submit">发布</a>
	</div>
	<div class="wrap"> 
		<textarea  name="description" datatype="*"  nullmsg="请填写内容！" class="content" placeholder="这一刻想说的..."></textarea>
		<div class="blog">
		    <div> 
				<a class="sele_hos">选择所在医院</a>
			</div>
			<div>
				<a class="sele_tag">选择标签</a>
			</div>
		</div>
		<?=$this->element('sele_hos')?>
		<?=$this->element('sele')?>
		<div class="wl3" id="divContent">
    		<!-- <span style="float:left;">添加图片：</span> -->
            <section class="z_file fl">
                <img src="/admin/js/plupload/img/a11.png" tppabs="/admin/js/plupload/img/a11.png" class="add-img" id="btnUpload">
            </section>
    	</div>
	</div> 
    <input type="hidden" name="dosubmit" value="1"/>      
</form> 

</div>
<script type="text/javascript"> 
	$('input').focus();
	$(".dataForm").Validform({
	    btnSubmit:".submit", 
	    tiptype:function(msg){
	        layer.alert(msg,{icon:0});   
	    },
	    tipSweep:true,
	    ajaxPost:true,
	    callback:function(o){
	        if (o.status == 200) {
	            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'}); 
                setTimeout("window.location.href='"+o.data.returns+"'", 1000); 
	        } else {
	            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
	        } 
	    }
	});
$(document).ready(function(){ 
	var MAX_FILE_NUM = 3;   
	var multi = new plupload.Uploader({
	    browse_button: 'btnUpload', // this can be an id of a DOM element or the DOM element itself
	    url: '/ajax/file?ajaxdata=answer', 
	    max_file_size: '10mb',
	    chunk_size: '3mb',
	    filters: {
	        mime_types: [
	          { title: "Image files", extensions: "BMP,jpg,JPEG,gif,png" }
	        ]
	    },
	    runtimes: 'html5',
	    multi_selection: true
	});
	
	multi.init();

	multi.bind('FilesAdded', function (up, files) {  
	    var uptLenth=$('.add_pic').size();   
	    if(((uptLenth-1)+files.length) >= MAX_FILE_NUM){ 
	         layer.alert('最多添加3张',{icon:0});  
	         multi.splice();
	         multi.refresh();
	        return;
	    }else{ 
	        layer.load(1); 
	        plupload.each(files, function (file) {     
	            $('#divContent').append('<section class="up-section fl add_pic" id="pic_'+file.id+'"></section>');
	        });
	        multi.start();
	    }
	});  
	multi.bind('FileUploaded', function (up, file, resp) {  
	    var json = JSON.parse(resp.response); 
		if(json.status==200){
	    	$('#pic_' + file.id).html('<span class="up-span"></span><img src="/admin/js/plupload/img/a7.png" class="close-upimg" onclick="delFile(' + json.data.id + ',\'pic_' + file.id + '\')"><img class="up-img" src="' + json.data.url + '"/><input type="hidden"  name="UptPicture[]" value="'+json.data.id+'" />'); 
		}
		else 
            layer.msg(json.message,{icon:0,skin:'layer_mobile_login'});  
	    layer.closeAll('loading'); 
	});

	multi.bind('Error', function (up, err) {    
	    layer.msg("Error #" + err.code + ": " + err.message,{icon:0,skin:'layer_mobile_login'});  
	}); 
});  
	//判断键盘是否弹出
    var wHeight = window.innerHeight;   //获取初始可视窗口高度
    $(window).resize(function() {         //监测窗口大小的变化事件
        var hh = window.innerHeight;     //当前可视窗口高度
        if(wHeight > hh ){           //可以作为虚拟键盘弹出事件
            $(".footnav").hide();
        }else{         //可以作为虚拟键盘关闭事件
            $(".footnav").show();
        }
    });
	var H = window.innerHeight-50;
    $(".wrap").css({height:H}); 
	$(".wl_hospital label").click(function(){  
		$(this).parent("div").siblings("div").children('i').css({display:'none'});
		$(this).parent("div").children('i').css({display:'block'});
		$(':radio[name="hospital_id"]').removeAttr("checked");
		var cid = $(this).parent("div").attr("data-id");    
		$("input[name='hospital_id'][value='"+cid+"']").attr("checked",true);   
	}); 
	$(".wl_tags label").click(function(){ 
		$(this).parent("div").siblings("div").children('i').css({display:'none'});
		$(this).parent("div").children('i').css({display:'block'});
		$(':radio[name="tag_id"]').removeAttr("checked");
		var cid = $(this).parent("div").attr("data-id");    
		$("input[name='tag_id'][value='"+cid+"']").attr("checked",true);   
	}); 

	$(".back").click(function(){
		$(".sele").hide();
		$(".footnav").show();
	})
    $(".sele_hos").click(function(){
    	$(".sele_1").show();
    	$(".footnav").hide();
    })
    $(".sele_tag").click(function(){
    	$(".sele_2").show();
    	$(".footnav").hide();
    })
</script>