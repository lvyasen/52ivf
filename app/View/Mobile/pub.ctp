<style>
.header,footer,.online-service{display:none;}
.pub{width:100%;height:auto;overflow:hidden;}
.pub .top{height:44px;padding-top:16px;}
.pub .top span{color:#323232;font-size:16px;float:left;margin-left:14px;}
.pub .top span a{color:#323232;}
.pub .top a{color:#323232;font-size:16px;float:right;margin-right:14px;}
.pub .wrap{width:100%;height:auto;}
.pub .wrap .title{width:94%;margin:0 auto;border-bottom:1px solid #eee;padding-bottom:16px;}
.pub .wrap .title input{width:100%;border:none;outline:none;padding-left:12px;border-left:2px solid #f29c9f;font-size:16px;color:#aaa;border-radius: 0;}
.pub .wrap .content{border:none;width:94%;height:200px;margin:0 auto;padding-top:16px;font-size:16px;display:block;outline:none; }

</style>
<div class="pub">
    <form  class="dataForm" action="/mobile/pub/<?=$type?>-<?=$cid?>.html" method="post">  
        <div class="top">
            <span onclick="history.go(-1)">取消</span>
            <a href="javascript:void(0)" class="submit">发布</a>
        </div>
        <div class="wrap"> 
            <textarea  name="content" datatype="*"  nullmsg="请填写评论内容！" class="content" placeholder="这一刻想说的..."></textarea>
        </div>   
    </form> 

</div>
<script type="text/javascript">

$(".dataForm").Validform({
    btnSubmit:".submit", 
    tiptype:function(msg){
        layer.alert(msg,{icon:0});    
    },
    tipSweep:true,
    ajaxPost:true,
    callback:function(o){
        layer.closeAll();
        if (o.status == 200) {
            layer.msg(o.message,{
                icon:6,
                skin:'layer_mobile_login'
            });       
            setTimeout("window.location.href='"+o.data.regerurl+"'", 2000);
        } else {
            layer.msg(o.message,{
                icon:5,
                skin:'layer_mobile_login'
            });    
        } 
    }
});
//判断键盘是否弹出
    var wHeight = window.innerHeight;   //获取初始可视窗口高度
    $(window).resize(function() {         //监测窗口大小的变化事件
        var hh = window.innerHeight;     //当前可视窗口高度
        if(wHeight > hh ){           //可以作为虚拟键盘弹出事件
            $(".footnav").hide();
        }else{         //可以作为虚拟键盘关闭事件
            $(".footnav").show();
        }
    });
</script>