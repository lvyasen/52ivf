<?php echo $this->Html->css('/css/mobile/name.css');?>
<?php echo $this->Html->script('/js/jquery.form.min.js');?>   
<?php echo $this->Html->script('/js/mobile/user.js');?>  
<?php $this->Sgwy->addJs('/js/mobile/bright.js');?>
<div class="name">
	<div class="nav_title">
		<p>我的昵称</p>
		<img class="lazy" onclick="window.location.href='/mobile/means/'" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
	</div>
	<div class="ipts">
	    <p>温馨提示：昵称将是您在试管无忧里独一无二的标志，请设置。</p> 
        <form  class="dataForm" action="/mobile/name" id="dataform"  method="post" >
		<div class="name_push">
            <i></i>
		    <input type="text" placeholder="请输入您的昵称" id="u_nick" name="u_nick"  >
		</div>		 
        <input type="hidden" name="dosubmit" value="1"/>   
		<button type="button" onclick="_user.uptNick(this);">保存</button>
		</form>
	</div>
</div>
