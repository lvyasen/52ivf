<?php echo $this->Html->css('/css/mobile/my_reservation.css');?>
<div class="my_reservation">
	<div class="nav_title">
		<p>我的预约</p>
		<img class="lazy" onclick="history.go(-1)" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
	</div>
	<div class="list">
	    <ul class="list_ul">
            <?php foreach($dataList as $key => $value):?>
            <?php if($value['UsersAppoint']['hospital_id']>0){ ?>
	    	<li>
	    		<div class="list_img">
	    			<img class="lazy" src="<?php echo $value['UsersAppoint']['hospital_image'];?>" data-original="<?php echo $value['UsersAppoint']['hospital_image'];?>">
	    		</div>
	    		<div class="list_text">
	    			<p><?php echo $value['UsersAppoint']['hospital'];?></p>
	    			<span>已预约</span>
	    			<div class="time"><i></i><?php echo Ivf::formatTime($value['UsersAppoint']['create_time']);?></div>
	    		</div>
	    	</li>
	    	<?php }else if($value['UsersAppoint']['doctor_id']>0){?>
	    	<li>
	    		<div class="list_img">
	    			<img class="lazy" src="<?php echo $value['UsersAppoint']['doctor_image'];?>" data-original="<?php echo $value['UsersAppoint']['doctor_image'];?>">
	    		</div>
	    		<div class="list_text">
	    			<p><?php echo $value['UsersAppoint']['doctor'];?></p>
	    			<span>已预约</span>
	    			<div class="time"><i></i><?php echo Ivf::formatTime($value['UsersAppoint']['create_time']);?></div>
	    		</div>
	    	</li>
	    	<?php }?>
            <?php endforeach;?>
	    	
	    </ul>
	</div>
</div>
