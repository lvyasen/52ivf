<?php echo $this->Html->script('/admin/js/ivf52/user.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.min.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.bootstrap.js');?>  
    <div class="margin Competence_style" id="page_style">
        <div class="operation clearfix"> 
            <a href="/adminmanage/roles_edit"  class="btn button_btn bg-deep-blue" title="添加角色"><i class="fa  fa-edit"></i>&nbsp;添加角色</a>  
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr> 
                        <!--<th class="center"><label><input type="checkbox" class="ace" onclick="checkAll(this, 'checkboxes')"><span class="lbl"></span></label></th>-->
                        <th>序号</th>
                        <th>角色名称</th>  
                        <th>角色描述</th>  
                        <th class="hidden-480">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($roleList as $key => $v) { ?>
                    <tr> 
                        <!--<td class="center"><label><input type="checkbox" class="ace" name="checkboxes"><span class="lbl"></span></label></td>-->
                        <td><?php echo $v['AdminGroup']['id'];?></td>
                        <td><?php echo $v['AdminGroup']['gname'];?></td>  
                        <td><?php echo $v['AdminGroup']['intro'];?></td>  
                        <td class="td-manage"> 
                            <a title="编辑" href="/adminmanage/roles_edit?id=<?= $v['AdminGroup']['id']?>" class="btn button_btn bg-deep-blue">权限</a>    
                            <?php if($userId == '9'): ?><a title="删除" href="javascript:;" onclick="delAct(this,'roles',<?php echo $v['AdminGroup']['id'];?>,'single')" class="btn button_btn btn-danger">删除</a><?php endif; ?>
                        </td>
                    </tr>
                    <?php }?> 
                </tbody>
            </table>
        </div>
    </div> 

<script type="text/javascript">  
var oTable1 = $('#data_table').dataTable( {
"bPaginate": true, 
"width":"100%", 
"bLengthChange":false,
"iDisplayLength": 20,
//"columns" : _tableCols, 
"bStateSave": false,//状态保存
"searching": false,
"aoColumnDefs": [{"orderable":false,"aTargets":[3]
}]

});
</script>
