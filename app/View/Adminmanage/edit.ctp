 
		<div class="margin">
			<form role="form" class="dataForm" action="<?= $_SERVER['REQUEST_URI'] ?>" method="post">
				<div class="add_style">
					<ul>
						<li class="clearfix"><label class="label_name col-xs-1"><i>*</i>用户名：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="user_name" type="text"  class="col-xs-4" value="<?php echo isset($dataList['user_name'])?$dataList['user_name']:''?>" datatype="s2-12" errormsg="用户名至少2个字符,最多12个字符！" nullmsg="请填写姓名！" /></div> </li> 
						<li class="clearfix"><label class="label_name col-xs-1"><i>*</i>邮箱：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="email" type="text"  class="col-xs-4" value="<?php echo isset($dataList['email'])?$dataList['email']:''?>"  datatype="e" errormsg="邮箱格式不正确！" nullmsg="请填写邮箱！"/></div></li> 
						<li class="clearfix"><label class="label_name col-xs-1">密码：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="password" type="password"  class="col-xs-4" value="" placeholder="不修改请留空，注册留空默认123456"  datatype="empty|*6-15" errormsg="密码范围在6~15位之间,不能使用空格！"  nullmsg="请填写密码！"/></div></li> 
						 
							<li class="clearfix"><label class="col-xs-1 col-lg-2 label_name" for="form-field-1">用户类型：</label>
							<div class="col-sm-9">
                               <label class="radio-inline"><input name="utype" class="ace radio" type="radio" value="1" <?php echo !isset($dataList['roles']) || $dataList['roles']=='all'?'checked':''?>><span class="lbl">超级管理员</span></label>
                               <label class="radio-inline"><input name="utype" class="ace radio" type="radio" value="2" <?php echo isset($dataList['roles']) && !in_array($dataList['roles'], array('all','3','4'))?'checked':''?>><span class="lbl">普通管理员</span></label> 
                           </div>
                       		</li>    
					    <li class="clearfix" id='show_2' style="<?php echo isset($dataList['roles']) && !in_array($dataList['roles'], array('all','3','4'))?'display:block':'display:none'?>"><label class="col-xs-1 col-lg-2 label_name" for="form-field-1">分派角色：</label>
					    <div class="col-xs-6 admin_name clearfix">
							<?php foreach ($roleList as $key => $v) :?> 
					       <label class="middle"><input class="ace" type="checkbox" name="roles[]" value="<?php echo $v['AdminGroup']['id'];?>" id="id-disable-check" <?php echo isset($dataList['roles'])  && strpos(','.$dataList['roles'].',',','.$v['AdminGroup']['id'].',') !== false?'checked':''?>><span class="lbl"><?php echo $v['AdminGroup']['gname'];?></span></label>
                        	<?php endforeach ?>
						</div></li> 
						<li class="clearfix">
							<label class="label_name col-xs-1">是否有效：&nbsp;&nbsp;</label>
							<div class="Add_content col-xs-9">
								<label class="l_f checkbox_time"><input type="checkbox" name="status" class="ace" id="checkbox" value="1"  <?php echo isset($dataList['status']) && $dataList['status']==1?'checked=checked':''?>><span class="lbl">是</span></label> 
							</div> 
						</li> 
					</ul>
					<!--按钮操作-->
					<li class="clearfix">
						<div class="col-xs-2 col-lg-2">&nbsp;</div>
						<div class="col-xs-6">
							<input class="btn button_btn bg-deep-blue" type="submit" value="保存提交"/>
							<input name="reset" class="btn button_btn btn-gray" value="取消重置" type="reset">
							<a href="/adminmanage/index/"  class="btn btn-info button_btn"><i class="fa fa-reply"></i> 返回上一步</a>
						</div>
					</li> 
					<input type="hidden" name="dosubmit" value="1"/>  
				</form>
			</div>
		</div> 
	<script type="text/javascript">  
		$(".dataForm").Validform({tiptype:3,datatype: {"empty": /^\s*$/}}); 
		$("input:radio[name='utype']").change(function(){
			$('input[type=radio]').each(function(index,item){ 
	            if(this.checked==true){
					$("#show_"+$(this).val()).attr('style','display:block');
	            }else{
					$("#show_"+$(this).val()).attr('style','display:none');
	            }
        	}); 
		});
	</script>