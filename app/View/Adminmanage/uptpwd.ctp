<?php echo $this->Html->script('/admin/js/ivf52/user.js');?>    
  <div class="margin" id="page_style">
   <div class="personal_info">
     <div class="add_style clearfix border_style">
       <form id="user_info" action="" method="post">
        <div class="clearfix">

          <div class="form-group clearfix col-xs-3">
            <label class="col-xs-3 label_name" for="form-field-1">用户名：</label> 
            <div class="col-xs-9 line_height1"> <span><?php echo $userInfo['user_name'];?></span></div>      
          </div>  
          <div class="form-group clearfix col-xs-3"><label class="col-xs-3 label_name" for="form-field-1">电子邮箱： </label>
            <div class="col-xs-9 line_height1"> <span><?php echo $userInfo['email'];?></span></div>    
          </div>
          <!--<div class="form-group clearfix col-xs-3"><label class="col-xs-3 label_name" for="form-field-1">所属部门： </label>
            <div class="col-xs-9 line_height1"> <span><?php echo $userInfo['organizations'];?></span></div>     
          </div>
          <div class="form-group clearfix col-xs-3"><label class="col-xs-3 label_name" for="form-field-1">所属职位： </label>
            <div class="col-xs-9 line_height1"> <span><?php echo $userInfo['title'];?></span></div>     
          </div>--> 
          <div class="form-group clearfix col-xs-3"><label class="col-xs-3 label_name" for="form-field-1">注册时间： </label>
            <div class="col-xs-9 line_height1"> <span><?php echo date('Y-m-d',strtotime($userInfo['add_time']))?></span></div>
          </div>
        </div>
        
        <div class="Button_operation clearfix">  
          <a href="javascript:void(0)" onclick="_user.uptPwd(this)" class="btn bg-green operation_btn">修改密码</a>     
        </div>
      </form>
    </div><div id="text_name"></div>
  </div>
  <!--操作记录--><!--
  <div class="admin_recording clearfix">
    <div class="searchs  clearfix  operation">
     <label class="label_name">记录搜索：</label>
     <input class="inline laydate-icon " id="start" type="text"  style=" margin-right:10px; height:auto; float:left; width:150px;" />
     <input name="" type="text"  class="form-control col-xs-1" style=" width:250px"/><button class="btn button_btn bg-deep-blue " onclick=""  type="button"><i class="fa  fa-search"></i>&nbsp;搜索</button>

   </div>
   <table id="sample_table" class="table table_list table_striped table-bordered dataTable no-footer">
     <thead>
      <tr>
        <th  width="80px">排序</th>
        <th>内容</th>
        <th width="150px">操作时间</th>
        <th width="300px">备注</th>       
      </tr>
    </thead>
    <tbody>
     <tr><td>1</td><td>添加了10个商品信息</td><td>2016-9-29 12:23</td><td>一个未保存成功</td></tr>
     <tr><td>2</td><td>添加了20个商品信息</td><td>2016-9-29 12:23</td><td>一个未保存成功</td></tr>
     <tr><td>3</td><td>添加了10个商品信息</td><td>2016-9-28 12:23</td><td>一个未保存成功</td></tr>
     <tr><td>4</td><td>添加了10个商品信息</td><td>2016-9-27 12:23</td><td>一个未保存成功</td></tr>
     <tr><td>5</td><td>添加了10个商品信息</td><td>2016-9-26 12:23</td><td>一个未保存成功</td></tr>
     <tr><td>6</td><td>添加了10个商品信息</td><td>2016-9-24 12:23</td><td>一个未保存成功</td></tr>
     <tr><td>7</td><td>添加了10个商品信息</td><td>2016-9-19 12:23</td><td></td></tr>
   </tbody>
 </table>   
</div>-->
</div>
<!--修改密码样式-->
<div class="change_Pass_style display" id="change_Pass">
 <form role="form" id="userForm" action="/admin/manage_uptpwd/" method="post">
  <ul class="change_Password clearfix">
     <li><label class="label_name">原&nbsp;&nbsp;密&nbsp;码</label><span class="change_text"><input name="password" data-name="原密码" type="password" class="" id="password"></span></li>
     <li><label class="label_name">新&nbsp;&nbsp;密&nbsp;码</label><span class="change_text"><input name="Nes_pas"  data-name="新密码" type="password" class="" id="Nes_pas"></span></li>
     <li><label class="label_name">确认密码</label><span class="change_text"><input name="c_mew_pas" type="password" data-name="再次确认密码" class="" id="c_mew_pas"></span></li>              
   </ul> 
  <input type="hidden" name="dosubmit" value="1"/>   
 </form>         
 </div> 