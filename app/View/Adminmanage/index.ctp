     <?php echo $this->Html->script('/admin/js/ivf52/user.js');?>
	<?php echo $this->Html->script('/admin/js/jquery.dataTables.min.js');?>
	<?php echo $this->Html->script('/admin/js/jquery.dataTables.bootstrap.js');?>  
	<div class="margin Competence_style" id="page_style">
		<div class="operation clearfix"> 
			<a href="/adminmanage/edit/"  class="btn button_btn bg-deep-blue" title="添加管理员"><i class="fa  fa-edit"></i>&nbsp;添加管理员</a> 
			<div class="search  clearfix"> 
            	<form role="form" id="userForm" action="/adminmanage/index/" method="get">
				<input name="keywords" type="text" value="<?= @$_GET['keywords'] ?>" placeholder="名称/email" class="form-control col-xs-8"/><button class="btn button_btn bg-deep-blue " onclick="this.form.submit()" type="button"><i class="fa  fa-search"></i>&nbsp;搜索</button>
				</form>
			</div>
		</div>
		<div class="compete_list">
			<table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
				<thead>
					<tr> 
						<th>序号</th>
						<th>用户名</th>
						<th>email</th>
						<th>角色</th> 
						<th>状态</th>         
						<th class="hidden-480">注册时间</th>  
						<th class="hidden-480">操作</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($dataList as $key => $v) { ?>
					<tr> 
						<td><?php echo $v['AdminUser']['id'];?></td>
						<td><?php echo $v['AdminUser']['user_name'];?></td>
						<td><?php echo $v['AdminUser']['email'];?></td> 
						<td><?php echo $v['AdminUser']['roles'];?></td>  
						<td class="td-status"><span  class="<?php echo $v['AdminUser']['status']==1?'yesimg':'noimg';?>" onclick="toggle(this, 'manages', <?php echo $v['AdminUser']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
						<td><?php echo date("Y-m-d H:i:s",strtotime($v['AdminUser']['add_time']))?></td>
						<td class="td-manage"> 
							<?php if($v['AdminUser']['roles']!='all'){?><a title="编辑" href="/adminmanage/edit/?id=<?php echo $v['AdminUser']['id'];?>" class="btn button_btn bg-deep-blue">编辑</a> <?php }?> 
							<?php if($userRoles=='all'){?>   <a title="删除" href="javascript:;" onclick="delAct(this,'manages',<?php echo $v['AdminUser']['id'];?>,'single')" class="btn button_btn btn-danger">删除</a><?php }?>   
						</td>
					</tr>
					<?php }?> 
				</tbody>
			</table>
		</div>
	</div> 
<script type="text/javascript">  
var oTable1 = $('#data_table').dataTable( {
"bPaginate": true, 
"width":"100%",	
"bLengthChange":false,
"iDisplayLength": 20,
//"columns" : _tableCols, 
"bStateSave": false,//状态保存
"searching": false,
"aoColumnDefs": [{"orderable":false,"aTargets":[6]
}]

});
</script>
