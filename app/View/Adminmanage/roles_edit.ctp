 
        <div class="margin" id="page_style">
            <form role="form" id="userForm" action="/adminmanage/roles_edit" method="post"  class="dataForm">
            <div class=" add_Competence_style margin" id="add_Competence_style">
                <ul class="add_style">
                    <li class="clearfix"><label class="label_name col-xs-1 col-lg-2">角色名称：</label><span class="col-xs-6"><input name="role_name" type="text" value="<?php echo isset($roleList['gname'])?$roleList['gname']:''?>" class="col-xs-5"  datatype="s2-10" errormsg="角色名称至少2个字符,最多10个字符！"  nullmsg="请填写角色名称！"/></span></li>
                    <li class="clearfix"><label class="col-xs-1 col-lg-2 label_name" for="form-field-1">角色描述：</label>
                        <span class="col-xs-6"><textarea name="role_intro" class="form-control col-xs-10" id="form_textarea" placeholder="" onkeyup="checkLength(this,100,'sy');"><?php echo isset($roleList['intro'])?$roleList['intro']:''?></textarea><span class="wordage">剩余字数：<span id="sy" style="color:Red;">100</span>字</span></span>
                    </li> 
                </ul>
                </div>
                <div class="Competence_list">
                    <div class="title_name"><span>权限列表</span></div>
                    <div class="list_cont clearfix">
                        <?php foreach ($menu as $key => $val): 
                            $cked2=''; 
                            if(isset($roleList['sourceroleids']) && !empty($roleList['sourceroleids']) && in_array($key, $roleList['sourceroleids']))
                            { 
                                $cked2='checked=checked';
                            }
                        ?> 
                        <div class="clearfix col-xs-4 col-lg-6 ">
                            <dl class="Competence_name"> 
                                <dt class="Columns_One"><label class="middle"><input class="ace" type="checkbox" <?php echo $cked2;?> value="<?= $key ?>" name="roles[]" id="id-disable-check"><span class="lbl"><?= $menuTitle[$key] ?></span></label></dt>
                                <dd class="permission_list clearfix">

                                <?php foreach ($val as $kk => $vv): 
                                        $cked=''; 
                                        if(isset($roleList['sourceroleids']) && !empty($roleList['sourceroleids']) && in_array($kk, $roleList['sourceroleids']))
                                        { 
                                            $cked='checked=checked';
                                        }
                                    ?> 
                                    <label class="middle"><input class="ace" type="checkbox" name="roles[]" value="<?= $kk ?>" <?php echo $cked;?> id="id-disable-check"><span class="lbl"><?= $vv['label'] ?></span></label> 
                                <?php endforeach ?>
                                </dd>
                            </dl>
                        </div>
                        <?php endforeach ?>
                    </div>
                </div>
                <!--按钮操作-->
                <div class="Button_operation btn_width">
                    <input class="btn button_btn bg-deep-blue" type="submit" value="提交"/>
                    <a href="/adminmanage/roles" class="btn btn-info button_btn"><i class="fa fa-reply"></i> 返回上一步</a>
                </div>
            </div>
            <input type="hidden" name="dosubmit" value="1"/> 
            <input type="hidden" name="rid" value="<?= @$_GET['id'] ?>"/>  
            </form> 
        <script type="text/javascript">
        $(".dataForm").Validform({tiptype:3});   
    /*按钮复选框选择*/
    $(".Competence_name dt input:checkbox").click(function(){
        $(this).closest("dl").find("dd input:checkbox").prop("checked",$(this).prop("checked"));
    });
    $(".permission_list input:checkbox").click(function(){
        var l =$(this).parent().parent().find("input:checked").length;
        if($(this).prop("checked")){
            $(this).closest("dl").find("dt input:checkbox").prop("checked",true);
            $(this).parents(".Competence_name").find("dt").first().find("input:checkbox").prop("checked",true);
        }
        else{
            if(l==0){
                $(this).closest("dl").find("dt input:checkbox").prop("checked",false);
            }           
        }       
    });
        </script>