<?= $this->Sgwy->addCss('/css/site/minfo.css'); ?>
<!-- <?= $this->Sgwy->addJs('/plugin/ad-gallery/clipboard.js');?> -->
<?= $this->Sgwy->addJs('/js/site/minfo.js');?>
<?php App::uses('Sgwy', 'Lib'); ?>
<div class="content-wrap">
	<div id="wav_title">
        <p>最新活动</p>
        <img class="lazy" onclick="history.go(-1)" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
    </div>
    <div class="content">
		<div class="wap_edm_index">
			<div class="wap_edm_main">
				
				<div class="wap_ban">
					 <div id="flashBg">
				        <div id="flashLine">
				            <div id="flash"> 
				                <a id="flash1"  target="_blank" style="display:block"><img src="/images/site/010.jpg" width="100%" /></a>
				                <a id="flash2"  target="_blank"><img src="/images/site/011.jpg" width="100%"  /></a>
				                <a  target="_blank" id="flash3"><img src="/images/site/012.jpg" width="100%" /></a>
				                <div class="flash_bar" id="control_point">
				                    <div class="dq"  id="f1" onClick="changeflash(1)"></div>
				                    <div class="no"  id="f2" onClick="changeflash(2)"></div>
				                    <div class="no"  id="f3" onClick="changeflash(3)"></div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>
				
				<div class="title">【赴泰考察】 泰国全球生殖中心考察之旅</div>
				<!-- <div class="text">1月除夕前期，试管无忧将带你免费考察泰国知名生殖中心，去四面佛祈福好孕，开开心心“造人”</div> -->
				<div class="guod"></div>
				<div class="cont">
					<div class="edm_xuan">
						<div class="chufa"><span class="glyphicons glyphicons-plane left_fj"></span>出发城市：<span>您所在地</span></div>
						<div class="zhongdian"><span class="glyphicons glyphicons-nearby-circle right_cs"></span>到达城市：<span>泰国曼谷</span></div>
					</div>
					<div class="edm_hui">
						只需1分钟,即可获得国外专家一对一就诊的机会!
					</div>
					<div class="edm_ling">
						<div class="wx">
							<div class="tou" style="background: url(/images/site/nana.png) center top no-repeat;"></div>
							<div class="text_contt">
								<div class="sha">
									<div class="textt">
										娜娜
										<span>行程定制师</span>
									</div>
									<!-- <div class="tdd">加我微信</div> -->
								</div>
								<div class="xia">
									<div class="left">
										<img class="lazy" src="/images/common/grey.gif" data-original="/images/site/shiditubiao.png" align="center">实地认证
									</div>
									<div class="right">
										<img class="lazy" src="/images/common/grey.gif" data-original="/images/site/biaoqian.png" align="center">现场签单，立享优惠
									</div>
								</div>
							</div>
						</div>
						<div class="contentt">
							<span class="mr">服务特色</span><br>
							<span>【名医零距离】</span> 亚洲顶尖专家一对一会诊<br>
							<span>【参观实验室】 </span>深度了解国外先进技术细节<br>
							<span>【好孕祈福之旅】</span>朝拜四面佛坛，许下求子心愿<br>
							<span>【畅游泰国岛屿】</span>欣赏美丽的沙美岛风景
							<div class="fangh">
								<marquee scrollamount="3">
									本次活动也是为了不孕不育的患者而举行的，让大家不仅可以体验到最专业的医生护士团队同时还可以观摩整个就医环节。本月报名的前10名将免费体验这次活动，名额有限，抓紧时间。
								</marquee>
							</div>
						</div>
					</div>
					<div class="edm_yellow">
						<span>3人以上拼团可免医院考察费！</span>
						<p>￥698<i>/元起</i></p>
					</div>
					<div class="edm_submit">
						<a class="wap_submit">提交申请</a>
					</div>
				</div>
				<div class="guod"></div>
				<div class="second_t">
					<div class="title_s">行程详情</div>
					<div class="bankuai">
						<div class="title_z"><i>Step<span>1</span></i><p>您的所在地-曼谷</p></div>
						<div class="pic">
							<img class="lazy" src="/imag/grey.gif" data-original="/images/site/m/picture01.jpg">
						</div>
						<div class="contt">
							<div class="cc">
								<p class="left">行程安排：</p>
								<p class="right red">乘飞机赴泰&nbsp;>&nbsp;曼谷易思庭酒店</p>
							</div>
							<div class="cc">
								<p class="left">自理费用：</p>
								<p class="right">机票、酒店费用自理</p>
							</div>
							<div class="cc">
								<p class="left">行程提示：</p>
								<p class="right">自行前往机场，搭乘国际航班前往泰国首都--曼谷。请提前3个小时抵达您所在地机场，办理登机手续。<br>
								由于泰国全年平均气温在28度，请做好防晒措施，酒店出于环保因素，无论星级，均须自备牙膏、牙刷、拖鞋、电吹风机、等个人生活用品。</p>
							</div>
						</div>
					</div>
					<div class="bankuai">
						<div class="title_z"><i>Step<span>2</span></i><p>曼谷易思庭酒店-全球生殖中心</p></div>
						<div class="pic">
							<img class="lazy" src="/imag/grey.gif" data-original="/images/site/m/picture02.jpg">
						</div>
						<div class="contt">
							<div class="cc">
								<p class="left">行程安排：</p>
								<p class="right red">参观全球生殖中心&nbsp;>&nbsp;参观胚胎培育室&nbsp;>&nbsp;与提迪贡博士（Dr. Thitikorn ）一对一会诊</p>
							</div>
							<div class="cc">
								<p class="left">自理费用：</p>
								<p class="right">无</p>
							</div>
							<div class="cc">
								<p class="left">医院详情：</p>
								<p class="right">全球生殖中心(Worldwide Fertility Center)研究所成立于2008年，由前芭亚泰国际医院生殖中心创始人提迪贡博士创办，又称孕诚生殖中心。
								中心拥有完善的胚胎培育实验室、精子检验分析实验室、 遗传学实验室和常规实验室，配备了齐全的现代化设备。提迪贡博士是泰国著名的辅。
								助生殖专家，有着28年不孕不育治疗经验，他在对卵巢功能衰退的高龄妇女进行促排方面、胚胎培育方面有特别丰富的经验，因此试管婴儿成
								功率远远高于泰国同行。</p>
							</div>
							<div class="cc">
								<p class="left">医生介绍：</p>
								<p class="right">提迪贡博士(Dr. Thitikorn wanichkul M.D)是泰国著名的试管婴儿专家，主要从事不孕不育的治疗和子宫内膜异位治疗。有着二十多年不孕不育的治疗经验。</p>
							</div>
						</div>
					</div>
					<div class="bankuai">
						<div class="title_z"><i>Step<span>3</span></i><p>全球生殖中心-四面佛</p></div>
						<div class="pic">
							<img class="lazy" src="/imag/grey.gif" data-original="/images/site/m/picture03.jpg">
						</div>
						<div class="contt">
							<div class="cc">
								<p class="left">行程安排：</p>
								<p class="right red">祈福求子&nbsp;>&nbsp;欣赏曼谷夜景</p>
							</div>
							<div class="cc">
								<p class="left">自理费用：</p>
								<p class="right">无</p>
							</div>
							<div class="cc">
								<p class="left">行程提示：</p>
								<p class="right">四面佛，人称“有求必应”佛，该佛有四尊佛面，分别代表健康、事业、爱情与财运，掌管人间的一切事务，是泰国香火最旺的佛像之一。对
								信奉佛教的人来说，到曼谷来不拜四面佛，就如入庙不拜神一样，是一件不可想像的事。据说四面佛的灵验超乎寻常，因此，有些游客为了在
								四面佛前许誓还愿而多次往返泰国，更有许多港台影视明星，年年都来泰国膜拜四面佛，可见四面佛的魅力。</p>
							</div>
						</div>
					</div>
					<div class="bankuai">
						<div class="title_z"><i>Step<span>4</span></i><p>四面佛-沙美岛</p></div>
						<div class="pic">
							<img class="lazy" src="/imag/grey.gif" data-original="/images/site/m/picture04.jpg">
						</div>
						<div class="contt">
							<div class="cc">
								<p class="left">行程安排：</p>
								<p class="right red">沐浴阳光&nbsp;>&nbsp;海边餐饮</p>
							</div>
							<div class="cc">
								<p class="left">自理费用：</p>
								<p class="right">具体旅游活动项目自费</p>
							</div>
							<div class="cc">
								<p class="left">行程提示：</p>
								<p class="right">沙美岛是热带季风气候，本该有着明显的雨季和旱季的区分。但这里濒临海边，海风凉爽宜人，所以一年四季都能保持美景呈现在游客们眼前
								气温差别不大，波动很小，几乎感觉不到雨季和旱季的来临。这里终年的气温都差不多，平均气温大概在二十摄氏度。</p>
							</div>
						</div>
					</div>
					<div class="bankuai">
						<div class="title_z"><i>Step<span>5</span></i><p>曼谷-您的所在地</p></div>
						<div class="pic">
							<img class="lazy" src="/imag/grey.gif" data-original="/images/site/m/picture05.jpg">
						</div>
						<div class="contt">
							<div class="cc">
								<p class="left">行程安排：</p>
								<p class="right red">返程回国</p>
							</div>
							<div class="cc">
								<p class="left">回国须知：</p>
								<p class="right">1.检查好您的行李，避免遗漏。
								<br>2.交验护照，签证，机票（纸票），行程单（电子票）。</p>
							</div>
						</div>
					</div>
				</div>
				<!-- <div class="guod"></div>
				<div class="sm">
					<div class="title_s">行程详情</div>
					<div class="cont_s">
						<div class="div">报名时间：<span>2017年 1月3日 - 1月31日</span></div>
						<div class="div">考察日期：<span>2017年 2月13日 - 2月16日</span>（如行程变动，以试管无忧官方通知为准）。</div>
						<div class="div">报名方式：直接点击提交申请，填写报名信息。</div>
						</div>
				</div> -->
				<div class="guod"></div>
				<div class="sm">
					<div class="title_s">注意事项</div>
					<div class="cont_s">
						<div class="div">试管无忧秉承着将最珍贵的考察机会留给最有需要的姐妹，请您珍惜这次千载难逢的机会，如无需要请不要随便报名，以免耽误其他更需要试管帮助的姐妹，如果考察期间确定进周可额外获得价值 <span>599 RMB</span>试管无忧服务抵用券。</div>
						<div class="div">因考察期间，所有预订的酒店不予退订，为了避免临行前决定不赴泰而造成无法成团事件，需要您赴泰前先交<span>2,000 RMB</span>酒店预订金（如您在出发前临时不去，则预订金不予退还），回国后三个工作日内全部退回至您指定的银行账号。</div>
						<div class="div">在赴泰考察期间，所有产生的机票、酒店、饮食、交通等费用自行承担。</div>
						<div class="div">请获奖的姐妹尽快在赴泰考察前办理好您的护照、签证，以免错过考察之旅。</div>
					</div>
				</div>
				<div class="guod"></div>
				<div class="sm">
				<form  class="videoForm" action="/site/comment"  method="post" >
					<div class="title_s">用户点评<span></span>
					</div>
					<div class="cont_s">
						<textarea rows="3"  name="content" placeholder="请说明您的评价内容" datatype="*"  nullmsg="请填写评论内容！" class="textare" id="msg" autocomplete="off"></textarea>
					</div>
					<div class="cont_s mr">
						<input class="inqut_z" placeholder="请输入您预约的手机号"  name="u_mobile"  datatype="m" errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！"  id="mobile" autocomplete="off">
						<div class="submit">提交留言</div>
					</div>
					<div class="span_d"></div>
					<input type="hidden" name="dosubmit" value="1"/>
				</form>
				</div>
				<div class="guod"></div>
				<div class="sm">
					<div class="title_s">获奖名单<span></span>
					</div>
					<div class="cont_s mt">
						<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
							<tr>
							   <th>地区</th>
							   <th>昵称</th>
							   <td>联系方式</td>
							   <td>时间</td>
							</tr>
						</table>
						<div id="mydiv" onMouseOver="iScrollAmount=0" onmouseout="iScrollAmount=1" style="width:100%; height:100px; overflow:hidden; margin:0 auto;">
							<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
							    <tr>
								    <th>上海</th>
								    <th>王女士</th>
								    <td>1379****310</td>
								    <td>2016.12.22</td>
							    </tr>
								<tr>
								    <th>武汉</th>
								    <th>孙女士</th>
								    <td>1370****891</td>
								    <td>2016.12.22</td>
								</tr>
								<tr>
								    <th>深圳</th>
								    <th>王女士</th>
								    <td>1350****022</td>
								    <td>2016.12.23</td>
								</tr>
								<tr>
								    <th>深圳</th>
								    <th>胡女士</th>
								    <td>1562****899</td>
								    <td>2016.12.24</td>
								</tr>
								<tr>
								    <th>上海</th>
								    <th>沈女士</th>
								    <td>1377****225</td>
								    <td>2016.12.24</td>
								</tr>
								<tr>
								    <th>南京</th>
								    <th>赵女士</th>
								    <td>1358****742</td>
								    <td>2016.12.25</td>
								</tr>
								<tr>
								    <th>广州</th>
								    <th>赵女士</th>
								    <td>1363****298</td>
								    <td>2016.12.26</td>
								</tr>
								<tr>
								    <th>佛山</th>
								    <th>孙女士</th>
								    <td>1856****411</td>
								    <td>2016.12.27</td>
								</tr>
								<tr>
								    <th>上海</th>
								    <th>戴女士</th>
								    <td>1316****698</td>
								    <td>2016.12.28</td>
								</tr>
								<tr>
								    <th>杭州</th>
								    <th>王女士</th>
								    <td>1385****217</td>
								    <td>2016.12.29</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="guod"></div>
				<div class="sm" id="bgw">
					<div class="title_s">热门评论
						<span class="quanbu" id='look_all_msg_btn'>查看全部评论</span>
					</div>
					<div class="cont_s mr">
						<div id='leave_msg_box'>
							<?php foreach ($comments as $k => $val):?>
								<div class="pingl" id="bgw">
									<div class="tou_pic">
										<img src="/images/site/pic-female.jpg">
									</div>
									<div class="cont">
										<div class="title">
											<span><?php echo $val['SiteComment']['mobile'];?></span>
										</div>
										<div class="text"><?php echo $val['SiteComment']['content'];?></div>
										<div class="date"><?php echo $val['SiteComment']['comment_time'];?></div>
									</div>
								</div>
								<?php endforeach;?> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<form  class="siteForm" action="/site/index"  method="post" >
<div class="edm_tk">
	<div class="edm_tk_bg"></div>
	<div class="edm_tk_box">
		<div class="top"><span class="glyphicons glyphicons-remove cha" aria-hidden="true"></span></div>
		<div class="dw">
			<div class="tou_pic">
				<img src="/images/site/tubiao01.png">
			</div>
		</div>
		<div class="cont crrter">
			<div class="text_x">请填写您的申请资料</div>
			<div class="textdiv">
				<span class="glyphicons glyphicons-user"></span>
				<input type="text" class="input" id='nickname' value="" name="u_name"  placeholder="您的称呼" autocomplete="off" >
			</div>
			<div class="textdiv">
				<span class="glyphicons glyphicons-earphone"></span>
				 <input type="text" class="input" id='sub_mobile' value="" name="u_mobile"  placeholder="您的手机" autocomplete="off" >
			</div>
			 <div class="submi">提&nbsp;交&nbsp;申&nbsp;请</div>
			 <div class="text">
			 	<span class="span"></span>
			 	<span class="span_ok"></span>
			 </div>
		</div>
		<div class="cont_r">
			<span class="glyphicons glyphicons-tick" aria-hidden="true"></span>
			<p>提交成功</p><br>我们将在1-2个工作日与您联系
		</div>
		<div class="bottom"></div>
	</div>
</div>
<input type="hidden" name="dosubmit" value="1"/>  
</form>   