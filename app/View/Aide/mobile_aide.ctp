<?= $this->Sgwy->addCss('/css/aide/mobile_aide.css'); ?>
<?php $this->Sgwy->addJs('/js/aide/mobile_aide.js')?>
<?= $this->Sgwy->addJs("/js/mobile/user.js"); ?>
<script type="text/javascript">
	$(function(){
	var heig=$(window).height()-96;
	$(".con_01").css("height",heig);
});
</script>
<div class="m_aide">
	<div class="wav_title">
        <p>试管助手</p>
        <img class="lazy" onclick="history.go(-1)" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
    </div>
	<div class="title">
		试管助手
		<a href="javascript:history.go(-1)"><img src="/images/aide/zuojiantou.png" border="0"></a>
	</div>
	<div class="main">
		<div class="main_01 con_01">
		    <div>	
		        <h3>试管助手</h3>
				<p>一分钟为您预测试管婴儿方案<br>并估算您的成功率</p>
			    <a class="begin btn_01">马上开始<span>></span></a>
		    </div>
		</div>
		<div class="bar">
			<div class="bar_length"></div>
            <span><i class="bar_sco">0</i>%</span>	
		</div>
		<?= $this->element('aide'); ?>
		<div class="con_03 conts">
			<p>试管助手成功率的算法基于真实用户的大数据信息，为防止虚假数据干扰准确性，请输入手机号以确保用户信息的真实性：</p>
            <form  class="aideForm" action="/aide/mobile_aide"  method="post" >
			<input class="phone" type="text" placeholder="请输入您的手机号" id="aide_mobile" name="u_mobile"  datatype="m" errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！"><br>
			<input class="code" type="text" placeholder="填写验证码" name="u_code" value="" placeholder="输入6位验证码" datatype="*6-6" errormsg="请输入6位验证码！"  nullmsg="请输入6位验证码！">
            <input class="pull_code send_sms_btn" type="button"   onclick="_user.sendAidecode('send_aide_code')" value="获取验证码"/> 
			<div class="btn_03 btn_02 button aidesubmit">马上预测</div>
            <input type="hidden" name="dosubmit" value="1"/>                 
		</form>
		</div>
		<div class="main_04">
			<div class="bg">
				<img class="img_01" src="/images/aide/huan01.png" alt="">
				<img class="img_02" src="/images/aide/huan02.png"  alt="">
				<div class="num"><span id="change">0</span>% <p>成功率</p></div>
			</div>
			<div class="box">
				<i>参考建议</i>
				<p>通过您第一胎的受孕情况知道二胎也有机会自然受孕，同时疤痕子宫在修复不良或疤痕处着床情况下会有流产、大出血、子宫破裂等的风险；</p>
				<p>您的身体状况：很健康，孕前的检查也是不可少的哟；</p>
				<p>您的老公性生活频率低，备孕前需要评估男方生育能力。</p>
			</div>
			<div class="newsubmit">重新测试</div>
		</div>
	</div>
	<script type="text/javascript"> 
    $(".aideForm").Validform({
        btnSubmit:".aidesubmit", 
        tiptype:function(msg){
            layer.alert(msg,{
            	icon:0
            });    
        },
        tipSweep:true,
        ajaxPost:true,
        callback:function(o){
            layer.closeAll();
            if (o.status == 4000) { 
                $('.login').trigger("click");    
            }
            else if (o.status == 200) {     
				$(".main_04").show();
				$(".con_03").hide();
            } else {
                layer.alert(o.message,{icon:0});    
            } 
        }
    });
	</script>
<?= $this->element('footnav'); ?>