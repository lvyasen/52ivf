<?= $this->Sgwy->addCss('/css/sitemap/index.css'); ?>
<div class="sitemap">
	<div class="content">
		<p><a href="/">首页</a> > <a href="">网站地图</a></p>
		<div class="con">
			<?= $this->element('leftnav'); ?>
			<div class="main">
				<div id="wav_title" style="margin-left: 0;">
			        <p>网站地图</p>
			        <img class="lazy" onclick="history.go(-1)" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
                </div>
                <h1>网站地图</h1>
                <?php foreach ($dataList as $key => $value):?>

				<div>
					<span><?=$value['MapCate']['name']?></span>
					<?php if(sizeof($value['MapCate']['map'])>0){?>
					<div class="tag">
                		<?php foreach ($value['MapCate']['map'] as $key => $value):?>
						<a href="<?=$value['Map']['url']?>"><?=$value['Map']['name']?></a> 
                		<?php endforeach;?>
					</div>
					<?php }?>
				</div>
                <?php endforeach;?>
			</div>
		</div>
	</div>
</div>

