<?= $this->Sgwy->addCss('/css/join/info.css'); ?>
<div class="join_info">
	<div class="content">
		<p><a href="/">首页</a> > 加入我们</p>
		<div class="main">
	        <div id="wav_title">
		        <p style="">加入我们</p>
		        <img class="lazy" onclick="history.go(-1)" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
    		</div>
    		<div class="left">
    			<div class="left_top">
    				<p>上海韵瑶文化传播有限公司</p>
                    <a href="https://www.lagou.com/gongsi/131537.html" target="_blank">查看主页</a>
    			</div>
    			<div class="left_bot">
                    <h2>基本信息</h2>
                    <ul>
                        <li>http://www.ivf52.com</li>
                        <li>海外医疗</li>
                        <li>50-100人</li>
                        <li>成长型（B轮）</li>
                        <li>上海市奉贤区奉浦大道97号绿地智尊A座916室</li>
                    </ul>      
                    <div class="tag">
                        <a>定期体检</a>
                        <a>带薪年假</a>
                        <a>午餐补助</a>
                        <a>五险一金</a>
                        <a>节日礼物</a>
                        <a>福利多</a><a>定期旅游</a><a>生日派对</a><a>弹性工作</a><a>领导好</a>
                    </div>    
                </div>
    		</div>
    		<div class="right">
                <div class="right_con">
                    <div class="column">
                        <div class="tag_title">试管无忧招聘</div>
                        <h1><?php echo $info["Join"]["title"];?></h1>
                        <em>
                            <?php 
                                $salary="面议";
                                if(!empty($info['Join']['SalaryA']) && !empty($info['Join']['SalaryB']))
                                    $salary=$info['Join']['SalaryA']."-".$info['Join']['SalaryB'];
                                elseif(!empty($info['Join']['SalaryA']))
                                    $salary=$info['Join']['SalaryA'];
                                elseif(!empty($info['Join']['SalaryB']))
                                    $salary=$info['Join']['SalaryB'];
                                echo $salary;
                            ?>
                        </em><span><?=$education[$info['Join']['Education']]?>/<?=$experience[$info['Join']['Experience']]?></span>
                        <div class="co_b">扁平化管理、午餐、晋升空间、有一群有梦想、有追求的伙伴</div>
                    </div>   
                    <div class="job">
                        <h2>职位描述</h2>
                        <div class="jo_list"><?php echo $info["Join"]["description"];?></div>
                    </div>   
                    <div class="trem">
                        <h2>任职资格</h2>
                        <div class="jo_list"><?php echo $info["Join"]["Qualification"];?></div>
                        <p>工作时间：早8:30-晚17:30 周末双休 法定节假日休息</p> 
                    </div>
                    <div class="department">
                        <h2>所属部门</h2>
                        <p><?php echo $info["Join"]["Dept"];?></p>
                    </div>
                </div>
                <div class="right_bot">*亲~如果您觉得符合上述要求，可以将您的简历发送到邮箱：<span>bhu@ivf52.com</span> HR会在1~2个工作日与您取得联系。</div>
            </div>
    	</div>
	</div>
</div>
