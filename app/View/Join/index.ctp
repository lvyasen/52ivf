<?= $this->Sgwy->addCss('/css/join/index.css'); ?>
<?php echo $this->Html->script('/js/page.js');?>  
<div class="join">
	<div class="content">
		<p><a href="/">首页</a> > <a href="#">加入我们</a></p>
		<div class="con">
			<?= $this->element('leftnav'); ?>
			<div class="main">
			    <h2>加入我们</h2>
		       	<div id="wav_title">
			        <p>加入我们</p>
			        <img class="lazy" onclick="history.go(-1)" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
		        </div>
			    <div id="loadPage">

		        <ul class="job_list">
                    <?php foreach($dataList as $key => $value):?>
		        	<li>
		        		<div class="li_left"><?=$value['Join']['title']?><br><span><?php echo Ivf::formatTime($value['Join']['create_time']);?></span></div>
		        		<div class="li_cen">
		        			<?php 
		        				$salary="面议";
		        				if(!empty($value['Join']['SalaryA']) && !empty($value['Join']['SalaryB']))
		        					$salary=$value['Join']['SalaryA']."-".$value['Join']['SalaryB'];
		        				elseif(!empty($value['Join']['SalaryA']))
		        					$salary=$value['Join']['SalaryA'];
		        				elseif(!empty($value['Join']['SalaryB']))
		        					$salary=$value['Join']['SalaryB'];
		        				echo $salary;
		        			?>
		        		 <br><span><?=$education[$value['Join']['Education']]?>/<?=$experience[$value['Join']['Experience']]?></span></div>
		        		<div class="li_right"><a href="/join/info/<?php echo $value["Join"]["id"].'.html';?>">查看职位</a></div>
		        	</li>
                    <?php endforeach;?>
		        </ul>
                    <?php if($dataCount > 5){?>
                    <div class="more">
                        <a href="javascript:void(0)" title=""  onclick="_page.getJoins(this,5)">加载更多</a>
                    </div>  
                    <?php }?> 
			    </div>
				
			    <div class="more_list">
			    	<?php echo $strPages;?> 
			    </div>
			</div>
		</div>
	</div>
</div>