<?php $this->Sgwy->addCss('/css/news/info.css');?>
<!-- <?= $this->Sgwy->addJs('/js/news/info.js'); ?> -->
<div class="news_info">
    <div class="content">
    	<div class="top">
	        <p><a href="/">首页</a> > <?php if($info["New"]["category_id"]==4){?><a href="/news/index/">新闻资讯</a><?php }else{?> <a href="/headlines/index/">常见问题</a> <?php }?> ><!--  <?php if(!empty($cate_name)){?><a href="/news/index/<?php echo $info["New"]["category_id"].".html";?>"><?php echo $cate_name;?></a> > --><?php }?> <?=$info["New"]["title"]?></p>
	    </div>
        
        <div id="wav_title">
            <p><?php if($info["New"]["category_id"]==4){?> 资讯详情<?php }else{?>试管详情 <?php }?></p>
            <img class="lazy" onclick="history.go(-1)" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
        </div>
	    <div class="main">
	    	<div class="subject">
                <div class="wrap">
                    <h1><?php echo $info["New"]["title"];?></h1>
                    <div class="personal"> 
                        <img src="<?=$info["New"]["avatar"]?>" style=" margin-top: 0px;">
                        <span><?=$info["New"]["username"]?></span>
                        <em><?=$info["New"]["create_time"]?></em>
                    </div>
                    <div id="cont_detail"><?=$info["New"]["content"]?> </div>
                </div> 
	    	</div>
            <div class="next">
                <?php if($prev>0){?><a href="/news/info/<?php echo $prev.".html";?>">上一篇：<span><?=$prev_title?></span></a><?php }?>
                <?php if($next>0){?><a href="/news/info/<?php echo $next.".html";?>">下一篇：<span><?=$next_title?></span></a><?php }?> 
            </div>
	    </div>
	    <div class="fixed"> 
            <div class="right_fix">
                <?= $this->element('leftnav'); ?>
    	        <div class="fix_03">
                    <p>热门推荐</p>
                    <ul>
                        <?php foreach($top4List as $key => $value):?>
                        <a href="/news/info/<?=$value['New']['id']?>.html"><li>
                            <img class="lazy" src="<?=$value['New']['image']?>" data-original="<?=$value['New']['image']?>">
                            <div>
                                <span><?=$value['New']['title']?></span>
                                <p><?=$value['New']['description']?></p>
                            </div>
                        </li></a>
                        <?php endforeach;?> 
                    </ul>
                </div>
            </div>
	    </div>
    </div>
	
</div>
<script type="text/javascript">

$(".answerForm").Validform({
    btnSubmit:".release", 
    tiptype:function(msg){
        layer.alert(msg,{icon:0});    
    },
    tipSweep:true,
    ajaxPost:true,
    callback:function(o){
        layer.closeAll();
        if (o.status == 4000) { 
            $('.login').trigger("click");    
        }
        else if (o.status == 200) {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});       
            setTimeout(function () {
                window.location.reload();
            }, 1000);   
        } else {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
        } 
    }
});
$("#cont_detail img").removeAttr("alt");
$("#cont_detail img").attr("alt","泰国试管婴儿,泰国试管婴儿医院,泰国试管婴儿费用,泰国试管婴儿攻略，杰特宁，全球生殖中心，帕亚泰是拉差，碧娅威国际医院， 泰国第三代试管婴儿，试管无忧");
</script> 
<?= $this->element('tophui'); ?>