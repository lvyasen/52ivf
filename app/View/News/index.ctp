<?= $this->Sgwy->addCss('/css/news/index.css'); ?>
<?php echo $this->Html->script('/js/page.js');?>  
<div class="news">
	<div class="content">
		<p><a href="/">首页</a> > <a href="/news/index/">新闻资讯</a></p>
		<div class="con">
			<?= $this->element('leftnav'); ?>
			<div class="main">
			    <div id="wav_title">
			        <p>新闻资讯</p>
			        <img class="lazy" onclick="history.go(-1)" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
                </div>
			    <h1>新闻资讯</h1>
			    <div class="new-list" id="loadPage">
			    	<ul>
                     	<?php foreach($dataList as $key => $value):?>
			    		<li><a href="/news/info/<?php echo $value["New"]["id"].".html";?>">
			    			<?php $ua = Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT']));?>
			    		    <img src="<?=$ua != 'pc'?$value['New']['mb_image']:$value['New']['image']?>" alt="">
			    		    <div class="right-box">
				    			<h5><?=$value['New']['title']?></h5>
				    			<div class="text"><?=$value['New']['description']?></div>
				    			<p><?=date("Y-m-d",strtotime($value['New']['create_time']))?></p>
				    		</div>	
			    		</a></li> 
                    	<?php endforeach;?> 
			    	</ul>
                    <?php if($dataCount > 5){?>
                    <div class="more">
                        <a href="javascript:void(0)" onclick="_page.getNews(this,5)">加载更多</a>
                    </div>  
                    <?php }?> 
			    </div>
			    <div class="more_list">
			    	<?php echo $strPages;?> 	
			    </div>
			</div>
		</div>
	</div>
</div> 