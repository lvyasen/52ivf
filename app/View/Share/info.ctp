<?php echo $this->Html->css('/css/share/info.css');?>
    <?= $this->Sgwy->addJs('/js/share/info.js'); ?>
<?php echo $this->Html->script('/js/page.js');?>   
    <div class="share_info">      
        <div class="content">
            <div class="wav_title">
        <p>分享详情</p>
        <img class="lazy" onclick="document.referrer === '' ?
          window.location.href = '/' :
          window.history.go(-1);" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
        </div>

            <div class="top">
                <p><a href="/community/index/">首页</a> >  <a href="/share/index/">分享列表</a> > 分享详情</p>
            </div>
            <div class="main">
                <p class="label">热门标签： 
                    <?php  foreach($share_tags as $key => $value):?> 
                        <a href="/share/index/<?php echo "0-".$key.".html";?>" title="<?=$value?>"><?=$value?></a>
                     <?php endforeach;?>  
                    </p>
                <div class="detail">
                    <div class="major">
                        <div class="substence">
                            <!-- <h1><?=$info["Share"]["title"]?></h1> -->
                            <div class="s_head">
                                <div class="head_img"><img class="lazy" src="<?=$info["Share"]["avatar"]?>"></div>
                                <div class="head_name"><?=$info["Share"]["username"]?></div>
                                <div class="date"><?=$info["Share"]["create_time"]?></div>
                                <div class="browse bottam">
                                    <span class="like adagree b_pic3 <?php echo $info['Share']['myagree']>0?'add_up':'';?>" <?php if($info['Share']['myagree']==0){?>onclick="adAgree(this,<?=$info["Share"]["id"]?>,'agree_share')"<?php }?>><?=$info["Share"]["agree_num"]?></span>
                                    <span class="look"><?=$info["Share"]["browse_num"]?></span>
                                </div>
                                <div class="head_box">
                                    <div class="date_box">
                                        第<span class="date_num"></span>天
                                    </div>
                                    <ul>
                                        <li>
                                            <label>人气值：</label>
                                            <span class="ren">
                                                <span></span>
                                            </span>
                                        </li>
                                        <li>
                                            <label>活跃度：</label>
                                            <span class="huo"></span>
                                        </li>
                                        <li>
                                            <label>发帖数：</label>
                                            <span class="fatie"><?php echo $num+"10";?>条</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div id="cont_detail">
                            <p><?=$info["Share"]["content"]?></p> </div>
                            <!-- <?php foreach ($info['Share']['images'] as $k => $v):?> 
                            <img class="lazy"  data-original="<?php echo $v['Picture']['thumb'];?>" src="<?php echo $v['Picture']['thumb'];?>"?>   
                            <?php endforeach;?> -->
                            <div class="state">
                                试管无忧声明：本帖内容为患者就医经验及心得分享，不得用于其他医疗中介商业用途
                            </div>
                        </div>
                    </div>
                    <div class="reply_list" id="loadPage">
                        <?php if(sizeof($comments) >= 1){?><p>全部评论</p><?php }?>
                        <ul>
                            <?php foreach ($comments as $key => $value):?> 
                            <li>
                                <div class="pinglu">
                                    <div class="head_pic">
                                        <img src="<?=$value['Comment']['avatar']?>">
                                    </div>
                                    <div class="conte">
                                        <div class="namec"><?=$value['Comment']['username']?></div>
                                        <div class="contc"><?=$value['Comment']['content']?></div>
                                        <div class="timec"><?php echo Ivf::formatTime($value['Comment']['comment_time']);?></div>
                                        <a class="creplay" href="javascript:void(0);" onclick="replayComment(<?=$value['Comment']['id']?>)">回复</a>
                                        <?php if(!empty($value['Comment']['childCmt'])){?>
                                        <span class="moreHui">更多回复</span> 
                                        <?php }?>
                                    </div> 
                                    <!--子回复开始-->
                                    <div class="conteBox">
                                    <?php if(!empty($value['Comment']['childCmt'])){?>
                                    <?php foreach ($value['Comment']['childCmt'] as $kk => $vv):?> 
                                    <div class="conte">
                                        <div class="namec"><?php echo $vv['replayName'];?></div>
                                        <div class="contc"><?=$vv['content']?></div>
                                        <div class="timec"><?php echo Ivf::formatTime($vv['comment_time']);?></div>
                                        <a class="creplay" href="javascript:void(0);" onclick="replayComment(<?=$vv['id']?>)">回复</a>
                                    </div>
                                    <?php endforeach;?>  
                                    <?php }?>
                                    </div> 
                                    <!--子回复结束-->
                                </div>
                            </li>
                            <?php endforeach;?>    
                        </ul>
                        <div class="reBg">
                        <form  class="replayForm" action="/share/replay_answer"  method="post" >
                            <div class="ph_btn_two">
                                <a>x</a>
                                <div class="text_two">
                                    <textarea  name="content" datatype="*"  nullmsg="请填写评论内容！"  placeholder="我来说一说..."></textarea>
                                    <input type="submit" class="button" value="回复"/>
                                </div>
                            </div> 
                            <input type="hidden" id="repalyVal" name="reply_id" value="0"/>  
                            <input type="hidden" name="cid" value="<?=$info["Share"]["id"]?>"/>  
                            <input type="hidden" name="dosubmit" value="1"/>                 
                        </form></div> 
                        <?php if(sizeof($comments) >= 5){?>
                        <div class="more"> 
                            <a href="javascript:void(0)" title="" onclick="_page.getPageComment(this,<?=$info['Share']['id']?>,'share_comment')">加载更多</a>
                        </div>
                               
                        <?php }?>
                        <form  class="shareForm" action="/share/replay"  method="post" >
                        <div class="text_box">
                            <p>发表评论:</p>
                            <textarea name="content" datatype="*"  nullmsg="请填写评论内容！"></textarea> 
                            <div class="code">    
                                <a class="release" href="javascript:void(0);">发表回复</a>
                                <input type="text" name="u_code" value="" placeholder="请输入验证码" datatype="*4-4" errormsg="请输入4位数验证码！"  nullmsg="请输入验证码！" autocomplete="off">
                                <div class="code_img">
                                    <img src="/personal/captcha" title="点击更换" onclick="this.src='/personal/captcha/?'+Math.random()"/>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="cid" value="<?=$info["Share"]["id"]?>"/>  
                        <input type="hidden" name="dosubmit" value="1"/>                 
                        </form>
                        <a href="/mobile/pub/2-<?=$info["Share"]["id"]?>.html" class="btn_one" rel="nofollow">我要评论</a> 
                        
                    </div>
                </div>
            </div>
            <div class="my_share"> 
                <?php if(isset($userInfo) && !empty($userInfo)){?>
                <div class="fixed_login">
                    <img src="<?php echo $userInfo['avatar'];?>">
                    <a href="/personal/index/" title=""><?php echo empty($userInfo['nick_name'])?'未设置昵称':$userInfo['nick_name'];?></a> 
                </div>
                <?php }else{?> 
                <div class="fixed_login">
                    <img src="/images/temp/touxiang_001.png">
                    <a href="javascript:void(0)" title="" class="login">请先登录</a>
                    <p>以便分享与评论</p>
                </div>
                <?php }?>
                <div class="fixed_ranking">
                    <span>精彩分享排行</span>
                    <a class="btn_week"style="margin-left:32px;">周排行</a>
                    <a class="btn_total">总排行</a>
                    <div class="rank_week">
                        <ul>
                            <?php if(!empty($weekList)){?>
                            <?php foreach ($weekList as $key => $value):?> 
                            <li><a href="/share/info/<?php echo $value["Share"]["id"].".html";?>" >
                                <img class="lazy" src="<?=$value["Share"]["avatar"]?>" data-original="<?=$value["Share"]["avatar"]?>">
                                <div>
                                    <p><?=$value["Share"]["title"]?></p>
                                    <em>回答数:<i><?=$value["Share"]["comment_num"]?></i> </em>
                                </div>
                            </a></li>
                            <?php endforeach;?>  
                            <?php }else{?>
                            <?php foreach ($topList as $key => $value):?> 
                            <li><a href="/share/info/<?php echo $value["Share"]["id"].".html";?>" >
                                <img class="lazy" src="<?=$value["Share"]["avatar"]?>" data-original="<?=$value["Share"]["avatar"]?>">
                                <div>
                                    <p><?=$value["Share"]["title"]?></p>
                                    <em>回答数:<i><?=$value["Share"]["comment_num"]?></i> </em>
                                </div>
                            </a></li>
                            <?php endforeach;?>   
                            <?php }?>
                        </ul>
                    </div>
                    <div class="rank_total">
                        <ul>
                            <?php foreach ($topList as $key => $value):?> 
                            <li><a href="/share/info/<?php echo $value["Share"]["id"].".html";?>" >
                                <img class="lazy" src="<?=$value["Share"]["avatar"]?>" data-original="<?=$value["Share"]["avatar"]?>">
                                <div>
                                    <p><?=$value["Share"]["title"]?></p>
                                    <em>回答数:<i><?=$value["Share"]["comment_num"]?></i> </em>
                                </div>
                            </a></li>
                            <?php endforeach;?>  
                            
                        </ul>
                    </div>
                </div>  
            </div>
        </div> 
    </div>
    <script type="text/javascript"> 
    $(".shareForm").Validform({
        btnSubmit:".release", 
        tiptype:function(msg){
            layer.alert(msg,{icon:0});    
        },
        tipSweep:true,
        ajaxPost:true,
        callback:function(o){
            layer.closeAll();
            if (o.status == 4000) { 
                if(o.return_url=="/mobile/login/")
                { 
                    layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
                    setTimeout("window.location.href='/mobile/login/'", 2000);
                }
                else
                   $('.login').trigger("click");    
            }
            else if (o.status == 200) {
                layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});       
                setTimeout(function () {
                    window.location.reload();
                }, 1000);   
            } else {
                layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
            } 
        }
    });
    $('.btn_week').css('color','#ff9a14');
    $('.btn_week').click(function(event) {
        $(this).css('color','#ff9a14');
        $('.btn_total').css('color','#666666')
        $('.rank_total').css({'display':'none'});
        $('.rank_week').css({'display':'block'});
    });
    $('.btn_total').click(function(event) {
        $(this).css('color','#ff9a14');
        $('.btn_week').css('color','#666666')
        $('.rank_week').css({'display':'none'});
        $('.rank_total').css({'display':'block'});
    }); 
    var num_fa = Math.floor(Math.random()*9+1);
    console.log(num_fa);
    $(".date_num").text(num_fa);
    $("#cont_detail img").removeAttr("alt");
    $("#cont_detail img").attr("alt","泰国试管婴儿,泰国试管婴儿医院,泰国试管婴儿费用,泰国试管婴儿攻略,杰特宁,全球生殖中心,帕亚泰是拉差,碧娅威国际医院, 泰国第三代试管婴儿,试管无忧");

    // var cont_de = document.getElementById("cont_detail");
    
    </script>
<?= $this->element('tophui'); ?>