<div class="nav">
    <ul>
    	<li><a href="/about/info/" <?php echo isset($nav) && $nav=='about'?'class="click_add"':'';?> rel="nofollow">品牌故事</a></li>
    	<li><a href="/site/index/" <?php echo isset($nav) && $nav=='site'?'class="click_add"':'';?> rel="nofollow" target="_blank">最新活动</a></li>
        <li><a href="/news/index/" <?php echo isset($nav) && $nav=='news'?'class="click_add"':'';?>>新闻资讯</a></li>
        <li><a href="/headlines/index/" <?php echo isset($nav) && $nav=='headlines'?'class="click_add"':'';?>>常见问题</a></li>
        <li><a href="/join/index/" <?php echo isset($nav) && $nav=='join'?'class="click_add"':'';?>>加入我们</a></li>
        <li><a href="/contact/index/" <?php echo isset($nav) && $nav=='contact'?'class="click_add"':'';?> rel="nofollow">联系我们</a></li>
        <li><a href="/sitemap/index/" <?php echo isset($nav) && $nav=='sitemap'?'class="click_add"':'';?>>网站地图</a></li>
    </ul>
</div>