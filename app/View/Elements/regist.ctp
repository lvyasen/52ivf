   
    <?php echo $this->Html->script('/js/mobile/user.js');?>   
    <style>
       /*layer 弹窗样式*/
.layui-layer-dialog{border-radius:5px !important;overflow:hidden!important;width:380px !important;height:150px;}
.layui-layer-btn .layui-layer-btn0{background-color:#f99c1b !important;border-color:#f99c1b !important;}
.layui-layer-dialog .layui-layer-padding{padding-left:65px!important;}
.layui-layer-btn a{margin: 0px 6px 0!important;}

    </style>
    <div class="mu">
        <div class="mu_blank"></div>
        <div>
             <div class="pop">            
                <div class="pop_title">
                    <i class="close"></i>
                    <h2>注册试管无忧 <span>已有帐号?<a class="login">马上登录></a></span></h2>
                </div>
                <div class="pop_form"> 
                        <form  class="pregForm" action="/mobile/register"  method="post" >
                        <div class="inp_1">                        
                            <input class="phone" type="text" placeholder="请输入手机号(限11位)" id="u_mobile_mb" name="u_mobile"  datatype="m" errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！" /> 
                        </div>
                        <div style="height:33px;">
                            <input class="code" type="text"  name="u_code" value="" placeholder="输入6位验证码" datatype="*6-6" errormsg="请输入6位验证码！"  nullmsg="请输入6位验证码！"/> 
                            <input class="get_code" type="button"  id="send_mc_btn_mb" onclick="_user.sendPcRcode('send_code')" value="免费获取验证码"/>
                        </div>
                        <div class="inp_2">                      
                            <input  class="password" type="password"  name="u_pwd"  placeholder="输入密码(8~16个数字与组合)"  datatype="*8-16" errormsg="密码至少8个字符,最多16个字符！"  nullmsg="请输入密码！" /> 
                        </div>
                        <div  class="inp_2">
                            <input  class="confirm" type="password"  name="u_comfirm_pwd"  type="text" placeholder="确认密码"  datatype="*" recheck="u_pwd" errormsg="两次输入的密码不一致！"  nullmsg="请再次输入密码！" /> 
                        </div>
                    <div class="agree">
                        <input class="yes" type="radio" value="1" checked />
                        <i>同意</i>
                        <a href="#">《用户注册协议》</a>
                        <label for=""></label> 
                        <input type="hidden" name="dosubmit" value="1"/>    
                        <input type="submit" class="submit" value="立即注册" style="width:300px;height:35px ;border:none;background:#ff818a;margin-top:24px;color:#fff;font-size: 16px;border-radius:5px;outline:none;cursor: pointer;" />
                    </div> 
                    </form>               
                </div>        
            </div>
            <div class="pop_login">
                <div class="pop_title">
                    <i class="close"></i>
                    <h2>登录试管无忧 <span>没有帐号?<a class="register">5秒注册></a></span></h2>
                </div>
                <div class="pop_form">
                    <form  class="ploginForm" action="/personal/login"  method="post" >
                        <div class="inp_01">
                            <input class="phone_1" type="text" placeholder="请输入手机号(限11位)" name="u_mobile"  datatype="m" errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！"/> 
                        </div>                   
                        <div class="inp_02">
                            <input  class="password_1" type="password"  name="u_pwd"  placeholder="请输入密码"  datatype="*8-16" errormsg="密码至少8个字符,最多16个字符！"  nullmsg="请输入密码！"/> 
                        </div> 
                        <div style="height:33px;">
                            <input class="login_code_text" type="text" name="u_code" value=""placeholder="请输入验证码" datatype="*4-4" errormsg="请输入4位数验证码！"  nullmsg="请输入验证码！">  
                            <div class="login_code">
                                <img src="/personal/captcha" title="点击更换" onclick="this.src='/personal/captcha/?'+Math.random()"/>
                            </div> 
                        </div> 
                    <div class="sub">   
                        <input type="submit" class="submit" value="登录" />           
                        <a  class="make" >忘记密码?</a>
                    </div>   
                    <input type="hidden" name="dosubmit" value="1"/>                 
                    </form>               
                </div>     
            </div>
            <div class="pop_make">
                <div class="pop_title">
                    <i class="close"></i>
                    <h3>密码找回</h3>
                </div>
                <div class="pop_form">
                        <form  class="findForm" action="/mobile/retrieval"  method="post" > 
                        <div class="inp_1">                        
                            <input class="phone" type="text" placeholder="请输入手机号(限11位)" id="u_mobile" name="u_mobile"  datatype="m" errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！"/> 
                        </div>
                        <div style="height:33px;">
                            <input class="code" type="text" placeholder="请输入短信验证码" name="u_code" value="" placeholder="输入6位验证码" datatype="*6-6" errormsg="请输入6位验证码！"  nullmsg="请输入6位验证码"/>   
                            <input class="get_code" type="button"  id="send_mc_btn" onclick="_user.sendRcode('findpwd_send_code')" value="获取验证码"/>    
                        </div>
                        <div class="inp_2">                      
                            <input  class="password" type="password" placeholder="请输入密码"  name="u_pwd" datatype="*8-16" errormsg="密码至少8个字符,最多16个字符！"  nullmsg="请输入新密码！"/>
                            <label for="">请输入密码！</label>
                        </div>
                        <div class="inp_2">
                            <input  class="confirm" type="password" placeholder="请确认密码" name="u_comfirm_pwd"  type="text"  datatype="*" recheck="u_pwd" errormsg="两次输入的密码不一致！"  nullmsg="请确认新密码！"/>
                            <label for="">两次密码不一致！</label>
                        </div>
                        <input type="hidden" name="dosubmit" value="1"/>  
                        <input type="submit" class="true" value="确定" />   
                    </form> 
                </div>     
            </div>
        </div>
        
    </div>
<script type="text/javascript"> 
$(document).ready(function(){
    $(".ploginForm").Validform({ 
        tiptype:function(msg){
            layer.alert(msg,{icon:0});   
        },
        tipSweep:true,
        ajaxPost:true,
        callback:function(o){
            if (o.status == 200) {
                layer.msg(o.message,{
                    icon:6,
                    skin:'layer_mobile_login'
                });          
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            } else {
                layer.msg(o.message,{
                    icon:5,
                    skin:'layer_mobile_login'
                });    
            } 
        }
    });
    $(".findForm").Validform({
        tiptype:function(msg){
            layer.alert(msg,{icon:0});   
        },
        tipSweep:true,
        ajaxPost:true,
        callback:function(o){
            if (o.status == 200) {
                layer.msg(o.message,{
                    icon:6,
                    skin:'layer_mobile_login'
                });    
                window.location.reload();
            } else {
                layer.alert(o.message,{icon:0});    
            } 
        }
    });
    $(".pregForm").Validform({
        tiptype:function(msg){
            layer.alert(msg,{icon:0});   
        },
        tipSweep:true,
        ajaxPost:true,
        callback:function(o){
            if (o.status == 200) {
                layer.msg(o.message,{
                    icon:6,
                    skin:'layer_mobile_login'
                });    
                window.location.reload();
            } else {
                layer.alert(o.message,{icon:0});    
            } 
        }
    });
})
</script>    