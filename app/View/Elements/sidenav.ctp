<div class="sidenav">
    <div class="side_menu">
        <a href="/"><span class="glyphicons glyphicons-home"></span>首页</a>
        <a href="/service/remote/"><span class="glyphicons glyphicons-display"></span>远程咨询</a>
        <a href="/service/booking/"><span class="glyphicons glyphicons-user-add"></span>精准预约</a>
        <a class="zuo_click" ><span class="glyphicons glyphicons-note"></span></span>服务套餐</a>
        <div id="zuo_btn">
            <a href="/service/vip/"><span class="glyphicons glyphicons-file"></span>VIP尊享</a>
            <a href="/service/gold/"><span class="glyphicons glyphicons-file"></span>金牌无忧</a>
        </div>
        <a href="/hospital/index/"><span class="glyphicons glyphicons-hospital"></span>热门医院</a>
        <a href="/community/index/"><span class="glyphicons glyphicons-group"></span>试管邦社区</a>
        <a class="nav_click" ><span class="glyphicons glyphicons-menu-hamburger"></span>更多...</a>
        <div id="nav_btn">
            <a href="/about/info"><span class="glyphicons glyphicons-file"></span>品牌故事</a>
            <a href="/site/index/"><span class="glyphicons glyphicons-file"></span>最新活动</a>
            <a href="/news/index/"><span class="glyphicons glyphicons-file"></span>新闻资讯</a>
            <a href="/headlines/index/"><span class="glyphicons glyphicons-file"></span>常见问题</a>
            <a href="/contact/index/"><span class="glyphicons glyphicons-file"></span>联系我们</a>
            <a href="/sitemap/index/"><span class="glyphicons glyphicons-file"></span>网站地图</a>
            <a href="/join/index/"><span class="glyphicons glyphicons-file"></span>加入我们</a>
        </div>
    </div>
</div>
