<?= $this->Sgwy->addCss('/css/aide/index.css'); ?>
<?= $this->Sgwy->addJs("/js/aide/index.js"); ?>
<?= $this->Sgwy->addJs("/js/mobile/user.js"); ?>
<div class="box_bg">
	<div class="box">
		<div class="close"></div>
		<div class="title">
			试管助手<p>已有<span><?php echo $randNum;?></span>人测试</p>
		</div>
		<div class="content">
		    <div class="jie">根据泰国生殖医生DR.THITIKORN WANICHKUL M.D等人提出的前三次试管婴儿成功率（活产率）预测模型，为准备试管的姐妹打造了这个新功能，只要轻松填写你的信息，就能分分钟帮你分析成功率！试管不用愁！</div>
			<div class="con_01">
				<div class="begin">
					想要了解你的试管成功率吗？<br>
					赶紧来测试一下吧！<br>
					<div class="button btn_01">开始测试</div>
				</div>
			</div>
			<div class="bar">
				<div class="bar_length"></div>
                <span><i class="bar_sco">0</i>%</span>	
			</div>
			<?= $this->element('aide'); ?>

			<div class="conts con_03  btn_02">
				<div class="pink_box">
					<p>试管助手成功率的算法基于真实用户的大数据信息，为防止虚假数据干扰准确性，请输入手机号以确保用户信息的真实性：</p>
            		<form  class="aideForm" action="/aide/mobile_aide"  method="post" >
					<input class="phone" type="text" placeholder="请输入您的手机号" id="aide_mobile" name="u_mobile"  datatype="m" errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！"><br>
					<input class="code" type="text" placeholder="填写验证码" name="u_code" value="" placeholder="输入6位验证码" datatype="*6-6" errormsg="请输入6位验证码！"  nullmsg="请输入6位验证码！"> 
            		<input class="pull_code  send_sms_btn" type="button"  onclick="_user.sendAidecode('send_aide_code')" value="获取验证码"/> 
					<div class="button btn_02 aidesubmit">马上预测</div>
	            	<input type="hidden" name="dosubmit" value="1"/>  
	        	</form>
				</div>               
			</div>
			<div class="con_04 cont_01">
				<div class="white_box">
				    <div>测试结果 <br>您的试管成功率为<span ><i class="change">1</i>%</span></div>
				</div>
				<div class="newsubmit">重新测试</div>
			</div>
		</div>
	</div>
</div>