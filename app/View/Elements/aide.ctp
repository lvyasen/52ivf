	
			<div class="conts con_02 cont_00">
				<div class="pink_box" id="aide">
					<p>准妈妈年龄（岁）</p>
					<div class="">
						<input type="radio" id="01_A" name="1" value="A" >
						<label for="01_A">A.20-30</label>
					</div>
					<div>
						<input type="radio" id="01_B" name="1" value="B">
						<label for="01_B">B.30-35</label>
					</div>
					<div>
						<input type="radio" id="01_C" name="1" value="C">
						<label for="01_C">C.35-40</label>
					</div>
					<div>
						<input type="radio" id="01_D" name="1" value="D">
						<label for="01_D">D.40以上</label>
					</div>
				</div>
			</div>
			<div class="conts con_02">
				<div class="pink_box" id="aide">
					<p>备孕难题</p>
					<div class="">
						<input type="radio" id="02_A" name="2" value="A">
						<label for="02_A">A.男方情况</label>
					</div>
					<div>
						<input type="radio" id="02_B" name="2" value="B">
						<label for="02_B">B.子宫情况</label>
					</div>
					<div>
						<input type="radio" id="02_C" name="2" value="C">
						<label for="02_C">C.卵巢情况</label>
					</div>
					<div>
						<input type="radio" id="02_D"name="2" value="D">
						<label for="02_D">D.其他</label>
					</div>
				</div>
			</div>
			<div class="conts con_02">
				<div class="pink_box" id="aide">
					<p>试管次数</p>
					<div class="">
						<input type="radio" id="03_A" name="3" value="A">
						<label for="03_A">A.0</label>
					</div>
					<div>
						<input type="radio" id="03_B" name="3" value="B">
						<label for="03_B">B.1</label>
					</div>
					<div>
						<input type="radio" id="03_C" name="3" value="C">
						<label for="03_C">C.2</label>
					</div>
					<div>
						<input type="radio" id="03_D" name="3" value="D">
						<label for="03_D">D.3次及以上</label>
					</div>
				</div>
			</div>
			<div class="conts con_02">
				<div class="pink_box" id="aide">
					<p>男方情况</p>
					<div class="">
						<input type="radio" id="04_A" name="4" value="A">
						<label for="04_A">A.正常</label>
					</div>
					<div>
						<input type="radio" id="04_B" name="4" value="B">
						<label for="04_B">B.少精/弱精</label>
					</div>
					<div>
						<input type="radio" id="04_C" name="4" value="C">
						<label for="04_C">C.无精</label>
					</div>
					<div>
						<input type="radio" id="04_D" name="4" value="D">
						<label for="04_D">D.其他</label>
					</div>
				</div>
			</div>
			<div class="conts con_02">
				<div class="pink_box" id="aide">
					<p>子宫情况</p>
					<div class="">
						<input type="radio" id="05_A" name="5" value="A">
						<label for="05_A">A.正常</label>
					</div>
					<div>
						<input type="radio" id="05_B" name="5" value="B">
						<label for="05_B">B.子宫内膜异位</label>
					</div>
					<div>
						<input type="radio" id="05_C" name="5" value="C">
						<label for="05_C">C.子宫肌瘤</label>
					</div>
					<div>
						<input type="radio" id="05_D" name="5" value="D">
						<label for="05_D">D.其他</label>
					</div>
				</div>
			</div>
			<div class="conts con_02">
				<div class="pink_box" id="aide">
					<p>准妈妈AMH（ng/ml）</p>
					<div class="">
						<input type="radio" id="06_A" name="6" value="A">
						<label for="06_A">A.0-2</label>
					</div>
					<div>
						<input type="radio" id="06_B" name="6" value="B">
						<label for="06_B">B.2-6.8</label>
					</div>
					<div>
						<input type="radio" id="06_C" name="6" value="C">
						<label for="06_C">C.6.8以上</label>
					</div>
					<div>
						<input type="radio" id="06_D" name="6" value="D">
						<label for="06_D">D.不清楚</label>
					</div>
				</div>
			</div>
			<div class="conts con_02">
				<div class="pink_box" id="aide">
					<p>准妈妈卵泡个数</p>
					<div class="">
						<input type="radio" id="07_A" name="7" value="A">
						<label for="07_A">A.10个以上</label>
					</div>
					<div>
						<input type="radio" id="07_B" name="7" value="B">
						<label for="07_B">B.5-10个</label>
					</div>
					<div>
						<input type="radio" id="07_C" name="7" value="C">
						<label for="07_C">C.0-5个</label>
					</div>
					<div>
						<input type="radio" id="07_D" name="7" value="D">
						<label for="07_D">D.不清楚</label>
					</div>
				</div>
			</div>
			<div class="conts con_02">
				<div class="pink_box" id="aide">
					<p>准妈妈FSH(mIU/ml）</p>
					<div class="">
						<input type="radio" id="08_A" name="8" value="A">
						<label for="08_A">A.10-15</label>
					</div>
					<div>
						<input type="radio" id="08_B" name="8" value="B">
						<label for="08_B">B.6-10</label>
					</div>
					<div>
						<input type="radio" id="08_C" name="8" value="C">
						<label for="08_C">C.4-6</label>
					</div>
					<div>
						<input type="radio" id="08_D" name="8" value="D">
						<label for="08_D">D.不清楚</label>
					</div>
				</div>
			</div>
			<div class="conts con_02">
				<div class="pink_box" id="aide">
					<p>准爸爸精子活力（A+B%)</p>
					<div class="">
						<input type="radio" id="09_A" name="9" value="A">
						<label for="09_A">A.40以上</label>
					</div>
					<div>
						<input type="radio" id="09_B" name="9" value="B">
						<label for="09_B">B.20-40</label>
					</div>
					<div>
						<input type="radio" id="09_C" name="9" value="C">
						<label for="09_C">C.0-20</label>
					</div>
					<div>
						<input type="radio" id="09_D" name="9" value="D">
						<label for="09_D">D.不清楚</label>
					</div>
				</div>
			</div>
			<div class="conts con_02">
				<div class="pink_box" id="aide">
					<p>宝宝性别要求</p>
					<div class="">
						<input type="radio" id="10_A" name="10" value="A">
						<label for="10_A">A.随缘</label>
					</div>
					<div>
						<input type="radio" id="10_B" name="10" value="B">
						<label for="10_B">B.女宝</label>
					</div>
					<div>
						<input type="radio" id="10_C" name="10" value="C">
						<label for="10_C">C.男宝</label>
					</div>
					<div>
						<input type="radio" id="10_D" name="10" value="D">
						<label for="10_D">D.龙凤</label>
					</div>
				</div>
			</div>
		