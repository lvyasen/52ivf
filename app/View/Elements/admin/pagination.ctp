</br>
<ul class="pagination"> 
    <?php
        echo $this->Paginator-> prev(__('上一页'), array('tag' =>  'li'), null, array('tag' =>  'li','class' =>  'disabled','disabledTag' =>  'a'));
        echo $this->Paginator-> numbers(array('separator' =>  '','currentTag' =>  'a', 'currentClass' =>  'active','tag' =>  'li','first' =>  1));
        echo $this->Paginator-> next(__('下一页'), array('tag' =>  'li','currentClass' =>  'disabled'), null, array('tag' =>  'li','class' =>  'disabled','disabledTag' =>  'a')); 
 		
 		$attrPage=$this->Paginator->params();
 		$pageCount=$attrPage['pageCount']; 
 		$current=$attrPage['page']; 
 		echo "&nbsp;&nbsp;跳转至 <select id='toPage'>";
 		for($i=1;$i<=$pageCount;$i++){  
 			$st=$current==$i?" selected":"";
 			echo " <option value =".$i.$st." >".$this->Paginator->link('第'.$i.'页',
        array('page' => $i))."</option>"; 
 		}
 		echo "</select>"; 
 		for($i=1;$i<=$pageCount;$i++){   
        	echo "<div style='display:none' id='ycPage".$i."'>".$this->Paginator->link('第'.$i.'页',
        array('page' => $i))."</div>";
 		}
    ?> 
</ul> 
<script type="text/javascript">
	$("select#toPage").change(function(){ 
		var goto=$("#ycPage"+$(this).val()+" a").attr('href'); 
	    window.location.href=goto;
	});
</script>
