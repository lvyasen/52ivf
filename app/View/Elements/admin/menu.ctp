<dl class="nav nav-list" id="menu_list">  
  <dd class="submenu" style="height:0px; border:0px;"></dd> 
  <?php foreach ($menu as $key => $val): ?> 
       <?php if($userRoles=='all' || in_array($key, $user_permissions)): ?>
        <dt class="nav_link ">
        <a href="javascript:void(0)" class="dropdown-toggle title_nav"><span class="menu-text"> <?= $menuTitle[$key] ?> </span><b class="arrow fa fa-angle-down"></b></a>
      </dt> 
      <dd class="submenu">
        <ul> 
          <?php foreach ($val as $k => $v): ?> 
              <?php if($userRoles=='all' || in_array($k, $user_permissions)): ?><?php if($v['showItem']): ?><li class="home"><a href="javascript:void(0)" name="<?= $v['url'] ?>" title="<?= $v['label'] ?>" class="iframeurl"><!-- <i class="fa fa-angle-double-right"></i> --><?= $v['label'] ?></a></li><?php endif; ?><?php endif; ?>
          <?php endforeach ?> 
        </ul>
      </dd> <?php endif; ?>
  <?php endforeach ?>