<?php echo $this->Html->css('/css/community/index.css?wy=20180208');?>
<?= $this->Sgwy->addJs('/plugin/ctimg/js/jqthumb.js'); ?>
<?= $this->Sgwy->addJs('/plugin/ctimg/js/main.js'); ?>

<div class="community">
        <div class="banner_pc">
           <a class="AideClick wpc" href="javascript:void(0);"><img src="/images/community/chenggonglv.png" alt="泰国试管婴儿攻略"></a>
        </div>
        <div class="banner_ph">
            <img class="lazy" src="/images/community/shequ_banner.jpg" alt="泰国试管婴儿攻略">        
        </div>      
        <div class="content">
            <div class="menu">
                <ul>
                    <li><a href="/plan/index/">
                        <img src="/images/community/z_1.png" alt="泰国试管婴儿攻略">
                        <p>试管攻略</p>
                    </a></li>
                    <li><a href="/share/index/">
                        <img src="/images/community/z_2.png" alt="泰国试管婴儿攻略">
                        <p>就医分享</p>
                    </a></li>
                    <li><a href="/answer/index/">
                        <img src="/images/community/z_3.png" alt="泰国试管婴儿攻略">
                        <p>患者问答</p>
                    </a></li>
                    <li><a href="/audio/index/">
                        <img src="/images/community/z_4.png" alt="泰国试管婴儿攻略">
                        <p>胖妈妈之声</p>
                    </a></li>
                    <li><a href="/video/index/">
                        <img src="/images/community/z_5.png" alt="泰国试管婴儿攻略">
                        <p>自媒体</p>
                    </a></li>
                </ul>
            </div>
            <div class="raiders">
                <div class="title">
                    <p>攻略&问答</p>
                    <span>Introduction Q & A</span>
                </div>
                <div class="raiders_box">
                    <div class="r_b_left">
                        <p>泰国试管攻略</p><a href="/plan/info/93.html">
                        <img class="lazy" src="/images/common/grey.gif" data-original="/images/community/photo.jpg" alt="泰国试管婴儿攻略"> 
                        <div class="le_0">
                            <p>一、二、三代试管婴儿究竟有何不同？</p>
                            很多姐妹在聊试管婴儿技术的问题，特别是准备要进行试管的准妈妈们。她们当中的许都人都希望进行第三代试管婴儿，都觉得肯定第三代技术比第二代技术好，更比第一代技术强！真的是这样吗？
                        </div></a>
                        
                    </div>
                    <div class="service">
                        <div class="leftdiv">无忧服务</div>
                        <div class="rightdiv">
                            <a href="/service/booking/index/"><div class="afiv1">精准预约<img src="/images/community/bn1.png" alt="泰国试管婴儿攻略"></div></a>
                            <a href="/service/vip/index/"><div class="afiv2">VIP尊享<img src="/images/community/bn2.png" alt="泰国试管婴儿攻略"></div></a>
                            <a href="/service/gold/index/"><div class="afiv3">金牌无忧<img src="/images/community/bn3.png" alt="泰国试管婴儿攻略"></div></a>
                            <a href="/service/remote/index/"><div class="afiv4">远程问诊<img src="/images/community/bn4.png" alt="泰国试管婴儿攻略"></div></a>
                        </div>
                    </div>
                    <div class="r_b_center">
                        <p>泰国试管攻略<a href="/plan/index/">更多></a></p>
                        <div class="tag_ph">
                                <a href="/plan/index/" class="mt">全部</a>
                                <a href="/plan/index/7.html">交通与吃住</a>
                                <a href="/plan/index/3.html">费用与流程</a>
                                <a href="/plan/index/8.html">高龄与疑难试管</a>
                                <a href="/plan/index/1.html">医院与医生</a>
                                <a href="/plan/index/2.html">技术与成功率</a>
                                <a href="/plan/index/6.html">出国前准备</a>
                                <a href="/plan/index/11.html">其他</a>
                        </div>
                        <ul class="">
                            <?php foreach ($guideList as $key => $value):?>
                            <li><a href="/plan/info/<?php echo $value["Article"]["id"].".html";?>">
                                <img class="toux" src="<?=$value['Article']['avatar']?>" alt="<?=$value['Article']['username']?>"> 
                                <div>
                                    <span><?=$value['Article']['username']?></span>
                                    <p><?=$value['Article']['title']?></p>
                                    <em><?=$value['Article']['username']?></em>
                                </div>
                            </a></li> 
                            <?php endforeach;?>
                        </ul>
                    </div>
                    <div class="r_b_right">
                        <p>试管问答</p>
                        <div class="an_box"><a href="/answer/info/<?php echo $rand_li['Share']['id']?>.html ">
                        <p><?php echo $rand_li['Share']['title']?></p></a><a class="ans" href="/answer/info/<?php echo $rand_li['Share']['id']?>.html ">帮答</a>
                        </div>
                        <div class="sele_ques">
                            <p>精选问题</p>
                            <ul>
                                <?php foreach ($answerList as $key => $value):?>
                                <li><a href="/answer/info/<?php echo $value["Share"]["id"].".html";?>"><?=$value['Share']['content']?></a></li>
                                <?php endforeach;?>
                            </ul>
                        </div>
                        <form  class="shareForm" action="/community/ask"  method="post" >
                        <div class="ipt_ques">
                            <input type="text" placeholder="请输入你的问题" id="ask_content" name="content" datatype="*"  nullmsg="请输入你的问题！">
                            <a class="ques_btn">提问</a>      
                            <a class="ans_btn">搜答案</a>
                        </div>      
                        <input type="hidden" name="dosubmit" value="1"/>       
                        </form>   
                    </div>
                </div>
            </div>
            <div class="media">
                <div class="title">
                    <p>无忧自媒体</p>
                    <span>Ivf52 Media</span>
                </div>
                <div class="media_box">
                    <div class="media_right">
                        <div class="me_title">
                            <span>胖妈妈之声</span>
                            <a href="/audio/index/">更多></a>
                        </div>
                        <div class="right_list">
                            <ul>
                                <li><a href="/audio/info/16.html" >
                                    <div class="r_video">
                                        <img src="/uploads/albums/image/20170918/20170918094701_28966.jpg" alt="泰国试管婴儿攻略">
                                    </div>
                                    <div class="r_wen">
                                        <span>2016年10月专辑</span>
                                        <p>胖妈妈之声是有Nancy主播，分享试管姐妹们的心情故事</p>
                                    </div>
                                </a></li>
                                <li><a href="/audio/info/17.html" >
                                    <div class="r_video">
                                        <img src="/uploads/albums/image/20170918/20170918094706_97813.jpg" alt="泰国试管婴儿攻略">
                                    </div>
                                    <div class="r_wen">
                                        <span>2016年11月专辑</span>
                                        <p>胖妈妈之声是有Nancy主播，分享试管姐妹们的心情故事</p>
                                    </div>
                                </a></li>
                                <li><a href="/audio/info/18.html" >
                                    <div class="r_video">
                                        <img src="/uploads/albums/image/20170918/20170918094713_30794.jpg" alt="泰国试管婴儿攻略">
                                    </div>
                                    <div class="r_wen">
                                        <span>2016年12月专辑</span>
                                        <p>胖妈妈之声是有Nancy主播，分享试管姐妹们的心情故事</p>
                                    </div>
                                </a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="media_left">
                        <div class="me_title">
                            <span>视频自媒体</span>
                            <a href="/video/index/">更多></a>
                        </div>
                        <div class="left_list">
                            <div class="list_left"><a href="/video">
                                <div>
                                    <img src="/images/community/taiguoshiguan.jpg" alt="泰国试管婴儿攻略">
                                    <img class="play_b" src="/images/community/zx1.png" alt="泰国试管婴儿攻略">
                                </div>
                                <p>为什么要创办《泰国试管那些事儿》？</p>
                                <span>泰国试管那些事儿 第1期</span></a>
                            </div>
                            <div class="list_right">
                                <ul>
                                    <li><a href="/video/index/">
                                        <div>
                                            <img src="/images/video/m/m1.jpg" alt="泰国试管婴儿攻略">
                                            <img class="play_b" src="/images/community/zx3.png" alt="泰国试管婴儿攻略">
                                        </div>
                                        <p>男性不育的研究问题与现状</p>
                                        <span>小忧说试管</span></a>
                                    </li>
                                    <li><a href="/video/index/">
                                        <div>
                                            <img src="/images/video/m/m2.jpg" alt="泰国试管婴儿攻略">
                                            <img class="play_b" src="/images/community/zx3.png" alt="泰国试管婴儿攻略">
                                        </div>
                                        <p>男性不育的注意事项</p>
                                        <span>小忧说不孕</span></a>
                                    </li>
                                    <li><a href="/video/index/">
                                        <div>
                                            <img src="/images/video/m/m5.jpg" alt="泰国试管婴儿攻略">
                                            <img class="play_b" src="/images/community/zx3.png" alt="泰国试管婴儿攻略">
                                        </div>
                                        <p>苏查妲博士专访</p>
                                        <span>名医专访</span></a>
                                    </li>
                                    <li><a href="/video/index/">
                                        <div>
                                            <img src="/images/video/m/m9.jpg" alt="泰国试管婴儿攻略">
                                            <img class="play_b" src="/images/community/zx3.png" alt="泰国试管婴儿攻略">
                                        </div>
                                        <p>高龄二胎妈妈，扑泰喜得男宝</p>
                                        <span>客户案例</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="share">
                <div class="title">
                    <p>泰国就医分享</p>
                    <span>Thailand Medical treatment sharing </span>
                </div>
                <div class="share_box">
                    <div class="sh_box_left">
                        <div class="tab_btn">
                            <ul>
                                <li class="addBtn">全部</li>
                                <li>初次检查</li>
                                <li>促排阶段</li>
                                <li>取卵移植</li>
                                <li>保胎</li>
                                <li>就医经验</li>
                                <li>心情</li>
                            </ul>
                        </div>
                        <div class="share_ph">
                            <span>社区分享</span>
                            <div class="tag_ph">
                                <a href="/share/index/" class="mt">全部</a>
                                <a href="/share/index/0-10.html">初次检查</a>
                                <a href="/share/index/0-20.html">促排阶段</a>
                                <a href="/share/index/0-30.html">取卵移植</a>
                                <a href="/share/index/0-40.html">保胎</a>
                                <a href="/share/index/0-60.html">心情</a>
                                <a href="/share/index/0-50.html">就医经验</a>
                                <a href="/share/index/0-70.html">其他</a>
                            </div>  
                        </div>
                        <div class="tab_boxs">
                            <div class="tab_box">
                                <div class="patient">
                                    <div class="new_list_b">
                                        <ul>
                                         <?php foreach ($shareList['all'] as $key => $value):?>
                                            <li>
                                                <div class="list_li">
                                                    <div class="touxiang"><img src="<?=$value['Share']['avatar']?>" alt="<?=$value['Share']['username']?>"></div>
                                                    <div class="list_b_right">
                                                        <a href="/share/info/<?php echo $value["Share"]["id"].".html";?>">
                                                        <div class="title_top">
                                                        <?php 
                                                        echo Ivf::trimall($value['Share']['description']); 
                                                        ?></div> </a>
                                                        <div class="bottam">
                                                            <div class="b_name"><?=$value['Share']['username']?></div>
                                                            <div class="b_right">
                                                                <div class="b_pic1"><?=$value['Share']['browse_num']?></div>
                                                                <div class="b_pic2"><?=$value['Share']['comment_num']?></div>
                                                                <div class="b_pic3 up <?php echo $value['Share']['myagree']>0?'add_up':'';?>" <?php if($value['Share']['myagree']==0){?>onclick="adAgree(this,<?=$value["Share"]["id"]?>,'agree_share')"<?php }?>><?=$value['Share']['agree_num']?></div>
                                                                  <div class="b_right_mm">
                                                <div><img src="/images/hospital/y_1.png" />&nbsp;&nbsp;<?=$value['Share']['browse_num']?></div>
                                                <div><img src="/images/hospital/y_2.png" />&nbsp;&nbsp;<?=$value['Share']['comment_num']?></div>
                                                <div><img src="/images/hospital/y_3.png" />&nbsp;&nbsp;<?=$value['Share']['agree_num']?></div>
                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="img_b">
                                                    <!-- <?php foreach ($value['Share']['images'] as $k => $v):?> 
                                                    <img class="demo" src="<?php echo $v['Picture']['thumb'];?>" ?>   
                                                    <?php endforeach;?>  -->
                                                    <?php foreach ($value['Share']['cont_img'] as $k => $v):?> 
                                                        <?php foreach ($v as $kk => $vv):?>
                                                            <?php if($kk<9):?>
                                                                <?php echo $vv.'class="demo"/>';?>
                                                            <?php endif;?>
                                                        <?php endforeach;?> 
                                                    <?php endforeach;?>
                                                </div>
                                            </li>
                                        <?php endforeach;?> 
                                        </ul>
                                        <a class="more" href="/share/index/">查看更多></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab_box" style="display:none;">
                                <div class="patient">
                                    <div class="new_list_b">
                                        <ul>
                                        <?php foreach ($shareList['frst'] as $key => $value):?>
                                            <li>
                                                <div class="list_li">
                                                    <div class="touxiang"><img src="<?=$value['Share']['avatar']?>" alt="<?=$value['Share']['username']?>"></div>
                                                    <div class="list_b_right">
                                                        <a href="/share/info/<?php echo $value["Share"]["id"].".html";?>">
                                                        <div class="title_top"><?php 
                                                        echo Ivf::trimall($value['Share']['description']); 
                                                        ?></div> </a>
                                                        <div class="bottam">
                                                            <div class="b_name"><?=$value['Share']['username']?></div>
                                                            <div class="b_right">
                                                                <div class="b_pic1"><?=$value['Share']['browse_num']?></div>
                                                                <div class="b_pic2"><?=$value['Share']['comment_num']?></div>
                                                                <div class="b_pic3 up <?php echo $value['Share']['myagree']>0?'add_up':'';?>" <?php if($value['Share']['myagree']==0){?>onclick="adAgree(this,<?=$value["Share"]["id"]?>,'agree_share')"<?php }?>><?=$value['Share']['agree_num']?></div>
                                                                  <div class="b_right_mm">
                                                <div><img src="/images/hospital/y_1.png" />&nbsp;&nbsp;<?=$value['Share']['browse_num']?></div>
                                                <div><img src="/images/hospital/y_2.png" />&nbsp;&nbsp;<?=$value['Share']['comment_num']?></div>
                                                <div><img src="/images/hospital/y_3.png" />&nbsp;&nbsp;<?=$value['Share']['agree_num']?></div>
                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="img_b">
                                                    <!-- <?php foreach ($value['Share']['images'] as $k => $v):?> 
                                                    <img src="<?php echo $v['Picture']['thumb'];?>"?>   
                                                    <?php endforeach;?> -->
                                                    <?php foreach ($value['Share']['cont_img'] as $k => $v):?> 
                                                        <?php foreach ($v as $kk => $vv):?>
                                                            <?php if($kk<9):?>
                                                                <?php echo $vv.'class="demo"/>';?>
                                                            <?php endif;?>
                                                        <?php endforeach;?> 
                                                    <?php endforeach;?>
                                                </div>
                                            </li>
                                        <?php endforeach;?>  
                                        </ul>
                                        <a class="more" href="/share/index/0-10.html">查看更多></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab_box" style="display:none;">
                                <div class="patient">
                                    <div class="new_list_b">
                                        <ul>
                                         <?php foreach ($shareList['cpjd'] as $key => $value):?>
                                            <li>
                                                <div class="list_li">
                                                    <div class="touxiang"><img src="<?=$value['Share']['avatar']?>" alt="<?=$value['Share']['username']?>"></div>
                                                    <div class="list_b_right">
                                                        <a href="/share/info/<?php echo $value["Share"]["id"].".html";?>">
                                                        <div class="title_top"><?php 
                                                        echo Ivf::trimall($value['Share']['description']); 
                                                        ?></div> </a>
                                                        <div class="bottam">
                                                            <div class="b_name"><?=$value['Share']['username']?></div>
                                                            <div class="b_right">
                                                                <div class="b_pic1"><?=$value['Share']['browse_num']?></div>
                                                                <div class="b_pic2"><?=$value['Share']['comment_num']?></div>
                                                                <div class="b_pic3 up <?php echo $value['Share']['myagree']>0?'add_up':'';?>" <?php if($value['Share']['myagree']==0){?>onclick="adAgree(this,<?=$value["Share"]["id"]?>,'agree_share')"<?php }?>><?=$value['Share']['agree_num']?></div>
                                                                
                                                                 <div class="b_right_mm">
                                                <div><img src="/images/hospital/y_1.png" />&nbsp;&nbsp;<?=$value['Share']['browse_num']?></div>
                                                <div><img src="/images/hospital/y_2.png" />&nbsp;&nbsp;<?=$value['Share']['comment_num']?></div>
                                                <div><img src="/images/hospital/y_3.png" />&nbsp;&nbsp;<?=$value['Share']['agree_num']?></div>
                                            </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="img_b">
                                                    <?php foreach ($value['Share']['cont_img'] as $k => $v):?> 
                                                        <?php foreach ($v as $kk => $vv):?>
                                                            <?php if($kk<9):?>
                                                                <?php echo $vv.'class="demo"/>';?>
                                                            <?php endif;?>
                                                        <?php endforeach;?> 
                                                    <?php endforeach;?>
                                                </div>
                                            </li>
                                        <?php endforeach;?>  
                                        </ul>
                                        <a class="more" href="/share/index/0-20.html">查看更多></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab_box" style="display:none;">
                                <div class="patient">
                                    <div class="new_list_b">
                                        <ul>
                                         <?php foreach ($shareList['qlyz'] as $key => $value):?>
                                            <li>
                                                <div class="list_li">
                                                    <div class="touxiang"><img src="<?=$value['Share']['avatar']?>" alt="<?=$value['Share']['username']?>"></div>
                                                    <div class="list_b_right">
                                                        <a href="/share/info/<?php echo $value["Share"]["id"].".html";?>">
                                                        <div class="title_top"><?php 
                                                        echo Ivf::trimall($value['Share']['description']); 
                                                        ?></div> </a>
                                                        <div class="bottam">
                                                            <div class="b_name"><?=$value['Share']['username']?></div>
                                                            <div class="b_right">
                                                                <div class="b_pic1"><?=$value['Share']['browse_num']?></div>
                                                                <div class="b_pic2"><?=$value['Share']['comment_num']?></div>
                                                                <div class="b_pic3 up <?php echo $value['Share']['myagree']>0?'add_up':'';?>" <?php if($value['Share']['myagree']==0){?>onclick="adAgree(this,<?=$value["Share"]["id"]?>,'agree_share')"<?php }?>><?=$value['Share']['agree_num']?></div>
                                                                 <div class="b_right_mm">
                                                <div><img src="/images/hospital/y_1.png" />&nbsp;&nbsp;<?=$value['Share']['browse_num']?></div>
                                                <div><img src="/images/hospital/y_2.png" />&nbsp;&nbsp;<?=$value['Share']['comment_num']?></div>
                                                <div><img src="/images/hospital/y_3.png" />&nbsp;&nbsp;<?=$value['Share']['agree_num']?></div>
                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="img_b">
                                                    <?php foreach ($value['Share']['cont_img'] as $k => $v):?> 
                                                        <?php foreach ($v as $kk => $vv):?>
                                                            <?php if($kk<9):?>
                                                                <?php echo $vv.'class="demo"/>';?>
                                                            <?php endif;?>
                                                        <?php endforeach;?> 
                                                    <?php endforeach;?>
                                                </div>
                                            </li>
                                        <?php endforeach;?>  
                                        </ul>
                                        <a class="more" href="/share/index/0-30.html">查看更多></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab_box" style="display:none;">
                                <div class="patient">
                                    <div class="new_list_b">
                                        <ul>
                                         <?php foreach ($shareList['bt'] as $key => $value):?>
                                            <li>
                                                <div class="list_li">
                                                    <div class="touxiang"><img src="<?=$value['Share']['avatar']?>" alt="<?=$value['Share']['username']?>"></div>
                                                    <div class="list_b_right">
                                                        <a href="/share/info/<?php echo $value["Share"]["id"].".html";?>">
                                                        <div class="title_top"><?php 
                                                        echo Ivf::trimall($value['Share']['description']); 
                                                        ?></div> </a>
                                                        <div class="bottam">
                                                            <div class="b_name"><?=$value['Share']['username']?></div>
                                                            <div class="b_right">
                                                                <div class="b_pic1"><?=$value['Share']['browse_num']?></div>
                                                                <div class="b_pic2"><?=$value['Share']['comment_num']?></div>
                                                                <div class="b_pic3 up <?php echo $value['Share']['myagree']>0?'add_up':'';?>" <?php if($value['Share']['myagree']==0){?>onclick="adAgree(this,<?=$value["Share"]["id"]?>,'agree_share')"<?php }?>><?=$value['Share']['agree_num']?></div>
                                                                 <div class="b_right_mm">
                                                <div><img src="/images/hospital/y_1.png" />&nbsp;&nbsp;<?=$value['Share']['browse_num']?></div>
                                                <div><img src="/images/hospital/y_2.png" />&nbsp;&nbsp;<?=$value['Share']['comment_num']?></div>
                                                <div><img src="/images/hospital/y_3.png" />&nbsp;&nbsp;<?=$value['Share']['agree_num']?></div>
                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="img_b">
                                                    <?php foreach ($value['Share']['cont_img'] as $k => $v):?> 
                                                        <?php foreach ($v as $kk => $vv):?>
                                                            <?php if($kk<9):?>
                                                                <?php echo $vv.'class="demo"/>';?>
                                                            <?php endif;?>
                                                        <?php endforeach;?> 
                                                    <?php endforeach;?>
                                                </div>
                                            </li>
                                        <?php endforeach;?>  
                                        </ul>
                                        <a class="more" href="/share/index/0-40.html">查看更多></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab_box" style="display:none;">
                                <div class="patient">
                                    <div class="new_list_b">
                                        <ul>
                                         <?php foreach ($shareList['jyjy'] as $key => $value):?>
                                            <li>
                                                <div class="list_li">
                                                    <div class="touxiang"><img src="<?=$value['Share']['avatar']?>" alt="<?=$value['Share']['username']?>"></div>
                                                    <div class="list_b_right">
                                                        <a href="/share/info/<?php echo $value["Share"]["id"].".html";?>">
                                                        <div class="title_top"><?php 
                                                        echo Ivf::trimall($value['Share']['description']); 
                                                        ?></div> </a>
                                                        <div class="bottam">
                                                            <div class="b_name"><?=$value['Share']['username']?></div>
                                                            <div class="b_right">
                                                                <div class="b_pic1"><?=$value['Share']['browse_num']?></div>
                                                                <div class="b_pic2"><?=$value['Share']['comment_num']?></div>
                                                                <div class="b_pic3 up <?php echo $value['Share']['myagree']>0?'add_up':'';?>" <?php if($value['Share']['myagree']==0){?>onclick="adAgree(this,<?=$value["Share"]["id"]?>,'agree_share')"<?php }?>><?=$value['Share']['agree_num']?></div>
                                                                 <div class="b_right_mm">
                                                <div><img src="/images/hospital/y_1.png" />&nbsp;&nbsp;<?=$value['Share']['browse_num']?></div>
                                                <div><img src="/images/hospital/y_2.png" />&nbsp;&nbsp;<?=$value['Share']['comment_num']?></div>
                                                <div><img src="/images/hospital/y_3.png" />&nbsp;&nbsp;<?=$value['Share']['agree_num']?></div>
                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="img_b">
                                                    <?php foreach ($value['Share']['cont_img'] as $k => $v):?> 
                                                        <?php foreach ($v as $kk => $vv):?>
                                                            <?php if($kk<9):?>
                                                                <?php echo $vv.'class="demo"/>';?>
                                                            <?php endif;?>
                                                        <?php endforeach;?> 
                                                    <?php endforeach;?>
                                                </div>
                                            </li>
                                        <?php endforeach;?>  
                                        </ul>
                                        <a class="more" href="/share/index/0-50.html">查看更多></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab_box" style="display:none;">
                                <div class="patient">
                                    <div class="new_list_b">
                                        <ul>
                                         <?php foreach ($shareList['xq'] as $key => $value):?>
                                            <li>
                                                <div class="list_li">
                                                    <div class="touxiang"><img src="<?=$value['Share']['avatar']?>" alt="<?=$value['Share']['username']?>"></div>
                                                    <div class="list_b_right">
                                                        <a href="/share/info/<?php echo $value["Share"]["id"].".html";?>">
                                                        <div class="title_top"><?php 
                                                        echo Ivf::trimall($value['Share']['description']); 
                                                        ?></div> </a>
                                                        <div class="bottam">
                                                            <div class="b_name"><?=$value['Share']['username']?></div>
                                                            <div class="b_right">
                                                                <div class="b_pic1"><?=$value['Share']['browse_num']?></div>
                                                                <div class="b_pic2"><?=$value['Share']['comment_num']?></div>
                                                                <div class="b_pic3 up <?php echo $value['Share']['myagree']>0?'add_up':'';?>" <?php if($value['Share']['myagree']==0){?>onclick="adAgree(this,<?=$value["Share"]["id"]?>,'agree_share')"<?php }?>><?=$value['Share']['agree_num']?></div>
                                                                 <div class="b_right_mm">
                                                <div><img src="/images/hospital/y_1.png" />&nbsp;&nbsp;<?=$value['Share']['browse_num']?></div>
                                                <div><img src="/images/hospital/y_2.png" />&nbsp;&nbsp;<?=$value['Share']['comment_num']?></div>
                                                <div><img src="/images/hospital/y_3.png" />&nbsp;&nbsp;<?=$value['Share']['agree_num']?></div>
                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="img_b">
                                                    <?php foreach ($value['Share']['cont_img'] as $k => $v):?> 
                                                        <?php foreach ($v as $kk => $vv):?>
                                                            <?php if($kk<9):?>
                                                                <?php echo $vv.'class="demo"/>';?>
                                                            <?php endif;?>
                                                        <?php endforeach;?> 
                                                    <?php endforeach;?>
                                                </div>
                                            </li>
                                        <?php endforeach;?>  
                                        </ul>
                                        <a class="more" href="/share/index/0-60.html">查看更多></a>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="sh_box_right fixed">
                        <div class="promise">
                            <h3>无忧承诺</h3>
                            <ul>
                                <li>优质医院，实地考察</li>
                                <li>快速预约，直达医院</li>
                                <li>低价保证，服务保障</li>
                            </ul>   
                        </div>
                        <div class="com_pro">
                            <h3>常见问题</h3>
                            <ul>
                                <li><a href="/plan/index/1.html">
                                    <img src="/images/community/p1.png" alt="泰国试管婴儿攻略">
                                    <p>泰国医院</p></a>
                                </li>
                                <li><a href="/plan/index/3.html">
                                    <img src="/images/community/p2.png" alt="泰国试管婴儿攻略" >
                                    <p>费用流程</p></a>
                                </li>
                                <li><a href="/plan/index/2.html">
                                    <img src="/images/community/p3.png" alt="泰国试管婴儿攻略">
                                    <p>成功率</p></a>
                                </li>
                                <li><a href="/plan/index/2.html">
                                    <img src="/images/community/p4.png" alt="泰国试管婴儿攻略">
                                    <p>试管技术</p></a>
                                </li>
                                <li><a href="/plan/index/8.html">
                                    <img src="/images/community/p5.png" alt="泰国试管婴儿攻略">
                                    <p>疑难试管</p></a>
                                </li>
                                <li><a href="/plan/index/10.html">
                                   <img src="/images/community/p6.png" alt="泰国试管婴儿攻略">
                                    <p>最新资讯</p></a>
                                </li>
                                <li><a href="/plan/index/7.html">
                                    <img src="/images/community/p7.png" alt="泰国试管婴儿攻略" >
                                    <p>交通吃住</p></a>
                                </li>
                                <li><a href="/plan/index/6.html">
                                    <img src="/images/community/p8.png" alt="泰国试管婴儿攻略">
                                    <p>出国准备</p></a>
                                </li>
                                <li><a href="/plan/index/11.html">
                                    <img src="/images/community/p9.png" alt="泰国试管婴儿攻略">
                                    <p>经验心得</p></a>
                                </li>
                            </ul>
                        </div>
                        <div class="safe">
                            <a href="/site/index/" target="_blank">
                                <img src="/images/community/guanggao.jpg" alt="泰国试管婴儿攻略">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div> 
    
    <script type="text/javascript" charset="utf-8" >
        $(document).ready(function(e) {
            // $("AideClick").click(function(){
            //     $(".box_bg").fadeIn();
            // })
            $('.tab_btn li').click(function(event) {
                /* Act on the event */
                var index = $(this).index();
                $(this).attr('class','addBtn').siblings('li').attr('class','hiddenBtn');
                $('.tab_box').eq(index).show().siblings('.tab_box').hide();
            });
            $(".shareForm").Validform({
                btnSubmit:".ques_btn", 
                tiptype:function(msg){
                    layer.alert(msg,{
                        icon:0
                    });    
                },
                tipSweep:true,
                ajaxPost:true,
                callback:function(o){
                    layer.closeAll();
                    if (o.status == 4000) { 
                        $('.login').trigger("click");    
                    }
                    else if (o.status == 200) {
                        layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});       
                        setTimeout("window.location.href='/answer/'", 1000);
                    } else {
                        layer.alert(o.message,{
                            icon:5
                        });    
                    } 
                }
            });
        })

          $(".ans_btn").click(function(){
            var val=$("#ask_content").val();
             window.location.href = '/answer/index/0-' + val +".html";
        });
          $(".wpc").click(function(){
            $(".box_bg").show();
        });
    </script>
    <?= $this->element('aide_pc'); ?>