   
   <?= $this->Sgwy->addCss('/css/login/index.css'); ?>
   <?= $this->Sgwy->addJs("/js/mobile/user.js"); ?>
    <div class="logins">
            <!-- 登录 -->
            <div class="pop_login">
                <div class="pop_title">
                    <h2>登录试管无忧 </h2>
                </div>
                <div class="pop_form">
                        <form  class="ploginForm" action="/personal/login"  method="post" >
                        <div class="inp_1">
                            <input class="phone_1" type="text" placeholder="请输入手机号(限11位)" name="u_mobile"  datatype="m" errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！"/> 
                        </div>                   
                        <div class="inp_2">
                            <input  class="password_1" type="password"  name="u_pwd"  placeholder="请输入密码"  datatype="*8-16" errormsg="密码至少8个字符,最多16个字符！"  nullmsg="请输入密码！"/> 
                        </div> 
                        <div>
                            <input class="login_code_text" type="text" name="u_code" value="" placeholder="请输入验证码" datatype="*4-4" errormsg="请输入4位数验证码！"  nullmsg="请输入验证码！" autocomplete="off">  
                            <div class="login_code">
                                <img src="/personal/captcha" title="点击更换" onclick="this.src='/personal/captcha/?'+Math.random()"/>
                            </div> 
                        </div> 
                    <div class="sub">   
                        <input type="submit" class="submit" value="登录" />           
                        <a  class="make" >忘记密码?</a>
                    </div>   
                    <input type="hidden" name="dosubmit" value="1"/>                 
                    </form>               
                </div>     
            </div>
            <!-- 密码找回 -->
            <div class="pop_make">
                <div class="pop_title">
                    <h3>密码找回</h3>
                </div>
                <div class="pop_form">
                        <form  class="findForm" action="/mobile/retrieval"  method="post" > 
                        <div class="inp_1">                        
                            <input class="phone" type="text" placeholder="请输入手机号(限11位)" id="u_mobile" name="u_mobile"  datatype="m" errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！" autocomplete="off"/> 
                        </div>
                        <div style="height: 33px;">
                            <input class="code" type="text" placeholder="请输入短信验证码" name="u_code" value="" placeholder="输入6位验证码" datatype="*6-6" errormsg="请输入6位验证码！"  nullmsg="请输入6位验证码！"/>   
                            <input class="get_code" type="button"  id="send_mc_btn" onclick="_user.sendRcode('findpwd_send_code')" value="获取验证码"/>    
                        </div>
                        <div class="inp_2">                      
                            <input  class="password" type="password" placeholder="请输入密码"  name="u_pwd" datatype="*8-16" errormsg="密码至少8个字符,最多16个字符！"  nullmsg="请输入新密码！" autocomplete="off"/>
                            <label for="">请输入密码！</label>
                        </div>
                        <div class="inp_2">
                            <input  class="confirm" type="password" placeholder="请确认密码" name="u_comfirm_pwd"  type="text"  datatype="*" recheck="u_pwd" errormsg="两次输入的密码不一致！"  nullmsg="请确认新密码" autocomplete="off"/>
                            <label for="">两次密码不一致！</label>
                        </div>
                        <input type="hidden" name="dosubmit" value="1"/>  
                        <input type="submit" class="true" value="确定" />   
                    </form> 
                </div>     
            </div>
    </div>
<script >
$(document).ready(function(){
    $(".make").click(function(event){
        event.stopPropagation();
        $(".pop_make").show().siblings('div').hide();

    });

    $(".findForm").Validform({
        tiptype:function(msg){
            layer.alert(msg,{icon:0});   
        },
        tipSweep:true,
        ajaxPost:true,
        callback:function(o){
            if (o.status == 200) {
                layer.msg(o.message,{
                    icon:6,
                    skin:'layer_mobile_login'
                });    
                setTimeout("window.location.href='/personal/index/'", 2000);
            } else {
                layer.msg(o.message,{
                    icon:5,
                    skin:'layer_mobile_login'
                });    
            } 
        }
    });
    $(".ploginForm").Validform({
        tiptype:function(msg){
            layer.msg(msg,{
                icon:0,
                skin:'layer_mobile_login'
            });   
        },
        tipSweep:true,
        ajaxPost:true,
        callback:function(o){
            if (o.status == 200) {
                layer.msg(o.message,{
                    icon:6,
                    skin:'layer_mobile_login'
                });    
                setTimeout("window.location.href='/personal/index/'", 2000);
            } else {
                layer.msg(o.message,{
                    icon:5,
                    skin:'layer_mobile_login'
                });    
            } 
        }
    });
    $(".pregForm").Validform({
        tiptype:function(msg){
            layer.msg(msg,{
                icon:0,
                skin:'layer_mobile_login'
            });   
        },
        tipSweep:true,
        ajaxPost:true,
        callback:function(o){
            if (o.status == 200) {
                layer.msg(o.message,{
                    icon:6,
                    skin:'layer_mobile_login'
                });    
                setTimeout("window.location.href='/personal/index/'", 2000);
            } else {
                layer.msg(o.message,{
                    icon:5,
                    skin:'layer_mobile_login'
                });    
            } 
        }
    });
    
})
</script>  
</script>