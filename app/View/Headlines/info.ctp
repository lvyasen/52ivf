<?php $this->Sgwy->addCss('/css/news/info.css');?>

<div class="news_info">
    <div class="content">
    	<div class="top">
	        <p><a href="/community">首页</a> >  <a href="/news">泰国试管头条号</a> > <a href="/news/index/<?php echo $info["New"]["category_id"].".html";?>"><?php echo $cate_name;?></a> > <?=$info["New"]["title"]?></p>
	    </div> 
        <div class="Title">
            <h1>泰国试管头条号</h1><img class="lazy" onclick="history.go(-1)" src="/images/common/grey.gif" data-original="/images/answer/fanhui.jpg">
        </div>
	    <div class="main">
	    	<div class="subject">
                <div class="wrap">
                    <h1><?php echo $info["New"]["title"];?> </h1>
                    <div class="personal"> 
                        <img src="<?=$info["New"]["avatar"]?>" style=" margin-top: 0px;">
                        <span><?=$info["New"]["username"]?></span>
                        <em><?=$info["New"]["create_time"]?></em>
                    </div>
                    <?=$info["New"]["content"]?> 
                </div> 
	    	</div>
	    </div>

	    <div class="fixed"> 
            <div class="right_fix">
    	        <!-- <div class="nav">
                    <ul>
                        <li><a href="/contact">关于我们</a></li>
                        <li class="click_add"><a href="/news">新闻资讯</a></li>
                        <li><a href="/headlines">泰国试管头条号</a></li>
                        <li><a href="https://hr.lagou.com/company/gongsi/131537.html">加入我们</a></li>
                        <li><a href="/sitemap">网站地图</a></li>
                    </ul>
                </div> -->
                <?= $this->element('leftnav'); ?>
    	        <div class="fix_03">
                    <p>热门推荐</p>
                    <ul>
                        <?php foreach($top4List as $key => $value):?>
                        <a href="/news/info/<?=$value['New']['id']?>.html"><li>
                            <img class="lazy" src="<?=$value['New']['image']?>" data-original="<?=$value['New']['image']?>">
                            <div>
                                <span><?=$value['New']['title']?></span>
                                <p><?=$value['New']['description']?></p>
                            </div>
                        </li></a>
                        <?php endforeach;?> 
                    </ul>
                </div>
            </div>
	    </div>
    </div>
	
</div>
<script type="text/javascript">

$(".answerForm").Validform({
    btnSubmit:".release", 
    tiptype:function(msg){
        layer.msg(msg,{
            icon:5,
            skin:'layer_mobile_login'
        });    
    },
    tipSweep:true,
    ajaxPost:true,
    callback:function(o){
        layer.closeAll();
        if (o.status == 4000) { 
            $('.login').trigger("click");    
        }
        else if (o.status == 200) {
            layer.msg(o.message,{
                icon:5,
                skin:'layer_mobile_login'
            });       
            setTimeout(function () {
                window.location.reload();
            }, 1000);   
        } else {
            layer.msg(o.message,{
                icon:5,
                skin:'layer_mobile_login'
            });    
        } 
    }
});
</script> 