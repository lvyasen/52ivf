
		<div class="margin">
			<form role="form" id="userForm" class="dataForm" action="<?= $_SERVER['REQUEST_URI'] ?>" method="post">
				<div class="add_style">
					<ul>   
                        <li class="clearfix"><label class="label_name col-xs-1"><i>*</i>国家名称：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="country" type="text"  class="col-xs-4" value="<?php echo isset($dataList['country'])?$dataList['country']:''?>" placeholder="国家名称"  datatype="*2-50" errormsg="名称至少2个字符,最多20个字符！"  nullmsg="请填写国家名称！"/></div> </li>  

						 
	                    <li class="clearfix"><label class="label_name col-xs-1">描述：</label>
	                        <span class="col-xs-6"><textarea name="description" class="form-control col-xs-10" id="form_textarea" placeholder="" onkeyup="checkLength(this,100,'sy');"><?php echo isset($dataList['description'])?$dataList['description']:''?></textarea><span class="wordage">剩余字数：<span id="sy" style="color:Red;">100</span>字</span></span>
	                    </li> 

						<li class="clearfix"><label class="label_name col-xs-1">排序：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="sort" type="text" value="<?php echo isset($dataList['sort'])?$dataList['sort']:'100'?>" datatype="n" errormsg="请填写数字！"   ignore="ignore" class="col-xs-1"/>（列表从小到大排列）</div>  
						</li>  
					</ul>
					<!--按钮操作-->
					<li class="clearfix">
						<div class="col-xs-2 col-lg-2">&nbsp;</div>
						<div class="col-xs-6">
							<input class="btn button_btn bg-deep-blue" type="submit" value="保存提交"/>
							<input name="reset" class="btn button_btn btn-gray" value="取消重置" type="reset">
							<a href="/admin/district/"  class="btn btn-info button_btn"><i class="fa fa-reply"></i> 返回上一步</a>
						</div>
					</li> 
					<input type="hidden" name="dosubmit" value="1"/>  
				</form>
			</div>
		</div> 
	<script type="text/javascript">  
		$(".dataForm").Validform({tiptype:3});  
	</script>