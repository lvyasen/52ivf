<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<?php echo $this->Html->css("/admin/css/comment.css");?> 
    <?php echo $this->Html->script('/admin/js/ivf52/comment.js');?>    
    <?php echo $this->Html->script('/js/page.js');?>     
    <?php echo $this->Html->script('/admin/js/laydate/laydate.js');?>  
</head>
<body>
	<div class="comment">       
        <div class="content">
            <div class="main">
                <div class="info">
                    <div class="blog" >
                        <img class="lazy" src="<?=$info["avatar"]?>">
                        <div class="blog_text">
                            <h4><?=$info["title"]?></h4>
                            <h1><?=$info["content"]?></h1>
                            <img class="lazy" src="<?=$info["avatar"]?>">   
                            <div class="nate">
                                <span class="name"><?=$info["username"]?></span>
                                <span class="date"><?php echo Ivf::formatTime($info['create_time']);?></span>
                            </div>
                        </div>
                    </div>
                    <div class="reply" id="loadPage">  
                        <?php if(sizeof($comments) >= 1){?><p>全部评论</p><?php }?>
                        <ul class="moreAns">
                            <?php foreach ($comments as $key => $value):?> 
                            <li>
                                <div class="pinglu">
                                    <div class="head_pic">
                                        <img src="<?=$value['Comment']["avatar"]?>">
                                    </div>
                                    <div class="conte">
                                        <div class="namec"><?=$value['Comment']["username"]?></div>
                                        <div class="contc"><?php echo $value['Comment']['content'];?></div>
                                        <div class="timec"><?php echo Ivf::formatTime($value['Comment']['comment_time']);?></div>
                                        <div class="DC">
                                        <a class="delect" href="javascript:;" onclick="delAct(this,'comment',<?php echo $value['Comment']['id'];?>,'single','li')">删除</a>
                                        <a class="creplay"  href="javascript:void(0);" onclick="replayComment(<?=$value['Comment']['id']?>)">回复</a></div>
                                        <?php if(!empty($value['Comment']['childCmt'])){?>
                                        <span class="moreHui">更多回复</span>
                                        <?php }?>
                                    </div>
                                    <!--子回复开始-->
                                    <div class="conteBox">
                                        <?php if(!empty($value['Comment']['childCmt'])){?>
                                        <?php foreach ($value['Comment']['childCmt'] as $kk => $vv):?> 
                                        
	                                    <div class="conte">
	                                        <div class="namec"><?php echo $vv['replayName'];?></div>
	                                        <div class="contc"><?php echo $vv['content'];?></div>
	                                        <div class="timec"><?php echo Ivf::formatTime($vv['comment_time']);?></div>
	                                        <a class="creplay"  href="javascript:void(0);" onclick="replayComment(<?=$vv['id']?>)">回复</a>
	                                    </div>
                                        <?php endforeach;?>  
                                        <?php }?>
                                    </div> 
                                    <!--子回复结束-->
                                </div>
                            </li>
                            <?php endforeach;?>   
                        </ul>
                        <div class="reBg">
                        <form  class="replayForm" action="/admin/replay_answer_comment"  method="post" >
                            <div class="ph_btn_two">
                                <a>x</a>
                                <div class="text_two">
                                    <textarea  name="content" datatype="*"  nullmsg="请填写评论内容！"  placeholder="我来说一说..."></textarea>
                                    <div>
                                        <label>评论用户：</label>
                                        <select name="uid" id="shareUser" class="shareUser"> 
                                            <?php foreach ($usersDate as $key => $value) {?> 
                                            <option value="<?php echo $value['User']['id'];?>" <?php echo isset($dataList['uid']) && $dataList['uid']==$value['User']['id']?'selected':''?>><?php echo $value['User']['nick_name'];?></option>   
                                            <?php }?> 
                                        </select> 
                                        <input class="shuiji" type="button" onclick="toggerUser(<?php echo $ctype;?>,'shareUser')"  value="随机筛选发布会员">
                                    </div> 
                                    <div>
                                        <label for="">评论时间：</label>
                                        <input class="laydate-icon col-xs-3" name="comment_time" value="<?php echo date("Y-m-d H:i:s");?>" id="start6" >
                                    </div>
                                    <div>
                                        <label for="">评论顺序：</label>
                                        <input name="sort" type="text"  value="1000"  />
                                    </div>
                                    <input type="submit" class="button" value="回复"/>
                                </div>
                            </div> 
                            <input type="hidden" id="repalyVal" name="reply_id" value="0"/>  
                            <input type="hidden" name="cid" value="<?=$info["id"]?>"/>  
                            <input type="hidden" name="ctype" value="<?php echo $ctype;?>"/>    
                            <input type="hidden" name="dosubmit" value="1"/>                 
                        </form></div>  
                        <?php if(sizeof($comments) >= 5){?>
                        <div class="more"> 
                            <a href="javascript:void(0)" title="" onclick="_page.getPageComment(this,<?=$info['id']?>,'<?php echo  $ajaxComment;?>','manage')">加载更多</a>
                        </div>
                               
                        <?php }?>

                        <form  class="answerForm" action="/admin/replay_comment"  method="post" >
                        <div class="text_box">
                            <div>
	                            <label>发表回复:</label>
	                            <textarea name="content" datatype="*"  nullmsg="请填写评论内容！"></textarea> 
                            </div>  
                            <div>
	                          	<label>评论用户：</label>
	                          	<select name="uid" id="shareUser2" class="shareUser2"> 
                                    <?php foreach ($usersDate as $key => $value) {?> 
                                    <option value="<?php echo $value['User']['id'];?>" <?php echo isset($dataList['uid']) && $dataList['uid']==$value['User']['id']?'selected':''?>><?php echo $value['User']['nick_name'];?></option>   
                                    <?php }?> 
                                </select> 
	                          	<input class="shuiji" type="text" onclick="toggerUser(<?php echo $ctype;?>,'shareUser2')"  value="随机筛选发布会员">
                            </div> 
                          	<div>
                          		<label for="">评论时间：</label>
                          		<input class="laydate-icon col-xs-3" name="comment_time" value="<?php echo date("Y-m-d H:i:s");?>" id="start5" >
                          	</div>
                          	<div>
                          		<label for="">评论顺序：</label>
                          		<input name="sort" type="text"  value="1000"  />
                          	</div>
                            <div class="re_btn">
                                <a class="release" href="javascript:void(0);">发表回复</a>
                                <a onclick="history.back(-1);" class="btn btn-info button_btn"><i class="fa fa-reply"></i> 返回上一步</a>
                                
                            </div>
                        </div> 
                        <input type="hidden" name="ctype" value="<?php echo $ctype;?>"/>    
                        <input type="hidden" name="cid" value="<?=$info["id"]?>"/>       
                        </form>   
                        
                    </div>
                </div>
                

            </div>
        </div>
    </div>  
</body>
            
<script> 
    var start5 = {
        elem: '#start5',
        format: 'YYYY-MM-DD hh:mm:ss',
       // min: laydate.now(), //设定最小日期为当前日期
        max: '2099-06-16', //最大日期
        istime: true,
        istoday: false,
        choose: function(datas){
             end.min = datas; //开始日选好后，重置结束日的最小日期
             end.start = datas //将结束日的初始值设定为开始日
         }
     };
    var start6 = {
        elem: '#start5',
        format: 'YYYY-MM-DD hh:mm:ss',
       // min: laydate.now(), //设定最小日期为当前日期
        max: '2099-06-16', //最大日期
        istime: true,
        istoday: false,
        choose: function(datas){
             end.min = datas; //开始日选好后，重置结束日的最小日期
             end.start = datas //将结束日的初始值设定为开始日
         }
     };
    laydate(start5); 
    laydate(start6); 
</script>
</html>


    