<?php echo $this->Html->script('/admin/js/laydate/laydate.js');?> 
    <div class="margin Competence_style" id="page_style">
        <div class="operation clearfix">   
            <ul class="choice_search">
                <form role="form" id="userForm" action="/admin/account_comment" method="get">  
                    <li class="clearfix col-xs-2 "><label class="label_name col-xs-1">评论板块：&nbsp;&nbsp;</label><div class="Add_content col-xs-1">
                        <select name="ctype"> 
                        <option value="0">---请选择---</option>
                            <?php foreach ($comment_type as $key => $value) {?> 
                        <option value="<?php echo $value;?>" <?php echo isset($_GET['ctype']) && $_GET['ctype']==$value?'selected':''?>><?php echo $key;?></option>  
                        <?php }?> 
                        </select></div>  
                    </li>
                    <li class="clearfix col-xs-4 col-lg-5 col-ms-5 "><label class="label_name ">时间：</label> 
                        <input class="laydate-icon col-xs-4 col-lg-3" name="startTime" value="<?= @$_GET['startTime'] ?>" id="start" style=" margin-right:10px; height:28px; line-height:28px; float:left">
                        <span  style=" float:left; padding:0px 10px; line-height:32px;">至</span>
                        <input class="laydate-icon col-xs-4 col-lg-3" name="endTime" id="end" value="<?= @$_GET['endTime'] ?>" style="height:28px; line-height:28px; float:left">
                    </li> 
                    <input name="username" type="text" value="<?= @$_GET['username'] ?>" placeholder="评论内容" class="form-control"/>
                    <button class="btn button_btn bg-deep-blue " onclick="this.form.submit()"  type="button"><i class="fa  fa-search"></i>&nbsp;搜索</button> 
                    <a href="javascript:void(0)"  class="btn button_btn bg-deep-blue" title="添加评论"  onclick="editAct('添加评论',0)"><i class="fa  fa-edit"></i>&nbsp;添加评论</a>    
                </form> 
            </ul> 
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr>  
                        <th>序号</th>
                        <th>类型</th>
                        <th>评论主题</th>
                        <th>评论会员</th>   
                        <th>评论内容</th>
                        <!--<th>浏览数</th>
                        <th>赞数</th>   
                        <th>置顶</th> -->  
                        <th class="hidden-480">评论时间</th>   
                        <th class="hidden-480">创建时间</th>   
                        <th class="hidden-480">操作</th>   
                    </tr>
                </thead> 
                <tbody> 
                    <?php foreach ($dataList as $key => $v): ?>
                    <tr> 
                        <td><?= $v['Comment']['id'] ?></td>  
                        <td><?php echo array_search($v['Comment']['topic_type'], $comment_type);?></td>  
                        <td><?php echo $v['Comment']['topic_title'];?></td> 
                        <td><?php echo $v['Comment']['uid'];?></td> 
                        <td><?php echo $v['Comment']['content'];?></td> 
                       <!-- <td><?php echo $v['Comment']['browse_num'];?></td> 
                        <td><?php echo $v['Comment']['agree_num'];?></td>  
                        <td><span  class="<?php echo $v['Comment']['is_top']==1?'yesimg':'noimg';?>" onclick="toggle(this,'comment', <?php echo $v['Comment']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>  -->
                        <td><?= date('Y-m-d H:i:s',strtotime($v['Comment']['comment_time'])) ?></td> 
                        <td><?= date('Y-m-d H:i:s',strtotime($v['Comment']['create_time'])) ?></td> 
                        <td class="td-manage"> 
                            <a title="编辑"  href="javascript:void(0)" onclick="editAct('编辑评论',<?php echo $v['Comment']['id'];?>)"  class="btn button_btn bg-deep-blue">编辑</a> 
                            <a title="删除" href="javascript:;" onclick="delAct(this,'comment',<?php echo $v['Comment']['id'];?>,'single')" class="btn button_btn btn-danger">删除</a>
                        </td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table> 
            <?php echo $this->element('admin/pagination');?>
        </div>
    </div>    
<!--添加栏目-->
<div class="add_columns_style" id="add_columns_style" style=" display:none"> 
    <form action="/admin/edit_comment" id="formEdit"  class="dataForm" role="form"  method="post" enctype="multipart/form-data"></form>
</div>  
<script type="text/javascript">

/******时间设置*******/
var start = {
    elem: '#start',
    format: 'YYYY-MM-DD',
   // min: laydate.now(), //设定最小日期为当前日期
    max: '2099-06-16', //最大日期
    istime: true,
    istoday: false,
    choose: function(datas){
         end.min = datas; //开始日选好后，重置结束日的最小日期
         end.start = datas //将结束日的初始值设定为开始日
     }
 };
 var end = {
    elem: '#end',
    format: 'YYYY-MM-DD',
    //min: laydate.now(),
    max: '2099-06-16',
    istime: true,
    istoday: false,
    choose: function(datas){
        start.max = datas; //结束日选好后，重置开始日的最大日期
    }
};
laydate(start);
laydate(end);
</script>