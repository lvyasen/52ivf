<?php echo $this->Html->script('/admin/js/laydate/laydate.js');?>   
    <div class="margin Competence_style" id="page_style">
        <div class="operation clearfix">   
                <ul class="choice_search">
                <form role="form" id="userForm" action="/admin/account_message" method="get">
                    <li class="clearfix col-xs-4 col-lg-5 col-ms-3 "><label class="label_name ">时间：</label> 
                        <input class="laydate-icon col-xs-4 col-lg-3" name="startTime" value="<?= @$_GET['startTime'] ?>" id="start" style=" margin-right:10px; height:28px; line-height:28px; float:left">
                        <span  style=" float:left; padding:0px 10px; line-height:32px;">至</span>
                        <input class="laydate-icon col-xs-4 col-lg-3" name="endTime" id="end" value="<?= @$_GET['endTime'] ?>" style="height:28px; line-height:28px; float:left">
                    </li>
                    <li class="clearfix col-xs-4 col-lg-5 col-ms-3 ">
                    <input name="username" type="text" value="<?= @$_GET['username'] ?>" placeholder="真实姓名/手机号" class="form-control col-xs-8"/>
                    </li>
                    <button class="btn button_btn bg-deep-blue " onclick="this.form.submit()" type="button"><i class="fa  fa-search"></i>&nbsp;搜索</button> 
                    <a href="/admin/account_message/?import=1&startTime=<?= @$_GET['startTime'] ?>&endTime=<?= @$_GET['endTime'] ?>&username=<?= @$_GET['username'] ?>"  class="btn button_btn bg-deep-blue" title="导出">&nbsp;导出</a> 
                </form>
                </ul> 
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr>   
                        <th>序号</th> 
                        <th>真实姓名</th>   
                        <th>手机号</th> 
                        <th>QQ</th>   
                        <th>电子邮件</th> 
                        <th>留言内容</th>  
                        <th class="hidden-480">留言时间</th>   
                    </tr>
                </thead> 
                <tbody> 
                    <?php foreach ($dataList as $key => $v): ?>
                    <tr> 
                        <td><?= $v['Leave']['id'] ?></td> 
                        <td><?php echo $v['Leave']['name'];?></td>
                        <td><?php echo $v['Leave']['phone'];?></td> 
                        <td><?php echo $v['Leave']['qq'];?></td>   
                        <td><?php echo $v['Leave']['email'];?></td>   
                        <td><?php echo $v['Leave']['content'];?></td>   
                        <td><?php echo $v['Leave']['ctime'];?></td> 
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table> 
            <?php echo $this->element('admin/pagination');?>
        </div>
    </div>    

<script type="text/javascript"> 
/******时间设置*******/ 
laydate({elem: '#start', istime: true, format: 'YYYY-MM-DD'});  
laydate({elem: '#end', istime: true, format: 'YYYY-MM-DD'});  
</script>