
    <?php echo $this->Html->script('/admin/js/laydate/laydate.js');?>  
    <div class="margin Competence_style" id="page_style">
        <div class="operation clearfix">
            <ul class="choice_search">
                <form role="form" id="userForm" action="/admin/hospital" method="get">   
                    <li class="clearfix col-xs-2"><label class="label_name col-xs-1">医院区域：&nbsp;&nbsp;</label><div class="Add_content col-xs-1">
                        <select name="city_id"> 
                        <option value="0">---请选择---</option>
                        <?php foreach ($districtList as $key => $value) {?> 
                        <option value="<?php echo $value['District']['id'];?>" <?php echo isset($_GET['city_id']) && $_GET['city_id']==$value['District']['id']?'selected':''?>><?php echo $value['District']['country'];?></option>  
                        <?php }?> 
                        </select></div>  
                    </li>
                    <li class="clearfix col-xs-4 col-lg-5 col-ms-5 "><label class="label_name ">时间：</label> 
                        <input class="laydate-icon col-xs-4 col-lg-3" name="startTime" value="<?= @$_GET['startTime'] ?>" id="start" style=" margin-right:10px; height:28px; line-height:28px; float:left">
                        <span  style=" float:left; padding:0px 10px; line-height:32px;">至</span>
                        <input class="laydate-icon col-xs-4 col-lg-3" name="endTime" id="end" value="<?= @$_GET['endTime'] ?>" style="height:28px; line-height:28px; float:left">
                    </li> 
                    <input name="keywords" type="text" value="<?= @$_GET['keywords'] ?>" placeholder="医院" class="form-control"/>
                    <button class="btn button_btn bg-deep-blue " onclick="this.form.submit()"  type="button"><i class="fa  fa-search"></i>&nbsp;搜索</button> 
                    <a href="/admin/hospital_edit"  class="btn button_btn bg-deep-blue" title="添加医院"><i class="fa  fa-edit"></i>&nbsp;添加医院</a>    
                </form> 
            </ul>  
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr>    
                        <th>封面图</th>   
                        <th>医院名称</th>   
                        <th>英文名称</th>  
                        <th>等级</th>    
                        <th>人均价格</th>    
                        <th>擅长项目</th>   
                        <th>人气分</th>   
                        <th>口碑分</th> 
                        <th>活跃分</th> 
                        <th>预约数</th> 
                        <th>案例数</th>  
                        <th>关注数</th>  
                        <th>候诊时间</th>  
                        <th>状态</th>  
                        <th>发布时间</th> 
                        <th class="hidden-480">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataList as $key => $v) { ?>
                    <tr>    
                        <td><img src="<?php echo $v['Hospital']['image'];?>" width="50px"></td>   
                        <td><?php echo $v['Hospital']['en_name'];?></td>
                        <td><?php echo $v['Hospital']['cn_name'];?></td> 
                        <td><?php echo $v['Hospital']['lever'];?></td>  
                        <td><?php echo $v['Hospital']['price'];?></td>  
                        <td><?php echo $v['Hospital']['projects'];?></td>  
                        <td><?php echo $v['Hospital']['popularity'];?></td>  
                        <td><?php echo $v['Hospital']['public_praise'];?></td>  
                        <td><?php echo $v['Hospital']['active'];?></td> 
                        <td><?php echo $v['Hospital']['appoint_num'];?></td> 
                        <td><?php echo $v['Hospital']['case_num'];?></td> 
                        <td><?php echo $v['Hospital']['follow_num'];?></td> 
                        <td><?php echo $v['Hospital']['waiting_time'];?></td>  
                        <td><span  class="<?php echo $v['Hospital']['status']==1?'yesimg':'noimg';?>" onclick="toggle(this,'hospital', <?php echo $v['Hospital']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>  
                        <td><?= date('Y-m-d H:i:s',strtotime($v['Hospital']['create_time'])) ?></td>  
                        <td class="td-manage"> 
                            <a title="编辑" href="/admin/account_comment?ctype=20&cid=<?= $v['Hospital']['id']?>" class="btn button_btn bg-deep-blue">查看评论</a>
                            <a title="编辑" href="/admin/hospital_edit?id=<?= $v['Hospital']['id']?>" class="btn button_btn bg-deep-blue">编辑</a>
                            <a title="经营项目" href="/admin/hospital_project_list?pid=<?= $v['Hospital']['id']?>" class="btn button_btn bg-deep-blue">经营项目</a>
                            <?php if($userRoles=='all'){?>   <a title="删除" href="javascript:;" onclick="delAct(this,'hospital',<?php echo $v['Hospital']['id'];?>,'single')" class="btn button_btn btn-danger">删除</a><?php }?>   
                        </td>
                    </tr>
                    <?php }?> 
                </tbody>
            </table>
            <?php echo $this->element('admin/pagination');?>
        </div>
    </div> 

<script type="text/javascript">

/******时间设置*******/
var start = {
    elem: '#start',
    format: 'YYYY-MM-DD',
   // min: laydate.now(), //设定最小日期为当前日期
    max: '2099-06-16', //最大日期
    istime: true,
    istoday: false,
    choose: function(datas){
         end.min = datas; //开始日选好后，重置结束日的最小日期
         end.start = datas //将结束日的初始值设定为开始日
     }
 };
 var end = {
    elem: '#end',
    format: 'YYYY-MM-DD',
    //min: laydate.now(),
    max: '2099-06-16',
    istime: true,
    istoday: false,
    choose: function(datas){
        start.max = datas; //结束日选好后，重置开始日的最大日期
    }
};
laydate(start);
laydate(end);
</script>