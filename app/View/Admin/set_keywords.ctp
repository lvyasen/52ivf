 
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.min.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.bootstrap.js');?>  
    <div class="margin Competence_style" id="page_style">
        <div class="operation clearfix"> 
            <a href="javascript:void(0)"  onclick="editAct('设置关键词',0)" class="btn button_btn bg-deep-blue" title="添加三要素"><i class="fa  fa-edit"></i>&nbsp;添加三要素</a>  
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr>  
                        <th>序号</th>
                        <th>键值</th>  
                        <th>meta_title</th>  
                        <th>meta_keywords</th>  
                        <th>meta_description</th>  
                        <th class="hidden-480">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataList as $key => $v) { ?>
                    <tr>  
                        <td><?php echo $v['Keyword']['id'];?></td>   
                        <td><?php echo array_search($v['Keyword']['key'], $keywords);?></td>  
                        <td><?php echo $v['Keyword']['meta_title'];?></td> 
                        <td><?php echo $v['Keyword']['meta_keywords'];?></td> 
                        <td><?php echo $v['Keyword']['meta_description'];?></td>  
                        <td class="td-manage"> 
                            <a title="编辑"  href="javascript:void(0)" onclick="editAct('设置关键词',<?php echo $v['Keyword']['id'];?>)" class="btn button_btn bg-deep-blue">编辑</a><a title="删除" href="javascript:;" onclick="delAct(this,'keywords',<?php echo $v['Keyword']['id'];?>,'single')" class="btn button_btn btn-danger">删除</a>
                        </td>
                    </tr>
                    <?php }?> 
                </tbody>
            </table>
        </div>
    </div> 
<!--添加栏目-->
<div class="add_columns_style" id="add_columns_style" style=" display:none"> 
    <form action="/admin/keywords_edit" id="formEdit"  class="dataForm" role="form"  method="post"></form>
</div>  
<script type="text/javascript">  
var oTable1 = $('#data_table').dataTable( {
"bPaginate": true, 
"width":"100%", 
"bLengthChange":false,
"iDisplayLength": 20,
//"columns" : _tableCols, 
"bStateSave": false,//状态保存
"searching": false,
"aoColumnDefs": [{"orderable":false,"aTargets":[5]
}]

});
</script>
