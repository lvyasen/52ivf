<?php echo $this->Html->script('/admin/js/ivf52/user.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.min.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.bootstrap.js');?>  
    <div class="margin Competence_style" id="page_style">
        <div class="operation clearfix"> 
            <a href="/admin/specials_edit"  class="btn button_btn bg-deep-blue" title="添加链接"><i class="fa  fa-edit"></i>&nbsp;添加专题</a>  
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr>  
                        <th>序号</th> 
                        <th>专题名称</th>  
                        <th>meta_title</th>  
                        <th>meta_keywords</th>  
                        <th>meta_description</th>  
                        <th class="hidden-480">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataList as $key => $v) { ?>
                    <tr>  
                        <td><?php echo $v['Special']['id'];?></td>  
                        <td><a target="_blank" href="http://www.ivf52.com/special/info/<?php echo $v['Special']['id'];?>.html"><?php echo $v['Special']['title'];?></a></td> 
                        <td><?php echo $v['Special']['meta_title'];?></td>   
                        <td><?php echo $v['Special']['meta_keywords'];?></td>   
                        <td><?php echo $v['Special']['meta_description'];?></td>   
                        <td class="td-manage"> 
                            <a title="编辑" href="/admin/specials_edit?id=<?= $v['Special']['id']?>" class="btn button_btn bg-deep-blue">编辑</a><a title="删除" href="javascript:;" onclick="delAct(this,'specials',<?php echo $v['Special']['id'];?>,'single')" class="btn button_btn btn-danger">删除</a>
                        </td>
                    </tr>
                    <?php }?> 
                </tbody>
            </table>
        </div>
    </div> 
<script type="text/javascript">  
var oTable1 = $('#data_table').dataTable( {
"bPaginate": true, 
"width":"100%", 
"bLengthChange":false,
"iDisplayLength": 20,
//"columns" : _tableCols, 
"bStateSave": false,//状态保存
"searching": false,
"aoColumnDefs": [{"orderable":false,"aTargets":[5]
}]

});
</script>
