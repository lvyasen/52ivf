
		<div class="margin">
			<form role="form" id="userForm" class="dataForm" action="<?= $_SERVER['REQUEST_URI'] ?>" method="post">
				<div class="add_style">
					<ul>  
						<li class="clearfix"><label class="label_name col-xs-1">岗位名称：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="title" type="text"  class="col-xs-4" value="<?php echo isset($dataList['title'])?$dataList['title']:''?>" placeholder="岗位名称"  datatype="*2-50" errormsg="名称至少2个字符,最多20个字符！" /></div> </li>
 
						<li class="clearfix"><label class="label_name col-xs-1">所属部门：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="Dept" type="text"  class="col-xs-4" value="<?php echo isset($dataList['Dept'])?$dataList['Dept']:''?>" placeholder="所属部门"  datatype="*2-50" errormsg="部门至少2个字符,最多20个字符！"   ignore="ignore"/></div> </li>

						<li class="clearfix"><label class="label_name col-xs-1">最低学历：&nbsp;&nbsp;</label><div class="Add_content col-xs-9">
				        	<select name="Education"> 
				            <?php foreach ($education as $key => $value) {?> 
				            <option value="<?php echo $key;?>" <?php echo isset($dataList['Education']) && $dataList['Education']==$key?'selected':''?>><?php echo $value;?></option>  
				            <?php }?> 
				        	</select></div>  
				    	</li>   

						<li class="clearfix"><label class="label_name col-xs-1">工作经验：&nbsp;&nbsp;</label><div class="Add_content col-xs-9">
				        	<select name="Experience"> 
				            <?php foreach ($experience as $key => $value) {?> 
				            <option value="<?php echo $key;?>" <?php echo isset($dataList['Experience']) && $dataList['Experience']==$key?'selected':''?>><?php echo $value;?></option>  
				            <?php }?> 
				        	</select></div>  
				    	</li> 
 
						<li class="clearfix"><label class="label_name col-xs-1">薪资范围：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="SalaryA" type="text"  class="col-xs" size="6"  value="<?php echo isset($dataList['SalaryA'])?$dataList['SalaryA']:''?>" placeholder="最低"/>&nbsp;-&nbsp;<input name="SalaryB" type="text" class="col-xs" size="6"  value="<?php echo isset($dataList['SalaryB'])?$dataList['SalaryB']:''?>" placeholder="最高"/>&nbsp;（不填面议）</div></div> </li>

                        <li class="clearfix"><label class="label_name col-xs-1">职位描述：&nbsp;&nbsp;</label><span class="Add_content col-xs-9"><textarea name="description" class="form-control col-xs-12 col-sm-5" id="form_textarea" placeholder=""><?php echo isset($dataList['description'])?$dataList['description']:''?></textarea></span></li>


                        <li class="clearfix"><label class="label_name col-xs-1">任职资格：&nbsp;&nbsp;</label><span class="Add_content col-xs-9"><textarea name="Qualification" class="form-control col-xs-12 col-sm-5" id="form_textarea" placeholder="" ><?php echo isset($dataList['Qualification'])?$dataList['Qualification']:''?></textarea></span></li>  

						<li class="clearfix"><label class="label_name col-xs-1">排序：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="sort" type="text" value="<?php echo isset($dataList['sort'])?$dataList['sort']:'100'?>" datatype="n" errormsg="请填写数字！"   ignore="ignore" class="col-xs-1"/>（列表从大到小排列）</div>  
						</li> 
  
					</ul>
					<!--按钮操作-->
					<li class="clearfix">
						<div class="col-xs-2 col-lg-2">&nbsp;</div>
						<div class="col-xs-6">
							<input class="btn button_btn bg-deep-blue" type="submit" value="保存提交"/>
							<input name="reset" class="btn button_btn btn-gray" value="取消重置" type="reset">
							<a href="/admin/joins/"  class="btn btn-info button_btn"><i class="fa fa-reply"></i> 返回上一步</a>
						</div>
					</li> 
					<input type="hidden" name="dosubmit" value="1"/>  
				</form>
			</div>
		</div> 
	<script type="text/javascript">  
		$(".dataForm").Validform({tiptype:3});  
	</script>