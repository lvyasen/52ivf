<div class="add_style">
    <ul> 
        <li class="clearfix"><label class="label_name col-xs-3">设置页面：&nbsp;&nbsp;</label><div class="Add_content col-xs-9">
            <select name="key"> 
                <?php foreach ($keywords as $key => $value) {?> 
                <option value="<?php echo $value;?>" <?php echo isset($dataList['key']) && $dataList['key']==$value?'selected':''?>><?php echo $key;?></option>  
                <?php }?> 
            </select></div>  
    </li>
    <li class="clearfix"><label class="label_name col-xs-3"><i>*</i>meta_title：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="meta_title" type="text"  class="col-xs-11" value="<?php echo isset($dataList['meta_title'])?$dataList['meta_title']:''?>" placeholder="meta_title"  datatype="*2-50" errormsg="meta_title至少2个字符,最多50个字符！"  nullmsg="请填写meta_title！"/></div> </li>

    <li class="clearfix"><label class="label_name col-xs-3"><i>*</i>meta_keywords：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="meta_keywords" type="text"  class="col-xs-11" value="<?php echo isset($dataList['meta_keywords'])?$dataList['meta_keywords']:''?>" placeholder="meta_keywords"  datatype="*2-100" errormsg="meta_keywords至少2个字符,最多100个字符！"  nullmsg="请填写meta_keywords！"/></div> </li> 

    <li class="clearfix"><label class="label_name col-xs-3"><i>*</i>meta_description：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="meta_description" type="text"  class="col-xs-11" value="<?php echo isset($dataList['meta_description'])?$dataList['meta_description']:''?>" placeholder="meta_description"  datatype="*2-250" errormsg="meta_description至少2个字符,最多250个字符！"  nullmsg="请填写meta_description！"/></div> </li>
</ul>
<input type="hidden" name="dosubmit" value="1"/>  
</div>
<script type="text/javascript">  
$(".dataForm").Validform({tiptype:3});  
</script>