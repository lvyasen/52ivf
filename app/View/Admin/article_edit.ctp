 
    <!--复文本编辑框--> 
    <?php echo $this->Html->script('/admin/js/ueditor/ueditor.config.js');?>  
    <?php echo $this->Html->script('/admin/js/ueditor/ueditor.all.min.js');?>  
    <?php echo $this->Html->script('/admin/js/ueditor/lang/zh-cn/zh-cn.js');?>    
    <?php echo $this->Html->script('/admin/js/plupload/plupload.full.min.js');?>     

    <link rel="stylesheet" type="text/css" href="/admin/js/plupload/css/common.css">
    <link rel="stylesheet" type="text/css" href="/admin/js/plupload/css/index.css"> 
    <?php echo $this->Html->script('/admin/js/ivf52/pic.js');?>
        <div class="margin">
            <form role="form" id="userForm" class="dataForm" action="<?= $_SERVER['REQUEST_URI'] ?>" method="post"  enctype="multipart/form-data">
                <div class="add_style">
                    <ul>            
                        <li class="clearfix"><label class="label_name col-xs-1"><i>*</i>标题名称：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="title" type="text"  class="col-xs-4" value="<?php echo isset($dataList['title'])?$dataList['title']:''?>" placeholder="标题名称"  datatype="*2-50" errormsg="名称至少2个字符,最多20个字符！"  nullmsg="请填写标题名称！"/></div> </li>  
 
                        <li class="clearfix"><label class="label_name col-xs-1">是否用户发布：&nbsp;&nbsp;</label><div class="Add_content col-xs-9">
                            <select name="uid" id="shareUser"> 
                            <option value="0">--选择用户--</option>
                            <?php foreach ($usersDate as $key => $value) {?> 
                            <option value="<?php echo $value['User']['id'];?>" <?php echo isset($dataList['uid']) && $dataList['uid']==$value['User']['id']?'selected':''?>><?php echo $value['User']['nick_name'];?></option>   
                            <?php }?> 
                        </select>  
                        &nbsp;&nbsp;&nbsp;
                        <a href="javascript:void(0);" onclick="toggerUser(3,'shareUser')" class="btn btn-info button_btn">随机筛选未发布会员</a>  
                        </li>  

                        <li class="clearfix"><label class="label_name col-xs-1">就诊医院：&nbsp;&nbsp;</label><div class="Add_content col-xs-9">
                            <select name="hospital_id"> 
                            <option value="0">--选择医院--</option>
                            <?php foreach ($hospitalList as $key => $value) {?> 
                            <option value="<?php echo $value['Hospital']['id'];?>" <?php echo isset($dataList['hospital_id']) && $dataList['hospital_id']==$value['Hospital']['id']?'selected':''?>><?php echo $value['Hospital']['cn_name'];?></option>  
                            <?php }?> 
                            </select></div>  
                        </li> 

                        <li class="clearfix"><label class="label_name col-xs-1">晒图：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"> 
                           <section class=" img-section">
                            <p class="up-p"><span class="up-span">最多可以上传3张图片，马上上传</span></p>
                            <div class="z_photo upimg-div clear"  id="divContent">
                                     <section class="z_file fl" id="btnMultipleUpload">
                                        <img src="/admin/js/plupload/img/a11.png" tppabs="/admin/js/plupload/img/a11.png" class="add-img">
                                        <input type="file" name="file" id="file" class="file" value="" accept="image/jpg,image/jpeg,image/png,image/bmp" multiple />
                                     </section>
                                    <?php if(!empty($picData)){ foreach ($picData as $key => $value) { ?>
                                       <section class="up-section fl add_pic" id="pic_<?php echo $key;?>"> 
                                            <span class="up-span"></span>
                                            <img src="/admin/js/plupload/img/a7.png" class="close-upimg" onclick="_pic.DeleteFile(<?php echo $value['Picture']['id'];?>,'pic_<?php echo $key;?>')"> 
                                            <img src="<?php echo $value['Picture']['thumb'];?>" class="up-img">  
                                        </section> 
                                    <?php }}?>
                             </div>
                         </section>
                        </li> 
  
                        <li class="clearfix"><label class="label_name col-xs-1">所属分类：&nbsp;&nbsp;</label><div class="Add_content col-xs-9">
                            <select name="category_id">
                                <option value="0">--选择攻略分类--</option>
                                <?php foreach ($cateList as $key => $value) {?> 
                                <option value="<?php echo $value['id'];?>" <?php echo isset($dataList['category_id']) && $dataList['category_id']==$value['id']?'selected':''?>><?php echo $value['name'];?></option> 
                                <?php if(isset($value['_child']) && sizeof($value['_child'])>0){ foreach ($value['_child'] as $kk => $vv) {?> 
                                    <option value="<?php echo $vv['id'];?>" <?php echo isset($dataList['category_id']) && $dataList['category_id']==$vv['id']?'selected':''?>>&nbsp;---<?php echo $vv['name'];?></option>
                                <?php }}}?> 
                            </select></div>  
                        </li>  
                    <li class="clearfix"><label class="label_name col-xs-1">描述：</label>
                        <span class="col-xs-6"><textarea name="description" class="form-control col-xs-10" id="form_textarea" ><?php echo isset($dataList['description'])?$dataList['description']:''?></textarea></span>
                    </li> 
                    <li class="clearfix"><label class="label_name col-xs-1"><i>*</i>攻略内容：&nbsp;&nbsp;</label>
                        <div class="Add_content col-xs-11"><textarea name="content"  id="content"  style="width:100%;height:500px;"><?php echo isset($dataList['content'])?$dataList['content']:''?></textarea></div>
                    </li> 
                    <li class="clearfix"><label class="label_name col-xs-1">点赞数：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="agree_num" type="text"  class="col-xs"  value="<?php echo isset($dataList['agree_num'])?$dataList['agree_num']:mt_rand(10, 80);?>"  datatype="n1-10" errormsg="请填写浏览量！"  nullmsg="请填写浏览量！"/></div></li> 

                    <li class="clearfix"><label class="label_name col-xs-1">浏览数：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="browse_num" type="text"  class="col-xs" value="<?php echo isset($dataList['browse_num'])?intval($dataList['browse_num']):mt_rand(90, 200);?>"  datatype="n1-10" errormsg="请填写浏览数！"  nullmsg="请填写浏览数！"/></div></li>
                    <li class="clearfix"><label class="label_name col-xs-1">评论数：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="comment_num" type="text"  class="col-xs" value="<?php echo isset($dataList['comment_num'])?intval($dataList['comment_num']):mt_rand(1, 60);?>"  datatype="n1-10" errormsg="请填写评论数！"  nullmsg="请填写评论数！"/></div></li> 

                    <li class="clearfix"><label class="label_name col-xs-1">推荐排序：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="sort" type="text" value="<?php echo isset($dataList['sort'])?$dataList['sort']:'100'?>" class="col-xs-1"/></div></li>
                    <li class="clearfix">
                        <label class="label_name col-xs-1">是否有效：&nbsp;&nbsp;</label>
                        <div class="Add_content col-xs-9">
                            <label class="l_f checkbox_time"><input type="checkbox" name="status" class="ace" id="checkbox" value="1"  <?php echo isset($dataList['status']) && $dataList['status']==0?'':'checked=checked'?>><span class="lbl">是</span></label> 
                        </div> 
                    </li> 
                    <li class="clearfix">
                        <label class="label_name col-xs-1">是否置顶：&nbsp;&nbsp;</label>
                        <div class="Add_content col-xs-9">
                            <label class="l_f checkbox_time"><input type="checkbox" name="is_top" class="ace" id="checkbox" value="1"  <?php echo isset($dataList['is_top']) && $dataList['is_top']==1?'checked=checked':''?>><span class="lbl">是</span></label> 
                        </div> 
                    </li>
                    <li class="clearfix">
                        <label class="label_name col-xs-1">是否精华：&nbsp;&nbsp;</label>
                        <div class="Add_content col-xs-9">
                            <label class="l_f checkbox_time"><input type="checkbox" name="is_elite" class="ace" id="checkbox" value="1"  <?php echo isset($dataList['is_elite']) && $dataList['is_elite']==1?'checked=checked':''?>><span class="lbl">是</span></label> 
                        </div> 
                    </li>
                    <li class="clearfix">
                        <label class="label_name col-xs-1">热门推荐：&nbsp;&nbsp;</label>
                        <div class="Add_content col-xs-9">
                            <label class="l_f checkbox_time"><input type="checkbox" name="is_rec" class="ace" id="checkbox" value="1"  <?php echo isset($dataList['is_rec']) && $dataList['is_rec']==1?'checked=checked':''?>><span class="lbl">是</span></label> 
                        </div> 
                    </li> 
                </ul>
                <!--按钮操作-->
                <li class="clearfix">
                    <div class="col-xs-2 col-lg-2">&nbsp;</div>
                    <div class="col-xs-6">
                        <input class="btn button_btn bg-deep-blue" type="submit" value="保存提交"/>
                        <input name="reset" class="btn button_btn btn-gray" value="取消重置" type="reset">
                        <a href="/admin/article/"  class="btn btn-info button_btn"><i class="fa fa-reply"></i> 返回上一步</a>
                    </div>
                </li> 
                <input type="hidden" name="dosubmit" value="1"/>  
            </form> 
        </div>
    </div>  
<script type="text/javascript">  
$(function(){ 
    UE.getEditor('content'); 
    $(".dataForm").Validform({tiptype:3});     
});
var MAX_FILE_NUM = 3;   
var multi = new plupload.Uploader({
    browse_button: 'btnMultipleUpload', // this can be an id of a DOM element or the DOM element itself
    url: '/adminajax/file?ajaxdata=article', 
    max_file_size: '10mb',
    chunk_size: '3mb',
    filters: {
        mime_types: [
          { title: "Image files", extensions: "BMP,jpg,JPEG,gif,png" }
        ]
    },
    runtimes: 'html5',
    multi_selection: true
});

multi.init();

multi.bind('FilesAdded', function (up, files) { 
    var uptLenth=$('.add_pic').size();   
    if(((uptLenth-1)+files.length) >= MAX_FILE_NUM){ 
         layer.msg('最多添加3张',{icon:0,time:1000});  
         multi.splice();
         multi.refresh();
        return;
    }else{ 
        layer.load(1); 
        plupload.each(files, function (file) {     
            $('#divContent').append('<section class="up-section fl add_pic" id="pic_'+file.id+'"></section>');
        });
        multi.start();
    }
});  
multi.bind('FileUploaded', function (up, file, resp) { 
    var json = JSON.parse(resp.response); 
    $('#pic_' + file.id).html('<span class="up-span"></span><img src="/admin/js/plupload/img/a7.png" class="close-upimg" onclick="_pic.DeleteFile(' + json.data.id + ',\'pic_' + file.id + '\')"><img class="up-img" src="' + json.data.url + '"/><input type="hidden"  name="UptPicture[]" value="'+json.data.id+'" />');
    layer.closeAll('loading');
});

multi.bind('Error', function (up, err) {   
    layer.msg("Error #" + err.code + ": " + err.message,{icon:0,time:1000});  
}); 

</script> 