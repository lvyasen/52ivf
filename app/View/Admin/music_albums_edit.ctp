
		<div class="margin">
			<form role="form" id="userForm" class="dataForm" action="<?= $_SERVER['REQUEST_URI'] ?>" method="post"  enctype="multipart/form-data">
				<div class="add_style">
					<ul>
						<li class="clearfix"><label class="label_name col-xs-1">专辑封面：&nbsp;&nbsp;</label>
						     <span class="cont_style col-xs-9">        
						       <div id="preview" class="preview_img"><img id="imghead" width="190" src="<?php echo isset($dataList['img']) && !empty($dataList['img'])?$dataList['img']:'/admin/images/image.png';?>" border="0"></div>
						       <div class="fileInput ">
						           <input onchange="previewImage(this)" name="imgFile" id="upfile" class="upfile" type="file">
						        <input class="upFileBtn" value="上传图片" onclick="document.getElementById('upfile').click()" type="button">
						        </div>
						      </span>
						  </li> 
						<li class="clearfix"><label class="label_name col-xs-1">专辑名称：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="name" type="text"  class="col-xs-4" value="<?php echo isset($dataList['name'])?$dataList['name']:''?>" placeholder="名称"  datatype="s2-50" errormsg="名称至少2个字符,最多50个字符！"   ignore="ignore"/></div> </li>

						<li class="clearfix"><label class="label_name col-xs-1">上级分类：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><select name="parent_id">
				            <option value="0">--顶级分类--</option>
				            <?php foreach ($cateList as $key => $value) {?> 
				            <option value="<?php echo $key;?>" <?php echo isset($dataList['parent_id']) && $dataList['parent_id']==$key?'selected':''?>><?php echo $value;?></option>   
				            <?php }?> 
				        </select></div>  
				    </li>
                     
				    <li class="clearfix"><label class="label_name col-xs-1">简介：&nbsp;&nbsp;</label><span class="Add_content col-xs-9"><textarea name="description" class="form-control col-xs-12 col-sm-5" id="form_textarea" placeholder="" onkeyup="checkLength(this,100,'sy');"><?php echo isset($dataList['description'])?$dataList['description']:''?></textarea><span class="wordage">剩余字数：<span id="sy" style="color:Red;">100</span>字</span></span></li> 
				    <li class="clearfix"><label class="label_name col-xs-1">排序：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="sort" type="text"  value="<?php echo isset($dataList['sort'])?$dataList['sort']:'1000'?>"  class="col-xs-1"/></div>  
				    </li> 
				    <li class="clearfix"><label class="label_name col-xs-1">作者：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="author" type="text"  value="<?php echo isset($dataList['author'])?$dataList['author']:''?>"  class="col-xs-1"/></div>  
				    </li> 

				    <li class="clearfix">
				        <label class="label_name col-xs-1">是否有效：&nbsp;&nbsp;</label>
				        <div class="Add_content col-xs-9">
				            <label class="l_f checkbox_time"><input type="checkbox" name="status" class="ace" id="checkbox" value="1"  <?php echo isset($dataList['status']) && $dataList['status']==1?'checked=checked':''?>><span class="lbl">是</span></label> 
				        </div> 
				    </li> 
					</ul>
					<!--按钮操作-->
					<li class="clearfix">
						<div class="col-xs-2 col-lg-2">&nbsp;</div>
						<div class="col-xs-6">
							<input class="btn button_btn bg-deep-blue" type="submit" value="保存提交"/>
							<input name="reset" class="btn button_btn btn-gray" value="取消重置" type="reset">
							<a href="/admin/music_albums/"  class="btn btn-info button_btn"><i class="fa fa-reply"></i> 返回上一步</a>
						</div>
					</li> 
					<input type="hidden" name="dosubmit" value="1"/>  
				</form>
			</div>
		</div> 
	<script type="text/javascript">  
		$(".dataForm").Validform({tiptype:3});  
	</script>