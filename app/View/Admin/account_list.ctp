
    <div class="margin Competence_style" id="page_style">
        <div class="operation clearfix">  
            <ul class="choice_search">
                <form role="form" id="userForm" action="/admin/account_list" method="get"> 
                <li><label class="col-xs-1 col-lg-2 label_name" for="form-field-1">用户类型：</label> 
                        <label class="radio-inline"><input name="type" class="ace radio" type="radio" value="all" <?php echo $type=='all'?'checked':'checked'?>><span class="lbl">全部</span></label> 
                        <label class="radio-inline"><input name="type" class="ace radio" type="radio" value="0" <?php echo $type=='0'?'checked':''?>><span class="lbl">真实用户</span></label>
                        <label class="radio-inline"><input name="type" class="ace radio" type="radio" value="1" <?php echo $type=='1'?'checked':''?>><span class="lbl">测试用户</span></label>  
                    </li> &nbsp;&nbsp;&nbsp; 
                <li class="clearfix col-xs-2 "><input name="username" type="text" value="<?= @$_GET['username'] ?>" placeholder="昵称/真实姓名/手机号" class="form-control col-xs-9"/>  </li>
                <button class="btn button_btn bg-deep-blue " onclick="this.form.submit()" type="button"><i class="fa  fa-search"></i>&nbsp;搜索</button> 
                <a href="#"  class="btn button_btn bg-deep-blue " id="btnSingleUpload">批量导入</a> 
                <a href="/admin/account_list/?import=1"  class="btn button_btn bg-deep-blue" title="导出模板">&nbsp;导出模板</a>    
                </form>
            </ul>  
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr>  
                        <th>序号</th>
                        <th>昵称</th>
                        <th>真实姓名</th>   
                        <th>手机号</th>
                        <th>性别</th>
                        <th>年龄</th>   
                        <th>身份证号码</th>    
                        <th>IP</th>   
                        <th>状态</th>   
                        <th class="hidden-480">创建时间</th>   
                        <th class="hidden-480">操作</th>   
                    </tr>
                </thead> 
                <tbody> 
                    <?php foreach ($dataList as $key => $v): ?>
                    <tr> 
                        <td><?= $v['User']['id'] ?></td> 
                        <td><?php echo $v['User']['nick_name'];?></td>
                        <td><?php echo $v['User']['user_name'];?></td> 
                        <td><a href="/admin/users_info/<?php echo $v['User']['mobile'];?>"><?php echo $v['User']['mobile'];?></a></td> 
                        <td><?php echo $sex[$v['User']['sex']];?></td>  
                        <td><?php echo $v['User']['age'];?></td> 
                        <td><?php echo $v['User']['cert_num'];?></td> 
                        <td><?php echo $v['User']['ip'];?></td>  
                        <td><span  class="<?php echo $v['User']['status']==1?'yesimg':'noimg';?>" onclick="toggle(this,'account', <?php echo $v['User']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>  
                        <td><?= date('Y-m-d H:i:s',strtotime($v['User']['create_time'])) ?></td> 
                        <td>
                            <?php if($userRoles=='all'){?>   <a title="删除" href="javascript:;" onclick="delAct(this,'account',<?php echo $v['User']['id'];?>,'single')" class="btn button_btn btn-danger">删除</a><?php }?>  
                        </td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table> 
            <?php echo $this->element('admin/pagination');?>
        </div>
    </div> 
    <?php echo $this->Html->script('/admin/js/plupload/plupload.full.min.js');?>     

<script type="text/javascript">
 //上传excel文件
    var single = new plupload.Uploader({
        browse_button: 'btnSingleUpload', // this can be an id of a DOM element or the DOM element itself
        url: "/adminajax/file/?ajaxdata=add_users",
        filters: {
            mime_types: [
                { title: "Excel files", extensions: "xlsx,xls" }
            ],
            max_file_size:'2048kb'
        },
        runtimes: 'html5',
        multi_selection:false
    });

    single.init();
        
    single.bind('FilesAdded', function (up, files) {
        layer.msg('正在上传,请稍候...');
        single.start();
    });

    single.bind('UploadProgress', function (up, file) { 
        layer.msg('已上传'+file.percent+"%"); 
    });

    single.bind('FileUploaded', function (up, file, resp) {  
        var res = eval('(' + resp.response + ')');  
        if(res.status==200){
            layer.msg(res.message);  
            setTimeout(function () {  
                location.reload();
            }, 2000);
        }else{
            layer.msg(res.message); 
        } 
    });

    single.bind('Error', function (up, err) { 
        layer.msg('上传失败，请检查格式重新上传....'); 
    }); 
</script> 