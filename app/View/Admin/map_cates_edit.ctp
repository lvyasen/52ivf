<div class="add_style">
    <ul>
        <li class="clearfix"><label class="label_name col-xs-3"><i>*</i>分类名称：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="name" type="text"  class="col-xs-6" value="<?php echo isset($dataList['name'])?$dataList['name']:''?>" placeholder="分类名称"  datatype="*2-50" errormsg="分类名称至少2个字符,最多50个字符！"  nullmsg="请填写分类名称！"/></div>  
        </li>  
        <li class="clearfix"><label class="label_name col-xs-3">上级分类：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><select name="parent_id">
            <option value="0">--顶级分类--</option>
            <?php foreach ($cateList as $key => $value) {?> 
            <option value="<?php echo $value['id'];?>" <?php echo isset($dataList['parent_id']) && $dataList['parent_id']==$value['id']?'selected':''?>><?php echo $value['name'];?></option> 
            <?php if(isset($value['_child']) && sizeof($value['_child'])>0):?>
            <?php  foreach ($value['_child'] as $kk => $vv) {?> 
            <option value="<?php echo $vv['id'];?>" <?php echo isset($dataList['parent_id']) && $dataList['parent_id']==$vv['id']?'selected':''?>>&nbsp;---<?php echo $vv['name'];?></option>
            <?php }?> 
            <?php endif; ?>
            <?php }?> 
        </select></div>  
    </li>
    <li class="clearfix"><label class="label_name col-xs-3">分类描述：&nbsp;&nbsp;</label><span class="Add_content col-xs-9"><textarea name="description" class="form-control col-xs-12 col-sm-5" id="form_textarea" placeholder="" onkeyup="checkLength(this,100,'sy');"><?php echo isset($dataList['description'])?$dataList['description']:''?></textarea><span class="wordage">剩余字数：<span id="sy" style="color:Red;">100</span>字</span></span></li> 
    <li class="clearfix"><label class="label_name col-xs-3">排序：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="sort" type="text"  value="<?php echo isset($dataList['sort'])?$dataList['sort']:'1000'?>"  class="col-xs-1"/></div>  
    </li>
    <li class="clearfix">
        <label class="label_name col-xs-3">是否有效：&nbsp;&nbsp;</label>
        <div class="Add_content col-xs-9">
            <label class="l_f checkbox_time"><input type="checkbox" name="status" class="ace" id="checkbox" value="1"  <?php echo isset($dataList['status']) && $dataList['status']==0?'':'checked=checked'?>><span class="lbl">是</span></label> 
        </div> 
    </li> 
</ul>
<input type="hidden" name="dosubmit" value="1"/>  
</div>
<script type="text/javascript">  
$(".dataForm").Validform({tiptype:3});  
</script>