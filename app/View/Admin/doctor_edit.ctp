         
    <?php echo $this->Html->script('/admin/js/ueditor/ueditor.config.js');?>  
    <?php echo $this->Html->script('/admin/js/ueditor/ueditor.all.min.js');?>  
    <?php echo $this->Html->script('/admin/js/ueditor/lang/zh-cn/zh-cn.js');?>    
		<div class="margin">
			<form role="form" id="userForm" class="dataForm" action="<?= $_SERVER['REQUEST_URI'] ?>" method="post"  enctype="multipart/form-data">
				<div class="add_style">
					<ul>  
				        <li class="clearfix"><label class="label_name col-xs-1">主图：&nbsp;&nbsp;</label>
				             <span class="cont_style col-xs-9">        
				               <div id="preview" class="preview_img"><img id="imghead" width="190" src="<?php echo isset($dataList['avatar']) && !empty($dataList['avatar'])?$dataList['avatar']:'/admin/images/image.png';?>" border="0"></div>
				               <div class="fileInput ">
				                   <input onchange="previewImage(this)" name="imgFile" id="upfile" class="upfile" type="file">
				                <input class="upFileBtn" value="上传图片" onclick="document.getElementById('upfile').click()" type="button">
				                </div>
				              </span>
				          </li> 
				        <li class="clearfix"><label class="label_name col-xs-1">手机详情封面图：&nbsp;&nbsp;</label>
				             <span class="cont_style col-xs-9">        
				               <div id="preview2" class="preview_img"><img id="imghead2" width="190" src="<?php echo isset($dataList['mb_image']) && !empty($dataList['mb_image'])?$dataList['mb_image']:'/admin/images/image.png';?>" border="0"></div>
				               <div class="fileInput ">
				                   <input onchange="PreviewImage(this,'preview2')" name="imgFile2" id="upfile2" class="upfile" type="file">
				                <input class="upFileBtn" value="上传图片" onclick="document.getElementById('upfile2').click()" type="button">
				                </div>
				              </span>
				         </li> 
						       
                        <li class="clearfix"><label class="label_name col-xs-1"><i>*</i>中文名称：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="cn_name" type="text"  class="col-xs-4" value="<?php echo isset($dataList['cn_name'])?$dataList['cn_name']:''?>" placeholder="中文名称"  datatype="*2-100" errormsg="名称至少2个字符,最多100个字符！"  nullmsg="请填写中文名称！"/></div> </li>  

                        <li class="clearfix"><label class="label_name col-xs-1"><i>*</i>英文名称：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="en_name" type="text"  class="col-xs-4" value="<?php echo isset($dataList['en_name'])?$dataList['en_name']:''?>" placeholder="英文名称"  datatype="*2-100" errormsg="名称至少2个字符,最多100个字符！"  nullmsg="请填写英文名称！"/></div> </li> 
 

                        <li class="clearfix"><label class="label_name col-xs-1">职务：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="duty" type="text"  class="col-xs-4" value="<?php echo isset($dataList['duty'])?$dataList['duty']:''?>" placeholder="职务" /></div></li> 
                        <li class="clearfix"><label class="label_name col-xs-1">工作年限：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="job_duration" type="text"  class="col-xs-4" value="<?php echo isset($dataList['job_duration'])?$dataList['job_duration']:''?>" placeholder="工作年限" /></div></li>  

				        <li class="clearfix"><label class="label_name col-xs-1">最高学历：&nbsp;&nbsp;</label><div class="Add_content col-xs-9">
				        	<select name="highest_education"> 
				            <?php foreach ($education as $key => $value) {?> 
				            <option value="<?php echo $key;?>" <?php echo isset($dataList['highest_education']) && $dataList['highest_education']==$key?'selected':''?>><?php echo $value;?></option>  
				            <?php }?> 
				        	</select></div>  
				    	</li>
				        <li class="clearfix"><label class="label_name col-xs-1">所属医院：&nbsp;&nbsp;</label><div class="Add_content col-xs-9">
				        	<select name="hospital_id"> 
				            <?php foreach ($hospitalList as $key => $value) {?> 
				            <option value="<?php echo $value['Hospital']['id'];?>" <?php echo isset($dataList['hospital_id']) && $dataList['hospital_id']==$value['Hospital']['id']?'selected':''?>><?php echo $value['Hospital']['cn_name'];?></option>  
				            <?php }?> 
				        	</select></div>  
				    	</li>
                        <li class="clearfix"><label class="label_name col-xs-1">口碑分：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="public_praise" type="text"  class="col-xs-4" value="<?php echo isset($dataList['public_praise'])?$dataList['public_praise']:''?>" placeholder="口碑分" /></div></li> 


                        <li class="clearfix"><label class="label_name col-xs-1">人气分：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="popularity" type="text"  class="col-xs-4" value="<?php echo isset($dataList['popularity'])?$dataList['popularity']:''?>" placeholder="人气分" /></div></li>  

                        <li class="clearfix"><label class="label_name col-xs-1">活跃分：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="active" type="text"  class="col-xs-4" value="<?php echo isset($dataList['active'])?$dataList['active']:''?>" placeholder="活跃分" /></div></li> 

                        <li class="clearfix"><label class="label_name col-xs-1">预约数：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="appoint_num" type="text"  class="col-xs-4" value="<?php echo isset($dataList['appoint_num'])?$dataList['appoint_num']:''?>" placeholder="预约数" /></div></li> 
 

                        <li class="clearfix"><label class="label_name col-xs-1">案例数：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="case_num" type="text"  class="col-xs-4" value="<?php echo isset($dataList['case_num'])?$dataList['case_num']:''?>" placeholder="案例数" /></div></li> 

                        <li class="clearfix"><label class="label_name col-xs-1">关注数：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="follow_num" type="text"  class="col-xs-4" value="<?php echo isset($dataList['follow_num'])?$dataList['follow_num']:''?>" placeholder="关注数" /></div></li> 
 
    
                        <li class="clearfix"><label class="label_name col-xs-1">医院环境评分：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="environment_score" type="text"  class="col-xs-4" value="<?php echo isset($dataList['environment_score'])?$dataList['environment_score']:''?>" placeholder="医院环境评分" /></div></li>  
                        <li class="clearfix"><label class="label_name col-xs-1">医疗技术评分：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="skill_score" type="text"  class="col-xs-4" value="<?php echo isset($dataList['skill_score'])?$dataList['skill_score']:''?>" placeholder="医疗技术评分" /></div></li>  
                        <li class="clearfix"><label class="label_name col-xs-1">服务评分：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="service_score" type="text"  class="col-xs-4" value="<?php echo isset($dataList['service_score'])?$dataList['service_score']:''?>" placeholder="服务评分" /></div></li>  
                        <li class="clearfix"><label class="label_name col-xs-1">综合指数评分：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="composite_score" type="text"  class="col-xs-4" value="<?php echo isset($dataList['composite_score'])?$dataList['composite_score']:''?>" placeholder="综合指数评分" /></div></li>  

                        <!--<li class="clearfix"><label class="label_name col-xs-1">导师服务：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="tutor_service" type="text"  class="col-xs-4" value="<?php echo isset($dataList['tutor_service'])?$dataList['tutor_service']:''?>" placeholder="导师服务" /></div></li> 
                        <li class="clearfix"><label class="label_name col-xs-1">候诊时间：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="waiting_time" type="text"  class="col-xs-4" value="<?php echo isset($dataList['waiting_time'])?$dataList['waiting_time']:''?>" placeholder="推荐数" /></div></li> -->
                        <li class="clearfix"><label class="label_name col-xs-1">擅长项目：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="projects" type="text"  class="col-xs-4" value="<?php echo isset($dataList['projects'])?$dataList['projects']:''?>" placeholder="擅长项目" />（逗号分隔，如：眼部,鼻部,面部轮廓）</div></li> 
  

                        <li class="clearfix"><label class="label_name col-xs-1">简介：&nbsp;&nbsp;</label><span class="Add_content col-xs-9"><textarea name="introduction" class="form-control col-xs-12 col-sm-5" id="form_textarea" placeholder=""><?php echo isset($dataList['introduction'])?$dataList['introduction']:''?></textarea></span></li>


                        <li class="clearfix"><label class="label_name col-xs-1">资历描述：&nbsp;&nbsp;</label><span class="Add_content col-xs-9"><textarea name="description" class="form-control col-xs-12 col-sm-5" id="form_textarea" placeholder="" ><?php echo isset($dataList['description'])?$dataList['description']:''?></textarea></span></li>  


						<li class="clearfix"><label class="label_name col-xs-1">排序：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="sort" type="text" value="<?php echo isset($dataList['sort'])?$dataList['sort']:'100'?>" datatype="n" errormsg="请填写数字！"   ignore="ignore" class="col-xs-1"/>（列表从小到大排列）</div>  
						</li> 
						<li class="clearfix">
							<label class="label_name col-xs-1">是否有效：&nbsp;&nbsp;</label>
							<div class="Add_content col-xs-9">
								<label class="l_f checkbox_time"><input type="checkbox" name="status" class="ace" id="checkbox" value="1"  <?php echo isset($dataList['status']) && $dataList['status']==1?'checked=checked':''?>><span class="lbl">是</span></label> 
							</div> 
						</li> 
						<li class="clearfix">
							<label class="label_name col-xs-1">是否推荐：&nbsp;&nbsp;</label>
							<div class="Add_content col-xs-9">
								<label class="l_f checkbox_time"><input type="checkbox" name="rec" class="ace" id="checkbox" value="1"  <?php echo isset($dataList['rec']) && $dataList['rec']==1?'checked=checked':''?>><span class="lbl">是</span></label> 
							</div> 
						</li> 

					</ul>
					<!--按钮操作-->
					<li class="clearfix">
						<div class="col-xs-2 col-lg-2">&nbsp;</div>
						<div class="col-xs-6">
							<input class="btn button_btn bg-deep-blue" type="submit" value="保存提交"/>
							<input name="reset" class="btn button_btn btn-gray" value="取消重置" type="reset">
							<a href="/admin/doctor/"  class="btn btn-info button_btn"><i class="fa fa-reply"></i> 返回上一步</a>
						</div>
					</li> 
					<input type="hidden" name="dosubmit" value="1"/>  
				</form>
			</div>
		</div> 
<script type="text/javascript">  
$(function(){ 
    UE.getEditor('rule'); 
    $(".dataForm").Validform({tiptype:3});  
});
</script>