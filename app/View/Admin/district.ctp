<?php echo $this->Html->script('/admin/js/ivf52/user.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.min.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.bootstrap.js');?>  
    <div class="margin Competence_style" id="page_style">
        <div class="operation clearfix"> 
            <a href="/admin/district_edit"  class="btn button_btn bg-deep-blue" title="添加地区"><i class="fa  fa-edit"></i>&nbsp;添加地区</a>  
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr>  
                        <th>序号</th>
                        <th>国家</th>  
                        <th>描述</th>   
                        <th class="hidden-480">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataList as $key => $v) { ?>
                    <tr>  
                        <td><?php echo $v['District']['id'];?></td>
                        <td><?php echo $v['District']['country'];?></td> 
                        <td><?php echo $v['District']['description'];?></td>  
                        <td class="td-manage"> 
                            <a title="编辑" href="/admin/district_edit?id=<?= $v['District']['id']?>" class="btn button_btn bg-deep-blue">编辑</a>
                    </tr>
                    <?php }?> 
                </tbody>
            </table>
        </div>
    </div> 
<script type="text/javascript">  
var oTable1 = $('#data_table').dataTable( {
"bPaginate": true, 
"width":"100%", 
"bLengthChange":false,
"iDisplayLength": 20,
//"columns" : _tableCols, 
"bStateSave": false,//状态保存
"searching": false,
"order": [[ 0, "desc" ]],
"aoColumnDefs": [{"orderable":false,"aTargets":[3]
}]

});
</script>
