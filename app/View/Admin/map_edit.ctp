
		<div class="margin">
			<form role="form" id="userForm" class="dataForm" action="<?= $_SERVER['REQUEST_URI'] ?>" method="post"  enctype="multipart/form-data">
				<div class="add_style">
					<ul> 
                        <li class="clearfix"><label class="label_name col-xs-1">所属分类：&nbsp;&nbsp;</label><div class="Add_content col-xs-9">
                            <select name="parent_id">
                                <option value="0">--选择分类--</option>
                                <?php foreach ($cateList as $key => $value) {?> 
                                <option value="<?php echo $value['id'];?>" <?php echo isset($dataList['parent_id']) && $dataList['parent_id']==$value['id']?'selected':''?>><?php echo $value['name'];?></option> 
                                <?php if(isset($value['_child']) && sizeof($value['_child'])>0){ foreach ($value['_child'] as $kk => $vv) {?> 
                                    <option value="<?php echo $vv['id'];?>" <?php echo isset($dataList['parent_id']) && $dataList['parent_id']==$vv['id']?'selected':''?>>&nbsp;---<?php echo $vv['name'];?></option>
                                <?php }}}?> 
                            </select></div>  
                        </li> 
						<li class="clearfix"><label class="label_name col-xs-1">名称：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="name" type="text"  class="col-xs-4" value="<?php echo isset($dataList['name'])?$dataList['name']:''?>" placeholder="名称"  datatype="*2-50" errormsg="名称至少2个字符,最多20个字符！"   ignore="ignore"/></div> </li>

						<li class="clearfix"><label class="label_name col-xs-1">链接：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="url" type="text"  class="col-xs-4" value="<?php echo isset($dataList['url'])?$dataList['url']:''?>" placeholder="url"/></div></li> 

						<li class="clearfix"><label class="label_name col-xs-1">排序：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="sort" type="text" value="<?php echo isset($dataList['sort'])?$dataList['sort']:'100'?>" datatype="n" errormsg="请填写数字！"   ignore="ignore" class="col-xs-1"/>（列表从小到大排列）</div>  
						</li> 
						<li class="clearfix">
							<label class="label_name col-xs-1">是否有效：&nbsp;&nbsp;</label>
							<div class="Add_content col-xs-9">
								<label class="l_f checkbox_time"><input type="checkbox" name="status" class="ace" id="checkbox" value="1"  <?php echo isset($dataList['status']) && $dataList['status']==0?'':'checked=checked'?>><span class="lbl">是</span></label> 
							</div> 
						</li> 
					</ul>
					<!--按钮操作-->
					<li class="clearfix">
						<div class="col-xs-2 col-lg-2">&nbsp;</div>
						<div class="col-xs-6">
							<input class="btn button_btn bg-deep-blue" type="submit" value="保存提交"/>
							<input name="reset" class="btn button_btn btn-gray" value="取消重置" type="reset">
							<a href="/admin/map/"  class="btn btn-info button_btn"><i class="fa fa-reply"></i> 返回上一步</a>
						</div>
					</li> 
					<input type="hidden" name="dosubmit" value="1"/>  
				</form>
			</div>
		</div> 
	<script type="text/javascript">  
		$(".dataForm").Validform({tiptype:3});  
	</script>