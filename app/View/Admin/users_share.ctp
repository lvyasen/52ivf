    <?php echo $this->Html->script('/admin/js/laydate/laydate.js');?>     
    <div class="margin Competence_style" id="page_style"> 
        <div class="operation clearfix">
            <ul class="choice_search">
                <form role="form" id="userForm" action="/admin/users_share" method="get">
                    <li class="clearfix col-xs-2 "><label class="label_name col-xs-1">标签：&nbsp;&nbsp;</label><div class="Add_content col-xs-1">
                        <select name="tag"> 
                        <option value="">---请选择---</option>
                            <?php foreach ($share_tags as $key => $value) {?> 
                        <option value="<?php echo $key;?>" <?php echo isset($_GET['tag']) && $_GET['tag']==$key?'selected':''?>><?php echo $value;?></option>  
                        <?php }?> 
                        </select></div>  
                    </li>  
                    <li class="clearfix col-xs-4 col-lg-5 col-ms-5 "><label class="label_name ">时间：</label> 
                        <input class="laydate-icon col-xs-4 col-lg-3" name="startTime" value="<?= @$_GET['startTime'] ?>" id="start" style=" margin-right:10px; height:28px; line-height:28px; float:left">
                        <span  style=" float:left; padding:0px 10px; line-height:32px;">至</span>
                        <input class="laydate-icon col-xs-4 col-lg-3" name="endTime" id="end" value="<?= @$_GET['endTime'] ?>" style="height:28px; line-height:28px; float:left">
                    </li> 
                    <input name="keywords" type="text" value="<?= @$_GET['keywords'] ?>" placeholder="标题" class="form-control"/>
                    <button class="btn button_btn bg-deep-blue " onclick="this.form.submit()"  type="button"><i class="fa  fa-search"></i>&nbsp;搜索</button> 
                    <a href="/admin/share_edit"  class="btn button_btn bg-deep-blue" title="添加分享"><i class="fa  fa-edit"></i>&nbsp;添加分享</a>    
                </form> 
            </ul>  
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr>  
                        <th>序号</th> 
                        <th>标题</th>   
                        <th>标签</th>  
                        <th>标签2</th>   
                        <th>城市</th>  
                        <th>用户</th> 
                        <th>排序</th>    
                        <th>评论数</th>    
                        <th>浏览数</th>    
                        <th>点赞数</th>   
                        <th>状态</th> 
                        <th>置顶</th>  
                        <th>发布时间</th>    
                        <th class="hidden-480">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataList as $key => $v) { ?>
                    <tr>      
                        <td><?php echo $v['Share']['id'];?></td> 
                        <td><a href="/share/info/<?php echo $v['Share']['id'];?>.html" target="_blank"><?php echo $v['Share']['title'];?></a></td> 
                        <td><?php echo isset($share_tags[$v['Share']['tag']])?$share_tags[$v['Share']['tag']]:"无";?></td> 
                        <td><?php echo isset($share_tags2[$v['Share']['tag2']])?$share_tags2[$v['Share']['tag2']]:"无";?></td> 
                        <td><?php echo $v['Share']['city_id'];?></td> 
                        <td><?php echo $v['Share']['uid'];?></td> 
                        <td><?php echo $v['Share']['sort'];?></td>  
                        <td><?php echo $v['Share']['comment_num'];?></td>  
                        <td><?php echo $v['Share']['browse_num'];?></td>  
                        <td><?php echo $v['Share']['agree_num'];?></td>  
                        <td><span  class="<?php echo $v['Share']['status']==1?'yesimg':'noimg';?>" onclick="toggle(this,'share', <?php echo $v['Share']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td> 
                        <td><span  class="<?php echo $v['Share']['is_top']==1?'yesimg':'noimg';?>" onclick="toggle(this,'share_top', <?php echo $v['Share']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>  
                        <td><?= date('Y-m-d H:i:s',strtotime($v['Share']['create_time'])) ?></td>  
                        <td class="td-manage"> 
                            <a title="编辑" href="/admin/obj_comment?ctype=50&cid=<?= $v['Share']['id']?>" class="btn button_btn bg-deep-blue">操作评论</a>
                            <a title="编辑" href="/admin/share_edit?id=<?= $v['Share']['id']?>" class="btn button_btn bg-deep-blue">编辑</a>
                           <!-- <a title="回复"  href="javascript:void(0)" onclick="editAct('添加评论',<?php echo $v['Share']['id'];?>)"  class="btn button_btn bg-deep-blue">操作回复</a>-->
                            <?php if($userRoles=='all'){?>   <a title="删除" href="javascript:;" onclick="delAct(this,'share',<?php echo $v['Share']['id'];?>,'single')" class="btn button_btn btn-danger">删除</a><?php }?>   
                        </td>
                    </tr>
                    <?php }?> 
                </tbody>
            </table>
            <?php echo $this->element('admin/pagination');?>
        </div>
    </div> 
<!--添加栏目-->
<div class="add_columns_style" id="add_columns_style" style=" display:none"> 
    <form action="/admin/replay_share" id="formEdit"  class="dataForm" role="form"  method="post"></form>
</div>  

<script type="text/javascript">

/******时间设置*******/
var start = {
    elem: '#start',
    format: 'YYYY-MM-DD',
   // min: laydate.now(), //设定最小日期为当前日期
    max: '2099-06-16', //最大日期
    istime: true,
    istoday: false,
    choose: function(datas){
         end.min = datas; //开始日选好后，重置结束日的最小日期
         end.start = datas //将结束日的初始值设定为开始日
     }
 };
 var end = {
    elem: '#end',
    format: 'YYYY-MM-DD',
    //min: laydate.now(),
    max: '2099-06-16',
    istime: true,
    istoday: false,
    choose: function(datas){
        start.max = datas; //结束日选好后，重置开始日的最大日期
    }
};
laydate(start);
laydate(end);
</script>