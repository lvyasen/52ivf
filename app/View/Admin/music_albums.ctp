    <?php echo $this->Html->script('/admin/js/jquery.dataTables.min.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.bootstrap.js');?>  
    <div class="margin Competence_style" id="page_style">
        <div class="operation clearfix"> 
            <a href="/admin/music_albums_edit"  class="btn button_btn bg-deep-blue" title="添加专辑"><i class="fa  fa-edit"></i>&nbsp;添加专辑</a>  
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr>  
                        <th>序号</th>
                        <th>分类</th>
                        <th>专辑封面</th>  
                        <th>专辑名称</th>  
                        <th>排序</th>  
                        <th>作者</th>  
                        <th>状态</th>  
                        <th>添加时间</th>  
                        <th class="hidden-480">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataList as $key => $v) { ?>
                    <tr>  
                        <td><?php echo $v['Album']['id'];?></td>
                        <td><?php echo $cateList[$v['Album']['parent_id']];?></td> 
                        <td><img src="<?php echo $v['Album']['img'];?>" width="50px"></td>  
                        <td><?php echo $v['Album']['name'];?></td> 
                        <td><?php echo $v['Album']['sort'];?></td> 
                        <td><?php echo $v['Album']['author'];?></td>  
                        <td><span  class="<?php echo $v['Album']['status']==1?'yesimg':'noimg';?>" onclick="toggle(this,'albums', <?php echo $v['Album']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td> 
                        <td><?= date('Y-m-d H:i:s',strtotime($v['Album']['create_time'])) ?></td>  
                        <td class="td-manage"> 
                            <a title="编辑" href="/admin/music_albums_edit?id=<?= $v['Album']['id']?>" class="btn button_btn bg-deep-blue">编辑</a>
                            <a title="删除" href="javascript:;" onclick="delAct(this,'album',<?php echo $v['Album']['id'];?>,'single')" class="btn button_btn btn-danger">删除</a>
                        </td>
                    </tr>
                    <?php }?> 
                </tbody>
            </table>
        </div>
    </div> 
<script type="text/javascript">  
var oTable1 = $('#data_table').dataTable( {
"bPaginate": true, 
"width":"100%", 
"bLengthChange":false,
"iDisplayLength": 20,
//"columns" : _tableCols, 
"bStateSave": false,//状态保存
"searching": false,
"aoColumnDefs": [{"orderable":false,"aTargets":[7]
}]

});
</script>
