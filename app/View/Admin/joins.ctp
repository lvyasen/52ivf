<?php echo $this->Html->script('/admin/js/ivf52/user.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.min.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.bootstrap.js');?>  
    <div class="margin Competence_style" id="page_style">
        <div class="operation clearfix"> 
            <a href="/admin/join_edit"  class="btn button_btn bg-deep-blue" title="发布招聘"><i class="fa  fa-edit"></i>&nbsp;发布招聘</a>  
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr>  
                        <th>序号</th>
                        <th>岗位</th>  
                        <th>学历</th>  
                        <th>经验</th>  
                        <th>薪资范围</th> 
                        <th>所属部门</th>  
                        <th>排序</th>  
                        <th>发布时间</th>  
                        <th class="hidden-480">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataList as $key => $v) { ?>
                    <tr>  
                        <td><?php echo $v['Join']['id'];?></td> 
                        <td><?php echo $v['Join']['title'];?></td> 
                        <td><?php echo $education[$v['Join']['Education']];?></td> 
                        <td><?php echo $experience[$v['Join']['Experience']];?></td>  
                        <td><?php echo $v['Join']['SalaryA']."-".$v['Join']['SalaryB'];?></td>  
                        <td><?php echo $v['Join']['Dept'];?></td>   
                        <td><?php echo $v['Join']['sort'];?></td>  
                        <td><?php echo $v['Join']['create_time'];?></td>   
                        <td class="td-manage"> 
                            <a title="编辑" href="/admin/join_edit?id=<?= $v['Join']['id']?>" class="btn button_btn bg-deep-blue">编辑</a><a title="删除" href="javascript:;" onclick="delAct(this,'joins',<?php echo $v['Join']['id'];?>,'single')" class="btn button_btn btn-danger">删除</a>
                        </td>
                    </tr>
                    <?php }?> 
                </tbody>
            </table>
        </div>
    </div> 
<script type="text/javascript">  
var oTable1 = $('#data_table').dataTable( {
"bPaginate": true, 
"width":"100%", 
"bLengthChange":false,
"iDisplayLength": 20,
//"columns" : _tableCols, 
"bStateSave": false,//状态保存
"searching": false,
"aoColumnDefs": [{"orderable":false,"aTargets":[8]
}]

});
</script>
