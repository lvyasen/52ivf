    
    <?php echo $this->Html->script('/admin/js/laydate/laydate.js');?>     
    <div class="margin" id="page_style">
        <div class="operation clearfix">
            <ul class="choice_search">
                <form role="form" id="userForm" action="/admin/hospital_project_log" method="get">  
                    <li class="clearfix col-xs-4 col-lg-5 col-ms-5 "><label class="label_name ">时间：</label> 
                        <input class="laydate-icon col-xs-4 col-lg-3" name="startTime" value="<?= @$_GET['startTime'] ?>" id="start" style=" margin-right:10px; height:28px; line-height:28px; float:left">
                        <span  style=" float:left; padding:0px 10px; line-height:32px;">至</span>
                        <input class="laydate-icon col-xs-4 col-lg-3" name="endTime" id="end" value="<?= @$_GET['endTime'] ?>" style="height:28px; line-height:28px; float:left">
                    </li> 
                    <input name="keywords" type="text" value="<?= @$_GET['keywords'] ?>" placeholder="项目名称" class="form-control"/>
                    <button class="btn button_btn bg-deep-blue " onclick="this.form.submit()"  type="button"><i class="fa  fa-search"></i>&nbsp;搜索</button>    
                </form> 
            </ul>
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                    <thead>
                        <tr>      
                            <th width="10%">序号</th> 
                            <th width="35%">项目</th> 
                            <th width="35%">消费用户</th> 
                            <th width="20%" class="hidden-480">消费时间</th>   
                        </tr>
                    </thead>
                    <tbody> 
                        <?php foreach ($dataList as $key => $v): ?>
                        <tr> 
                            <td><?= $v['HospitalProjectLog']['id'] ?></td> 
                            <td><?php echo $v['HospitalProjectLog']['pro_name'];?></td>  
                            <td><?= $v['HospitalProjectLog']['uid'] ?></td> 
                            <td><?= date('Y-m-d H:i:s',strtotime($v['HospitalProjectLog']['create_time'])) ?></td> 
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
                <?php echo $this->element('admin/pagination');?>
        </div>
    </div> 
<script type="text/javascript">

/******时间设置*******/
var start = {
    elem: '#start',
    format: 'YYYY-MM-DD',
   // min: laydate.now(), //设定最小日期为当前日期
    max: '2099-06-16', //最大日期
    istime: true,
    istoday: false,
    choose: function(datas){
         end.min = datas; //开始日选好后，重置结束日的最小日期
         end.start = datas //将结束日的初始值设定为开始日
     }
 };
 var end = {
    elem: '#end',
    format: 'YYYY-MM-DD',
    //min: laydate.now(),
    max: '2099-06-16',
    istime: true,
    istoday: false,
    choose: function(datas){
        start.max = datas; //结束日选好后，重置开始日的最大日期
    }
};
laydate(start);
laydate(end);
</script>