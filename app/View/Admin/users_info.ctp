<div class="margin Competence_style" id="page_style">
        <div class="operation clearfix">  
            <div class="search  clearfix"> 
                <form role="form" id="userForm" action="/admin/account_list" method="get">
                    <input name="username" type="text" value="<?= @$_GET['username'] ?>" placeholder="昵称/真实姓名/手机号" class="form-control col-xs-8"/><button class="btn button_btn bg-deep-blue " onclick="this.form.submit()" type="button"><i class="fa  fa-search"></i>&nbsp;搜索</button>
                </form>
            </div>
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr>  
                        <th>序号</th>
                        <th>昵称</th>
                        <th>真实姓名</th>   
                        <th>手机号</th>
                        <th>性别</th>
                        <th>年龄</th>   
                        <th>身份证号码</th>  
                        <th>IP</th>   
                        <th>状态</th>   
                        <th class="hidden-480">创建时间</th>   
                    </tr>
                </thead> 
                <tbody> 
                    <?php foreach ($dataList as $key => $v): ?>
                    <tr> 
                        <td><?= $v['User']['id'] ?></td> 
                        <td><?php echo $v['User']['nick_name'];?></td>
                        <td><?php echo $v['User']['user_name'];?></td> 
                        <td><?php echo $v['User']['mobile'];?></td> 
                        <td><?php echo $sex[$v['User']['sex']];?></td>  
                        <td><?php echo $v['User']['age'];?></td> 
                        <td><?php echo $v['User']['cert_num'];?></td> 
                        <td><?php echo $v['User']['ip'];?></td>  
                        <td><span  class="<?php echo $v['User']['status']==1?'yesimg':'noimg';?>" onclick="toggle(this,'account', <?php echo $v['User']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>  
                        <td><?= date('Y-m-d H:i:s',strtotime($v['User']['create_time'])) ?></td> 
                    </tr>
                    <?php endforeach ?>
                </tbody>
                <tbody>
                    <tr>
                        <td colspan="10">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="10" align="left">预约</td>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th>序号</th>
                        <th>病症</th>
                        <th>年龄</th>
                        <th>意向医院</th>
                        <th>意向医生</th>
                        <th>提交来源</th>
                        <th>提交平台</th>
                        <th>是否联系</th>
                        <th>IP</th>
                        <th>创建时间</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($yuy as $value):?>
                    <tr>
                        <td><?=$value['Users_appoint']['id'] ?></td>
                        <td><?php
                            switch ($value['Users_appoint']['types']){
                            case '0':echo "我不清楚症状";break;
                            case '10':echo "染色体异常";break;
                            case '20':echo "精子问题";break;
                            case '30':echo "卵巢问题";break;
                            case '40':echo "子宫问题";break;
                            case '50':echo "输卵管问题";break;
                            case '60':echo "胎停流产";break;
                            case '70':echo "不明原因不孕";break;
                            }?></td>
                        <td><?=$value['Users_appoint']['age'] ?></td>
                        <td><?=$value['Users_appoint']['hospital_id'] ?></td>
                        <td><?=$value['Users_appoint']['doctor_id'] ?></td>
                        <td><?=$value['Users_appoint']['source'] ?></td>
                        <td><?=$value['Users_appoint']['system'] ?></td>
                        <td><span  class="<?php echo $value['Users_appoint']['status']==1?'yesimg':'noimg';?>" onclick="toggle(this,'appoint', <?php echo $value['Users_appoint']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                        <td><?=$value['Users_appoint']['ip'] ?></td>
                        <td><?= date('Y-m-d H:i:s',strtotime($value['Users_appoint']['create_time'])) ?></td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
                <tbody>
                    <tr>
                        <td colspan="10">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="10" align="left">攻略</td>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th>序号</th>
                        <th colspan="8">标题</th>
                        <th colspan="1">创建时间</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($article_text as $value):?>
                    <tr>
                        <td><?=$value['Article']['id']?></td>
                        <td colspan="8"><a href="/plan/info/<?php echo $value['Article']['id'];?>"><?=$value['Article']['title']?></a></td>
                        <td colspan="1"><?=($value['Article']['create_time']=="无")?"无":date('Y-m-d H:i:s',strtotime($value['Article']['create_time'])) ?></td>
                   </tr>
                    <?php endforeach;?>
                </tbody>
                <tbody>
                    <tr>
                        <td colspan="10">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="10" align="left">分享</td>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th>序号</th>
                        <th colspan="8">标题</th>
                        <th colspan="1">创建时间</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($share_text as $value):?>
                    <tr>
                        <td><?=$value['Share']['id']?></td>
                        <td colspan="8"><a href="/share/info/<?php echo $value['Share']['id'];?>"><?=$value['Share']['title']?></a></td>
                        <td colspan="1"><?=($share_text['Share']['create_time']='无')?"无":date('Y-m-d H:i:s',strtotime($value['Share']['create_time'])) ?></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
                <tbody>
                    <tr>
                        <td colspan="10">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="10" align="left">问答</td>
                    </tr>
                </tbody> 
               <thead>
                    <tr>
                        <th>序号</th>
                        <th colspan="8">标题</th>
                        <th colspan="1">创建时间</th>
                    </tr>
                </thead>
                <tbody>
               <?php foreach ($answer_text as $value):?>
                    <tr>
                        <td><?=$value['Share']['id']?></td>
                        <td colspan="8"><a href="/answer/info/<?php echo $value['Share']['id']?>"><?=$value['Share']['content']?></a></td>
                        <td colspan="1"><?=($answer_text['Share']['create_time']='无')?"无":date('Y-m-d H:i:s',strtotime($value['Share']['create_time'])) ?></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
               <tbody>
                    <tr>
                        <td colspan="10">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="10" align="left">评论</td>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th>序号</th>
                        <th colspan="8">评论内容</th>
                        <th colspan="1">创建时间</th>
                    </tr>
                </thead>
                <tbody>
               <?php foreach ($comment_text as $value):?>
                    <tr>
                        <td><?=$value['Comment']['id']?></td>
                        <td colspan="8"><?=$value['Comment']['content']?></td>
                        <td colspan="1"><?=($value['Comment']['create_time']='无')?"无":date('Y-m-d H:i:s',strtotime($value['Comment']['create_time'])) ?></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table> 
        </div>
    </div>    