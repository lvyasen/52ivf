 
    <!--复文本编辑框--> 
    <?php echo $this->Html->script('/admin/js/ueditor/ueditor.config.js');?>  
    <?php echo $this->Html->script('/admin/js/ueditor/ueditor.all.js?20171213');?>  
    <?php echo $this->Html->script('/admin/js/ueditor/lang/zh-cn/zh-cn.js');?>    
        <div class="margin">
            <form role="form" id="userForm" class="dataForm" action="<?= $_SERVER['REQUEST_URI'] ?>" method="post"  enctype="multipart/form-data">
                <div class="add_style">
                <ul>      
					<li class="clearfix"><label class="label_name col-xs-1">专题名称：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="title" type="text"  class="col-xs-4" value="<?php echo isset($dataList['title'])?$dataList['title']:''?>" placeholder="名称"  datatype="*2-50" errormsg="名称至少2个字符,最多50个字符！"   ignore="ignore"/></div> </li>  

				    <li class="clearfix"><label class="label_name col-xs-1">meta_title：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="meta_title" type="text"  class="col-xs-11" value="<?php echo isset($dataList['meta_title'])?$dataList['meta_title']:''?>" placeholder="meta_title"  datatype="*2-50" errormsg="meta_title至少2个字符,最多20个字符！"  nullmsg="请填写meta_title！"/></div> </li>

				    <li class="clearfix"><label class="label_name col-xs-1">meta_keywords：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="meta_keywords" type="text"  class="col-xs-11" value="<?php echo isset($dataList['meta_keywords'])?$dataList['meta_keywords']:''?>" placeholder="meta_keywords"  datatype="*2-50" errormsg="meta_keywords至少2个字符,最多20个字符！"  nullmsg="请填写meta_keywords！"/></div> </li> 

				    <li class="clearfix"><label class="label_name col-xs-1">meta_description：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="meta_description" type="text"  class="col-xs-11" value="<?php echo isset($dataList['meta_description'])?$dataList['meta_description']:''?>" placeholder="meta_description"  datatype="*2-250" errormsg="meta_description至少2个字符,最多20个字符！"  nullmsg="请填写meta_description！"/></div> </li>  
                    <li class="clearfix"><label class="label_name col-xs-1"><i>*</i>自定义内容：&nbsp;&nbsp;</label>
                        <div class="Add_content col-xs-11"><textarea name="content"  id="content"  style="width:100%;height:500px;"><?php echo isset($dataList['content'])?$dataList['content']:''?></textarea></div>
                    </li>   
                </ul>
                <!--按钮操作-->
                <li class="clearfix">
                    <div class="col-xs-2 col-lg-2">&nbsp;</div>
                    <div class="col-xs-6">
                        <input class="btn button_btn bg-deep-blue" type="submit" value="保存提交"/>
                        <input name="reset" class="btn button_btn btn-gray" value="取消重置" type="reset"> 
                    </div>
                </li> 
                <input type="hidden" name="dosubmit" value="1"/>  
            </form> 
        </div>
    </div>  
<script type="text/javascript">  
$(function(){ 
    UE.getEditor('content');  
}); 
</script> 