<?php echo $this->Html->script('/admin/js/laydate/laydate.js');?>   
    <div class="margin Competence_style" id="page_style">
        <div class="operation clearfix">   
                <ul class="choice_search">
                <form role="form" id="userForm" action="/admin/account_appoint" method="get">
                    <li class="clearfix col-xs-4 col-lg-5 col-ms-3 "><label class="label_name ">时间：</label> 
                        <input class="laydate-icon col-xs-4 col-lg-3" name="startTime" value="<?= @$_GET['startTime'] ?>" id="start" style=" margin-right:10px; height:28px; line-height:28px; float:left">
                        <span  style=" float:left; padding:0px 10px; line-height:32px;">至</span>
                        <input class="laydate-icon col-xs-4 col-lg-3" name="endTime" id="end" value="<?= @$_GET['endTime'] ?>" style="height:28px; line-height:28px; float:left">
                    </li>
                    <li class="clearfix col-xs-4 col-lg-5 col-ms-3 ">
                    <input name="username" type="text" value="<?= @$_GET['username'] ?>" placeholder="真实姓名/手机号" class="form-control col-xs-8"/>
                    </li>
                    <button class="btn button_btn bg-deep-blue " onclick="this.form.submit()" type="button"><i class="fa  fa-search"></i>&nbsp;搜索</button> 
                    <a href="/admin/account_appoint/?import=1&startTime=<?= @$_GET['startTime'] ?>&endTime=<?= @$_GET['endTime'] ?>&username=<?= @$_GET['username'] ?>"  class="btn button_btn bg-deep-blue" title="导出">&nbsp;导出</a> 
                </form>
                </ul> 
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr>   
                        <th>序号</th>
                        <th>病症</th>
                        <th>真实姓名</th>   
                        <th>手机号</th> 
                        <th>年龄</th>   
                        <th>意向医院</th> 
                        <th>意向医生</th> 
                        <th>描述</th>  
                        <th>提交来源</th>   
                        <th>提交平台</th>   
                        <th>设备</th>   
                        <th>是否联系</th>    
                        <th>ip</th>   
                        <th class="hidden-480">创建时间</th>   
                    </tr>
                </thead> 
                <tbody> 
                    <?php foreach ($dataList as $key => $v): ?>
                    <tr> 
                        <td><?= $v['UsersAppoint']['id'] ?></td> 
                        <td><?php echo $v['UsersAppoint']['bingzheng'];?></td>
                        <td><?php echo $v['UsersAppoint']['user_name'];?></td> 
                        <td><?php echo $v['UsersAppoint']['mobile'];?></td>  
                        <td><?php echo $v['UsersAppoint']['age'];?></td>  
                        <td><?php echo $v['UsersAppoint']['hospital_id'];?></td> 
                        <td><?php echo $v['UsersAppoint']['doctor_id'];?></td>  
                        <td><?php echo $v['UsersAppoint']['description'];?></td>  
                        <td>
                            <?php if(empty($v['UsersAppoint']['source_url'])){ 
                                echo $v['UsersAppoint']['source'];
                            }else{
                            ?> 
                            <a href="<?php echo $v['UsersAppoint']['source_url'];?>" target="_blank"><?php echo $v['UsersAppoint']['source'];?></a>
                            <?php }?>
                        </td> 
                        <td><?php echo $v['UsersAppoint']['system'];?></td> 
                        <td><?php echo $v['UsersAppoint']['brand'];?></td> 
                        <td><span  class="<?php echo $v['UsersAppoint']['status']==1?'yesimg':'noimg';?>" onclick="toggle(this,'appoint', <?php echo $v['UsersAppoint']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>  
                        <td><?php echo $v['UsersAppoint']['ip'];?></td>  
                        <td><?php echo $v['UsersAppoint']['create_time'];?></td> 
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table> 
            <?php echo $this->element('admin/pagination');?>
        </div>
    </div>    

<script type="text/javascript"> 
/******时间设置*******/ 
laydate({elem: '#start', istime: true, format: 'YYYY-MM-DD'});  
laydate({elem: '#end', istime: true, format: 'YYYY-MM-DD'});  
</script>