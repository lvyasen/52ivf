   
    <?php echo $this->Html->script('/admin/js/ueditor/ueditor.config.js');?>  
    <?php echo $this->Html->script('/admin/js/ueditor/ueditor.all.min.js');?>  
    <?php echo $this->Html->script('/admin/js/ueditor/lang/zh-cn/zh-cn.js');?>    
    <?php echo $this->Html->script('/admin/js/plupload/plupload.full.min.js');?>    
    <link rel="stylesheet" type="text/css" href="/admin/js/plupload/css/common.css">
    <link rel="stylesheet" type="text/css" href="/admin/js/plupload/css/index.css"> 
    <?php echo $this->Html->script('/admin/js/ivf52/pic.js');?>
		<div class="margin">
			<form role="form" id="userForm" class="dataForm" action="<?= $_SERVER['REQUEST_URI'] ?>" method="post"   enctype="multipart/form-data">
				<div class="add_style">
					<ul> 
				        <li class="clearfix"><label class="label_name col-xs-1">封面图：&nbsp;&nbsp;</label>
				             <span class="cont_style col-xs-9">        
				               <div id="preview" class="preview_img"><img id="imghead" width="190" src="<?php echo isset($dataList['image']) && !empty($dataList['image'])?$dataList['image']:'/admin/images/image.png';?>" border="0"></div>
				               <div class="fileInput ">
				                   <input onchange="previewImage(this)" name="imgFile" id="upfile" class="upfile" type="file">
				                <input class="upFileBtn" value="上传图片" onclick="document.getElementById('upfile').click()" type="button">
				                </div>
				              </span>
				          </li> 
				        <li class="clearfix"><label class="label_name col-xs-1">手机详情封面图：&nbsp;&nbsp;</label>
				             <span class="cont_style col-xs-9">        
				               <div id="preview2" class="preview_img"><img id="imghead" width="190" src="<?php echo isset($dataList['mb_image']) && !empty($dataList['mb_image'])?$dataList['mb_image']:'/admin/images/image.png';?>" border="0"></div>
				               <div class="fileInput ">
				                   <input onchange="PreviewImage(this,'preview2')" name="imgFile2" id="upfile2" class="upfile" type="file">
				                <input class="upFileBtn" value="上传图片" onclick="document.getElementById('upfile2').click()" type="button">
				                </div>
				              </span>
				         </li> 
						       
                        <li class="clearfix"><label class="label_name col-xs-1"><i>*</i>中文名称：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="cn_name" type="text"  class="col-xs-4" value="<?php echo isset($dataList['cn_name'])?$dataList['cn_name']:''?>" placeholder="中文名称"  datatype="*2-100" errormsg="名称至少2个字符,最多100个字符！"  nullmsg="请填写中文名称！"/></div> </li>  

                        <li class="clearfix"><label class="label_name col-xs-1"><i>*</i>英文名称：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="en_name" type="text"  class="col-xs-4" value="<?php echo isset($dataList['en_name'])?$dataList['en_name']:''?>" placeholder="英文名称"  datatype="*2-100" errormsg="名称至少2个字符,最多100个字符！"  nullmsg="请填写英文名称！"/></div> </li> 
 

                        <li class="clearfix"><label class="label_name col-xs-1"><i>*</i>医院等级：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="lever" type="text"  class="col-xs-4" value="<?php echo isset($dataList['lever'])?$dataList['lever']:''?>" placeholder="医院等级"  datatype="*2-50" errormsg="医院等级至少2个字符,最多20个字符！"  nullmsg="请填写医院等级！"/>(如：三级甲等) </div> </li> 


                        <li class="clearfix"><label class="label_name col-xs-1">医院环境图：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"> 
                           <section class=" img-section">
                            <p class="up-p"><span class="up-span">最多可以上传6张图片，马上上传</span></p>
                            <div class="z_photo upimg-div clear"  id="divContent">
                                     <section class="z_file fl" id="btnMultipleUpload">
                                        <img src="/admin/js/plupload/img/a11.png" tppabs="/admin/js/plupload/img/a11.png" class="add-img">
                                        <input type="file" name="file" id="file" class="file" value="" accept="image/jpg,image/jpeg,image/png,image/bmp" multiple />
                                     </section>
                                    <?php if(!empty($picData)){ foreach ($picData as $key => $value) { ?>
                                       <section class="up-section fl add_pic" id="pic_<?php echo $key;?>"> 
                                            <span class="up-span"></span>
                                            <img src="/admin/js/plupload/img/a7.png" class="close-upimg" onclick="_pic.DeleteFile(<?php echo $value['Picture']['id'];?>,'pic_<?php echo $key;?>')"> 
                                            <img src="<?php echo $value['Picture']['thumb'];?>" class="up-img">  
                                        </section> 
                                    <?php }}?>
                             </div>
                         </section>
                        </li> 

				        <li class="clearfix"><label class="label_name col-xs-1">医院评分：&nbsp;&nbsp;</label><div class="Add_content col-xs-9">
				        	<select name="score"> 
				            <?php foreach ($score as $key => $value) {?> 
				            <option value="<?php echo $key;?>" <?php echo isset($dataList['score']) && $dataList['score']==$key?'selected':''?>><?php echo $value;?></option>  
				            <?php }?> 
				        	</select></div>  
				    	</li>
				        <li class="clearfix"><label class="label_name col-xs-1">医院区域：&nbsp;&nbsp;</label><div class="Add_content col-xs-9">
				        	<select name="city_id"> 
				            <?php foreach ($districtList as $key => $value) {?> 
				            <option value="<?php echo $value['District']['id'];?>" <?php echo isset($dataList['city_id']) && $dataList['city_id']==$value['District']['id']?'selected':''?>><?php echo $value['District']['country'];?></option>  
				            <?php }?> 
				        	</select></div>  
				    	</li>
                        <li class="clearfix"><label class="label_name col-xs-1">口碑分：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="public_praise" type="text"  class="col-xs-4" value="<?php echo isset($dataList['public_praise'])?$dataList['public_praise']:''?>" placeholder="口碑分" /></div></li> 


                        <li class="clearfix"><label class="label_name col-xs-1">人气分：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="popularity" type="text"  class="col-xs-4" value="<?php echo isset($dataList['popularity'])?$dataList['popularity']:''?>" placeholder="人气分" /></div></li>  

                        <li class="clearfix"><label class="label_name col-xs-1">活跃分：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="active" type="text"  class="col-xs-4" value="<?php echo isset($dataList['active'])?$dataList['active']:''?>" placeholder="活跃分" /></div></li> 

                        <li class="clearfix"><label class="label_name col-xs-1">预约数：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="appoint_num" type="text"  class="col-xs-4" value="<?php echo isset($dataList['appoint_num'])?$dataList['appoint_num']:''?>" placeholder="预约数" /></div></li>  
                        
                        <li class="clearfix"><label class="label_name col-xs-1">案例数：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="case_num" type="text"  class="col-xs-4" value="<?php echo isset($dataList['case_num'])?$dataList['case_num']:''?>" placeholder="案例数" /></div></li> 

                        <li class="clearfix"><label class="label_name col-xs-1">关注数：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="follow_num" type="text"  class="col-xs-4" value="<?php echo isset($dataList['follow_num'])?$dataList['follow_num']:''?>" placeholder="关注数" /></div></li> 

                        <li class="clearfix"><label class="label_name col-xs-1">推荐数：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="rec_num" type="text"  class="col-xs-4" value="<?php echo isset($dataList['rec_num'])?$dataList['rec_num']:''?>" placeholder="推荐数" /></div></li> 
    
                        <li class="clearfix"><label class="label_name col-xs-1">医院环境评分：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="environment_score" type="text"  class="col-xs-4" value="<?php echo isset($dataList['environment_score'])?$dataList['environment_score']:''?>" placeholder="医院环境评分" /></div></li>  
                        <li class="clearfix"><label class="label_name col-xs-1">医疗技术评分：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="skill_score" type="text"  class="col-xs-4" value="<?php echo isset($dataList['skill_score'])?$dataList['skill_score']:''?>" placeholder="医疗技术评分" /></div></li>  
                        <li class="clearfix"><label class="label_name col-xs-1">服务评分：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="service_score" type="text"  class="col-xs-4" value="<?php echo isset($dataList['service_score'])?$dataList['service_score']:''?>" placeholder="服务评分" /></div></li>  
                        <li class="clearfix"><label class="label_name col-xs-1">综合指数评分：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="composite_score" type="text"  class="col-xs-4" value="<?php echo isset($dataList['composite_score'])?$dataList['composite_score']:''?>" placeholder="综合指数评分" /></div></li>  

                        <li class="clearfix"><label class="label_name col-xs-1">导师服务：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="tutor_service" type="text"  class="col-xs-4" value="<?php echo isset($dataList['tutor_service'])?$dataList['tutor_service']:''?>" placeholder="导师服务" /></div></li> 
                        <li class="clearfix"><label class="label_name col-xs-1">候诊时间：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="waiting_time" type="text"  class="col-xs-4" value="<?php echo isset($dataList['waiting_time'])?$dataList['waiting_time']:''?>" placeholder="推荐数" /></div></li> 
                        <li class="clearfix"><label class="label_name col-xs-1">擅长项目：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="projects" type="text"  class="col-xs-4" value="<?php echo isset($dataList['projects'])?$dataList['projects']:''?>" placeholder="擅长项目" />（逗号分隔，如：眼部,鼻部,面部轮廓）</div></li> 
                       

                        <li class="clearfix"><label class="label_name col-xs-1">简介：&nbsp;&nbsp;</label><span class="Add_content col-xs-9"><textarea name="introduction" class="form-control col-xs-12 col-sm-5" id="form_textarea" placeholder=""><?php echo isset($dataList['introduction'])?$dataList['introduction']:''?></textarea></span></li>


                        <li class="clearfix"><label class="label_name col-xs-1">描述：&nbsp;&nbsp;</label><span class="Add_content col-xs-9"><textarea name="description" class="form-control col-xs-12 col-sm-5" id="form_textarea" placeholder=""><?php echo isset($dataList['description'])?$dataList['description']:''?></textarea></span></li> 


                        <li class="clearfix"><label class="label_name col-xs-1">人均价格：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="price" type="text"  class="col-xs-4" value="<?php echo isset($dataList['price'])?$dataList['price']:''?>" placeholder="人均价格" /></div></li> 
                     <!--
                    <li class="clearfix"><label class="label_name col-xs-1"><i>*</i>规则：&nbsp;&nbsp;</label>
                        <div class="Add_content col-xs-11"><textarea name="rule"  id="rule"  style="width:100%;height:500px;"><?php echo isset($dataList['rule'])?stripslashes($dataList['rule']):''?></textarea></div>
                    </li> -->
                    <li class="clearfix"><label class="label_name col-xs-1">自定义模块1：&nbsp;&nbsp;</label>
                        <div class="Add_content col-xs-11"><textarea name="custom_text1"  id="custom_text1"  style="width:100%;height:500px;"><?php echo isset($dataList['custom_text1'])?stripslashes($dataList['custom_text1']):''?></textarea></div>
                    </li>
                    <li class="clearfix"><label class="label_name col-xs-1">自定义模块2：&nbsp;&nbsp;</label>
                        <div class="Add_content col-xs-11"><textarea name="custom_text2"  id="custom_text2"  style="width:100%;height:500px;"><?php echo isset($dataList['custom_text2'])?stripslashes($dataList['custom_text2']):''?></textarea></div>
                    </li>
                    <li class="clearfix"><label class="label_name col-xs-1"><i>*</i>收费项目：&nbsp;&nbsp;</label>
                        <div class="Add_content col-xs-11"><textarea name="object"  id="object"  style="width:100%;height:500px;"><?php echo isset($dataList['object'])?stripslashes($dataList['object']):''?></textarea></div>
                    </li> 


						<li class="clearfix"><label class="label_name col-xs-1">排序：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="sort" type="text" value="<?php echo isset($dataList['sort'])?$dataList['sort']:'100'?>" datatype="n" errormsg="请填写数字！"   ignore="ignore" class="col-xs-1"/>（列表从小到大排列）</div>  
						</li> 
						<li class="clearfix">
							<label class="label_name col-xs-1">是否有效：&nbsp;&nbsp;</label>
							<div class="Add_content col-xs-9">
								<label class="l_f checkbox_time"><input type="checkbox" name="status" class="ace" id="checkbox" value="1"  <?php echo isset($dataList['status']) && $dataList['status']==0?'':'checked=checked'?>><span class="lbl">是</span></label> 
							</div> 
						</li> 
					</ul>
					<!--按钮操作-->
					<li class="clearfix">
						<div class="col-xs-2 col-lg-2">&nbsp;</div>
						<div class="col-xs-6">
							<input class="btn button_btn bg-deep-blue" type="submit" value="保存提交"/>
							<input name="reset" class="btn button_btn btn-gray" value="取消重置" type="reset">
							<a href="/admin/hospital/"  class="btn btn-info button_btn"><i class="fa fa-reply"></i> 返回上一步</a>
						</div>
					</li> 
					<input type="hidden" name="dosubmit" value="1"/>  
				</form>
			</div>
		</div> 
<script type="text/javascript">  
$(function(){  
    UE.getEditor('custom_text2'); 
    UE.getEditor('custom_text1'); 
    $(".dataForm").Validform({tiptype:3});  

	var MAX_FILE_NUM = 6;   
	var multi = new plupload.Uploader({
	    browse_button: 'btnMultipleUpload', // this can be an id of a DOM element or the DOM element itself 
    	url: '/adminajax/file?ajaxdata=hospit', 
	    max_file_size: '10mb',
	    chunk_size: '3mb',
	    filters: {
	        mime_types: [
	          { title: "Image files", extensions: "BMP,jpg,JPEG,gif,png" }
	        ]
	    },
	    runtimes: 'html5',
	    multi_selection: true
	});

	multi.init();

	multi.bind('FilesAdded', function (up, files) { 
	    var uptLenth=$('.add_pic').size();   
	    if(((uptLenth-1)+files.length) >= MAX_FILE_NUM){ 
	         layer.msg('最多添加6张',{icon:0,time:1000});  
	         multi.splice();
	         multi.refresh();
	        return;
	    }else{ 
	        layer.load(1); 
	        plupload.each(files, function (file) {     
	            $('#divContent').append('<section class="up-section fl add_pic" id="pic_'+file.id+'"></section>');
	        });
	        multi.start();
	    }
	});  
	multi.bind('FileUploaded', function (up, file, resp) { 
	    var json = JSON.parse(resp.response); 
	    $('#pic_' + file.id).html('<span class="up-span"></span><img src="/admin/js/plupload/img/a7.png" class="close-upimg" onclick="_pic.DeleteFile(' + json.data.id + ',\'pic_' + file.id + '\')"><img class="up-img" src="' + json.data.url + '"/><input type="hidden"  name="UptPicture[]" value="'+json.data.id+'" />');
	    layer.closeAll('loading');
	});

	multi.bind('Error', function (up, err) {   
	    layer.msg("Error #" + err.code + ": " + err.message,{icon:0,time:1000});  
	}); 

});
</script>