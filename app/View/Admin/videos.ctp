<?php echo $this->Html->script('/admin/js/ivf52/user.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.min.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.bootstrap.js');?>  
    <div class="margin Competence_style" id="page_style">
        <div class="operation clearfix"> 
            <a href="/admin/video_edit"  class="btn button_btn bg-deep-blue" title="添加视频"><i class="fa  fa-edit"></i>&nbsp;添加视频</a>  
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr>  
                        <th>序号</th>
                        <th>标题</th>
                        <th>封面图</th>
                        <th>分类</th>  
                        <th>标签</th>  
                        <th>播放数</th> 
                        <th>搜索数</th> 
                        <th>排序</th> 
                        <th>推荐</th>  
                        <th>状态</th>    
                        <th>上传时间</th>  
                        <th class="hidden-480">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataList as $key => $v) { ?>
                    <tr>  
                        <td><?php echo $v['Video']['id'];?></td>  
                        <td><?php echo $v['Video']['title'];?></td> 
                        <td><img src="<?php echo $v['Video']['img'];?>" width="50px"></td>  
                        <td><?php echo $v['Video']['cate_name'];?></td>   
                        <td><?php echo $v['Video']['tags'];?></td>   
                        <td><?php echo $v['Video']['play_num'];?></td>  
                        <td><?php echo $v['Video']['search_num'];?></td>  
                        <td><?php echo $v['Video']['sort'];?></td>  
                        <td><span  class="<?php echo $v['Video']['rec']==1?'yesimg':'noimg';?>" onclick="toggle(this,'video_rec', <?php echo $v['Video']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td> 
                        <td><span  class="<?php echo $v['Video']['status']==1?'yesimg':'noimg';?>" onclick="toggle(this,'video', <?php echo $v['Video']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td> 

                        <td><?= date('Y-m-d H:i:s',strtotime($v['Video']['create_time'])) ?></td> 
                        <td class="td-manage"> 
                            <a title="编辑" href="/admin/account_comment?ctype=40&cid=<?= $v['Video']['id']?>" class="btn button_btn bg-deep-blue">查看评论</a>
                            <a title="编辑" href="/admin/video_edit?id=<?= $v['Video']['id']?>" class="btn button_btn bg-deep-blue">编辑</a><a title="删除" href="javascript:;" onclick="delAct(this,'video',<?php echo $v['Video']['id'];?>,'single')" class="btn button_btn btn-danger">删除</a>
                        </td>
                    </tr>
                    <?php }?> 
                </tbody>
            </table>
        </div>
    </div> 
<script type="text/javascript">  
var oTable1 = $('#data_table').dataTable( {
"bPaginate": true, 
"width":"100%", 
"bLengthChange":false,
"iDisplayLength": 20,
//"columns" : _tableCols, 
"bStateSave": false,//状态保存
"searching": false,
"aoColumnDefs": [{"orderable":false,"aTargets":[2,11]
}]

});
</script>
