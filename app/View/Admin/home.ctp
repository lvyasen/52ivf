<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">  
	<?php echo $this->Html->css('/admin/report/css/bootstrap.min.css?20171112'); ?>
	<?php echo $this->Html->css('/admin/report/css/base.css?20171112'); ?>
	<?php echo $this->Html->css('/admin/report/css/style.css?20171112'); ?> 
	<?php echo $this->Html->script('/admin/js/jquery-1.9.1.min.js');?> 
	<?php echo $this->Html->script('/admin/js/bootstrap.min.js');?>
	<?php echo $this->Html->script('/admin/js/layer/layer.js');?>   
	<?php echo $this->Html->script('/admin/report/js/echarts.min.js');?>   
	<?php echo $this->Html->script('/admin/report/js/newSetting.js?20171112');?>   
	<?php echo $this->Html->script('/admin/js/laydate/laydate.js');?>   
	<title>首页</title>   
</head>
<body>
<!-- 顶部导航 -->
<div class="wrapper">  
	<div class="gray-bg dashboard"  style="padding-bottom:40px"> 
		<div class="row border-bottom">
			<div class="col-sm-12">
				<div class="box float-e-margins">
					<div class="box-title">
						<h5>平台数据总览</h5>
						<small class="pull-right"><span style="float:right;height: 100%"><label class="label_name ">&nbsp;&nbsp;<button class="btn" style="background:#438eb9 none repeat scroll 0 0;color:#fff;font-size: 12px;height: 28px;"  type="button"  id="seachData">搜索</button></label> 
							<input class="laydate-icon col-xs-4 col-lg-4" name="startTime" value="" id="start8" style=" margin-right:10px; height:28px; line-height:28px; float:left">
							<span  style=" float:left; padding:0px 10px; line-height:32px;">至</span>
							<input class="laydate-icon col-xs-4 col-lg-4" name="endTime" id="end8" value="" style="height:28px; line-height:28px; float:left">
						</span></small> </div>
						<div class="box-content">
							<div class="row text-center" id="splashSummary">
								<div class="col-sm-3">
									<h1 class="num" id="totaluv"><?php echo $dataList['totaluv'];?></h1>
									<small>UV总计</small> </div>
									<div class="col-sm-3">
										<h1 class="num" id="regUser"><?php echo $dataList['regUser']?></h1>
										<small>注册用户（人）</small> </div>
										<div class="col-sm-3"> 
											<h1 class="num" id="txUser"><?php echo $dataList['txUser']?></h1>
											<small>预约用户（人）</small> </div> 
										</div>
										<hr /> 
									</div>
								</div>
							</div>
						</div> 
						<div class="row">
							<div class="col-sm-12">
								<div class="col-lg-12 col-sm-6">
									<div class="box float-e-margins">
										<div class="box-title">
											<h5>平台数据趋势</h5>
											<span style="float:right;height: 100%">
												<span style="float:left;"><select id="squdao" name="q">
													<option value="">---选择渠道商---</option>
													<?php foreach ($qudaoList as $key => $value) {?> 
													<option value="<?php echo $value['Yonghuqudao']['yonghuqudaohao'];?>" ><?php echo $value['Yonghuqudao']['title'];?></option>
													<?php }?> 
												</select>
											</span>&nbsp;&nbsp;
											<label class="label_name "><input class="laydate-icon col-xs-4 col-lg-5" name="startTime" value="" id="start" style=" margin-right:10px; height:28px; line-height:28px; float:left">
												&nbsp;&nbsp;<button class="btn" style="background:#438eb9 none repeat scroll 0 0;color:#fff;font-size: 12px;height: 28px;"  type="button" id="seachData1">搜索</button></label> 

											</span>
										</div>
										<div class="box-content"> 
											<div id="shujuqushi" style="width: 100%;height:400px;"></div>
										</div> 
										<?php  
										$attrData=[];
										foreach ($reportUV as $key => $value) { 
											$attrData['uv'][]=$value['count']; 
											$attrData['time'][]=$value['dateTime']; 
										}
										foreach ($reportUserReg as $key => $value) { 
											$attrData['reg'][]=$value['count']; 
										}
										foreach ($reportApply as $key => $value) { 
											$attrData['applyProds'][]=$value['count']; 
										}
										?>  
										<div style="display:none" id="qudaoUvData"><?php echo json_encode($attrData['uv']);?></div> 
										<div style="display:none" id="qudaoregData"><?php echo json_encode($attrData['reg']);?></div> 
										<div style="display:none" id="applyProdsData"><?php echo json_encode($attrData['applyProds']);?></div> 
										<div style="display:none" id="uvtimeData"><?php echo json_encode($attrData['time']);?></div>  
									</div>
								</div> 
							</div>  
						</div> 
					</br>  
					<div class="row">
						<div class="col-sm-12">
							<div class="col-lg-6 col-sm-6">
								<div class="box float-e-margins">
									<div class="box-title">
										<span style="float:right;height: 100%">
											<span style="float:left;"><select id="qqudao" name="qudao">
												<option value="">---选择渠道商---</option>
												<?php foreach ($qudaoList as $key => $value) {?> 
												<option value="<?php echo $value['Yonghuqudao']['yonghuqudaohao'];?>" ><?php echo $value['Yonghuqudao']['title'];?></option>
												<?php }?> 
											</select>
											<h5>渠道UV Top10&nbsp;</h5>
										</span>
										&nbsp;&nbsp;<span style="float:right;width:50%"><label class="label_name "  style="float:right;">&nbsp;&nbsp;<button class="btn" style="background:#438eb9 none repeat scroll 0 0;color:#fff;font-size: 12px;height: 28px;"  type="button" id="seachData4">搜索</button></label>  
										<input class="laydate-icon col-lg-4" placeholder="开始时间"  name="startTime" value="" id="start3" style=" margin-right:10px; height:28px; line-height:28px; float:left"> 
										<span  style=" float:left; padding:0px 10px; line-height:32px;">至</span>
										<input class="laydate-icon col-lg-4" placeholder="截止时间"  name="endTime" id="end3" value="" style="height:28px; line-height:28px; float:left"> 
									</span>
								</span>
							</div>
							<div class="box-content">
								<div id="qudaoUvstop10" style="width: 100%;height:400px;"></div> 
							</div> 
							<?php 
							$attrName=[];
							$attrData=[];
							foreach ($qudaoUvTop as $key => $value) {
								$attrName[]=$value['title'];
								$attrData[$key]['name']=$value['title'];
								$attrData[$key]['value']=$value['nums'];  
							}
							?> 
							<div style="display:none" id="qudaoUvName"><?php echo json_encode($attrName);?></div> 
							<div style="display:none" id="qudaoUvDatas"><?php echo json_encode($attrData);?></div> 

						</div>
					</div>
					<div class="col-lg-6 col-sm-6">
						<div class="box float-e-margins">
							<div class="box-title"> 
								<span style="float:right;height: 100%">
									<span style="float:left;"><select id="zqudao" name="zqudao">
										<option value="">---选择渠道商---</option>
										<?php foreach ($qudaoList as $key => $value) {?> 
										<option value="<?php echo $value['Yonghuqudao']['yonghuqudaohao'];?>" ><?php echo $value['Yonghuqudao']['title'];?></option>
										<?php }?> 
									</select>
									<h5>渠道注册量 Top10</h5>
								</span>
								&nbsp;&nbsp;<span style="float:right;width:48%"><label class="label_name "  style="float:right;">&nbsp;&nbsp;<button class="btn" style="background:#438eb9 none repeat scroll 0 0;color:#fff;font-size: 12px;height: 28px;"  type="button" id="seachData5">搜索</button></label>  
								<input class="laydate-icon col-lg-4" placeholder="开始时间"  name="startTime" value="" id="start4" style=" margin-right:10px; height:28px; line-height:28px; float:left"> 
								<span  style=" float:left; padding:0px 10px; line-height:32px;">至</span>
								<input class="laydate-icon col-lg-4" placeholder="截止时间"  name="endTime" id="end4" value="" style="height:28px; line-height:28px; float:left"> 
							</span>
						</span>

					</div>
					<div class="box-content">
						<div id="qudaoregtop10" style="width: 100%;height:400px;"></div> 
					</div>
					<?php 
					$attrName=[];
					$attrData=[];
					foreach ($qudaoPvTop as $key => $value) {
						$attrName[]=$value['title'];
						$attrData[$key]['name']=$value['title'];
						$attrData[$key]['value']=$value['nums'];  
					}
					?> 
					<div style="display:none" id="qudaoRegName"><?php echo json_encode($attrName);?></div> 
					<div style="display:none" id="qudaoRegData"><?php echo json_encode($attrData);?></div> 
				</div>
			</div>  
		</div>  
	</div> 
</div> 
</div> 
</body>
</html> 
<script type="text/javascript">  
laydate({elem: '#start', format: 'YYYY-MM'});  
laydate({elem: '#start3'});
laydate({elem: '#end3'});
laydate({elem: '#start4'});
laydate({elem: '#end4'});
laydate({elem: '#start8'});
laydate({elem: '#end8'});
laydate({elem: '#start5', format: 'YYYY-MM'}); 
</script>