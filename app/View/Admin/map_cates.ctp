 
    <div class="margin" id="page_style">
        <div class="operation"> 
            <span class="submenu"><a href="javascript:void(0)"class="btn button_btn bg-deep-blue" title="添加分类"  onclick="editAct('添加分类',0)"><i class="fa  fa-edit"></i>&nbsp;添加分类</a></span>
        </div> 
        <div class="Columns_list slideBox margin-top" id="Columns_list"> 
            <div class="bd">
                <ul class="main_column">
                    <table class="table table_list table_striped table-bordered  margin-top" id="data_table">
                        <thead>
                            <tr>
                               <th width="15%">序号</th>
                               <th width="55%">分类名称</th> 
                               <th width="10%">排序</th>
                               <th width="5%">是否有效</th>
                               <th width="15%">操作</th> 
                            </tr>   
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="6" class="padding_none">
                                    <div class="dd" id="nestable">
                                        <ol class="dd-list">   
                                            <?php foreach ($dataList as $key => $v) { ?>
                                            <li class="dd-item" data-id="<?php echo $v['id'];?>">
                                                <div class="dd-handle <?php echo isset($v['_child'])?'dd-columns':'';?>">
                                                    <table>
                                                        <tbody>
                                                            <tr>
                                                                <td width="15%"><?php echo $v['id'];?></td>
                                                                <td width="55%"><?php echo $v['name'];?></td> 
                                                                <td width="10%"><?php echo $v['sort'];?></td>  
                                                                <td width="5%"><span id='edit' class="<?php echo $v['status']==1?'yesimg':'noimg';?>" onclick="toggle(this,'mapcate', <?php echo $v['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>  
                                                                <td width="15%"><a title="编辑" id='edit' href="javascript:void(0)" onclick="editAct('添加分类',<?php echo $v['id'];?>)" class="btn btn-white button_btn">编辑</a>         </td> 
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>              
                                            <?php if(isset($v['_child']) && sizeof($v['_child'])>0){ ?> 
                                            <ol class="dd-list">
                                                <?php foreach ($v['_child'] as $kk => $vv) { ?> 
                                                <li class="dd-item" data-id="<?php echo $vv['id'];?>">
                                                    <div class="dd-handle dd-handle_two">
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td width="15%"><?php echo $vv['id'];?></td>
                                                                    <td width="55%"><?php echo $vv['name'];?></td> 
                                                                    <td width="10%"><?php echo $vv['sort'];?></td>  
                                                                    <td width="5%"><span  id='edit' class="<?php echo $vv['status']==1?'yesimg':'noimg';?>" onclick="toggle(this,'mapcate', <?php echo $vv['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>  
                                                                    <td width="15%"><a title="编辑"  id='edit'  href="javascript:void(0)" onclick="editAct('添加分类',<?php echo $vv['id'];?>)" class="btn button_btn bg-deep-blue">编辑</a></td>     
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </li> 
                                            <?php }?> 
                                        </ol> 
                                        <?php }?> 
                                    </li> 
                                    <?php }?>  
                                </ol>
                            </div>  
                        </td> 
                    </tr>
                </tbody>
            </table>
        </ul>  
    </div>
</div>
</div>
<!--添加栏目-->
<div class="add_columns_style" id="add_columns_style" style=" display:none"> 
    <form action="/admin/map_cates_edit" id="formEdit"  class="dataForm" role="form"  method="post"></form>
</div>  