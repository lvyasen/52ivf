<?php echo $this->Html->script('/admin/js/ivf52/user.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.min.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.bootstrap.js');?>  
    <div class="margin Competence_style" id="page_style">
        <div class="operation clearfix"> 
            <a href="/admin/map_edit"  class="btn button_btn bg-deep-blue" title="添加链接"><i class="fa  fa-edit"></i>&nbsp;添加链接</a>  
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr>  
                        <th>序号</th> 
                        <th>分类</th>  
                        <th>名称</th>  
                        <th>链接</th>  
                        <th>状态</th>  
                        <th class="hidden-480">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataList as $key => $v) { ?>
                    <tr>  
                        <td><?php echo $v['Map']['id'];?></td> 
                        <td><?php echo $v['Map']['cate_name'];?></td>  
                        <td><?php echo $v['Map']['name'];?></td> 
                        <td><?php echo $v['Map']['url'];?></td>  
                        <td><span  class="<?php echo $v['Map']['status']==1?'yesimg':'noimg';?>" onclick="toggle(this,'map', <?php echo $v['Map']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td> 
                        <td class="td-manage"> 
                            <a title="编辑" href="/admin/map_edit?id=<?= $v['Map']['id']?>" class="btn button_btn bg-deep-blue">编辑</a><a title="删除" href="javascript:;" onclick="delAct(this,'map',<?php echo $v['Map']['id'];?>,'single')" class="btn button_btn btn-danger">删除</a>
                        </td>
                    </tr>
                    <?php }?> 
                </tbody>
            </table>
        </div>
    </div> 
<script type="text/javascript">  
var oTable1 = $('#data_table').dataTable( {
"bPaginate": true, 
"width":"100%", 
"bLengthChange":false,
"iDisplayLength": 20,
//"columns" : _tableCols, 
"bStateSave": false,//状态保存
"searching": false,
"order": [[ 0, "desc" ]],
"aoColumnDefs": [{"orderable":false,"aTargets":[5]
}]

});
</script>
