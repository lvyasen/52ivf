
    <?php echo $this->Html->script('/admin/js/laydate/laydate.js');?>  
    <div class="margin Competence_style" id="page_style">
        <div class="operation clearfix">
            <ul class="choice_search">
                <form role="form" id="userForm" action="/admin/doctor" method="get">   
                    <li class="clearfix col-xs-2 "><label class="label_name col-xs-1">医院：&nbsp;&nbsp;</label><div class="Add_content col-xs-1">
                        <select name="hospital_id"> 
                        <option value="0">---请选择---</option>
                            <?php foreach ($hospitalList as $key => $value) {?> 
                        <option value="<?php echo $value['Hospital']['id'];?>" <?php echo isset($_GET['hospital_id']) && $_GET['hospital_id']==$value['Hospital']['id']?'selected':''?>><?php echo $value['Hospital']['cn_name'];?></option>  
                        <?php }?> 
                        </select></div>  
                    </li>
                    <li class="clearfix col-xs-4 col-lg-5 col-ms-5 "><label class="label_name ">时间：</label> 
                        <input class="laydate-icon col-xs-4 col-lg-3" name="startTime" value="<?= @$_GET['startTime'] ?>" id="start" style=" margin-right:10px; height:28px; line-height:28px; float:left">
                        <span  style=" float:left; padding:0px 10px; line-height:32px;">至</span>
                        <input class="laydate-icon col-xs-4 col-lg-3" name="endTime" id="end" value="<?= @$_GET['endTime'] ?>" style="height:28px; line-height:28px; float:left">
                    </li> 
                    <input name="keywords" type="text" value="<?= @$_GET['keywords'] ?>" placeholder="医院" class="form-control"/>
                    <button class="btn button_btn bg-deep-blue " onclick="this.form.submit()"  type="button"><i class="fa  fa-search"></i>&nbsp;搜索</button> 
                    <a href="/admin/doctor_edit"  class="btn button_btn bg-deep-blue" title="添加医院"><i class="fa  fa-edit"></i>&nbsp;添加医生</a>    
                </form> 
            </ul>  
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr>   
                        <th>主图</th>   
                        <th>医生名称</th>   
                        <th>英文名称</th>  
                        <th>最高学历</th>    
                        <th>职务</th>    
                        <th>擅长项目</th>   
                        <th>人气分</th>   
                        <th>口碑分</th> 
                        <th>活跃分</th> 
                        <th>预约数</th> 
                        <th>案例数</th>  
                        <th>关注数</th>  
                        <th>候诊时间</th>  
                        <th>状态</th>  
                        <th>推荐</th>  
                        <th>发布时间</th> 
                        <th class="hidden-480">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataList as $key => $v) { ?>
                    <tr>
                        <td><img src="<?php echo $v['Doctor']['avatar'];?>" width="50px"></td>     
                        <td><?php echo $v['Doctor']['en_name'];?></td>
                        <td><?php echo $v['Doctor']['cn_name'];?></td> 
                        <td><?php echo $education[$v['Doctor']['highest_education']];?></td>  
                        <td><?php echo $v['Doctor']['duty'];?></td>  
                        <td><?php echo $v['Doctor']['projects'];?></td>  
                        <td><?php echo $v['Doctor']['popularity'];?></td>  
                        <td><?php echo $v['Doctor']['public_praise'];?></td>  
                        <td><?php echo $v['Doctor']['active'];?></td> 
                        <td><?php echo $v['Doctor']['appoint_num'];?></td> 
                        <td><?php echo $v['Doctor']['case_num'];?></td> 
                        <td><?php echo $v['Doctor']['follow_num'];?></td> 
                        <td><?php echo $v['Doctor']['waiting_time'];?></td>  
                        <td><span  class="<?php echo $v['Doctor']['status']==1?'yesimg':'noimg';?>" onclick="toggle(this,'doctor', <?php echo $v['Doctor']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>  
                        <td><span  class="<?php echo $v['Doctor']['rec']==1?'yesimg':'noimg';?>" onclick="toggle(this,'doctor_rec', <?php echo $v['Doctor']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>  
                        <td><?= date('Y-m-d H:i:s',strtotime($v['Doctor']['create_time'])) ?></td>  
                        <td class="td-manage"> 
                            <a title="编辑" href="/admin/account_comment?ctype=30&cid=<?= $v['Doctor']['id']?>" class="btn button_btn bg-deep-blue">查看评论</a>
                            <a title="编辑" href="/admin/doctor_edit?id=<?= $v['Doctor']['id']?>" class="btn button_btn bg-deep-blue">编辑</a> 
                            <?php if($userRoles=='all'){?>   <a title="删除" href="javascript:;" onclick="delAct(this,'doctor',<?php echo $v['Doctor']['id'];?>,'single')" class="btn button_btn btn-danger">删除</a><?php }?>   
                        </td>
                    </tr>
                    <?php }?> 
                </tbody>
            </table>
            <?php echo $this->element('admin/pagination');?>
        </div>
    </div> 

<script type="text/javascript">

/******时间设置*******/
var start = {
    elem: '#start',
    format: 'YYYY-MM-DD',
   // min: laydate.now(), //设定最小日期为当前日期
    max: '2099-06-16', //最大日期
    istime: true,
    istoday: false,
    choose: function(datas){
         end.min = datas; //开始日选好后，重置结束日的最小日期
         end.start = datas //将结束日的初始值设定为开始日
     }
 };
 var end = {
    elem: '#end',
    format: 'YYYY-MM-DD',
    //min: laydate.now(),
    max: '2099-06-16',
    istime: true,
    istoday: false,
    choose: function(datas){
        start.max = datas; //结束日选好后，重置开始日的最大日期
    }
};
laydate(start);
laydate(end);
</script>