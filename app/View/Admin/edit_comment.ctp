
<?php echo $this->Html->css('/admin/css/jquery.searchableSelect.css');?>  
<?php echo $this->Html->script('/admin/js/jquery.searchableSelect.js');?>     
<script type="text/javascript"> 
$("input:radio[name='topic_type']").change(function(){ 
	$('input[type=radio]').each(function(index,item){ 
		if(this.checked==true){
			$("#show_"+$(this).val()).attr('style','display:block');
			$("#setUser").val($(this).val()); 
		}else{
			$("#show_"+$(this).val()).attr('style','display:none');
		}
	}); 
});
</script>
				<div class="add_style">
					<ul>   
					<li class="clearfix"><label class="label_name col-xs-3">类型：&nbsp;&nbsp;</label> 
						<div class="col-sm-9">
				            <?php foreach ($comment_type as $key => $value) {?> 
							<label class="radio-inline"><input name="topic_type" class="ace radio" type="radio" value="<?php echo $value;?>" <?php echo isset($dataList['topic_type']) && $dataList['topic_type']==$value?'checked':''?>><span class="lbl"><?php echo $key;?></span></label>
				            <?php }?>  
						</div> 
				    </li> 

					<div id='show_10' style="<?php echo isset($dataList['topic_type']) && $dataList['topic_type']=='10'?'display:block':'display:none'?>">
					<li class="clearfix"><label class="label_name col-xs-3">选择攻略：&nbsp;&nbsp;</label><div class="Add_content col-xs-9">
						<select name="g_id" class="search">
							<?php foreach ($guideList as $key => $value) {?> 
							<option value="<?php echo $value['Article']['id'];?>" <?php echo isset($dataList['topic_id']) && $dataList['topic_id']==$value['Article']['id']?'selected':''?>><?php echo $value['Article']['title'];?></option>
							<?php }?> 
						</select></div>  
					</li>   
					</div> 

					<div id='show_20' style="<?php echo isset($dataList['topic_type']) && $dataList['topic_type']=='20'?'display:block':'display:none'?>">
					<li class="clearfix"><label class="label_name col-xs-3">选择医院：&nbsp;&nbsp;</label><div class="Add_content col-xs-9">
						<select name="h_id" class="search">
							<?php foreach ($hospitalList as $key => $value) {?> 
							<option value="<?php echo $value['Hospital']['id'];?>" <?php echo isset($dataList['topic_id']) && $dataList['topic_id']==$value['Hospital']['id']?'selected':''?>><?php echo $value['Hospital']['cn_name'];?></option>
							<?php }?> 
						</select></div>  
					</li>   
					</div> 

					<div id='show_30' style="<?php echo isset($dataList['topic_type']) && $dataList['topic_type']=='30'?'display:block':'display:none'?>">
					<li class="clearfix"><label class="label_name col-xs-3">选择医生：&nbsp;&nbsp;</label><div class="Add_content col-xs-9">
						<select name="d_id" class="search">
							<?php foreach ($doctorList as $key => $value) {?> 
							<option value="<?php echo $value['Doctor']['id'];?>" <?php echo isset($dataList['topic_id']) && $dataList['topic_id']==$value['Doctor']['id']?'selected':''?>><?php echo $value['Doctor']['cn_name'];?></option>
							<?php }?> 
						</select></div>  
					</li>   
					</div> 

					<div id='show_40' style="<?php echo isset($dataList['topic_type']) && $dataList['topic_type']=='40'?'display:block':'display:none'?>">
					<li class="clearfix"><label class="label_name col-xs-3">选择视频：&nbsp;&nbsp;</label><div class="Add_content col-xs-9">
						<select name="v_id" class="search">
							<?php foreach ($videoList as $key => $value) {?> 
							<option value="<?php echo $value['VideoCate']['id'];?>" <?php echo isset($dataList['topic_id']) && $dataList['topic_id']==$value['VideoCate']['id']?'selected':''?>><?php echo $value['VideoCate']['name'];?></option> 
							<?php }?> 
						</select></div>  
					</li>   
					</div> 

					<div id='show_50' style="<?php echo isset($dataList['topic_type']) && $dataList['topic_type']=='50'?'display:block':'display:none'?>">
					<li class="clearfix"><label class="label_name col-xs-3">选择分享：&nbsp;&nbsp;</label><div class="Add_content col-xs-9">
						<select name="s_id" class="search">
							<?php foreach ($shareList as $key => $value) {?> 
							<option value="<?php echo $value['Share']['id'];?>" <?php echo isset($dataList['topic_id']) && $dataList['topic_id']==$value['Share']['id']?'selected':''?>><?php echo $value['Share']['title'];?></option> 
							<?php }?> 
						</select></div>  
					</li>   
					</div> 

					<div id='show_60' style="<?php echo isset($dataList['topic_type']) && $dataList['topic_type']=='60'?'display:block':'display:none'?>">
					<li class="clearfix"><label class="label_name col-xs-3">选择问答：&nbsp;&nbsp;</label><div class="Add_content col-xs-9">
						<select name="a_id" class="search">
							<?php foreach ($answerList as $key => $value) {?> 
							<option value="<?php echo $value['Share']['id'];?>" <?php echo isset($dataList['topic_id']) && $dataList['topic_id']==$value['Share']['id']?'selected':''?>><?php echo $value['Share']['content'];?></option> 
							<?php }?> 
						</select></div>  
					</li>   
					</div> 

					<li class="clearfix"><label class="label_name col-xs-3">评论用户：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><select name="uid" id="shareUser"> 
				            <?php foreach ($usersDate as $key => $value) {?> 
				            <option value="<?php echo $value['User']['id'];?>" <?php echo isset($dataList['uid']) && $dataList['uid']==$value['User']['id']?'selected':''?>><?php echo $value['User']['nick_name'];?></option>   
				            <?php }?> 
				        </select>&nbsp;&nbsp;&nbsp;
                        <a href="javascript:void(0);" onclick="setUsers()" class="btn btn-info button_btn">随机筛选未发布会员</a>  </div>  
				    </li>  

				    <li class="clearfix"><label class="label_name col-xs-3">评论内容：&nbsp;&nbsp;</label><span class="Add_content col-xs-9"><textarea name="content" class="form-control col-xs-12 col-sm-5" id="form_textarea" placeholder="" ><?php echo isset($dataList['content'])?$dataList['content']:''?></textarea></span></li> 

				    <li class="clearfix"><label class="label_name label_name col-xs-3">评论时间：&nbsp;&nbsp;</label> <span class="Add_content col-xs-9">
				        <input class="laydate-icon col-xs-5" name="comment_time" value="<?php echo isset($dataList['comment_time'])?$dataList['comment_time']:date("Y-m-d H:i:s");?>" id="start5" style=" margin-right:10px; height:28px; line-height:28px; float:left"> </span>
				    </li>

                    <!--<li class="clearfix"><label class="label_name col-xs-3">浏览数：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="browse_num" type="text"  class="col-xs-4" value="<?php echo isset($dataList['browse_num'])?$dataList['browse_num']:'1000'?>" placeholder="浏览数" /></div></li> 

                    <li class="clearfix"><label class="label_name col-xs-3">赞数：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="agree_num" type="text"  class="col-xs-4" value="<?php echo isset($dataList['agree_num'])?$dataList['agree_num']:'1000'?>" placeholder="赞数" /></div></li> 
					-->
				    <li class="clearfix"><label class="label_name col-xs-3">评论排序：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="sort" type="text"  value="<?php echo isset($dataList['sort'])?$dataList['sort']:'1000'?>"  class="col-xs-2"/></div>  
				    </li>   
 
					</ul> 
					<input type="hidden" id="setUser" value="10"/>
					<input type="hidden" name="dosubmit" value="1"/>   
			</div>  
<script type="text/javascript"> 
$(function(){
	$('.search').searchableSelect();
}); 
function setUsers(){
	var val=$('#setUser').val();
	toggerUser(val,'shareUser');
}
$(".dataForm").Validform({tiptype:3});  
var start5 = {
    elem: '#start5',
    format: 'YYYY-MM-DD hh:mm:ss',
   // min: laydate.now(), //设定最小日期为当前日期
    max: '2099-06-16', //最大日期
    istime: true,
    istoday: false,
    choose: function(datas){
         end.min = datas; //开始日选好后，重置结束日的最小日期
         end.start = datas //将结束日的初始值设定为开始日
     }
 };
laydate(start5);
</script>