
        <div class="margin">
            <form role="form" id="userForm" class="dataForm" action="<?= $_SERVER['REQUEST_URI'] ?>" method="post"  enctype="multipart/form-data">
                <div class="add_style">
                    <ul>      
                        <li class="clearfix"><label class="label_name col-xs-1">音频名称：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="title" type="text"  class="col-xs-4" value="<?php echo isset($dataList['title'])?$dataList['title']:''?>" placeholder="音频名称"  datatype="*"   nullmsg="请填写音频名称！"/></div> </li>

                        <li class="clearfix"><label class="label_name col-xs-1">所属专辑：&nbsp;&nbsp;</label><div class="Add_content col-xs-9">
                        <select name="parent_id">
                            <option value="0">--无--</option>
                            <?php foreach ($cateList as $key => $value) {?> 
                            <option value="<?php echo $value['Album']['id'];?>" <?php echo isset($dataList['parent_id']) && $dataList['parent_id']==$value['Album']['id']?'selected':''?>><?php echo $value['Album']['name'];?></option>  
                            <?php }?> 
                        </select></div>  
                        </li>
 
                        <li class="clearfix"><label class="label_name col-xs-1">音频地址：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="src" type="text"  class="col-xs-4" value="<?php echo isset($dataList['src'])?$dataList['src']:''?>" placeholder="url"/></div></li>   

                        <li class="clearfix"><label class="label_name col-xs-1">时长：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="h" type="text"  class="col-xs" size="6"  value="<?= !isset($dataList['length']) ? '' : date('H',strtotime($dataList['length'])); ?>" placeholder="00 (小时)" datatype="n" errormsg="格式不正确！" nullmsg="请填写时长！"/>&nbsp;：&nbsp;<input name="i" type="text" class="col-xs" size="6"  value="<?= !isset($dataList['length']) ? '' : date('i',strtotime($dataList['length']))?>" placeholder="00 (分)" datatype="n" errormsg="格式不正确！" nullmsg="请填写时长！"/>&nbsp;：&nbsp;<input name="s" type="text" class="col-xs" size="6"  value="<?= !isset($dataList['length']) ? '' : date('s',strtotime($dataList['length']))?>" placeholder="00 (秒)" datatype="n" errormsg="格式不正确！" nullmsg="请填写时长！"/></div></li>


                        <li class="clearfix"><label class="label_name col-xs-1">播放数：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="play_count" type="text" value="<?php echo isset($dataList['play_count'])?$dataList['play_count']:'1000'?>" datatype="n" errormsg="请填写数字！"   ignore="ignore" class="col-xs-1"/></div>  
                        </li> 

                        <li class="clearfix"><label class="label_name col-xs-1">排序：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="sort" type="text" value="<?php echo isset($dataList['sort'])?$dataList['sort']:'100'?>" datatype="n" errormsg="请填写数字！"   ignore="ignore" class="col-xs-1"/>（列表从小到大排列）</div>  
                        </li> 

                        <li class="clearfix"><label class="label_name col-xs-1">作者：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="author" type="text"  class="col-xs-4" value="<?php echo isset($dataList['author'])?$dataList['author']:''?>"/></div></li>   

                        <li class="clearfix">
                            <label class="label_name col-xs-1">是否有效：&nbsp;&nbsp;</label>
                            <div class="Add_content col-xs-9">
                                <label class="l_f checkbox_time"><input type="checkbox" name="status" class="ace" id="checkbox" value="1"  <?php echo isset($dataList['status']) && $dataList['status']==1?'checked=checked':''?>><span class="lbl">是</span></label> 
                            </div> 
                        </li> 
                    </ul>
                    <!--按钮操作-->
                    <li class="clearfix">
                        <div class="col-xs-2 col-lg-2">&nbsp;</div>
                        <div class="col-xs-6">
                            <input class="btn button_btn bg-deep-blue" type="submit" value="保存提交"/>
                            <input name="reset" class="btn button_btn btn-gray" value="取消重置" type="reset">
                            <a href="/admin/music/"  class="btn btn-info button_btn"><i class="fa fa-reply"></i> 返回上一步</a>
                        </div>
                    </li> 
                    <input type="hidden" name="dosubmit" value="1"/>  
                </form>
            </div>
        </div> 
    <script type="text/javascript">  
        $(".dataForm").Validform({tiptype:3});  
    </script>