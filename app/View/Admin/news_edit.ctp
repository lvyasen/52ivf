 
    <!--复文本编辑框--> 
    <?php echo $this->Html->script('/admin/js/ueditor/ueditor.config.js');?>  
    <?php echo $this->Html->script('/admin/js/ueditor/ueditor.all.min.js');?>  
    <?php echo $this->Html->script('/admin/js/ueditor/lang/zh-cn/zh-cn.js');?>     
        <div class="margin">
            <form role="form" id="userForm" class="dataForm" action="<?= $_SERVER['REQUEST_URI'] ?>" method="post"  enctype="multipart/form-data">
                <div class="add_style">
                    <ul>   
                        <li class="clearfix"><label class="label_name col-xs-1">PC主图：&nbsp;&nbsp;</label>
                             <span class="cont_style col-xs-9">        
                               <div id="preview" class="preview_img"><img id="imghead" width="190" src="<?php echo isset($dataList['image']) && !empty($dataList['image'])?$dataList['image']:'/admin/images/image.png';?>" border="0"></div>
                               <div class="fileInput ">
                                   <input onchange="previewImage(this)" name="imgFile" id="upfile" class="upfile" type="file">
                                <input class="upFileBtn" value="上传图片" onclick="document.getElementById('upfile').click()" type="button">
                                </div>
                              </span>
                          </li>   
                        <li class="clearfix"><label class="label_name col-xs-1">手机主图：&nbsp;&nbsp;</label>
                             <span class="cont_style col-xs-9">        
                               <div id="preview2" class="preview_img"><img id="imghead" width="190" src="<?php echo isset($dataList['mb_image']) && !empty($dataList['mb_image'])?$dataList['mb_image']:'/admin/images/image.png';?>" border="0"></div>
                               <div class="fileInput ">
                                   <input onchange="PreviewImage(this,'preview2')" name="imgFile2" id="upfile2" class="upfile" type="file">
                                <input class="upFileBtn" value="上传图片" onclick="document.getElementById('upfile2').click()" type="button">
                                </div>
                              </span>
                         </li>         
                        <li class="clearfix"><label class="label_name col-xs-1"><i>*</i>标题名称：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="title" type="text"  class="col-xs-4" value="<?php echo isset($dataList['title'])?$dataList['title']:''?>" placeholder="标题名称"  datatype="*"   nullmsg="请填写标题名称！"/></div> </li>   
  
                        <li class="clearfix"><label class="label_name col-xs-1">所属分类：&nbsp;&nbsp;</label><div class="Add_content col-xs-9">
                            <select name="category_id">
                                <option value="0">--选择新闻分类--</option>
                                <?php foreach ($cateList as $key => $value) {?> 
                                <option value="<?php echo $value['id'];?>" <?php echo isset($dataList['category_id']) && $dataList['category_id']==$value['id']?'selected':''?>><?php echo $value['name'];?></option> 
                                <?php if(isset($value['_child']) && sizeof($value['_child'])>0){ foreach ($value['_child'] as $kk => $vv) {?> 
                                    <option value="<?php echo $vv['id'];?>" <?php echo isset($dataList['category_id']) && $dataList['category_id']==$vv['id']?'selected':''?>>&nbsp;---<?php echo $vv['name'];?></option>
                                <?php }}}?> 
                            </select></div>  
                        </li>  
                    <li class="clearfix"><label class="label_name col-xs-1">描述：</label>
                        <span class="col-xs-6"><textarea name="description" class="form-control col-xs-10" id="form_textarea" placeholder="" onkeyup="checkLength(this,200,'sy');"><?php echo isset($dataList['description'])?$dataList['description']:''?></textarea><span class="wordage">剩余字数：<span id="sy" style="color:Red;">200</span>字</span></span>
                    </li> 
                    <li class="clearfix"><label class="label_name col-xs-1"><i>*</i>新闻内容：&nbsp;&nbsp;</label>
                        <div class="Add_content col-xs-11"><textarea name="content"  id="content"  style="width:100%;height:500px;"><?php echo isset($dataList['content'])?$dataList['content']:''?></textarea></div>
                    </li> 
                    <li class="clearfix"><label class="label_name col-xs-1">点赞数：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="agree_num" type="text"  class="col-xs"  value="<?php echo isset($dataList['agree_num'])?$dataList['agree_num']:mt_rand(10, 80);?>"  datatype="n1-10" errormsg="请填写浏览量！"  nullmsg="请填写浏览量！"/></div></li> 

                    <li class="clearfix"><label class="label_name col-xs-1">浏览数：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="browse_num" type="text"  class="col-xs" value="<?php echo isset($dataList['browse_num'])?intval($dataList['browse_num']):mt_rand(90, 200);?>"  datatype="n1-10" errormsg="请填写浏览数！"  nullmsg="请填写浏览数！"/></div></li> 

                    <li class="clearfix"><label class="label_name col-xs-1">评论数：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="comment_num" type="text"  class="col-xs" value="<?php echo isset($dataList['comment_num'])?intval($dataList['comment_num']):mt_rand(1, 60);?>"  datatype="n1-10" errormsg="请填写评论数！"  nullmsg="请填写评论数！"/></div></li> 

                    <li class="clearfix"><label class="label_name col-xs-1">推荐排序：&nbsp;&nbsp;</label><div class="Add_content col-xs-9"><input name="sort" type="text" value="<?php echo isset($dataList['sort'])?$dataList['sort']:'100'?>" class="col-xs-1"/></div></li>
                    <li class="clearfix">
                        <label class="label_name col-xs-1">是否有效：&nbsp;&nbsp;</label>
                        <div class="Add_content col-xs-9">
                            <label class="l_f checkbox_time"><input type="checkbox" name="status" class="ace" id="checkbox" value="1"  <?php echo isset($dataList['status']) && $dataList['status']==0?'':'checked=checked'?>><span class="lbl">是</span></label> 
                        </div> 
                    </li> 
                    <li class="clearfix">
                        <label class="label_name col-xs-1">首页推荐：&nbsp;&nbsp;</label>
                        <div class="Add_content col-xs-9">
                            <label class="l_f checkbox_time"><input type="checkbox" name="is_home" class="ace" id="checkbox" value="1"  <?php echo isset($dataList['is_home']) && $dataList['is_home']==0?'':'checked=checked'?>><span class="lbl">是</span></label> 
                        </div> 
                    </li>  
                </ul>
                <!--按钮操作-->
                <li class="clearfix">
                    <div class="col-xs-2 col-lg-2">&nbsp;</div>
                    <div class="col-xs-6">
                        <input class="btn button_btn bg-deep-blue" type="submit" value="保存提交"/>
                        <input name="reset" class="btn button_btn btn-gray" value="取消重置" type="reset">
                        <a href="/admin/news/"  class="btn btn-info button_btn"><i class="fa fa-reply"></i> 返回上一步</a>
                    </div>
                </li> 
                <input type="hidden" name="dosubmit" value="1"/>  
            </form> 
        </div>
    </div>  
<script type="text/javascript">  
$(function(){ 
    UE.getEditor('content'); 
    $(".dataForm").Validform({tiptype:3});     
});
</script> 