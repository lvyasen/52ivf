<?php echo $this->Html->script('/admin/js/ivf52/user.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.min.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.bootstrap.js');?>  
    <div class="margin Competence_style" id="page_style">
        <div class="operation clearfix"> 
            <a href="/admin/music_edit"  class="btn button_btn bg-deep-blue" title="添加音频"><i class="fa  fa-edit"></i>&nbsp;添加音频</a>  
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr>  
                        <th>序号</th> 
                        <th>音频名称</th>
                        <th>专辑</th> 
                        <th>时长</th> 
                        <th>来源</th>  
                        <th>播放数</th>  
                        <th>排序</th>  
                        <th>状态</th>    
                        <th>发布时间</th>  
                        <th class="hidden-480">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataList as $key => $v) { ?>
                    <tr>  
                        <td><?php echo $v['Audio']['id'];?></td>  
                        <td><?php echo $v['Audio']['title'];?></td> 
                        <td><?php echo $cateList[$v['Audio']['parent_id']];?></td>  
                        <td><?php echo $v['Audio']['length'];?></td>  
                        <td><?php echo $v['Audio']['src'];?></td>  
                        <td><?php echo $v['Audio']['play_count'];?></td>  
                        <td><?php echo $v['Audio']['sort'];?></td>   
                        <td><span  class="<?php echo $v['Audio']['status']==1?'yesimg':'noimg';?>" onclick="toggle(this,'music', <?php echo $v['Audio']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td> 

                        <td><?= date('Y-m-d H:i:s',strtotime($v['Audio']['create_time'])) ?></td> 
                        <td class="td-manage"> 
                            <a title="编辑" href="/admin/music_edit?id=<?= $v['Audio']['id']?>" class="btn button_btn bg-deep-blue">编辑</a><a title="删除" href="javascript:;" onclick="delAct(this,'music',<?php echo $v['Audio']['id'];?>,'single')" class="btn button_btn btn-danger">删除</a>
                        </td>
                    </tr>
                    <?php }?> 
                </tbody>
            </table>
        </div>
    </div> 
<script type="text/javascript">  
var oTable1 = $('#data_table').dataTable( {
"bPaginate": true, 
"width":"100%", 
"bLengthChange":false,
"iDisplayLength": 20,
//"columns" : _tableCols, 
"bStateSave": false,//状态保存
"searching": false,
"aoColumnDefs": [{"orderable":false,"aTargets":[9]
}]

});
</script>
