<?php echo $this->Html->script('/admin/js/ivf52/user.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.min.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.bootstrap.js');?>  
    <div class="margin Competence_style" id="page_style">
        <div class="operation clearfix"> 
            <a href="/admin/blackIp_edit"  class="btn button_btn bg-deep-blue" title="添加链接"><i class="fa  fa-edit"></i>&nbsp;添加黑名单</a>  
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr>  
                        <th>序号</th>
                        <th>屏蔽ip地址</th>   
                        <th>状态</th>  
                        <th class="hidden-480">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataList as $key => $v) { ?>
                    <tr>  
                        <td><?php echo $v['BlackIp']['id'];?></td> 
                        <td><?php echo $v['BlackIp']['address'];?></td>  
                        <td><span  class="<?php echo $v['BlackIp']['status']==1?'yesimg':'noimg';?>" onclick="toggle(this,'blackIp', <?php echo $v['BlackIp']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td> 
                        <td class="td-manage"> 
                            <a title="编辑" href="/admin/blackIp_edit?id=<?= $v['BlackIp']['id']?>" class="btn button_btn bg-deep-blue">编辑</a><a title="删除" href="javascript:;" onclick="delAct(this,'blackIp',<?php echo $v['BlackIp']['id'];?>,'single')" class="btn button_btn btn-danger">删除</a>
                        </td>
                    </tr>
                    <?php }?> 
                </tbody>
            </table>
        </div>
    </div> 
<script type="text/javascript">  
var oTable1 = $('#data_table').dataTable( {
"bPaginate": true, 
"width":"100%", 
"bLengthChange":false,
"iDisplayLength": 20,
//"columns" : _tableCols, 
"bStateSave": false,//状态保存
"searching": false,
"aoColumnDefs": [{"orderable":false,"aTargets":[5]
}]

});
</script>
