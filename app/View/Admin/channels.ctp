<?php echo $this->Html->script('/admin/js/ivf52/user.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.min.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.dataTables.bootstrap.js');?>  
    <div class="margin Competence_style" id="page_style">
        <div class="operation clearfix"> 
            <a href="/admin/channel_edit"  class="btn button_btn bg-deep-blue" title="添加渠道"><i class="fa  fa-edit"></i>&nbsp;添加渠道</a>  
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr>  
                        <th>序号</th>
                        <th>渠道编号</th>  
                        <th>名称</th>  
                        <th>负责人</th>  
                        <th>负责人电话</th> 
                        <th>推广地址</th>  
                        <th>状态</th>  
                        <th class="hidden-480">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataList as $key => $v) { ?>
                    <tr>  
                        <td><?php echo $v['Yonghuqudao']['id'];?></td> 
                        <td><?php echo $v['Yonghuqudao']['yonghuqudaohao'];?></td> 
                        <td><?php echo $v['Yonghuqudao']['title'];?></td> 
                        <td><?php echo $v['Yonghuqudao']['contact'];?></td>  
                        <td><?php echo $v['Yonghuqudao']['contact_mobile'];?></td>  
                        <td><?php echo "http://www.ivf52.com/?sc=".$v['Yonghuqudao']['yonghuqudaohao'];?></td>  
                        <td><span  class="<?php echo $v['Yonghuqudao']['status']==1?'yesimg':'noimg';?>" onclick="toggle(this,'channel', <?php echo $v['Yonghuqudao']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td> 
                        <td class="td-manage"> 
                            <a title="编辑" href="/admin/channel_edit?id=<?= $v['Yonghuqudao']['id']?>" class="btn button_btn bg-deep-blue">编辑</a><a title="删除" href="javascript:;" onclick="delAct(this,'channel',<?php echo $v['Yonghuqudao']['id'];?>,'single')" class="btn button_btn btn-danger">删除</a>
                        </td>
                    </tr>
                    <?php }?> 
                </tbody>
            </table>
        </div>
    </div> 
<script type="text/javascript">  
var oTable1 = $('#data_table').dataTable( {
"bPaginate": true, 
"width":"100%", 
"bLengthChange":false,
"iDisplayLength": 20,
//"columns" : _tableCols, 
"bStateSave": false,//状态保存
"searching": false,
"aoColumnDefs": [{"orderable":false,"aTargets":[6]
}]

});
</script>
