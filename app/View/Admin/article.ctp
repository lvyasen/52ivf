
    <?php echo $this->Html->script('/admin/js/laydate/laydate.js');?>     
    <div class="margin Competence_style" id="page_style"> 
        <div class="operation clearfix">
            <ul class="choice_search">
                <form role="form" id="userForm" action="/admin/article" method="get">  
                    <li class="clearfix col-xs-2 "><label class="label_name col-xs-1">攻略分类：&nbsp;&nbsp;</label><div class="Add_content col-xs-1">
                        <select name="category_id">  
                            <option value="0">--选择攻略分类--</option>
                            <?php foreach ($cateList as $key => $value) {?> 
                            <option value="<?php echo $value['id'];?>" <?php echo isset($_GET['category_id']) && $_GET['category_id']==$value['id']?'selected':''?>><?php echo $value['name'];?></option> 
                            <?php if(isset($value['_child']) && sizeof($value['_child'])>0){ foreach ($value['_child'] as $kk => $vv) {?> 
                                <option value="<?php echo $vv['id'];?>" <?php echo isset($_GET['category_id']) && $_GET['category_id']==$vv['id']?'selected':''?>>&nbsp;---<?php echo $vv['name'];?></option>
                            <?php }}}?> 
                        </select></div>  
                    </li>  
                    <li class="clearfix col-xs-4 col-lg-5 col-ms-5 "><label class="label_name ">时间：</label> 
                        <input class="laydate-icon col-xs-4 col-lg-3" name="startTime" value="<?= @$_GET['startTime'] ?>" id="start" style=" margin-right:10px; height:28px; line-height:28px; float:left">
                        <span  style=" float:left; padding:0px 10px; line-height:32px;">至</span>
                        <input class="laydate-icon col-xs-4 col-lg-3" name="endTime" id="end" value="<?= @$_GET['endTime'] ?>" style="height:28px; line-height:28px; float:left">
                    </li> 
                    <input name="keywords" type="text" value="<?= @$_GET['keywords'] ?>" placeholder="标题" class="form-control"/>
                    <button class="btn button_btn bg-deep-blue " onclick="this.form.submit()"  type="button"><i class="fa  fa-search"></i>&nbsp;搜索</button> 
                    <a href="/admin/article_edit"  class="btn button_btn bg-deep-blue" title="添加攻略"><i class="fa  fa-edit"></i>&nbsp;添加攻略</a>    
                </form> 
            </ul>  
        </div>
        <div class="compete_list">
            <table id="data_table" class="table table_list table_striped table-bordered dataTable no-footer">
                <thead>
                    <tr>  
                        <th>序号</th>
                        <th>标题</th>   
                        <th>分类</th>  
                        <th>排序</th>    
                        <th>评论数</th>    
                        <th>浏览数</th>    
                        <th>点赞数</th>   
                        <th>状态</th> 
                        <th>置顶</th> 
                        <th>精华</th> 
                        <th>发布时间</th> 
                        <th>更新时间</th>  
                        <th class="hidden-480">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dataList as $key => $v) { ?>
                    <tr>  
                        <td><?php echo $v['Article']['id'];?></td> 
                        <td><a href="/plan/info/<?php echo $v['Article']['id'];?>.html" target="_blank"><?php echo $v['Article']['title'];?></a></td> 
                        <td><?php echo $v['Article']['cate_name'];?></td> 
                        <td><?php echo $v['Article']['sort'];?></td>  
                        <td><?php echo $v['Article']['comment_num'];?></td>  
                        <td><?php echo $v['Article']['browse_num'];?></td>  
                        <td><?php echo $v['Article']['agree_num'];?></td>  
                        <td><span  class="<?php echo $v['Article']['status']==1?'yesimg':'noimg';?>" onclick="toggle(this,'article', <?php echo $v['Article']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td> 
                        <td><span  class="<?php echo $v['Article']['is_top']==1?'yesimg':'noimg';?>" onclick="toggle(this,'article_top', <?php echo $v['Article']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td> 
                        <td><span  class="<?php echo $v['Article']['is_elite']==1?'yesimg':'noimg';?>" onclick="toggle(this,'article_elite', <?php echo $v['Article']['id'];?>)">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td> 
                        <td><?= date('Y-m-d H:i:s',strtotime($v['Article']['create_time'])) ?></td> 
                        <td><?= date('Y-m-d H:i:s',strtotime($v['Article']['update_time'])) ?></td> 
                        <td class="td-manage">  
                            <a title="编辑" href="/admin/obj_comment?ctype=10&cid=<?= $v['Article']['id']?>" class="btn button_btn bg-deep-blue">操作评论</a>
                            <a title="编辑" href="/admin/article_edit?id=<?= $v['Article']['id']?>" class="btn button_btn bg-deep-blue">编辑</a>
                            <a title="删除" href="javascript:;" onclick="delAct(this,'article',<?php echo $v['Article']['id'];?>,'single')" class="btn button_btn btn-danger">删除</a>
                        </td>
                    </tr>
                    <?php }?> 
                </tbody>
            </table>
            <?php echo $this->element('admin/pagination');?>
        </div>
    </div> 

<script type="text/javascript"> 
/******时间设置*******/ 
laydate({elem: '#start', istime: true, format: 'YYYY-MM-DD'});  
laydate({elem: '#end', istime: true, format: 'YYYY-MM-DD'});  
</script>