<?= $this->Sgwy->addCss('/css/index/index.css'); ?>
<?= $this->Sgwy->addCss('/css/index/idangerous.swiper.css'); ?><!--PC—banner--> 
<?= $this->Sgwy->addJs('/js/index/idangerous.swiper.js'); ?><!--PC—banner--> 
<?= $this->Sgwy->addJs('/js/index/banner.js'); ?><!--PC—banner--> 
<?= $this->Sgwy->addJs('/js/index/nav.js'); ?>
<?= $this->Sgwy->addJs('/js/index/index.js'); ?>
<?php echo $this->Html->script('/js/page.js');?>

<div class="index">
	<div class="banner">
        <div class="left_nav">
            <ul class="l_nav_menu">
                <li><a class="wpc" href="javascript:void(0);" >
                    <p>测成功率</p>
                    <span>自测成功率 方案 预算</span>
                </a></li>
                <li><a href="/hospital/index/">
                    <p>泰国热门医院</p>
                    <span>医院排名 技术特点 费用</span>
                </a></li>
                <li><a href="/plan/index/">
                    <p>泰国试管攻略</p>
                    <span>攻略 技术 方案 疑难病例</span>
                </a></li>
                <li><a href="/community/index/">
                    <p>试管邦社区</p>
                    <span>就医历程 分享心情 求助</span>
                </a></li>
                <li><a href="/video/index/">
                    <p>试管课堂</p>
                    <span>胖妈妈之声FM 小忧说试管</span>
                </a></li>
            </ul>
            <div class="l_nav_box">
                <div class="nav_box1 nbox">
                    <p>试管助手</p>
                    <span>一分钟为您预测试管婴儿方案</span><br><span>并估算成功率和费用</span>
                    <a class="wpc" href="javascript:void(0);">马上开始</a>
                </div>
                <div class="nav_box2 nbox">
                    <h3>逛医院，轻松约名医 </h3>
                    <a class="zy" href="/hospital/index/">找医院</a><a href="/doctor/index/">找医生</a>
                    <ul class="nb_ul">
                        <li><a href="/hospital/info/9.html">【泰国】全球生殖中心</a></li>
                        <li><a href="/hospital/info/3.html">【泰国】BNH医院</a></li>
                        <li><a href="/hospital/info/10.html">【泰国】暹罗试管生殖中心</a></li>
                        <li><a href="/hospital/info/4.html">【泰国】爱宝贝生殖中心</a></li>
                        <li><a href="/hospital/info/11.html">【泰国】三美泰医院</a></li>
                    </ul>
                    <img src="/images/index/inp1.jpg" alt="泰国热门医院 试管无忧">
                </div>
                <div class="nav_box3 nbox">
                    <h3>各大试管，抢先看 </h3>
                    <a class="zy" href="/plan/index/1.html">试管医院</a><a href="/plan/index/3.html">试管费用</a><a class="zy" href="/plan/index/2.html">试管技术</a><a href="/plan/index/3.html">试管流程</a>
                   <ul class="nb_ul">
                       <li>查看攻略，了解当地医疗特点</li>
                       <li>印象点评，快速获知地区特色</li>
                       <li>泰国数家医院，任您挑选</li>
                       <li>泰国优质医院，100%实地考察</li>
                    </ul>
                    <img src="/images/index/inp3.jpg" alt="试管无忧">
                </div>
                <div class="nav_box4 nbox">
                    <h3>获取经验，分享心情</h3>
                    <a class="zy" href="/share/index/0-50.html">就医经验</a><a href="/share/index/0-10.html">初次检查</a><a class="zy" href="/share/index/0-30.html">取卵经验</a><a href="/share/index/0-40.html">保胎</a>
                   <ul class="nb_ul">
                       <li>获取经验，让就医少走弯路</li>
                       <li>分享心情，传递幸福好孕</li>
                       <li>真诚交友，让就医不再孤单</li>
                       <li>社区分享，随时关注病友情况</li>
                    </ul>
                    <img src="/images/index/inp4.jpg" alt="互动社区 泰国试管婴儿攻略 试管无忧 泰国试管婴儿费用">
                </div>
                <div class="nav_box5 nbox">
                    <h3>无忧媒体，让试管就医更轻松</h3>
                    <a class="zy" href="/audio/index/">胖妈妈之声</a><a href="/video/index/">小忧说试管</a><a class="zy" href="/video/index/">小忧说不孕</a><a href="/video/index/#hbox">名医访谈</a>
                   <ul class="nb_ul">
                       <li>我知道 现在的你 很累</li>
                       <li>多次失败，每况愈下</li>
                       <li>可是未来的路还长</li>
                       <li>无忧媒体，陪你勇敢向前</li>
                    </ul>
                    <img src="/images/index/inp5.jpg" alt="自媒体频道 泰国试管婴儿攻略 试管无忧">
                </div>
            </div>
        </div>
	    <div class="swiper-container">
	        <div class="swiper-wrapper">
                <!-- <div class="swiper-slide wpp">
                    <video width="100%" height="360" controls>
                      <source src="/images/banner.mp4" type="video/mp4">
                    </video>
                </div>  -->
	            <div class="swiper-slide link-box">
	                <a href="/site/index/" style="background: url(/images/index/banner01.jpg) center no-repeat;" class="ppic" ></a>
	                <a href="/site/index/"><img class="mpic" src="/images/index/ban01.jpg" alt="泰国试管婴儿多少钱 泰国试管婴儿费用 试管无忧" /></a>
	            </div>
	            <div class="swiper-slide wpp">
	                <a href="javascript:void(0);" style="background: url(/images/index/banner02.jpg) center no-repeat;" class="ppic" ></a>
	                <img class="mpic" src="/images/index/ban02.jpg" alt="泰国试管婴儿费用 泰国试管婴儿医院 泰国试管婴儿攻略 试管无忧 " />
	            </div> 
	            <div class="swiper-slide wpp">
	                <a href="javascript:void(0);" style="background: url(/images/index/banner03.jpg) center no-repeat;" class="ppic" ></a>
	                <img class="mpic" src="/images/index/ban03.jpg" alt="泰国试管婴儿医院 泰国试管婴儿多少钱 试管无忧" />
	            </div>
	        </div>
	    </div>
	    <div class="banner_icon">
	        <ul class="banner_icon_ul"></ul>
	        <input type="hidden" class="banner_icon_ul_li_img lazy" value="/images/index/r2.png" />
	        <input type="hidden" class="banner_icon_ul_li_img_active lazy" value="/images/index/r1.png" />
	    </div>
	</div>
	<div class="zf_nav">
		<div class="cont">
			<div class="left_div">
				<div><a href="/plan/index/" target="_blank">
					泰国试管<br>服务人群</a>
				</div>
			</div>
			<div class="center_div">
				<a href="javascript:void(0)" target="_blank">多次试管失败</a>
				<a href="javascript:void(0)" target="_blank">高龄备孕</a>
				<a href="javascript:void(0)" target="_blank">疑难不孕</a>
				<a href="javascript:void(0)" target="_blank">想生儿子</a>
				<a href="javascript:void(0)" target="_blank">基因筛查</a>
                <a href="javascript:void(0)" target="_blank" class="wbroder">同性与HIV备孕</a>
			</div>
			<div class="right_div">
				<a href="http://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn" target="_blank">1对1深度咨询</a>
			</div>
		</div>
		<div class="web_cont">
			<a class="div1">试管攻略</a>
			<a class="div2">医院排名</a>
			<a class="div3">医院费用</a>
			<a class="div4">泰国技术</a>
			<a class="div5">PGD/NGS</a>
			<a class="div6">疑难方案</a>
			<a class="div7">双促排</a>
			<a class="div8">成功率</a>
		</div>
	</div>
	<div class="index_news">
        <div class="news_left">
            <p>最新资讯</p>
            <div class="n_l_top">
                <?php  if(sizeof($top4List) > 0){?>
                <div id="zSlider">
                    <div id="picshow">
                        <div id="picshow_img">
                            <ul>
                                <?php  foreach($top4List as $key => $value):?>
                                  <li><a href="/news/info/<?php echo $value["New"]["id"].".html";?>" target="_blank"><img src="<?=$value['New']['image']?>" alt="<?=$value['New']['title']?>"></a></li> 
                                <?php endforeach;?> 
                            </ul>
                        </div>
                    </div>
                    <div id="select_btn">
                      <ul>
                        <?php  foreach($top4List as $key => $value):?>
                        <li ><a href="/news/info/<?php echo $value["New"]["id"].".html";?>">
                        <i><?=$key+1?>.</i>
                        <div>
                            <h5><?=$value['New']['title']?></h5>
                            <p><?=$value['New']['description']?></p></div>
                        </a></li>
                        <?php endforeach;?>  
                      </ul>
                    </div>  
                </div>
                <?php }?>
            </div>
        </div>
        <div class="news_right">
            <p>常见问题</p>
            <div class="n_r">
                <?php  if(sizeof($top6List) > 0){?> 

               <!--  <div class="an_box"><a href="/answer/info/<?=$top6List[0]['Share']['id']?>.html">
                    <h4><?=$top6List[0]['Share']['title']?></h4>
                    <p class="an_box_title"><?=$top6List[0]['Share']['content']?> </p></a> -->

                <div class="an_box"><a href="/news/info/<?=$top6List[0]['New']['id']?>.html">
                    <h4 style=""><?=$top6List[0]['New']['title']?></h4>
                    <p class="an_box_title"><?=$top6List[0]['New']['description']?></p> 
                    </a>
                </div>
                <div class="sele_ques">
                    <p>往期回顾</p>
                    <ul>
                        <?php  foreach($top6List as $key => $value):?>
                        <?php if($key>=1){?>
                        <li><a href="/news/info/<?php echo $value["New"]["id"].".html";?>"><?=$value['New']['title']?></a></li>
                        <?php }?>
                        <?php endforeach;?>   
                    </ul>
                </div>
                <?php }?>
            </div>
        </div>
    </div>
	   <div class="index_one">
        <div class="ztitle">
            为什么那么多人选择去泰国
        </div>
        <div class="ftitle">
            成功率才是大家口口相传的关键
        </div>
    </div>
    <div class="index_four">
        <div class="ztitle">
            简单三步，出国备孕其实很方便
        </div>
        <div class="ftitle">
            10分钟预约医生，28天圆您母亲梦！
        </div>
        <div class="cont">
            <img src="/images/common/grey.gif" data-original="/images/index/buzou_01.jpg" class="lazy" alt="联系试管无忧就医顾问 泰国试管婴儿攻略 试管无忧">
            <img src="/images/common/grey.gif" data-original="/images/index/buzou_02.jpg" class="lazy" alt="合理规划就医行程 泰国试管婴儿攻略 试管无忧">
            <a href="http://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn" target="_blank" width="500px" height="500px"><img src="/images/common/grey.gif" data-original="/images/index/buzou_03.jpg" class="mr lazy" alt="体验一对一个性治疗 泰国试管婴儿攻略 试管无忧"></a>
        </div>
    </div>
        <div class="index_three">
        <div class="ztitle">
            顶尖试管医院任你挑           
        </div>
        <div class="ftitle">
            对接全球医疗资源，优质试管婴儿医院随便选
        </div>
        <div class="index02_content">
            <p id="A" class="index02_content03">
                <span class="span01">认证推荐</span>
                <span id="newstab1" onclick="MenuTabNews('newstab',1,2)" class="newsoverfirst">
                    知名医院
                </span>
                <span id="newstab2" onclick="MenuTabNews('newstab',2,2)" class="newsoutlast">
                    知名医生
                </span>
                <span class="span02 link_hospital"><a href='/hospital/index/' target="_blank">查看医院列表>> </a></span>
                <span class="span02 link_doctor hide"><a href='/doctor/index/' target="_blank">查看医生列表>> </a></span>
            </p> 
            <div id="B">
              <div  id="newstabcon1" class="index02_content04">
                <div class="index02_content04_left"></div>
                <div class="index02_content04_middle clearfix">
                    <div class='fl'>
                        <a href="/hospital/info/1.html" target="_blank"><img src="/images/common/grey.gif" data-original="/images/index/hosp/01.png" alt="帕亚泰是拉差 试管无忧" class="lazy" border="0"></a>
                    </div>
                    <div class='fl'>
                        <a href="/hospital/info/4.html" target="_blank"><img src="/images/common/grey.gif" data-original="/images/index/hosp/02.png" alt="泰国iBaby生殖中心 试管无忧" class="lazy" border="0"></a>
                    </div>
                    <div class='fl'>
                        <a href="/hospital/info/9.html" target="_blank"><img src="/images/common/grey.gif" data-original="/images/index/hosp/03.png" alt="泰国全球生殖中心 试管无忧" class="lazy" border="0"></a>
                    </div>
                    <div class='fl'>
                        <a href="/hospital/info/5.html" target="_blank"><img src="/images/common/grey.gif" data-original="/images/index/hosp/04.png" alt="泰国康民国际医院 试管无忧" class="lazy" border="0"></a>
                    </div>
                    <div class='fl'>
                        <a href="/hospital/info/7.html" target="_blank"><img src="/images/common/grey.gif" data-original="/images/index/hosp/05.png" alt="泰国SUPERIOR A.R.T中心 试管无忧" class="lazy" border="0"></a>
                    </div>
                    <div class='fl'>
                        <a href="/hospital/info/3.html" target="_blank"><img src="/images/common/grey.gif" data-original="/images/index/hosp/06.png" alt="泰国BNH医院 试管无忧" class="lazy" border="0"></a>
                    </div>
                    <div class='fl'>
                        <a href="/hospital/info/2.html" target="_blank"><img src="/images/common/grey.gif" data-original="/images/index/hosp/07.png" alt="杰特宁 试管无忧" class="lazy" border="0"></a>
                    </div>
                    <div class='fl'>
                        <a href="/hospital/info/8.html" target="_blank"><img src="/images/common/grey.gif" data-original="/images/index/hosp/08.png" alt="泰国曼谷安全生殖中心 试管无忧" class="lazy" border="0"></a>
                    </div>
                    <div class='fl'>
                        <a href="/hospital/info/10.html" target="_blank"><img src="/images/common/grey.gif" data-original="/images/index/hosp/09.png" alt="暹罗试管生殖中心 试管无忧" class="lazy" border="0"></a>
                    </div>
                        </div>
                <div class="index02_content04_right">
                    <dl>
                        <div class='clearfix' style='width:230px;height:50px'>
                            <div class='fl new_hospital_title'>热门医院</div>
                            <div class="line"></div>
                        </div>
                        <a href="/hospital/info/2.html" target="_blank" >
                        <div class='new_hospital_rows clearfix  '>
                            <div class='fl hospital_logo_box'>
                                <img src="/images/index/hosp/10.png" alt="试管无忧">
                            </div>
                            <div class='fl hospital_name_box'>
                                泰国杰特宁医院<br>(Jetanin)<br><span>泰国</span>
                            </div>
                            
                        </div>
                        </a>
                        <a href="/hospital/info/10.html" target="_blank">
                        <div class='new_hospital_rows clearfix  '>
                            <div class='fl hospital_logo_box'>
                                <img src="/images/index/hosp/11.png" alt="泰国试管婴儿攻略 试管无忧">
                            </div>
                            <div class='fl hospital_name_box'>
                                暹罗试管生殖中心<br>(SIAM FERTILITY)<br><span>泰国</span>
                            </div>
                            
                        </div>
                        </a>
                        <a href="/hospital/info/5.html" target="_blank">
                        <div class='new_hospital_rows clearfix last '>
                            <div class='fl hospital_logo_box'>
                                <img src="/images/index/hosp/12.png" alt="泰国试管婴儿攻略 试管无忧">
                            </div>
                            <div class='fl hospital_name_box'>
                                泰国康民国际医院<br>(Bumrungrad)<br><span>泰国</span>
                            </div>   
                        </div>
                        </a>
                    </dl>
                </div>        
              </div>
              <div  id="newstabcon2" style="display:none;" class="index02_content04">
                <div class="index02_content04_left"></div>
                <div class="index02_content04_bottom">
                    <ul>
                    <a href="/doctor/info/4.html" target="_blank">
                        <li>
                            <div class="kuai">
                                <div class='header_box'>
                                    <div class='out'>
                                        <div class='mid'>
                                            <div class='in'>
                                                <img class='doctor_head_img' src="/images/index/doct/13.jpg" alt="维苏 试管无忧">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="intro">
                                    <p>Dr.Visut</p>
                                    <div>维苏</div>
                                    <p></p>
                                </div>    
                            </div>
                        </li>
                    </a>
                    <a href="/doctor/info/21.html" target="_blank">
                        <li>
                            <div class="kuai">
                                <div class='header_box'>
                                    <div class='out'>
                                        <div class='mid'>
                                            <div class='in'>
                                                <img class='doctor_head_img' src="/images/index/doct/02.jpg" alt="察拆 试管无忧">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="intro">
                                    <p>Dr.Chartchai</p>
                                    <div>察拆</div>
                                    <p></p>
                                </div>    
                            </div>
                        </li>
                        </a>
                        <a href="/doctor/info/11.html" target="_blank">
                        <li>
                            <div class="kuai">
                                <div class='header_box'>
                                    <div class='out'>
                                        <div class='mid'>
                                            <div class='in'>
                                                <img class='doctor_head_img' src="/images/index/doct/03.png" alt="优冠 试管无忧">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="intro">
                                    <p>Dr.Yoko</p>
                                    <div>优冠</div>
                                    <p></p>
                                </div>    
                            </div>
                        </li>
                        </a>
                        <a href="/doctor/info/6.html" target="_blank">
                        <li>
                            <div class="kuai">
                                <div class='header_box'>
                                    <div class='out'>
                                        <div class='mid'>
                                            <div class='in'>
                                                <img class='doctor_head_img' src="/images/index/doct/04.jpg" alt="钟杰 试管无忧">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="intro">
                                    <p>Dr.Jongjate</p>
                                    <div>钟杰</div>
                                    <p></p>
                                </div>    
                            </div>
                        </li>
                        </a>
                        <a href="/doctor/info/14.html" target="_blank">
                        <li>
                            <div class="kuai">
                                <div class='header_box'>
                                    <div class='out'>
                                        <div class='mid'>
                                            <div class='in'>
                                                <img class='doctor_head_img' src="/images/index/doct/15.jpg" alt="潘亚 试管无忧">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="intro">
                                    <p>Dr.Panya</p>
                                    <div>潘亚</div>
                                    <p></p>
                                </div>    
                            </div>
                        </li>
                        </a>
                        <a href="/doctor/info/10.html" target="_blank">
                        <li>
                            <div class="kuai">
                                <div class='header_box'>
                                    <div class='out'>
                                        <div class='mid'>
                                            <div class='in'>
                                                <img class='doctor_head_img' src="/images/index/doct/06.jpg" alt="披娅潘 试管无忧">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="intro">
                                    <p>Dr.Piyaphan</p>
                                    <div>披娅潘</div>
                                    <p></p>
                                </div>    
                            </div>
                        </li>
                        </a>
                        <a href="/doctor/info/12.html" target="_blank">
                        <li>
                            <div class="kuai">
                                <div class='header_box'>
                                    <div class='out'>
                                        <div class='mid'>
                                            <div class='in'>
                                                <img class='doctor_head_img' src="/images/index/doct/14.jpg" alt="燕威娜 卡露 试管无忧">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="intro">
                                    <p>Dr.Weena</p>
                                    <div>燕威娜 卡露</div>
                                    <p></p>
                                </div>    
                            </div>
                        </li>
                        </a>
                        <a href="/doctor/info/15.html" target="_blank">
                        <li>
                            <div class="kuai">
                                <div class='header_box'>
                                    <div class='out'>
                                        <div class='mid'>
                                            <div class='in'>
                                                <img class='doctor_head_img' src="/images/index/doct/08.jpg" alt="提迪贡 试管无忧">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="intro">
                                    <p>Dr.Thitikorn</p>
                                    <div>提迪贡</div>
                                    <p></p>
                                </div>    
                            </div>
                        </li>
                        </a>
                        <a href="/doctor/info/17.html" target="_blank">
                        <li>
                            <div class="kuai">
                                <div class='header_box'>
                                    <div class='out'>
                                        <div class='mid'>
                                            <div class='in'>
                                                <img class='doctor_head_img' src="/images/index/doct/09.jpg" alt="维瓦 试管无忧">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="intro">
                                    <p>Dr.Viwat</p>
                                    <div>维瓦</div>
                                    <p></p>
                                </div>    
                            </div>
                        </li>
                        </a>
                        <a href="/doctor/info/1.html" target="_blank">
                        <li>
                            <div class="kuai">
                                <div class='header_box'>
                                    <div class='out'>
                                        <div class='mid'>
                                            <div class='in'>
                                                <img class='doctor_head_img' src="/images/index/doct/10.jpg" alt="披实 试管无忧">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="intro">
                                    <p>Dr.Pisit</p>
                                    <div>披实</div>
                                    <p></p>
                                </div>    
                            </div>
                        </li>
                        </a>
                        <a href="/doctor/info/9.html" target="_blank">
                        <li>
                            <div class="kuai">
                                <div class='header_box'>
                                    <div class='out'>
                                        <div class='mid'>
                                            <div class='in'>
                                                <img class='doctor_head_img' src="/images/index/doct/11.jpg" alt="宋杰 试管无忧">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="intro">
                                    <p>Dr.Somjate</p>
                                    <div>宋杰</div>
                                    <p></p>
                                </div>    
                            </div>
                        </li>
                        </a>
                        <a href="/doctor/info/22.html" target="_blank">
                        <li>
                            <div class="kuai">
                                <div class='header_box'>
                                    <div class='out'>
                                        <div class='mid'>
                                            <div class='in'>
                                                <img class='doctor_head_img' src="/images/index/doct/12.jpg" alt="苏拉差 试管无忧 泰国试管婴儿医院">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="intro">
                                    <p>Dr.Surachai</p>
                                    <div>苏拉差</div>
                                    <p></p>
                                </div>    
                            </div>
                        </li>
                        </a>
                    </ul>
                </div>
                <div class="index02_content04_right">
                    <dl>
                        <div class='clearfix' style='width:230px;height:50px'>
                            <div class='fl new_hospital_title'>热门医生</div>
                            <div class="line"></div>
                        </div>
                        <ul>
                        <a href="/doctor/info/18.html" target="_blank">
                            <li>
                                <div class="kuai">
                                    <div class='header_box'>
                                        <div class='out'>
                                            <div class='mid'>
                                                <div class='in'>
                                                    <img class='doctor_head_img' src="/images/index/doct/01.jpg" alt="查隆宛 试管无忧">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="intro">
                                        <p>Dr.Chalomkwan</p>
                                        <div>查隆宛</div>
                                        
                                        <!-- <div class="line01"></div> -->
                                    </div>
                                </div>
                            </li>
                            </a>
                            <a href="/doctor/info/20.html" target="_blank">
                            <li>
                                <div class="kuai">
                                    <div class='header_box'>
                                        <div class='out'>
                                            <div class='mid'>
                                                <div class='in'>
                                                    <img class='doctor_head_img' src="/images/index/doct/07.jpg" alt="简医生 试管无忧">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="intro">
                                        <p>Dr.SASAWIMOL</p>
                                        <div>简医生</div>
                                        <div class="line02" style="top: 125px"></div>
                                    </div>
                                </div>
                            </li>
                            </a>
                            <a href="/doctor/info/8.html" target="_blank">
                            <li>
                                <div class="kuai">
                                    <div class='header_box'>
                                        <div class='out'>
                                            <div class='mid'>
                                                <div class='in'>
                                                    <img class='doctor_head_img' src="/images/index/doct/05.jpg" alt="科林柴 试管无忧">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="intro">
                                        <p>Dr.Kriengchai</p>
                                        <div>科林柴</div>
                                        <div class="line01"  style="top: 208px"></div>
                                    </div>
                                </div>
                            </li>
                            </a>
                        </ul>
                    </dl>
                </div>        
              </div> 
            </div> 
        </div>    
    </div>
    <div class="index_two">
        <div class="ztitle">为什么大家都找试管无忧</div>
        <div class="ftitle">直到遇见试管无忧，才知道这么多年做泰国试管婴儿一直在走弯路</div>
        <div class="index_two_cont">
            <div class="tleft"><img src="/images/common/grey.gif" data-original="/images/index/diannao.jpg" class="lazy" alt="泰国试管婴儿多少钱 泰国试管婴儿费用 
             试管无忧"></div>
            <div class="tright">
                <div class="two_box pic1"><span>我们不是中介、不赚差价</span>不会只推荐患者去自己合作的个别医院，也不会利用患者的信息闭塞，赚取高昂的中介费或医疗差价。</div>
                <div class="two_box pic2"><span>聚焦疑难不孕人群</span>我们是国内首家也是唯一一家专注疑难试管的服务医院，并且在疑难病例上有着丰富的资源和服务经验。</div>
                <div class="two_box pic3"><span>服务方式更精准</span>我们会结合患者的个性情况，去寻找、匹配、预约最适合的医院和医生并提供更有针对性的服务。</div>
                <div class="two_box pic4"><span>“ 五心 ”服务，感动体验</span>我们倡导省心、放心、安心、贴心、热心的“五心”服务，感动式服务。</div>
            </div>
        </div>    
    </div>
    <div class="index_five">
        <div class="ztitle">精准的服务方式，让您的就医更加自信</div>
        <div class="ftitle">
            试管无忧帮您做到个性预约、一对一治疗，让您省心省力！
        </div>
        <div class="nav-arrow">
            <a class="arrow prev swiper-prev"><i></i></a>
            <a class="arrow next swiper-next"><i></i></a>
        </div>
        <div class="index_five_cont">
            <div class="goodness">
                <div class="section-content">
                    <div id="Goodness" class="slide swiper-container">
                        <ul class="swiper-wrapper">
                            <li class="swiper-slide">
                                <a href="/service/booking/" class="index_five_01"><div class="ab"><img alt="【精准预约】泰国试管婴儿攻略 试管无忧" src="/images/index/b-1.jpg"></div><div class="top1"><span class="right">￥<span>7,999</span>元</span>【精准预约】结合个性情况，匹配名医、指...<br><span>匹配名医 · 指定预约 · 个性导诊</span></div><div class="bottom1">解决就诊期间语言障碍<br>对国外就诊医院不熟悉等问题<br>提高出国就医效率</div></a>
                            </li>
                            <li class="swiper-slide">  
                                <a href="/service/vip/" class="index_five_01 b2 "><div class="ab"><img alt="【VIP尊享】泰国试管婴儿攻略 泰国试管婴儿费用 试管无忧" src="/images/index/b-2.jpg"></div><div class="top1"><span class="right">￥<span>16,800</span>元</span>【VIP尊享】为国内特殊的不孕不育患者提..<br><span>特殊需求 · 严格甄选 · 绿色通道</span></div><div class="bottom1">解决特需服务特殊需求，提供优质资源<br>对相关志愿者严格把关，无后顾之忧<br>打通医院绿色就医通道</div></a>
                            </li>
                            <li class="swiper-slide">
                                <a href="/service/gold/" class="index_five_01 b3 "><div class="ab"><img alt="【金牌无忧】 泰国试管婴儿攻略 试管无忧" src="/images/index/b-3.jpg"></div><div class="top1"><span class="right">￥<span>36,000</span>元</span>【金牌无忧】针对部分需要有更好服务体...<br><span>全程陪诊 · 星级酒店 · 医食无忧</span></div><div class="bottom1">包含所有的基础服务套餐<br>解决了在泰国期间生活起居问题<br>真正做到“医食无忧”</div></a>
                            </li>
                            <li class="swiper-slide">
                                <a href="/service/remote/" class="index_five_01 b4 "><div class="ab"><img alt="【远程咨询】泰国试管婴儿攻略 泰国试管婴儿医院 试管无忧" src="/images/index/b-4.jpg"></div><div class="top1"><span class="right">￥<span>1999</span>元</span>【远程咨询】为国内多年不孕多次失败的患者提...<br><span>问诊服务 · 分析病情 · 提供依据</span></div><div class="bottom1">提供境外医生一对一问诊服务<br>分析病情、找出失败的真正原因<br>为后续的治疗提供最佳依据</div></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <div class="index_seven">
        <div class="ztitle">
            远程问诊，与泰国试管婴儿名医<span>零距离</span>
        </div>
        <div class="ftitle">
            每月前50名可获取免费问诊名额
        </div>
        <form  class="dataForm" action="/main/index" id="regForm" method="post" >
        <div class="seven_cont">
            <!-- <select class="select" name="symptoms" id="symptoms" class="select b2" >
                <option value="0" disabled selected>请选择您想咨询的病症</option>
                <?php foreach ($disease as $key => $value) :?>
                <option value="<?php echo $key;?>"><?php echo $value;?></option>
                <?php endforeach?> 
            </select>
            <div class="div222"><div class="bg2"></div></div> -->

            <div class="test_3" >
                <select id="test_3" name="symptoms" id="symptoms">
                    <option value="0" disabled selected>请选择您想咨询的病症</option>
                    <?php foreach ($disease as $key => $value) :?>
                    <option value="<?php echo $key;?>"><?php echo $value;?></option>
                    <?php endforeach?> 
                </select>
            </div>

            <input class="input bg1" type="text"  placeholder="您的姓名" id='nickname'  name="u_name"  datatype="s2-10" errormsg="姓名格式不正确！" nullmsg="请填写姓名！">
            <input class="input bg3" type="text"  placeholder="您的手机" id="u_mobile" name="u_mobile"  datatype="m" errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！" autocomplete="off">
            <a class="submit">免费申请远程问诊</a>
            <div class="span"></div>
        </div>
            <input type="hidden" name="dosubmit" value="1"/>    
        </form>     
    </div>
	<div class="index_six">
		<div class="ztitle">
			千万试管婴儿妈妈,&nbsp;&nbsp;见证我们的成长
		</div>
		<div class="ftitle">
			大家的认可，是我们前进的动力
		</div>
		<div class="cont">
			<div class="cont_nav">
				<span id="navtab1" onclick="MenuTabnav('navtab',1,4)" class="navoverfirst">
                	就医现场
                </span>
                <span id="navtab2" onclick="MenuTabnav('navtab',2,4)" class="navout">
                    感恩回馈
                </span>
                <span id="navtab3" onclick="MenuTabnav('navtab',3,4)" class="navout">
                    公益活动
                </span>
                <span id="navtab4" onclick="MenuTabnav('navtab',4,4)" class="navoutlast">
                    新闻报道
                </span>
			</div>
			<div class="cont_cont">
				<div id="navtabcon1">
					<div class="pbox1">
						<div class="text1">轻松舒适的就医环境</div>
						<div class="text2">试管无忧拥有专业的服务团队，优质的国外试管婴儿医院资源，为用户提供更有针对性的精准服务。全面专业，陪诊一对一，最大化提升就医效率和成功率。</div>
					</div>
					<div class="pbox2">
						<img src="/images/index/nav1/p_01.jpg" alt="就医现场 泰国试管婴儿攻略 试管无忧">
					</div>
					<div class="pbox2">
						<img src="/images/index/nav1/p_02.jpg" alt="就医现场 泰国试管婴儿攻略">
					</div>
					<div class="pbox2">
						<img src="/images/index/nav1/p_03.jpg" alt="就医现场 泰国试管婴儿攻略 试管无忧">
					</div>
					<div class="pbox2">
						<img src="/images/index/nav1/p_04.jpg" alt="就医现场 泰国试管婴儿攻略 试管无忧">
					</div>
					<div class="pbox3">
						<img src="/images/index/nav1/p_07.jpg" alt="就医现场 泰国试管婴儿攻略 试管无忧">
					</div>
					<div class="pbox2">
						<img src="/images/index/nav1/p_05.jpg" alt="就医现场 泰国试管婴儿攻略 试管无忧" >
					</div>
					<div class="pbox2">
						<img src="/images/index/nav1/p_06.jpg" alt="就医现场 泰国试管婴儿攻略 试管无忧">
					</div>
				</div>
				<div id="navtabcon2" style="display:none;">
                    <div class="pbox1">
                        <div class="text1">试管无忧 真诚为你</div>
                        <div class="text2">试管无忧以真诚真心对每一位患者提供有价值的感动师服务，始终为患者创造价值、尊重客户的隐私和知情权，在国际医疗上保持着令人惊讶的医患反馈。
                    </div>
                    </div>
                    <div class="pbox2">
                        <img src="/images/index/nav2/ga01.jpg" alt="感恩回馈 试管无忧">
                    </div>
                    <div class="pbox2">
                        <img src="/images/index/nav2/ga02.jpg" alt="感恩回馈 试管无忧">
                    </div>
                    <div class="pbox2">
                        <img src="/images/index/nav2/ga03.jpg" alt="感恩回馈 泰国试管婴儿攻略 试管无忧">
                    </div>
                    <div class="pbox2">
                        <img src="/images/index/nav2/ga04.jpg" alt="感恩回馈 试管无忧">
                    </div>
                    <div class="pbox3">
                        <img src="/images/index/nav2/ga05.jpg" alt="感恩回馈 试管无忧">
                    </div>
                    <div class="pbox2">
                        <img src="/images/index/nav2/ga06.jpg" alt="感恩回馈 泰国试管婴儿攻略 试管无忧">
                    </div>
                    <div class="pbox2">
                        <img src="/images/index/nav2/ga07.jpg" alt="感恩回馈 试管无忧">
                    </div>            
                </div>
				<div id="navtabcon3" style="display:none;">
                    <div class="pbox1">
                        <div class="text1">不忘初心，才能越阻远行</div>
                        <div class="text2">试管无忧创造价值，更注重社会责任，始终积极投身各类公益事业，奉献爱心，传递着人间的温暖，用实际行动书写一个行业领导者的社会责任。</div>
                    </div>
                    <div class="pbox2">
                        <img src="/images/index/nav3/gy01.jpg" alt="公益活动 试管无忧">
                    </div>
                    <div class="pbox2">
                        <img src="/images/index/nav3/gy02.jpg" alt="公益活动 泰国试管婴儿攻略 试管无忧">
                    </div>
                    <div class="pbox2">
                        <img src="/images/index/nav3/gy03.jpg" alt="公益活动 试管无忧">
                    </div>
                    <div class="pbox2">
                        <img src="/images/index/nav3/gy04.jpg" alt="公益活动 试管无忧">
                    </div>
                    <div class="pbox3">
                        <img src="/images/index/nav3/gy05.jpg" alt="公益活动 试管无忧">
                    </div>
                    <div class="pbox2">
                        <img src="/images/index/nav3/gy06.jpg" alt="公益活动 泰国试管婴儿攻略 试管无忧">
                    </div>
                    <div class="pbox2">
                        <img src="/images/index/nav3/gy07.jpg" alt="公益活动 试管无忧">
                    </div>               
                </div>
				<div id="navtabcon4" style="display:none;">
                    <div class="pbox1">
                        <div class="text1">新闻资讯</div>
                        <div class="text2">获悉最新新闻与活动，详细了解我们的最新动态，我们以最大的热忱和超预期的服务体验来帮助每一位患者，让客户满意，让客户感动我们的唯一目的。</div>
                    </div>
                    <div class="pbox2">
                        <img src="/images/index/nav4/xw01.jpg" alt="新闻报道 试管无忧">
                    </div>
                    <div class="pbox2">
                        <img src="/images/index/nav4/xw02.jpg" alt="新闻报道 试管无忧">
                    </div>
                    <div class="pbox2">
                        <img src="/images/index/nav4/xw03.jpg" alt="新闻报道 泰国试管婴儿攻略 试管无忧">
                    </div>
                    <div class="pbox2">
                        <img src="/images/index/nav4/xw04.jpg" alt="新闻报道 试管无忧">
                    </div>
                    <div class="pbox3">
                        <img src="/images/index/nav4/xw05.jpg" alt="新闻报道 泰国试管婴儿攻略 试管无忧">
                    </div>
                    <div class="pbox2">
                        <img src="/images/index/nav4/xw06.jpg" alt="新闻报道 试管无忧">
                    </div>
                    <div class="pbox2">
                        <img src="/images/index/nav4/xw07.jpg" alt="新闻报道 试管无忧">
                    </div>   
                </div>
			</div>
		</div>
	</div>
    <div class="index_eight">
        <b>试管无忧的五心服务</b>
        <ul>
            <li>
                <span>省心</span><br>
                <em>一站式定制服务</em>
                <p>从线上报名、线下咨询、提交材料到泰国医院和最后的效果跟踪，我们全程提供专业的医疗咨询，协助办理全部手续。</p>
            </li>
            <li>
                <span>放心</span><br>
                <em>权威医疗机构</em>
                <p>我们有专业的泰国医学顾问团队，精挑细选只符合试管无忧医疗标准的全球顶级医疗机构和合作伙伴，为客户提供国际高水准的医疗服务。</p>
            </li>
            <li>
                <span>安心</span><br>
                <em>隐私和保障体系</em>
                <p>试管无忧为您提供多重医疗服务保障，我们尊重顾客的隐私和知情权，确保所有环节可追溯、确保客户隐私不被泄露。</p>
            </li>
            <li>
                <span>贴心</span><br>
                <em>一对一服务</em>
                <p>为保障患者就医效率，我们会为每一位患者规划出国就医行程安排，提供国外出行攻略，并对生活上提供一对一服务。</p>
            </li>
            <li>
                <span>热心</span><br>
                <em>感动式服务</em>
                <p>试管无忧的客服团队为每一位客户提供感动式服务。无论是远程咨询、出国试管还是出境体验，我们会7x24小时在线帮助您解决服务过程出现的任何问题。</p>
            </li>
        </ul>
    </div>
<div class="wap_middle" >
    <span class="xuqiu a1"><a href="/plan/index" style="width: 100%;height:100%;color:#f29c9f !important;background:#fff;border-radius:5px;">泰国试管攻略</a></span>
    <a class="wx6 a2" href="/hospital/index/">泰国医院排名</a>
    <a class="wx4 a3" href="/plan/index/3.html">医院费用</a>
    <a class="wx5 br0 shen a4" href="/plan/index/2.html">双促排技术</a>
    <a class="wx5 shen" href="/plan/index/2.html">PGD与NGS </a>
    <a class="wx6" href="/plan/index/8.html">疑难试管方案</a>
    <a class="wx6" href="/plan/index/2.html">技术与成功率</a>
    <a class="wx4 br0" href="plan/index/8.html">高龄试管</a>
</div>
<div class="wap_gray" ></div>
<div class="wap_one">
    <div class="wap_new_kuai">
        <div class="border_kuai"></div><div class="text_kuai">为什么那么多人选择泰国试管婴儿</div><div class="border_kuai"></div>
    </div>
    <div class="wap_pic">
        <div class="wap_gray_x"></div>
        <div class="wap_pic">
            <div class="p1">
                <span class="glyphicons glyphicons-zoom-in h1"></span>
            </div>
            <div class="p2 h8">囊胚移植<br>技术精湛</div>
            <div class="p1">
                <span class="glyphicons glyphicons-disk-open h2"></span>
            </div>
            <div class="p2 h8">高成功率<br>远胜国内</div>
            <div class="p1">
                <span class="glyphicons glyphicons-stethoscope h3"></span>
            </div>
            <div class="p2">专业医生<br>顶尖水平</div>
        </div>  
        <div class="wap_gray_x" ></div>
        <div class="wap_pic">
            <div class="p1">
                <span class="glyphicons glyphicons-usd h4"></span>
            </div>
            <div class="p2 h8">价格实惠<br><p name='tag_more_success'>性价比高</p></div>
            <div class="p1">
                <span class="glyphicons glyphicons-nameplate h5"></span>
            </div>
            <div class="p2 h8">政策宽松<br>只需护照</div>
            <div class="p1">
                <span class="glyphicons glyphicons-send h6"></span>
            </div>
            <div class="p2">临近中国<br>来去便利</div>
        </div>      
        <div class="wap_gray_x"></div>
    </div>
</div>
<div class="wap_gray" ></div>
<div class="wap_six">
    <div class="wap_new_kuai">
        <div class="border_kuai"></div><div class="text_kuai">如何实现高效的出国就医</div><div class="border_kuai"></div>
    </div>
    <div class="cont">
        <img src="/images/index/00.jpg" alt="泰国试管婴儿费用 泰国试管婴儿医院 泰国试管婴儿攻略 试管无忧" width="100%;" class="video_btn">
    </div>
    <div class="text">努力不一定有成果，方法对了才能事半功倍</div>   
</div>
<div class="wap_gray" ></div>
<div class="wap_seven">
    <div class="wap_new_kuai">
        <div class="border_kuai"></div><div class="text_kuai">简单三步，助您轻松好孕</div><div class="border_kuai"></div>
    </div>
    <div class="cont">
        <div class="text1">诊前咨询与预约<br><span>诊前报告评估，精准预约名医</span></div>
        <div class="text2">一对一个性治疗<br><span>诊中行程提醒，全程翻译，个性导诊</span></div>
        <div class="opic"><img src="/images/index/tu01.png" alt="泰国试管婴儿医院 泰国试管婴儿费用 泰国试管婴儿攻略 试管无忧"></div>
        <div class="text3">验孕回国<br><span>诊后关怀，保胎跟踪，孕期指导</span></div>
        <div class="opic"><img src="/images/index/tu02.png" alt="泰国试管婴儿多少钱 泰国试管婴儿费用 泰国试管婴儿攻略 试管无忧"></div>
    </div>
    <a href="http://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn&p=sm" class="more">我要约医生</a>
</div>
<div class="wap_gray"></div>
<div class="wap_eight">
    <div class="wap_new_kuai">
        <div class="border_kuai"></div><div class="text_kuai">为什么选择试管无忧</div><div class="border_kuai"></div>
    </div>
    <div class="cont">
        <div class="qiqiu">
            <div class="conbox1"><img src="/images/index/yuan1.png" alt="泰国试管婴儿多少钱 泰国试管婴儿医院 泰国试管婴儿攻略 试管无忧"/></div>
            <div class="conbox2 pl"><span>不是中介</span><br>
            中介机构会利用患者对泰国试管婴儿医院的信息闭塞以赚取中介费或高昂的医疗费差价，而试管无忧从不赚差价，只收取合理的服务费用。</div>
        </div>
        <div class="qiqiu">
            <div class="conbox2 pr" style="margin-right:15px; margin-left: 0;"><span>服务更精准</span><br>
            结合您的就医情况，为您提供更及时、更可靠的就医服务，让您不再为就医而烦恼。</div>
            <div class="conbox1"><img src="/images/index/yuan2.png" alt="泰国试管婴儿费用 泰国试管婴儿医院 泰国试管婴儿攻略 试管无忧" /></div>
        </div>
        <div class="qiqiu">
            <div class="conbox1"><img src="/images/index/yuan3.png" alt="泰国试管婴儿多少钱 泰国试管婴儿费用 试管无忧" /></div>
            <div class="conbox2 pl"><span>聚焦疑难不孕</span><br>
            国内首家“专注疑难试管”的服务医院，只做疑难人群，在疑难病例上，有着丰富的资源和服务经验。</div>
        </div>
    </div>
</div>
<div class="wap_gray"></div>
<div class="wap_ele">
    <div class="wap_new_kuai">
        <div class="border_kuai"></div><div class="text_kuai">对接泰国顶尖医疗资源</div><div class="border_kuai"></div>
        <div class="cont">
            <div class="box"><a href="/hospital/info/2.html"><img src="/images/index/hosp_09.jpg" border="0" alt="泰国试管婴儿多少钱 泰国试管婴儿费用 试管无忧"></a></div>
            <div class="box"><a href="/hospital/info/1.html"><img src="/images/index/hosp_05.jpg" border="0" alt="泰国试管婴儿多少钱 泰国试管婴儿医院 泰国试管婴儿攻略 试管无忧"></a></div>
            <div class="box"><a href="/hospital/info/3.html"><img src="/images/index/hosp_03.jpg" border="0" alt="泰国试管婴儿医院 泰国试管婴儿费用 泰国试管婴儿攻略 试管无忧"></a></div>
            <div class="box"><a href="/hospital/info/9.html"><img src="/images/index/hosp_10.jpg" border="0" alt="泰国试管婴儿多少钱 泰国试管婴儿费用 泰国试管婴儿攻略 试管无忧"></a></div>
        </div>
        <a href="/hospital/index/" class="more">我要找医院</a>
    </div>
</div>
<div class="wap_gray"></div>
<div class="wap_four">
    <div class="wap_new_kuai">
        <div class="border_kuai"></div><div class="text_kuai">精准服务,&nbsp;&nbsp;让就医更自信</div><div class="border_kuai"></div>
    </div>
    <div class="case">
        <div class="pic1 pic2 pic3"><a href="/service/booking/"><img src="/images/index/fw_04.jpg" alt="精准预约 泰国试管婴儿攻略 试管无忧" />
        <span>【精准预约】结合个性情况,匹配名医、指定预约...</span><p>￥<span>7,999</span><span class="spoo">预约数：556</span></p></a></div>
        <div class="pic1 pic3"><a href="/service/remote/"><img src="/images/index/fw_01.jpg" alt="远程咨询 泰国试管婴儿攻略 试管无忧"  />
        <span>【远程咨询】为国内多年不孕多次失败的患...</span><p>￥<span>699</span><span class="spoo">预约数：1085</span></p></a></div>
        <div class="pic1 pic2 pic3"><a href="/service/vip/"><img src="/images/index/fw_03.jpg"  alt="VIP尊享 泰国试管婴儿攻略 试管无忧" />
        <span>【VIP尊享】为国内特殊的不孕不育患者，提供...</span><p>￥<span>16,800</span><span class="spoo">预约数：459</span></p></a></div>
        <div class="pic1 pic3"><a href="/service/gold/"><img src="/images/index/fw_02.jpg" alt="金牌无忧 泰国试管婴儿攻略 试管无忧"/>
        <span>【金牌无忧】针对部分希望有更好服务体验的患者...</span><p>￥<span>36,000</span><span class="spoo">预约数：742</span></p></a></div>
    </div>
    <a href="http://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn&p=sm" class="more">我想问费用</a>
</div>
<div class="wap_gray"></div>
<div class="wap_ten">
    <div class="wap_new_kuai">
        <div class="border_kuai"></div><div class="text_kuai">千万试管婴儿妈妈,&nbsp;&nbsp;见证我们的成长</div><div class="border_kuai"></div>
    </div>
    <div class="ten_nav">
        <span id="mavtab1" onclick="MenuTabmav('mavtab',1,4)" class="mavoverfirst">
            就医现场
        </span>
        <span id="mavtab2" onclick="MenuTabmav('mavtab',2,4)" class="mavout">
            感恩回馈
        </span>
        <span id="mavtab3" onclick="MenuTabmav('mavtab',3,4)" class="mavout">
            公益活动
        </span>
        <span id="mavtab4" onclick="MenuTabmav('mavtab',4,4)" class="mavoutlast">
            新闻报道
        </span>
    </div>
    <div class="cont">
        <div id="mavtabcon1">
            <div class="obox"><img src="/images/index/atr/at1_1.jpg" alt="就医现场 试管无忧"></div>
            <div class="obox"><img src="/images/index/atr/at1_2.jpg" alt="就医现场 泰国试管婴儿攻略 试管无忧"></div>
            <div class="obox"><img src="/images/index/atr/at1_3.jpg" alt="就医现场 试管无忧"></div>
            <div class="obox"><img src="/images/index/atr/at1_4.jpg" alt="就医现场 泰国试管婴儿攻略 试管无忧"></div>
        </div>
        <div id="mavtabcon2" style="display: none;">
            <div class="pbox"><img src="/images/index/atr/at2_1.jpg" alt="感恩回馈 泰国试管婴儿攻略 试管无忧"></div>
            <div class="pbox"><img src="/images/index/atr/at2_2.jpg" alt="感恩回馈 试管无忧"></div>
            <div class="pbox"><img src="/images/index/atr/at2_3.jpg" alt="感恩回馈 泰国试管婴儿攻略 试管无忧"></div>
            <div class="pbox"><img src="/images/index/atr/at2_4.jpg" alt="感恩回馈 试管无忧"></div>
        </div>
        <div id="mavtabcon3" style="display: none;">
            <div class="qbox"><img src="/images/index/atr/at3_1.jpg" alt="公益活动 泰国试管婴儿攻略 试管无忧"></div>
            <div class="qbox"><img src="/images/index/atr/at3_2.jpg" alt="公益活动 试管无忧"></div>
            <div class="qbox"><img src="/images/index/atr/at3_3.jpg" alt="公益活动 泰国试管婴儿攻略 试管无忧"></div>
            <div class="qbox"><img src="/images/index/atr/at3_4.jpg" alt="公益活动 试管无忧"></div>
        </div>
        <div id="mavtabcon4" style="display: none;">
            <div class="rbox"><img src="/images/index/atr/at4_1.jpg" alt="新闻报道 试管无忧"></div>
            <div class="rbox"><img src="/images/index/atr/at4_2.jpg" alt="新闻报道 泰国试管婴儿攻略 试管无忧"></div>
            <div class="rbox"><img src="/images/index/atr/at4_3.jpg" alt="新闻报道 泰国试管婴儿攻略 试管无忧"></div>
            <div class="rbox"><img src="/images/index/atr/at4_4.jpg" alt="新闻报道 泰国试管婴儿攻略 试管无忧"></div>
        </div>
    </div>
    <a href="http://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn&p=sm" id="more">我也要好孕</a>
</div>
<div class="wap_gray"></div>
<div class="wap_five">
    <div class="five_text">
        远程问诊，与泰国试管名医<span>零距离</span>
    </div>
    <div class="fivetext">每月前50名可获取免费问诊名额</div>
    <div class="from">
        <form  class="dataForm2" action="/main/index" id="regForm" method="post" >
            <select class="select" name="symptoms" id="symptoms" >
                <option value="0" disabled selected>请选择您想咨询的病症</option> 
                <?php foreach ($disease as $key => $value) :?>
                <option value="<?php echo $key;?>"><?php echo $value;?></option>
                <?php endforeach?> 
            </select>
            <input type="text" class="input" id="u_name"   name="u_name"  datatype="s2-10" errormsg="姓名格式不正确！" nullmsg="请填写姓名！"  placeholder="您的姓名" autocomplete="off">
            <input type="text" class="input"  name="u_mobile" id="u_mobile"  datatype="m" errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！"  placeholder="您的手机号">
            <a class="submit"/>领取免费名额</a>
            <div class="dw">
                <div class="yd_01"><span class="glyphicons glyphicons-article"></span></div>
                <div class="yd_02"><span class="glyphicons glyphicons-user"></span></div>
                <div class="yd_03"><span class="glyphicons glyphicons-earphone"></span></div>
            </div>
            <div class="span"></div>
            <input type="hidden" name="dosubmit" value="1"/>    
        </form>
    </div>
</div>
<div class="video_bf" style="display: none; width: 100%; height: 100%;">
    <div class="video_bg"></div>
     <div class="video_jm">
        <div class="video_top">
            <div class="video_close"><img src="/images/index/cha.png" alt="泰国试管婴儿多少钱 泰国试管婴儿费用 试管无忧"></div>
        </div>
        <div class="video_cont">
             <video controls="" preload="none" name="media" style="width: 960px; height:540px;" id="video1" >
            <source src="http://1.ivf51.com/be4dedcd3e64480fa635027a5ee3c9ac/4e851e3d6cde4ef6946bd33420dab22d-5287d2089db37e62345123a1be272f8b.mp4" type="video/mp4">
        </video>
        </div>
    </div>
</div>
<div class="mvideo_tbg"></div>
<div class="mvideo_bg">
    <div class="mvideo_top">
        <div class="mvideo_cha"></div>
    </div>
    <div class="mvideo_cont">
        <video controls="" preload="none" name="media" style="width: 100%;" id="video2">
            <source src="http://1.ivf51.com/9a1a8fc6d3294ef58ce0bc2386e9474c/334bc123a2164f869bf5f2bb47f97edc-5287d2089db37e62345123a1be272f8b.mp4" type="video/mp4">
        </video>
    </div>
</div> 
</div>
<script language="javascript" src="http://pht.zoosnet.net/JS/LsJS.aspx?siteid=PHT13532865&float=1&lng=cn"></script>
<script>
    // window.onload = function (argument) {
        var le = $(".an_box_title").text().length;
        console.log(le);
        if(le>33){
            $(".an_box_title").addClass('after');
        }
    // }
    
</script>
<?= $this->element('aide_pc'); ?>
<?= $this->element('leave'); ?>