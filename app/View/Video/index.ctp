<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<!--让网页的宽度自动适应手机屏幕的宽度。 其中： width=device-width ：表示宽度是设备屏幕的宽度 initial-scale=1.0：表示初始的缩放比例 minimum-scale=0.5：表示最小的缩放比例 maximum-scale=2.0：表示最大的缩放比例 user-scalable=yes：表示用户是否可以调整缩放比例-->
<title><?php echo isset($keysInfo['Keyword']['meta_title'])?$keysInfo['Keyword']['meta_title']:'试管无忧';?></title>
<meta name="keywords" content="<?php echo isset($keysInfo['Keyword']['meta_keywords'])?$keysInfo['Keyword']['meta_keywords']:'试管无忧';?>" />
<meta name="description" content="<?php echo isset($keysInfo['Keyword']['meta_description'])?$keysInfo['Keyword']['meta_description']:'试管无忧';?>" />
<meta name="format-detection" content="telephone=no" /><!--防止在iOS设备中的Safari将数字识别为电话号码-->
<link rel="stylesheet" type="text/css" href="/css/common.css">
<link rel="stylesheet" type="text/css" href="/css/glyphicons.css">
<link rel="stylesheet" type="text/css" href="/css/video/index.css">
<link rel="stylesheet" type="text/css" href="/css/jquery.mCustomScrollbar.css">
<script type="text/javascript" src="/js/jquery.1.8.3.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/jquery.lazyload.js"></script>
<script type="text/javascript" src="/js/jquery.mCustomScrollbar.concat.min.js"></script>  
<script type="text/javascript" src="/admin/js/Validform/Validform.min.js"></script> 
</head>
<body>
<div class="video">
	<div class="top">
		<div class="main"><a href="/community/index/">返回社区首页</a></div>
	</div>
	<div class="banner"></div>
	<div class="webban"><img src="/images/video/banner_meiti.png"></div>
	<div class="cont">
		<div class="main">
			<?php foreach ($topVideo as $k => $val):?>
			<div class="xbox">
				<div class="xtitle"><?php echo $val['VideoCate']['name'];?></div>
				<div class="conte">
					<div class="left"><img src="<?php echo $val['VideoCate']['img'];?>"></div>
					<div class="right">
						<div class="head">
						<?php echo $val['VideoCate']['name'];?>&nbsp;&nbsp;<span></span><br>
						<span>简介：<?php echo $val['VideoCate']['description'];?></span>
						</div>
						<div class="bottom mCustomScrollbar" data-mcs-theme="dark" style="overflow: hidden;">
							<?php if(sizeof($val['VideoCate']['sub']) > 0){?>
							<?php foreach ($val['VideoCate']['sub'] as $key => $value):?>
							<a class="sbox" href="/video/info/<?=$value['Video']['id']?>.html"><?= date('Y-m-d',strtotime($value['Video']['create_time'])) ?>：<?php if(strlen($value['Video']['title'])>28):?><?php echo mb_substr($value['Video']['title'],0,10);?>...<?php else:?><?php echo $value['Video']['title']?><?php endif;?><?php if($key=="0"):?><div class="newp"></div><?php endif;?></a>
							<?php endforeach;?>
							<?php }?>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach;?> 

			<div class="hbox" id="hbox">
				<div class="htitle">名医专访</div>
				<div class="contr">
					<div class="mbox">
						<a  href="/video/info/84.html"><img src="/images/video/j_01.png">
						<img class="play_b" src="/images/community/zx3.png" alt=""></a><span>苏查妲.梦琨蔡帕医生</span>
					</div>
					<div class="mbox">
						<a  href="javascript:void(0)" class="vid_btn"><img src="/images/video/j_02.png">
						<img class="play_b" src="/images/community/zx3.png" alt=""></a><span>维瓦医生专访</span>
					</div>
					<div class="mbox">
						<a  href="javascript:void(0)" class="vid_btn"><img src="/images/video/j_03.png">
						<img class="play_b" src="/images/community/zx3.png" alt=""></a><span>提迪贡博士专访</span>
					</div>
					<div class="mbox">
						<a  href="javascript:void(0)" class="vid_btn"><img src="/images/video/j_04.png">
						<img class="play_b" src="/images/community/zx3.png" alt=""></a><span>彼时医生专访</span>
					</div>
					<div class="mbox">
						<a  href="javascript:void(0)" class="vid_btn"><img src="/images/video/j_05.png">
						<img class="play_b" src="/images/community/zx3.png" alt=""></a><span>康民医院察拆博士专访</span>
					</div>
				</div>
			</div>
			<div class="hbox" id="hbox">
				<div class="htitle">客户案例</div>
				<div class="contr">
					<div class="mbox">
						<a  href="/video/info/94.html"><img src="/images/video/j_06.png"><img class="play_b" src="/images/community/zx3.png" alt=""></a><span>高龄二胎妈妈赴泰喜得男宝</span>
					</div>
					<div class="mbox">
						<a href="javascript:void(0)" class="vid_btn"><img src="/images/video/j_07.png"><img class="play_b" src="/images/community/zx3.png" alt=""></a><span>“一波三折”的高龄求子之路</span>
					</div>
					<div class="mbox">
						<a href="javascript:void(0)" class="vid_btn"><img src="/images/video/j_08.png"><img class="play_b" src="/images/community/zx3.png" alt=""></a><span>47岁陈女女士重燃孕育希望</span>
					</div>
					<div class="mbox">
						<a href="javascript:void(0)" class="vid_btn"><img src="/images/video/j_09.png"><img class="play_b" src="/images/community/zx3.png" alt=""></a><span>高龄妈妈孕育二胎</span>
					</div>
					<div class="mbox">
						<a href="javascript:void(0)" class="vid_btn"><img src="/images/video/j_10.jpg"><img class="play_b" src="/images/community/zx3.png" alt=""></a><span>陪诊一对一让出行更便捷</span>
					</div>
				</div>
			</div>
  
		</div>
		
		
		<div class="webmain">
			<div class="wtitle">无忧系列</div>
			<div class="ncont">
				<div class="nbox">
					<a href="/video/info/29.html"><img src="/images/video/m/m1.jpg"></a>
					<span>小忧说试管</span>
					<p>带您了解试管小知识</p>
				</div>
				<div class="nbox">
					<a href="/video/info/81.html"><img src="/images/video/m/m2.jpg"></a>
					<span>小忧说不孕</span>
					<p>全面普及不孕不育知识</p>
				</div>
				<div class="nbox">
					<a href="/video/info/96.html"><img src="/images/video/m/m3.jpg"></a>
					<span>泰国试管那些事儿</span>
					<p>了解泰国试管真实情况</p>
				</div>
				<div class="nbox">
					<a href="javascript:void(0)" class="vieo_btn"><img src="/images/video/m/m4.jpg"></a>
					<span>远程咨询</span>
					<p>不出国门深度咨询</p>
				</div>
			</div>
		</div> 
		<div class="webmain">
			<div class="wtitle">名医访谈</div>
			<div class="ncont">
				<div class="nbox">
					<a href="/video/info/84.html"><img src="/images/video/m/m5.jpg"></a>
					<span>苏查妲.梦琨蔡帕医生</span>
				</div>
				<div class="nbox">
					<a href="javascript:void(0)" class="vieo_btn"><img src="/images/video/m/m6.jpg"></a>
					<span>维瓦医生</span>
				</div>
				<div class="nbox">
					<a href="javascript:void(0)" class="vieo_btn"><img src="/images/video/m/m7.jpg"></a>
					<span>提迪贡医生</span>
				</div>
				<div class="nbox">
					<a href="javascript:void(0)" class="vieo_btn"><img src="/images/video/m/m8.jpg"></a>
					<span>察拆医生</span>
				</div>
			</div>
		</div> 
		<div class="webmain">
			<div class="wtitle">客户案例</div>
			<div class="ncont">
				<div class="nbox">
					<a href="/video/info/94.html"><img src="/images/video/j_06.png"></a>
					<span>高龄二胎妈妈赴泰生子</span>
					
				</div>
				<div class="nbox">
					<a href="javascript:void(0)" class="vieo_btn"><img src="/images/video/j_07.png"></a>
					<span>高龄求子之路</span>
					
				</div>
				<div class="nbox">
					<a href="javascript:void(0)" class="vieo_btn"><img src="/images/video/j_08.png"></a>
					<span>47岁陈女女士重燃希望</span>
					
				</div>
				<div class="nbox">
					<a href="javascript:void(0)" class="vieo_btn"><img src="/images/video/j_09.png"></a>
					<span>高龄妈妈孕育二胎</span>
					
				</div>
			</div>
		</div>
	<div class="vbottom">“试管无忧，专注疑难不孕”！&nbsp;&nbsp;沪ICP备14046762号</div>
</div>	
<?= $this->element('video'); ?>
</body>
</html>