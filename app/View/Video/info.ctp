 <!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<!--让网页的宽度自动适应手机屏幕的宽度。 其中： width=device-width ：表示宽度是设备屏幕的宽度 initial-scale=1.0：表示初始的缩放比例 minimum-scale=0.5：表示最小的缩放比例 maximum-scale=2.0：表示最大的缩放比例 user-scalable=yes：表示用户是否可以调整缩放比例-->
<title><?=$keysInfo['Keyword']['meta_title'];?></title>
<meta name="keywords" content="<?=$keysInfo['Keyword']['meta_keywords'];?>" />
<meta name="description" content="<?=$keysInfo['Keyword']['meta_description'];?>" />
<meta name="format-detection" content="telephone=no" /><!--防止在iOS设备中的Safari将数字识别为电话号码-->
<link rel="stylesheet" type="text/css" href="/css/common.css">
<link rel="stylesheet" type="text/css" href="/css/glyphicons.css">
<link rel="stylesheet" type="text/css" href="/css/video/info.css">
<link rel="stylesheet" type="text/css" href="/css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" href="/css/idangerous.swiper.css">
<script type="text/javascript" src="/js/jquery.1.8.3.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script> 
<script type="text/javascript" src="/admin/js/Validform/Validform.min.js"></script>
<script type="text/javascript" src="/js/jquery.mCustomScrollbar.concat.min.js"></script>   
<?php echo $this->Html->script('/js/public.js');?>   
<?php echo $this->Html->script('/js/page.js');?>   
</head>
<body>
<div class="video_info">
    <div class="top">
        <div class="main"><a href="/video/index/">返回列表</a></div>
    </div>
    <div class="bg_color">
    	<div class="video_cont" style="z-index: -1!important;">
			<div class="v_play" style="z-index: -1!important;">
                 <video  style="z-index: -1!important;" controls="" autoplay="autoplay" name="media" class="video_app">
                    <source style="z-index: -1!important;" src="<?=$info['Video']['url']?>" type="video/mp4">
                 </video>         
            </div>
			<div class="v_list">
				<p>往期回顾</p>
				<ul class="list mCustomScrollbar" data-mcs-theme="dark" >
                    <?php foreach ($nextVideo as $k => $val):?>
                    <a href="/video/info/<?=$val['Video']['id']?>.html">
					<li class="<?php echo $info['Video']['id']==$val['Video']['id']?'hov':'';?>">
						<img src="<?php echo isset($cateInfo['VideoCate']['img'])?$cateInfo['VideoCate']['img']:"";?>" data-original="<?php echo isset($cateInfo['VideoCate']['img'])?$cateInfo['VideoCate']['img']:"";?>">
						<div>
							<p><?php echo $val['Video']['title'];?></p>
							<span><?php echo $val['Video']['play_num'];?>万</span>
						</div>
					</li>
                    </a>
                    <?php endforeach;?>  
				</ul>
				<!-- <div class="icon b_right">
				    <a href="javascript:void(0)" class="b_pic3 <?php echo $info['Video']['myagree']>0?'add_up':'';?>" <?php if($info['Video']['myagree']==0){?>onclick="adAgree(this,<?=$info["Video"]["id"]?>,'agree_video')"<?php }?>><div class="icon_01"><i></i><span id="video_num"><?=$info['Video']['search_num']?></span></div></a>
				</div> -->
			</div>
		</div>
        <!-- <iframe style="width:100%;height:100px;z-index:100!important;background:red;position:absolute;top:0;left: 0;" frameborder="0"></iframe> -->
        <a href="/video/index/" class="v_back"></a>
		<div class="v_title">
			<h1><span><?php echo isset($cateInfo['VideoCate']['name'])?$cateInfo['VideoCate']['name']:"";?></span>：<?php echo $info['Video']['title'];?></h1>
			<div class="v_l"><a href="/community/index/">社区</a> > <a href="/video/index/">无忧自媒体</a> > <a href="#"><?php echo isset($cateInfo['VideoCate']['name'])?$cateInfo['VideoCate']['name']:"";?></a></div>
			<div class="v_c">
                <?php if(!empty($info['Video']['tags'])){ 
                    $tags=explode(",", $info['Video']['tags']);
                    foreach ($tags as $key => $value) {
                ?>
                <span><?php echo $value;?></span>
                <?php }}?>
            </div>
			<div class="v_r">
				播放：<?php echo $info['Video']['play_num'];?>
			</div>
		</div>
        <div class="v_list_m">
            <p>往期回顾</p>
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <?php foreach ($nextVideo as $k => $val):?>
                    <div class="swiper-slide <?php echo $info['Video']['id']==$val['Video']['id']?'hov_m':'';?>"><a href="/video/info/<?=$val['Video']['id']?>.html">
                        <img class="lazy" src="<?php echo isset($cateInfo['VideoCate']['img'])?$cateInfo['VideoCate']['img']:"";?>" data-original="<?php echo isset($cateInfo['VideoCate']['img'])?$cateInfo['VideoCate']['img']:"";?>">
                        <p><?php echo $val['Video']['title'];?></p></a>
                    </div>
                    <?php endforeach;?>   
                </div>
            </div>
        </div>
    </div>
    <div class="content">
    	<div class="cont_left">
            <p class="jjie">简介</p>
            <div class="jianjie"><?php echo $info['Video']['description'];?></div>
    		<p>评论<span>(<?php echo $cmtCount;?>)</span></p>
            <form  class="videoForm" action="/video/replay"  method="post" >
    		<div class="text_box"> 
                <textarea name="content"  placeholder="我来说两句" datatype="*"  nullmsg="请填写评论内容！"></textarea> 
    			<div class="text_submit">
                    <input type="button" class="video_btn" value="发布" />  
    			</div>
    		</div>
            <input type="hidden" name="cid" value="<?=$info["Video"]["id"]?>"/>  
            <input type="hidden" name="dosubmit" value="1"/>                 
            </form> 
            <?php if(!empty($comments)):?>
    		<div class="new">
    			<p>最新评论</p>
    			<ul>
                    <?php foreach ($comments as $key => $value):?> 
    				<li>
					 <div class="pinglu">
                     <div class="head_pic">
                            <img src="<?=$value['Comment']['avatar']?>">
                    </div>
                    <div class="conte">
                            <div class="namec"><?=$value['Comment']['username']?></div>
                            <div class="contc"><?=$value['Comment']['content']?></div>
                            <div class="timec"><?php echo Ivf::formatTime($value['Comment']['comment_time']);?></div>
                        </div>
                        </div>
    				</li>
                    <?php endforeach;?>   
    			</ul>
                <?php if(sizeof($comments) >= 10){?>
                <div class="more"> 
                    <a href="javascript:void(0)" title="" onclick="_page.getPageComment(this,<?=$info['Video']['id']?>,'video_comment')">加载更多</a>
                </div>
                <?php }?>
    		</div>
        <?php endif;?>
    	</div>
    	<div class="cont_right">
    		<p>热门节目</p>
    		<ul>
                <?php foreach ($hotVideo as $k => $val):?>
                <li class="<?php echo $k<=3 ? 'xuanzhong':''?>"><a href="/video/info/<?=$val['Video']['id']?>.html">
                    <p><?php echo $val['Video']['title'];?></p>
                    <!-- <span><?php echo $val['Video']['title'];?></span> -->
                </a></li>
                <?php endforeach;?>   
    		</ul>
    		<a href="/service/remote/"></a>
    	</div>
    </div>
	<div class="vbottom">“试管无忧，专注疑难不孕”！沪ICP备14046762号</div>
</div>
<script type="text/javascript"> 
$(".videoForm").Validform({ 
    btnSubmit:".video_btn", 
    tiptype:function(msg){
        layer.alert(msg,{icon:0});    
    },
    tipSweep:true,
    ajaxPost:true,
    callback:function(o){
        layer.closeAll();
        if (o.status == 4000) { 
            if(o.return_url=="/mobile/login/")
            { 
                layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
                setTimeout("window.location.href='/mobile/login/'", 2000);
            }
            else
               window.location.href='/login/';  
        }
        else if (o.status == 200) {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});      
            setTimeout(function () {
                window.location.reload();
            }, 1000);   
        } else {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
        } 
    }
});

</script>
<!-- Swiper JS -->
    <script src="/js/idangerous.swiper.js"></script>

    <!-- Initialize Swiper -->
    <script type="text/javascript">
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        spaceBetween: 30
    });
    </script>
</body>
</html> 

