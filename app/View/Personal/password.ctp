<?= $this->Sgwy->addCss('/css/personal/password.css'); ?>
<div class="password">
	<div class="content">
		<div class="top">
			<p><a href="/community/index/">首页</a> >  <a href="/personal/index/">个人中心</a> > 修改密码</p>   
			<div class="title">
			    <div>
			    	<p>修改密码：</p>
					请按步骤进行密码修改。
			    </div>
			</div>
		</div>
		<div class="main">
			<div class="left">
				<ul>
					<li><a href="/personal/index/" class="a1">资料管理</a></li>
					<li><a href="/personal/password/" class="a2">修改密码</a></li>
					<li><a href="/personal/my_shares/" class="a3">我的分享</a></li>
					<!--<li><a href="/personal/my_reservations" class="a4">我的预约</a></li>-->
					<li><a href="/personal/my_answers/" class="a5">我的问答</a></li>
					<li><a href="/personal/my_plan/" class="a6">我的攻略</a></li>
				</ul>
			</div>
			<div class="right">
    			<form  class="dataForm" action="/mobile/password" method="post" >
				    <div>
				    	<input class="old_pass" type="password" placeholder="请输入旧密码"  name="u_pwd"  datatype="*8-16" errormsg="密码至少8个字符,最多16个字符！"  nullmsg="请输入密码！">
				    	<img class="lazy" src="/images/common/grey.gif" data-original="/images/mobile/suo.png"> 
				    </div>
				    <div>
				    	<input class="new_pass1" type="password" placeholder="请输入新的密码"  name="new_pwd"  datatype="*8-16" errormsg="密码至少8个字符,最多16个字符！"  nullmsg="请输入密码！">
				    	<img class="lazy" src="/images/common/grey.gif" data-original="/images/mobile/suo.png"> 
				    </div>
				    <div>
				    	<input class="new_pass2" type="password" placeholder="请在此输入新的密码" name="new_pwd_comfirm" datatype="*" recheck="new_pwd" errormsg="两次输入的密码不一致！"  nullmsg="请再次输入密码！">
				    	<img class="lazy" src="/images/common/grey.gif" data-original="/images/mobile/suo.png"> 
				    </div> 
				<div class="submit">
					<button type="button" class="submit_btn">保存</button>
					<p>温馨提示：密码必须至少8个字符，而且同时包含字母和数字。</p>
				</div>
		        <input type="hidden" name="dosubmit" value="1"/>    
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"> 
    $(".dataForm").Validform({
    	btnSubmit:".submit_btn", 
        tiptype:function(msg){
            layer.alert(msg,{icon:0});   
        },
        tipSweep:true,
        ajaxPost:true,
        callback:function(o){
            if (o.status == 200) {
                layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});             
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            } else {
                layer.alert(o.message,{icon:0});    
            } 
        }
    });
</script>         