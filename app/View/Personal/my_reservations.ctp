<?= $this->Sgwy->addCss('/css/personal/my_reservations.css'); ?>
<div class="my_reservations">
	<div class="content">
		<div class="top">
			<p><a href="/community/index/">首页</a> >  <a href="/personal/index/">个人中心</a> > 我的预约</p>   
			<div class="title">
			    <div>
			    	<p>我的预约：</p>
					如需咨询或预约，请您进一步完善账户资料。
			    </div>
			</div>
		</div>
		<div class="main">
			<div class="left">
				<ul>
					<li><a href="/personal/index/" class="a1">资料管理</a></li>
					<li><a href="/personal/password/" class="a2">修改密码</a></li>
					<li><a href="/personal/my_shares/" class="a3">我的分享</a></li>
					<!--<li><a href="/personal/my_reservations" class="a4">我的预约</a></li>-->
					<li><a href="/personal/my_answers/" class="a5">我的问答</a></li>
					<li><a href="/personal/my_plan/" class="a6">我的攻略</a></li>
				</ul>
			</div>
			<div class="right">
				<ul class="list_ul">
            		<?php foreach($dataList as $key => $value):?>
            		<?php if($value['UsersAppoint']['hospital_id']>0){ ?>
    				<li>
			    		<div class="list_img">
			    			<img class="lazy" src="<?php echo $value['UsersAppoint']['hospital_image'];?>" data-original="<?php echo $value['UsersAppoint']['hospital_image'];?>">
			    		</div>
			    		<div class="list_text">
			    			<p class="text_title"><?php echo $value['UsersAppoint']['hospital'];?></p>
			    			<p class="text_box"><?php echo $value['UsersAppoint']['introduction'];?></p>
			    			<span>已预约</span>
			    			<div class="time"><?php echo $value['UsersAppoint']['create_time'];?><a href="/hospital/info/<?php echo $value["UsersAppoint"]["hospital_id"].".html";?>">【查看详情】</a></div>

			    		</div>
			    	</li>
	    			<?php }else if($value['UsersAppoint']['doctor_id']>0){?>
			    	<li>
			    		<div class="list_img">
			    			<img class="lazy" src="<?php echo $value['UsersAppoint']['doctor_image'];?>" data-original="<?php echo $value['UsersAppoint']['doctor_image'];?>">
			    		</div>
			    		<div class="list_text">
			    			<p class="text_title"><?php echo $value['UsersAppoint']['doctor'];?></p>
			    			<p class="text_box"><?php echo $value['UsersAppoint']['introduction'];?></p>
			    			<span>已预约</span>
			    			<div class="time"><?php echo $value['UsersAppoint']['create_time'];?><a href="/doctor/info/<?php echo $value["UsersAppoint"]["doctor_id"].".html";?>">【查看详情】</a></div>

			    		</div>
			    	</li>
	    			<?php }?>
            		<?php endforeach;?>
				</ul>
			</div>
		</div>
	</div>
</div>
