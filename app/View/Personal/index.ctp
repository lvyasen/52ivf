<?= $this->Sgwy->addCss('/css/personal/account.css'); ?> 
<?= $this->Sgwy->addCss('/plugin/croppic/assets/css/croppic.css'); ?>
<?php echo $this->Html->script('/js/jquery.form.min.js');?>   
<?php echo $this->Html->script('/js/mobile/user.js');?>  
<?php echo $this->Html->script('/plugin/croppic/assets/js/bootstrap.min.js');?>  
<?php echo $this->Html->script('/plugin/croppic/assets/js/jquery.mousewheel.min.js');?>  
<?php echo $this->Html->script('/plugin/croppic/croppic.min.js');?>
<style type="text/css">
#cropContainerHeader {
    color: #FFF;
    margin: 10px 0 30px 71px;
    padding: 15px 50px;
    width: 100px;
    display: block;
    text-align: center;
    font-weight: 400;
    background: #2CE987;
    text-transform: uppercase;
    border-radius: 2px; 
    font-size: 20px;
}

</style>   
<div class="account">
    <div class="content">
        <div class="top">
            <p><a href="/community/index/">首页</a> >  <a href="">个人中心</a> > <a href="">资料管理</a></p>   
            <div class="title">
                <div>
                    <p>资料管理：</p>
                    如需咨询或预约，请您进一步完善账户资料。
                </div>
            </div>
        </div>
        <div class="main">
            <div class="left">
                <ul>
                    <li><a href="/personal/" class="a1">资料管理</a></li>
                    <li><a href="/personal/password/" class="a2">修改密码</a></li>
                    <li><a href="/personal/my_shares/" class="a3">我的分享</a></li>
                    <!--<li><a href="/personal/my_reservations" class="a4">我的预约</a></li>-->
                    <li><a href="/personal/my_answers/" class="a5">我的问答</a></li>
                    <li><a href="/personal/my_plan/" class="a6">我的攻略</a></li>
                </ul>
            </div>
            <div class="right">
                <div class="btn">
                    <a class="btn_01 btn_add">个人信息</a>
                    <a class="btn_02">头像设置</a>
                </div>
                <div class="right_con">
                    <div class="con_01">
                        <form  class="dataForm" action="/personal/index" id="regForm" method="post" >
                        <div>
                            <label for="">注册手机号：</label>
                            <span><?php echo substr_replace($userInfo['mobile'],"****","3","4");?></span>
                        </div>
                        <div>
                            <label for="" style="float:left;">昵称：</label>
                            <span style="float:left;"><?php echo isset($userInfo['nick_name']) && !empty($userInfo['nick_name'])?$userInfo['nick_name']:"";?></span>
                            <img class="shuru" src="/images/community_common/shuru.png" alt="">
                            <input class="ming" type="text" placeholder="请输入昵称" name="u_nick" value="<?php echo isset($userInfo['nick_name']) && !empty($userInfo['nick_name'])?$userInfo['nick_name']:"";?>" datatype="*1-10" errormsg="昵称最多10个字符！"  nullmsg="请输入昵称！">
                        </div>
                        <div class="sex">
                            <label for="">性别：</label>
                            <input type="radio" id="boy" name="sex" value="1" <?php echo $userInfo['sex']==1?"checked":""?>>
                            <label for="boy">男</label>
                            <input type="radio" id="girl" name="sex" value="2" <?php echo $userInfo['sex']==2?"checked":""?>>
                            <label for="girl">女</label>

                        </div>
                        <div>
                            <label for="">年龄：</label>
                            <input type="text" placeholder="请输入年龄" name="age"   datatype="n1-3" errormsg="请填写正确年龄！"  ignore="ignore" value="<?php echo isset($userInfo['age']) && !empty($userInfo['age'])?$userInfo['age']:"";?>">
                        </div>
                        <input type="hidden" name="dosubmit" value="1"/>    
                        <button class="submit">保存</button>        
                    </form>
                    </div>
                    <div class="con_02"> 
                        <div class="col-lg-6 cropHeaderWrapper">
                            <div id="croppic"></div>
                            <span class="btn" id="cropContainerHeader">更换头像</span>
                        </div>     
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        // body...
        $(".btn_02").click(function(){
            $(this).addClass('btn_add');
            $(".con_02").show();
            $(".btn_01").removeClass('btn_add');
            $(".con_01").hide();
        })
        $(".btn_01").click(function(){
            $(this).addClass('btn_add');
            $(".con_01").show();
            $(".btn_02").removeClass('btn_add');
            $(".con_02").hide();
        })
    })
</script>
<script type="text/javascript"> 
    var croppicHeaderOptions = {
        //uploadUrl:'img_save_to_file.php',
        cropData: {
            "dummyData": 1,
            "dummyData2": "asdas"
        },
        cropUrl: '/ajax/file/?ajaxdata=avatar', 
        customUploadButtonId: 'cropContainerHeader',
        modal: false,
        processInline: true,
        loaderHtml: '<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> '
    }
    var croppic = new Croppic('croppic', croppicHeaderOptions);
    $(".dataForm").Validform({
        btnSubmit:".submit", 
        tiptype:function(msg){
            //layer.msg(msg);   
        },
        tipSweep:true,
        ajaxPost:true,
        callback:function(o){
            if (o.status == 200) {  
                layer.msg("修改成功！",{icon:0,skin:'layer_mobile_login'});         
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            } else {
                layer.msg(o.message);    
            } 
        }
    }); 
    $(".shuru").click(function(){
        $(this).hide();
        $(this).siblings('span').hide();
        $(".ming").show();
    })
</script>     