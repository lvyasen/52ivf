<?= $this->Sgwy->addCss('/css/personal/my_shares.css'); ?>
<?php echo $this->Html->script('/admin/js/plupload/plupload.full.min.js');?>     
 
<link rel="stylesheet" type="text/css" href="/admin/js/plupload/css/common.css">
<link rel="stylesheet" type="text/css" href="/admin/js/plupload/css/index.css">  
<div class="my_share">
	<div class="content">
		<div class="top">
			<p><a href="/community/index/">首页</a> >  <a href="/personal/index/">个人中心</a> > 我的分享</p>   
			<div class="title">
			    <div>
			    	<p>我的分享：</p>
					您可以在这里查看您的分享记录。
			    </div>
			    <a class="release">发布</a>
			</div>
		</div>
		<div class="main">
			<div class="left">
				<ul>
					<li><a href="/personal/index/" class="a1">资料管理</a></li>
					<li><a href="/personal/password/" class="a2">修改密码</a></li>
					<li><a href="/personal/my_shares/" class="a3">我的分享</a></li>
					<!--<li><a href="/personal/my_reservations" class="a4">我的预约</a></li>-->
					<li><a href="/personal/my_answers/" class="a5">我的问答</a></li>
					<li><a href="/personal/my_plan/" class="a6">我的攻略</a></li>
				</ul>
			</div>
			<div class="right">
				<ul class="list_ul">
                    <?php foreach ($dataList as $key => $value):?>
    				<li>
			            <div class="tx">
			                <img class="lazy" src="<?=$userInfo['avatar']?>" data-original="<?=$userInfo['avatar']?>" alt="">
			            </div>
			            <div class="title1">
			                <div class="b1">
			                    <a href="/share/info/<?=$value['Share']['id']?>.html"><?=$value['Share']['title']?></a>
			                </div>
			                <div class="b1" style="color: #cdcfcf;">
			                    <?=$value['Share']['content']?>               
			                </div>
			                <div class="share_img">
			                	<ul class="list_img">
				                    <?php foreach ($value['Share']['images'] as $k => $v):?> 
				                    <li><img class="lazy"   data-original="<?php echo $v['Picture']['thumb'];?>" src="<?php echo $v['Picture']['thumb'];?>"?>  </li> 
				                    <?php endforeach;?> 
					    		</ul>
			                </div>
			                <div class="b2">
			                    <div class="b3"><?=$value['Share']['browse_num']?>
			                    </div> 
			                    <div class="b5">
			                        <?=$value['Share']['create_time']?>                    </div>
			                    <a class="b6 share_replay_show" href="/share/info/<?=$value['Share']['id']?>.html">
			                        【查看详情】
			                    </a>  
			                </div>
			            </div>  
			        </li>
                	<?php endforeach;?>
				</ul>
				<div class="plan_write">
    			  <div class="p_w_con">
    			    <h5 class="titleh5">我要分享<a id="close">关闭</a></h5>
                    <div class="w_left">
        			<form  class="dataForm" action="/personal/my_shares" method="post" enctype="multipart/form-data">
                        <div class="wl0">
                        	就诊医院：<select name="hospital_id" id="hospital_id" style="height: 27px;">
                            <?php foreach ($hospitalList as $key => $value) {?> 
                            <option value="<?php echo $value['Hospital']['id'];?>"><?php echo $value['Hospital']['cn_name'];?></option>  
                            <?php }?>  
                        	</select>
                        </div>
                    	<div class="wl1">
                    		分享标题：<input type="text" placeholder="请输入标题" name="title" datatype="*2-80" errormsg="标题名称至少2个字符,最多80个字符！"  nullmsg="请填写分享标题！">
                    	</div>
                    	<div class="wl2">
                    		<span style="float:left">详细描述：</span><textarea name="description"  id="form_textarea" placeholder="" onkeyup="checkLength(this,2000,'sy');" placeholder="请输入分享步骤"></textarea><span style="float:left">剩余字数：<span id="sy" style="color:Red;">2000</span>字</span>
                    	</div> 
                    	<div class="wl3"  id="divContent">
                    		<span style="float:left;">添加图片：</span>
	                        <section class="z_file fl" id="btnMultipleUpload">
	                            <img src="/admin/js/plupload/img/a11.png" tppabs="/admin/js/plupload/img/a11.png" class="add-img">
	                            <input type="file" name="file" id="file" class="file" value="" accept="image/jpg,image/jpeg,image/png,image/bmp" multiple />
	                        </section>
                    	</div>
                    	<div class="wl4">
                    		<span style="float:left;">主题标签：</span> 
                    		<div class="wl_tag"> 
            					<?php foreach ($share_tags as $key => $value) :?> 
                    			<div data-id="<?php echo $key;?>">
                    				<label for="r01"><?php echo $value;?><input type="radio"  name="tag" value="<?php echo $key;?>"></label>
                    				
                    			</div> 
                				<?php endforeach;?>
                    		</div> 
                    	</div> 
                    	<!-- <div class="wl4">
                    		<span style="float:left;">就诊国家：</span>   
                    		<div class="wl_tag">
                    			<div>
                    				<label for="l01">中国</label>
                    				<input type="radio" id="l01" name="1">
                    			</div>
                    			<div>
                    				<label for="l02">泰国</label>
                    				<input type="radio" id="l02" name="1">
                    			</div>
                    			<div>
                    				<label for="l03">美国</label>
                    				<input type="radio" id="l03" name="1">
                    			</div>
                    		</div>
                    	</div> -->
                    	<div class="wl6">
                    		<span style="float:left;">验证码：</span><input type="text"  name="u_code" value="" placeholder="请输入验证码" datatype="*4-4" errormsg="请输入4位数验证码！"  nullmsg="请输入验证码！"><div class="code"><img src="/personal/captcha" title="点击更换" onclick="this.src='/personal/captcha/?'+Math.random()"/></div> 
                    	</div>
                    	<button class="submit">提交</button>
			            <input type="hidden" name="dosubmit" value="1"/>    
			        </form>   
                    </div>
                  </div>
    			</div>	
			</div>
		</div>
	</div>
</div> 
<script>
	$(".release").click(function (argument) {
		$(".list_ul").hide();
		$(".plan_write").fadeIn();
	})
	$("#close").click(function (argument) {
		$(".plan_write").fadeOut();
		$(".list_ul").show();
	})
	$(".wl_tag label").click(function(){
        $(this).parent("div").addClass('ck_tag').siblings("div").removeClass('ck_tag');
        if($(this).parent("div").hasClass("ck_tag")){ 
			$(':radio[name="tag"]').removeAttr("checked");
    		var cid = $(this).parent("div").attr("data-id");   
			$("input[name='tag'][value='"+cid+"']").attr("checked",true);  
        }  
	}); 
	$(".dataForm").Validform({
	    btnSubmit:".submit", 
	    tiptype:function(msg){
	        layer.alert(msg,{icon:0});   
	    },
	    tipSweep:true,
	    ajaxPost:true,
	    callback:function(o){
	        if (o.status == 200) {
	            layer.alert(o.message,{icon:0}); 
                setTimeout("window.location.href='/personal/my_shares'", 1000); 
	        } else {
	            layer.alert(o.message,{icon:0});    
	        } 
	    }
	}); 
	var MAX_FILE_NUM = 3;   
	var multi = new plupload.Uploader({
	    browse_button: 'btnMultipleUpload', // this can be an id of a DOM element or the DOM element itself
	    url: '/ajax/file?ajaxdata=share', 
	    max_file_size: '10mb',
	    chunk_size: '3mb',
	    filters: {
	        mime_types: [
	          { title: "Image files", extensions: "BMP,jpg,JPEG,gif,png" }
	        ]
	    },
	    runtimes: 'html5',
	    multi_selection: true
	});

	multi.init();

	multi.bind('FilesAdded', function (up, files) { 
	    var uptLenth=$('.add_pic').size();   
	    if(((uptLenth-1)+files.length) >= MAX_FILE_NUM){ 
	         layer.alert('最多添加3张',{icon:0});  
	         multi.splice();
	         multi.refresh();
	        return;
	    }else{ 
	        layer.load(1); 
	        plupload.each(files, function (file) {     
	            $('#divContent').append('<section class="up-section fl add_pic" id="pic_'+file.id+'"></section>');
	        });
	        multi.start();
	    }
	});  
	multi.bind('FileUploaded', function (up, file, resp) { 
	    var json = JSON.parse(resp.response); 
		if(json.status==200){
	    	$('#pic_' + file.id).html('<span class="up-span"></span><img src="/admin/js/plupload/img/a7.png" class="close-upimg" onclick="delFile(' + json.data.id + ',\'pic_' + file.id + '\')"><img class="up-img" src="' + json.data.url + '"/><input type="hidden"  name="UptPicture[]" value="'+json.data.id+'" />');
		}
		else 
            layer.alert(json.message,{icon:0});   
	    layer.closeAll('loading');
	});

	multi.bind('Error', function (up, err) {   
	    layer.alert("Error #" + err.code + ": " + err.message,{icon:0});  
	}); 
    <?php if(sizeof($dataList) == 0){?>
            layer.alert("暂未发布分享！",{icon:5,time:1000});  
    <?php }?>
</script>