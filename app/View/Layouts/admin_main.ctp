<!DOCTYPE html>
<html>
<head>       
    <?php $this->Sgwy->addCss('/admin/skin/default/skin.css?20170823'); ?>   
    <?= $this->element('admin/head'); ?>   
    <?php echo $this->Html->script('/admin/js/bootstrap.min.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.nicescroll.js');?>  
</head>
<body>
 <div class="Sellerber" id="Sellerber">
  <!--顶部-->
  <div class="Sellerber_header apex clearfix" id="Sellerber_header">
   <!-- <div class="l_f logo"><img src="/admin/images/logo_03.png" /></div> -->
   <div class="oldTime">您上次登录的时间：<span>2016-02-12 15:21:31</span></div>
   <div class="r_f Columns_top clearfix">
   <div class="administrator l_f">
     <span class="user-info"><?php echo $userName;?></span>
     <img src="/admin/images/houtai/guanliyuan.png"  />
   </div>
   <a class="out" href="javascript:void(0)" id="Exit_system">退出</a>
 </div>
 <div class="">
   
 </div>
</div>
<!--左侧-->
<div class="Sellerber_left menu" id="menuBar">
  <div class="show_btn" id="rightArrow"><span></span></div>
  <div class="side_title"><a title="隐藏" class="close_btn"><span></span></a></div>
  <div class="menu_style" id="menu_style">
   <div class="list_content"> 
  <!-- <div class="search_style">
   <form action="#" method="get" class="sidebar_form">
    <div class="input-group">
      <input type="text" name="q" class="form-control">
      <span class="input-group-btn">
        <a class="btn_flat" href="javascript:" onclick=""><i class="fa fa-search"></i></a>
      </span>
    </div>
  </form>
</div>   -->
<div class="head_logo">
   试管无忧<br>后台管理网站
</div>
<!--include menu-->  
<?= $this->element('admin/menu'); ?>     
</div>
</div>
</div>
<!--内容-->
<div class="Sellerber_content" id="contents">
<div class="Sellerber_bg" style="padding-top:24px;width: 96%;margin:0 auto;">
  <div class="breadcrumbs" id="breadcrumbs">
   <a id="js-tabNav-prev" class="radius btn-default left_roll" href="javascript:;"><i class="fa fa-backward"></i></a>
   <div class="breadcrumb_style clearfix">
     <ul class="breadcrumb clearfix" id="min_title_list">
      <li class="active home"><span title="我的桌面" data-href="/admin/home/"><i class="fa fa-home home-icon"></i>首页</span></li>
    </ul>
  </div>
  <a id="js-tabNav-next" class="radius btn-default right_roll" href="javascript:;"><i class="fa fa-forward"></i></a>
  <div class="btn-group radius roll-right">
   <a class="dropdown tabClose" data-toggle="dropdown" aria-expanded="false">
     页签操作<i class="fa fa-caret-down" style="padding-left: 3px;"></i>
   </a>
   <ul class="dropdown-menu dropdown-menu-right" id="dropdown_menu">
     <li><a class="tabReload" href="javascript:void(0);">刷新当前</a></li>
     <li><a class="tabCloseCurrent" href="javascript:void(0);">关闭当前</a></li>
     <li><a class="tabCloseAll" href="javascript:void(0);">全部关闭</a></li>
     <li><a class="tabCloseOther" href="javascript:void(0);">除此之外全部关闭</a></li>
   </ul>
 </div>
 <a href="javascript:void(0)" class="radius roll-right fullscreen"><i class="fa fa-arrows-alt"></i></a>
</div>
<!--具体内容-->  
<div id="iframe_box" class="iframe_content">
  <div class="show_iframe" id="show_iframe">
    <iframe scrolling="yes" class="simei_iframe" frameborder="0" src="/admin/home/" name="iframepage" data-href="/admin/home/"></iframe>
  </div>
</div>
</div>
</div>
<!--底部-->
<!--include footer-->
<?= $this->element('admin/footer'); ?>   
</div>
</body>
</html>
<script type="text/javascript">
//设置框架
$(function() {  
 $("#Sellerber").frame({
  float : 'left',
  color_btn:'.skin_select',
      header:70,//顶部高度
      bottom:30,//底部高度
      menu:200,//菜单栏宽度
      Sellerber_menu:'.list_content',
      Sellerber_header:'.Sellerber_header',
    });
}); 
$('#Exit_system').on('click', function(){
 layer.confirm('是否确定退出系统？', {
     btn: ['是','否'] ,//按钮
     icon:2,
   }, 
   function(){
    location.href="/admin/logout/"; 
  });
});
$("#menu_style").niceScroll({  
  cursorcolor:"#888888",  
  cursoropacitymax:1,  
  touchbehavior:false,  
  cursorwidth:"5px",  
  cursorborder:"0",  
  cursorborderradius:"5px"  
}); 
</script>

