<!DOCTYPE html>
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">   
   <title>ivf52-后台管理平台</title> 
   <script type="text/javascript">
   var LOCAL_HOME = "";
   </script>     
    <?php echo $this->Html->script('/admin/js/jquery.js');?>
    <?php echo $this->Html->script('/admin/js/jquery.form.js');?>
    <?php echo $this->Html->script('/admin/js/layer/layer.js');?> 
    <?php echo $this->Html->script('/admin/js/ivf52/user.js');?>    
    <?php $this->Sgwy->addCss('/admin/css/style.css'); ?>
    <?php $this->Sgwy->addCss('/admin/css/Sellerber.css'); ?>
    <?php $this->Sgwy->addCss('/admin/css/bkg_ui.css'); ?>
    <?php $this->Sgwy->addCss('/admin/font/css/font-awesome.min.css'); ?>
    <?php echo $this->Sgwy->getCombinedCssLink(); ?>      
</head>

<body class="login_style login-layout">    

        <?php echo $this->Flash->render(); ?>
        <?php echo $this->fetch('content'); ?>
</body>
</html> 
<script type="text/javascript"> 
$(document).ready(function(){
  $("input[type='text'],input[type='password']").blur(function(){
    var $el = $(this);
    var $parent = $el.parent();
    $parent.attr('class','frame_style').removeClass(' form_error');
    if($el.val()==''){
      $parent.attr('class','frame_style').addClass(' form_error');
   }
});
  $("input[type='text'],input[type='password']").focus(function(){     
   var $el = $(this);
   var $parent = $el.parent();
   $parent.attr('class','frame_style').removeClass(' form_errors');
   if($el.val()==''){
      $parent.attr('class','frame_style').addClass(' form_errors');
   } else{
     $parent.attr('class','frame_style').removeClass(' form_errors');
  }
});
})
</script>