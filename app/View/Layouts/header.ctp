<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="applicable-device"content="pc,mobile">
<meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<!--让网页的宽度自动适应手机屏幕的宽度。 其中： width=device-width ：表示宽度是设备屏幕的宽度 initial-scale=1.0：表示初始的缩放比例 minimum-scale=0.5：表示最小的缩放比例 maximum-scale=2.0：表示最大的缩放比例 user-scalable=yes：表示用户是否可以调整缩放比例-->
<title><?php echo isset($keysInfo['Keyword']['meta_title'])?$keysInfo['Keyword']['meta_title']:'试管无忧';?></title>
<meta name="keywords" content="<?php echo isset($keysInfo['Keyword']['meta_keywords'])?$keysInfo['Keyword']['meta_keywords']:'试管无忧';?>" />
<meta name="description" content="<?php echo isset($keysInfo['Keyword']['meta_description'])?$keysInfo['Keyword']['meta_description']:'试管无忧';?>" />
<meta name="format-detection" content="telephone=no" /><!--防止在iOS设备中的Safari将数字识别为电话号码-->
<link href="/favicon.ico" type="image/x-icon" rel="icon"/>
<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
<link rel="stylesheet" type="text/css" href="/css/common.css?zy=20180718">
<link rel="stylesheet" type="text/css" href="/css/glyphicons.css">
<?php echo $this->Sgwy->getCombinedCssLink(); ?>
<script type="text/javascript" src="/js/jquery.1.8.3.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/jquery.lazyload.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script> 
<script type="text/javascript" src="/js/public.js"></script> 
<?php echo $this->Html->script('/admin/js/Validform/Validform.min.js');?>  
<!--[if lt IE 9]>
<script type="text/javascript" src="http://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script type="text/javascript" src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
<![endif]--> 
</head>
<body>
<div class="header">
    <div class="slip">
        <div class="slip-cont">
        <span>您好，欢迎来到试管无忧官网！</span>
        <div><a href="/about/info/" rel="nofollow">品牌故事</a><a href="/contact/index/" rel="nofollow">联系我们</a><a href="/headlines/index/" rel="nofollow">试管常见问题</a><a href="/sitemap/index/">网站地图</a></div></div>
    </div>
    <div class="head-nav">
        <div class="dw"><img src="/images/common/new.gif" alt="泰国试管婴儿医院,泰国试管婴儿费用"></div>
        <div class="dw dw2"><img src="/images/common/new.gif" alt="泰国试管婴儿医院,泰国试管婴儿费用"></div>
        <div class="h-l-img"><a href="/" style="width:100%;height: 100%;display:block;"></a>
        </div>
        <ul class="nav-top">
            <li <?php echo isset($nav) && $nav=='index'?'class="pink_add"':'';?> >
                <a  href="/" rel="nofollow">首页</a>
            </li>
            <li <?php echo isset($nav) && $nav=='remote'?'class="pink_add"':'';?> >
                <a href="/service/remote/" rel="nofollow">远程咨询</a>
            </li>

            <!-- <li <?php echo isset($nav) && $nav=='booking'?'class="pink_add"':'';?> >
                <a href="/service/booking/" rel="nofollow">精准预约</a>
            </li> -->
            <li class="tao  <?php echo isset($nav) && $nav=='taocan'?'pink_add':'';?>" >
                <a href="/service/vip/">套餐服务</a>
                <ul class="drop-down">
                    <li><a href="/service/booking/" rel="nofollow">精准预约</a></li>
                    <li><a href="/service/vip/" rel="nofollow">VIP尊享</a></li>
                    <li><a href="/service/gold/" rel="nofollow">金牌无忧</a></li>
                </ul>
            </li>
            <li class="tao <?php echo isset($nav) && $nav=='hospital'?'"pink_add"':'';?>" >
                <a href="/hospital/index/">热门医院</a>
                <ul class="drop-down">
                    <li><a href="/hospital/index/" rel="nofollow">知名医院</a></li>
                    <li><a href="/doctor/index/" rel="nofollow">知名医生</a></li>
                </ul>               
            </li>
            <li>
                <a target="_blank" href="/video/index/">试管课堂</a>
            </li>
            <li class="tao she">
                <a href="/community/index/" target="_blank">试管邦社区</a>
                <ul class="drop-down" style="width: 154px;">
                    <li><a href="/plan/index/" rel="nofollow">泰国就医攻略</a></li>
                    <li><a href="/share/index/" rel="nofollow">试管经验分享</a></li>
                    <li><a href="/answer/index/" rel="nofollow">试管疑难问答</a></li>
                </ul>
            </li>    
        </ul>
        <div class="h-r-img">
        <div class="nav_cm"></div>
        </div>
    </div>
</div>
<?= $this->element('sidenav'); ?>