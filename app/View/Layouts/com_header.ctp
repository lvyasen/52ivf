<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<!--让网页的宽度自动适应手机屏幕的宽度。 其中： width=device-width ：表示宽度是设备屏幕的宽度 initial-scale=1.0：表示初始的缩放比例 minimum-scale=0.5：表示最小的缩放比例 maximum-scale=2.0：表示最大的缩放比例 user-scalable=yes：表示用户是否可以调整缩放比例-->
<meta name=""charset="utf-8">
<meta name="applicable-device"content="pc,mobile">
<title><?php echo isset($keysInfo['Keyword']['meta_title'])?$keysInfo['Keyword']['meta_title']:'试管无忧';?></title>
<meta name="keywords" content="<?php echo isset($keysInfo['Keyword']['meta_keywords'])?$keysInfo['Keyword']['meta_keywords']:'试管无忧';?>" />
<meta name="description" content="<?php echo isset($keysInfo['Keyword']['meta_description'])?$keysInfo['Keyword']['meta_description']:'试管无忧';?>" />
<meta name="format-detection" content="telephone=no" /><!--防止在iOS设备中的Safari将数字识别为电话号码-->
<link href="/favicon.ico" type="image/x-icon" rel="icon"/>
<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
<link rel="stylesheet" type="text/css" href="/css/common.css">
<link rel="stylesheet" type="text/css" href="/css/glyphicons.css">
<?php echo $this->Sgwy->getCombinedCssLink(); ?>
<script type="text/javascript" src="/js/jquery.1.8.3.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/jquery.lazyload.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script> 
<script type="text/javascript" src="/js/public.js"></script> 
<?php echo $this->Html->script('/admin/js/Validform/Validform.min.js');?>  
</head>
<body>
<div class="comheader">
    <div class="comhead">
        <div class="comhead_top">
            <div class="h_t_img"><a href="/community/index/" style="width:100%;height: 100%;display:block;"></a></div>
            <div class="h_t_search"> 
                <div class="h_t_search_zhu">
                    <div>
                        <select class="sele" id="seachType">
                            <option value="0" <?php echo !isset($seachType) || $seachType==0?'selected':'';?>>攻略</option>
                            <option value="1" <?php echo isset($seachType) && $seachType==1?'selected':'';?>>分享</option>
                            <option value="2" <?php echo isset($seachType) && $seachType==2?'selected':'';?>>问答</option>
                        </select>
                    </div>
                    <div>
                        <input class="input bg1" type="text"  id="seachTitle" value="<?php echo isset($seachTitle)?$seachTitle:'';?>"  datatype="s2-10" errormsg="姓名格式不正确！">
                    </div>
                    <a href="javascript:void(0)" id="seachBtn" >
                    <div class="search">
                        <img src="/images/community_common/sosuo.jpg"  >
                    </div>
                    </a>
                </div>    
                <div class="com_p">大家都在搜：泰国试管婴儿医院&nbsp;&nbsp;泰国第三代试管婴儿费用</div>
            </div>
            <div class="h_t_login">
                <a href="/">试管无忧官网</a>
                <?php if(isset($userInfo) && !empty($userInfo)){?>
                <div class="per">
                    <a class="touxiang">
                        <img src="<?php echo $userInfo['avatar'];?>" alt="">
                        <span><?php echo empty($userInfo['nick_name'])?'未设置昵称':$userInfo['nick_name'];?></span>
                    </a>
                    <div class="xinxi">
                        <a href="/personal/index/">个人中心</a>
                        <a href="/personal/login_out">退出</a>
                    </div>
                </div>
                <?php }else{?>
                <a class="register">注册</a>
                <a class="login" >登录</a>
                <?php }?>
            </div>
            <a class="fanhui" href="/">返回官网</a>
        </div>
        <div class="comhead_nav">
            <div class="nav">
                <a <?php echo isset($nav) && $nav=='cindex'?'class="nav_sty"':'';?> href="/community/index/">首页</a>
                <a <?php echo isset($nav) && $nav=='plan'?'class="nav_sty"':'';?>href="/plan/index/">攻略</a>
                <a <?php echo isset($nav) && $nav=='share'?'class="nav_sty"':'';?>href="/share/index/">分享</a>
                <a <?php echo isset($nav) && $nav=='answer'?'class="nav_sty"':'';?>href="/answer/index/">问答</a>
                <a <?php echo isset($nav) && $nav=='audio'?'class="nav_sty"':'';?>href="/audio/index/" target="_blank">胖妈妈之声</a>
                <a <?php echo isset($nav) && $nav=='video'?'class="nav_sty"':'';?>href="/video/index/" target="_blank">视频自媒体</a>
            </div>
        </div>      
    </div>    
</div>
<?= $this->element('regist'); ?>
<?= $this->element('sidenav'); ?>
<script type="text/javascript">  
    $('#seachBtn').click(function(){   
        var val=$("#seachTitle").val();
        var st=$("#seachType").val();
         window.location.href = '/Community/seach/'+st+'-' + val +".html";
      });
</script>