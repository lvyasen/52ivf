<footer>
    <div class="foot-pink">
        <div class="foot-p-content">
            <div class="foot-p-c-left">
                <h2>关于试管无忧</h2>
                <ul>
                    <li><a href="/about/info/" rel="nofollow">品牌故事</a></li>
                    <li><a href="/news/index/">新闻资讯</a></li>
                    <li><a href="/headlines/index/">常见问题</a></li>
                    <li><a href="/join/index/" rel="nofollow">加入我们</a></li>
                    <li><a href="/sitemap/index/">网站地图</a></li>
                </ul>
            </div>
            <div class="foot-p-c-center">
                <h2>微信二维码（关注好礼）</h2>
                <div class="weixin">
                    <img src="/images/common/grey.gif" data-original="/images/common/erweima01.jpg" alt="" class="lazy" />
                    <div class="">
                        <h2>其实怀孕可以很简单!</h2>
                        <div>
                            <img src="/images/common/grey.gif" data-original="/images/common/weixin.jpg" alt="" class="lazy" />
                        </div>                        
                    </div>
                </div>
            </div>
            <div class="foot-p-c-right">
                <h2>关注微博</h2>
                <div class="weibo">
                    <img src="/images/common/grey.gif" data-original="/images/common/erweima02.jpg" alt="" class="lazy" />
                    <a href="http://weibo.com/5967833423/profile?topnav=1&wvr=6&is_hot=1" rel="nofollow" target="_blank"><span>新浪微博</span></a>
                </div>
                
            </div>
        </div>
        <?= $this->element('content_us'); ?>
    </div>
    <?php if(isset($nav) && $nav=='index'):?>
        <div class="foot_link">
            <div class="wenan">
                <p>“试管无忧，专注疑难不孕”！确保每一位患者的试管就医困难都得到满意的解决。</p>
                © 2017 ivf52 All Rights Reserved 沪ICP备14046762号<br>
                <div>
                友情链接&nbsp;&nbsp;&nbsp;&nbsp;
                <?php foreach($linkList as $key => $value):?>
                <a href="<?=$value['Link']['url']?>" target="_blank"><?=$value['Link']['name']?></a>
                <?php endforeach;?> </div>
            </div>
        </div>
    <?php else:?>
        <div class="foot-white">
            <div  class="foot-w-content">
                “试管无忧，专注疑难不孕”！确保每一位患者的试管就医困难都得到满意的解决。 
                <p>© 2017 ivf52 All Rights Reserved <br>沪ICP备14046762号</p>
            </div>
        </div>
    <?php endif;?>
    <div class="wap_foot">
        @2017 ivf52 All Rights Reserved<br>地址：上海市奉贤区奉浦大道97号绿地智尊A座916室<br>试管无忧版权所有<a href="http://www.miitbeian.gov.cn" target="_blank">沪ICP备14046762号</a></div>
    </div>
</footer>
<div class="online-service">
    <div class="online-chat">
        <a href="http://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn" rel="nofollow">
          <i class="icon icon-online-chat"></i>
          <p>在线咨询</p>
        </a>
    </div>
    <div class="online-tel">
        <a href="http://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn" rel="nofollow">
          <i class="icon icon-online-tel"></i>
          <p>立即询价</p>
        </a>
    </div>
</div>
<?= $this->element('footnav'); ?>
<script type="text/javascript" src="/js/foot.js"></script>
<?php echo $this->Sgwy->getCombinedJsLink(); ?>
<script type="text/javascript">
    $("#clear_x").click(function(){
        $(".zixun").hide();
    });
</script>
<script>
(function(){
    var bp = document.createElement('script');
    var curProtocol = window.location.protocol.split(':')[0];
    if (curProtocol === 'https') {
        bp.src = 'https://zz.bdstatic.com/linksubmit/push.js';        
    }
    else {
        bp.src = 'http://push.zhanzhang.baidu.com/push.js';
    }
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(bp, s);
})();
</script>
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?c42dad33dce2f736bb429177ae6b283b";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
</body>
</html>
