<html>
<head>
<meta charset="UTF-8">
<meta name="applicable-device"content="pc,mobile">
<meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<!--让网页的宽度自动适应手机屏幕的宽度。 其中： width=device-width ：表示宽度是设备屏幕的宽度 initial-scale=1.0：表示初始的缩放比例 minimum-scale=0.5：表示最小的缩放比例 maximum-scale=2.0：表示最大的缩放比例 user-scalable=yes：表示用户是否可以调整缩放比例-->
<title>【试管无忧官网】_泰国试管婴儿医院_泰国试管婴儿费用_泰国试管婴儿攻略 </title>
<meta name="keywords" content="泰国试管婴儿,泰国试管婴儿医院,泰国试管婴儿费用,泰国试管婴儿攻略" />
<meta name="description" content="试管无忧一家专注泰国试管婴儿的服务机构，想了解泰国试管生男孩要花多少钱，第三代泰国试管婴儿成功率，泰国试管婴儿流程，欢迎咨询24小时免费热线：400-003-9103" />
<meta name="format-detection" content="telephone=no" /><!--防止在iOS设备中的Safari将数字识别为电话号码-->
<link href="/favicon.ico" type="image/x-icon" rel="icon"/>
<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
<link rel="stylesheet" type="text/css" href="/css/common.css?zy=20180208">
<link rel="stylesheet" type="text/css" href="/css/glyphicons.css">
<link rel="stylesheet" type="text/css" href="/css/index/index.css"/> <link rel="stylesheet" type="text/css" href="/css/index/idangerous.swiper.css"/> <link rel="stylesheet" type="text/css" href="/css/aide/index.css"/><script type="text/javascript" src="/js/jquery.1.8.3.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/jquery.lazyload.js"></script>
<script type="text/javascript" src="/js/layer/layer.js"></script> 
<script type="text/javascript" src="/js/public.js"></script> 
<script type="text/javascript" src="/admin/js/Validform/Validform.min.js"></script> 
</head>
<body> 
<div class="leave" style="display: block;">
    <div class="box">
        <div class="title"></div>
        <div class="content">
            <p>终于等到你，还好没放弃~亲爱的姐妹，为了帮你早日好孕，能够更高效地为你的就医提供帮助，我们特意设置了这个留言平台，请尽量完整地填写下面的信息，好让我们早日见证你的好孕消息！</p>
            <form action="" method="post">
                <div>
                    <label for=""><em>*</em>姓名：</label>
                    <input type="text" name="name" id="name">
                </div>
                <div>
                    <label for=""><em>*</em>QQ：</label>
                    <input type="text" name="qq" id="qq">
                </div>
                <div>
                    <label for=""><em>*</em>手机号码：</label>
                    <input type="text" name="phone" id="phone">
                </div>
                <div>
                    <label for=""><em></em>电子邮件：</label>
                    <input type="text" name="email" id="email">
                </div>
                <div>
                    <label for=""><em>*</em>留言内容：</label>
                    <textarea name="content" id="content" cols="30" rows="10"></textarea>
                </div>
                <div class="btn">   
                <div class="leave_sbt" style="cursor:pointer;margin-right: 19px;padding:4px 10px;background:transparent;border:1px solid #f29c9f;border-radius:3px; width: 28px; height: 17px; line-height: 17px; display: block;">提交</div>
                <div class="span"></div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript"> 
$('.leave_sbt').click(function(){
        var name = $("#name").val();
        var qq = $("#qq").val();
        var phone = $("#phone").val();
        var email = $("#email").val();
        var content = $("#content").val();
        $.post('/main/leave_sub', {name:name,qq:qq,phone:phone,email:email,content:content}, function(data){
            var data = JSON.parse(data);console.log(data);  
            if(data.status == "fail"){
                $('.span').text(data.message);
            }else{
                alert('留言成功！');
            }
        });
    });
</script>
</body>
</html>