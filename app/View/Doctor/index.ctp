	<?php $this->Sgwy->addCss('/css/doctor/index.css');?>
    <?php $this->Sgwy->addJs('/js/doctor/index.js');?>
<?php echo $this->Html->script('/js/mobile/user.js');?> 
    <?php echo $this->Html->script('/js/page.js');?>   

    <div class="doctor">
        <div class="wav_title">
        <p>热门医生</p>
        <img class="lazy" onclick="window.location.href='/';" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png" alt=""><span>首页</span>
    </div>
        <div class="banner_pc"></div>
        <div class="banner_ph">
            <img class="lazy" src="/images/common/grey.gif" data-original="/images/hospital/banner_yiyuan_ph.jpg" alt="">
        </div>
        <div class="contain">
            <div class="content">
                <div class="nav">
                    <!-- <div class="country">
                        <a class="<?php echo empty($cid)? "ct_click":"";?>" href="/doctor">全部</a> 
                    <?php foreach($districtList as $key => $value):?>
                        <a href="/doctor/index/<?=$value["District"]["id"]?>.html" class="<?php echo !empty($cid) && $value["District"]["id"]==$cid ? "ct_click":"";?>"><?=$value["District"]["country"]?></a>
                    <?php endforeach;?>   
                    </div> -->
                    <div class="yiyuan">
                        <a  href="/hospital/index/<?php echo !empty($cid)? $cid.".html":"";?>" title="">找医院</a>
                        <a class="yy_click" href="/doctor/index/<?php echo !empty($cid)?$cid.".html":"";?>" title="">找医生</a>
                    </div>
                </div>
                <div class="list" id="loadPage">
                    <ul >
                    <?php foreach($dataList as $key => $value):?>
						<input  type="hidden" id="HN_<?php echo $value["Doctor"]["id"];?>" value="<?php echo $value["Doctor"]["cn_name"];?>"/> 
                        <li>
                            <a  href="/doctor/info/<?php echo $value["Doctor"]["id"].".html";?>" title="">
                            <div class="list_left">
                                <img class="lazy" src="<?=$value["Doctor"]["avatar"]?>" data-original="<?=$value["Doctor"]["avatar"]?>" alt="<?php echo $value["Doctor"]["cn_name"]?>">
                            </div>
                            <div class="list_center">
                                <p class="title"><?php echo $value["Doctor"]["cn_name"]?><span>（<?=$value["Doctor"]["en_name"]?>）</span> </p>
                                <div class="wenan">
                                	<p>所属医院：<span><?php echo $value["Doctor"]["hospital"]?></span></p>
                                	<p>医生职务：<span><?php echo $value["Doctor"]["duty"]?></span>&nbsp;从医年限：<span><?php echo $value["Doctor"]["job_duration"]?></span>&nbsp;最高学历：<span><?=$education[$value["Doctor"]["highest_education"]]?></span></p>
                                    <p>擅长项目：<i><?=$value["Doctor"]["projects"]?></i></p>
                                	<p>已有<em><?=$value["Doctor"]["appoint_num"]?></em>位病友预约该医生</p>
                                </div>                               
                                <div class="score_m">
                                    <div>
                                       <?php 
                                        for ($x=1; $x<=$value['Doctor']['popularity']; $x++) {
                                          echo "<img src=/images/hospital/wujiaoxing_01.jpg  alt=''>";
                                            } 
                                        ?> 

                                    </div>
                                    <span><?=$value["Doctor"]["popularity"]?>分</span>
                                </div>
                                <p class="wenan_m"><?=$value["Doctor"]["introduction"]?></p>
                            </div>
                            </a>
                            <div class="list_right">
                                <p></p>
                                <a class="pc_yu" href="javascript:void(0)" title="" onclick="_user.doctor_appoint(<?php echo $value["Doctor"]["id"];?>);_user.showDiv(1);" rel="nofollow">我要预约</a>
                                <a class="ph_yu" href="/mobile/order_doctor/<?php echo $value["Doctor"]["id"].'.html';?>" title="" rel="nofollow">预约</a>
                            </div>
                        </li>
                    <?php endforeach;?>

                    </ul>
                </div>
                <?php if(sizeof($dataList) >= 6){?>
                <div class="more" onclick="_page.getDoctor(this,<?php echo intval($cid);?>)">
                    <a href="javascript:void(0)" title="">加载更多</a>
                </div>
                <?php }?>
            </div>
            <div class="flot_right">
                <div class="right_top">
                    <h4>加我微信&nbsp;沟通更快</h4>
                    <div class="touxiang">
                        <p>Nana <br>行程顾问</p>
                    </div>                
                </div>
                <div class="right_cen">
                    <h4>无忧承诺</h4>
                    <p>优质医院，实地考察</p>
                    <p>快速预约，直达医院</p>
                    <p>低价保证，服务保障</p>
                </div>
                <div class="right_bot"> <a href="/site/index/" target="_blank">
                    <img src="/images/hospital/guanggao.jpg" alt="赴泰考察"></a>
                </div>
            </div>
        </div>
        <div class="box_bg">
            <div class="box">
                <div class="close"></div>
				<form  class="dataForm" action="/doctor/index" method="post" >
				<input type="hidden" name="did" id="did" value="0"/>   
                <div class="content">
                    <div class="con-1">
                        <label for="">希望就诊医生：</label>
                        <span id="hospitalName"></span>
                    </div>
                     <div class="con-2">
                        <label for="">病情描述：</label>
                        <textarea name="description" value=""></textarea>
                    </div>
                    <div class="con-3">
                        <label for="">您的姓名：</label>
                        <input type="text" placeholder="请输入您的姓名" name="user_name">
                    </div>
                    <div class="con-3">
                        <label for="">您的手机号：</label>
                        <input type="text"  placeholder="请输入手机号" id="u_mobile" name="u_mobile"  datatype="m" errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！">
                    </div>
                    <div class="con-4">
                        <label for="">验证码：</label>
                        <input type="text" name="u_code" value="" datatype="*6-6" errormsg="请输入6位验证码！"  nullmsg="请输入6位验证码！" autocomplete="off">
                        <input class="get_code" type="button"  id="send_mc_btn" onclick="_user.sendRcode('send_code_all')" value="获取验证码"/>
                    </div>
                    <div class="con-5">
                        <span></span>
                        <button>马上预订</button>
                    </div>
                </div>
				<input type="hidden" name="dosubmit" value="1"/>    
				</form>       
            </div>
        </div>
    </div>
   <script>
        $(".close").click(function(){
            $(".box_bg").fadeOut();
        }) 
		$(".dataForm").Validform({
			tiptype:function(msg){
				layer.alert(msg,{icon:0});   
			},
			tipSweep:true,
			ajaxPost:true,
			callback:function(o){
				if (o.status == 200) {
					layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});  
                    $(".box_bg").fadeOut();            
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
				} else {
					layer.msg(o.message,{
                        icon:0,
                        skin:'layer_mobile_login'
                    });    
				} 
			}
		});
    </script>