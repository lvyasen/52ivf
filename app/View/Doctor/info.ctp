<?php $this->Sgwy->addCss('/css/doctor/info.css');?>
<?php $this->Sgwy->addCss('/css/jquery.mCustomScrollbar.css');?>
<?php $this->Sgwy->addJs('/js/doctor/info.js');?>
<?php $this->Sgwy->addJs('/js/jquery.mCustomScrollbar.concat.min.js');?>
<?php echo $this->Html->script('/js/mobile/user.js');?>
<?php echo $this->Html->script('/js/page.js');?>   
<div class="doc_info">
     <div class="wav_title">
        <p>医生详情</p>
        <img class="lazy" onclick="history.go(-1)" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
    </div>
        <div class="banner_pc"></div>
        <div class="nav_ph"></div>
        <div class="contain">
            <div class="grade">
                <a href="/">首页</a> > <a href="/doctor/index/">热门医生</a> > 医生详情
            </div>   
            <div class="top">
                <div class="top_left">
                    <img src="<?=$info['Doctor']['avatar']?>" alt="">
                </div>
                <div class="top_center">
                    <h3><?=$info["Doctor"]["cn_name"]?><i>（<?=$info["Doctor"]["en_name"]?>）</i><span>&nbsp;认证</span></h3>
                    <p class="koubei">口碑：<span>&nbsp;<?=$info["Doctor"]["public_praise"]?></span>&nbsp;&nbsp;人气：<span>&nbsp;<?=$info["Doctor"]["popularity"]?></span>&nbsp;&nbsp;活跃：<span>&nbsp;<?=$info["Doctor"]["active"]?></span>&nbsp;&nbsp;擅长项目：&nbsp;&nbsp;<em><?=$info["Doctor"]["projects"]?></em></p>
                    <p>所属医院：<span><?=$info["Doctor"]["hospital"]?></span></p>
                    <p class="pyi">医生职务：<span><?=$info["Doctor"]["duty"]?></span>从医经验：<span><?=$info["Doctor"]["job_duration"]?></span>最高学历：<span><?=$education[$info["Doctor"]["highest_education"]]?></span></p>
                    <p class="jianjie"><span>资历：</span><?=$info["Doctor"]["description"]?></p>
                </div>
                <div class="top_right">
                    <a class="t_y">
                    <img class="lazy" src="/images/common/grey.gif" data-original="/images/hospital/bottom.png">
                    </a>
                </div>
            </div>
            <div class="top_ph">
                <div class="head_img">
                    <img src="<?=$info['Doctor']['mb_image']?>">
                </div>
                <h1><?=$info["Doctor"]["cn_name"]?><span>医生</span></h1>
                <ul>
                    <li>
                        <p><?=$info['Doctor']['appoint_num']?></p>
                        <span>预约</span>
                    </li>
                    <li>
                        <p><?=$info['Doctor']['case_num']?></p>
                        <span>案例</span>
                    </li>
                    <li>
                        <p><?=$info['Doctor']['follow_num']?></p>
                        <span>咨询排行</span>
                    </li>
                </ul>
                <div class="skill">
                    <p>技术：</p>
                    <span><?=$info['Doctor']['projects']?></span>
                </div>
            </div>

            <div class="con_left">
            
                <div class="rule">
                    <h5>专业背景</h5>
                   
                    <div class="rule_bg">
                        <div class="rule_platform">
                            <p><?=$info['Doctor']['introduction']?></p>
                        </div>
                    </div>
                </div>
                <div class="hos_score">
                    <h5>医生评分</h5>

                    <div class="score_cen">
                        <ul>
                            <li>
                                <p>医院环境<span><?=$info['Doctor']['environment_score']?></span></p>
                                <div>
                                    <?php 
                                    for ($x=1; $x <= $info['Doctor']['environment_score']; $x++) {
                                      echo "<img src=/images/hospital/wujiaoxing_01.jpg>";
                                        } 
                                    ?> 
                                  
                                </div>
                            </li>
                            <li>
                                <p>医疗技术<span><?=$info['Doctor']['skill_score']?></span></p>
                                <div>
                                    <?php 
                                    for ($x=1; $x <= $info['Doctor']['skill_score']; $x++) {
                                      echo "<img src=/images/hospital/wujiaoxing_01.jpg>";
                                        } 
                                    ?> 
                                </div>
                            </li>
                            <li>
                                <p>服务评分<span><?=$info['Doctor']['service_score']?></span></p>
                                <div>
                                    <?php 
                                    for ($x=1; $x <= $info['Doctor']['service_score']; $x++) {
                                      echo "<img src=/images/hospital/wujiaoxing_01.jpg>";
                                        } 
                                    ?> 
                                </div>
                            </li>
                            <li>
                                <p>综合指数<span><?=$info['Doctor']['composite_score']?></span></p>
                                <div>
                                    <?php 
                                    for ($x=1; $x <= $info['Doctor']['composite_score']; $x++) {
                                      echo "<img src=/images/hospital/wujiaoxing_01.jpg>";
                                        } 
                                    ?> 
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="patient" id="loadPage">
                    <h5 class="bo">患者评价</h5>
                    <div class="text_writ">
                        <textarea name="" id="" placeholder="我来说两句..."></textarea>
                        <!-- <p>还可以输入<span>140</span>字</p> -->
                        <div>
                            <a class="publish">发表评价</a>
                            <!-- <input type="file" id="file">
                            <label for="file"></label> -->
                        </div>
                    </div>
                    <ul class="pat_list"> 
                        <?php foreach ($comments as $key => $value):?>
                        <li> 
                            <div class="pinglu">
                                <div class="head_pic">
                                    <img src="<?=$value['Comment']['avatar']?>">
                                </div>
                                <div class="conte">
                                    <div class="namec"><?=$value['Comment']['username']?></div>
                                    <div class="contc"><?=$value['Comment']['content']?></div>
                                    <div class="timec"><?php echo Ivf::formatTime($value['Comment']['comment_time']);?></div>
                                </div>
                            </div>
                        </li>
                        <?php endforeach;?>  
                    </ul> 
                    <?php if(sizeof($comments) >= 5){?>
                    <div class="more" onclick="_page.getPageComment(this,<?=$info['Doctor']['id']?>,'doctor_comment')"> 
                        <a href="javascript:void(0)" title="">加载更多</a>
                    </div>
                    <?php }?>
                </div>
            </div>
            <div class="con_right">
                <h5>远程咨询服务</h5>
                <ul>
                    <li class="direct_list">
                        <img src="/images/hospital/Dr.Thitikorn.jpg">
                        <div class="direct">
                            <span>提迪贡 Thitikorn</span>
                            <a href="/doctor/info/15.html" title="">预约</a>
                            <p>全球院长，泰国著名的试管婴儿医生，二十多年不孕不育的治疗经验。</p>
                        </div>
                    </li>
                    <li class="direct_list">
                        <img src="/images/hospital/Dr.Kriengchai.jpg">
                        <div class="direct">
                            <span>科林柴 Kriengchai</span>
                            <a href="/doctor/info/8.html" title="">预约</a>
                            <p>杰特宁医院主治医生，是杰特宁接待患者最多的医生之一。</p>
                        </div>
                    </li>
                    <li class="direct_list">
                        <img src="/images/hospital/Dr.Suchada.jpg">
                        <div class="direct">
                            <span>苏查妲 Suchada</span>
                            <a href="/doctor/info/13.html" title="">预约</a>
                            <p>是拉差·帕亚泰主任医生，泰国最具实力的生殖医学医生之一。</p>
                        </div>
                    </li>
                </ul>
            </div>    
        </div>   
        <div class="box_bg">
            <div class="box">
                <div class="close"></div>
                <form  class="dataForm" action="/doctor/index" method="post" >
                <input type="hidden" name="did" id="did" value="<?=$info['Doctor']['id']?>"/>   
                <div class="content">
                    <div class="con-1">
                        <label for="">希望就诊医生：</label>
                        <span id="hospitalName"><?=$info['Doctor']['cn_name']?></span>
                    </div>
                     <div class="con-2">
                        <label for="">病情描述：</label>
                        <textarea name="description" value=""></textarea>
                    </div>
                    <div class="con-3">
                        <label for="">您的姓名：</label>
                        <input name="user_name" type="text" placeholder="请输入您的姓名">
                    </div>
                    <div class="con-3">
                        <label for="">您的手机号：</label>
                        <input type="text"  placeholder="请输入手机号" id="u_mobile" name="u_mobile"  datatype="m" errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！">
                    </div>
                    <div class="con-4">
                        <label for="">验证码：</label>
                        <input type="text" name="u_code" value="" datatype="*6-6" errormsg="请输入6位验证码！"  nullmsg="请输入6位验证码！" autocomplete="off">
                        <input class="get_code" type="button"  id="send_mc_btn" onclick="_user.sendRcode('send_code_all')" value="获取验证码"/>
                    </div>
                    <div class="con-5">
                        <span></span>
                        <button>马上预订</button>
                    </div>
                </div>
                <input type="hidden" name="dosubmit" value="1"/>    
                </form>       
            </div>
        </div>
    </div>
  
