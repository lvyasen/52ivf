<?= $this->Sgwy->addCss('/css/answer/index.css'); ?>
<?= $this->Sgwy->addJs("/js/hospital/swiper.jquery.min.js"); ?>
<?= $this->Sgwy->addJs("/js/answer/index.js"); ?>
<!-- <?= $this->Sgwy->addJs('/plugin/ctimg/js/jqthumb.js'); ?>
<?= $this->Sgwy->addJs('/plugin/ctimg/js/main.js'); ?> -->
<?php echo $this->Html->script('/plugin/ctimg/js/jqthumb.js');?>  
<?php echo $this->Html->script('/plugin/ctimg/js/main.js');?>  
<?php echo $this->Html->script('/js/page.js');?>  

    <div class="answer">
        <div class="wav_title">
        <p>问答</p>
        <img class="lazy" onclick="window.location.href='/community/index/' " src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png"><span>首页</span>
        </div>
        <div class="content">
            <div class="main">
                <div class="top">
                    <p><a rel="nofollow" href="/community/index/">首页</a> >  问答</p>     
                    <h5>社区问答</h5>           
                    <div class="search">
                        <input type="text" id="seachKey" value="<?=$seachKey?>" placeholder="看看你的问题是否有人询问过(请输入关键词)"> 
                        <button type="" onclick="seachVal()">搜索</button>
                        <a rel="nofollow" href="/personal/my_answers/" title="">+我要提问</a>
                    </div>
                </div>
                <div class="area">
                    <h2>问答专区</h2>
                    <img class="lazy" src="/images/common/grey.gif" data-original="/images/answer/tupian.png">
                    <div class="area_problem">
                        <h3>有多少姐妹是因宫外孕而试管的？成功率吗？</h3>
                        <ul> 
                            <!-- <?php foreach ($topList as $key => $value):?>
                           <li><a href="/answer/info/<?php echo $value["Share"]["id"].".html";?>"><?=$value['Share']['title']?></a></li>
                            <?php endforeach;?>  --> 
                            <li><a rel="nofollow" href="/answer/info/66.html">我来月经超过24小时还可以取内膜吗？</a></li>
                            <li><a rel="nofollow" href="/answer/info/68.html">为什么我取了12个卵子，但最后却只形成了7个胚胎?</a></li>
                            <li><a rel="nofollow" href="/answer/info/16.html">很多人说试管婴儿的寿命不长的，是吗？</a></li>
                            <li><a rel="nofollow" href="/answer/info/28.html">去泰国做试管婴儿需要准备什么证件？</a></li>
                            <li><a rel="nofollow" href="/answer/info/30.html">泰国试管婴儿包成功吗？</a></li>
                        </ul>
                    </div>   
                </div>
                <div class="banner">
                    <img class="lazy" src="/images/common/grey.gif" data-original="/images/answer/banner_wenda.jpg" alt="泰国试管婴儿攻略">
                    <div>
                        <input type="text"  id="seachKeyMb" value="<?=$seachKey?>" placeholder="看看你的问题是否有人询问过">
                        <i></i>
                    </div>                    
                </div>
                <div class="question_tab">
                    <div class="tab_btn">
                        <a rel="nofollow" href="/answer/index/<?php echo !empty($seachKey)?"0-".$seachKey.".html":"";?>" class="<?php echo empty($cid) || $cid==0 ? "btn_block":"btn_hidden";?>">热门回答</a>
                        <a rel="nofollow" href="/answer/index/<?php echo !empty($seachKey)?"1-".$seachKey.".html":"1.html";?>" class="<?php echo !empty($cid) && $cid==1 ? "btn_block":"btn_hidden";?>">最新问题</a>
                        <a rel="nofollow" href="/answer/index/<?php echo !empty($seachKey)?"2-".$seachKey.".html":"2.html";?>" class="<?php echo !empty($cid) && $cid==2 ? "btn_block":"btn_hidden";?>">待回答问题</a>
                    </div>
                    <div class="white_block"></div>
                    <div class="tab_con">
                        <div class="btncontent">
                            <div class="new_list_b" id="loadPage">
                            <ul>
                                <?php foreach ($dataList as $key => $value):?>
                                <li>
                                    <div class="list_li">
                                        <div class="touxiang"><img src="<?=$value['Share']['avatar']?>" alt="<?=$value['Share']['username']?>" alt="泰国试管婴儿攻略"></div>
                                        <div class="list_b_right">
                                            <a href="/answer/info/<?php echo $value["Share"]["id"].".html";?>">
                                            <div class="title_top">
                                            <?php 
                                            echo '【'.str_replace('？', '',str_replace('?', '', $value['Share']['title'])).'】';
                                            echo Ivf::trimall($value['Share']['content']); 
                                            ?></div> </a>
                                            <div class="bottam">
                                                <div class="b_name"><?=$value['Share']['username']?></div>
                                                <div class="b_right">
                                                    <div class="b_pic1"><?=$value['Share']['browse_num']?></div>
                                                    <div class="b_pic2"><?=$value['Share']['comment_num']?></div>
                                                    <div class="b_pic3 up <?php echo $value['Share']['myagree']>0?'add_up':'';?>" <?php if($value['Share']['myagree']==0){?>onclick="adAgree(this,<?=$value["Share"]["id"]?>,'agree_answer')"<?php }?>><?=$value['Share']['agree_num']?></div>
                                                    <div class="b_right_mm">
                                                <div><img src="/images/hospital/y_1.png" />&nbsp;&nbsp;<?=$value['Share']['browse_num']?></div>
                                                <div><img src="/images/hospital/y_2.png" />&nbsp;&nbsp;<?=$value['Share']['comment_num']?></div>
                                                <div><img src="/images/hospital/y_3.png" />&nbsp;&nbsp;<?=$value['Share']['agree_num']?></div>
                                            </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="img_b">
                                        <?php foreach ($value['Share']['images'] as $k => $v):?> 
                                            <img class="demo" src="<?php echo $v['Picture']['thumb'];?>"?>   
                                        <?php endforeach;?>
                                    </div>
                                </li>
                                <?php endforeach;?> 
                            </ul>
                            </div>
                            <?php if(sizeof($dataList) >= 6){?>
                            <div class="more" onclick="_page.getAnswer(this,<?php echo intval($cid);?>,'<?php echo empty($seachTitle)?$seachKey:$seachTitle;?>')">
                                <a href="javascript:void(0)" title="">加载更多</a>
                            </div>
                            <?php }?> 
                                
                        </div>  
                    </div>
                </div>

            </div>
            <div class="fixed">
                <div class="fixed_pro">
                    <p>已解决问题:<em>21536</em></p>
                    <p><span>51536</span>人贡献宝贵经验</p>
                </div> 
                <?php if(isset($userInfo) && !empty($userInfo)){?>
                <div class="fixed_login">
                    <img src="<?php echo $userInfo['avatar'];?>">
                    <a href="/personal/index/" title=""><?php echo empty($userInfo['nick_name'])?'未设置昵称':$userInfo['nick_name'];?></a> 
                </div>
                <?php }else{?> 
                <div class="fixed_login">
                    <img src="/images/temp/touxiang_001.png">
                    <a href="javascript:void(0)" title="" class="login">请先登录</a>
                    <p>以便提问和回答问题</p>
                </div>
                <?php }?>
                <div class="fixed_site">
                    <p>热门医院</p>
                    <a href="/hospital/info/9.html">全球生殖中心</a>
                    <a href="/hospital/info/2.html">杰特宁试管婴儿中心</a>
                    <a href="/hospital/info/1.html">泰国帕亚泰·是拉差</a> <br>
                    <a href="/hospital/info/4.html">泰国iBoby生殖中心</a>
                </div>
            </div>
        
        </div>
    </div>
    <script type="text/javascript">
    function seachVal(){
        var val=$("#seachKey").val();
         window.location.href = '/answer/index/0-' + val +".html";
    }
    $("#seachKeyMb").blur(function(){
        var val=$("#seachKeyMb").val();
         window.location.href = '/answer/index/0-' + val +".html";
    });
    <?php if(sizeof($dataList) == 0){?>
            layer.msg("您搜索的问答不存在",{icon:5,time:1000,skin:'layer_mobile_login'});  
    <?php }?>
    </script>