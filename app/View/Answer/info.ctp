<?php echo $this->Html->css("/css/answer/info.css");?>
<?= $this->Sgwy->addJs('/js/answer/info.js'); ?>
    <div class="answer">       <div class="wav_title">
        <p>问答详情</p>
        <img class="lazy" onclick="document.referrer === '' ?
          window.location.href = '/' : window.history.go(-1);" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
        </div>
        <div class="content">
            <div class="main">
                <div class="top">
                    <p><a  rel="nofollow" href="/community/index/">首页</a> > <a rel="nofollow" href="/answer/index/">问答列表</a> > 问答</p>     
                    <h2>社区问答</h2>           
                    <div class="search">
                        <input type="text"  id="seachKey" value=""  placeholder="看看你的问题是否有人询问过(请输入关键词)">
                        <button type="" onclick="seachVal()">搜索</button>
                        <a rel="nofollow" href="/personal/my_answers/" title="">+我要提问</a>
                    </div>
                </div>                
                <div class="info">
                    <div class="blog">
                        <img class="lazy" src="<?=$info["Share"]["avatar"]?>" data-original="<?=$info["Share"]["avatar"]?>">
                        <div class="blog_text">
                            <h4><?=$info["Share"]["title"]?></h4>
                            <h1><?=$info["Share"]["content"]?></h1>
                            <?php foreach ($info['Share']['images'] as $k => $v):?> 
                            <img alt="泰国试管婴儿,泰国试管婴儿医院,泰国试管婴儿费用,泰国试管婴儿攻略,杰特宁,全球生殖中心,帕亚泰是拉差,碧娅威国际医院, 泰国第三代试管婴儿,试管无忧" class="lazy"  data-original="<?php echo $v['Picture']['thumb'];?>" src="<?php echo $v['Picture']['thumb'];?>"?>   
                            <?php endforeach;?>
                            <div class="nate">
                                <span class="name"><?=$info["Share"]["username"]?></span>
                                
                                <span class="date"><?=$info["Share"]["create_time"]?></span>
                            </div>
                            <div class="browse bottam">
                                <span class="like adagree b_pic3 <?php echo $info['Share']['myagree']>0?'add_up':'';?>" <?php if($info['Share']['myagree']==0){?>onclick="adAgree(this,<?=$info["Share"]["id"]?>,'agree_answer')"<?php }?>><?=$info["Share"]["agree_num"]?></span>
                                <span class="look"><?=$info["Share"]["browse_num"]?></span>
                                
                            </div>
                        </div>
                    </div>
                    <div class="blog_ph">
                        <h4><?=$info["Share"]["title"]?></h4>
                        <div class="nate"> 
                            <img class="lazy" src="<?=$info["Share"]["avatar"]?>" data-original="<?=$info["Share"]["avatar"]?>">
                            <div>
                                <p class="name"><?=$info["Share"]["username"]?></p>
                                <span class="date"><?=date("m-d H:i",strtotime($info["Share"]["create_time"]))?></span>
                            </div>
                            <div class="browse bottam">
                                <span class="like adagree b_pic3 <?php echo $info['Share']['myagree']>0?'add_up':'';?>" <?php if($info['Share']['myagree']==0){?>onclick="adAgree(this,<?=$info["Share"]["id"]?>,'agree_answer')"<?php }?>><?=$info["Share"]["agree_num"]?></span>
                                <span class="look"><?=$info["Share"]["browse_num"]?></span>
                            </div>
                        </div>
                        <div class="blog_ph_text">
                            <?=$info["Share"]["content"]?>
                            <?php foreach ($info['Share']['images'] as $k => $v):?> 
                            <img class="lazy"  data-original="<?php echo $v['Picture']['thumb'];?>" src="<?php echo $v['Picture']['thumb'];?>"?>   
                            <?php endforeach;?> 
                        </div>
                    </div>
                    <div class="reply" id="loadPage"> 
                        <?php if(sizeof($comments) >= 1){?><p>全部回答</p><?php }?>
                        <ul class="moreAns">
                            <?php foreach ($comments as $key => $value):?> 
                            <li>
                                <div class="pinglu">
                                    <div class="head_pic">
                                        <img src="<?=$value['Comment']['avatar']?>">
                                    </div>
                                    <div class="conte">
                                        <div class="namec"><?php echo empty($value['Comment']['username'])?'...':$value['Comment']['username'];?></div>
                                        <div class="contc"><?=$value['Comment']['content']?></div>
                                        <div class="timec"><?php echo Ivf::formatTime($value['Comment']['comment_time']);?></div>
                                        <a class="creplay" href="javascript:void(0);" onclick="replayComment(<?=$value['Comment']['id']?>)">回复</a> 
                                        <?php if(!empty($value['Comment']['childCmt'])){?>
                                        <span class="moreHui">更多回复</span> 
                                        <?php }?>
                                    </div>
                                    <!--子回复开始-->
                                    <div class="conteBox">
                                    <?php if(!empty($value['Comment']['childCmt'])){?>
                                    <?php foreach ($value['Comment']['childCmt'] as $kk => $vv):?> 
                                    <div class="conte">
                                        <div class="namec"><?php echo $vv['replayName'];?></div>
                                        <div class="contc"><?=$vv['content']?></div>
                                        <div class="timec"><?php echo Ivf::formatTime($vv['comment_time']);?></div>
                                        <a class="creplay" href="javascript:void(0);" onclick="replayComment(<?=$vv['id']?>)">回复</a>
                                    </div>
                                    <?php endforeach;?>  
                                    <?php }?>
                                    </div> 
                                    <!--子回复结束-->
                                </div>
                            </li>
                            <?php endforeach;?>     
                        </ul>
                        <div class="reBg">
                        <form  class="replayForm" action="/answer/replay_answer"  method="post" >
                            <div class="ph_btn_two">
                                <a>x</a>
                                <div class="text_two">
                                    <textarea  name="content" datatype="*"  nullmsg="请填写评论内容！"  placeholder="我来说一说..."></textarea>
                                    <input type="submit" class="button" value="回复"/>
                                </div>
                            </div> 
                            <input type="hidden" id="repalyVal" name="reply_id" value="0"/>  
                            <input type="hidden" name="cid" value="<?=$info["Share"]["id"]?>"/>  
                            <input type="hidden" name="dosubmit" value="1"/>                 
                        </form></div> 
                        <?php if(sizeof($comments) >= 5){?>
                        <div class="more"> 
                            <a rel="nofollow" href="javascript:void(0)" title="" onclick="_page.getPageComment(this,<?=$info['Share']['id']?>,'answer_comment')">加载更多</a>
                        </div>
                        <?php }?>

                        <form  class="answerForm" action="/answer/replay"  method="post" >
                        <div class="text_box">
                            <p>发表回复:</p>
                            <textarea name="content" datatype="*"  nullmsg="请填写评论内容！"></textarea> 
                            <div class="code">    
                                <a class="release" href="javascript:void(0);">发表回复</a>
                                <input type="text" name="u_code" value="" placeholder="请输入验证码" datatype="*4-4" errormsg="请输入4位数验证码！"  nullmsg="请输入验证码！"  autocomplete="off">
                                <div class="code_img">
                                    <img src="/personal/captcha" title="点击更换" onclick="this.src='/personal/captcha/?'+Math.random()"/>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="cid" value="<?=$info["Share"]["id"]?>"/>  
                        <input type="hidden" name="dosubmit" value="1"/>                 
                        </form>   
                        <a rel="nofollow" href="/mobile/pub/3-<?=$info["Share"]["id"]?>.html" class="btn_one" rel="nofollow">我要评论</a> 
                    </div>
                </div>
                

            </div>
            <div class="fixed">
                <div class="fixed_pro">
                    <p>已解决问题:<em>21536</em></p>
                    <p><span>51536</span>人贡献宝贵经验</p>
                </div> 
                <?php if(isset($userInfo) && !empty($userInfo)){?>
                <div class="fixed_login">
                    <img src="<?php echo $userInfo['avatar'];?>">
                    <a rel="nofollow" href="/personal/index/" title=""><?php echo empty($userInfo['nick_name'])?'未设置昵称':$userInfo['nick_name'];?></a> 
                </div>
                <?php }else{?> 
                <div class="fixed_login">
                    <img src="/images/temp/touxiang_001.png">
                    <a rel="nofollow" href="javascript:void(0)" title="" class="login">请先登录</a>
                    <p>以便提问和回答问题</p>
                </div>
                <?php }?>
                
            </div>
        </div>
    </div>  
<?= $this->element('tophui'); ?>