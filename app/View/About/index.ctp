<?= $this->Sgwy->addCss('/css/about/mediaMobile.css'); ?>
<?= $this->Sgwy->addCss('/css/about/flexslider.css'); ?>
<link rel="stylesheet" href="http://data.dadaabc.com/skin/lib/css/bootstrap.min.css">
<link rel="stylesheet" href="http://data.dadaabc.com/skin/lib/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/about/index.css">
<?= $this->Sgwy->addJs('/js/about/index.js'); ?>
<?= $this->Sgwy->addJs('/js/about/flexslider.min.js');?>
<div class="about">
    <div class="wav_title">
        <p>品牌故事</p>
        <img alt="" class="lazy" onclick="history.go(-1)" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
    </div>
	<div class="about1">
	    <div class="ban"></div>
	    <div class="about_w">
	        <div class="big_title">关于我们</div>
	        <div class="about_one">
	            <p>试管无忧是国内首家也是唯一一家专注“疑难试管”的服务医院，公司旨在通过整合、连接全球范围内优质辅助生殖医疗资源，为用	户提供更有针对性的精准服务。全面专业、安全可靠的精准预约、陪诊一对一、高龄辅助、VIP尊享等一站式海外就医解决方案，最大化提升就医效率和成功率。</p>
	            <p class="p_span">试管婴儿手术是一项复杂的医疗服务项目。对于患者而言，医院的技术水平、治疗方案、设备的先进性、药品质量都会对治疗的最后结果（即成功率）产生至关重要的影响。</p>
	            <p>目前已经与泰国、美国、台湾、香港、澳门、新加坡、柬埔寨等国家和地区的30余所生殖医院级医生团队建立了良好而深入的合作关系。</p>
	        </div>
	    </div>
	    <div class="about_g">
	        <div class="big_title">让试管无忧成为你试管就医的引路人</div>
	        <div class="about_two">
	            <div class="left_pic"><a href="/video/index/"><img alt="泰国试管婴儿攻略" src="/images/about/p-1.jpg" id="pp_01"><img src="/images/about/p-2.jpg" id="pp_02"><img alt="泰国试管婴儿攻略" src="/images/about/p-3.jpg" id="pp_03"></a></div>
	            <div class="right_pic">
	                <div class="p_box" id="xx_01"><img alt="泰国试管婴儿攻略" src="/images/about/xuanxiang.png" id="pc_01"></div>
	                <div class="p_box" id="xx_02"><img alt="泰国试管婴儿攻略" src="/images/about/xuanxiang.png" id="pc_02"></div>
	                <div class="p_box" id="xx_03"><img alt="泰国试管婴儿攻略" src="/images/about/xuanxiang.png" id="pc_03"></div>
	            </div>
	        </div>
	    </div>
	    <div class="about_w">
	        <div class="big_title">更多患者的心声</div>
	        <div class="about_three">
	            <div class="ffl1"></div>
	            <div class="ffl2" style="height: 230px;">
	                <span>某外企高管 Lisa小姐</span><p>"我是一家外企公司的高管，一直忙于工作，生育问题被忽略了，35岁了才想起来要孩子，突然想生的时候，发现自己已经失去了自然生育的能力，为了提高就医效率，贻误时机，去了很多医院，花了很多精力与时间都没能怀孕，整个人都充满了疑惑、心酸与疲惫。机缘巧合之下结识了试管无忧，通过试管无忧选择了国外的医院，技术好，服务周到，终于圆了我做母亲的梦想！"</p>
	            </div>
	            <div class="ffl3">
	                <span>求医无门的 张妈妈</span><p>"……我因为患有卵巢过度刺激综合征，一直不能生，心里很落魄，也找不到好的解决办法，经朋友介绍认识了试管无忧，想着抱着试一试的心态。后来终于通过试管无忧匹配了一家泰国的医院，采用最先进的方案进行手术。备孕期间，试管无忧的就医顾问时常跟我取得联系，询问我目前的近况以及告知我一些注意事项与保胎只是。现在已经是两个宝宝的妈妈了，感谢试管无忧！"</p>
	            </div>
	            <div class="ffl4"></div>
	        </div>
	    </div>
	    <div class="about_g">
	        <div class="big_title">试管无忧的服务</div>
	        <div class="about_four">
	            <div class="left_text">
	                <div class="box1"></div>
	                <div class="box5">远程咨询 ￥399</div>
	                <div class="box7">
	                	为国内多年不孕、多次失败的患者，提供境外试管医生一对一问诊服务。分析病情，找出失败的真正原因，为后续的治疗提供最佳依据。<br><span>（此服务适合在出国前想要详细了解自身情况并需要国外医生提供诊前分析服务的患者）
	                    </span>
	                </div>
	                <div class="box2"></div>
	                <div class="box5">金牌无忧 ￥36000</div>
	                <div class="box7">
	                    针对部分希望有更好服务体验的患者，提供全程陪诊、星级酒店入住、就餐安排等一条龙服务。<br><span>（此服务包吃包住，适合想要享受更好服务的患者）</span>
	                </div>
	            </div>
	            <div class="right_text">
	                <div class="box6">精准预约 ￥7999</div>
	                <div class="box7">
	                    结合个性情况，匹配名医、指定预约和个性导诊，真正做到一对一个性治疗，从根本上提高就医效率和成功率。<br><span>（此服务适合对国外就诊流程不熟悉，无法自主预约医生和医生沟通有障碍的患者）</span>
	                </div>
	                <div class="box3"></div>
	                <div class="box6">VIP尊享 ￥16800</div>
	                <div class="box7">
	                    为国内特殊的不孕不育患者，提供境外生殖医院“特殊案例”就医申请，以及第三方志愿者资源对接与翻译服务。<br><span>（此服务适合特需试管的患者）</span>
	                </div>
	                <div class="box4"></div>
	            </div>
	        </div>
	    </div>
	    <div class="about_w">
	        <div class="big_title">丰富的线下活动</div>
	        <div class="about_five">
	            <div class="section-content">
	                <!--大图-->
	                <div id="slider" class="flexslider">
	                    <ul class="slides">
	                        <li class="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
	                            <img alt="泰国试管婴儿攻略" src="/images/about/L1.jpg" draggable="false" style="width: 1072px!important;">
	                            <p class="flex-caption">试管无忧</p>
	                        </li>
	                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide">
	                            <img alt="泰国试管婴儿攻略" src="/images/about/L2.jpg" draggable="false" style="width: 1072px!important;">
	                            <p class="flex-caption">试管无忧</p>
	                        </li>
	                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
	                            <img alt="泰国试管婴儿攻略" src="/images/about/L3.jpg" draggable="false" style="width: 1072px!important;" >
	                            <p class="flex-caption">试管无忧</p>
	                        </li>
	                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
	                            <img alt="泰国试管婴儿攻略" src="/images/about/L4.jpg" draggable="false" style="width: 1072px!important;">
	                            <p class="flex-caption">试管无忧</p>
	                        </li>
	                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
	                            <img alt="泰国试管婴儿攻略" src="/images/about/L5.jpg" draggable="false" style="width: 1072px!important;">
	                            <p class="flex-caption">试管无忧</p>
	                        </li>
	                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
	                            <img alt="泰国试管婴儿攻略" src="/images/about/L6.jpg" draggable="false" style="width: 1072px!important;">
	                            <p class="flex-caption">试管无忧</p>
	                        </li>
	                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
	                            <img alt="泰国试管婴儿攻略" src="/images/about/L7.jpg" draggable="false" style="width: 1072px!important;">
	                            <p class="flex-caption">试管无忧</p>
	                        </li>
	                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
	                            <img alt="泰国试管婴儿攻略" src="/images/about/L8.jpg" draggable="false" style="width: 1072px!important;">
	                            <p class="flex-caption">试管无忧</p>
	                        </li>

	                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
	                            <img alt="泰国试管婴儿攻略" src="/images/about/L9.jpg" draggable="false"  style="width: 1072px!important;">
	                            <p class="flex-caption">试管无忧</p>
	                        </li>
	                    </ul>
	                	<ul class="flex-direction-nav">
	                		<li class="flex-nav-prev"><a class="flex-prev" href="#">Previous</a></li>
	                		<li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li>
	                	</ul>
	            	</div>
		            <div id="carousel" class="flexslider">     
		                <div class="flex-viewport" style="overflow: hidden; position: relative;">
		                	<ul class="slides" style="width: 4600%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
		                        <li style="width: 120px; margin-right: 11px; float: left; display: block;"><img alt="泰国试管婴儿攻略" src="/images/about/x1.jpg" draggable="false" width="122"></li>
		                        <li style="width: 120px; margin-right: 11px; float: left; display: block;" class="flex-active-slide"><img src="/images/about/x2.jpg" draggable="false" alt="泰国试管婴儿攻略"></li>
		                        <li style="width: 120px; margin-right: 11px; float: left; display: block;"><img alt="泰国试管婴儿攻略" src="/images/about/x3.jpg" draggable="false"></li>
		                        <li style="width: 120px; margin-right: 11px; float: left; display: block;"><img alt="泰国试管婴儿攻略" src="/images/about/x4.jpg" draggable="false"></li>
		                        <li style="width: 120px; margin-right: 11px; float: left; display: block;"><img alt="泰国试管婴儿攻略" src="/images/about/x5.jpg" draggable="false"></li>
		                        <li style="width: 120px; margin-right: 11px; float: left; display: block;"><img alt="泰国试管婴儿攻略" src="/images/about/x6.jpg" draggable="false"></li>
		                        <li style="width: 120px; margin-right: 11px; float: left; display: block;"><img alt="泰国试管婴儿攻略" src="/images/about/x7.jpg" draggable="false"></li>
		                        <li style="width: 120px; margin-right: 11px; float: left; display: block;"><img alt="泰国试管婴儿攻略" src="/images/about/x8.jpg" draggable="false"></li>
		                        <li style="width: 120px; margin-right: 11px; float: left; display: block;"><img alt="泰国试管婴儿攻略" src="/images/about/x9.jpg" draggable="false"></li>
		                    </ul>
		                </div>
		            	<ul class="flex-direction-nav">
		            		<li class="flex-nav-prev"><a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a></li>
		            		<li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li>
		            	</ul>
		            </div>
	            </div>
	        </div>
	    </div>
	</div>
	<div class="about2">
		<div class="pages">
			<div class="banner">
			    <img src="/images/nabout/about01.jpg" alt="泰国试管婴儿攻略" width="100%">
			</div>
		    <div class="wrapper">
		    	<div class="section silverbg">
		        	<div class="section-inner aboutus">
		        		<div class="section-title">
		          			<h3>关于我们</h3>
		        		</div>
				        <div class="section-content">
					          <p>试管无忧成立于2013年，是一个专注“疑难不孕”和“疑难试管”人群的海外试管服务品牌，致力于通过精准预约等个性化服务，不断提高广大出国看病人群的就医效率和成功率。通过多年的发展，试管无忧已经成为国内最具人气的海外试管知名品牌，目前70%的客户来自试管姐妹间的口碑介绍。透明、个性化、超预期的服务特点，让试管无忧赢得了业内外的一致口碑。</p>
					          <p>目前，试管无忧合作的医院遍布泰国、美国、新加坡等国家以及港澳台等地区。所有合作的医院均采用“实地考察+一站式对接”的模式，以泰国地区为例，试管无忧是杰特林生殖研究所（JE）、全球生殖中心（WW）、帕亚泰·是拉差（PH）等医院在中国地区少有的战略合作伙伴之一。据介绍，试管无忧提倡的“五心”服务（即：省心、放心、安心、贴心、热心）受到广大试管姐妹的追捧。通过更加个性、更加精准的服务方式，提高出国看病的效率和成功率，让广大试管姐妹少走弯路，让出国看病不再难，已经成为试管无忧全体成员的共同认知和奋斗目标。</p>
				        </div>
				    </div>
		        </div>
			    <div class="section">
				    <div class="section-inner dream">
				        <div class="section-title">
				          <h3>让试管无忧成为你试管就医的引路人</h3>
				        </div>
				        <div class="section-content" id="video_list" style="overflow: hidden;">
				           <div class="xbout"><a href="/video/index/"><img alt="泰国试管婴儿攻略" src="/images/nabout/a1.jpg">《泰国试管那些事儿》</a></div>
				           <div class="xbout"><a href="/video/index/"><img alt="泰国试管婴儿攻略" src="/images/nabout/a2.jpg">《高龄妈妈赴泰喜得男宝》</a></div>
				           <div class="xbout"><a href="/video/index/"><img alt="泰国试管婴儿攻略" src="/images/nabout/a3.jpg">《小忧说试管》</a></div>
				           <div class="xbout"><a href="/video/index/"><img alt="泰国试管婴儿攻略" src="/images/nabout/a4.jpg">《小忧说不孕》</a></div>
				        </div>
				    </div>
			    </div>
			    <div class="section mamashuo">
				    <div class="section-inner">
				        <div class="section-title align-center">
				          <h3>更多患者的心声</h3>
				        </div>
				        <div class="section-content">
				            <div class="item-wrap clearfix">
					            <div class="item">
					                <div class="print"><img alt="泰国试管婴儿攻略" src="/images/nabout/a1.png" alt=""></div>
						            <div class="cont">
						                <div class="title">
						                  <h4>某外企高管 Lisa小姐</h4>
						                </div>
						                <div class="say">
						                  "我是一家外企公司的高管，一直忙于工作，生育问题被忽略了，35岁了才想起来要孩子，突然想生的时候，发现自己已经失去了自然生育的能力，为了提高就医效率，贻误时机，去了很多医院，花了很多精力与时间都没能怀孕，整个人都充满了疑惑、心酸与疲惫。机缘巧合之下结识了试管无忧，通过试管无忧选择了国外的医院，技术好，服务周到，终于圆了我做母亲的梦想！"
						                </div>
						                <div class="round round2"></div>
						            </div>
					            </div>
					            <div class="item">
						            <div class="print"><img alt="泰国试管婴儿攻略" src="/images/nabout/a2.png"></div>
						         	<div class="cont">
						                <div class="title">
						                    <h4>求医无门的 张妈妈</h4>
						                </div>
						                <div class="say">
						                    "……我因为患有卵巢过度刺激综合征，一直不能生，心里很落魄，也找不到好的解决办法，经朋友介绍认识了试管无忧，想着抱着试一试的心态。后来终于通过试管无忧匹配了一家泰国的医院，采用最先进的方案进行手术。备孕期间，试管无忧的就医顾问时常跟我取得联系，询问我目前的近况以及告知我一些注意事项与保胎只是。现在已经是两个宝宝的妈妈了，感谢试管无忧！"
						                </div>
						                <div class="round round3"></div>
						            </div>
					            </div>
				            </div>
				        </div>
				    </div>
			    </div>
		        <div class="section silverbg">
		        	<div class="section-inner goodness">
				        <div class="section-title align-center">
				            <h3>试管无忧的服务</h3>
				        </div>
		                <div class="section-content">
					        <div class="line">
					            <div class="dot dot1"></div>
					            <div class="dot dot7"></div>
					        </div>
					        <div class="items">
					            <div class="desc">
						            <div class="items-img"><img alt="泰国试管婴儿攻略" src="/images/about/yuan01.png"/></div>
					                <h3 class="title">精准预约 ¥7999</h3>
						            <div class="info">
						               结合个性情况，匹配名医、指定预约和个性导诊，真正做到一对一个性治疗，从根本上提高就医效率和成功率。
						            </div>
					            </div>
					        </div>
					        <div class="items">
					            <div class="desc">
					                <div class="items-img"><img alt="泰国试管婴儿攻略" src="/images/about/yuan02.png"/></div>
					                <h3 class="title">远程问诊 ¥399</h3>
					                <div class="info">
					                	为国内多年不孕、多次失败的患者，提供境外试管医生一对一问诊服务。分析病情，找出失败的真正原因，为后续的治疗提供最佳依据。
					                </div>
					            </div>
					        </div>
					        <div class="items">
					            <div class="desc">
					                <div class="items-img"><img alt="泰国试管婴儿攻略" src="/images/about/yuan03.png"/></div>
					                <h3 class="title">VIP尊享 ¥16800</h3>
					                <div class="info">
					                	为国内特殊的不孕不育患者，提供境外生殖医院“特殊案例”就医申请，以及第三方志愿者资源对接与翻译服务。
					                </div>
					            </div>
					        </div>
					        <div class="items">
					            <div class="desc">
					                <div class="items-img"><img alt="泰国试管婴儿攻略" src="/images/about/yuan04.png"/></div>
					                <h3 class="title">金牌无忧 ¥36000</h3>
					                <div class="info">
					                	针对部分希望有更好服务体验的患者，提供全程陪诊、星级酒店入住、就餐安排等一条龙服务。
					                </div>
					            </div>
					        </div>
		                </div>
		            </div>
		        </div>
		        <div class="section">
		            <div class="section-inner offline" style="margin-bottom: 0; padding-bottom: 0;">
				        <div class="section-title">
				           <h3>丰富的线下活动</h3>
				        </div>
				        <div class="section-content">
					        <div class="offline-slider flexslider_mob">
					            <div class="flex-viewport" style="overflow: hidden; position: relative;">
					            	<ul class="slides" style="width: 5000%; transition-duration: 0s; transform: translate3d(-1276px, 0px, 0px);">
					            		<li class="clone" aria-hidden="true" style="width: 319px; margin-right: 0px; float: left; display: block;">
					                		<img alt="泰国试管婴儿攻略" src="/images/mabout/l1.jpg" draggable="false"  style="width: 100%!important;">
					                		<p class="flex-caption">试管无忧员工参观泰国BNH医院</p>
					              		</li>
							            <li class="" data-thumb-alt="" style="width: 319px; margin-right: 0px; float: left; display: block;">
							                <img alt="泰国试管婴儿攻略" src="/images/mabout/l2.jpg" draggable="false" style="width: 100%!important;">
							                <p class="flex-caption">试管无忧员工真情关爱残障儿童</p>
							            </li>
							            <li data-thumb-alt="" class="" style="width: 319px; margin-right: 0px; float: left; display: block;">
							                <img alt="泰国试管婴儿攻略" src="/images/mabout/l3.jpg" draggable="false" style="width: 100%!important;">
							                <p class="flex-caption">试管无忧员工接受培训</p>
							            </li>
							            <li data-thumb-alt="" class="" style="width: 319px; margin-right: 0px; float: left; display: block;">
							                <img alt="泰国试管婴儿攻略" src="/images/mabout/l4.jpg" draggable="false" style="width: 100%!important;">
							                <p class="flex-caption">传递爱与温暖，与福利儿童一起过圣诞</p>
							            </li>
							            <li data-thumb-alt="" class="flex-active-slide" style="width: 319px; margin-right: 0px; float: left; display: block;">
							                <img alt="泰国试管婴儿攻略" src="/images/mabout/l5.jpg" draggable="false" style="width: 100%!important;">
							                <p class="flex-caption">试管无忧员工参观泰国BNH医院</p>
							            </li>
							            <li data-thumb-alt="" style="width: 319px; margin-right: 0px; float: left; display: block;">
							                <img alt="泰国试管婴儿攻略" src="/images/mabout/l6.jpg" draggable="false" style="width: 100%!important;">
							                <p class="flex-caption">参观泰国大皇宫</p>
							            </li>
							            <li data-thumb-alt="" style="width: 319px; margin-right: 0px; float: left; display: block;">
							                <img alt="泰国试管婴儿攻略" src="/images/mabout/l7.jpg" draggable="false" style="width: 100%!important;">
							                <p class="flex-caption">医院现场</p>
							            </li>
							            <li data-thumb-alt="" style="width: 319px; margin-right: 0px; float: left; display: block;">
							                <img alt="泰国试管婴儿攻略" src="/images/mabout/l8.jpg" draggable="false" style="width: 100%!important;">
							                <p class="flex-caption">试管无忧员工参观泰国BNH医院</p>
							            </li>
							            <li data-thumb-alt="" style="width: 319px; margin-right: 0px; float: left; display: block;">
							                <img alt="泰国试管婴儿攻略" src="/images/mabout/l9.jpg" draggable="false" style="width: 100%!important;">
							                <p class="flex-caption">试管无忧员工参观胚胎移植实验室</p>
							            </li>
					                </ul>
					            </div>
					        </div>
				        </div>
		    		</div>
		  		</div>
		    </div>
		</div>
	</div>
</div>
  <script>
    // 幻灯片
    $(document).ready(function () {
      $(".flexslider_mob").flexslider({
        slideshowSpeed: 4000, //展示时间间隔ms
        animationSpeed: 100, //滚动时间ms
        touch: true, //是否支持触屏滑动
        animationLoop: true,
        directionNav: false,
        animation: "slide"
      });
    });
  </script>











































