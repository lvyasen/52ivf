<?= $this->Sgwy->addCss('/css/about/mediaMobile.css'); ?>
<?= $this->Sgwy->addCss('/css/about/flexslider.css'); ?>
<?= $this->Sgwy->addCss('/css/about/info.css'); ?>
<?= $this->Sgwy->addJs('/js/about/flexslider.min.js');?>
<?= $this->Sgwy->addJs('/js/about/info.js'); ?>
<div class="about">
	<div class="content">
		<div class="pp"><a href="/">首页</a> > <a href="">品牌故事</a></div>
		<div class="con">
			<?= $this->element('leftnav'); ?>
			<div class="main">
				<div id="wav_title">
			        <p>品牌故事</p>
			        <img class="lazy" onclick="history.go(-1)" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
                </div>
				<h2>品牌故事</h2>
				<div class="info">
					<div class="banner">
					<div class="pc_ban"><img src="/images/about/info/banner.jpg"></div>
					<div class="web_ban"><img src="/images/nabout/about01.jpg"></div>
				</div>
				<div class="wrapper">
			    	<div class="section silverbg">
			        	<div class="section-inner aboutus">
			        		<div class="section-title">
			          			<h3>品牌故事</h3>
			        		</div>
					        <div class="section-content">
						          <p>试管无忧成立于2013年，是一个专注“疑难不孕”和“疑难试管”人群的海外试管服务品牌，致力于通过精准预约等个性化服务，不断提高广大出国看病人群的就医效率和成功率。通过多年的发展，试管无忧已经成为国内最具人气的海外试管知名品牌，目前70%的客户来自试管姐妹间的口碑介绍。透明、个性化、超预期的服务特点，让试管无忧赢得了业内外的一致口碑。</p>
						          <p>目前，试管无忧合作的医院遍布泰国、美国、新加坡等国家以及港澳台等地区。所有合作的医院均采用“实地考察+一站式对接”的模式，以泰国地区为例，试管无忧是杰特林生殖研究所（JE）、全球生殖中心（WW）、帕亚泰·是拉差（PH）等医院在中国地区少有的战略合作伙伴之一。据介绍，试管无忧提倡的“五心”服务（即：省心、放心、安心、贴心、热心）受到广大试管姐妹的追捧。通过更加个性、更加精准的服务方式，提高出国看病的效率和成功率，让广大试管姐妹少走弯路，让出国看病不再难，已经成为试管无忧全体成员的共同认知和奋斗目标。</p>
					        </div>
					    </div>
			        </div>
				    <div class="section">
					    <div class="section-inner dream styqq">
					        <div class="section-title">
					          <h3>让试管无忧成为你试管就医的引路人</h3>
					        </div>
					        <div class="section-content" id="video_list" style="overflow: hidden;margin-top:17px;">
					           <div class="xbout"><a href="/video/index/"><img src="/images/nabout/a1.jpg">《泰国试管那些事儿》</a></div>
					           <div class="xbout"><a href="/video/index/"><img src="/images/nabout/a2.jpg">《高龄妈妈赴泰喜得男宝》</a></div>
					           <div class="xbout"><a href="/video/index/"><img src="/images/nabout/a3.jpg">《小忧说试管》</a></div>
					           <div class="xbout"><a href="/video/index/"><img src="/images/nabout/a4.jpg">《小忧说不孕》</a></div>
					        </div>
					    </div>
				    </div>
				    <div class="section mamashuo">
					    <div class="section-inner">
					        <div class="section-title align-center">
					          <h3>更多患者的心声</h3>
					        </div>
					        <div class="section-content" style="margin-top:17px;">
					            <div class="item-wrap clearfix">
						            <div class="item">
						                <div class="print"><img src="/images/nabout/a1.png" alt="泰国试管婴儿攻略"></div>
							            <div class="cont">
							                <div class="title">
							                  <h4>某外企高管 Lisa小姐</h4>
							                </div>
							                <div class="say">
							                  "我是一家外企公司的高管，一直忙于工作，生育问题被忽略了，35岁了才想起来要孩子，突然想生的时候，发现自己已经失去了自然生育的能力，为了提高就医效率，贻误时机，去了很多医院，花了很多精力与时间都没能怀孕，整个人都充满了疑惑、心酸与疲惫。机缘巧合之下结识了试管无忧，通过试管无忧选择了国外的医院，技术好，服务周到，终于圆了我做母亲的梦想！"
							                </div>
							                <div class="round round2"></div>
							            </div>
						            </div>
						            <div class="item trt">
							            <div class="print"><img src="/images/nabout/a2.png" alt="泰国试管婴儿攻略"></div>
							         	<div class="cont">
							                <div class="title">
							                    <h4>求医无门的 张妈妈</h4>
							                </div>
							                <div class="say">
							                    "……我因为患有卵巢过度刺激综合征，一直不能生，心里很落魄，也找不到好的解决办法，经朋友介绍认识了试管无忧，想着抱着试一试的心态。后来终于通过试管无忧匹配了一家泰国的医院，采用最先进的方案进行手术。备孕期间，试管无忧的就医顾问时常跟我取得联系，询问我目前的近况以及告知我一些注意事项与保胎只是。现在已经是两个宝宝的妈妈了，感谢试管无忧！"
							                </div>
							                <div class="round round3"></div>
							            </div>
						            </div>
					            </div>
					        </div>
					    </div>
				    </div>
			        <div class="section silverbg">
			        	<div class="section-inner goodness">
					        <div class="section-title align-center">
					            <h3>试管无忧的服务</h3>
					        </div>
			                <div class="section-content">
						        <div class="line">
						            <div class="dot dot1"></div>
						            <div class="dot dot7"></div>
						        </div>
						        <div class="items">
						            <div class="desc">
							            <div class="items-img"><img src="/images/about/yuan01.png"/ src="泰国试管婴儿攻略"></div>
						                <h3 class="title">精准预约 ¥7999</h3>
							            <div class="info">
							               结合个性情况，匹配名医、指定预约和个性导诊，真正做到一对一个性治疗，从根本上提高就医效率和成功率。
							            </div>
						            </div>
						        </div>
						        <div class="items">
						            <div class="desc">
						                <div class="items-img"><img src="/images/about/yuan02.png" alt="泰国试管婴儿攻略" /></div>
						                <h3 class="title">远程问诊 ¥699</h3>
						                <div class="info">
						                	为国内多年不孕、多次失败的患者，提供境外试管医生一对一问诊服务。分析病情，找出失败的真正原因，为后续的治疗提供最佳依据。
						                </div>
						            </div>
						        </div>
						        <div class="items">
						            <div class="desc">
						                <div class="items-img"><img src="/images/about/yuan03.png" alt="泰国试管婴儿攻略" /></div>
						                <h3 class="title">VIP尊享 ¥16800</h3>
						                <div class="info">
						                	为国内特殊的不孕不育患者，提供境外生殖医院“特殊案例”就医申请，以及第三方志愿者资源对接与翻译服务。
						                </div>
						            </div>
						        </div>
						        <div class="items">
						            <div class="desc">
						                <div class="items-img"><img src="/images/about/yuan04.png" alt="泰国试管婴儿攻略" /></div>
						                <h3 class="title">金牌无忧 ¥36000</h3>
						                <div class="info">
						                	针对部分希望有更好服务体验的患者，提供全程陪诊、星级酒店入住、就餐安排等一条龙服务。
						                </div>
						            </div>
						        </div>
			                </div>
			            </div>
			        </div>
			        <div class="section">
			            <div class="section-inner offline" style="margin-bottom: 0; padding-bottom: 0;">
					        <div class="section-title">
					           <h3>丰富的线下活动</h3>
					        </div>
					        <div class="section-content">
			      				<div id="www_qpsh_com" style="overflow:hidden;height:170px;width:100%;margin: 20px auto;">
			                        <table width="0" border="0" cellpadding="0" cellspacing="0">
			                            <tr>
			                                <td id="www_qpsh_com1">
			                                    <table width="0" border="0" cellpadding="0" cellspacing="0"> 
			                                        <tr class="trd">
			                                         	<td align="center"><img src="/images/mabout/l1.jpg" height="170" alt="泰国试管婴儿攻略"></td>  
			                                          	<td align="center"><img src="/images/mabout/l2.jpg" height="170" alt="泰国试管婴儿攻略"></td>
			                                           	<td align="center"><img src="/images/mabout/l3.jpg" height="170" alt="泰国试管婴儿攻略"></td>
			                                            <td align="center"><img src="/images/mabout/l4.jpg" height="170" alt="泰国试管婴儿攻略"></td>
			                                            <td align="center"><img src="/images/mabout/l5.jpg" height="170" alt="泰国试管婴儿攻略"></td>
			                                          	<td align="center"><img src="/images/mabout/l6.jpg" height="170" alt="泰国试管婴儿攻略"></td>
			                                           	<td align="center"><img src="/images/mabout/l7.jpg" height="170" alt="泰国试管婴儿攻略"></td>
			                                            <td align="center"><img src="/images/mabout/l8.jpg" height="170" alt="泰国试管婴儿攻略"></td>
			                                            <td align="center"><img src="/images/mabout/l9.jpg" height="170" alt="泰国试管婴儿攻略"></td>
				 									</tr>
			                                    </table>
			                                </td>
			                                <td id="www_qpsh_com2"></td>
			                            </tr>
			                        </table>
			                    </div>
					        </div>
			    		</div>
			  		</div>
		    	</div>
			</div>
		</div>
	</div>
</div>
