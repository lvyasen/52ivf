<?= $this->Sgwy->addCss('/css/contact/index.css'); ?>
<div class="contact">
	<div class="content">
		<p><a href="/">首页</a> > <a href="">联系我们</a></p>
		<div class="con">
			<?= $this->element('leftnav'); ?>
			<div class="main">
			    <h2>联系我们</h2>
				       <div id="wav_title">
        <p style="padding-right: 20px;">联系我们</p>
        <img class="lazy" onclick="history.go(-1)" src="/images/common/grey.gif" data-original="/images/mobile/fanhui.png">
        </div>
				<div>无论您是对试管无忧有宝贵意见，还是想与试无忧合作共赢；无论您是想成为妈妈，还是想加入试管无忧团队，都欢迎通过以下方式联系我们，感谢您对试管无忧的关注和关爱！</div>
				<h3>企业名称</h3>
				<p>上海韵瑶文化传播有限公司</p>
				<p>Email：bhu@ivf52.com</p>
				<hr>
				<h3>企业服务</h3>
				<p>客服热线：<span>400-003-9103</span></p>
				<p>微博：<span><a style="color: #d67b33;" href="http://weibo.com/5967833423/profile?topnav=1&wvr=6&is_hot=1">http://weibo.com/5967833423/profile?topnav=1&wvr=6&is_hot=1</a> </span></p>
				<p>微信：IVF_360（微信二维码）</p>
				<img class="lazy" src="/images/common/grey.gif" alt="" data-original="/images/common/erweima.jpg">
				<hr>
				<h3>医院/医生/医院合作</h3>
				<p>阮女士</p>
				<p>联系方式：<span>17317708720</span></p>
				<p>邮箱：ryn@ivf51.com</p>

			</div>
		</div>
	</div>
</div>