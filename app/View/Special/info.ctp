<?= $this->Sgwy->addCss('/css/special/info.css'); ?>
<?= $this->Sgwy->addJs('/js/jquery.1.8.3.js'); ?>
<?= $this->Sgwy->addJs('/js/special/info.js'); ?>
<?=stripslashes($info['Special']['content'])?>
<div class="box5">
	<h2>为您<span>免费定制</span>私人孕育方案</h2>
	<p>最近一个月<span>536</span>人在线成功定制</p>
	<form action="/special/info/<?=$uid?>"  method='post' class="yueForm">
		<input type="text" placeholder="您的姓名" name="user_name">
		<input type="text" placeholder="您的年龄" name="age">
		<input type="text" placeholder="您的手机号（限11位）" name="mobile">
		<button type="submit">确认预定</button>
	</form>
</div>
<script type="text/javascript">
	$(".yueForm").Validform({
	btnSubmit:".aidesubmit", 
	tiptype:function(msg){
	    layer.alert(msg,{icon:0});    
	},
	tipSweep:true,
	ajaxPost:true,
	callback:function(o){
	    layer.closeAll();
	    layer.alert(o.message,{icon:0});    
	}
});
</script>