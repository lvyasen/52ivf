<?= $this->Sgwy->addCss('/css/special/info.css?c=125'); ?>
<?= $this->Sgwy->addJs('/js/jquery.1.8.3.js'); ?>
<?= $this->Sgwy->addJs('/js/special/info.js?c=125'); ?>
<div class="zty6 zty">
		<div class="banner">
			<a href="https://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn "><img src="/images/special/zty6/banner.jpg" alt=""></a>
		</div>
		<div class="box6">
			<ul>
				<li><a href="/about/index/">
					<img src="/images/special/zty6/z1.png" alt="">
					<p>关于我们</p></a>
				</li>
				<li><a href="/special/info/122">
					<img src="/images/special/zty6/z2.png" alt="">
					<p>试管成功率</p></a>
				</li>
				<li><a href="/special/info/121">
					<img src="/images/special/zty6/z3.png" alt="">
					<p>试管费用</p></a>
				</li>
				<li><a href="/special/info/123">
					<img src="/images/special/zty6/z4.png" alt="">
					<p>试管流程</p></a>
				</li>
				<li><a href="/special/info/124">
					<img src="/images/special/zty6/z5.png" alt="">
					<p>试管医院</p></a>
				</li>
				<li><a href="/doctor/index/">
					<img src="/images/special/zty6/z6.png" alt="">
					<p>试管医生</p></a>
				</li>
				<li><a href="/special/info/125">
					<img src="/images/special/zty6/z7.png" alt="">
					<p>试管技术</p></a>
				</li>
				<li><a href="https://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn">
					<img src="/images/special/zty6/z8.png" alt="">
					<p>联系我们</p></a>
				</li>
			</ul>
		</div>
		<div class="box1 box">
			<em>为什么要到泰国去做试管婴儿</em>
			<p>试管婴儿成功率对比</p>
			<div class="box1_img">
				<img src="/images/special/zty6/x1.png" alt="">
				<img src="/images/special/zty6/x2.png" alt="">
				<img src="/images/special/zty6/x3.png" alt="">
			</div>
			<div class="btn">
				<a href="https://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn">点我预约泰国试管婴儿</a>
				<a href="/aide/mobile_aide/">点击测试我的成功率</a>
			</div>
		</div>
		<div class="box7 box">
			<em>泰国试管婴儿费用具体怎么收？</em>
			<p>试管无忧坚持收费公开透明</p>
			<div class="box7_circle">
				<img src="/images/special/zty6/x4bg.jpg" alt="">
				<p class="box7_cir_p1">6%-试管无忧<br>服务费用</p>
				<p class="box7_cir_p2">10%-泰国衣食<br>住行费用</p>
				<p class="box7_cir_p3">84%-泰国试管<br>婴儿医疗费用</p>
			</div>
			<div class="state">
				<p><label>*声明：</label><i >试管无忧收费符合国家统一标准，不存在隐形消费，价格公开透明，并积极配合有关部门监督审查</i></p>
				<div class="btn">
					<a href="https://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn ">泰国试管医疗费用是多少</a>
					<a href="https://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn ">试管无忧服务费是多少</a>
				</div>
			</div>
		</div>
		<div class="box8 box">
			<em>2018泰国试管婴儿医院年度总评排行榜</em>
			<a href="/special/info/124"><img src="/images/special/zty6/x5.png" alt=""></a>
			<div class="btn">
				<a href="https://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn ">泰国试管医院哪家好</a>
				<a href="https://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn ">了解更多泰国试管医院 </a>
			</div>
		</div>
		<div class="box9 box" style="margin-bottom:0px;">
			<em>想要去泰国做试管</em>
			<p>却为这些问题烦恼？</p>
			<div class="box9_cont">
				<div class="box9_cont_left"><a href="https://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn">
					<img src="/images/special/zty6/c1.jpg" alt="">
					<p><i>3</i>没有人陪伴？</p></a>
				</div>
				<ul class="box9_cont_right">
					<li><a href="https://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn">
						<img src="/images/special/zty6/c2.jpg" alt="">
						<p><i>1</i>不会泰文？</p></a>
					</li>
					<li class="box9_li_right"><a href="https://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn">
						<img src="/images/special/zty6/c3.jpg" alt="">
						<p><i>2</i>水土不服？</p></a>
					</li>
					<li><a href="https://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn">
						<img src="/images/special/zty6/c4.jpg" alt="">
						<p><i>4</i>不能出去玩？</p></a>
					</li>
					<li class="box9_li_right"><a href="https://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn">
						<img src="/images/special/zty6/c5.jpg" alt="">
						<p><i>5</i>吃不惯饭菜？</p></a>
					</li>
				</ul>
			</div>
		</div>
		<div class="box10 box">
			<em>选择试管无忧</em>
			<p>一切问题都不是问题</p>
			<ul>
				<li><a href="https://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn"><img src="/images/special/zty6/v1.jpg" alt=""></a></li>
				<li><a href="https://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn"><img src="/images/special/zty6/v2.jpg" alt=""></a></li>
				<li><a href="https://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn"><img src="/images/special/zty6/v3.jpg" alt=""></a></li>
				<li><a href="https://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn"><img src="/images/special/zty6/v4.jpg" alt=""></a></li>
			</ul>
		</div>
		<div class="box2 box">
			<em>我们的服务流程</em>
			<ul>
				<li>
					<img src="/images/special/zty6/b1.png" alt="">
					<p>客户来访<br>在线咨询</p>
				</li>
				<li class="jian">
					<img src="/images/special/zty6/right.png" alt="">
				</li>
				<li>
					<img src="/images/special/zty6/b2.png" alt="">
					<p>导医接待<br>建立档案</p>
				</li>
				<li class="jian">
					<img src="/images/special/zty6/right.png" alt="">
				</li>
				<li>
					<img src="/images/special/zty6/b3.png" alt="">
					<p>医疗跟进<br>初步了解</p>
				</li>
				<li class="jian">
					<img src="/images/special/zty6/right.png" alt="">
				</li>
				<li>
					<img src="/images/special/zty6/b4.png" alt="">
					<p>建立档案<br>确定项目</p>
				</li>
                <div class="bottom"></div>
				<li>
					<img src="/images/special/zty6/b5.png" alt="">
					<p>成功受孕<br>跟踪指导</p>
				</li>
				<li class="jian">
					<img src="/images/special/zty6/left.png" alt="">
				</li>
				<li>
					<img src="/images/special/zty6/b6.png" alt="">
					<p>鉴定协议<br>选择服务</p>
				</li>
				<li class="jian">
					<img src="/images/special/zty6/left.png" alt="">
				</li>
				<li>
					<img src="/images/special/zty6/b7.png" alt="">
					<p>沟通事宜<br>商讨方案</p>
				</li>
				<li class="jian">
					<img src="/images/special/zty6/left.png" alt="">
				</li>
				<li>
					<img src="/images/special/zty6/b8.png" alt="">
					<p>挂号检查<br>引见医生</p>
				</li>
			</ul>
			<div class="tab1">
				<a href="https://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn">点击获取我的个人专属方案</a>
			</div>
		</div>
		<div class="box3 box">
			<em>泰国试管婴儿真实好孕日记</em>
			<p>看他们的评价是否对您有帮助？</p>
			<div class="box3_list">
				<ul>
					<li class="box3_li">
						<div class="b3_li_top">
							<img src="/image/zty1/r1.png" alt="">
							<div>
								<p><i>赵**</i> <strong>35岁</strong><em>某高管企业</em></p>
								<label>术前情况：</label>二胎要男孩&nbsp;<label>好孕医院：</label>全球生殖中心<br>
								<label>选择技术：</label>泰国第三代试管婴儿 &nbsp;<label>好孕专家：</label>提迪贡博士
							</div>
						</div>
						<div class="b3_li_bot">
							<p>这次泰国之旅非常感谢试管无忧，翻译很专业、很贴心带给我们很多帮助，最重要的时还认识了提迪贡博士，帮我们实现了多年的心愿，真的是太感谢了
							</p>
							<div class="b3_img">
								<img src="/image/zty1/y1.jpg" alt="">
								<img src="/image/zty1/y2.jpg" alt="">
								<img src="/image/zty1/y3.jpg" alt="">
							</div>
						</div>
					</li>
					<li class="box3_li">
						<div class="b3_li_top">
							<img src="/image/zty1/r2.png" alt="">
							<div>
								<p><i>李**</i> <strong>41岁</strong><em>自由职业者</em></p>
								<label>术前情况：</label>国内试管多次失败 <label>好孕医院：</label>杰特宁<br>
								<label>选择技术：</label>泰国第三代试管婴儿   <label>好孕专家：</label>YOKO
							</div>
						</div>
						<div class="b3_li_bot">
							<p>今年8月在网站找到的试管无忧，开始挺纠结的，也对比过好多家，去了他们办公室，和客户接触过后抱着尝试的心情定制了金牌无忧，因为个人原因到10月份才去了泰国，翻译小妹妹很敬业，辛苦她了，除了医疗生活上也帮助了我们很多，感谢感谢！
							</p>
							<div class="b3_img">
								<img src="/image/zty1/y4.jpg" alt="">
								<img src="/image/zty1/y5.jpg" alt="">
								<img src="/image/zty1/y6.jpg" alt="">
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="tab1">
				<a href="https://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn">立即咨询</a>
			</div>
		</div>
		<div class="box4 box" style="margin-bottom: 0;">
			<em>优惠还能享受国际高品质服务</em>
			<p>选择试管无忧，省钱！省心！超值！</p>
			<div class="tab2">
				<div class="tab2_btn">
					<em class="add2">精准</em>
					<em>贴心</em>
					<em>专业</em>
					<em>省钱</em>
				</div>
				<div class="tab2_box">
					<div style="display:block;"><img src="/image/zty1/u1.jpg" alt=""> </div>
					<div><img src="/image/zty1/u2.jpg" alt=""></div>
					<div><img src="/image/zty1/u3.jpg" alt=""></div>
					<div><img src="/image/zty1/u4.jpg" alt=""></div>
				</div>
			</div>
		</div>
		<div class="box5">
			<h2>为您<span>免费定制</span>私人孕育方案</h2>
			<p>最近一个月<span>536</span>人在线成功定制</p>
			<form action="">
				<input type="text" placeholder="您的姓名">
				<input type="text" placeholder="您的年龄">
				<input type="text" placeholder="您的手机号（限11位）">
				<button type="submit">确认预定</button>
			</form>
		</div>

	</div>