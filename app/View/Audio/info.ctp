<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<!--让网页的宽度自动适应手机屏幕的宽度。 其中： width=device-width ：表示宽度是设备屏幕的宽度 initial-scale=1.0：表示初始的缩放比例 minimum-scale=0.5：表示最小的缩放比例 maximum-scale=2.0：表示最大的缩放比例 user-scalable=yes：表示用户是否可以调整缩放比例-->
<title><?=$keysInfo['Keyword']['meta_title'];?></title>
<meta name="keywords" content="<?=$keysInfo['Keyword']['meta_keywords'];?>" />
<meta name="description" content="<?=$keysInfo['Keyword']['meta_description'];?>" />
<meta name="format-detection" content="telephone=no" /><!--防止在iOS设备中的Safari将数字识别为电话号码-->
<link rel="stylesheet" type="text/css" href="/css/common.css">
<link rel="stylesheet" type="text/css" href="/css/glyphicons.css">
<link rel="stylesheet" type="text/css" href="/css/audio/info.css">
<link rel="stylesheet" type="text/css" href="/css/jquery.mCustomScrollbar.css">
<script type="text/javascript" src="/js/jquery.1.8.3.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/jquery.lazyload.js"></script>
<script type="text/javascript" src="/admin/js/Validform/Validform.min.js"></script>
<script type="text/javascript" src="/js/jquery.mCustomScrollbar.concat.min.js"></script>   
</head>
<body>
<div class="audio_info_c">
	<div class="top">
		<div class="mainn"><a href="/audio/index/">返回列表</a></div>
	</div>
	<div class="content">
		<div class="dw">
			<div class="contt" id="contt">
				<div class="title">
					<a href="/">首页</a><span>></span><a href="/audio/index/">胖妈妈之声</a><span>></span><?=$album['Album']['name'];?>			</div>
				<div class="audio_left" class="left_btn">
				<div class="bg_img">
					<img src="<?=$album['Album']['img'];?>" class="img rotation" id="img_btn" style="animation-play-state:paused;" />
					<div class="audio_dw">
						<div class="chou"><img src="/images/audio/1.png"></div>
						<div class="audio_pn">
							<img src="/images/audio/4.png"/>
						</div>
					</div>
				</div>
				<div class="seconrd">
					<div class="player1" id="player" style="background-image: url(/images/audio/002.png);" mp="0"></div>	
					<div  class="left_btn left_time">0.0</div>
				  		<div class="audio_time_b">
							<div class="dn circle_motion circle_pauser">
								<div class="zhou"></div>
							 	<audio class="audio_btn">
										<source src="<?=$audio['0']['Audio']['src']?>" type="audio/mpeg">
								</audio>
							</div>
						</div>
					<div class="right_btn right_time">0.0</div>
					<div class="player" id="player" style="background-image: url(/images/audio/002.png); "></div>	
					</div>
					<div class="play_title text_btn"><?=$audio['0']['Audio']['title']?></div>
				</div>
				<div class="audio_right right_btn">
					<div class="play_zhuanji"><?=$album['Album']['name'];?></div>
					<div class="name_tou">CoCo</div>
					<div class="play_text"><?=$album['Album']['description']?></div>
					<div class="list_title">节目列表（共<?php echo count($audio);?>集）</div>
					<div class="audio_list nano" id="about">
						<ul class="mCustomScrollbar" data-mcs-theme="dark">
						<!--循环-->
							<?php foreach ($audio as $key => $value):?> 
							<?php $len = explode(':',$value['Audio']['length']) ?>
							<li>
								<div class="li_title">
									<?=$value['Audio']['title']?><br>
									<span>播放量：<?=$value['Audio']['play_count']?></span><span>总时长：<?=$len[1]."'".$len[2].'"';?></span>
								</div>
								<div class="li_btn">
									<img src="/images/audio/003.png"  source="<?=$value['Audio']['src']?>" play_btn="0"  class="list_btn btn" alt="<?=$value['Audio']['title']?>" data-hour='<?= $len[0] ?>' data-minute='<?= $len[1] ?>' data-second='<?= $len[2] ?>' mp="<?=$key?>">
								</div>
							</li>
							<?php endforeach;?>				 
							
						<!--结束-->
						</ul>
					</div>
					<a class="audio_more" href="/audio/index/">查看更多</a>
				</div>
			</div>
		</div>
		<div class="pic" style="background-image:url(<?=$album['Album']['img'];?>); "><img src="/images/audio/bg_01.png"></div>
	</div>
</div>
<div class="audio_info_n">
	<div class="top">
		<div class="mainn"><a href="/audio/index/">返回列表</a></div>
	</div>
	<div class="content">
		<div class="dw">
			<div class="contt" id="contt">
				<div class="audio_left" class="left_btn">
				<div class="bg_img">
					<img src="<?=$album['Album']['img'];?>" class="img rotation" id="img_btn" style="animation-play-state:paused;"/>
					<div class="audio_dw">
						<div class="chou"><img src="/images/audio/1.png"></div>
						<div class="audio_pn">
							<img src="/images/audio/4.png"/>
						</div>
					</div>
				</div>
				<div class="play_title text_btn"><?=$audio['0']['Audio']['title']?></div>
				<div class="seconrd">
					<div class="left_btn left_time">0.0&nbsp;</div>
				  		<div class="audio_time_b">
							<div class="dn circle_motion circle_pauser">
								<div class="zhou"></div>
							 	<audio class="audio_btn">
									<source src="<?=$audio['0']['Audio']['src']?>" type="audio/mpeg">
								</audio>
							</div>
						</div>
					<div class="right_btn right_time">0.0</div>
				</div>
				<div class="player" id="player" style="background-image: url(/images/audio/002.png); " mm="0"></div>	
				</div>
				<div class="audio_right right_btn">
					<div class="play_zhuanji"><?=$album['Album']['name'];?></div>
					<div class="list_title">节目列表（共<?php echo count($audio);?>集）</div>
					<div class="audio_list nano" id="about">
						<ul class="mCustomScrollbar" data-mcs-theme="dark">
						<!--循环-->
							<?php foreach ($audio as $key => $value):?> 
							<?php $len = explode(':',$value['Audio']['length']) ?> 
							<li>
								<div class="li_title">
									<?=$value['Audio']['title']?><span>播放量：<?=$value['Audio']['play_count']?>&nbsp;&nbsp;&nbsp;总时长：<?=$len[1]."'".$len[2].'"';?></span>
								</div>
								<div class="li_btn">
									<img src="/images/audio/003.png"  source="<?=$value['Audio']['src']?>" play_btn="0"  class="btn o_btn"  alt="<?=$value['Audio']['title']?>" data-hour='<?= $len[0] ?>' data-minute='<?= $len[1] ?>' data-second='<?= $len[2] ?>' mm="<?=$key?>">
								</div>
							</li>
							<?php endforeach;?>		
							
						<!--结束-->
						</ul>
					</div>
					<a class="audio_more" href="/audio/index/">查看更多</a>
				</div>
			</div>
		</div>
		<div class="pic" style="background-image:url(<?=$album['Album']['img'];?>); "><img src="/images/audio/bg_01.png"></div>
	</div>
</div>
</div>
</body>
<script type="text/javascript" src="/js/audio/info.js"></script>
</html>