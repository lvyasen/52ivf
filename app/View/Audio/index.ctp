<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<!--让网页的宽度自动适应手机屏幕的宽度。 其中： width=device-width ：表示宽度是设备屏幕的宽度 initial-scale=1.0：表示初始的缩放比例 minimum-scale=0.5：表示最小的缩放比例 maximum-scale=2.0：表示最大的缩放比例 user-scalable=yes：表示用户是否可以调整缩放比例-->
<title><?php echo isset($keysInfo['Keyword']['meta_title'])?$keysInfo['Keyword']['meta_title']:'试管无忧';?></title>
<meta name="keywords" content="<?php echo isset($keysInfo['Keyword']['meta_keywords'])?$keysInfo['Keyword']['meta_keywords']:'试管无忧';?>" />
<meta name="description" content="<?php echo isset($keysInfo['Keyword']['meta_description'])?$keysInfo['Keyword']['meta_description']:'试管无忧';?>" />
<meta name="format-detection" content="telephone=no" /><!--防止在iOS设备中的Safari将数字识别为电话号码-->
<link rel="stylesheet" type="text/css" href="/css/common.css">
<link rel="stylesheet" type="text/css" href="/css/glyphicons.css">
<link rel="stylesheet" type="text/css" href="/css/audio/index.css">
<link rel="stylesheet" type="text/css" href="/css/jquery.mCustomScrollbar.css">
<script type="text/javascript" src="/js/jquery.1.8.3.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<script type="text/javascript" src="/js/jquery.lazyload.js"></script> 
<script type="text/javascript" src="/js/jquery.mCustomScrollbar.concat.min.js"></script>   
</head>
<body>
<div class="audio1">
	<div class="top">
		<div class="mainn"><a href="/community/index/">返回社区首页</a></div>
	</div>
	<div class="banner" bg_btn="1"></div>
	<div class="audio_main">
		<div class="audio_cont">
			<div style="position: relative; width: 100%; height: 0;">
				<div class="xf">
				 	<div class="divv1">
				 		<img src="/images/audio/erweima.jpg">
				 		<p>关注公众号 将故事装进口袋</p>
				 	</div>
				 	<div class="divv3"><div class="cha"><img src="/images/audio/off.png"></div></div>
				 	<div class="divv2"><p>官方微博</p><a href="http://weibo.com/5967833423/profile?topnav=1&wvr=6" target="_blank"><img src="/images/audio/weibo.png" border="0"></a></div>
		 		</div>
			</div>
			<?php foreach ($dataList as $k => $val):?>
			<div class="title"><?php echo $cateList[$k];?></div>
			<div class="box_box">
			<!--循环--> 
				<?php foreach ($dataList[$k] as $key => $value):?>
				<div class="bt_box">
					<div class="img">
						<img src="<?=$value['img']?>" alt="<?=$value['name']?>" >
					</div>
					<div class="htbox">
						<a href="/audio/info/<?=$value['id']?>.html">
						<div class="ytbox">
							<div class="huiying"></div>
							<div class="type">
								<div class="text">
									<?=$value['name']?>
								</div>
							</div>
						</div>
						</a>
						<div class="ttbox">
						<span><a href="/audio/info/<?=$value['id']?>.html"><?=$value['name']?></a></span>
						<?=$value['description']?>	
					</div>
					</div>
				</div> 
				<?php endforeach;?>
			</div>
			<!--结束-->
			<?php endforeach;?>
		</div>
	</div>
	<div class="vbottom">“试管无忧，专注疑难不孕”！确保每一份患者的试管就医&nbsp;&nbsp;沪ICP备14046762号</div>
</div>
<div class="audio2">
	<div class="top">
		<div class="mainn"><a href="/community/index/">返回社区首页</a></div>
	</div>
	<div class="webban"><img src="/images/audio/banner.png"></div>
	<div class="main"> 
		<?php foreach ($dataList as $k => $val):?>
		<div class="titlee"><?php echo $cateList[$k];?></div>
		<?php foreach ($dataList[$k] as $key => $value):?>
			<a href="/audio/info/<?=$value['id']?>.html">
			<div class="audio_box">
				<div class="box_left">
					<img src="<?=$value['img']?>" alt="<?=$value['name']?>" >
				</div>
				<div class="box_right">
					<div class="title_name"><?=$value['name']?></div>
					<div class="cont"><?=$value['description']?></div>
				</div>
			</div>
			</a> 
		<?php endforeach;?>
		<div class="di"></div> 
		<?php endforeach;?>
	</div>
	<div class="vbottom">“试管无忧，专注疑难不孕”！&nbsp;&nbsp;沪ICP备14046762号</div>
</div>
<script type="text/javascript">
$(".cha").click(function(){
    $(".xf").hide();
});
</script>
<script type="text/javascript">
	 $(window).scroll(function(e){
		if($(document).scrollTop()<= 500){
			$(".xf").css({"position":"absolute", "top": "70px","left":"960px","margin-left": "0" });
		}
		else if($(document).scrollTop()> 500){
			$(".xf").css({"position": "fixed", "top":"0px","left": "50%","margin-left": "367px"});
		}
	});
</script>	
</body>
</html>