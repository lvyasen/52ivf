<?= $this->Sgwy->addCss('/css/service/index.css'); ?>
<?= $this->Sgwy->addCss('/css/idangerous.swiper.css'); ?>
<?= $this->Sgwy->addJs('/js/service/index.js'); ?>
<?= $this->Html->script('/js/idangerous.swiper.js'); ?>
<div class="gold">
	<div class="gold1">
		<div class="ser_si">
			<div class="ban"><a href="http://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn" target="_blank"></a></div>
			<div class="ione">
				<div class="title1">全方位尊享体验，一键解决跨境衣食住行所有问题</div>
				<div class="cont">
					<div class="abox"><img src="/images/service/e1.png" alt="试管无忧 泰国试管婴儿攻略"><br><span>医</span>找对医院，找准医生，定制合理的医疗方案，享受VIP尊享所有服务</div>
					<div class="abox"><img src="/images/service/e2.png" alt="试管无忧 泰国试管婴儿攻略"><br><span>食</span>28天免费营养定制餐，贴心保姆服务</div>
					<div class="abox"><img src="/images/service/e3.png" alt="试管无忧 泰国试管婴儿攻略"><br><span>住</span>28天星级酒店入住，移植后免费洗衣服务等</div>
					<div class="abox"><img src="/images/service/e4.png" alt="试管无忧 泰国试管婴儿攻略"><br><span>行</span>专业陪诊翻译，外出逛吃陪同等服务，让异国语言无障碍</div>
				</div>
			</div>
			<div class="itwo">
				<div class="ifour">
					<div class="pic1"><span>国内试管多次，反复失败，就医体验糟糕？</span><br>生育的过程漫长，环节繁复，人们遭受精神和经济的双重压力，试管患者<br>都想得到更好的就医体验，贵宾式的就医服务，泰国试管就能够满足。</div>
				</div>
			</div>
			<div class="ithree">
				<div class="ifive">
					<div class="pic2"><span>找对医院、合适的医生，不是难事！</span><br>名医匹配、个性导诊、专业翻译、定制医疗方案等。6年时间里，我们积累<br>了数千病例， 用我们的服务帮助更多患者高效就医。</div>
				</div>
			</div>
			<div class="itwo">
				<div class="isix">
					<div class="pic3"><span>出国看病，困难重重？想要更好的服务，出国不操心？试管无忧帮你实现！</span><br>最佳的出行规划、国外专业的陪诊翻译、28天吃住无忧、专职保姆服务等，手术前的<br>准备 工作，手术过程中的陪诊服务，手术后的保胎知识，试管无忧都将提供最贴心的方案，为您保驾护航。</div>
				</div>
			</div>
			<div class="ithree">
				<div class="iseven">
					<div class="pic4"><span>疑难病症、特殊需求？</span><br>根据不同情况和需求，提出最佳解决方案。<br>聚焦高龄、特殊人群，帮助寻求合理解决方案，让您早日实现梦想！</div>
				</div>
			</div>
			<div class="new_ser_one" style="background: #faf9f9;">
			    <div class="new_ser_one_01">金牌无忧助孕套餐</div>
			    <div class="new_ser_one_02">Gold medal is not vexed</div>
			    <div class="new_ser_one_03"><img class="lazy" src="/images/common/grey.gif" data-original="/images/service/sekuai.png" alt="试管无忧 泰国试管婴儿攻略"></div>
			    <div class="new_ser_one_04">精准、高效、专业、聚焦、贴心</div>
			    <div class="new_ser_one_05">
			        <div class="goodness">
			            <div class="inner-min">
			                <div class="section-content">
			                    <div class="nav-arrow">
			                        <a class="arrow prev swiper-prev"><i></i></a>
			                        <a class="arrow next swiper-next"><i></i></a>
			                    </div>
			                    <div id="Goodness" class="slide swiper-container" style="height: 290px;">
			                        <ul class="swiper-wrapper">
			                            <li class="swiper-slide">
			                                <img src="/images/service/jin/j1.jpg" alt="试管无忧 泰国试管婴儿攻略">
			                                <div class="desc">泰国期间28天四星级<br>酒店或公寓住宿服务</div>
			                            </li>
			                            <li class="swiper-slide">
			                                <img src="/images/service/jin/j2.jpg" alt="试管无忧 泰国试管婴儿攻略">
			                                <div class="desc">泰国期间28天中、<br>晚营养膳食</div>
			                            </li>
			                            <li class="swiper-slide">
			                                <img src="/images/service/jin/j3.jpg" alt="试管无忧 泰国试管婴儿攻略">
			                                <div class="desc">泰国曼谷机场专车往<br>返接送，夫妻双方各两次</div>
			                            </li>
			                            <li class="swiper-slide">
			                                <img src="/images/service/jin/j4.jpg" alt="试管无忧 泰国试管婴儿攻略">
			                                <div class="desc">资深医务翻译1-2次<br>陪同出行逛街</div>
			                            </li>
			                            <li class="swiper-slide">
			                                <img src="/images/service/jin/j5.jpg" alt="试管无忧 泰国试管婴儿攻略">
			                                <div class="desc">移植后免费<br>洗衣服务</div>
			                            </li>
			                            <li class="swiper-slide">
			                                <img src="/images/service/jin/j6.jpg" alt="试管无忧 泰国试管婴儿攻略">
			                                <div class="desc">护士免费上<br>门打促排针</div>
			                            </li>
			                             <li class="swiper-slide">
			                                <img src="/images/service/jin/j7.jpg" alt="试管无忧 泰国试管婴儿攻略">
			                                <div class="desc">诊疗期间，医务翻译<br>全程陪同接送</div>
			                            </li>
			                             <li class="swiper-slide">
			                                <img src="/images/service/jin/j8.jpg" alt="试管无忧 泰国试管婴儿攻略">
			                                <div class="desc">赠送泰国本地电话卡<br>两张，方便与家人保持联系</div>
			                            </li>
			                            <li class="swiper-slide">
			                                <img src="/images/service/jin/j9.jpg" alt="试管无忧 泰国试管婴儿攻略">
			                                <div class="desc">资深医务翻译全程一对一<br>服务，与医生轻松交流</div>
			                            </li>
			                            <li class="swiper-slide">
			                                <img src="/images/service/jin/j10.jpg" alt="试管无忧 泰国试管婴儿攻略">
			                                <div class="desc">定制《就医方案》、《出行宝典》<br>及《行程规划》各一份</div>
			                            </li>
			                        </ul>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
    		<div class="ieight"><a rel="nofollow" href="http://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn" target="_blank" ></a></div>
		</div>
	</div>
	<div class="gold2">
	<div class="wav_title">
        <p>金牌无忧</p>
         <img  onclick="window.location.href='/';" src="/images/mobile/fanhui.png"><span>首页</span>
    </div>
        <div class="wap_banner">
            <a href="http://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn"><img class='banner_img' src="/images/service/banner03.png" width="100%" alt="试管无忧 泰国试管婴儿攻略"></a>
        </div>
        <div class="ser_si">
        	<div class="fbox_o">
        		<div class="btitle">全方位尊享体验</div>
        		<div class="cont">
        			<div class="conbox"><img src="/images/service/Z1.png" alt="试管无忧 泰国试管婴儿攻略"><span>医</span><br>定制合理的医疗方案</div>
        			<div class="conbox"><img src="/images/service/Z2.png" alt="试管无忧 泰国试管婴儿攻略"><span>食</span><br>免费定制营养餐</div>
        			<div class="conbox"><img src="/images/service/Z3.png" alt="试管无忧 泰国试管婴儿攻略"><span>住</span><br>入住星级酒店</div>
        			<div class="conbox"><img src="/images/service/Z4.png" alt="试管无忧 泰国试管婴儿攻略"><span>行</span><br>专业翻译全程陪同</div>
        		</div>
        	</div>
        	<div class="fbox_t">
        		<div class="lbtitle">国内试管多次，反复失败，就医体验糟糕？</div>
        		<div class="lstitle">生育的过程漫长，环节繁复，人们遭受精神和经济的双重压力，试管患者都想得到更好的就医体验，贵宾式的就医服务，
泰国试管就能够满足</div>
        		<div class="fbox_pic fbox_pim"><img src="/images/service/c1.png"  alt="试管无忧 泰国试管婴儿攻略" style="margin:10px auto;"></div>
        	</div>
        	<div class="fbox_th">
        		<div class="rbtitle">找对医院、合适的医生，不是难事！</div>
        		<div class="rstitle">生育的过程漫长，环节繁复，人们遭受精神和经济的双重压力试管患者都想得到更好的就医体验，贵宾式的就医服务泰国试管就能够满足</div>
        		<div class="fbox_pic"><img src="/images/service/c2.png" alt="试管无忧 泰国试管婴儿攻略" style="margin-top: 20px;  width: 100%;"></div>
        	</div>
        	<div class="fbox_t">
        		<div class="lbtitle">出国看病，困难重重？想要更好的服务，出国不操心？试管无忧帮你实现</div>
        		<div class="lstitle">最佳的出行规划、国外专业的陪诊翻译、28天吃住无忧、专职保姆服务等，手术前的准备 工作，手术过程中的陪诊服务，手术后的保胎知识，试管无忧都将提供最贴心的方案，为 您保驾护航</div>
        		<div class="fbox_pic"><img src="/images/service/c3.png" alt="试管无忧 泰国试管婴儿攻略" style="margin-top: 15px; width: 80%;"></div>
        	</div>
        	<div class="fbox_th">
        		<div class="rbtitle">疑难病症、特殊需求？</div>
        		<div class="rstitle">根据不同情况和需求，提出最佳解决方案。聚焦高龄特殊人群，帮助寻求合理解决方案让您早日实现梦想</div>
        		<div class="fbox_pic fbox_pim"><img src="/images/service/c4.png" alt="试管无忧 泰国试管婴儿攻略" style="margin-top: 15px; width: 70%; margin-bottom:10px;"></div>
        	</div>
            <div class="guodu"></div>
<div class="new_ser_one">
    <div class="one_title">金牌无忧助孕套餐</div>
    <div class="one_content">
        <div class="boodness">
            <div class="inner-min">
                <div class="section-content">
                    <div class="nav-arrow">
                        <a class="arrow prev swiper-prev"><i></i></a>
                        <a class="arrow next swiper-next"><i></i></a>
                    </div>
                    <div id="Boodness" class="slide swiper-container" style="height: 240px;">
                        <ul class="swiper-wrapper">
                            <li class="swiper-slide">
                                <img src="/images/service/jin/j1.jpg" alt="试管无忧 泰国试管婴儿攻略">
                                <ul><li>泰国期间28天四星级酒店或公寓住宿服务</li></ul>
                            </li>
                            <li class="swiper-slide">
                                <img src="/images/service/jin/j2.jpg" alt="试管无忧 泰国试管婴儿攻略">
                                <ul><li>泰国期间28天中、晚营养膳食</li></ul>
                            </li>
                            <li class="swiper-slide">
                                <img src="/images/service/jin/j3.jpg" alt="试管无忧 泰国试管婴儿攻略">
                                <ul><li>泰国曼谷机场专车往返接送，夫妻双方各两次</li></ul>
                            </li>
                            <li class="swiper-slide">
                                <img src="/images/service/jin/j4.jpg" alt="试管无忧 泰国试管婴儿攻略">
                                <ul><li>资深医务翻译1-2次陪同出行逛街</li></ul>
                            </li>
                            <li class="swiper-slide">
                                <img src="/images/service/jin/j5.jpg" alt="试管无忧 泰国试管婴儿攻略">
                                <ul><li>移植后免费洗衣服务</li></ul>
                            </li>
                            <li class="swiper-slide">
                                <img src="/images/service/jin/j6.jpg" alt="试管无忧 泰国试管婴儿攻略">
                                <ul><li>护士免费上门打促排针</li></ul>
                            </li>
                             <li class="swiper-slide">
                                <img src="/images/service/jin/j7.jpg" alt="试管无忧 泰国试管婴儿攻略">
                                <ul><li>诊疗期间，医务翻译全程陪同接送</li></ul>
                            </li>
                             <li class="swiper-slide">
                                <img src="/images/service/jin/j8.jpg" alt="试管无忧 泰国试管婴儿攻略">
                                <ul><li>赠送泰国本地电话卡两张，方便与家人保持联系</li></ul>
                            </li>
                            <li class="swiper-slide">
                                <img src="/images/service/jin/j9.jpg" alt="试管无忧 泰国试管婴儿攻略">
                                <ul><li>资深医务翻译全程一对一服务，与医生轻松交流</li></ul>
                            </li>
                            <li class="swiper-slide">
                                <img src="/images/service/jin/j10.jpg" alt="试管无忧 泰国试管婴儿攻略">
                                <ul><li>定制《就医方案》、《出行宝典》及《行程规划》各一份</li></ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        </div>
    </div>
</div>
	</div>
</div>