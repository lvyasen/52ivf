<?= $this->Sgwy->addCss('/css/service/index.css'); ?>
<?= $this->Sgwy->addCss('/css/jquery.mCustomScrollbar.css'); ?>
<?= $this->Sgwy->addJs('/js/jquery.mCustomScrollbar.concat.min.js'); ?>
<?= $this->Sgwy->addJs('/js/service/remove.js'); ?>
<div class="remote">
    <div class="wav_title">
        <p>远程咨询</p>
        <img  onclick="window.location.href='/';" src="/images/mobile/fanhui.png"><span>首页</span>
    </div>
    <div class="banner_pc"><a href="#nowOrder"></a></div>
    <div class="banner_ph">
        <a href="#nowOrder1"><img src="/images/common/grey.gif" data-original="/images/service/yuancheng_banner.jpg" alt="" class="lazy"></a>
    </div>
    <div class="why">
        <h2>为什么咨询海外医生？</h2>
        <div class="henghong"></div>
        <!-- <p>疑难不孕，我们需要更多专业建议，少走弯路</p> -->
        <ul>
            <li>
                <img src="/images/common/grey.gif" data-original="/images/service/t11.png" alt="泰国试管婴儿攻略 试管无忧" class="lazy" />
                <p class="pc_why">帮助病情复杂的患者明确诊断，<br />获取第二诊疗意见</p>
                <p class="ph_why">国内顶尖医疗 <br/>资源短缺</p>
            </li>
            <li>
                <img src="/images/common/grey.gif" data-original="/images/service/t22.png" alt="泰国试管婴儿攻略 试管无忧" class="lazy"/>
                <p class="pc_why">无需出国，即可预约国外顶级<br />医院专家会诊,降低成本</p>
                <p class="ph_why">国内医院 <br/>人满为患</p>
            </li>
            <li>
            <img src="/images/common/grey.gif" data-original="/images/service/t33.png" alt="泰国试管婴儿攻略 试管无忧" class="lazy"/>
                <p class="pc_why">获取新药，多学科联合诊疗方案，<br />最新临床试验等信息</p>
                <p class="ph_why">病情复杂<br />诊断难以明确</p>
            </li>
            <li>
            <img src="/images/common/grey.gif" data-original="/images/service/t44.png" alt="泰国试管婴儿攻略 试管无忧" class="lazy"/>
                <p class="pc_why">需要医生予以生活，用药或康复<br />指导</p>
                <p class="ph_why">想出国就诊<br />但有顾虑</p>
            </li>
        </ul>
    </div>
    <div class="person">
        <h2>远程咨询服务适合哪些人群？</h2>
        <div class="henghong"></div>
        <ul>
            <li>
                <img src="/images/service/p01.png" alt="">
                <p>多次试管失败</p>
            </li>
            <li>
                <img src="/images/service/p02.png" alt="">
                <p>高龄备孕</p>
            </li>
            <li>
                <img src="/images/service/p03.png" alt="">
                <p>疑难不孕</p>
            </li>
            <li>
                <img src="/images/service/p04.png" alt="">
                <p>想生儿子</p>
            </li>
            <li>
                <img src="/images/service/p07.png" alt="">
                <p>基因筛查</p>
            </li>
            <li>
                <img src="/images/service/p05.png" alt="">
                <p>同性与HIV备孕</p>
            </li>
            <li>
                <img src="/images/service/p06.png" alt="">
                <p>失独人群</p>
            </li>
            
        </ul>
    </div>
    <div class="suggest">
        <h2>远程咨询，获取第二医学建议</h2>
        <div class="henghong"></div>
        <p>跨越地域、时间限制，帮您获得国际医生的意见。您可以通过试管无忧海外医生<br />获得第二医学院建议。根据不同的情况，需要4-15个工作日拿到最终报告。</p>
        <div class="lump"><a href="#nowOrder">
            <div class="lump-left">
                <img src="/images/common/grey.gif" data-original="/images/service/diannao.png" alt="泰国试管婴儿攻略 试管无忧" class="lazy"/>
                <img src="/images/common/grey.gif" data-original="/images/service/z_1.jpg" alt="泰国试管婴儿攻略 试管无忧" class="lazy"/>
                <p>视频咨询</p>
                <span class="lump_pc_text">利用现代化的通讯设备，让患者与泰国的医生教授<br />或全科医生面对面、无语言障碍地视频沟通咨询，<br />获取专业可靠的第二诊疗意见。</span>
                <span class="lump_ph_text">现代化的通讯设备 <br/> 与医生面对面沟通<br/>获取第二诊疗建议</span>
            </div>
            <div class="lump-right">
                <img src="/images/common/grey.gif" data-original="/images/service/wenzi.png" alt="试管无忧 泰国试管婴儿攻略"class="lazy" />
                <img src="/images/common/grey.gif" data-original="/images/service/z_2.jpg" alt="试管无忧 泰国试管婴儿攻略" class="lazy" />
                <p>文字咨询</p>
                <span class="lump_pc_text">
                    将客户的病例资料和需要咨询的问题整理并翻译<br />为泰文，以文字方式提交给泰国医生，泰国医生提供<br />第二诊疗意见报告
                </span>   
                <span class="lump_ph_text">将病例资料翻译成泰文<br/>书面转交给泰国医生<br/>提供第二诊疗建议</span>            
            </div></a>
        </div>
    </div>
    <div class="team">
        <h2>海外试管医生团</h2>
        <div class="henghong"></div>
        <p>帮助每一位试管患者，根据患者的病情<br>找到东南亚最好的医院和医生</p>
        <ul>
            <li>
                <a href="/doctor/info/6.html" rel="nofollow"><img src="/images/common/grey.gif" data-original="/images/service/y_1.png" alt="试管无忧 泰国试管婴儿攻略" class="lazy" /></a>
                <!-- <h4>钟杰医生</h4> -->
                <p>Dr.Jongjate</p>
                <span>钟杰医生是杰特宁医院院长。中国甚至国外很多人慕名来指定找他做试管婴儿移植，他曾通过试管技术给泰国皇室培育出一位泰国皇孙。</span>
            </li> 
            <li>
                <a href="/doctor/info/17.html" rel="nofollow"><img src="/images/common/grey.gif" data-original="/images/service/y_2.png" alt="试管无忧 泰国试管婴儿攻略" class="lazy" /></a>
                <p>Di.Viwat</p>
                <span>维瓦医生是BNH医院试管婴儿医生。专长不孕不育、子宫内膜异位、内窥镜手术-盆腔镜及宫腔镜、子宫肌瘤切除手术。2002年于澳洲悉尼ivf中心接受培训和学习。</span>
            </li> 
            <li>
                <a href="/doctor/info/15.html" rel="nofollow"><img src="/images/common/grey.gif" data-original="/images/service/y_3.png" alt="试管无忧 泰国试管婴儿攻略" class="lazy" /></a>
                <p>Thitkorn</p>
                <span>提迪贡医生主要从事不孕不育、子宫内膜异位、女子妇科疾病等病症的治疗，是泰国著名的试管婴儿医生，在从医的二十多年间平均每年进行人工授精和试管婴儿手术高达1100多例。</span>               
            </li> 
            <li>
                <a href="/doctor/info/7.html" rel="nofollow"><img src="/images/common/grey.gif" data-original="/images/service/y_4.png" alt="试管无忧 泰国试管婴儿攻略" class="lazy"/></a>
                <p>Dr.Pinyo</p>
                <span>拼优医生是杰特宁医院专家，泰国试管婴儿界的名医。专长不孕不育治疗、宫腔镜手术、子宫内膜异位、女子妇科疾病等病症的治疗。1987年泰国普及古高医学院学士学位。</span>                
            </li> 
             <li>
                <a href="/doctor/info/8.html" rel="nofollow"><img src="/images/common/grey.gif" data-original="/images/service/y_5.jpg" alt="试管无忧 泰国试管婴儿攻略" class="lazy"/></a>
                <p>Dr.Chai</p>
                <span>Chai（杰特宁医院的医生，泰国试管婴儿界的名医。专长不孕不育治疗-宫腔内人工授精、体外授精；妇科腔镜手术（卵巢肌瘤或囊肿、子宫肌瘤、宫外孕、子宫内膜异位））。</span>
                
            </li> 
        </ul>
        <a rel="nofollow" href="http://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn" target="_blank" class="zixun">立即咨询</a>
    </div>
    <div class="order" id="nowOrder">
        <h2>快速预约</h2>
        <div class="henghong"></div>
        <p>对接海外医疗资源，让您精准预约泰国名医</p>
        <div class="advan">
            <form  class="dataForm" action="/service/remote" method="post">
            <div class="box_left">
                <div class="adv_title">
                    <span>1</span>选择不孕的病症
                </div>
                <div class="cont mCustomScrollbar" data-mcs-theme="dark">
                    <?php foreach ($disease as $key => $value) :?>
                    <div class="cbox">
                        <div class="xbox <?php echo $key==10?'xred':''?>"><?php echo $value;?></div>
                        <label>
                            <input type="radio" name='symptoms' value='<?php echo $key;?>' class='radio_btn' <?php echo $key==10?"checked='true'":''?>>
                        </label>
                    </div> 
                    <?php endforeach?>  
                   
                </div>
            </div>
            <div class="box_center">
                <div class="adv_title">
                    <span>2</span>选择意向的医院
                </div>
                <div class="contt mCustomScrollbar" data-mcs-theme="dark">
                    <div class="rbox">
                        <div class="sbox mt">我没有意向中的医院</div>
                        <label>
                            <input type="radio" name='tag' value='1' class='radi_btn' checked="ture">
                        </label>
                    </div>
                    <div class="rbox">
                        <div class="sbox">杰特宁生殖研究中心</div>
                        <label>
                            <input type="radio" name='u_hospital' value='杰特宁生殖研究中心' class='radi_btn'>
                        </label>
                    </div> 
                    <div class="rbox">
                        <div class="sbox">全球生殖中心</div>
                        <label>
                            <input type="radio" name='u_hospital' value='全球生殖中心' class='radi_btn'>
                        </label>
                    </div> 
                    <div class="rbox">
                        <div class="sbox">爱宝贝生殖中心</div>
                        <label>
                            <input type="radio" name='u_hospital' value='爱宝贝生殖中心' class='radi_btn'>
                        </label>
                    </div> 
                    <div class="rbox">
                        <div class="sbox">SUPERIOR A.R.T中心</div>
                        <label>
                            <input type="radio" name='u_hospital' value='SUPERIOR A.R.T中心' class='radi_btn'>
                        </label>
                    </div> 
                    <div class="rbox">
                        <div class="sbox">帕亚泰是拉差</div>
                        <label>
                            <input type="radio" name='u_hospital' value='帕亚泰是拉差' class='radi_btn'>
                        </label>
                    </div> 
                    <div class="rbox">
                        <div class="sbox">暹罗试管生殖中心</div>
                        <label>
                            <input type="radio" name='u_hospital' value='暹罗试管生殖中心' class='radi_btn'>
                        </label>
                    </div> 
                    <div class="rbox">
                        <div class="sbox">BNH医院</div>
                        <label>
                            <input type="radio" name='u_hospital' value='BNH医院' class='radi_btn'>
                        </label>
                    </div> 
                    
                </div>
            </div>
            <div class="box_right">
                <div class="adv_title">
                    <span>3</span>提交申请
                </div>
                <input type="text"  name="u_name"  datatype="s2-10" errormsg="姓名格式不正确！" class="name" nullmsg="请填写姓名！" placeholder="您的姓名" autocomplete="off" />
                    <select name="u_age" class="opts" onmousedown="if(this.options.length>6){this.size=6}" onblur="this.size=0" onchange="this.size=0">
                        <option value="0">--选择您的年龄--</option>
                        <option value="20">20岁</option>
                        <option value="21">21岁</option>
                        <option value="22">22岁</option>
                        <option value="23">23岁</option>
                        <option value="24">24岁</option>
                        <option value="25">25岁</option>
                        <option value="26">26岁</option>
                        <option value="27">27岁</option>
                        <option value="28">28岁</option>
                        <option value="29">29岁</option>
                        <option value="30">30岁</option>
                        <option value="31">31岁</option>
                        <option value="32">32岁</option>
                        <option value="33">33岁</option>
                        <option value="34">34岁</option>
                        <option value="35">35岁</option>
                        <option value="36">36岁</option>
                        <option value="37">37岁</option>
                        <option value="38">38岁</option>
                        <option value="39">39岁</option>
                        <option value="40">40岁</option>
                        <option value="41">41岁</option>
                        <option value="42">42岁</option>
                        <option value="43">43岁</option>
                        <option value="44">44岁</option>
                        <option value="45">45岁</option>
                        <option value="46">46岁</option>
                        <option value="47">47岁</option>
                        <option value="48">48岁</option>
                        <option value="49">49岁</option>
                        <option value="50">50岁</option>
                    </select>
                    <input type="text" name="u_mobile"  datatype="m" errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！"  value="" placeholder="您的手机号（限11位）" class="phone">
                    <button type="button" class="submit">立即预约</button>
                    <input type="hidden" name="dosubmit" value="1"/>    
            </div>
            </form>
        </div>
    </div>
    <div class="data">
        <div class="data_cont">
            <h2>前期需准备材料</h2>
            <div class="henghong"></div>
            <ul class="data_box">
                <p>小贴士</p>
                <li><i>1</i>病史：现病史、即病史；</li>
                <li><i>2</i>男女双方检查：血常规、传染性疾病、染色体、支原体/衣原体等；</li>
                <li><i>3</i>女方检查：性激素六项、阴道B超、AMH（抗苗勒试管激素）等；</li>
                <li><i>4</i>男方检查：精液分析等；</li>
                <li><i>5</i>病理：病理诊断报告；</li>
                <li><i>6</i>病程记录：过往手术记录；</li>
            </ul>
        </div>
    </div>
    <div class="past">
        <h2>往期回顾</h2>
        <div class="henghong"></div>
        <p>远程问诊海外专家案例，让您不出国门<br />就可享受泰国顶尖试管专家问诊</p>
        <div class="screen">
            <div class="screen-play">
                <div class="ck_img">
                    <img class="lazy vieo_btn" src="/images/service/l1.jpg" alt="试管无忧 泰国试管婴儿攻略">
                    <img class="lazy vieo_btn" src="/images/service/l2.jpg" alt="试管无忧 泰国试管婴儿攻略">
                    <img class="lazy vieo_btn" src="/images/service/l3.jpg" alt="试管无忧 泰国试管婴儿攻略">
                </div>
                <img class="vid_btn" src="/images/service/play-01.png" alt="试管无忧 ">
            </div>
            <div class="screen-list">
                <ul>
                    <li class="play_ck">
                        <div class="list-img">
                            <img src="/images/service/l01.jpg" alt="试管无忧 泰国试管婴儿攻略">
                        </div>
                        <div class="list-text">
                            41岁陈女士(卵巢萎缩)：赴泰前，与蔡医生远程问诊
                        </div>
                    </li>
                    <li>
                        <div class="list-img">
                            <img src="/images/service/l02.jpg" alt="试管无忧 泰国试管婴儿攻略">
                        </div>
                        <div class="list-text">
                            山东夫妇(求龙凤胎)：远程问诊，确定预约提迪贡医生
                        </div>
                    </li>
                    <li>
                        <div class="list-img">
                            <img src="/images/service/l03.jpg" alt="试管无忧 泰国试管婴儿攻略">
                        </div>
                        <div class="list-text">
                            胡女士(多次流产)：咨询治疗和护理建议，确定赴泰时间
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="problem">
        <div class="problem-text">
            <div class="pro-ion"></div>
            <h2 id="abcd">常见问题</h2>
            <div class="henghong"></div>
            <p>您想问的问题，这里都有</p>
            <ul>
                <li>
                    <h3>我想问下，远程问诊是不是可以指定某个医生，是医生本人吗？</h3>
                    <p>是的，只要您有意向的医院医生都可以指定预约咨询，是医生本人和您一对一视频的，请放心！</p>
                </li>
                <li>
                    <h3>预约远程问诊，要提前准备哪些材料吗？</h3>
                    <p>您只需提前准备好夫妻双方三个月内的检查报告、以及填写试管无忧的远程问诊受理单，并注明想要咨询的问题提交试管无忧就医顾问即可。医生会以视频的方式，或者我们翻译过来的文字方式为您解答。</p>
                </li>
                <li>
                    <h3>远程咨询多少钱？怎么付款？</h3>
                    <p>试管无忧的远程问诊费用只需699元，您提交申请后，将费用直接支付到我司的公司账户或者支付宝账号即可，一般3-4天内即可和指定医生进行视频咨询。</p>
                </li>
                <li>
                    <h3>远程咨询，可以和医生一对一视频多长时间？</h3>
                    <p>通常视频问诊5~10分钟左右，所以提前准备好您所要咨询的问题，具体视情况而定。您可以根据医生的建议，先在国内调理好身体，然后再去泰国进行试管，成功率会大大提高哦！</p>
                </li>
                <li>
                    <h3>为什么其他医院远程咨询都那么贵，你们的这么便宜？</h3>
                    <p>试管无忧开设远程咨询这个服务目的：是帮助更多想去泰国做试管婴儿的姐妹，足不出户，就可以获得泰国试管医生的第二诊疗建议，调理好身体后再出国做试管，免得来回跑浪费钱。699元的远程咨询只是收取了一定的人工费和预约医生的费用。我们是真心想帮助您！</p>
                </li>
            </ul>
            <!-- <a href="" class="more">加载更多</a> -->
        </div>
    </div>
    <div class="wap_five">
    <div class="five_text">
        远程问诊，与泰国名医<span>零距离</span>
    </div>
    <div class="fivetext" id="nowOrder1">每月前50名可获取免费问诊名额</div>
    <div class="from">
        <form  class="dataForm2" action="/main/index" id="regForm" method="post" >
            <select class="select" name="symptoms" id="symptoms" >
                <option value="0" disabled selected>请选择您想咨询的病症</option> 
                <?php foreach ($disease as $key => $value) :?>
                <option value="<?php echo $key;?>"><?php echo $value;?></option>
                <?php endforeach?> 
            </select>
            <input type="text" class="input"   name="u_name"  datatype="s2-10" errormsg="姓名格式不正确！" nullmsg="请填写姓名！" autocomplete="off" placeholder="您的姓名">
            <input type="text" class="input"  name="u_mobile"  datatype="m" errormsg="手机号码格式不正确！" nullmsg="请填写手机号码！"  placeholder="您的手机号">
            <a class="submit"/>领取免费名额</a>
            <div class="dw">
                <div class="yd_01"><span class="glyphicons glyphicons-article"></span></div>
                <div class="yd_02"><span class="glyphicons glyphicons-user"></span></div>
                <div class="yd_03"><span class="glyphicons glyphicons-earphone"></span></div>
            </div>
            <div class="span"></div>
            <input type="hidden" name="dosubmit" value="1"/>    
        </form>
    </div>
</div>
</div>
<script type="text/javascript"> 
$(".dataForm").Validform({
    btnSubmit:".submit", 
    tiptype:function(msg){
        layer.alert(msg,{
            icon:0
        });   
    },
    tipSweep:true,
    ajaxPost:true,
    callback:function(o){
        if (o.status == 200) {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});     
        } else {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
        } 
    }
});
$(".dataForm2").Validform({
    btnSubmit:".submit", 
    tiptype:function(msg){
        layer.alert(msg,{
            icon:0
        });   
    },
    tipSweep:true,
    ajaxPost:true,
    callback:function(o){
        if (o.status == 200) {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});     
        } else {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
        } 
    }
});
</script>