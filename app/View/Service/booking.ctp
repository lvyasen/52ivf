<?= $this->Sgwy->addCss('/css/service/index.css'); ?>
<?= $this->Sgwy->addCss('/css/idangerous.swiper.css'); ?>
<?= $this->Sgwy->addJs('/js/service/index.js'); ?>
<?= $this->Html->script('/js/idangerous.swiper.js'); ?>
<?= $this->Sgwy->addJs('/js/service/sankuaifanzhuan.js'); ?>
<div class="booking">
    <div class="booking1">
        <div class="div33">
            <div class="div33_01">
                <a href="http://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn" target="_blank">申请精准预约 ></a>
            </div>
        </div>
        <div class="div34">
            <div class="div34_01">您还在为做试管而烦恼吗？</div>
            <div class="div34_04">
                <div class="section2">
                    <div class="wow wow01" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="s2-scr">
                            <!-- <a class="s2-btn front-face">反复失败？</a> -->
                            <a class="s2-btn back-face">精神压力巨大，而且随着年龄<br>增长，愈来愈被动</a>
                            <i class="i1"></i>
                        </div>
                    </div>
                    <div class="wow wow02" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
                        <div class="s2-scr">
                            <!-- <a class="s2-btn front-face">想继续尝试？</a> -->
                            <a class="s2-btn back-face">找不出原因，又担心医院不靠<br>谱，怕继续失败</a>
                            <i class="i2"></i>
                        </div>
                    </div>
                    <div class="wow" data-wow-delay="1s" style="visibility: visible; animation-delay: 1s; animation-name: fadeInUp;">
                        <div class="s2-scr">
                            <!-- <a class="s2-btn front-face">想出国治疗？</a> -->
                            <a class="s2-btn back-face" id="last">不熟悉医院，不知道哪个医生好，没法做到一对一精细诊疗</a>
                            <i class="i3"></i>
                        </div>
                    </div>    
                </div>
            </div>
            <div class="div34_05">
                <ul>
                    <li><img src="/images/service/w11.png" alt="试管无忧 还在为做试管婴儿感到烦恼吗,泰国试管婴儿攻略"><p>反复失败？</p></li>
                    <li><img src="/images/service/w22.png" alt="试管无忧 还在为做试管婴儿感到烦恼吗,泰国试管婴儿攻略"><p>想继续尝试？</p></li>
                    <li><img src="/images/service/w33.png" alt="试管无忧 还在为做试管婴儿感到烦恼吗,泰国试管婴儿攻略"><p>想出国治疗？</p></li>
                </ul>
            </div>
        </div>
        <div class="div35_wai"> 
            <div class="div35">
                <div class="div35_01">提高成功率的有效途径</div>
                <div class="div35_03">
                    <img src="/images/service/sekuai.png" alt="试管无忧 泰国试管婴儿攻略">
                </div>
                <div class="div35_04">找对医院 &nbsp;&nbsp;找对医生 &nbsp;&nbsp;选对方案 &nbsp;&nbsp;一对一诊疗</div>
                <section class="section7">
                    <div id="box">
                        <div id="list3" onselectstart="return false">
                            <ul>
                                <li style="padding:20px; width: 39px; height: 21px; top: 86px; left: 274px; z-index: 4;" class="">
                                    <img src="/images/service/zhaoduiyiyuan.jpg" alt="试管无忧 找对医院,泰国试管婴儿攻略" style='border-radius:10px'  class="lazy">
                                    <div style="opacity: 0; "></div>
                                </li>
                                <li style="padding:20px; width: 39px; height: 21px; top: 86px; left: 274px; z-index: 6;" class="">
                                    <img src="/images/service/zhaoduiyisheng.jpg" alt="试管无忧 找对医生,泰国试管婴儿攻略" style='border-radius:10px'  class="lazy">
                                    <div style="opacity: 0; "></div>
                                </li>
                                <li style="padding:20px; width: 390px; height: 210px; top: 26px; left: 659px; z-index: 8;" class="lazy">
                                    <img src="/images/service/xuanduifangan.jpg" alt="试管无忧 选对方案,泰国试管婴儿攻略" style='border-radius:10px'>
                                    <div style="opacity: 0; "></div>
                                </li>
                                <li style="padding:20px; width: 622px; height: 346px; top: -42px; left: 274px; z-index: 10;" class="lazy">
                                    <img src="/images/service/yiduiyi.jpg" alt="试管无忧 一对一就诊,泰国试管婴儿攻略" style='border-radius:10px'>
                                    <div style="opacity: 0; "></div>
                                </li>  
                            </ul>
                        </div>
                    </div>
                </section>
                <div class="div35_06">国外优质资源、一对一导诊，强有力提高疑难试管的成功率</div>
                <div class="div35_07">
                    <p class="p1">试管无忧拥有专业的服务团队，优质的国外试管资源，为患者提供名医匹配、指定预约、个性导诊服务</p>
                    <p class="p1">最大化提高疑难试管患者的就医效率</p>
                </div>        
            </div>
        </div>
        <div class="div36">
            <div class="div36_01">聚焦疑难的精准预约服务</div>
            <div class="div36_03">
                <img src="/images/common/grey.gif" data-original="/images/service/sekuai.png" alt="试管无忧 泰国试管婴儿攻略"  class="lazy">
            </div>
            <div class="div36_04">精准预约服务聚焦疑难，真正做到个性、精准、高效</div>
            <div class="div36_05">
                <ul class="div36_05_left">
                    <img src="/images/common/grey.gif" data-original="/images/service/children.png" alt="试管无忧 一群小孩,泰国试管婴儿攻略"  style="width:720px;height:448px;"  class="lazy">
                </ul>
                <ul class="div36_05_right">
                    <li class="div36_05_lie"><a href="/doctor/index/">
                        <div class="div36_05_lie_pic">
                            <img src="/images/common/grey.gif" data-original="/images/service/jingzhuntubiao1.png" alt="试管无忧 医药箱"  class="lazy">
                        </div> 
                        <div class="div36_05_lie_words">
                            <p class="title">名医匹配</p>
                            <p class="p02">结合患者的自身情况，及医院技术特点</p>
                            <p>医生的擅长，匹配最合适的医院和医生</p>
                        </div></a>       
                    </li>
                    <li class="div36_05_lie"><a href="/#index_seven">
                        <div class="div36_05_lie_pic">
                            <img src="/images/common/grey.gif" data-original="/images/service/jingzhuntubiao2.png" alt="试管无忧 化验单"  class="lazy">
                        </div> 
                        <div class="div36_05_lie_words">
                            <p class="title">指定预约</p>
                            <p class="p02">帮助患者预约由试管无忧推荐或者患者</p>
                            <p>自己指定的医生。</p>
                        </div> </a>    
                    </li>
                    <li class="div36_05_lie"><a href="http://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn','_blank','scrollbars=0,resizable=0,width=900,height=700;">
                        <div class="div36_05_lie_pic">
                            <img src="/images/common/grey.gif" data-original="/images/service/jingzhuntubiao3.png" alt="试管无忧 听诊器"  class="lazy">
                        </div> 
                        <div class="div36_05_lie_words">
                            <p class="title">个性导诊</p>
                            <p class="p02">全程为患者提供一对一个性导诊，让患</p>
                            <p>者与医生充分沟通、个性治疗。</p>
                        </div></a>     
                    </li>
                </ul>
            </div>
        </div>
        <div class="div37_wai">
            <div class="div37">
                <div class="div37_01">我们都选择了精准预约</div>
                <div class="div37_03">
                    <a href="http://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn"><img src="/images/service/sekuai.png" alt="试管无忧 " border="0"></a>
                </div>
                <div class="div37_04">精准预约已累计为2000名试管患者提供精准预约服务</div>
                <div class="div37_05">
                    <div class="div37_05_nei01">
                      <ul class="ul01">
                        <li class="li01">
                            <img src="/images/common/grey.gif" data-original="/images/service/photo1_03.jpg" alt="试管无忧 多年失败的王女士,泰国试管婴儿攻略"  class="lazy">
                        </li>
                        <li class="li02">多年失败的王女士</li>
                        <li class="li03">
                            <img src="/images/common/grey.gif" data-original="/images/service/sekuai1.png" alt="试管无忧 泰国试管婴儿攻略"  class="lazy">
                        </li>
                        <li class="li04">"&nbsp;我一心想要个孩子，由于自身条件限制，导致多年来无法成功怀孕，找遍了国内大大小小的试管医院，就是怀不上，后来通过试管无忧的‘精准预约’服务，如今终于如愿以偿。"
                        </li>
                      </ul>
                    </div>  
                    <div class="div37_05_nei02">
                      <ul class="ul01" id="ul02">
                        <li class="li01">
                            <img src="/images/common/grey.gif" data-original="/images/service/photo2_03.jpg" alt="试管无忧 求医无门的张妈妈,泰国试管婴儿攻略"  class="lazy">
                        </li>
                        <li class="li02">求医无门的张妈妈</li>
                        <li class="li03">
                            <img src="/images/common/grey.gif" data-original="/images/service/sekuai2.png" alt="试管无忧 泰国试管婴儿攻略"  class="lazy">
                        </li>
                        <li class="li04">"&nbsp;我因为患有卵巢过度刺激综合征，一直不能生，心里很落魄 ，也找不到好的解决办法，后来终于通过试管无忧匹配了一家泰国的医院，采用最先进的方案进行手术，现在已经是两个宝宝的妈妈了，感谢试管无忧！"
                        </li>
                      </ul>
                    </div> 
                    <div class="div37_05_nei03">
                      <ul class="ul01" id="ul03">
                        <li class="li01">
                            <img src="/images/common/grey.gif" data-original="/images/service/photo3_03.jpg" alt="试管无忧 某外企高管Lisa小姐,泰国试管婴儿攻略"  class="lazy">
                        </li>
                        <li class="li02">某外企高管Lisa小姐</li>
                        <li class="li03">
                            <img src="/images/common/grey.gif" data-original="/images/service/sekuai3.png" alt="试管无忧 泰国试管婴儿攻略"  class="lazy"> 
                        </li>
                        <li class="li04">"&nbsp;我是一家外企公司的高管，一直忙于工作，生育问题被忽略了，35岁了才想起来要孩子，突然想生的时候，发现自己已经失去了自然生育的能力，为了提高就医效率，贻误时机，我从一开始便通过试管无忧选择了国外的医院，技术好，服务周到，开心！"
                        </li>
                      </ul>
                    </div> 
                </div>
            </div> 
        </div>
        <div class="new_ser_one">
            <div class="new_ser_one_01">精准预约助孕套餐</div>
            <div class="new_ser_one_03"><img src="/images/common/grey.gif" data-original="/images/service/sekuai.png" alt="试管无忧 "  class="lazy"></div>
            <div class="new_ser_one_04">精准、高效、专业、聚焦、贴心</div>
            <div class="new_ser_one_05">
                <div class="goodness">
                    <div class="inner-min">
                        <div class="section-content">
                            <div class="nav-arrow">
                                <a class="arrow prev swiper-prev"><i></i></a>
                                <a class="arrow next swiper-next"><i></i></a>
                            </div>
                            <div id="Goodness" class="slide swiper-container" style="height: 290px;">
                                <ul class="swiper-wrapper">
                                    <li class="swiper-slide">
                                        <img src="/images/service/jing/j1.jpg" alt="试管无忧 泰国试管婴儿攻略">
                                        <div class="desc">泰国试管婴儿<br>全程咨询服务</div>
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/images/service/jing/j2.jpg" alt="试管无忧 泰国试管婴儿攻略">
                                        <div class="desc">协助国内做“试管婴儿<br>前的身体检查”</div>
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/images/service/jing/j3.jpg" alt="试管无忧 泰国试管婴儿攻略">
                                        <div class="desc">将检查报告呈送给泰国<br>资深生殖医生做初步评估</div>
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/images/service/jing/j4.jpg" alt="试管无忧 泰国试管婴儿攻略">
                                        <div class="desc">预约/匹配泰国资深<br>生殖医生、医院</div>
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/images/service/jing/j5.jpg" alt="试管无忧 泰国试管婴儿攻略">
                                        <div class="desc">就诊前定制个性<br>就医方案一份</div>
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/images/service/jing/j6.jpg" alt="试管无忧 泰国试管婴儿攻略">
                                        <div class="desc">各大医院就诊绿色通道<br>并享受VIP级别待遇</div>
                                    </li>
                                     <li class="swiper-slide">
                                        <img src="/images/service/jing/j7.jpg" alt="试管无忧 泰国试管婴儿攻略">
                                        <div class="desc">资深医务翻译全程<br>一对一服务，与医生轻松交流</div>
                                    </li>
                                     <li class="swiper-slide">
                                        <img src="/images/service/jing/j8.jpg" alt="试管无忧 泰国试管婴儿攻略">
                                        <div class="desc">IVF疗程期间建立<br>泰国IVF诊疗档案</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="booking2">
        <div class="wav_title">
            <p>精准预约</p>
            <img  onclick="window.location.href='/';" src="/images/mobile/fanhui.png"><span>首页</span>
        </div>
        <div class="wap_banner">
            <a class="banner" href="http://pht.zoosnet.net/LR/Chatpre.aspx?id=PHT13532865&lng=cn">
                <img src="/images/service/banner01.png" alt="试管无忧 泰国试管婴儿攻略">        
            </a>
        </div>
        <div class="fannao">
            <div class="fannao01">您还在为做试管而烦恼吗？</div>
            <div class="fannao04">
                <div class="div34_04">
                    <div class="section2">
                        <div class="wow">
                            <div class="s2-scr">
                                <a class="s2-btn front-face" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;"><img src="/images/service/duihuakuang01.png"></a>
                                <a class="s2-btn back-face"><img src="/images/service/duihuakuang02.png">
                                    </a>
                               
                            </div>
                        </div>
                        <div class="wow wow02" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
                            <div class="s2-scr">
                                <a class="s2-btn front-face"><img src="/images/service/duihuakuang03.png"></a>
                                <a class="s2-btn back-face"><img src="/images/service/duihuakuang04.png"></a>
                                <i class="i2"></i>
                            </div>
                        </div>
                        <div class="wow" data-wow-delay="1s" style="visibility: visible; animation-delay: 1s; animation-name: fadeInUp;">
                            <div class="s2-scr">
                                <a class="s2-btn front-face"><img src="/images/service/duihuakuang05.png"></a>
                                <a class="s2-btn back-face" id="last"><img src="/images/service/duihuakuang06.png"></a>
                                <i class="i3"></i>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
            <div class="fannao05">
                <img src="/images/service/xiaonvhai.png" alt="试管无忧 ">
            </div>
        </div>   
        <div class="tigao">
            <div class="jujiao">
                <div class="jujiao011">聚焦疑难的精准预约服务</div>
                <div class="jujiao03"><img src="/images/common/grey.gif" data-original="/images/service/henxian.png" alt="试管无忧 "  class="lazy"></div>
                <div class="jujiao04">
                    精准预约服务聚焦疑难
                </div>
                <div class="jujiao05">
                    真正做到个性、精准、高效
                </div>
                <div style='margin-bottom: 30px;'>
                    <div class="jujiao06" style='width:65%;float:left;margin-bottom: 0px;margin-left: 10px;'>
                        <img src="/images/service/jingzhun03.jpg" alt="试管无忧 " >
                    </div>
                    <div style='float:left;margin-top: 18px;'>
                        <div class='iconbox' style='background-color: #fdc839; '>
                            <span class="glyphicons glyphicons-user-add" style="line-height: 35px;" ></span>
                            <span class='icon-txt'>名医匹配</span>
                        </div>
                        <div class='iconbox' style='background-color: #5fc7a2'>
                            <span class="glyphicons glyphicons-calendar" style="line-height: 35px;" ></span>
                            <span class='icon-txt'>指定预约</span>
                        </div>
                        <div class='iconbox' style='background-color: #fd8958'>
                            <span class="glyphicons glyphicons-stethoscope" style="line-height: 35px;" ></span>
                            <span class='icon-txt'>个性导诊</span>
                        </div>
                    </div>
                    <div style='clear:both'></div>
                </div>
            </div>  
            <div class="women">
                <div class="women01">我们都选择了精准预约</div>
                <div class="women03"><img src="/images/service/henxian.png" alt="" ></div>
                <div class="women04">
                    精准预约已累计为2000名试管患者提供精准预约服务
                </div>
                <div class="women05">
                    <img src="/images/service/jingzhun06.jpg" alt="试管无忧 ">
                </div>
            </div>         
        </div> 
        <div class="new_ser_one">
            <div class="one_title">精准预约助孕套餐</div>
            <div class="one_content">
                <div class="boodness">
                    <div class="inner-min">
                        <div class="section-content">
                            <div class="nav-arrow">
                                <a class="arrow prev swiper-prev"><i></i></a>
                                <a class="arrow next swiper-next"><i></i></a>
                            </div>
                            <div id="Boodness" class="slide swiper-container" style="height: 240px;">
                                <ul class="swiper-wrapper">
                                    <li class="swiper-slide">
                                        <img src="/images/service/jing/j1.jpg" alt="试管无忧 " >
                                        <ul>
                                            <li>泰国试管婴儿全程咨询服务</li>
                                        </ul>
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/images/service/jing/j2.jpg" alt="试管无忧 " >
                                         <ul>
                                            <li>协助国内做“试管婴儿前的身体检查”</li>
                                        </ul>
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/images/service/jing/j3.jpg" alt="试管无忧 " >
                                         <ul>
                                            <li>将检查报告呈送给泰国资深生殖医生做初步评估</li>
                                        </ul>
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/images/service/jing/j4.jpg" alt="试管无忧 " >
                                         <ul>
                                            <li>预约/匹配泰国资深生殖医生、医院</li>
                                        </ul>
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/images/service/jing/j5.jpg" alt="试管无忧 " >
                                         <ul>
                                            <li>就诊前定制个性就医方案一份</li>
                                        </ul>
                                    </li>
                                    <li class="swiper-slide">
                                        <img src="/images/service/jing/j6.jpg" alt="试管无忧 " >
                                         <ul>
                                            <li>各大医院就诊绿色通道并享受VIP级别待遇</li>
                                        </ul>
                                    </li>
                                     <li class="swiper-slide">
                                        <img src="/images/service/jing/j7.jpg" alt="试管无忧 " >
                                         <ul>
                                            <li>资深医务翻译全程一对一服务，与医生轻松交流</li>
                                        </ul>
                                    </li>
                                     <li class="swiper-slide">
                                        <img src="/images/service/jing/j8.jpg" alt="试管无忧 " >
                                         <ul>
                                            <li>IVF疗程期间建立泰国IVF诊疗档案</li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>