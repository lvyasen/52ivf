<?php
Class RedisServer{

    static public $redis = null;
    static public $config = null;

    /* Redis锁 */
    const REDIS_LOCK_PREV = 'redis_lock:';

    /* 检查、限制 */
    const REDIS_KEY_MOBILE_SEND_LIMIT = 'ivf52_mobile_send_limit:';
    const REDIS_KEY_MOBILE_RESEND_TIME = 'ivf52_mobile_resend_time:';
    const REDIS_KEY_CHECK_USER_LOGIN_TODAY = 'ivf52_check_user_login_today:';

    /* 统计 */
    const REDIS_KEY_NEWS_READ_COUNT = 'ivf52_news_read_count:';
    const REDIS_KEY_NEWS_LOVE_COUNT = 'ivf52_news_love_count:';
    const REDIS_KEY_QUESTIONS_READ_COUNT = 'ivf52_questions_read_count:';
    const REDIS_KEY_QUESTIONS_LOVE_COUNT = 'ivf52_questions_love_count:';
    const REDIS_KEY_SHARE_READ_COUNT = 'ivf52_share_read_count:';
    const REDIS_KEY_SHARE_LOVE_COUNT = 'ivf52_share_love_count:';
    const REDIS_KEY_DISTRICT_TAG_LOVE_COUNT = 'ivf52_district_tag_love_count:';
    const REDIS_KEY_PLANS_ADD_COUNT = 'ivf52_plans_add_count:';
    const REDIS_KEY_AIDE_ADD_COUNT = 'ivf52_aide_add_count:';

    /* 缓存数据 */
    const REDIS_KEY_SEO = 'ivf52_site_seo_data:';
    const REDIS_KEY_CACHE_DATA_DISTRICT_GETMAPS = 'ivf52_cache_data:district_getmaps:';
    const REDIS_KEY_CACHE_DATA_DISTRICT_GET_TRAIN_DISTRICT = 'ivf52_cache_data:district_get_train_district:';
    const REDIS_KEY_CACHE_DATA_DISTRICT_TAGS = 'ivf52_cache_data:district_tags:';
    const REDIS_KEY_CACHE_DATA_TOP5_QUESTIONS = 'ivf52_cache_data:top5_questions:';
    const REDIS_KEY_CACHE_DATA_TOPS_SHARE = 'ivf52_cache_data:tops_share:';
    const REDIS_KEY_CACHE_DATA_HEAD_DISTRICT_SHARE_COUNT = 'ivf52_cache_data:head_district_share_count:';
    const REDIS_KEY_CACHE_DATA_HOME_SHARE = 'ivf52_cache_data:home_share:';
    const REDIS_KEY_CACHE_DATA_HOME_DOCTOR = 'ivf52_cache_data:home_doctor:';
    const REDIS_KEY_CACHE_DATA_HOME_NEW_DOCTOR = 'ivf52_cache_data:home_new_doctor:';
    const REDIS_KEY_CACHE_DATA_HOME_HOSPITAL = 'ivf52_cache_data:home_hospital:';
    const REDIS_KEY_CACHE_DATA_HOME_NEW_HOSPITAL = 'ivf52_cache_data:home_new_hospital:';
    const REDIS_KEY_CACHE_DATA_WEEK_TOP5_QUESTIONS = 'ivf52_cache_data:week_top5_questions:';
    const REDIS_KEY_CACHE_DATA_WEEK_TOTAL_TOP5_QUESTIONS = 'ivf52_cache_data:week_total_top5_questions:';
    const REDIS_KEY_CACHE_DATA_WEEK_READ_TOP5_SHARES = 'ivf52_cache_data:week_read_top5_shares:';


    public function __construct(){ 
        self::init();
        return self::$redis;
    }

    static function init(){ 
        $dir_config = dirname(dirname(__FILE__)).'/Config/';
        $production = Configure::read('ivf_production');
        $dir_config .= 'redis_'.$production.'.php';
        if(!empty(self::$redis)){
            return;
        }
        if(file_exists($dir_config)){
            require_once($dir_config);
            self::$config = REDIS_CONFIG::$SERVERS;
            $redis = new Redis();
            $redis->connect(self::$config['host'], self::$config['port']);
            if(!empty(self::$config['auth'])){
                $redis->auth(self::$config['auth']);
            }  
            self::$redis = $redis; 
        }else{ 
            exit('NO REDIS CONFIG!!');
        }
    }

    /**
     * 读取缓存数据
     * @param string $key 缓存变量名
     * @return mixed
     */
    static function get($key) {   
        $value = self::$redis->get(self::$config['prefix'].$key);
        $jsonData  = json_decode( $value, true );
        return ($jsonData === NULL) ? $value : $jsonData;   
    }
    /**
     * 数据添加到缓存 
     * @param string $key 缓存变量名
     * @param mixed $value  存储数据
     * @param integer $expire  有效时间（秒）
     * @return boolean
     */
    static function set($key, $value, $expire_time = null) { 
        $key   =   self::$config['prefix'].$key;
        $value  =  (is_object($value) || is_array($value)) ? json_encode($value) : $value; 
        if(is_int($expire_time) && $expire_time) { 
            $result = self::$redis->setex($key, $expire_time, $value);
        }else{
            $result = self::$redis->set($key, $value);
        }  
        return $result;
    } 
    /**
     * 删除缓存 
     * @param string $key 缓存变量名
     * @return boolean
     */
    static function del($key){
        self::$redis->delete(self::$config['prefix'].$key);  
    }


    static function exists($key)
    {
        return self::$redis->exists(self::$config['prefix'].$key);  
    } 
    
    static function get_lock_prev($lock_name){
        return self::REDIS_LOCK_PREV.$lock_name;
    }

    /**
     * redis 加锁
     * @author shensuoming
     */
    static function lock($lock_name, $time_out = 30){
        $lock_key = self::get_lock_prev($lock_name);
        if(self::$redis->setnx($lock_key, $lock_name)){
            self::$redis->expire($lock_key, $time_out); //redis锁定 默认30秒过期
            return true;
        }
        return false;
    }

    /**
     * 清除缓存 
     * @return boolean
     */
    static function clear() {
        return self::$redis->flushDB();
    }

    /**
     * redis 解锁
     * @author shensuoming
     */
    static function unlock($lock_name){
        $lock_key = self::get_lock_prev($lock_name);
        return self::$redis->delete($lock_key);
    }
}
