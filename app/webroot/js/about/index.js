$(document).ready(function () {
    $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 118,
        itemMargin: 8,
        asNavFor: '#slider'
    });

    $('#slider').flexslider({
        animation: "fade",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel",
    });
    $("img").lazyload({effect: "fadeIn"});
});

$("#pc_01").show()
$("#pp_01").show()
$("#xx_01").click(function(){
    $("#pc_01").show();
    $("#pc_02").hide();
    $("#pc_03").hide();
    $("#pp_01").show();
    $("#pp_02").hide();
    $("#pp_03").hide();
});
$("#xx_02").click(function(){
    $("#pc_02").show();
    $("#pc_01").hide();
    $("#pc_03").hide();
    $("#pp_01").hide();
    $("#pp_02").show();
    $("#pp_03").hide();
});
$("#xx_03").click(function(){
    $("#pc_03").show();
    $("#pc_02").hide();
    $("#pc_01").hide();
    $("#pp_03").show();
    $("#pp_02").hide();
    $("#pp_01").hide();
});