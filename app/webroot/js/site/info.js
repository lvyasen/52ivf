$('.apply').click(function(){
	$('.edm_tk').show();
});
$('.cha').click(function(){
	$('.edm_tk').hide();
});
$(".videoForm").Validform({ 
    btnSubmit:".submit", 
    tiptype:function(msg){
        layer.alert(msg,{icon:0});    
    },
    tipSweep:true,
    ajaxPost:true,
    callback:function(o){
        layer.closeAll();
        if (o.status == 200) {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});      
            setTimeout(function () {
                window.location.reload(); 
            }, 1000);   
        } else {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
        } 
    }
});
$(".siteForm").Validform({ 
    btnSubmit:".submi", 
    tiptype:function(msg){
        layer.alert(msg,{icon:0});    
    },
    tipSweep:true,
    ajaxPost:true,
    callback:function(o){
        layer.closeAll();
        if (o.status == 200) {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_succes'});  
            setTimeout(function () {
                window.location.reload(); 
            }, 3000);       
        } else {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
        } 
    }
});  

$(function() {
	var galleries = $('.ad-gallery').adGallery();
});
$('.paginator').find('span a').each(function($key,$val){ var url = $(this).attr('href');  $(this).attr('href',url+'#xxx'); });
//分页
$('#look_all_msg_btn').click(function(){
	$.ajax({
	    type : 'get',
	    url : '/api/leave_msg_lists',
	    success : function(data) {
			$('#leave_msg_box').html(data);
	    }
	});
	return false;
})
$('.paginator span a').live('click',function(){
	var url = $(this).attr('href');
	$.ajax({
	    type : 'get',
	    url : url,
	    success : function(data) {
	        $('#leave_msg_box').html(data);
	    }
	});
	return false;
});

/**
 * 锚点，平滑过度
 * 如何设定：class='tag_to' data-tag='tag_to_name'
 * 锚点定义：name='tag_to_name'
 * @author shensuoming
**/
$('.tag_to').click(function(){
    var tag_name = $(this).data('tag');
    $('html,body').animate({
        scrollTop:$("*[name="+tag_name+"]").offset().top
    }, 1000);
});


var oMarquee = document.getElementById("mydiv"); //滚动对象
var iLineHeight = 30;                           //单行高度，像素
var iLineCount = 7;                             //实际行数
var iScrollAmount = 1;                          //每次滚动高度，像素
function play()   
{
    oMarquee.scrollTop += iScrollAmount;
    if ( oMarquee.scrollTop == iLineCount * iLineHeight )
    oMarquee.scrollTop = 0;
    if ( oMarquee.scrollTop % iLineHeight == 0 ) {
    window.setTimeout( "play()", 2000 );
    } else {
    window.setTimeout( "play()", 50 );}
}
oMarquee.innerHTML += oMarquee.innerHTML;
window.setTimeout( "play()", 2000 ); //定时器用来循环滚动