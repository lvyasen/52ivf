$('.wap_submit').click(function(){
    $('.edm_tk').show();
});
$('.cha').click(function(){
    $('.edm_tk').hide();
});
$('.tdd').click(function(){
    $('.edm_wt').show();
    $('#ivf51_input').focus();
    $("#ivf51_input").select();
});
$('.chaa').click(function(){
    $('.edm_wt').hide();
    $('.copy_btn').text('复制微信号');
});
$(".videoForm").Validform({ 
    btnSubmit:".submit", 
    tiptype:function(msg){
        layer.alert(msg,{icon:0});    
    },
    tipSweep:true,
    ajaxPost:true,
    callback:function(o){
        layer.closeAll();
        if (o.status == 200) {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});      
            setTimeout(function () {
                window.location.reload(); 
            }, 1000);   
        } else {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
        } 
    }
});
$(".siteForm").Validform({ 
    btnSubmit:".submi", 
    tiptype:function(msg){
        layer.alert(msg,{icon:0});    
    },
    tipSweep:true,
    ajaxPost:true,
    callback:function(o){
        layer.closeAll();
        if (o.status == 200) {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_succes'});  
            setTimeout(function () {
                window.location.reload(); 
            }, 3000);       
        } else {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
        } 
    }
}); 

function clearSelections () {
    if (window.getSelector || typeof window.getSelection() === 'object') {
        // 获取选中
        var selection = window.getSelection();
        // 清除选中
        selection.removeAllRanges();
    } else if (document.selection && document.selection.empty) {
       // 兼容 IE8 以下，但 IE9+ 以上同样可用
        document.selection.empty();
        // 或使用 clear() 方法
        // document.selection.clear();
    }     
}
//复制
var clipboard = new Clipboard('.copy_btn');
clipboard.on('success', function(e) {
    $('.copy_btn').html('<span class="glyphicons glyphicons-tick" aria-hidden="true"></span>复制成功');
    clearSelections();
    // var t = window.setInterval(function(){
    //     $('.copy_btn').text('复制微信号');
    //     clearInterval(t);
    //     console.log(1);
    // }, 3000);
});
clipboard.on('error', function(e) {
    console.log(e);
});
// clipboard.on('error', function(e) {
//     console.log(e);
// });

$('.paginator').find('span a').each(function($key,$val){ var url = $(this).attr('href');  $(this).attr('href',url+'#xxx'); });
//分页
$('#look_all_msg_btn').click(function(){
    $.ajax({
        type : 'get',
        url : '/api/leave_msg_lists',
        success : function(data) {
            $('#leave_msg_box').html(data);
        }
    });
    return false;
})
$('.paginator span a').live('click',function(){
    var url = $(this).attr('href');
    $.ajax({
        type : 'get',
        url : url,
        success : function(data) {
            $('#leave_msg_box').html(data);
        }
    });
    return false;
});
$('#msg, #mobile').focus(function(){
       $('.div18').hide();
    });
$('#msg, #mobile').blur(function(){
       $('.div18').show();
    });

var oMarquee = document.getElementById("mydiv"); //滚动对象
var iLineHeight = 25;                           //单行高度，像素
var iLineCount = 7;                             //实际行数
var iScrollAmount = 1;                          //每次滚动高度，像素
function play()   
{
    oMarquee.scrollTop += iScrollAmount;
    if ( oMarquee.scrollTop == iLineCount * iLineHeight )
    oMarquee.scrollTop = 0;
    if ( oMarquee.scrollTop % iLineHeight == 0 ) {
    window.setTimeout( "play()", 2000 );
    } else {
    window.setTimeout( "play()", 50 );}
}
oMarquee.innerHTML += oMarquee.innerHTML;
window.setTimeout( "play()", 2000 ); //定时器用来循环滚动

/*手机端banner效果 START */
var currentindex=1;
var timerID;
function timer_tick() {
    currentindex=currentindex>=$("#control_point").children().length?1:currentindex+1;//此处的5代表幻灯片循环遍历的次数
    changeflash(currentindex);
}
function startAm(){
    timerID = setInterval("timer_tick()",8000);//8000代表间隔时间设置
}
function stopAm(){
    clearInterval(timerID);
    timerID=null;
}
function changeflash(i) {   
    currentindex=i;
    for (j=1;j<=$("#control_point").children().length;j++){//此处的5代表你想要添加的幻灯片的数量与下面的5相呼应
        if (j==i){
            $("#flash"+j).fadeIn(500);
            //$("#flash"+j).css("display","block");
            $("#f"+j).removeClass();
            $("#f"+j).addClass("dq");
            $("#flashBg").css("background-color",$("#flash"+j));
        }else{
            $("#flash"+j).css("display","none");
            //$("#flash"+j).fadeOut(500);
            $("#f"+j).removeClass();
            $("#f"+j).addClass("no");
        }
    }
}
$(document).ready(function(){  
       
    $("#flash>a").bind("mouseover",function(){
        //alert(11);
        stopAm();
    });
    $("#flash>a").bind("mouseout",function(){
        //alert(22);
        startAm();
    });
    $("#flashBg").css("background-color",$("#flash1").attr("name"));
    $(".flash_bar div").mouseover(function(){stopAm();}).mouseout(function(){startAm();});
    startAm();
    /*$(".item1").hover(function(){$("#tit_fc1").slideDown("normal");   }, function() {$("#tit_fc1").slideUp("fast");});                
    $(".item2").hover(function(){$("#tit_fc2").slideDown("normal"); }, function() {$("#tit_fc2").slideUp("fast");});
    $(".item3").hover(function(){$("#tit_fc3").slideDown("normal"); }, function() {$("#tit_fc3").slideUp("fast");});
    $(".item4").hover(function(){$("#tit_fc4").slideDown("normal"); }, function() {$("#tit_fc4").slideUp("fast");});*/
});          
/*手机端banner效果 END */