// JavaScript Document
var pageSize = 2;

var _page = {
    getHospital: function (n, cid) {
        $(n).removeAttr('onclick');
        $(n).html('加载中');
        $.ajax({
            url: "/ajax/page/",
            data: {ajaxdata: 'hospital', val: cid, p: pageSize, ajax: Math.random()},
            async: false,
            beforeSend: function () {
            },
            dataType: 'json',
            success: function (o) {
                if (o.status == 200) {
                    if (o.message != "") {
                        $("#loadPage > ul").append(o.message);
                        $(n).attr('onclick', "_page.getHospital(this," + cid + ")");
                        $(n).html('<a>加载更多</a>');
                        pageSize++;
                    }
                    else
                        $(n).html('已全加载');
                }
                else {
                    layer.msg(o.message, {icon: 0, skin: 'layer_mobile_login'});
                    $(n).attr('onclick', "_page.getHospital(this," + cid + ")");
                    $(n).html('<a>加载更多</a>');
                }
            },
            complete: function () {
            },
            error: function () {
            }
        });
    },
    getDoctor: function (n, cid) {
        $(n).removeAttr('onclick');
        $(n).html('加载中');
        $.ajax({
            url: "/ajax/page/",
            data: {ajaxdata: 'doctor', val: cid, p: pageSize, ajax: Math.random()},
            async: false,
            beforeSend: function () {
            },
            dataType: 'json',
            success: function (o) {
                if (o.status == 200) {
                    if (o.message != "") {
                        $("#loadPage > ul").append(o.message);
                        $(n).attr('onclick', "_page.getDoctor(this," + cid + ")");
                        $(n).html('<a>加载更多</a>');
                        pageSize++;
                    }
                    else
                        $(n).html('已全加载');
                }
                else {
                    layer.msg(o.message, {icon: 0, skin: 'layer_mobile_login'});
                    $(n).attr('onclick', "_page.getDoctor(this," + cid + ")");
                    $(n).html('<a>加载更多</a>');
                }
            },
            complete: function () {
            },
            error: function () {
            }
        });
    },
    getPlan: function (n, cid, t, k) {
        $(n).removeAttr('onclick');
        $(n).html('加载中');
        $.ajax({
            url: "/ajax/page/",
            data: {ajaxdata: 'plan', val: cid, t: t, k: k, p: pageSize, ajax: Math.random()},
            async: false,
            beforeSend: function () {
            },
            dataType: 'json',
            success: function (o) {
                if (o.status == 200) {
                    if (o.message != "") {
                        $("#loadPage > ul").append(o.message);
                        $(n).attr('onclick', "_page.getPlan(this," + cid + ",'" + t + "','" + k + "')");
                        $(n).html('<a>加载更多</a>');
                        pageSize++;
                        var script = document.createElement("script");
                        script.type = "text/javascript";
                        script.src = "/plugin/ctimg/js/main.js";
                        document.body.appendChild(script);
                    }
                    else
                        $(n).html('已全加载');
                }
                else {
                    layer.msg(o.message, {icon: 0, skin: 'layer_mobile_login'});
                    $(n).attr('onclick', "_page.getPlan(this," + cid + ",'" + t + "','" + k + "')");
                    $(n).html('<a>加载更多</a>');
                }
            },
            complete: function () {
            },
            error: function () {
            }
        });
    },
    getShare: function (n, cid, t, k) {
        $(n).removeAttr('onclick');
        $(n).html('加载中');
        $.ajax({
            url: "/ajax/page/",
            data: {ajaxdata: 'share', val: cid, t: t, k: k, p: pageSize, ajax: Math.random()},
            async: false,
            beforeSend: function () {
            },
            dataType: 'json',
            success: function (o) {
                if (o.status == 200) {
                    if (o.message != "") {
                        $("#loadPage > ul").append(o.message);
                        $(n).attr('onclick', "_page.getShare(this," + cid + ",'" + t + "','" + k + "')");
                        $(n).html('<a>加载更多</a>');
                        pageSize++;
                        var script = document.createElement("script");
                        script.type = "text/javascript";
                        script.src = "/plugin/ctimg/js/main.js";
                        document.body.appendChild(script);
                    }
                    else
                        $(n).html('已全加载');
                }
                else {
                    layer.msg(o.message, {icon: 0, skin: 'layer_mobile_login'});
                    $(n).attr('onclick', "_page.getShare(this," + cid + ",'" + t + "','" + k + "')");
                    $(n).html('<a>加载更多</a>');
                }
            },
            complete: function () {
            },
            error: function () {
            }
        });
    },
    getNews: function (n, cid) {
        $(n).removeAttr('onclick');
        $(n).html('加载中');
        $.ajax({
            url: "/ajax/page/",
            data: {ajaxdata: 'news', val: cid, p: pageSize, ajax: Math.random()},
            async: false,
            beforeSend: function () {
            },
            dataType: 'json',
            success: function (o) {
                if (o.status == 200) {
                    if (o.message != "") {
                        $("#loadPage > ul").append(o.message);
                        $(n).attr('onclick', "_page.getNews(this," + cid + ")");
                        $(n).html('<a>加载更多</a>');
                        pageSize++;
                    }
                    else
                        $(n).html('已全加载');
                }
                else {
                    layer.msg(o.message, {icon: 0, skin: 'layer_mobile_login'});
                    $(n).attr('onclick', "_page.getNews(this," + cid + ")");
                    $(n).html('<a>加载更多</a>');
                }
            },
            complete: function () {
            },
            error: function () {
            }
        });
    },
    getJoins: function (n, cid) {
        $(n).removeAttr('onclick');
        $(n).html('加载中');
        $.ajax({
            url: "/ajax/page/",
            data: {ajaxdata: 'joins', val: cid, p: pageSize, ajax: Math.random()},
            async: false,
            beforeSend: function () {
            },
            dataType: 'json',
            success: function (o) {
                if (o.status == 200) {
                    if (o.message != "") {
                        $("#loadPage > ul").append(o.message);
                        $(n).attr('onclick', "_page.getJoins(this," + cid + ")");
                        $(n).html('<a>加载更多</a>');
                        pageSize++;
                    }
                    else
                        $(n).html('已全加载');
                }
                else {
                    layer.msg(o.message, {icon: 0, skin: 'layer_mobile_login'});
                    $(n).attr('onclick', "_page.getJoins(this," + cid + ")");
                    $(n).html('<a>加载更多</a>');
                }
            },
            complete: function () {
            },
            error: function () {
            }
        });
    },
    getAnswer: function (n, cid, k) {
        $(n).removeAttr('onclick');
        $(n).html('加载中');
        $.ajax({
            url: "/ajax/page/",
            data: {ajaxdata: 'answer', val: cid, k: k, p: pageSize, ajax: Math.random()},
            async: false,
            beforeSend: function () {
            },
            dataType: 'json',
            success: function (o) {
                if (o.status == 200) {
                    if (o.message != "") {
                        $("#loadPage > ul").append(o.message);
                        $(n).attr('onclick', "_page.getAnswer(this," + cid + ",'" + k + "')");
                        $(n).html('<a>加载更多</a>');
                        pageSize++;
                        var script = document.createElement("script");
                        script.type = "text/javascript";
                        script.src = "/plugin/ctimg/js/main.js";
                        document.body.appendChild(script);
                    }
                    else
                        $(n).html('已全加载');
                }
                else {
                    layer.msg(o.message, {icon: 0, skin: 'layer_mobile_login'});
                    $(n).attr('onclick', "_page.getAnswer(this," + cid + ",'" + k + "')");
                    $(n).html('<a>加载更多</a>');
                }
            },
            complete: function () {
            },
            error: function () {
            }
        });
    },
    getPageComment: function (n, cid, act, mng) {
        var mng = arguments[3] ? arguments[3] : '';
        $(n).removeAttr('onclick');
        $(n).html('加载中');
        $.ajax({
            url: "/ajax/page/",
            data: {ajaxdata: act, val: cid, p: pageSize, u: mng, ajax: Math.random()},
            async: false,
            beforeSend: function () {
            },
            dataType: 'json',
            success: function (o) {
                if (o.status == 200) {
                    if (o.message != "") {
                        $("#loadPage > ul").append(o.message);
                        $(n).attr('onclick', "_page.getPageComment(this," + cid + ",'" + act + "')");
                        $(n).html('<a>加载更多</a>');
                        pageSize++;
                    }
                    else
                        $(n).html('已全加载');
                }
                else {
                    layer.msg(o.message, {icon: 0, skin: 'layer_mobile_login'});
                    $(n).attr('onclick', "_page.getPageComment(this," + cid + ",'" + act + "')");
                    $(n).html('<a>加载更多</a>');
                }
            },
            complete: function () {
            },
            error: function () {
            }
        });
    },
    changeNews: function () {
      $.ajax({
          url: "/ajax/page/",
          data:{ajaxdata:'change_news'},
          async:false,
          dataType:'json',
          success:function (o) {
              console.log(o);
              $('#select_btn').html(o.data)

          }
      });
    }
};