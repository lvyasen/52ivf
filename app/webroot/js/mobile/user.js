// JavaScript Document
var pwd_isok = 0;
var email_isok = 0;
var username_isok = 0;
var code_isok = 0;
var vemail_isok = 0;
var vmobile_isok = 0; 
var iTime = 59;
var Account; 

var _user = {
    emailExp: new RegExp("^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$"),
    usernameExp: new RegExp("^(?!_)(?!.*?_$)[a-zA-Z0-9_]{6,20}$"), //(\u4e00-\u9fa5)
    passwordExp: new RegExp("^[a-zA-Z0-9]{6,20}$"),
    mobileExp: new RegExp("^[1][3-8]\\d{9}$"),
    chkpwd: function (n) {
        if (!n) {
            alertMsg("请输入密码！");
            pwd_isok = 0;
        } else {
            if (this.passwordExp.test(n)) {
                pwd_isok = 1;
            } else {
                alertMsg("您输入的密码格式不正确！（6-20字母或数字）");
                pwd_isok = 0;
                return false;
            }
        }
    }, 
    remainTime: function (act) { 
        var iSecond,sSecond="",sTime="";
        if (iTime >= 0){
            iSecond = parseInt(iTime%60);
            if (iSecond >= 0){
                sSecond = iSecond + "秒";
            }
            sTime=sSecond;
            if(iTime==0){ 
                clearTimeout(Account);
                sTime='获取验证码';
                iTime = 59; 
                $('#send_mc_btn').addClass("bright");
                $('#send_mc_btn').attr('onclick',"_user.sendRcode('"+act+"')"); 
            }else{
                Account = setTimeout("_user.remainTime('"+act+"')",1000);
                iTime=iTime-1;
            }
        }else{
            sTime='没有倒计时';
        }
        $('#send_mc_btn').val(sTime);  
    },
    remainTime2: function (act) { 
        var iSecond,sSecond="",sTime="";
        if (iTime >= 0){
            iSecond = parseInt(iTime%60);
            if (iSecond >= 0){
                sSecond = iSecond + "秒";
            }
            sTime=sSecond;
            if(iTime==0){ 
                clearTimeout(Account);
                sTime='获取验证码';
                iTime = 59; 
                $('#send_mc_btn_mb').attr('onclick',"_user.sendPcRcode('"+act+"')"); 
            }else{
                Account = setTimeout("_user.remainTime2('"+act+"')",1000);
                iTime=iTime-1;
            }
        }else{
            sTime='没有倒计时';
        }
        $('#send_mc_btn_mb').val(sTime);  
    },
    remainTime3: function (act) { 
        var iSecond,sSecond="",sTime="";
        if (iTime >= 0){
            iSecond = parseInt(iTime%60);
            if (iSecond >= 0){
                sSecond = iSecond + "秒";
            }
            sTime=sSecond;
            if(iTime==0){ 
                clearTimeout(Account);
                sTime='获取验证码';
                iTime = 59; 
                $('.send_sms_btn').attr('onclick',"_user.sendAidecode('"+act+"')"); 
            }else{
                Account = setTimeout("_user.remainTime3('"+act+"')",1000);
                iTime=iTime-1;
            }
        }else{
            sTime='没有倒计时';
        }
        $('.send_sms_btn').val(sTime);  
    },
    sendRcode: function (act) {  
        var n = $("#u_mobile").val();
        if (!n) {
            layer.alert('请输入手机号码！',{icon:0}); 
            return false;
        }
        if (!this.mobileExp.test(n)) {
            layer.alert('请正确输入手机号码！',{icon:0});  
            return false;
        }
        $('#send_mc_btn').removeAttr('onclick'); 
        $('#send_mc_btn').removeClass("bright");
        $.ajax({ 
            url: "/ajax/other/",
            data: {ajaxdata: act, m: n, ajax: Math.random()},
            async: false, 
            beforeSend: function () {
            },
            dataType: 'json',
            success: function (o) { 
                if(o.status==200){
                    _user.remainTime(act);
                }
                else{ 
                    layer.alert(o.message,{icon:0});  
                    $('#send_mc_btn').attr('onclick',"_user.sendRcode('"+act+"')"); 
                    $('#send_mc_btn').addClass("bright");
                }
            },
            complete: function () {
            },
            error: function () {
            }
        });
    },  
    sendAidecode: function (act) {  
        var n = $("#aide_mobile").val();
        if (!n) {
            layer.alert('请输入手机号码！',{icon:0}); 
            return false;
        }
        if (!this.mobileExp.test(n)) {
            layer.alert('请正确输入手机号码！',{icon:0});  
            return false;
        }
        $('.send_sms_btn').removeAttr('onclick'); 
        $.ajax({ 
            url: "/ajax/other/",
            data: {ajaxdata: act, m: n, ajax: Math.random()},
            async: false, 
            beforeSend: function () {
            },
            dataType: 'json',
            success: function (o) { 
                if(o.status==200){
                    _user.remainTime3(act);
                }
                else{ 
                    layer.alert(o.message,{icon:0});  
                    $('.send_sms_btn').attr('onclick',"_user.sendAidecode('"+act+"')"); 
                }
            },
            complete: function () {
            },
            error: function () {
            }
        });
    }, 
    showDiv: function (t) {   
        if(t==0) 
            $(".hospital .box_bg").fadeIn();
        else
            $(".doctor .box_bg").fadeIn();
    }, 
    sendPcRcode: function (act) {  
        var n = $("#u_mobile_mb").val();
        if (!n) {
            layer.alert('请输入手机号码！',{icon:0}); 
            return false;
        }
        if (!this.mobileExp.test(n)) {
            layer.alert('请正确输入手机号码！',{icon:0});  
            return false;
        }
        $('#send_mc_btn_mb').removeAttr('onclick'); 
        $.ajax({ 
            url: "/ajax/other/",
            data: {ajaxdata: act, m: n, ajax: Math.random()},
            async: false, 
            beforeSend: function () {
            },
            dataType: 'json',
            success: function (o) { 
                if(o.status==200){
                    _user.remainTime2(act);
                }
                else{ 
                    layer.alert(o.message,{icon:0});  
                    $('#send_mc_btn_mb').attr('onclick',"_user.sendPcRcode('"+act+"')"); 
                }
            },
            complete: function () {
            },
            error: function () {
            }
        });
    }, 
    sendAvatar: function () {  
        var options = {
            beforeSubmit: function () {
                layer.load(1, {shade: false});  
            },
            dataType: 'json',
            success: function (o) {
                layer.closeAll();
                if(o.status==200){
                    layer.alert(o.message,{icon:0});   
                    setTimeout("window.location.reload()", 1000); 
                }
                else{ 
                    layer.alert(o.message,{icon:0});   
                }
            }
        };
        $('#upload-form').ajaxSubmit(options);
        return false;  
    },
    appoint: function (id) {  
		$("#hid").val(id); 
		$("#hospitalName").html($("#HN_"+id).val());
		$("#hospitalPrice").html($("#PR_"+id).val());
    },
    doctor_appoint: function (id) {  
		$("#did").val(id); 
		$("#hospitalName").html($("#HN_"+id).val()); 
    },
    uptNick: function (obj) {  
        var n = $("#u_nick").val();
        if (!n) {
            layer.alert('请输入昵称！',{icon:0}); 
            return false;
        }
        $(obj).removeAttr('onclick'); 
        var options = {
            beforeSubmit: function () {
                layer.load(1, {shade: false});  
            },
            dataType: 'json',
            success: function (o) {
                layer.closeAll();
                if(o.status==200){
                    layer.alert(o.message,{icon:0});   
                    setTimeout("window.location.href='/mobile/means/'", 2000);
                }
                else{ 
                    layer.alert(o.message,{icon:0}); 
                    $(obj).attr('onclick',"_user.uptNick(this)");    
                }
            }
        };
        $('#dataform').ajaxSubmit(options);
        return false;  
    },
    uptSex: function (val) {    
        $.ajax({ 
            url: "/ajax/user/",
            data: {ajaxdata: "upt_sex", val: val, ajax: Math.random()},
            async: false, 
            beforeSend: function () {
            },
            dataType: 'json',
            success: function (o) {  
                if(o.status!=200){
                    layer.alert(o.message,{icon:0});   
                } 
            },
            complete: function () {
            },
            error: function () {
            }
        });
    }  
};