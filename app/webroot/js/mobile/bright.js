// 所有输入项完成，按钮变色
    $('.vals').on('input', function(){
        if($(this).val() == ''){
            $(this).parent('div').removeClass('input_done');
        } else {
            $(this).parent('div').addClass('input_done');
        }
        var _parent = $(this).closest('.dataForm');
        var len = _parent.find('.vals').length;
        var lenDone = _parent.find('.input_done').length;
        if(lenDone >= len){
            $('.register_btn').addClass('bright');
        } else {
            $('.register_btn').removeClass('bright');
        }
    });
    //判断键盘是否弹出
    var wHeight = window.innerHeight;   //获取初始可视窗口高度
    $(window).resize(function() {         //监测窗口大小的变化事件
        var hh = window.innerHeight;     //当前可视窗口高度
        if(wHeight > hh ){           //可以作为虚拟键盘弹出事件
            $(".footnav").hide();
        }else{         //可以作为虚拟键盘关闭事件
            $(".footnav").show();
        }
    });