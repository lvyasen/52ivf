$(document).ready(function(e) {
    $('.gengduo').click(function(event) {
        $(".gd_pro").fadeIn(200);
        $(this).hide();
        $('.shou').show();
    });
    $('.shou').click(function(event) {
        $(".gd_pro").fadeOut(200);
        $(this).hide();
        $('.gengduo').show();
    });
    
    $(".close").click(function(){
    	$(".box_bg").fadeOut();
    })
    $(".t_y").click(function(){
    	$(".box_bg").fadeIn();
    })
    $(".publish").click(function () {
        if($(".text_writ textarea").val()==""){
           layer.alert('请填写内容！',{icon:0});   
           $(".text_writ textarea").focus();
           
        }else{
            layer.alert('您已评论！',{icon:0});  
            $(".text_writ textarea").val('');
        }
    })

    $(".dataForm").Validform({
        tiptype:function(msg){
            layer.alert(msg,{icon:0});   
        },
        tipSweep:true,
        ajaxPost:true,
        callback:function(o){
            if (o.status == 200) {
                layer.alert(o.message,{icon:0});  
                $(".box_bg").fadeOut();            
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            } else {
                layer.alert(o.message,{icon:0});    
            } 
        }
    });
    var wHeight = window.innerHeight;   //获取初始可视窗口高度
    $(window).resize(function() {         //监测窗口大小的变化事件
        var hh = window.innerHeight;     //当前可视窗口高度
        if(wHeight > hh){           //可以作为虚拟键盘弹出事件
            $(".footnav").hide();
        }else{         //可以作为虚拟键盘关闭事件
            $(".footnav").show();
        }
    });
})
