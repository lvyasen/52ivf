$(document).ready(function() {
    $(".publish").click(function () {
        if($(".text_writ textarea").val()==""){
           layer.alert('请填写内容！',{icon:0});   
           $(".text_writ textarea").focus();
           
        }else{
            layer.alert('您已评论！',{icon:0});  
            $(".text_writ textarea").val('');
        }
    })
    $(".dataForm").Validform({
        tiptype:function(msg){
            layer.alert(msg,{icon:0});   
        },
        tipSweep:true,
        ajaxPost:true,
        callback:function(o){
            if (o.status == 200) {
                layer.alert(o.message,{icon:0});    
                $(".box_bg").fadeOut();          
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            } else {
                layer.alert(o.message,{icon:0});    
            } 
        }
    });
    $(".vid_btn").click(function(){
        layer.alert('亲，目前还木有内容，敬请期待哟！',{icon:0});
    })

    $('.gengduo').click(function(event) {
        $(".gd_pro").fadeIn(200);
        $(this).hide();
        $('.shou').show();
    });
    $('.shou').click(function(event) {
        $(".gd_pro").fadeOut(200);
        $(this).hide();
        $('.gengduo').show();
    });
    $(".close").click(function(){
        $(".box_bg").fadeOut();
    }) 
    $(".t_y").click(function(){
        $(".box_bg").fadeIn();
    })

    // var swiper = new Swiper('.swiper-container',{
    //     slidesPerView: 4,                
    //     paginationClickable: true,
    //     spaceBetween: 30
    // });
    var swiper = new Swiper('.swiper-container', {
      slidesPerView: 2,
      spaceBetween: 30,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        1024: {
          slidesPerView:1,
          spaceBetween: 20,
        },
      }
    });
   
    function textLength (){
        $(".s_r_text").each(function(){
            var _this = $(this);
            var l = _this.text().length;            
            if(l>65){
            _this.html(_this.html().replace(/\s+/g, "").substr(0, 65) + "..." );
            } 
        })
        
    }
    textLength();
    
    var speed=15//速度数值越大速度越慢
    www_qpsh_com2.innerHTML=www_qpsh_com1.innerHTML
    function Marquee(){
        if(www_qpsh_com2.offsetWidth-www_qpsh_com.scrollLeft<=0)
        www_qpsh_com.scrollLeft-=www_qpsh_com1.offsetWidth
        else{
        www_qpsh_com.scrollLeft++
        }
    }
    var MyMar=setInterval(Marquee,speed)
    www_qpsh_com.onmouseover=function() {clearInterval(MyMar)}
    www_qpsh_com.onmouseout=function() {MyMar=setInterval(Marquee,speed)}
    
    var wHeight = window.innerHeight;   //获取初始可视窗口高度
    $(window).resize(function() {         //监测窗口大小的变化事件
        var hh = window.innerHeight;     //当前可视窗口高度
        if(wHeight > hh){           //可以作为虚拟键盘弹出事件
            $(".footnav").hide();
        }else{         //可以作为虚拟键盘关闭事件
            $(".footnav").show();
        }
    });
    
})