$('#video1').trigger('pause');
$('#video2').trigger('pause');
$(document).ready(function(){
    $(function() {
        $("#test_1").selectpick({
            container: '.test_1',
            onSelect: function(value,text){
                alert("这是回调函数，选中的值："+value+" \n选中的下拉框文本："+text);
            }
        });
        $("#test_2").selectpick({container:'.test_2',disabled:true});
        $("#test_3").selectpick({container:'.test_3',onSelect:function(value,text){
            enAble();//重新激活 test_2
        }
        });
    });
    $.fn.selectpick = function(options) {
      // selectpick的配置
      var selectpick_config = {
         container:"body",//模拟select生成的DIV存放的父容器
         height:59,
         width:600,
        // optionColor: "#3BAFDA",
        // selectedColor:"#3BAFDA",
         disabled: false, // 是否禁用,默认false
        // selectText: "", // 设置哪个文本被选中
         onSelect: "" // 点击后选中事件
      }
      var settings = $.extend({}, selectpick_config, options);
      
      // 每个下拉框组件的操作
      return this.each(function(elem_id) {
         var obj = this;
         var _offset = $(this).offset();
         var top = _offset.top + $(document).scrollTop();
         var elem_width = $(obj).width();
         var left = _offset.left + $(document).scrollLeft();
         var elem_id = $(obj).attr("id"); // 元素的ID
         // 生成的div的样式
         var _selectBody = "<div class='selectpick_div_box' onselectstart='return false;'><div class='selectpick_div selectpick_div_" + elem_id + "'  id='selectpick_" + elem_id + "'><span id='selectpick_span_" + elem_id + "'></span><span class='selectpick_icon' id='selectpick_icon_" + elem_id + "'></span></div><div class='selectpick_options selectpick_options_" + elem_id + "'></div></div>";
         $(_selectBody).appendTo(settings.container);
         $(obj).addClass("select_hide");

         // 设置selectpick显示的位置
         $(".selectpick_div_" + elem_id).css({
            "height": settings.height,
            "width": settings.width
         });
         
         //设置默认显示在div上的值为当前select的选中值
         $(".selectpick_div_" + elem_id + " span").first().text($(obj).find("option:selected").text());

         // 是否禁用下拉框
         if(settings.disabled){
             $(".selectpick_div_" + elem_id).addClass("selectpick_no_select");
             return;
         }
         // 点击div显示列表
         $(".selectpick_div_" + elem_id + ",#selectpick_span_" + elem_id + ",#selectpick_options_" + elem_id + "").bind("click", function(event) {
            var selected_text = $(".selectpick_div_" + elem_id + " span").first().text(); // 当前div中的值
            event.stopPropagation(); //  阻止事件冒泡

            if ($(".selectpick_ul_" + elem_id + " li").length > 0) {
               // 隐藏和显示div
               $(".selectpick_options_" + elem_id).empty().hide();
               return;
            } else {
               $(".selectpick_options").hide();
               $(".selectpick_options_" + elem_id).show();
               $(".selectpick_options ul li").remove();
               // 添加列表项
               var ul = "<ul class='selectpick_ul_" + elem_id + "'>";
               $(obj).children("option").each(function() {
                  if ($(this).text() == selected_text) {
                     ul += "<li class='selectpick_options_selected' style='height:" + (settings.height - 3) + "px; line-height:" + (settings.height - 3) + "px;'><label style='display:none;'>" + $(this).val() + "</label><label>" + $(this).text() + "</label></li>";
                  } else {
                     ul += "<li style='height:" + (settings.height - 3) + "px; line-height:" + (settings.height - 3) + "px;'><label style='display:none;'>" + $(this).val() + "</label><label>" + $(this).text() + "</label></li>";
                  }
               });
               ul += "</ul>";
               $(".selectpick_options_" + elem_id).css({
                  "width": settings.width + 5,
                  "left": 0,
                  "top": settings.height
               }).append(ul).show();
               
               // 每个li点击事件
               $(".selectpick_ul_" + elem_id + " li").bind("click", function() {
                  $(".selectpick_div_" + elem_id + " span").first().text($(this).children("label").first().next().text());
                  $(".selectpick_options_" + elem_id).empty().hide();
                  $(obj).val($(this).children("label").first().text());//设置下拉框的值
                  // 回调函数
                  if (settings.onSelect != undefined && settings.onSelect != "" && typeof settings.onSelect == "function") {
                     settings.onSelect($(this).children("label").first().text(), $(this).children("label").first().next().text());
                  }
               });
            }

         });
         // 点击div外面关闭列表
         $(document).bind("click", function(event) {
            var e = event || window.event;
            var elem = e.srcElement || e.target;
            if (elem.id == "selectpick_" + elem_id || elem.id == "selectpick_icon_" + elem_id || elem.id == "selectpick_span_" + elem_id) {
               return;
            } else {
               $(".selectpick_options_" + elem_id).empty().hide();
            }
         });

      });
    }



    // 侧菜单
    var divs = $('.l_nav_box .nbox');
    $(".l_nav_menu li").hover(function(){
        var index1 = $(this).index();
        $(this).addClass('nav_li').siblings('li').removeClass('nav_li').css({borderRight:'1px solid #f29c9f'});        
        $(divs).eq(index1).show().css({border:'1px solid #f29c9f',borderLeft:'none'}).siblings().hide();
    },function(){
        $(".left_nav").mouseleave(function(){
            $(divs).hide();
            $(".l_nav_menu li").removeClass('nav_li').css({borderRight:'none'});

        })
    });  
   

    $('#select_btn li:first').addClass('new_ck');
    if ($('#zSlider').length) {
        zSlider();
    }
    function zSlider(ID, delay){
        var ID=ID?ID:'#zSlider';
        var delay=delay?delay:2500;
        var new_ckEQ=0, picnum=$('#picshow_img li').size(), autoScrollFUN;
        $('#select_btn li').eq(new_ckEQ).addClass('new_ck');
        $('#picshow_img li').eq(new_ckEQ).show();
        // $('#picshow_tx li').eq(new_ckEQ).show();
        autoScrollFUN=setTimeout(autoScroll, delay);
        function autoScroll(){ 
            clearTimeout(autoScrollFUN);
            new_ckEQ++;
            if (new_ckEQ>picnum-1) new_ckEQ=0;
            $('#select_btn li').removeClass('new_ck');
            $('#picshow_img li').hide();
            // $('#picshow_tx li').hide().eq(new_ckEQ).slideDown(400);
            $('#select_btn li').eq(new_ckEQ).addClass('new_ck');
            $('#picshow_img li').eq(new_ckEQ).show();
            autoScrollFUN = setTimeout(autoScroll, delay);
        }
        $('#picshow').hover(function(){
            clearTimeout(autoScrollFUN);
        }, function(){
            autoScrollFUN = setTimeout(autoScroll, delay);
        });
        $('#select_btn li').hover(function(){
            var picEQ=$('#select_btn li').index($(this));
            if (picEQ==new_ckEQ) return false;
            new_ckEQ = picEQ;
            $('#select_btn li').removeClass('new_ck');
            $('#picshow_img li').hide();
            $('#select_btn li').eq(new_ckEQ).addClass('new_ck');
            $('#picshow_img li').eq(new_ckEQ).show();
            return false;
        });
    };
}) 
  $(".wpc").click(function(){
            $(".box_bg").show();
        });
$(".dataForm").Validform({
    btnSubmit:".submit", 
    tiptype:function(msg){
        layer.alert(msg,{
            icon:0
        });   
    },
    tipSweep:true,
    ajaxPost:true,
    callback:function(o){
        if (o.status == 200) {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});     
        } else {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
        } 
    }
});
$(".dataForm2").Validform({
    btnSubmit:".submit", 
    tiptype:function(msg){
        layer.alert(msg,{
            icon:0
        });   
    },
    tipSweep:true,
    ajaxPost:true,
    callback:function(o){
        if (o.status == 200) {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});     
        } else {
            layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
        } 
    }
});

$(".video_close").click(function(){
    $(".video_bf").hide();
    $('#video1').trigger('pause');
});
$('.tleft').click(function(){
    $('.video_bf').show();
    $('#video1').trigger('play');
}); 
$(".mvideo_cha").click(function(){
    $(".mvideo_bg").hide();
    $(".mvideo_tbg").hide();
    $('#video2').trigger('pause');  
    $(".footnav").show(); 
    $ (window).unbind ('scroll');
});
$(".video_btn").click(function(){
    $(".mvideo_bg").show();
    $(".mvideo_tbg").show();
    $('#video2').trigger('play');
    $(".footnav").hide();
    $ (window).scroll (function (){
        $ (this).scrollTop (0)
    });

});

var swiperGoodness = new Swiper('#Goodness', {
    autoplayDisableOnInteraction: false,
    onlyExternal: true,
    loop: true,
    speed: 500,
    autoplay: 4000,
    slidesPerView: 4,
    slidesPerGroup:1,
    grabCursor: true,
    prevButton: '.swiper-prev',
    nextButton: '.swiper-next',
});
$('.index_five .swiper-prev').on('click', function(e){
    e.preventDefault();
    swiperGoodness.swipePrev();
})
$('.index_five .swiper-next').on('click', function(e){
    e.preventDefault();
    swiperGoodness.swipeNext();
})
//判断键盘是否弹出
// var wHeight = window.innerHeight;   //获取初始可视窗口高度
// $(window).resize(function() {         //监测窗口大小的变化事件
//     var hh = window.innerHeight;     //当前可视窗口高度
//     if(wHeight > hh ){           //可以作为虚拟键盘弹出事件
//         $(".footnav").hide();
//     }else{         //可以作为虚拟键盘关闭事件
//         $(".footnav").show();
//     }
// });

$('.leave .exit').click(function(){
    $(".leave").hide();
});
$('#line').click(function(){
    $(".leave").show();
});
$(".leaveForm").Validform({
    btnSubmit:".leave_sbt", 
    tiptype:function(msg){
        layer.alert(msg,{
            icon:0
        });   
    },
    tipSweep:true,
    ajaxPost:true,
    callback:function(o){
        if (o.status == 200) {
                layer.msg(o.message,{
                    icon:6,
                    skin:'layer_mobile_login',
                    time:1200
                });
                setTimeout(function () {  
                    self.location=o.return_url;      
                },10);
            }else if (o.status == 300) {
                layer.msg(o.message,{
                    icon:6,
                    skin:'layer_mobile_login',
                    time:1200
                });
                setTimeout(function () {  
                    history.go(0) ;      
                },1500);    
            }else {
                layer.msg(o.message,{
                    icon:5,
                    skin:'layer_mobile_login',
                    time:1200
                });    
            }    
    }
});
 