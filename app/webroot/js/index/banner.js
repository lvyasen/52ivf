$(function(){
	for (var i=0;i<$(".banner").find(".swiper-slide").length;i++){
		var _img_src = '';
		if(i == 0){
			 _img_src = $(".banner_icon").find(".banner_icon_ul_li_img_active").val();
		} else {
			 _img_src = $(".banner_icon").find(".banner_icon_ul_li_img").val();
		}
		$(".banner_icon_ul").append('<li class="banner_icon_li"><img src="' + _img_src + '" /></li>');
	}
	
	var mySwiper = new Swiper('.banner .swiper-container',{
		mode:'horizontal',
		autoResize:true,
		autoplay:5000,
		speed:700,
		loop: true,
		onSlideChangeEnd:function(){
			var _img_src_active = $(".banner_icon").find(".banner_icon_ul_li_img_active").val();
			var _img_src = $(".banner_icon").find(".banner_icon_ul_li_img").val();
			$(".banner_icon_ul").find("li").each(function(a){
				$(this).find("img").attr("src",_img_src);
				if(mySwiper.activeLoopIndex == a){
					$(this).find("img").attr("src",_img_src_active);
				}
			});
		}
	});
	
	$(".banner_icon_ul").find("li").click(function(){
		mySwiper.swipeTo($(this).index());
	});	
	
})
// $(document).scroll(function() {
// 	if($(document).scrollTop() >= 498){
// 		$(".zf_nav").css({"position":"fixed","left":"0","top":"0","z-index":"1000"});
// 	} else {
// 		$(".zf_nav").css({"position":"relative","left":"0","top":"0","z-index":"0"});
// 	}
// });



