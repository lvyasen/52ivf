// 6大服务
var swiperGoodness = new Swiper('#Goodness', {
    autoplayDisableOnInteraction: false,
    onlyExternal: true,
    loop: true,
    speed: 500,
    autoplay: 4000,
    slidesPerView: 4,
    slidesPerGroup:1,
    grabCursor: true,
    prevButton: '.swiper-prev',
    nextButton: '.swiper-next',
});
$('.goodness .swiper-prev').on('click', function(e){
    e.preventDefault();
    swiperGoodness.swipePrev();
})
$('.goodness .swiper-next').on('click', function(e){
    e.preventDefault();
    swiperGoodness.swipeNext();
})

var swiperBoodness = new Swiper('#Boodness', {
    autoplayDisableOnInteraction: false,
    onlyExternal: true,
    loop: true,
    speed: 100,
    autoplay: 4000,
    slidesPerView: 1,
    slidesPerGroup:2,
    grabCursor: true,
    prevButton: '.swiper-prev',
    nextButton: '.swiper-next',
});
$('.boodness .swiper-prev').on('click', function(e){
    e.preventDefault();
    swiperGoodness.swipePrev();
})
$('.boodness .swiper-next').on('click', function(e){
    e.preventDefault();
    swiperGoodness.swipeNext();
})

