if(typeof audio == 'undefined'){
    var audio = $('.audio_btn')[0];
}
$(audio).bind('ended',play_finish);
// if(!is_weixin()){
//     var is_touch = false;
//     document.addEventListener('touchstart',touch, false); 
//     function touch(){
//         if(is_touch == false){
//             is_touch = true;
//             $('.list_btn')[0].click();
//         }
//     }
// }else{
//     //auto play
//     $(document).ready(function(){
//         $('.list_btn')[0].click();
//     });
// }
$('.list_btn').click(function(){
    var _this = $(this);
    var ele = $(this).closest('.right_btn');
    var hour = $(this).data('hour');
    var minute = $(this).data('minute');
    var second = $(this).data('second');
    var sec = 0;
    if(hour > 0){
        sec = hour * 60 * 60;
    }
    if(minute > 0){
        sec += minute * 60;
    }
    sec += second * 1;
    $('.circle_motion').css({'animation-duration' : sec + 's'});
    audio_ele($(this));
    if(_this.attr('play_btn') == '0'){
        ele.find('.list_btn').each(function(key, elem){
            $(elem).attr('play_btn','0');
            $(elem).attr('src','/images/audio/003.png');
            $(".player").css('background-image','url(/images/audio/001.png)');
        });
        _this.attr('play_btn','1');
        _this.parent("li").siblings('li').attr('src','/images/audio/003.png');
        _this.attr('src','/images/audio/004.png');
        if($('.audio_btn').attr('src') != _this.attr('source')){
            $('.audio_btn').attr('src',(_this.attr('source')));
            $('.circle_motion').addClass('circle_running');
            $('.circle_motion').removeClass('circle_pauser');
            $('.circle_motion').css({'animation-name':'none'});
            var track = window.setInterval(function(){
                $('.circle_motion').css({'animation-name':'track'});
                clearInterval(track);
            },1000);
        }
        audio.play();
        $('.text_btn').text(_this.attr('alt'));
        $('.rotation').css({'webkitAnimationPlayState':'running'});
        $('.right_time').text(minute+':'+second); 
        $(".player").css('background-image','url(/images/audio/001.png)');
        $(".player").attr('mm',_this.attr('mm'));
        $(".player1").css('background-image','url(/images/audio/001.png)');
        $(".player1").attr('mp',_this.attr('mp'));
        audio_t = window.setInterval(function(){
            $('.left_time').text(parseInt(audio.currentTime / 60, 10) +":"+parseInt(audio.currentTime % 60));
        },1000);
    }else{
        $('.rotation').css({'webkitAnimationPlayState':'paused'});
        $('#li_pp').attr('src','/images/audio/003.png')
        _this.attr('src','/images/audio/003.png');
        $(".player").css('background-image','url(/images/audio/002.png)');
        $(".player").attr('play_btn','0');
        $(".player1").css('background-image','url(/images/audio/002.png)');
        $(".player1").attr('play_btn','0');
        _this.attr('play_btn','0');  
        audio.pause();
        clearInterval(audio_t);
        $('.circle_motion').addClass('circle_pauser'); 
        $('.circle_motion').removeClass('circle_running'); 
    }
});
$('.o_btn').click(function(){
    var _this = $(this);
    var ele = $(this).closest('.right_btn');
    var hour = $(this).data('hour');
    var minute = $(this).data('minute');
    var second = $(this).data('second');
    var sec = 0;
    if(hour > 0){
        sec = hour * 60 * 60;
    }
    if(minute > 0){
        sec += minute * 60;
    }
    sec += second * 1;
    $('.circle_motion').css({'animation-duration' : sec + 's'});
    audio_ele($(this));
    if(_this.attr('play_btn') == '0'){
        ele.find('.o_btn').each(function(key, elem){
            $(elem).attr('play_btn','0');
            $(elem).attr('src','/images/audio/003.png');
            $(".player").css('background-image','url(/images/audio/001.png)');
        });
        _this.attr('play_btn','1');
        _this.parent("li").siblings('li').attr('src','/images/audio/003.png');
        _this.attr('src','/images/audio/004.png');
        if($('.audio_btn').attr('src') != _this.attr('source')){
            $('.audio_btn').attr('src',(_this.attr('source')));
            $('.circle_motion').addClass('circle_running');
            $('.circle_motion').removeClass('circle_pauser');
            $('.circle_motion').css({'animation-name':'none'});
            var track = window.setInterval(function(){
                $('.circle_motion').css({'animation-name':'track'});
                clearInterval(track);
            },1000);
        }
        audio.play();
        $('.text_btn').text(_this.attr('alt'));
        $('.rotation').css({'webkitAnimationPlayState':'running'});
        $('.right_time').text(minute+':'+second); 
        $(".player").css('background-image','url(/images/audio/001.png)');
        $(".player").attr('mm',_this.attr('mm'));
        $(".player1").css('background-image','url(/images/audio/001.png)');
        $(".player1").attr('mp',_this.attr('mp'));
        audio_t = window.setInterval(function(){
            $('.left_time').text(parseInt(audio.currentTime / 60, 10) +":"+parseInt(audio.currentTime % 60));
        },1000);
    }else{
        $('.rotation').css({'webkitAnimationPlayState':'paused'});
        $('#li_pp').attr('src','/images/audio/003.png')
        _this.attr('src','/images/audio/003.png');
        $(".player").css('background-image','url(/images/audio/002.png)');
        $(".player").attr('play_btn','0');
        $(".player1").css('background-image','url(/images/audio/002.png)');
        $(".player1").attr('play_btn','0');
        _this.attr('play_btn','0');  
        audio.pause();
        clearInterval(audio_t);
        $('.circle_motion').addClass('circle_pauser'); 
        $('.circle_motion').removeClass('circle_running'); 
    }
});
$('.player1').click(function(){
if($(this).attr('mp')=='0'){
        $('.list_btn')[0].click();
    }
    else{
      $('.list_btn')[$(this).attr('mp')].click();  
    }
});
$('.player').click(function(){
if($(this).attr('mm')=='0'){
        $('.o_btn')[0].click();
    }
    else{
      $('.o_btn')[$(this).attr('mm')].click();  
    }
});

var next_list_btn;
function audio_ele(ele){
    var li_ele = ele.closest('li').next('li');
    if(li_ele.length > 0){
        next_list_btn = li_ele.find('.list_btn');
    }
}
function play_finish(){
    next_list_btn.click();
    var track = window.setInterval(function(){
        $('.circle_motion').css({'animation-name':'track'});
        clearInterval(track);
    },1000);
    $('.rotation').css({'webkitAnimationPlayState':'paused'});
}
function getmatrix(a,b,c,d,e,f){
    var aa=Math.round(180*Math.asin(a)/ Math.PI);
    var bb=Math.round(180*Math.acos(b)/ Math.PI);
    var cc=Math.round(180*Math.asin(c)/ Math.PI);
    var dd=Math.round(180*Math.acos(d)/ Math.PI);
    var deg=0;
    if(aa==bb||-aa==bb){
        deg=dd;
    }else if(-aa+bb==180){
        deg=180+cc;
    }else if(aa+bb==180){
        deg=360-cc||360-dd;
    }
    return deg>=360?0:deg;
    //return (aa+','+bb+','+cc+','+dd);
}