$(document).ready(function(e) {
            
            $('.btn_week').css('color','#ff9a14');
            $('.btn_week').click(function(event) {
                $(this).css('color','#ff9a14');
                $('.btn_total').css('color','#666666')
                $('.rank_total').css({'display':'none'});
                $('.rank_week').css({'display':'block'});
            });
            $('.btn_total').click(function(event) {
                $(this).css('color','#ff9a14');
                $('.btn_week').css('color','#666666')
                $('.rank_week').css({'display':'none'});
                $('.rank_total').css({'display':'block'});
            });
            $('.tab_btn>a').click(function(event) {
                /* Act on the event */
                var index = $(this).index();
                $(this).attr('class','btn_block').siblings('a').attr('class','btn_hidden');
                $('.btncontent').eq(index).show().siblings('.btncontent').hide();
            });
        });