
isMobDevice = (/iphone|ipad|Android|webOS|iPod|BlackBerry|Windows Phone|ZuneWP7/gi).test(navigator.appVersion);
if(!isMobDevice){
    $("img.lazy").lazyload({effect: "fadeIn"});
}else{
    $('img.lazy').each(function(){
        $(this).attr('src',$(this).data('original'));
    });
}
