 
// check used
function getObj(id) {
    return document.getElementById(id);
}
// 检查 email 格式
function IsEmail(strg) {
    var patrn = new RegExp(
            '^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$');
    if (!patrn.test(strg))
        return false;
    return true;
}
// 验证电话
function IsTel(strg) {
    var patrn = new RegExp(
            '^(([0\\+]\\d{2,3}-)?(0\\d{2,3})-)?(\\d{7,8})(-(\\d{3,}))?$');
    if (!patrn.test(strg))
        return false;
    return true;
}
// 验证手机
function IsMobile(strg) {
    var patrn = new RegExp('^(13|15|18)[0-9]{9}$');
    if (!patrn.test(strg))
        return false;
    return true;
}
// 验证邮编
function IsZip(strg) {
    var patrn = new RegExp('^\\d{6}$');
    if (!patrn.test(strg))
        return false;
    return true;
}
// 是否是用户名
function IsUserName(strg) {
    var patrn = new RegExp('^\\w+$');
    if (!patrn.test(strg))
        return false;
    return true;
}  
  
//邀请好友
function copyToClipBoard(txt) {
    if (window.clipboardData) {
        window.clipboardData.clearData();
        window.clipboardData.setData("Text", txt);
    } else if (navigator.userAgent.indexOf("Opera") != -1) {
        //do nothing      
    } else if (window.netscape) {
        try {
            netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
        } catch (e) {
            alert("被浏览器拒绝！\n请在浏览器地址栏输入'about:config'并回车\n然后将 'signed.applets.codebase_principal_support'设置为'true'");
        }
        var clip = Components.classes['@mozilla.org/widget/clipboard;1'].createInstance(Components.interfaces.nsIClipboard);
        if (!clip)
            return;
        var trans = Components.classes['@mozilla.org/widget/transferable;1'].createInstance(Components.interfaces.nsITransferable);
        if (!trans)
            return;
        trans.addDataFlavor('text/unicode');
        var str = new Object();
        var len = new Object();
        var str = Components.classes["@mozilla.org/supports-string;1"].createInstance(Components.interfaces.nsISupportsString);
        var copytext = txt;
        str.data = copytext;
        trans.setTransferData("text/unicode", str, copytext.length * 2);
        var clipid = Components.interfaces.nsIClipboard;
        if (!clip)
            return false;
        clip.setData(trans, null, clipid.kGlobalClipboard);
    }
}

//字符替换
function tpl_replace(str, obj) {
    if (!(Object.prototype.toString.call(str) === '[object String]')) {
        return '';
    }

    // {}, new Object(), new Class()
    // Object.prototype.toString.call(node=document.getElementById("xx")) : ie678 == '[object Object]', other =='[object HTMLElement]'
    // 'isPrototypeOf' in node : ie678 === false , other === true
    if (!(Object.prototype.toString.call(obj) === '[object Object]' && 'isPrototypeOf' in obj)) {
        return str;
    }

    // https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/String/replace
    return str.replace(/\{([^{}]+)\}/g, function (match, key) {
        var value = obj[key];
        return (value !== undefined) ? '' + value : '';
    });
}
    


function PreviewImage(imgFile,pwidth,pid)
{
    var filextension=imgFile.value.substring(imgFile.value.lastIndexOf("."),imgFile.value.length);
    filextension=filextension.toLowerCase();
    if ((filextension!='.jpg')&&(filextension!='.gif')&&(filextension!='.jpeg')&&(filextension!='.png'))
    {
        alert("对不起，系统仅支持标准格式的照片，请您调整格式后重新上传，谢谢 !");
        imgFile.focus();
    }
    else
    {
        var path;
        if(document.all)//IE
        {
            imgFile.select();
            path = document.selection.createRange().text;
            document.getElementById(pid).innerHTML="";
            document.getElementById(pid).style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled='true',sizingMethod='scale',src=\"" + path + "\")";//使用滤镜效果      
        }
        else//FF
        {
            path=window.URL.createObjectURL(imgFile.files[0]);// FF 7.0以上
            //path = imgFile.files[0].getAsDataURL();// FF 3.0
            document.getElementById(pid).innerHTML = "<img id='img1' width='"+pwidth+"' src='"+path+"'/>";
            //document.getElementById("img1").src = path;
        }
    }
}

/*字数限制*/
function checkLength(which,maxLength,returnid) { 
    if(which.value.length > maxLength){
        layer.open({
            icon:2,
            title:'提示框',
            content:'您出入的字数超多限制!',  
        });
        // 超过限制的字数了就将 文本框中的内容按规定的字数 截取
        which.value = which.value.substring(0,maxLength);
        return false;
    }else{
        var curr = maxLength - which.value.length; //200 减去 当前输入的
        document.getElementById(returnid).innerHTML = curr.toString();
        return true;
    }
};

function delFile(id, localid) {  
    $.ajax({  
        url: "/ajax/file/",
        data: {ajaxdata: "del_pic", val: id, ajax: Math.random()},
        async: false, 
        beforeSend: function () {
        },
        dataType: 'json',
        success: function (o) {  
            if(o.status==200){
                layer.msg(o.message,{icon:0,skin:'layer_mobile_login'}); 
                $('#' + localid).remove();
            }
            else
                layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});  
                
        },
        complete: function () {
        },
        error: function () {
        }
    });
        
} 

function adAgree(n,cid,act){
    $(n).removeAttr('onclick'); 
    var ag_num=$(n).html(); 
    layer.closeAll();
    layer.load(1);
    $.ajax({ 
        url: "/ajax/user/",
        data: {ajaxdata: act,val:cid, ajax: Math.random()},
        async: false, 
        beforeSend: function () {
        },
        dataType: 'json',
        success: function (o) { 
            layer.closeAll();
            if (o.status == 4000) { 
                if(o.return_url=="/mobile/login/")
                { 
                    layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
                    setTimeout("window.location.href='/mobile/login/'", 2000);
                }
                else{
                    if(act=='agree_video')
                        window.location.href='/login/';  
                    else
                        $('.login').trigger("click");   
                }  
                $(n).attr('onclick',"adAgree(this,"+cid+",'"+act+"')");  
            }
            else if (o.status == 200) { 
                if(act=='agree_video'){ 
                    ag_num=$("#video_num").html(); 
                    $("#video_num").html(Number(ag_num)+1);  
                }
                else
                    $(n).html(Number(ag_num)+1); 
                 
                $(n).addClass('add_up');
                $(n).append("<em>+1</em>");
                // $(".bottam .add_up em").animate({
                //     top:'-30px',
                //     left:'20px',
                //     fontSize:'18px',
                //     opacity:'0.6'
                // },'slow');
                // $(".bottam .add_up em").animate({
                //     opacity:'0'
                // }); 
                $(n).find("em").animate({
                    top:'-30px',
                    left:'20px',
                    fontSize:'18px',
                    opacity:'0.6'
                },'slow');
                $(n).find("em").animate({
                    opacity:'0'
                }); 
            }
            else{ 
                layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});    
                $(n).attr('onclick',"adAgree(this,"+cid+",'"+act+"')");  
            }
        },
        complete: function () {
        },
        error: function () { 
        }
    });
}