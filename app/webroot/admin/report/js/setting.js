// Custom scripts
$(document).ready(function() {
    function markPage() {
        if ($(".js-ignore-pagemark").length != 0) return;
        var markNavItem = function(item) {
            item.addClass('active');
        }
        var $nav = $('.sidebar-collapse .nav'),
            i = 0,
            anchor;
        if ($nav.length != 0) {
            $nav.find("li").each(function() {
                anchor = $(this).find("a");
                if (window.location.href.indexOf(anchor.attr("href")) != -1 && anchor.attr("href") != "/") {
                    markNavItem($(this));
                    i = 1;
                }
            });
            if (i === 0) {
                markNavItem($nav.find("li").eq(1));
            }
        }
    };
    markPage();

    function fixHeight() {
        var height = $( window ).height();
        $("#page-wrapper").css("min-height", height + 'px');
    }

    $(window).bind("load resize click scroll", function() {
        fixHeight();
    })
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	//报表数据提取//////////////////////////////////////////////////////////////////////////////////////////////
	getXMZT();
	$("#zqzr").hide();
	$("#xmzt_a").click(function(){
		$.ajax({
	        url: LOCAL_HOME+"ajax/other/",
	        data: {ajaxdata: "home",stype: 0,ajax: Math.random()},
	        async: false, 
	        beforeSend: function () {
	        },
	        dataType: 'json',
	        success: function (o) { 
	            if (o.status == 200) {   
					$("#squdao option:first").prop("selected", 'selected');
					$("#zqzr_a").parent("li").removeClass("active");
					$("#xmzt_a").parent("li").addClass("active"); 
					$("#xmzt").show();
					$("#zqzr").hide();
					$("#qudaoDate").html(o.data.qdtime); 
					$("#qudaoNum").html(o.data.qdnum); 
					getXMZT();
	            } else {
	                layer.msg(o.message,{icon:0,time:1000}); 
	            } 
	        },
	        complete: function () {
	        },
	        error: function () { 
	        }
	    });
	});
	$("#zqzr_a").click(function(){  
		$.ajax({
	        url: LOCAL_HOME+"ajax/other/",
	        data: {ajaxdata: "home",stype: 1,ajax: Math.random()},
	        async: false, 
	        beforeSend: function () {
	        },
	        dataType: 'json',
	        success: function (o) { 
	            if (o.status == 200) {  
					$("#squdao option:first").prop("selected", 'selected');
					$("#xmzt_a").parent("li").removeClass("active");
					$("#zqzr_a").parent("li").addClass("active"); 
					$("#xmzt").hide();
					$("#zqzr").show(); 
					$("#userRegDate").html(o.data.qdtime); 
					$("#userRegNum").html(o.data.qdnum); 
					getZQZR();
	            } else {
	                layer.msg(o.message,{icon:0,time:1000}); 
	            } 
	        },
	        complete: function () {
	        },
	        error: function () { 
	        }
	    });
	});
	
	getButtonTop10();
	//融资期限分布
	getRZQXFB();
	//产品类型分布 - 数据//////////////////////////////////////////////////////////////////////////////////////////////
	getCPLXFB();
	//融资主体行业分布 - 数据//////////////////////////////////////////////////////////////////////////////////////////////
	getRZZTHYFB();
	
	//平台保证金变化 - 数据//////////////////////////////////////////////////////////////////////////////////////////////
	getPTBZJBH();
	//投资人年龄/性别分布 - 数据//////////////////////////////////////////////////////////////////////////////////////////////
	getTZRNL_XBFB();
	//投资额渠道分布 - 数据//////////////////////////////////////////////////////////////////////////////////////////////
	getTZEQDFB();
	reportMap();
});
function uptSource(obj){
	var stype=0;
	if($("#zqzr_a").parent("li").hasClass("active"))
		stype=1;
	$.ajax({
        url: LOCAL_HOME+"ajax/other/",
        data: {ajaxdata: "home",stype: stype,qudao:obj,ajax: Math.random()},
        async: false, 
        beforeSend: function () {
        },
        dataType: 'json',
        success: function (o) { 
            if (o.status == 200) {  
            	if(stype==0){
					$("#qudaoDate").html(o.data.qdtime); 
					$("#qudaoNum").html(o.data.qdnum); 
					getXMZT();
            	}
            	else{ 
					$("#userRegDate").html(o.data.qdtime); 
					$("#userRegNum").html(o.data.qdnum); 
					getZQZR();
            	}
            } else {
                layer.msg(o.message,{icon:0,time:1000}); 
            } 
        },
        complete: function () {
        },
        error: function () { 
        }
    });
}
function reportMap(){
	 var myChart = echarts.init(document.getElementById('report_map'));
option = {
    title : {
        text: 'iphone销量',
        subtext: '纯属虚构',
        x:'center'
    },
    tooltip : {
        trigger: 'item'
    },
    legend: {
        orient: 'vertical',
        x:'left',
        data:['iphone3','iphone4','iphone5']
    },
    dataRange: {
        min: 0,
        max: 2500,
        x: 'left',
        y: 'bottom',
        text:['高','低'],           // 文本，默认为数值文本
        calculable : true
    },
    toolbox: {
        show: true,
        orient : 'vertical',
        x: 'right',
        y: 'center',
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    roamController: {
        show: true,
        x: 'right',
        mapTypeControl: {
            'china': true
        }
    },
    series : [
        {
            name: 'iphone3',
            type: 'map',
            mapType: 'china',
            roam: false,
            itemStyle:{
                normal:{label:{show:true}},
                emphasis:{label:{show:true}}
            },
            data:[
                {name: '北京',value: Math.round(Math.random()*1000)},
                {name: '天津',value: Math.round(Math.random()*1000)},
                {name: '上海',value: Math.round(Math.random()*1000)},
                {name: '重庆',value: Math.round(Math.random()*1000)},
                {name: '河北',value: Math.round(Math.random()*1000)},
                {name: '河南',value: Math.round(Math.random()*1000)},
                {name: '云南',value: Math.round(Math.random()*1000)},
                {name: '辽宁',value: Math.round(Math.random()*1000)},
                {name: '黑龙江',value: Math.round(Math.random()*1000)},
                {name: '湖南',value: Math.round(Math.random()*1000)},
                {name: '安徽',value: Math.round(Math.random()*1000)},
                {name: '山东',value: Math.round(Math.random()*1000)},
                {name: '新疆',value: Math.round(Math.random()*1000)},
                {name: '江苏',value: Math.round(Math.random()*1000)},
                {name: '浙江',value: Math.round(Math.random()*1000)},
                {name: '江西',value: Math.round(Math.random()*1000)},
                {name: '湖北',value: Math.round(Math.random()*1000)},
                {name: '广西',value: Math.round(Math.random()*1000)},
                {name: '甘肃',value: Math.round(Math.random()*1000)},
                {name: '山西',value: Math.round(Math.random()*1000)},
                {name: '内蒙古',value: Math.round(Math.random()*1000)},
                {name: '陕西',value: Math.round(Math.random()*1000)},
                {name: '吉林',value: Math.round(Math.random()*1000)},
                {name: '福建',value: Math.round(Math.random()*1000)},
                {name: '贵州',value: Math.round(Math.random()*1000)},
                {name: '广东',value: Math.round(Math.random()*1000)},
                {name: '青海',value: Math.round(Math.random()*1000)},
                {name: '西藏',value: Math.round(Math.random()*1000)},
                {name: '四川',value: Math.round(Math.random()*1000)},
                {name: '宁夏',value: Math.round(Math.random()*1000)},
                {name: '海南',value: Math.round(Math.random()*1000)},
                {name: '台湾',value: Math.round(Math.random()*1000)},
                {name: '香港',value: Math.round(Math.random()*1000)},
                {name: '澳门',value: Math.round(Math.random()*1000)}
            ]
        },
        {
            name: 'iphone4',
            type: 'map',
            mapType: 'china',
            itemStyle:{
                normal:{label:{show:true}},
                emphasis:{label:{show:true}}
            },
            data:[
                {name: '北京',value: Math.round(Math.random()*1000)},
                {name: '天津',value: Math.round(Math.random()*1000)},
                {name: '上海',value: Math.round(Math.random()*1000)},
                {name: '重庆',value: Math.round(Math.random()*1000)},
                {name: '河北',value: Math.round(Math.random()*1000)},
                {name: '安徽',value: Math.round(Math.random()*1000)},
                {name: '新疆',value: Math.round(Math.random()*1000)},
                {name: '浙江',value: Math.round(Math.random()*1000)},
                {name: '江西',value: Math.round(Math.random()*1000)},
                {name: '山西',value: Math.round(Math.random()*1000)},
                {name: '内蒙古',value: Math.round(Math.random()*1000)},
                {name: '吉林',value: Math.round(Math.random()*1000)},
                {name: '福建',value: Math.round(Math.random()*1000)},
                {name: '广东',value: Math.round(Math.random()*1000)},
                {name: '西藏',value: Math.round(Math.random()*1000)},
                {name: '四川',value: Math.round(Math.random()*1000)},
                {name: '宁夏',value: Math.round(Math.random()*1000)},
                {name: '香港',value: Math.round(Math.random()*1000)},
                {name: '澳门',value: Math.round(Math.random()*1000)}
            ]
        },
        {
            name: 'iphone5',
            type: 'map',
            mapType: 'china',
            itemStyle:{
                normal:{label:{show:true}},
                emphasis:{label:{show:true}}
            },
            data:[
                {name: '北京',value: Math.round(Math.random()*1000)},
                {name: '天津',value: Math.round(Math.random()*1000)},
                {name: '上海',value: Math.round(Math.random()*1000)},
                {name: '广东',value: Math.round(Math.random()*1000)},
                {name: '台湾',value: Math.round(Math.random()*1000)},
                {name: '香港',value: Math.round(Math.random()*1000)},
                {name: '澳门',value: Math.round(Math.random()*1000)}
            ]
        }
    ]
};
                    
                    
        myChart.setOption(option);
}
//渠道UV - 数据//////////////////////////////////////////////////////////////////////////////////////////////
function getXMZT(){
	var qudaoNum=$("#qudaoNum").html(); 
	var qudaoDate=$("#qudaoDate").html(); 
	var xiangmuzhitou_data = {
		labels : qudaoDate.split(","),
		datasets : [
			{
				fillColor : "rgba(79,193,233,1)",//柱状填充颜色
				strokeColor : "rgba(79,193,233,1)",//柱状边框颜色
				data : qudaoNum.split(",")
			}
		]
	}
	var defaults = {
		// 刻度是否显示标签, 即Y轴上是否显示文字
		scaleShowLabels : true,
		// Y轴上的刻度,即文字
		scaleLabel : "<%=value/1000%>千",
		// 文字大小
		scaleFontSize : 14,
	}
	var xiangmuzhitou_ctx = document.getElementById("xiangmuzhitou").getContext("2d");
	var xiangmuzhitou = new Chart(xiangmuzhitou_ctx).Bar(xiangmuzhitou_data,defaults);
}
//用户注册 - 数据//////////////////////////////////////////////////////////////////////////////////////////////
function getZQZR(){
	var userRegNum=$("#userRegNum").html(); 
	var userRegDate=$("#userRegDate").html(); 
	var zhaiquanzhuanrang_data = {
		labels : userRegDate.split(","),
		datasets : [
			{
				fillColor : "rgba(79,193,233,1)",//柱状填充颜色
				strokeColor : "rgba(79,193,233,1)",//柱状边框颜色
				data : userRegNum.split(",")
			}
		]
	}
	var defaults = {
		// 刻度是否显示标签, 即Y轴上是否显示文字
		scaleShowLabels : true,
		// Y轴上的刻度,即文字
		scaleLabel : "<%=value/100%>百",
		// 文字大小
		scaleFontSize : 14,
	}
	var zhaiquanzhuanrang_ctx = document.getElementById("zhaiquanzhuanrang").getContext("2d");
	var zhaiquanzhuanrang = new Chart(zhaiquanzhuanrang_ctx).Bar(zhaiquanzhuanrang_data,defaults);
}

function getButtonTop10(){
	var myChart = echarts.init(document.getElementById('baozhangfangshifenbu'));
	option = {
    title : {
        text: '某站点用户访问来源',
        subtext: '纯属虚构',
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        left: 'left',
        data: ['直接访问','邮件营销','联盟广告','视频广告','搜索引擎']
    },
    series : [
        {
            name: '访问来源',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:335, name:'直接访问'},
                {value:310, name:'邮件营销'},
                {value:234, name:'联盟广告'},
                {value:135, name:'视频广告'},
                {value:1548, name:'搜索引擎'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};
    myChart.setOption(option);
}
//button点击 - 数据//////////////////////////////////////////////////////////////////////////////////////////////
function getBZFSFB(){
	var buttonClick=$("#buttonClick").html(); 
	buttonClick = eval("(" + buttonClick + ")");
	var baozhangfangshifenbu_data = buttonClick;
	var defaults = {
    // 块和块之间是否有间距
	segmentShowStroke : true,
	// 块和块之间间距的颜色
	segmentStrokeColor : "#fff",
	// 块和块之间间距的宽度
    segmentStrokeWidth : 1,
    // 是否有动画效果	
	animation : true,
	// 动画的步数
	animationSteps : 50,
	// 动画的效果
	animationEasing : "easeOutBounce",	
　　// 是否有从0度到360度的动画
    animateRotate : true,
　　// 是否有从中心到边缘的动画
    animateScale : true,
	// 动画完成后调用
	onAnimationComplete : null
	}
	var baozhangfangshifenbu_ctx = document.getElementById("baozhangfangshifenbu").getContext("2d");
	var baozhangfangshifenbu = new Chart(baozhangfangshifenbu_ctx).Doughnut(baozhangfangshifenbu_data,defaults);
}
//产品申请 - 数据//////////////////////////////////////////////////////////////////////////////////////////////
function getRZQXFB(){
	var prodsApply=$("#prodsApply").html(); 
	prodsApply = eval("(" + prodsApply + ")");
	var rongziqixianfenbu_data = prodsApply;
	var defaults = {
    // 块和块之间是否有间距
	segmentShowStroke : true,
	// 块和块之间间距的颜色
	segmentStrokeColor : "#fff",
	// 块和块之间间距的宽度
    segmentStrokeWidth : 1,
    // 是否有动画效果	
	animation : true,
	// 动画的步数
	animationSteps : 50,
	// 动画的效果
	animationEasing : "easeOutBounce",	
　　// 是否有从0度到360度的动画
    animateRotate : true,
　　// 是否有从中心到边缘的动画
    animateScale : true,
	// 动画完成后调用
	onAnimationComplete : null
	}
	var rongziqixianfenbu_ctx = document.getElementById("rongziqixianfenbu").getContext("2d");
	var rongziqixianfenbu = new Chart(rongziqixianfenbu_ctx).Doughnut(rongziqixianfenbu_data,defaults);
}
//渠道uv - 数据//////////////////////////////////////////////////////////////////////////////////////////////
function getCPLXFB(){
	var qudaoUvNum=$("#qudaoUvNum").html(); 
	qudaoUvNum = eval("(" + qudaoUvNum + ")");
	var chanpinleixingfenbu_data =qudaoUvNum;
	var defaults = {
    // 块和块之间是否有间距
	segmentShowStroke : true,
	// 块和块之间间距的颜色
	segmentStrokeColor : "#fff",
	// 块和块之间间距的宽度
    segmentStrokeWidth : 1,
    // 是否有动画效果	
	animation : true,
	// 动画的步数
	animationSteps : 50,
	// 动画的效果
	animationEasing : "easeOutBounce",	
　　// 是否有从0度到360度的动画
    animateRotate : true,
　　// 是否有从中心到边缘的动画
    animateScale : true,
	// 动画完成后调用
	onAnimationComplete : null
	}
	var chanpinleixingfenbu_ctx = document.getElementById("chanpinleixingfenbu").getContext("2d");
	var chanpinleixingfenbu = new Chart(chanpinleixingfenbu_ctx).Doughnut(chanpinleixingfenbu_data,defaults);
}
//渠道注册 - 数据//////////////////////////////////////////////////////////////////////////////////////////////
function getRZZTHYFB(){
	var qudaoRegNum=$("#qudaoRegNum").html(); 
	qudaoRegNum = eval("(" + qudaoRegNum + ")");
	var rongzizhutihangyefenbu_data = qudaoRegNum ;
	var defaults = {
    // 块和块之间是否有间距
	segmentShowStroke : true,
	// 块和块之间间距的颜色
	segmentStrokeColor : "#fff",
	// 块和块之间间距的宽度
    segmentStrokeWidth : 1,
    // 是否有动画效果	
	animation : true,
	// 动画的步数
	animationSteps : 50,
	// 动画的效果
	animationEasing : "easeOutBounce",	
　　// 是否有从0度到360度的动画
    animateRotate : true,
　　// 是否有从中心到边缘的动画
    animateScale : true,
	// 动画完成后调用
	onAnimationComplete : null
	}
	var rongzizhutihangyefenbu_ctx = document.getElementById("rongzizhutihangyefenbu").getContext("2d");
	var rongzizhutihangyefenbu = new Chart(rongzizhutihangyefenbu_ctx).Doughnut(rongzizhutihangyefenbu_data,defaults);
}
//平台保证金变化 - 数据//////////////////////////////////////////////////////////////////////////////////////////////
function getPTBZJBH(){
	var pingtaibaozhengjinbianhua_data = {
		labels : ["2014-03","2014-04","2014-05","2014-06","2014-07","2014-08","2014-09","2014-10","2014-11","2014-12","2015-01","2015-02","2015-03","2015-04","2015-05","2015-06","2015-07"],
		datasets : [
			{
				fillColor : "rgba(79,193,233,0.5)",//柱状填充颜色
				strokeColor : "rgba(79,193,233,1)",//柱状边框颜色
				pointColor : "rgba(79,193,233,1)",
				pointStrokeColor : "#fff",
				data : [28000,31000000,34000000,48000000,40000000,19000000,56000000,96000000,65000000,27000000,100000000,105000000,139000000,140000000,201000000,140000000,201000000]
			}
		]
	}
	var defaults = {
		// 刻度是否显示标签, 即Y轴上是否显示文字
		scaleShowLabels : true,
		// Y轴上的刻度,即文字
		scaleLabel : "<%=value/10000%>万",
		// 文字大小
		scaleFontSize : 14,
	}
	var pingtaibaozhengjinbianhua_ctx = document.getElementById("pingtaibaozhengjinbianhua").getContext("2d");
	var pingtaibaozhengjinbianhua = new Chart(pingtaibaozhengjinbianhua_ctx).Line(pingtaibaozhengjinbianhua_data,defaults);
}
//投资人年龄/性别分布 - 数据//////////////////////////////////////////////////////////////////////////////////////////////
function getTZRNL_XBFB(){
	var touzirennianling_xingbiefenbu_data = {
		labels : ["0-19","20-29","30-39","40-49","50-59","60+"],
		datasets : [
			{
				fillColor : "rgba(79,193,233,0.5)",//柱状填充颜色
				strokeColor : "rgba(79,193,233,1)",//柱状边框颜色
				pointColor : "rgba(79,193,233,1)",
				pointStrokeColor : "#fff",
				data : [65,59,90,81,56,55]
			},
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,1)",
				pointColor : "rgba(151,187,205,1)",
				pointStrokeColor : "#fff",
				data : [28,48,40,19,96,27,100]
			}
		]
	}
	var touzirennianling_xingbiefenbu_ctx = document.getElementById("touzirennianling_xingbiefenbu").getContext("2d");
	var touzirennianling_xingbiefenbu = new Chart(touzirennianling_xingbiefenbu_ctx).Radar(touzirennianling_xingbiefenbu_data);
}
//投资额渠道分布 - 数据//////////////////////////////////////////////////////////////////////////////////////////////
function getTZEQDFB(){		
	var touziequdaofenbu_data = [
	{
		value: 30,
		color:"#4FC1E9"
	},
	{
		value : 50,
		color : "#F89135"
	},
	{
		value : 60,
		color : "#F8585B"
	}			
	]
	var touziequdaofenbu_ctx = document.getElementById("touziequdaofenbu").getContext("2d");
	var touziequdaofenbu = new Chart(touziequdaofenbu_ctx).Pie(touziequdaofenbu_data);
}

//