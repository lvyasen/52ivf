// Custom scripts
$(document).ready(function() { 
    createDay();  
    getYHQD();  
    getTop10Func('渠道UV Top10','qudaoUvstop10','qudaoUvName','qudaoUvDatas');    
    getTop10Func('渠道注册量 Top10','qudaoregtop10','qudaoRegName','qudaoRegData');   
    $("#seachData").click(function(){ 
        var stime=$("#start8").val(); 
        var etime=$("#end8").val();  
        $.ajax({
            url: "/adminajax/other_get/", 
            data: {ajaxdata: "num_data",stime: stime,etime : etime,ajax: Math.random()},
            async: false, 
            beforeSend: function () {
            },
            dataType: 'json',
            success: function (o) { 
                if (o.status == 200) {    
                    $("#totaluv").html(o.data.totaluv); 
                    $("#regUser").html(o.data.regUser);  
                    $("#txUser").html(o.data.txUser);  
                } else {
                    layer.msg(o.message,{icon:0,time:1000}); 
                } 
            },
            complete: function () {
            },
            error: function () { 
            }
        });
    });
    $("#seachData1").click(function(){
        var startTime=$("#start").val(); 
        var qudao=$("#squdao").val();  
            $.ajax({
                url: "/adminajax/other_get/",
                data: {ajaxdata: "home",time: startTime,qudao: qudao,ajax: Math.random()},
                async: false, 
                beforeSend: function () {
                },
                dataType: 'json',
                success: function (o) { 
                    if (o.status == 200) {    
                        $("#qudaoUvData").html(o.data.qddata); 
                        $("#qudaoregData").html(o.data.regData); 
                        $("#applyProdsData").html(o.data.prodData);  
                        $("#uvtimeData").html(o.data.timeData);  
                        getYHQD();
                    } else {
                        layer.msg(o.message,{icon:0,time:1000}); 
                    } 
                },
                complete: function () {
                },
                error: function () { 
                }
            });
        });  
    $("#seachData4").click(function(){
        var stime=$("#start3").val(); 
        var etime=$("#end3").val();  
        var qqudao=$("#qqudao").val();   
            $.ajax({
                url: "/adminajax/other_get/",
                data: {ajaxdata: "qudaouvtop10",qudao: qqudao,stime: stime,etime : etime,ajax: Math.random()},
                async: false, 
                beforeSend: function () {
                },
                dataType: 'json',
                success: function (o) { 
                    if (o.status == 200) {   
                        $("#qudaoUvName").html(o.data.clickname); 
                        $("#qudaoUvDatas").html(o.data.clickData);  
                        getTop10Func('渠道UV Top10','qudaoUvstop10','qudaoUvName','qudaoUvDatas');  
                    } else {
                        layer.msg(o.message,{icon:0,time:1000}); 
                    } 
                },
                complete: function () {
                },
                error: function () { 
                }
            });
        });
    $("#seachData5").click(function(){
        var stime=$("#start4").val(); 
        var etime=$("#end4").val();  
        var zqudao=$("#zqudao").val();  
            $.ajax({
                url: "/adminajax/other_get/",
                data: {ajaxdata: "qudaoregtop10",qudao: zqudao,stime: stime,etime : etime,ajax: Math.random()},
                async: false, 
                beforeSend: function () {
                },
                dataType: 'json',
                success: function (o) { 
                    if (o.status == 200) {   
                        $("#qudaoRegName").html(o.data.clickname); 
                        $("#qudaoRegData").html(o.data.clickData);  
                        getTop10Func('渠道注册量 Top10','qudaoregtop10','qudaoRegName','qudaoRegData');   
                    } else {
                        layer.msg(o.message,{icon:0,time:1000}); 
                    } 
                },
                complete: function () {
                },
                error: function () { 
                }
            });
        });
 
    $("#seachData6").click(function(){
        var startTime=$("#start5").val();  
        var s=$("#seller").val();  
            $.ajax({
                url: "/adminajax/other_get/",
                data: {ajaxdata: "seller",time: startTime,s: s,ajax: Math.random()},
                async: false, 
                beforeSend: function () {
                },
                dataType: 'json',
                success: function (o) { 
                    if (o.status == 200) {    
                        $("#yongjinData").html(o.data.money); 
                        $("#xiakuanData").html(o.data.amount); 
                        $("#xiayouuserRegData").html(o.data.regData);  
                        $("#xiayouuserApplyData").html(o.data.applyData);  
                        $("#xiayouuserApplyThreeData").html(o.data.applyThreeData);  
                        $("#xiayouuserUVData").html(o.data.uvData);  
                        $("#xiayouuserFanKuanData").html(o.data.fanKuanData);  
                        $("#addTimeData").html(o.data.timeData);   
                        getSHJS();
                    } else {
                        layer.msg(o.message,{icon:0,time:1000}); 
                    } 
                },
                complete: function () {
                },
                error: function () { 
                }
            });
        });
});   
var daysOfMonth = [];
var days = [];
function createMonthDay() {
    var fullYear = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    var lastDayOfMonth = new Date(fullYear, month, 0).getDate();
    for (var i = 1; i <= lastDayOfMonth; i++) {
        daysOfMonth.push(i+ '日');
    }; 
}

function createDay() { 
    for (var i = 1; i <= 31; i++) {
        days.push(i+ '日');
    }; 
}

function getYHQD(){
    var uvAttr=$("#qudaoUvData").html(); 
    var regAttr=$("#qudaoregData").html(); 
    var applyAttr=$("#applyProdsData").html(); 
    var uvtimeData=$("#uvtimeData").html(); 
    uvAttr = eval("(" + uvAttr + ")"); 
    regAttr = eval("(" + regAttr + ")");
    applyAttr = eval("(" + applyAttr + ")");
    uvtimeData = eval("(" + uvtimeData + ")");
    var myChart = echarts.init(document.getElementById('shujuqushi'));
    option = { 
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data:['每日新增用户', '每日预约数', '每日UV']
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    toolbox: {
        feature: {
            saveAsImage: {}
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: uvtimeData
    },
    yAxis: {
        type: 'value'
    },
    series: [
        {
            name:'每日新增用户',
            type:'line',
            stack: '总量',
            data:regAttr
        },
        {
            name:'每日预约数',
            type:'line',
            stack: '总量', 
            itemStyle : {
                normal : {
                  color:'#e2c015',
                  lineStyle:{
                    color:'#e2c015'
                  }
                }
              }, 
            data:applyAttr
        },  
        {
            name:'每日UV',
            type:'line',
            stack: '总量',
            itemStyle : {
                normal : {
                  color:'#15e2bc',
                  lineStyle:{
                    color:'#15e2bc'
                  }
                }
              }, 
            data:uvAttr
        }
    ]
};

    myChart.setOption(option);
} 
 

//饼状图
function getTop10Func(title,id,nameAttr,valueAttr){ 
    var nameAttr=$("#"+nameAttr).html(); 
    var valueAttr=$("#"+valueAttr).html(); 
    buttonClick = eval("(" + nameAttr + ")"); 
    buttonClickData = eval("(" + valueAttr + ")");  
    var myChart = echarts.init(document.getElementById(id));
        option = { 
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        toolbox: {
            feature: {
                saveAsImage: {}
            }
        },
        legend: {
            orient: 'vertical',
            left: 'left',
            data: buttonClick
        },
        series : [
            {
                name: title,
                type: 'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:buttonClickData,
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    };
    myChart.setOption(option);
}  
 