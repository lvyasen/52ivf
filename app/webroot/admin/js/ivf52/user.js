// JavaScript Document
var pwd_isok = 0;
var email_isok = 0; 

var _user = {
    emailExp: new RegExp("^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$"),
    usernameExp: new RegExp("^(?!_)(?!.*?_$)[a-zA-Z0-9_]{6,20}$"), //(\u4e00-\u9fa5)
    passwordExp: new RegExp("^[a-zA-Z0-9]{6,20}$"),
    mobileExp: new RegExp("^[1][3-8]\\d{9}$"),  
    chkpwd: function (n) {
        if (!n) { 
            layer.alert("密码不能为空！",{ title: '提示框', icon:0,}); 
            pwd_isok = 0;
        } else {
            if (this.passwordExp.test(n)) {
                pwd_isok = 1;
            } else { 
                layer.alert("您输入的密码格式不正确！(6-20字母或数字)",{ title: '提示框', icon:0,}); 
                pwd_isok = 0; 
            }
        }
    },
    chkemail: function (n) {
        if (!n) {
            layer.alert("邮箱不能为空！",{title: '提示框', icon:0,}); 
            email_isok = 0;
        } else {
            if (this.emailExp.test(n)) {
                email_isok = 1;
            } else {
                layer.alert("邮箱格式不对！",{title: '提示框', icon:0,}); 
                email_isok = 0;
            }
        }
    },  
    uptPwd: function (n) { 
        layer.open({
          type: 1,
          title:'修改密码',
          area: ['300px','300px'],
          shadeClose: true,
          content: $('#change_Pass'),
          btn:['确认修改'],
          yes:function(index, layero){    
            var num=0;   
            $("input[type$='password']").each(function(b){
               if($(this).val()=="")
                { 
                   layer.alert($(this).attr("data-name")+"不能为空！",{ title: '提示框', icon:0,}); 
                   num++;
                   return false;            
               } 
               _user.chkpwd($(this).val()); 
                if (!pwd_isok) {
                    num++; 
                    return false; 
                }
            });
            if(num>0){  return false;} 
            if(!$("#c_mew_pas").val || $("#c_mew_pas").val() != $("#Nes_pas").val())
             {
                layer.alert('密码不一致!',{title: '提示框', icon:0,});
                return false;
            }   
            else{   
                var options = {
                    beforeSubmit: function () { },
                    dataType: 'json',
                    success: function (o) {
                        if (o.status == 200) {
                            layer.alert(o.message,{title: '提示框', icon:1,});  
                            layer.close(index);  
                        } else {
                            layer.alert(o.message,{title: '提示框', icon:0,});   
                        }     
                        return false;
                    }
                };
                $('#userForm').ajaxSubmit(options);
            }  
        }
        });    
    }, 
    //登录
    Login: function (n) { 
        var num=0;  
        $("input[type$='text'],input[type$='password']").each(function(n){
            if($(this).val()=="")
            { 
                layer.alert($(this).attr("data-name")+"不能为空！",{title: '提示框', icon:0,});  
                num++;
                return false;            
            }
            if($(this).attr("data-name")=="邮箱")
            {
                _user.chkemail($(this).val()); 
                if (!email_isok) {
                    num++; 
                    return false; 
                }
            } 
        });
        if(num>0){  return false;}  
        $(n).removeAttr('onclick');
        var options = {
            beforeSubmit: function () { },
            dataType: 'json',
            success: function (o) {
                if (o.status == 200) {
                    layer.alert(o.message,{title: '提示框', icon:1,}); 
                    setTimeout("window.location.href='"+o.return_url+"'", 500);
                } else {
                    layer.alert(o.message,{title: '提示框', icon:0,});  
                    $(n).attr('onclick',"_user.Login(this)");
                } 
                return false;
            }
        };
        $('#loginForm').ajaxSubmit(options);
        return false;       
    } 
};