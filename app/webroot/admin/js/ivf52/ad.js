var _ad = {   
    buildQrcode: function (obj,url) {  
        $(obj).removeAttr('onclick');
        $.ajax({
            url: LOCAL_HOME+"ajax/toggle/",
            data: {ajaxdata: "qrcode",id:1,val:url,ajax: Math.random()},
            async: false, 
            beforeSend: function () {
            },
            dataType: 'json',
            success: function (o) {
                if (o.status == 200) {
                    $("#spreadurl").val(url); 
                    $("#tg_qrcode").html(o.message); 
                    layer.open({
                        type: 1,
                        shade: false,
                        title: false, //不显示标题
                        area : ['600px' , '400px'],
                        content:$('#add_columns_style')
                    });
                } else { 
                    layer.msg(o.message,{icon:0,time:2000});   
                } 
                $(obj).attr('onclick',"_channel.buildQrcode(this,'"+url+"')"); 
            },
            complete: function () {
            },
            error: function (o) {  
                var str = JSON.stringify(o);    
                if(o.responseText!= undefined && o.responseText=="no access")
                { 
                    layer.msg("无此权限！",{icon:0,time:2000}); 
                    $(obj).attr('onclick',"_channel.buildQrcode(this,"+url+")"); 
                } 
            }
        });  
    }, 
    uptVideoCate: function () {   
        $('.dd').nestable();                                
        $('.dd-handle #edit').on('mousedown', function(e){
            e.stopPropagation();
        });
        $('.dd').on('change', function() {  
            var r = $('.dd').nestable('serialize'); 
            $.ajax({
                url: "/adminajax/other/",
                type:'POST',
                data: {ajaxdata: "video_cate",val:JSON.stringify(r),ajax: Math.random()},
                async: false, 
                beforeSend: function () {
                },
                dataType: 'json',
                success: function (o) {
                    if (o.status != 200) {
                        layer.msg(o.message,{icon:0,time:2000});   
                    }  
                }
            }); 
        });  
    },
    uptCate: function () {   
        $('.dd').nestable();                                
        $('.dd-handle #edit').on('mousedown', function(e){
            e.stopPropagation();
        });
        $('.dd').on('change', function() {  
            var r = $('.dd').nestable('serialize'); 
            $.ajax({
                url: "/adminajax/other/",
                type:'POST',
                data: {ajaxdata: "cate",val:JSON.stringify(r),ajax: Math.random()},
                async: false, 
                beforeSend: function () {
                },
                dataType: 'json',
                success: function (o) {
                    if (o.status != 200) {
                        layer.msg(o.message,{icon:0,time:2000});   
                    }  
                }
            }); 
        });  
    }
};