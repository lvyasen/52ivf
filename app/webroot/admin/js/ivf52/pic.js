
var _pic = {
    DeleteFile: function (id, localid) {  
        $.ajax({  
            url: "/adminajax/file/",
            data: {ajaxdata: "del_pic", val: id, ajax: Math.random()},
            async: false, 
            beforeSend: function () {
            },
            dataType: 'json',
            success: function (o) {  
                if(o.status==200){
                    layer.msg(o.message,{icon:1,time:1000}); 
					$('#' + localid).remove();
                }
				else
                    layer.msg(o.message,{icon:0,skin:'layer_mobile_login'});  
					
            },
            complete: function () {
            },
            error: function () {
            }
        });
		
    } 
};