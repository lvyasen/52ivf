
$(function(){
    /*******滚动条*******/
    $("body").niceScroll({  
        cursorcolor:"#888888",  
        cursoropacitymax:1,  
        touchbehavior:false,  
        cursorwidth:"5px",  
        cursorborder:"0",  
        cursorborderradius:"5px"  
    });
});

function checkAll(obj, chk)
{ 
    if (chk == null) 
        chk = 'checkboxes'; 
    if(obj.checked)    
        $('input[name="'+chk+'"]').prop("checked",true);
    else                  
        $('input[name="'+chk+'"]').prop("checked",false);   
}
/*字数限制*/
function checkLength(which,maxLength,returnid) { 
    if(which.value.length > maxLength){
        layer.open({
            icon:2,
            title:'提示框',
            content:'您出入的字数超多限制!',  
        });
        // 超过限制的字数了就将 文本框中的内容按规定的字数 截取
        which.value = which.value.substring(0,maxLength);
        return false;
    }else{
        var curr = maxLength - which.value.length; //200 减去 当前输入的
        document.getElementById(returnid).innerHTML = curr.toString();
        return true;
    }
};  
function jsCopy(content){
     var e=document.getElementById(content);//对象是contents   
    e.select(); //选择对象   
    tag=document.execCommand("Copy"); //执行浏览器复制命令  
    if(tag){    
        layer.msg('复制内容成功',{icon:1,time:2000});  
    }
    else 
        layer.msg('该浏览器不支持一键复制！请手工复制文本框链接地址～',{icon:0,time:2000});   
}  
/**
 * 切换状态
 */ 
function toggle(obj, act, id)
{ 
  var val = (obj.className.match(/yesimg/i)) ? 0 : 1;    
    $.ajax({ 
            url: "/adminajax/toggle/", 
            data: {ajaxdata: act,val: val,id:id,ajax: Math.random()},
            async: false, 
            beforeSend: function () {
            },
            dataType: 'json',
            success: function (o) {  
                if (o.status == 4000) {
                    layer.msg(o.message,{icon:0,time:2000});  
                }else if (o.status == 200) {    
                    obj.className = (o.data.res > 0) ? 'yesimg' : 'noimg';
                } else { 
                    layer.msg("<span style='color:red;'>" + o.message + "</span>",{icon:0,time:1000});  
                }
            },
            complete: function () {
            },
            error: function () {
            }
    });
}


function toggerUser(t,divId){
    $.ajax({ 
        url: "/adminajax/other_get/", 
        data: {ajaxdata: 'setUser', val: t, ajax: Math.random()},
        async: false, 
        beforeSend: function () {
        },
        dataType: 'json',
        success: function (o) { 
            if(o.status==200){  
                $("#"+divId+" option[value='"+o.message+"']").attr("selected","selected");   
            }
            else{ 
                layer.alert(o.message,{icon:0,time:1000});   
            }
        },
        complete: function () {
        },
        error: function () {
        }
    });
}

/**
 * 删除
 */ 
function delAct(obj, act, ids,type,ys)
{  
    var ys = arguments[4] ? arguments[4] : 'tr';
    layer.confirm('确认要删除吗？',{icon:0,},function(index){ 
        $(obj).removeAttr('onclick');
        $.ajax({  
            url: "/adminajax/del/",
            data: {ajaxdata:act,type: "single",rid:ids,ajax: Math.random()}, 
            async: false, 
            beforeSend: function () {
            },
            dataType: 'json',
            success: function (o) { 
                if (o.status == 4000) {
                    layer.msg(o.message,{icon:0,time:2000});  
                }else if (o.status == 200) {  
                    $(obj).parents(ys).remove(); 
                    layer.msg(o.message,{icon:1,time:1000}); 
                } else { 
                    layer.msg(o.message,{icon:0,time:1000});  
                    $(obj).attr('onclick',"delAct(this,"+act+","+ids+","+type+")"); 
                }  
            },
            complete: function () {
            },
            error: function (o) {  
                var str = JSON.stringify(o);    
                if(o.responseText!= undefined && o.responseText=="no access")
                { 
                    layer.msg("无此权限！",{icon:0,time:2000});  
                    $(obj).attr('onclick',"delAct(this,"+act+","+ids+","+type+")"); 
                } 
            }
        });
    }); 
}

function editAct(title,id) {
    layer.load();
    $('#formEdit').html(''); 
    var act=$('#formEdit').attr("action");  
    $.get(act+"?id="+id, function(res) {
        $('#formEdit').html(res);  
        layer.closeAll('loading');
        layer.open({
            type: 1,
            title: title,
            maxmin: false, 
            shadeClose:false, //点击遮罩关闭层
            area : ['600px' , '650px'],
            content:$('#add_columns_style'),
            btn:['提交','取消'],
            yes: function(index){ 
                $('#formEdit').attr("action",act+"?id="+id); 
                $('#formEdit').submit();  
            }
        });  
    });
}
//字符替换
function tpl_replace(str, obj) {
    if (!(Object.prototype.toString.call(str) === '[object String]')) {
        return '';
    }

    // {}, new Object(), new Class()
    // Object.prototype.toString.call(node=document.getElementById("xx")) : ie678 == '[object Object]', other =='[object HTMLElement]'
    // 'isPrototypeOf' in node : ie678 === false , other === true
    if (!(Object.prototype.toString.call(obj) === '[object Object]' && 'isPrototypeOf' in obj)) {
        return str;
    }

    // https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/String/replace
    return str.replace(/\{([^{}]+)\}/g, function (match, key) {
        var value = obj[key];
        return (value !== undefined) ? '' + value : '';
    });
}


function PreviewImage(imgFile,pid)
{
    var filextension=imgFile.value.substring(imgFile.value.lastIndexOf("."),imgFile.value.length);
    filextension=filextension.toLowerCase();
    if ((filextension!='.jpg')&&(filextension!='.gif')&&(filextension!='.jpeg')&&(filextension!='.png'))
    {
        alert("对不起，系统仅支持标准格式的照片，请您调整格式后重新上传，谢谢 !");
        imgFile.focus();
    }
    else
    {
        var path;
        if(document.all)//IE
        {
            imgFile.select();
            path = document.selection.createRange().text;
            document.getElementById(pid).innerHTML="";
            document.getElementById(pid).style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled='true',sizingMethod='scale',src=\"" + path + "\")";//使用滤镜效果      
        }
        else//FF
        {
            path=window.URL.createObjectURL(imgFile.files[0]);// FF 7.0以上
            //path = imgFile.files[0].getAsDataURL();// FF 3.0
            document.getElementById(pid).innerHTML = "<img id='img1'  src='"+path+"'/>";
            //document.getElementById("img1").src = path;
        }
    }
}
