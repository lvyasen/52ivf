<?php
//定时任务
class DoController extends AppController { 

    function beforeFilter() {
        parent::beforeFilter();
    }
    //
    function execfunc(){
        $act = $_GET['func'];  
        switch ($act) { 
            case 'commission':   
                break; 
            case 'video_play_num':  
                $this->loadModel('Video');  
                $this->loadModel('FuncSendLog');  
                $list = $this->Video->find('all',array('conditions'=>array('status'=>1)));  
                if(!empty($list)){
                    foreach ($list as $key => $value) {
                        $play_num=rand(20,200);  //随机20-200之间
                        //播放数+1 
                        $res=$this->Video->updateAll(array('Video.play_num'=>'`Video`.`play_num`+'.$play_num), array('Video.id'=>$value['Video']['id'])); 

                        if($res > 0){ 
                            $this->FuncSendLog->id=0;
                            $data['action']="execfunc:video_play_num"; 
                            $data['logDate']=date("Y-m-d",time());
                            $data['logTime'] = date("H:i:s",time()); 
                            $data['logString'] = "play_num增加:".$play_num."，info:".json_encode($value['Video']);   
                            $this->FuncSendLog->save($data);  
                        }
                    }
                }
                break; 
            case 'aide_num':   
                $this->loadModel('FuncSendLog');  
                $aide_num=rand(1,20);  //随机10-100之间
                $randNum=Ivf::aideCount('add',$aide_num);
                $data=[];
                $data['action']="execfunc:aide_num"; 
                $data['logDate']=date("Y-m-d",time());
                $data['logTime'] = date("H:i:s",time()); 
                $data['logString'] = "aide_num增加:".$aide_num."，now:".$randNum;   
                $this->FuncSendLog->save($data);   
                break; 
            case 'news_xml':   
                $this->loadModel('New');  

                $orderby=[];   
                $orderby['create_time']="desc";
                $conditions['status'] = 1;  
                $dataList =[]; 
                $dataList = $this->New->find('all',array('conditions' => $conditions,'order' => $orderby,'fields'=>array('update_time','id')));
                if(!empty($dataList)){
                    $dom = new DOMDocument('1.0','utf-8');//建立DOM对象  
                    $no1 = $dom->createElement('urlset');//创建普通节点：urlset
                    $no1->setAttribute('xmlns',"http://www.sitemaps.org/schemas/sitemap/0.9");
                    $no1->setAttribute('xmlns:mobile',"http://www.baidu.com/schemas/sitemap-mobile/1/");
                    $dom->appendChild($no1);//把urlset节点加入到DOM文档中   

                    foreach ($dataList as $key => $value) {
                        $no2 = $dom->createElement('url');
                        $no1->appendChild($no2); 

                        $no3 = $dom->createElement('loc');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("http://www.ivf52.com/news/info/".$value['New']['id'].".html");
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('mobile:mobile');
                        $no2->appendChild($no3); 
                        $no4 = $dom->createAttribute('type');
                        $no4->value = "pc,mobile";
                        $no3->appendChild($no4); 

                        $no3 = $dom->createElement('lastmod');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode(date("Y-m-d",strtotime($value['New']['update_time'])));
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('changefreq');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("monthly"); 
                        $no3->appendChild($no4); 


                        $no3 = $dom->createElement('priority');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("0.8");
                        $no3->appendChild($no4);  
                    }
 

                    header('Content-type:text/html;charset=utf-8');
                    echo $dom->save('newslist.xml')?'存储成功':'存储失败';//存储为xml文档
                } 
                $this->loadModel('FuncSendLog');   
                $data=[];
                $data['action']="execfunc:news_xml"; 
                $data['logDate']=date("Y-m-d",time());
                $data['logTime'] = date("H:i:s",time()); 
                $data['logString'] = "生成xml:".json_encode($dataList);   
                $this->FuncSendLog->save($data);   
                break;
            case 'community_xml':   
                $this->loadModel('Article');
                $this->loadModel('Share');       
                $this->loadModel('Hospital');   
                $this->loadModel('User');    


                $orderby['sort']="desc";
                $orderby['create_time']="desc";
                $conditions['status'] = 1;
                $conditions['is_top'] = 1;

                $guideList=$answerList=$shareList=$dataList=[];
                //攻略 
                $guideList = array(); 
                $guideList = $this->Article->find('all',array(
                    'conditions' => $conditions, 
                    'limit' => 5,
                    'order' => $orderby,
                    )
                );
                $dataList['guideList']=$guideList;

                $dom = new DOMDocument('1.0','utf-8');//建立DOM对象  
                $no1 = $dom->createElement('urlset');//创建普通节点：urlset
                $no1->setAttribute('xmlns',"http://www.sitemaps.org/schemas/sitemap/0.9");
                $no1->setAttribute('xmlns:mobile',"http://www.baidu.com/schemas/sitemap-mobile/1/");
                $dom->appendChild($no1);//把urlset节点加入到DOM文档中   
                if(!empty($guideList)){
                    foreach ($guideList as $key => $value) { 
                        $no2 = $dom->createElement('url');
                        $no1->appendChild($no2); 

                        $no3 = $dom->createElement('loc');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("http://www.ivf52.com/plan/info/".$value['Article']['id'].".html");
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('mobile:mobile');
                        $no2->appendChild($no3); 
                        $no4 = $dom->createAttribute('type');
                        $no4->value = "pc,mobile";
                        $no3->appendChild($no4); 

                        $no3 = $dom->createElement('lastmod');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode(date("Y-m-d",strtotime($value['Article']['update_time'])));
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('changefreq');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("monthly"); 
                        $no3->appendChild($no4); 


                        $no3 = $dom->createElement('priority');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("0.8");
                        $no3->appendChild($no4);  
                    }
                }  

                //精选问答 
                $conditions['type']=1; 
                $answerList = array(); 
                $answerList = $this->Share->find('all',array(
                    'conditions' => $conditions, 
                    'fields'=>array('id','update_time'),
                    'limit' => 4,
                    'order' => $orderby,
                    )
                ); 
                $dataList['answerList']=$answerList;

                if(!empty($answerList)){
                    foreach ($answerList as $key => $value) { 
                        $no2 = $dom->createElement('url');
                        $no1->appendChild($no2); 

                        $no3 = $dom->createElement('loc');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("http://www.ivf52.com/answer/info/".$value['Share']['id'].".html");
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('mobile:mobile');
                        $no2->appendChild($no3); 
                        $no4 = $dom->createAttribute('type');
                        $no4->value = "pc,mobile";
                        $no3->appendChild($no4); 

                        $no3 = $dom->createElement('lastmod');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode(date("Y-m-d",strtotime($value['Share']['update_time'])));
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('changefreq');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("monthly"); 
                        $no3->appendChild($no4); 


                        $no3 = $dom->createElement('priority');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("0.8");
                        $no3->appendChild($no4);  
                    }
                } 
                unset($conditions['is_top']);
                //就医分享-全部
                $conditions['type']=0;
                $shareList = array(); 
                $shareList = $this->Share->find('all',array(
                    'conditions' => $conditions, 
                    'fields'=>array('id','update_time'),
                    'limit' => 10,
                    'order' => $orderby,
                    )
                ); 
                $dataList['shareList_all']=$shareList;

                if(!empty($shareList)){
                    foreach ($shareList as $key => $value) { 
                        $no2 = $dom->createElement('url');
                        $no1->appendChild($no2); 

                        $no3 = $dom->createElement('loc');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("http://www.ivf52.com/share/info/".$value['Share']['id'].".html");
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('mobile:mobile');
                        $no2->appendChild($no3); 
                        $no4 = $dom->createAttribute('type');
                        $no4->value = "pc,mobile";
                        $no3->appendChild($no4); 

                        $no3 = $dom->createElement('lastmod');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode(date("Y-m-d",strtotime($value['Share']['update_time'])));
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('changefreq');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("monthly"); 
                        $no3->appendChild($no4); 


                        $no3 = $dom->createElement('priority');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("0.8");
                        $no3->appendChild($no4);  
                    }
                }
                //就医分享-初次检查
                $conditions['tag']=10;
                $shareList = array(); 
                $shareList= $this->Share->find('all',array(
                    'conditions' => $conditions, 
                    'fields'=>array('id','update_time'),
                    'limit' => 10,
                    'order' => $orderby,
                    )
                ); 
                $dataList['shareList_data2']=$shareList;

                if(!empty($shareList)){
                    foreach ($shareList as $key => $value) { 
                        $no2 = $dom->createElement('url');
                        $no1->appendChild($no2); 

                        $no3 = $dom->createElement('loc');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("http://www.ivf52.com/share/info/".$value['Share']['id'].".html");
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('mobile:mobile');
                        $no2->appendChild($no3); 
                        $no4 = $dom->createAttribute('type');
                        $no4->value = "pc,mobile";
                        $no3->appendChild($no4); 

                        $no3 = $dom->createElement('lastmod');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode(date("Y-m-d",strtotime($value['Share']['update_time'])));
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('changefreq');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("monthly"); 
                        $no3->appendChild($no4); 


                        $no3 = $dom->createElement('priority');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("0.8");
                        $no3->appendChild($no4);  
                    }
                }

                //就医分享-促排阶段 
                $conditions['tag']=20;
                $shareList = array(); 
                $shareList = $this->Share->find('all',array(
                    'conditions' => $conditions, 
                    'fields'=>array('id','update_time'),
                    'limit' => 10,
                    'order' => $orderby,
                    )
                ); 
                $dataList['shareList_data3']=$shareList;

                if(!empty($shareList)){
                    foreach ($shareList as $key => $value) { 
                        $no2 = $dom->createElement('url');
                        $no1->appendChild($no2); 

                        $no3 = $dom->createElement('loc');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("http://www.ivf52.com/share/info/".$value['Share']['id'].".html");
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('mobile:mobile');
                        $no2->appendChild($no3); 
                        $no4 = $dom->createAttribute('type');
                        $no4->value = "pc,mobile";
                        $no3->appendChild($no4); 

                        $no3 = $dom->createElement('lastmod');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode(date("Y-m-d",strtotime($value['Share']['update_time'])));
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('changefreq');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("monthly"); 
                        $no3->appendChild($no4); 


                        $no3 = $dom->createElement('priority');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("0.8");
                        $no3->appendChild($no4);  
                    }
                }

                //就医分享-取卵移植 
                $conditions['tag']=30;
                $shareList = array(); 
                $shareList= $this->Share->find('all',array(
                    'conditions' => $conditions, 
                    'fields'=>array('id','update_time'),
                    'limit' => 10,
                    'order' => $orderby,
                    )
                ); 
                $dataList['shareList_data4']=$shareList;

                if(!empty($shareList)){
                    foreach ($shareList as $key => $value) { 
                        $no2 = $dom->createElement('url');
                        $no1->appendChild($no2); 

                        $no3 = $dom->createElement('loc');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("http://www.ivf52.com/share/info/".$value['Share']['id'].".html");
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('mobile:mobile');
                        $no2->appendChild($no3); 
                        $no4 = $dom->createAttribute('type');
                        $no4->value = "pc,mobile";
                        $no3->appendChild($no4); 

                        $no3 = $dom->createElement('lastmod');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode(date("Y-m-d",strtotime($value['Share']['update_time'])));
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('changefreq');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("monthly"); 
                        $no3->appendChild($no4); 


                        $no3 = $dom->createElement('priority');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("0.8");
                        $no3->appendChild($no4);  
                    }
                }

                //就医分享-保胎 
                $conditions['tag']=40;
                $shareList = array(); 
                $shareList = $this->Share->find('all',array(
                    'conditions' => $conditions, 
                    'fields'=>array('id','update_time'),
                    'limit' => 10,
                    'order' => $orderby,
                    )
                ); 
                $dataList['shareList_data5']=$shareList;

                if(!empty($shareList)){
                    foreach ($shareList as $key => $value) { 
                        $no2 = $dom->createElement('url');
                        $no1->appendChild($no2); 

                        $no3 = $dom->createElement('loc');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("http://www.ivf52.com/share/info/".$value['Share']['id'].".html");
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('mobile:mobile');
                        $no2->appendChild($no3); 
                        $no4 = $dom->createAttribute('type');
                        $no4->value = "pc,mobile";
                        $no3->appendChild($no4); 

                        $no3 = $dom->createElement('lastmod');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode(date("Y-m-d",strtotime($value['Share']['update_time'])));
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('changefreq');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("monthly"); 
                        $no3->appendChild($no4); 


                        $no3 = $dom->createElement('priority');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("0.8");
                        $no3->appendChild($no4);  
                    }
                }

                //就医分享-就医经验 
                $conditions['tag']=50;
                $shareList = array(); 
                $shareList = $this->Share->find('all',array(
                    'conditions' => $conditions, 
                    'fields'=>array('id','update_time'),
                    'limit' => 10,
                    'order' => $orderby,
                    )
                ); 

                if(!empty($shareList)){
                    foreach ($shareList as $key => $value) { 
                        $no2 = $dom->createElement('url');
                        $no1->appendChild($no2); 

                        $no3 = $dom->createElement('loc');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("http://www.ivf52.com/share/info/".$value['Share']['id'].".html");
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('mobile:mobile');
                        $no2->appendChild($no3); 
                        $no4 = $dom->createAttribute('type');
                        $no4->value = "pc,mobile";
                        $no3->appendChild($no4); 

                        $no3 = $dom->createElement('lastmod');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode(date("Y-m-d",strtotime($value['Share']['update_time'])));
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('changefreq');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("monthly"); 
                        $no3->appendChild($no4); 


                        $no3 = $dom->createElement('priority');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("0.8");
                        $no3->appendChild($no4);  
                    }
                }

                //就医分享-心情 
                $conditions['tag']=60;
                $shareList = array(); 
                $shareList = $this->Share->find('all',array(
                    'conditions' => $conditions, 
                    'fields'=>array('id','update_time'),
                    'limit' => 10,
                    'order' => $orderby,
                    )
                );
                $dataList['shareList_data6']=$shareList;

                if(!empty($shareList)){
                    foreach ($shareList as $key => $value) { 
                        $no2 = $dom->createElement('url');
                        $no1->appendChild($no2); 

                        $no3 = $dom->createElement('loc');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("http://www.ivf52.com/share/info/".$value['Share']['id'].".html");
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('mobile:mobile');
                        $no2->appendChild($no3); 
                        $no4 = $dom->createAttribute('type');
                        $no4->value = "pc,mobile";
                        $no3->appendChild($no4); 

                        $no3 = $dom->createElement('lastmod');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode(date("Y-m-d",strtotime($value['Share']['update_time'])));
                        $no3->appendChild($no4);  


                        $no3 = $dom->createElement('changefreq');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("monthly"); 
                        $no3->appendChild($no4); 


                        $no3 = $dom->createElement('priority');
                        $no2->appendChild($no3);
                        $no4 = $dom->createTextNode("0.8");
                        $no3->appendChild($no4);  
                    }
                }  

                header('Content-type:text/html;charset=utf-8');
                echo $dom->save('community.xml')?'存储成功':'存储失败';//存储为xml文档
                $this->loadModel('FuncSendLog');   
                $data=[];
                $data['action']="execfunc:community_xml"; 
                $data['logDate']=date("Y-m-d",time());
                $data['logTime'] = date("H:i:s",time()); 
                $data['logString'] = "生成xml:".json_encode($dataList);   
                $this->FuncSendLog->save($data);   
                break;
            default:
                break;
        } 
        die(); 
    }  
}