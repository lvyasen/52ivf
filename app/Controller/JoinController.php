<?php
class JoinController extends AppController {

    public $layout = 'main';

    function beforeFilter() {
        parent::beforeFilter();
        $this->set('nav','join');  
    } 
    function index($p = 1){ 
        $this->loadModel('Join');     
        $p = $this->rm_end_html($p); 
        $pageSize=5;
        $orderby=[];  
        $orderby['sort']="desc";
        $orderby['create_time']="desc";  
        $dataList =[]; 
        $dataList = $this->Join->find('all',array( 
            'limit' => $pageSize,
            'page' => $p,
            'order' => $orderby,
            )
        );
        $dataCount = $this->Join->find('count');
        $strPages=$this->htmlPage($p, $dataCount, $p,$pageSize);   
        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['加入我们']),'order'=>array('id'=>'desc'))); 
        $this->set('keysInfo',$keysInfo);
        $this->set('dataList',$dataList);
        $this->set('strPages',$strPages);
        $this->set('dataCount',$dataCount);
        $this->set('education',Configure::read('EDUCATION'));
        $this->set('experience',Configure::read('Experience'));
    }
    function info($id = 0){
        if(empty($id)){
            $this->redirect('/');
        }
        $id = $this->rm_end_html($id);
        $this->loadModel('Join');  
    	$info = $this->Join->find('first',array('conditions'=>array('id'=>$id)));
        if(empty($info)){
            //处理错误跳转
            die("非法请求");
        }    
        $keysInfo=[];
        $keysInfo['Keyword']['meta_title']=$info['Join']['title'].' - 试管无忧'; 
        $keysInfo['Keyword']['meta_keywords']=$info['Join']['title']; 
        $keysInfo['Keyword']['meta_description']=Ivf::getSubstr(strip_tags($info['Join']['description']),0,200,''); 
        $this->set('keysInfo',$keysInfo);  
        $this->set('info',$info);  
        $this->set('education',Configure::read('EDUCATION'));
        $this->set('experience',Configure::read('Experience'));
    }
}