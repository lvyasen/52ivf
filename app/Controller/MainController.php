<?php
class MainController extends AppController {

    public $layout = 'main';

    function beforeFilter() {
        parent::beforeFilter();
        $this->set('nav','index');  
    }

    function index(){     
        $params = $this->request->data;
        if(empty($params['dosubmit']))
            $params['dosubmit']=0;
        if($this->request->is('post') && $params['dosubmit']==1){ 
            if(empty($params['u_mobile'])){
                $this->ajaxReturn(4001, '手机号不能为空!');  
            }
            if(!Ivf::isMobile($params['u_mobile']))
                $this->ajaxReturn(4002, '请输入有效的11位手机号码!');   

            $this->loadModel('UsersAppoint');  
            $dataList = $this->UsersAppoint->find('first',array('conditions'=>array('status'=>0,'mobile'=>$params['u_mobile'],'source'=>'ivf52-首页')));
            if(!empty($dataList))
                $this->ajaxReturn(4003, '不能重复提交！'); 
  
            $db_data = array();
            $db_data['types'] =  isset($params['symptoms'])?intval($params['symptoms']):0;
            $db_data['user_name'] =  $params['u_name'];
            $db_data['mobile'] =  $params['u_mobile'];
            $db_data['system'] = Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT']));
            $db_data['brand'] = Ivf::getOSBrand(strtolower($_SERVER['HTTP_USER_AGENT'])); 
            $db_data['source'] = "ivf52-首页";
            $db_data['source_url'] = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $db_data['status'] = 0; 
            $db_data['ip'] = Ivf::getClientIp();; 
            $db_data['create_time'] = date("Y-m-d H:i:s",time()); 
            if(!$this->UsersAppoint->save($db_data)) 
                    $this->ajaxReturn(4005, '预约失败！'); 
              
            $this->ajaxReturn(200, '申请成功！');   
        } 
        $this->loadModel('New'); 
        $this->loadModel('Share');  
        $this->loadModel('User');  
        $orderby=[];  
        $orderby['sort']="desc";
        $orderby['create_time']="desc";
        $conditions['id'] = array('2','3','5','7');
        $ronditions['status'] = 1;
        $ronditions['is_home'] = 1;
        $ronditions['category_id'] = 4;   //泰国试管报道分类id，可修改

        $shareCon['status'] = 1;
        $shareCon['is_top'] = 1;
        $shareCon['type'] = 1;
        $shareBy['comment_num'] = 'desc';
        $shareBy['sort'] = 'desc';
        $shareBy['update_time'] = 'desc';

        $top4List = $shareList = $top6List = [];
        $where['status'] = 1;
        $where['is_top'] = 1;
        $where['is_elite'] = 1;
        $top4List = $this->New->find('all',array(
                'conditions' => $conditions,
                'limit' => 4,
                'order' => $orderby,
            )
        );

        $conditions['category_id'] = 5;    //最新资讯分类id，可修改 
        $top6List = $this->New->find('all',array(
            'conditions' => $ronditions, 
            'limit' => 6,
            'order' => $orderby,
            )
        );

        //分享说，按评论数最多的显示
        $conditions= $orderby=[];  
        $orderby['comment_num']="desc";
        $conditions['status'] = 1;   
        $conditions['type'] = 0;   
        $shareList = $this->Share->find('all',array(
            'conditions' => $conditions, 
            'limit' => 3,
            'order' => $orderby,
            )
        );

        if(!empty($shareList)){
            foreach ($shareList as $key => $value) {   
                 $str= preg_replace('/<\s*img\s+[^>]*?src\s*=\s*(\'|\")(.*?)\\1[^>]*?\/?\s*>/i',"",$value['Share']['content']); 
                 $userInfo=$this->User->findById(intval($value['Share']['uid'])); 
                 $shareList[$key]['Share']['username']=isset($userInfo['User']['nick_name'])?$userInfo['User']['nick_name']:"保密"; 
                 $shareList[$key]['Share']['content']=Ivf::getSubstr(str_replace('　　', '', $str),0,20);  
                 $shareList[$key]['Share']['avatar']=isset($userInfo['User']['avatar'])?$userInfo['User']['avatar']:"";
            }
        }  
        //友情链接 
        $this->loadModel('Link');  
        $linkList = $this->Link->find('all',array('conditions'=>array('status'=>1),'order'=>array('sort'=>'desc')));  
        
        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['首页']),'order'=>array('id'=>'desc'))); 
        $randNum=Ivf::aideCount();
        $this->set('keysInfo',$keysInfo);
        $this->set('top6List',$top6List);
        $this->set('top4List',$top4List);
        $this->set('linkList',$linkList);
        $this->set('randNum',$randNum);  
        $this->set('shareList',$shareList);
        $this->set('disease',Configure::read('DISEASE_TOPIC')); 

        // $this->loadModel('User');
        // $user_info = $this->User->find('all',array());
        // $user_inf=[];
        // $num=1;
        // foreach($user_info as $key=>$value){
        //     if(substr($value['User']['mobile'],0,4)=="1822"){
        //         $value['User']['avatar']="/uploads/avatar/image/2018/2018".$num.".jpg";
        //         $num=$num+1;
        //         $user_inf['User']['avatar']=$value['User']['avatar'];
        //         $user_inf['User']['id']=$value['User']['id'];
        //         $this->User->save($user_inf);
        //     }
        // }
    }
    function leave(){ 
        // 留言板
        $params = $this->request->data;
        if(empty($params['dosubmit']))
            $params['dosubmit']=0;
        $this->loadModel('Leave');
        if ($this->request->is('post') && $params['dosubmit']==2) {
            if(empty($params['name'])){
                $this->ajaxReturn(4001, '姓名不能为空!');  
            }
            if(empty($params['iphone'])){
                $this->ajaxReturn(4001, '手机号不能为空!');  
            }
            if(empty($params['qq'])){
                $this->ajaxReturn(4001, 'QQ号不能为空!');  
            }
            if(empty($params['content'])){
                $this->ajaxReturn(4001, '留言内容不能为空!');  
            }
            if(!empty($params['email'])){
                if(!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$params['email'])) {
                    $this->ajaxReturn(4001, '无效的 email 格式！');
                }
            }
            if(!Ivf::isMobile($params['iphone']))
                $this->ajaxReturn(4002, '请输入有效的11位手机号码!'); 
            $db_leave = array();
            $db_leave['name'] = $params['name'];
            $db_leave['qq'] = $params['qq'];
            $db_leave['phone'] = $params['iphone'];
            $db_leave['email'] = $params['email'];
            $db_leave['content'] = $params['content'];
            $db_leave['ctime'] = date("Y-m-d H:i:s",time()); 
            if (!$this->Leave->save($db_leave))
                $this->ajaxReturn(4001, '留言失败!');
            $this->ajaxReturn(200, '留言成功!','','/main/index');   
        }
    }
    function leave_sub(){
        $params = $this->request->data;
        try {
            if(empty($params['name'])){
                throw new Exception("请填写姓名");
            }
            if(empty($params['phone'])){
                throw new Exception("请填写您的手机号");
            }
            $this->loadModel('Leave');
            $save_data = [];
            $save_data['name'] = $params['name'];
            $save_data['qq'] = $params['qq'];
            $save_data['phone'] = $params['phone'];
            $save_data['email'] = $params['email'];
            $save_data['content'] = $params['content'];
            $save_data['ctime'] = date("Y-m-d H:i:s",time()); 
            if(!$this->Leave->save($save_data)){
                throw new Exception("操作失败");
            }
            $data['status'] = 'success';
        } catch (Exception $e) {
            $data['status'] = 'fail';
            $data['message'] = $e->getMessage();
        }
        echo json_encode($data);
        die;
    }
}