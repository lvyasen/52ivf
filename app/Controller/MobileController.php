<?php

// 单独的手机端页面
class MobileController extends AppController {

    public $layout = 'mobile';
    public $user=array();

    function beforeFilter() {
        parent::beforeFilter();
        $this->user = $this->getUser();
        $this->set('userInfo',$this->user);
        $this->set('nav','account');
    }


    function login(){
        // 登录手机端
        if($this->checkLogin()){
            $this->redirect('/mobile/index/');
        }
        // 登陆   
        $params = $this->request->data;
        if($this->request->is('post') && $params['dosubmit']==1){
            if(empty($params['u_mobile'])){
                $this->ajaxReturn(4001, '手机号不能为空!');
            }
            if(!Ivf::isMobile($params['u_mobile']))
                $this->ajaxReturn(4002, '请输入有效的11位手机号码!');

            if(empty($params['u_pwd']))
                    $this->ajaxReturn(4003, '密码不能为空！');

            $this->loadModel('User');
            $dataList = $this->User->findByMobile($params['u_mobile']);
            if(empty($dataList))
                $this->ajaxReturn(4004, '手机号未注册！');
            $userInfo=$dataList['User'];
            if(md5($params['u_pwd'].$userInfo['auth'])!=$userInfo['password'])
                    $this->ajaxReturn(4005, '密码有误！');

            unset($userInfo['password'],$userInfo['auth']);
            $this->setUser($userInfo);
            $regerurl=$this->Session->read('regerurl');
            $this->Session->delete('regerurl');
            $this->ajaxReturn(200, '登陆成功！',array('regerurl' =>$regerurl ));
        }
        if ($this->isWeixin()){
            $this->wxlogin();
            die();
        }
        $this->Session->write('regerurl', isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/mobile');
    }
    public function wxlogin()
    {
        $params=$_GET;
        App::import('Vendor','wechat-php-sdk/wechat');
        $option=array(
            'appid'=>'wx7ef0124e343093cb', //填写高级调用功能的app id
            'appsecret'=>'acc5e5fcab919e7977d158395967213d' //填写高级调用功能的密钥
        );
        $code=isset($params['code'])?$params['code']:'';
        $weChat=new Wechat($option);
        $session=$this->Session;
        $open_id=$session->read('open_id');
        $lifeTime=$session->read('wx_life_time');
        $token=$session->read('access_token');
        $this->loadModel('Users');
        if (!$code && isset($open_id) && isset($token) && $lifeTime+7200>time()){
            //缓存中有 open_id token
            $userInfo=$this->Users->find('all',array(
                'conditions'=>array('openid'=>$open_id),
                'limit'=>1
            ));
            $this->setUser($userInfo['Users']);

            $regerurl=$this->Session->read('regerurl');
            $this->Session->delete('regerurl');
            $this->Session->write('regerurl', isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/mobile');
            $this->redirect('/mobile/index/');
        }else{
            //如果有code
            if ($code){
                //回调信息
                $back_info=$weChat->getOauthAccessToken();
                if (empty($back_info)){
                    return json_encode(array('message'=>'用户登录失败','code'=>4001));
                }
                //将信息存入session
                $open_id=$back_info['openid'];
                $access_token=$back_info['access_token'];
                $session->write('open_id',$back_info['openid']);
                $session->write('access_token',$back_info['access_token']);
                $session->write('wx_life_time',time());
                $userInfo=$this->Users->find('all',array(
                    'conditions'=>array('openid'=>$back_info['openid']),
                    'limit'=>1
                ));
                $wxUser=$weChat->getOauthUserinfo($access_token,$open_id);
                //存入数据库
                if (empty($userInfo)){
                    $userInfo['nick_name']=$wxUser['nickname'];
                    $userInfo['openid']=$wxUser['openid'];
                    $userInfo['sex']=$wxUser['sex'];
                    $userInfo['avatar']=$wxUser['headimgurl'];
                    $userInfo['orange_avatar']=$wxUser['headimgurl'];
                    $userInfo['create_time']=date('Y-m-d H:i:s');
//                    $userInfo['ip']=date('Y-m-d H:i:s');
                    $this->Users->save($userInfo);

                }
                $userInfo= $this->Users->findByOpenid($open_id);
                $this->setUser($userInfo['Users']);
                $regerurl=$this->Session->read('regerurl');
                $this->Session->delete('regerurl');
                $this->Session->write('regerurl', isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/mobile');
                $this->redirect('/mobile/index/');
            }else{
                //获取code
                $callback='HTTP://www.2.panduolala.com/mobile/wxlogin';
                $callback=$weChat->getOauthRedirect($callback);
                header('Location:'.$callback);
                die();
            }
        }
    }
    function register(){
        if($this->checkLogin()){
            $this->redirect('/mobile/index/');
        }
        // 注册   
        $params = $this->request->data;
        if($this->request->is('post') && $params['dosubmit']==1){
            if(empty($params['u_mobile'])){
                $this->ajaxReturn(4001, '手机号不能为空!');
            }
            if(!Ivf::isMobile($params['u_mobile']))
                $this->ajaxReturn(4002, '请输入有效的11位手机号码!');

            $this->loadModel('User');
            $dataList = $this->User->findByMobile($params['u_mobile']);
            if(!empty($dataList))
                $this->ajaxReturn(4003, '手机号已注册！');

            if(empty($params['u_pwd']))
                    $this->ajaxReturn(4004, '密码不能为空！');

            if(empty($params['u_comfirm_pwd']))
                    $this->ajaxReturn(4005, '确认密码不能为空！');

            if($params['u_comfirm_pwd'] != $params['u_pwd'])
                    $this->ajaxReturn(4006, '两次密码不一致！');

            $code = $this->Session->read('mobile_code');
            if(($params['u_mobile'] != $code['mobile']) || ($params['u_code'] != $code['code']))
                    $this->ajaxReturn(4007, '手机验证码有误！');

            $rizhi=$this->getSource();
            $db_data = array();
            $db_data['mobile'] = $db_data['mobile'] = $params['u_mobile'];
            $db_data['auth'] = Ivf::random(8);
            $db_data['status'] = 1;
            $db_data['source'] = isset($rizhi['source'])?$rizhi['source']:0;
            $db_data['logid'] = isset($rizhi['id'])?$rizhi['id']:0;
            $db_data['password'] = md5($params['u_pwd'].$db_data['auth']);
            $db_data['avatar'] = '/images/mobile/touxiang.png';
            $db_data['ip'] = Ivf::getClientIp();;
            $db_data['create_time'] = date("Y-m-d H:i:s",time());
            if(!$this->User->save($db_data))
                    $this->ajaxReturn(4008, '注册失败！');

            $this->Session->delete('mobile_code');
            $user = $this->User->findByMobile($params['u_mobile']);
            unset($user['User']['password'],$user['User']['auth']);
            $this->setUser($user['User']);
            $this->ajaxReturn(200, '注册成功！');
        }
    }
    function retrieval(){
        // 密码找回   
        $params = $this->request->data;
        if($this->request->is('post') && $params['dosubmit']==1){
            if(empty($params['u_mobile'])){
                $this->ajaxReturn(4001, '手机号不能为空!');
            }
            if(!Ivf::isMobile($params['u_mobile']))
                $this->ajaxReturn(4002, '请输入有效的11位手机号码!');

            $this->loadModel('User');
            $dataList = $this->User->findByMobile($params['u_mobile']);
            if(empty($dataList))
                $this->ajaxReturn(4003, '手机号未注册！');
            $dataList=$dataList['User'];

            if(empty($params['u_pwd']))
                    $this->ajaxReturn(4004, '新密码不能为空！');

            if(empty($params['u_comfirm_pwd']))
                    $this->ajaxReturn(4005, '确认密码不能为空！');

            if($params['u_comfirm_pwd'] != $params['u_pwd'])
                    $this->ajaxReturn(4006, '两次密码不一致！');

            $code = $this->Session->read('mobile_code');
            if(($params['u_mobile'] != $code['mobile']) || ($params['u_code'] != $code['code']))
                    $this->ajaxReturn(4007, '手机验证码有误！');

            $dataList['password'] = md5($params['u_pwd'].$dataList['auth']);
            if(!$this->User->save($dataList))
                    $this->ajaxReturn(4008, '密码重置失败！');

            $this->Session->delete('mobile_code');
            $this->ajaxReturn(200, '密码重置成功！');
        }
    }
    function index(){
    	// 个人中心主页
        $this->checkLogin(2);
        $this->set('sex',Configure::read('USER_SEX'));
    }
    function means(){
        // 设置个人信息页面
        $this->checkLogin(2);
        $this->set('sex',Configure::read('USER_SEX'));
    }
    function password(){
        // 修改密码页面
        $this->checkLogin(2);
        $this->loadModel('User');
        $params = $this->request->data;
        if($this->request->is('post') && $params['dosubmit']==1){
            $userInfo = $this->User->findById($this->user['id']);
            $userInfo=$userInfo['User'];
            if(empty($userInfo))
                $this->ajaxReturn(4001, '非法提交！');

            if(empty($params['u_pwd']))
                    $this->ajaxReturn(4002, '旧密码不能为空！');
            if($userInfo['password']!=md5($params['u_pwd'].$userInfo['auth']))
                    $this->ajaxReturn(4002, '旧密码有误！');

            if(empty($params['new_pwd']))
                    $this->ajaxReturn(4003, '新密码不能为空！');

            if(empty($params['new_pwd_comfirm']))
                    $this->ajaxReturn(4004, '确认密码不能为空！');

            if($params['new_pwd_comfirm'] != $params['new_pwd'])
                    $this->ajaxReturn(4005, '两次密码不一致！');
            $userInfo['password']=md5($params['new_pwd'].$userInfo['auth']);
            if(!$this->User->save($userInfo))
                $this->ajaxReturn(4003, '提交异常!');
            $this->setUser($userInfo);
            $this->ajaxReturn(200, '密码修改成功!');
        }

    }
    function name(){
        // 设置“我的昵称”页面
        $this->checkLogin(2);
        $this->loadModel('User');
        $params = $this->request->data;
        $userInfo=$this->user;
        if($this->request->is('post') && $params['dosubmit']==1){
            if(empty($params['u_nick']))
                $this->ajaxReturn(4002, '昵称不能为空！');
            $userInfo['nick_name']= $params['u_nick'];
            $userInfo['user_name']= $params['u_nick'];
            if(!$this->User->save($userInfo))
                $this->ajaxReturn(4003, '提交异常!');
            unset($userInfo['password'],$userInfo['auth']);
            $this->setUser($userInfo);
            $this->ajaxReturn(200, '修改成功!');
        }
    }
    function my_share(){
        // 我的分享页面 
        $this->checkLogin(2);
        $this->loadModel('Share');
        $this->loadModel('Picture');

        $orderby['create_time']="desc";
        $conditions['status'] = 1;
        $conditions['uid']=$this->user['id'];
        $conditions['type']=0;
        $dataList = array();
        $dataList = $this->Share->find('all',array(
            'conditions' => $conditions,
            'limit' => 20,
            'order' => $orderby,
            )
        );
        $picTopic=Configure::read('PIC_TOPIC');
        if(!empty($dataList)){
            foreach ($dataList as $key => $value) {
                $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['分享'],'obj_id'=>intval($value['Share']['id'])),'fields'=>array('thumb')));
                $dataList[$key]['Share']['images']=$picData;
                $dataList[$key]['Share']['content']=Ivf::getSubstr($value['Share']['content'],0,100);
            }
        }
        $this->set('dataList',$dataList);
    }
    function my_answer(){
       // 我的问答
        $this->checkLogin(2);
        $this->loadModel('Share');
        $this->loadModel('Picture');

        $orderby['create_time']="desc";
        $conditions['status'] = 1;
        $conditions['uid']=$this->user['id'];
        $conditions['type']=1;
        $dataList = array();
        $dataList = $this->Share->find('all',array(
            'conditions' => $conditions,
            'limit' => 20,
            'order' => $orderby,
            )
        );
        $picTopic=Configure::read('PIC_TOPIC');
        if(!empty($dataList)){
            foreach ($dataList as $key => $value) {
                $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['问答'],'obj_id'=>intval($value['Share']['id'])),'fields'=>array('thumb')));
                $dataList[$key]['Share']['images']=$picData;
                $dataList[$key]['Share']['content']=Ivf::getSubstr($value['Share']['content'],0,100);
            }
        }
        $this->set('dataList',$dataList);
    }
    function my_reservation(){
        // 我的预约
        $this->checkLogin(2);
        $this->loadModel('UsersAppoint');
        $this->loadModel('Hospital');
        $this->loadModel('Doctor');
        $userInfo=$this->user;
        $dataList = $this->UsersAppoint->find('all',array('conditions'=>array('mobile'=>$userInfo['mobile'],'source like'=>'ivf52-%')));
        if(!empty($dataList)){
            foreach ($dataList as $key => $value) {
                if($value['UsersAppoint']['hospital_id']>0){
                    $dataInfo = $this->Hospital->findById($value['UsersAppoint']['hospital_id']);
                    $dataList[$key]['UsersAppoint']['hospital']= isset($dataInfo['Hospital']['cn_name'])?$dataInfo['Hospital']['cn_name']:"无";
                    $dataList[$key]['UsersAppoint']['hospital_image']= isset($dataInfo['Hospital']['image'])?$dataInfo['Hospital']['image']:"";
                }
                if($value['UsersAppoint']['doctor_id']>0){
                    $dataInfo = $this->Doctor->findById($value['UsersAppoint']['doctor_id']);
                    $dataList[$key]['UsersAppoint']['doctor']= isset($dataInfo['Doctor']['cn_name'])?$dataInfo['Doctor']['cn_name']:"无";
                    $dataList[$key]['UsersAppoint']['doctor_image']= isset($dataInfo['Doctor']['avatar'])?$dataInfo['Doctor']['avatar']:"无";
                }
            }
        }
        $this->set('dataList',$dataList);
    }

    function setphoto(){
        $this->checkLogin(1);
        $this->loadModel('User');
        $dataList = $this->User->findById($this->user['id']);
        if(empty($dataList))
            $this->ajaxReturn(4001, '非法提交！');
        $dataList =$dataList['User'];
        if (isset($_FILES["imgFile"]) && is_uploaded_file($_FILES["imgFile"]['tmp_name'])) {
            $attrRes=Ivf::uploadImg('/uploads/avatar/');
            if($attrRes['status']!=200)
                $this->ajaxReturn(4002, '上传失败！原因:'.$attrRes['message']);
            $dataList['avatar'] =$attrRes['url'];
            $dataList['id']=$this->user['id'];
            if(!$this->User->save($dataList)){
                $this->ajaxReturn(4003, '参数异常！');
            }
            unset($dataList['password'],$dataList['auth']);
            $this->setUser($dataList);
            $this->ajaxReturn(200, '设置头像成功！');
        }
        $this->ajaxReturn(4003, '参数异常！');
    }
    // 预约医生
    function order_doctor($id = 0){
        $this->loadModel('Doctor');
        $paramDates = $this->request->data;
        if($this->request->is('post') && $paramDates['dosubmit']==1){
            if($paramDates['did'] <= 0){
                $this->ajaxReturn(4001, '请选择预约医生!');
            }

            $info = $this->Doctor->find('first',array('conditions'=>array('status'=>1,'id'=>intval($paramDates['did']))));
            if(empty($info)){
                $this->ajaxReturn(4002, '医生不存在!');
            }
            if(empty($paramDates['u_mobile'])){
                $this->ajaxReturn(4003, '手机号不能为空!');
            }
            if(!Ivf::isMobile($paramDates['u_mobile']))
                $this->ajaxReturn(4005, '请输入有效的11位手机号码!');

            $code = $this->Session->read('mobile_code');
            if(($paramDates['u_mobile'] != $code['mobile']) || ($paramDates['u_code'] != $code['code']))
                    $this->ajaxReturn(4006, '手机验证码有误！');

            $this->loadModel('UsersAppoint');
            $dataList = $this->UsersAppoint->find('first',array('conditions'=>array('status'=>0,'mobile'=>$paramDates['u_mobile'],'source'=>'ivf52-手机医生列表页')));
            if(!empty($dataList))
                $this->ajaxReturn(4007, '不能重复预约！');

            $user = $this->getUser();
            $db_data = array();
            $db_data['types'] =  0;
            $db_data['uid'] =  isset($user['id'])?$user['id']:0;
            // $db_data['user_name'] =  isset($user['user_name'])?$user['user_name']:''; 
            $db_data['user_name'] = $paramDates['user_name'];
            $db_data['hospital_id'] =  $info['Doctor']['hospital_id'];
            $db_data['doctor_id'] =  $paramDates['did'];
            $db_data['mobile'] =  $paramDates['u_mobile'];
            $db_data['system'] = Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT']));
            $db_data['brand'] = Ivf::getOSBrand(strtolower($_SERVER['HTTP_USER_AGENT']));
            $db_data['source'] = "ivf52-手机医生列表页";
            $db_data['source_url'] = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $db_data['status'] = 0;
            $db_data['description'] = $paramDates['description'];
            $db_data['ip'] = Ivf::getClientIp();;
            $db_data['create_time'] = date("Y-m-d H:i:s",time());
            if(!$this->UsersAppoint->save($db_data))
                    $this->ajaxReturn(4008, '预约失败！');

            $this->ajaxReturn(200, '预约成功！');
        }

        if(empty($id)){
            $this->redirect('/');
        }
        $id = $this->rm_end_html($id);
        $info = $this->Doctor->find('first',array('conditions'=>array('status'=>1,'id'=>intval($id))));
        if(empty($info)){
            die("医生不存在");
        }
        $this->set('info',$info);

    }
    // 预约医院
    function order_hospital($id = 0){
        $this->loadModel('Hospital');
        $paramDates = $this->request->data;
        if($this->request->is('post') && $paramDates['dosubmit']==1){
            if($paramDates['hid'] <= 0){
                $this->ajaxReturn(4001, '请选择预约医院!');
            }

            $info = $this->Hospital->find('first',array('conditions'=>array('status'=>1,'id'=>intval($paramDates['hid']))));
            if(empty($info)){
                $this->ajaxReturn(4002, '医院不存在!');
            }
            if(empty($paramDates['u_mobile'])){
                $this->ajaxReturn(4003, '手机号不能为空!');
            }
            if(!Ivf::isMobile($paramDates['u_mobile']))
                $this->ajaxReturn(4005, '请输入有效的11位手机号码!');

            $code = $this->Session->read('mobile_code');
            if(($paramDates['u_mobile'] != $code['mobile']) || ($paramDates['u_code'] != $code['code']))
                    $this->ajaxReturn(4006, '手机验证码有误！');

            $this->loadModel('UsersAppoint');
            $dataList = $this->UsersAppoint->find('first',array('conditions'=>array('status'=>0,'mobile'=>$paramDates['u_mobile'],'source'=>'ivf52-手机医院列表页')));
            if(!empty($dataList))
                $this->ajaxReturn(4007, '不能重复预约！');

            $user = $this->getUser();
            $db_data = array();
            $db_data['types'] =  0;
            $db_data['uid'] =  isset($user['id'])?$user['id']:0;
            // $db_data['user_name'] =  isset($user['user_name'])?$user['user_name']:''; 
            $db_data['user_name'] = $paramDates['user_name'];
            $db_data['hospital_id'] =  $paramDates['hid'];
            $db_data['mobile'] =  $paramDates['u_mobile'];
            $db_data['system'] = Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT']));
            $db_data['brand'] = Ivf::getOSBrand(strtolower($_SERVER['HTTP_USER_AGENT']));
            $db_data['source'] = "ivf52-手机医院列表页";
            $db_data['source_url'] = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $db_data['status'] = 0;
            $db_data['description'] = $paramDates['description'];
            $db_data['ip'] = Ivf::getClientIp();;
            $db_data['create_time'] = date("Y-m-d H:i:s",time());
            if(!$this->UsersAppoint->save($db_data))
                    $this->ajaxReturn(4008, '预约失败！');

            $this->ajaxReturn(200, '预约成功！');
        }

        if(empty($id)){
            $this->redirect('/');
        }
        $id = $this->rm_end_html($id);
        $info = $this->Hospital->find('first',array('conditions'=>array('status'=>1,'id'=>intval($id))));
        if(empty($info)){
            die("医院不存在");
        }
        $this->set('info',$info);
    }
    function my_plan(){
        $this->checkLogin(2);
        $this->loadModel('Article');
        $this->loadModel('Picture');
        $this->loadModel('Hospital');

        $orderby['create_time']="desc";
        $conditions['status'] = 1;
        $conditions['uid']=$this->user['id'];
        $dataList = array();
        $dataList = $this->Article->find('all',array(
            'conditions' => $conditions,
            'limit' => 20,
            'order' => $orderby,
            )
        );
        $picTopic=Configure::read('PIC_TOPIC');
        if(!empty($dataList)){
            foreach ($dataList as $key => $value) {
                $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['攻略'],'obj_id'=>intval($value['Article']['id'])),'fields'=>array('thumb')));
                $dataList[$key]['Article']['images']=$picData;
            }
        }
        $this->loadModel('Hospital');
        $hospitals = $this->Hospital->find('all',array(
            'conditions'=>array('status'=>1),
            'fields'=>array('id','cn_name'),
            )
        );
        $this->set('hospitalList',$hospitals);
        $this->set('dataList',$dataList);
    }
    function release($tp = 0){
        $tp = $this->rm_end_html($tp);
        $this->loadModel('ArticleCate');
        $cates = $this->ArticleCate->find('all',array('conditions'=>array('type'=>0,'parent_id'=>0)));
        $this->loadModel('Hospital');
        $hospitals = $this->Hospital->find('all',array(
            'conditions'=>array('status'=>1),
            'fields'=>array('id','cn_name'),
            )
        );
        $this->set('hospitalList',$hospitals);
        $this->set('cates',$cates);
        $this->set('share_tags',Configure::read('SHARE_TAG'));
        $this->set('tp',$tp);
    }

    function actrelease(){
        $this->checkLogin(1);
        $this->loadModel('Picture');
        // 发布攻略   
        $params = $this->request->data;
        $picTopic=Configure::read('PIC_TOPIC');
        if($this->request->is('post') && $params['dosubmit']==1){

            if(empty($params['title'])){
                $this->ajaxReturn(4001, '标题不能为空!');
            }

            if(empty($params['description'])){
                $this->ajaxReturn(4002, '内容不能为空!');
            }
            $hospital_id = isset($params['hospital_id'])?$params['hospital_id']:0;
            $tag = isset($params['tag_id'])?$params['tag_id']:0;
           // $hospital_id=$this->Session->read('ad_hospital_id');

            $db_data = array();
            $db_data['title'] = $params['title'];
            $db_data['hospital_id'] = intval($hospital_id);
            $db_data['description'] = $params['description'];
            $db_data['content'] = $params['description'];
            $db_data['sort'] = 110;
            $db_data['uid'] = $this->user['id'];
            $db_data['status'] = 1;
            $db_data['create_time'] = date("Y-m-d H:i:s",time());
            $db_data['update_time'] = date("Y-m-d H:i:s",time());

            $this->Session->delete('ad_hospital_id');

            if($params['tp']==0){
                $db_data['type'] = 0;
                $db_data['tag'] = intval($tag);
                $db_data['tag2'] = 0;
                //$this->Session->delete('ad_tag');
                $this->loadModel('Share');
                if(!$this->Share->save($db_data))
                    $this->ajaxReturn(4008, '发布失败！');
                if(!empty($params['UptPicture'])){
                    foreach ($params['UptPicture'] as $val){
                        $data=[];
                        $data['id'] = $val;
                        $data['obj_type'] = $picTopic['分享'];
                        $data['obj_id'] = $this->Share->id;
                        $this->Picture->save($data);
                    }
                }

            }
            else{
                //$category_id=$this->Session->read('ad_category_id');  
                $db_data['category_id'] = intval($tag);
               // $this->Session->delete('ad_category_id');
                $this->loadModel('Article');
                if(!$this->Article->save($db_data))
                    $this->ajaxReturn(4008, '发布失败！');
                if(!empty($params['UptPicture'])){
                    foreach ($params['UptPicture'] as $val){
                        $data=[];
                        $data['id'] = $val;
                        $data['obj_type'] = $picTopic['攻略'];
                        $data['obj_id'] = $this->Article->id;
                        $this->Picture->save($data);
                    }
                }
            }

            $this->ajaxReturn(200, '发布成功！', array('returns' => $params['tp']==1?'/mobile/my_plan':'/mobile/my_share'));
        }
    }
    function release_ans(){
        $this->checkLogin(1);
        $this->loadModel('Picture');
        $this->loadModel('Share');
        $this->loadModel('Hospital');
        // 发布攻略   
        $params = $this->request->data;
        $picTopic=Configure::read('PIC_TOPIC');
        if($this->request->is('post') && $params['dosubmit']==1){

            if(empty($params['description'])){
                $this->ajaxReturn(4002, '内容不能为空!');
            }
            //$hospital_id=$this->Session->read('ad_hospital_id'); 
            //$ad_as_tag=$this->Session->read('ad_as_tag'); 
            $hospital_id = isset($params['hospital_id'])?$params['hospital_id']:0;
            $ad_as_tag = isset($params['tag_id'])?$params['tag_id']:0;
            $city_id=0;

            if(intval($hospital_id)>0){
                $info = $this->Hospital->findById(intval($hospital_id));
                if(isset($info['Hospital']['id']))
                    $city_id=$info['Hospital']['city_id'];
            }

            $db_data = array();
            $db_data['type'] = 1;
            $db_data['city_id'] = $city_id;
            $db_data['hospital_id'] = intval($hospital_id);
            $db_data['description'] = $params['description'];
            $db_data['content'] = $params['description'];
            $db_data['uid'] = $this->user['id'];
            $db_data['sort'] = 110;
            $db_data['tag'] = intval($ad_as_tag);
            $db_data['tag2'] = 0;
            $db_data['status'] = 1;
            $db_data['create_time'] = date("Y-m-d H:i:s",time());
            //$this->Session->delete('ad_hospital_id');
            //$this->Session->delete('ad_as_tag');  
            if(!$this->Share->save($db_data))
                    $this->ajaxReturn(4008, '发布失败！');
            if(!empty($params['UptPicture'])){
                foreach ($params['UptPicture'] as $val){
                    $data=[];
                    $data['id'] = $val;
                    $data['obj_type'] = $picTopic['问答'];
                    $data['obj_id'] = $this->Share->id;
                    $this->Picture->save($data);
                }
            }
            $this->ajaxReturn(200, '发布成功！', array('returns' => '/mobile/my_answer'));
        }
        $this->loadModel('Hospital');
        $hospitals = $this->Hospital->find('all',array(
            'conditions'=>array('status'=>1),
            'fields'=>array('id','cn_name'),
            )
        );
        $this->set('hospitalList',$hospitals);
        $this->set('share_tags',Configure::read('SHARE_TAG'));
        $this->set('tp',2);
    }
    function sele($tp = 0){
        $params = $this->request->data;
        if($this->request->is('post') && $params['tag_id']>0){
            if($tp==0){
                $this->Session->write('ad_tag', intval($params['tag_id']));
                $this->redirect('/mobile/release/');
            }
            else if($tp==1){
                $this->Session->write('ad_category_id', intval($params['tag_id']));
                $this->redirect('/mobile/release/1.html');
            }
            else if($tp==2){
                $this->Session->write('ad_as_tag', intval($params['tag_id']));
                $this->redirect('/mobile/release_ans');
            }
        }
        $tp = $this->rm_end_html($tp);
        $tag_id=0;
        if($tp==0){
            $tag_id=$this->Session->read('ad_tag');
            $this->set('share_tags',Configure::read('SHARE_TAG'));
        }
        else if($tp==1){
            $this->loadModel('ArticleCate');
            $cates = $this->ArticleCate->find('all',array('conditions'=>array('type'=>0,'parent_id'=>0)));
            $tag_id=$this->Session->read('ad_category_id');
            $this->set('cates',$cates);
        }
        else if($tp==2){
            $tag_id=$this->Session->read('ad_as_tag');
            $this->set('share_tags',Configure::read('SHARE_TAG'));
        }

        $this->set('tag_id',intval($tag_id));
        $this->set('tp',$tp);
    }
    function sele_hos($tp = 0){
        $params = $this->request->data;
        if($this->request->is('post') && $params['hospital_id']>0){
            $this->Session->write('ad_hospital_id', intval($params['hospital_id']));
            if($tp==0)
                $this->redirect('/mobile/release/');
            else if($tp==1)
                $this->redirect('/mobile/release/1.html');
            else if($tp==2)
                $this->redirect('/mobile/release_ans');
        }
        $tp = $this->rm_end_html($tp);
        $this->loadModel('Hospital');
        $hospitals = $this->Hospital->find('all',array(
            'conditions'=>array('status'=>1),
            'fields'=>array('id','cn_name'),
            )
        );
        $hospital_id=$this->Session->read('ad_hospital_id');
        $this->set('hospitalList',$hospitals);
        $this->set('hospital_id',intval($hospital_id));
        $this->set('tp',$tp);
    }
    function pub($t = null){
        $this->set('nav','cindex');
        $this->checkLogin(2);
        $params = $this->get_params($t);
        $type = isset($params[0])?intval($params[0]):0;
        $cid = isset($params[1])?intval($params[1]):0;
        $attrType=[1,2,3];
        if(!in_array($type, $attrType) || $cid <=0)
            die("非法请求");
        $params = $this->request->data;
        $commentTopic=Configure::read('COMMENT_TOPIC');

        if($this->request->is('post')){
            if($cid <=0){
                $this->ajaxReturn(4008, '非法请求!');
            }
            if(empty($params['content'])){
                $this->ajaxReturn(4007, '内容不能为空!');
            }
            if($type==1){
                //攻略评论
                $this->loadModel('Article');
                $this->loadModel('Comment');
                $info = $this->Article->find('first',array('conditions'=>array('status'=>1,'id'=>intval($cid))));
                if(empty($info))
                    $this->ajaxReturn(4006, '非法提交!');

                $cinfo = $this->Comment->find('first',array('conditions'=>array('topic_type'=>$commentTopic['攻略'],'reply_id'=>0,'topic_id'=>$info['Article']['id'],'uid'=>$this->user['id'])));
                if(!empty($cinfo))
                    $this->ajaxReturn(4009, '不能重复评论!');
                //添加评论
                $data=[];
                $data['topic_type']=$commentTopic['攻略'];
                $data['topic_id']=$info['Article']['id'];
                $data['uid'] = $this->user['id'];
                $data['sort'] = 1100;
                $data['content'] = $params['content'];
                $data['comment_time']=date("Y-m-d H:i:s",time());
                $data['create_time']=date("Y-m-d H:i:s",time());
                $this->Comment->save($data);
                $data=[];
                $data['id']=$info['Article']['id'];
                $data['comment_num']=$info['Article']['comment_num']+1;
                $this->Article->save($data);
                $this->ajaxReturn(200, '发表成功！',array('regerurl'=>'/plan/info/'.$info['Article']['id'].".html"));
            }
            else if($type==2){
                //分享评论
                $this->loadModel('Share');
                $this->loadModel('Comment');
                $info = $this->Share->find('first',array('conditions'=>array('status'=>1,'type'=>0,'id'=>intval($cid))));
                if(empty($info))
                    $this->ajaxReturn(4006, '非法提交!');

                $cinfo = $this->Comment->find('first',array('conditions'=>array('topic_type'=>$commentTopic['分享'],'reply_id'=>0,'topic_id'=>$info['Share']['id'],'uid'=>$this->user['id'])));
                if(!empty($cinfo))
                    $this->ajaxReturn(4009, '不能重复评论!');
                //添加评论
                $data=[];
                $data['topic_type']=$commentTopic['分享'];
                $data['topic_id']=$info['Share']['id'];
                $data['uid'] = $this->user['id'];
                $data['sort'] = 1100;
                $data['content'] = $params['content'];
                $data['comment_time']=date("Y-m-d H:i:s",time());
                $data['create_time']=date("Y-m-d H:i:s",time());
                $this->Comment->save($data);
                $data=[];
                $data['id']=$info['Share']['id'];
                if($info['Share']['is_replay']==0){
                    $data['is_replay']=1;
                }
                $data['comment_num']=$info['Share']['comment_num']+1;
                $this->Share->save($data);
                $this->ajaxReturn(200, '发表成功！',array('regerurl'=>'/share/info/'.$info['Share']['id'].".html"));
            }
            else if($type==3){
                //问答评论
                $this->loadModel('Share');
                $this->loadModel('Comment');
                $info = $this->Share->find('first',array('conditions'=>array('status'=>1,'type'=>1,'id'=>intval($cid))));
                if(empty($info))
                    $this->ajaxReturn(4006, '非法提交!');

                $cinfo = $this->Comment->find('first',array('conditions'=>array('topic_type'=>$commentTopic['问答'],'reply_id'=>0,'topic_id'=>$info['Share']['id'],'reply_id'=>0,'uid'=>$this->user['id'])));
                if(!empty($cinfo))
                    $this->ajaxReturn(4009, '不能重复评论!');
                //添加评论
                $data=[];
                $data['topic_type']=$commentTopic['问答'];
                $data['topic_id']=$info['Share']['id'];
                $data['uid'] = $this->user['id'];
                $data['sort'] = 1100;
                $data['content'] = $params['content'];
                $data['comment_time']=date("Y-m-d H:i:s",time());
                $data['create_time']=date("Y-m-d H:i:s",time());
                $this->Comment->save($data);
                $data=[];
                $data['id']=$info['Share']['id'];
                if($info['Share']['is_replay']==0){
                    $data['is_replay']=1;
                }
                $data['comment_num']=$info['Share']['comment_num']+1;
                $this->Share->save($data);
                $this->ajaxReturn(200, '发表成功！',array('regerurl'=>'/answer/info/'.$info['Share']['id'].".html"));
            }
        }
        $this->set('type',$type);
        $this->set('cid',$cid);
    }
}