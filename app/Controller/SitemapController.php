<?php
// 网站地图
class SitemapController extends AppController {

    public $layout = 'main';

    function beforeFilter() {
        parent::beforeFilter();
        $this->set('nav','sitemap');  
    }

    function index(){ 
        $this->loadModel('MapCate');   
        $this->loadModel('Map');   
        $cates = $this->MapCate->find('all',array('conditions'=>array('parent_id'=>0,'status'=>1),'order'=>array('sort'=>'desc','id'=>'desc'))); 
        $dataList=[];
        if(!empty($cates)){
        	foreach ($cates as $key => $value) {
        		$dataList[$key]=$value;
    			$mapList = $this->Map->find('all',array('conditions'=>array('parent_id'=>$value['MapCate']['id'],'status'=>1),'order'=>array('sort'=>'desc','id'=>'desc')));  
        		$dataList[$key]['MapCate']['map']=$mapList;
        	}
        } 
        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['网站地图']),'order'=>array('id'=>'desc'))); 
        $this->set('keysInfo',$keysInfo);
        $this->set('dataList',$dataList);

    }

}