<?php
// 试管头条页面
class HeadlinesController extends AppController {

    public $layout = 'main';

    function beforeFilter() {
        parent::beforeFilter();
        $this->set('nav','headlines');  
    }

    function index($p = 1){ 
     	$this->loadModel('New');     
        $p = $this->rm_end_html($p); 
        $pageSize=5;
        $orderby=[];  
        $orderby['sort']="desc";
        $orderby['create_time']="desc";
        $conditions['status'] = 1; 
        $conditions['category_id'] = 5; 
        $dataList =[]; 
        $dataList = $this->New->find('all',array(
            'conditions' => $conditions, 
            'limit' => 5,
            'page' => $p,
            'order' => $orderby,
            )
        );
        $dataCount = $this->New->find('count',array('conditions' => $conditions));
        $strPages=$this->htmlPage($p, $dataCount, $p,$pageSize);   
        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['常见问题栏目']),'order'=>array('id'=>'desc'))); 
        $this->set('keysInfo',$keysInfo);
        $this->set('dataList',$dataList);
        $this->set('strPages',$strPages);
        $this->set('dataCount',$dataCount);
    }
    function info($id = 0){
        if(empty($id)){
            $this->redirect('/');
        }
        $id = $this->rm_end_html($id);
        $this->loadModel('New'); 
        $this->loadModel('ArticleCate');   
        $info = $this->New->find('first',array('conditions'=>array('status'=>1,'id'=>$id)));
        if(empty($info)){
            //处理错误跳转
            die("非法请求");
        } 
        $cateInfo = $this->ArticleCate->findById($info['New']['category_id']); 
        $cate_name='';
        if(!empty($cateInfo))
            $cate_name=$cateInfo['ArticleCate']['name'];
        $info['New']['username']="试管无忧";
        $info['New']['avatar']="/images/temp/head.jpg";

        $orderby=[];  
        $orderby['sort']="desc";
        $orderby['create_time']="desc";
        $conditions['status'] = 1;
        $conditions['is_home'] = 1; 
        $top4List =[]; 
        $top4List = $this->New->find('all',array(
            'conditions' => $conditions, 
            'limit' => 4,
            'order' => $orderby,
            )
        );

        //浏览数+1 
        $this->New->updateAll(array('New.browse_num'=>'`New`.`browse_num`+1'), array('New.id'=>$id)); 
        $this->set('info',$info); 
        $this->set('cate_name',$cate_name);  
        $this->set('top4List',$top4List);  
    }
}