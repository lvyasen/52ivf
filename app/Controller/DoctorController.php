<?php
//社区医生页面
class DoctorController extends AppController {

    public $layout = 'main';

    function beforeFilter() {
        parent::beforeFilter();
    }
	// 医生首页
    function index($cid = null){
        $params = $this->request->query;
        $paramDates = $this->request->data; 
        $this->loadModel('Doctor');
		if($this->request->is('post') && $paramDates['dosubmit']==1){ 
            if($paramDates['did'] <= 0){
                $this->ajaxReturn(4001, '请选择预约医生!');  
            }
			 
			$info = $this->Doctor->find('first',array('conditions'=>array('status'=>1,'id'=>intval($paramDates['did']))));
			if(empty($info)){
                $this->ajaxReturn(4002, '医生不存在!');  
			}
            if(empty($paramDates['u_mobile'])){
                $this->ajaxReturn(4003, '手机号不能为空!');  
            }
            if(!Ivf::isMobile($paramDates['u_mobile']))
                $this->ajaxReturn(4005, '请输入有效的11位手机号码!');   
			
			$code = $this->Session->read('mobile_code');
            if(($paramDates['u_mobile'] != $code['mobile']) || ($paramDates['u_code'] != $code['code'])) 
                    $this->ajaxReturn(4006, '手机验证码有误！');
				
            $this->loadModel('UsersAppoint');  
    		$dataList = $this->UsersAppoint->find('first',array('conditions'=>array('status'=>0,'mobile'=>$paramDates['u_mobile'],'source'=>'ivf52-Pc医生列表页')));
            if(!empty($dataList))
                $this->ajaxReturn(4007, '不能重复预约！'); 
            
			$user = $this->getUser();  
            $db_data = array();
            $db_data['types'] =  0; 
            $db_data['uid'] =  isset($user['id'])?$user['id']:0; 
            // $db_data['user_name'] =  isset($user['user_name'])?$user['user_name']:''; 
            $db_data['user_name'] = $paramDates['user_name']; 
            $db_data['hospital_id'] =  $info['Doctor']['hospital_id']; 
            $db_data['doctor_id'] =  $paramDates['did']; 
            $db_data['mobile'] =  $paramDates['u_mobile'];
            $db_data['source'] = "ivf52-Pc医生列表页";
            $db_data['source_url'] = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $db_data['status'] = 0; 
            $db_data['system'] = Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT']));
            $db_data['brand'] = Ivf::getOSBrand(strtolower($_SERVER['HTTP_USER_AGENT'])); 
            $db_data['description'] = $paramDates['description'];  
            $db_data['ip'] = Ivf::getClientIp();; 
            $db_data['create_time'] = date("Y-m-d H:i:s",time()); 
            if(!$this->UsersAppoint->save($db_data)) 
                    $this->ajaxReturn(4008, '预约失败！'); 
              
            $this->ajaxReturn(200, '申请成功！');   
        }
        //区域ID
        $cid = $this->rm_end_html($cid);
        $this->loadModel('District'); 
        $this->loadModel('Hospital');
        $conditions =$coments = array();
        $orderby=[];
        if(!empty($cid)){
            if(!is_numeric($cid)){
                //处理错误跳转
                die("非法请求");
            }
        	$datas = $this->District->findById($cid);
        	if(empty($datas)){
            	//处理错误跳转  
                die("非法请求");
        	}
        	$hopitalDate = $this->Hospital->find('all',array('conditions'=>array('city_id'=>$cid,'status'=>1),'fields'=>array('id')));
        	if(!empty($hopitalDate)){
        		foreach ($hopitalDate as $key => $value) {
        			$hosAttr[]=$value['Hospital']['id'];
        		}
            	$conditions['id'] = $hosAttr;
        	}
        	else 
            	$conditions['id'] = 1; 
        } 
        
        $orderby['sort']="asc"; 
        $orderby['rec']="desc";
        $orderby['create_time']="desc";
        $conditions['status'] = 1;
        $dataList = array(); 
        $dataList = $this->Doctor->find('all',array(
            'conditions' => $conditions, 
            'limit' => 6,
            'order' => $orderby,
            )
        );
        if(!empty($dataList)){
            foreach ($dataList as $key => $value) { 
                $dataList[$key]['Doctor']['hospital']="无";
                if(intval($value['Doctor']['hospital_id'])>0){ 
                    $info = $this->Hospital->findById(intval($value['Doctor']['hospital_id']));
                    if(isset($info['Hospital']['id']))
                        $dataList[$key]['Doctor']['hospital']=$info['Hospital']['cn_name'];
                }  
            }
        }
        
        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['医生列表']),'order'=>array('id'=>'desc'))); 
        $this->set('keysInfo',$keysInfo); 
        $districtList = $this->District->find('all',array('order'=>array('sort'=>'desc','id'=>'asc'),'fields'=>array('id','country'))); 
		$this->set('dataList',$dataList);
        $this->set('districtList',$districtList); 
        $this->set('education',Configure::read('EDUCATION')); 
        $this->set('cid',$cid); 
    }
	// 医生详情
    function info($id = 0){
        if(empty($id)){
            $this->redirect('/');
        }
        $id = $this->rm_end_html($id);
        $this->loadModel('Doctor');
        $this->loadModel('Comment'); 
        $this->loadModel('Hospital'); 
        $this->loadModel('User'); 
    	$info = $this->Doctor->find('first',array('conditions'=>array('status'=>1,'id'=>$id)));
        if(empty($info)){
            //处理错误跳转
            die("非法请求");
        }  
        $info['Doctor']['hospital']='无';
        if($info['Doctor']['hospital_id'] > 0){  
            $hospitalInfo = $this->Hospital->findById($info['Doctor']['hospital_id']); 
            if(!empty($hospitalInfo))
                 $info['Doctor']['hospital']=$hospitalInfo['Hospital']['cn_name']; 
        }
        
        $commentTopic=Configure::read('COMMENT_TOPIC');
        $comments = $this->Comment->find('all',array('conditions'=>array('topic_type'=>$commentTopic['医生'],'parent_id'=>0,'topic_id'=>$id),'order'=>array('is_top'=>'desc','sort'=>'desc','comment_time' => 'desc'),'limit' => 5));
        if(!empty($comments)){
            foreach ($comments as $key => $value) { 
                $userInfo = $this->User->findById($value['Comment']['uid']);   
                $comments[$key]['Comment']['username']=isset($userInfo['User']['nick_name'])?$userInfo['User']['nick_name']:"保密";
                $comments[$key]['Comment']['avatar']=$userInfo['User']['avatar'];
            }
        }
        $keysInfo=[];
        $keysInfo['Keyword']['meta_title']=$info['Doctor']['cn_name'].' - 试管无忧'; 
        $keysInfo['Keyword']['meta_keywords']=$info['Doctor']['cn_name']; 
        $keysInfo['Keyword']['meta_description']=Ivf::getSubstr(strip_tags($info['Doctor']['introduction']),0,200,''); 
        $this->set('keysInfo',$keysInfo); 
        $this->set('comments',$comments); 
        $this->set('info',$info);  
        $this->set('education',Configure::read('EDUCATION'));  
    } 
}