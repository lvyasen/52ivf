<?php
class AboutController extends AppController {

    public $layout = 'main';

    function beforeFilter() {
        parent::beforeFilter();
        $this->set('nav','about'); 
    }

    function index(){ 
        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['关于我们']),'order'=>array('id'=>'desc'))); 
        $this->set('keysInfo',$keysInfo);
    }

    function info(){
        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['关于我们']),'order'=>array('id'=>'desc'))); 
        $this->set('keysInfo',$keysInfo);
    }


}