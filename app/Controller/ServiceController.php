<?php
class ServiceController extends AppController {

    public $layout = 'main';

    function beforeFilter() {
        parent::beforeFilter();
    }
    //远程咨询
    function remote(){
        $params = $this->request->data;
        if($this->request->is('post') && $params['dosubmit']==1){ 
            if(empty($params['u_mobile'])){
                $this->ajaxReturn(4001, '手机号不能为空!');  
            }
            if(!Ivf::isMobile($params['u_mobile']))
                $this->ajaxReturn(4002, '请输入有效的11位手机号码!');   

            $this->loadModel('UsersAppoint');  
            $dataList = $this->UsersAppoint->find('first',array('conditions'=>array('status'=>0,'mobile'=>$params['u_mobile'],'source'=>'ivf52-远程咨询')));
            if(!empty($dataList))
                $this->ajaxReturn(4003, '不能重复提交！'); 
  
            $db_data = array();
            $db_data['types'] =  isset($params['symptoms'])?intval($params['symptoms']):0;
            $db_data['user_name'] =  $params['u_name'];
            $db_data['mobile'] =  $params['u_mobile'];
            $db_data['age'] =  $params['u_age'];
            $db_data['system'] = Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT']));
            $db_data['brand'] = Ivf::getOSBrand(strtolower($_SERVER['HTTP_USER_AGENT'])); 
            $db_data['hospital_id'] =  isset($params['u_hospital'])?intval($params['u_hospital']):0;
            $db_data['source'] = "ivf52-远程咨询";
            $db_data['source_url'] = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $db_data['status'] = 0; 
            $db_data['ip'] = Ivf::getClientIp();; 
            $db_data['create_time'] = date("Y-m-d H:i:s",time()); 
            if(!$this->UsersAppoint->save($db_data)) 
                    $this->ajaxReturn(4005, '预约失败！'); 
              
            $this->ajaxReturn(200, '申请成功！');   
        }  
        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['远程咨询']),'order'=>array('id'=>'desc'))); 
        $this->set('keysInfo',$keysInfo);

        $this->loadModel('Hospital');
        $hopital = $this->Hospital->find('all',array('conditions'=>array('status'=>1),'order'=>array('sort'=>'desc','id'=>'desc'),'fields'=>array('id','en_name','cn_name')));
        $this->set('hopital',$hopital);  
        $this->set('disease',Configure::read('DISEASE_TOPIC'));  
        $this->set('nav','remote');  
    }
    //精准预约
    function booking(){

        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['精准预约']),'order'=>array('id'=>'desc'))); 
        $this->set('keysInfo',$keysInfo);
        
        $this->set('nav','booking');  
    }
    //VIP尊享
    function vip(){ 
        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['VIP尊享']),'order'=>array('id'=>'desc'))); 
        $this->set('keysInfo',$keysInfo);
        
    }
    //金牌无忧
    function gold(){ 
        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['金牌无忧']),'order'=>array('id'=>'desc'))); 
        $this->set('keysInfo',$keysInfo);
        
    }
}