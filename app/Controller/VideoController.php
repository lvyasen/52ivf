<?php
// 视频
class VideoController extends AppController {

    public $layout = 'media';

    function beforeFilter() {
        parent::beforeFilter();
        $this->user = $this->getUser();  
        $this->set('nav','video');  
    }

    function index(){ 
        $this->loadModel('VideoCate');   
        $this->loadModel('Video');   
        $topVideo = $this->VideoCate->find('all',array('conditions' => array('status'=>1,'is_top'=>1,'parent_id'=>0)),array('order'=>array('sort'=>'desc','id'=>'desc'))); 
        if(!empty($topVideo)){
            foreach ($topVideo as $key => $value) { 
                $topVideo[$key]['VideoCate']['sub'] = $this->Video->find('all',array('conditions' => array('status'=>1,'parent_id'=>$value['VideoCate']['id']),'order'=>array('sort'=>'desc','id'=>'desc'),'fields'=>array('id','title','create_time'))); 
               
            }
        } 
        $cateVideo = $this->VideoCate->find('all',array('conditions' => array('status'=>1,'is_top'=>0,'parent_id'=>0)),array('order'=>array('sort'=>'desc','id'=>'desc'))); 
        if(!empty($cateVideo)){
            foreach ($cateVideo as $key => $value) { 
                $cateVideo[$key]['VideoCate']['sub'] = $this->Video->find('all',array('conditions' => array('status'=>1,'parent_id'=>$value['VideoCate']['id']),'order'=>array('rec'=>'desc','sort'=>'desc','id'=>'desc'),'fields'=>array('id','title','play_num','comment_num','img'),'limit'=>5)); 
               
            }
        }    
        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['视频自媒体']),'order'=>array('id'=>'desc'))); 
        $this->set('keysInfo',$keysInfo);
		$this->set('cateVideo',$cateVideo);
        $this->set('topVideo',$topVideo);
    }
    function info ($id = 0){
        $id = $this->rm_end_html($id);
        if(empty($id)){
            $this->redirect('/');
        }
        $this->loadModel('VideoCate');  
        $this->loadModel('Video');    
        $this->loadModel('Comment');    
        $this->loadModel('User');    
        $this->loadModel('Agree'); 
        $userId=intval($this->user['id']);   
        $agreeTopic=Configure::read('AGREE_TOPIC');
        $commentTopic=Configure::read('COMMENT_TOPIC');
        $info = $this->Video->find('first',array('conditions'=>array('status'=>1,'id'=>$id)));
        if(empty($info)){
            //处理错误跳转
            die("非法请求");
        }  
        $cateInfo = $this->VideoCate->findById($info['Video']['parent_id']);  

        $nextVideo = $this->Video->find('all',array('conditions'=>array('status'=>1,'parent_id'=>$info['Video']['parent_id'],'id !='=>$info['Video']['id']),'fields'=>array('id','title','play_num','img'),'limit'=>20)); 
        array_unshift($nextVideo,$info);
        $hotVideo = $this->Video->find('all',array('conditions'=>array('status'=>1),'order'=>array('search_num'=>'desc'),'fields'=>array('id','title','play_num','description','img'),'limit'=>10));  
        $comments = $this->Comment->find('all',array('conditions'=>array('topic_type'=>$commentTopic['视频'],'parent_id'=>0,'topic_id'=>$info['Video']['parent_id']),'order'=>array('is_top'=>'desc','sort'=>'desc','comment_time' => 'desc'),'limit' => 10));
        $cmtCount = $this->Comment->find('count',array('conditions'=>array('topic_type'=>$commentTopic['视频'],'parent_id'=>0,'topic_id'=>$info['Video']['parent_id'])));
        if(!empty($comments)){
            foreach ($comments as $key => $value) { 
                $userInfo = $this->User->findById($value['Comment']['uid']);   
                $comments[$key]['Comment']['username']=isset($userInfo['User']['nick_name'])?$userInfo['User']['nick_name']:"保密";
                $comments[$key]['Comment']['avatar']=$userInfo['User']['avatar'];
            }
        }  
        $agCount = $this->Agree->find('count',array('conditions'=>array('uid'=>$userId,'obj_id'=>$info['Video']['id'],'obj_type'=>$agreeTopic['视频'])));
        $info['Video']['myagree']=$agCount;
        //播放数+1 
        $this->Video->updateAll(array('Video.play_num'=>'`Video`.`play_num`+1'), array('Video.id'=>$id));
        $keysInfo=[];
        $keysInfo['Keyword']['meta_title']=$info['Video']['title'].'_视频自媒体  - 试管邦社区'; 
        $keysInfo['Keyword']['meta_keywords']=$info['Video']['title'].",视频自媒体"; 
        $keysInfo['Keyword']['meta_description']=Ivf::getSubstr(strip_tags($info['Video']['description']),0,200,'');  
        $this->set('info',$info); 
        $this->set('keysInfo',$keysInfo);  
        $this->set('cateInfo',$cateInfo);  
        $this->set('comments',$comments);  
        $this->set('cmtCount',$cmtCount);  
        $this->set('nextVideo',$nextVideo);  
        $this->set('hotVideo',$hotVideo);  
    } 
 
    function replay(){ 
        // 发表评论   
        $this->checkLogin(1);  
        $params = $this->request->data;
        $commentTopic=Configure::read('COMMENT_TOPIC');
        $this->loadModel('Video');   
        $this->loadModel('Comment');  
        if($this->request->is('post') && $params['dosubmit']==1){ 
            if($params['cid'] <=0){
                $this->ajaxReturn(4008, '非法请求!');  
            } 
 
            $info = $this->Video->find('first',array('conditions'=>array('status'=>1,'id'=>intval($params['cid']))));
            if(empty($info))
                $this->ajaxReturn(4006, '非法提交!');   

            $cinfo = $this->Comment->find('first',array('conditions'=>array('topic_type'=>$commentTopic['视频'],'topic_id'=>$info['Video']['parent_id'],'uid'=>$this->user['id'])));
            if(!empty($cinfo))
                $this->ajaxReturn(4009, '不能重复评论!');  
            //添加评论
            $data=[];
            $data['topic_type']=$commentTopic['视频'];
            $data['topic_id']=$info['Video']['parent_id'];
            $data['uid'] = $this->user['id']; 
            $data['sort'] = 1100; 
            $data['content'] = $params['content'];   
            $data['comment_time']=date("Y-m-d H:i:s",time());
            $data['create_time']=date("Y-m-d H:i:s",time());
            $this->Comment->save($data); 
            $this->ajaxReturn(200, '发表成功！');   
        }
        $this->ajaxReturn(4007, '异常提交！');    
    }  
}