<?php
class SiteController extends AppController {

    public $layout = 'main';

    function beforeFilter() {
        parent::beforeFilter();
        $this->set('nav','site');  
    }

    function index(){

        $this->loadModel('SiteComment');  
        $params = $this->request->data;  
    	if($this->request->is('post') && $params['dosubmit']==1){ 
            if(empty($params['u_mobile'])){
                $this->ajaxReturn(4001, '手机号不能为空!');  
            }
            if(!Ivf::isMobile($params['u_mobile']))
                $this->ajaxReturn(4002, '请输入有效的11位手机号码!');   

            $this->loadModel('UsersAppoint');  
    		$dataList = $this->UsersAppoint->find('first',array('conditions'=>array('status'=>0,'mobile'=>$params['u_mobile'],'source'=>'ivf52-赴泰页')));
            if(!empty($dataList))
                $this->ajaxReturn(4003, '不能重复提交！'); 
  
            $db_data = array();
            $db_data['types'] =  0;
            $db_data['user_name'] =  $params['u_name'];
            $db_data['mobile'] =  $params['u_mobile'];
            $db_data['system'] = Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT']));
            $db_data['brand'] = Ivf::getOSBrand(strtolower($_SERVER['HTTP_USER_AGENT'])); 
            $db_data['source'] = "ivf52-赴泰页";
            $db_data['source_url'] = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $db_data['status'] = 0; 
            $db_data['ip'] = Ivf::getClientIp();; 
            $db_data['create_time'] = date("Y-m-d H:i:s",time()); 
            if(!$this->UsersAppoint->save($db_data)) 
                    $this->ajaxReturn(4005, '预约失败！'); 
              
            $this->ajaxReturn(200, '申请成功，我们将在1-2个工作日与您取得联系!');   
        } 
        //点评列表
        $comments = $this->SiteComment->find('all',array('order'=>array('sort'=>'desc','comment_time' => 'desc'),'limit' => 10)); 
        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['活动页面']),'order'=>array('id'=>'desc'))); 
        $this->set('keysInfo',$keysInfo);
        $this->set('comments',$comments);   

        $ua = Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT']));
        if($ua != 'pc')
            $this -> render('minfo');  
        else
            $this -> render('info');  
    } 
    function comment(){ 
        // 发表点评    
        $params = $this->request->data;  
        $this->loadModel('SiteComment');  
        $this->loadModel('UsersAppoint');  
        if($this->request->is('post') && $params['dosubmit']==1){ 
    		$info = $this->UsersAppoint->find('all',array('conditions'=>array('status'=>1,'mobile'=>$params['u_mobile'],'source'=>'ivf52-赴泰页')));
            if(empty($info))
                $this->ajaxReturn(4006, '您未申请活动!');   

            $cinfo = $this->SiteComment->find('all',array('conditions'=>array('mobile'=>$params['u_mobile'])));
            if(!empty($cinfo))
                $this->ajaxReturn(4009, '不能重复点评!');  
            //添加评论
            $data=[];
            $data['mobile']=$params['u_mobile'];  
            $data['sort'] = 1100; 
            $data['content'] = $params['content'];   
            $data['comment_time']=date("Y-m-d H:i:s",time());
            $data['create_time']=date("Y-m-d H:i:s",time());
            $this->SiteComment->save($data); 
            $this->ajaxReturn(200, '点评成功！');   
        }
        $this->ajaxReturn(4007, '异常提交！');    
    }
}