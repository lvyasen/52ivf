<?php
// 动态页面
class SpecialController extends AppController {

    public $layout = 'main';

    function beforeFilter() {
        parent::beforeFilter();
    }

    function index(){ 
        $uid = 117;  
		$this->loadModel('Special'); 
        $dataList = $this->Special->findById($uid);    
        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['自定义专题']),'order'=>array('id'=>'desc')));  
        $this->set('keysInfo',$keysInfo);
        $this->set('info',$dataList);
    }

    function info($id = 0){
        if(empty($id)){
            $this->redirect('/');
        }
        $uid = $this->rm_end_html($id);
        $this->loadModel('Special');    
        $dataList = $this->Special->find("first",array('conditions'=>array('id'=>$uid),'fields'=>array('*')));   
        $keysInfo['Keyword']['meta_title']=$dataList['Special']['meta_title']; 
        $keysInfo['Keyword']['meta_keywords']=$dataList['Special']['meta_keywords']; 
        $keysInfo['Keyword']['meta_description']=$dataList['Special']['meta_description'];  
        $this->set('keysInfo',$keysInfo);
        $this->set('info',$dataList);
        $this->set('uid',$uid);
        $this->loadModel('UsersAppoint'); 
        $params = $this->request->data;
       if($this->request->is('post')){ 
            if(empty($params['user_name'])){
                $this->ajaxReturn(4001, '用户名不能为空!');  
            }
            if(empty($params['age'])){
                $this->ajaxReturn(4001, '年龄不能为空!');  
            }
            if(empty($params['mobile'])){
                $this->ajaxReturn(4001, '手机号不能为空!');  
            }
            if(!Ivf::isMobile($params['mobile']))
            $this->ajaxReturn(4002, '请输入有效的11位手机号码!');    
            $dataList = $this->UsersAppoint->find('first',array('conditions'=>array('status'=>0,'mobile'=>$params['mobile'],'source'=>'ivf91-专题页')));
        if(!empty($dataList))
             $this->ajaxReturn(4003, '不能重复提交！'); 
            $db_data = array();
                $db_data['types'] =  0;
                $db_data['user_name'] =  $params['user_name'];
                $db_data['mobile'] =  $params['mobile'];
                $db_data['age'] =  $params['age'];
                $db_data['system'] = Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT']));
                $db_data['brand'] = Ivf::getOSBrand(strtolower($_SERVER['HTTP_USER_AGENT'])); 
                $db_data['source'] = "ivf52-专题页";
                $db_data['source_url'] = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                $db_data['status'] = 0; 
                $db_data['ip'] = Ivf::getClientIp();; 
                $db_data['create_time'] = date("Y-m-d H:i:s",time()); 
                $this->UsersAppoint->save($db_data);
                if(!$this->UsersAppoint->save($db_data)) 
                        $this->ajaxReturn(4005, '预约失败！'); 
                $this->ajaxReturn(200, '预约成功'); 
            }  
    }
}