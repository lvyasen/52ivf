<?php
// 新闻资讯页面
class NewsController extends AppController {

    public $layout = 'main';
    private $links = null;

    function beforeFilter() {
        parent::beforeFilter();
        $this->set('nav','news');  
    }

    function index($p = 1){
     	$this->loadModel('New');
        $p = $this->rm_end_html($p); 
        $pageSize=5;
        $orderby=[];  
        $orderby['sort']="desc";
        $orderby['create_time']="desc";
        $conditions['status'] = 1; 
        $conditions['category_id'] = 4; 
        $dataList =[]; 
        $dataList = $this->New->find('all',array(
            'conditions' => $conditions, 
            'limit' => $pageSize,
            'page' => $p,
            'order' => $orderby,
            )
        );
        $dataCount = $this->New->find('count',array('conditions' => $conditions));
        $strPages=$this->htmlPage($p, $dataCount, $p,$pageSize);   
        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['新闻资讯栏目']),'order'=>array('id'=>'desc'))); 
        $this->set('keysInfo',$keysInfo);
        $this->set('dataList',$dataList);
        $this->set('strPages',$strPages);
        $this->set('dataCount',$dataCount);
    }
    function info($id = 0){
        if(empty($id)){
            $this->redirect('/');
        }
        $id = $this->rm_end_html($id);
        $this->loadModel('New'); 
        $this->loadModel('ArticleCate');   
        $info = $this->New->find('first',array('conditions'=>array('status'=>1,'id'=>$id)));
        if(empty($info)){
            //处理错误跳转
            die("非法请求");
        } 
        $cateInfo = $this->ArticleCate->findById($info['New']['category_id']);  
        $cate_name='';
        if(!empty($cateInfo))
            $cate_name=$cateInfo['ArticleCate']['name'];
        $info['New']['username']="试管无忧";
        $info['New']['avatar']="/images/temp/head.jpg";

        $orderby=[];  
        $orderby['sort']="desc";
        $orderby['create_time']="desc";
        $conditions['status'] = 1;
        $conditions['is_home'] = 1; 
        $top4List =[]; 
        $top4List = $this->New->find('all',array(
            'conditions' => $conditions, 
            'limit' => 3,
            'order' => $orderby,
            )
        );
        if($info["New"]["category_id"]==5)
            $this->set('nav','headlines');  
        $next=$prev=0;
        //获取上一篇
        $prevData = $this->New->find('first',array('conditions'=>array('id <'=>$id,'status'=>1),'order'=>array('id'=>'desc'),'fields'=>array('id','title')));   
        if(!empty($prevData)){
            $prev=$prevData['New']['id'];
            $prev_title=$prevData['New']['title'];
        }
        //获取下一篇 
        $nextData = $this->New->find('first',array('conditions'=>array('id >'=>$id,'status'=>1),'order'=>array('id'=>'desc'),'fields'=>array('id','title')));   
        if(!empty($nextData)){
            $next=$nextData['New']['id']; 
            $next_title=$nextData['New']['title']; 
        }
        //浏览数+1 
        $this->New->updateAll(array('New.browse_num'=>'`New`.`browse_num`+1'), array('New.id'=>$id)); 
        $keysInfo=[];
        $keysInfo['Keyword']['meta_title']=$info['New']['title'].'- 试管无忧'; 
        $keysInfo['Keyword']['meta_keywords']=$info['New']['title']; 
        $keysInfo['Keyword']['meta_description']=Ivf::getSubstr(strip_tags($info['New']['description']),0,200,''); 
        $this->set('keysInfo',$keysInfo); 
        $this->set('info',$info); 
        $this->set('next',$next); 
        $this->set('prev',$prev); 
        if(!empty($nextData))
        $this->set('next_title',$next_title); 
        if(!empty($prevData))
        $this->set('prev_title',$prev_title);
        $this->set('cate_name',$cate_name);  
        $this->set('top4List',$top4List);  
    }
}