<?php

// 社区热门医院页面
class HospitalController extends AppController {

    public $layout = 'main';

    function beforeFilter() {
        parent::beforeFilter();
        $this->set('nav','hospital');  
    }

	// 医院列表页
    function index($cid = null){
        $params = $this->request->query;    //获取get
        $paramDates = $this->request->data;   //获取post
		$this->loadModel('Hospital');  
        if($this->request->is('post') && $paramDates['dosubmit']==1){ 
            if($paramDates['hid'] <= 0){
                $this->ajaxReturn(4001, '请选择预约医院!');  
            }
			
			$info = $this->Hospital->find('first',array('conditions'=>array('status'=>1,'id'=>intval($paramDates['hid']))));
			if(empty($info)){
                $this->ajaxReturn(4002, '医院不存在!');  
			}
            if(empty($paramDates['u_mobile'])){
                $this->ajaxReturn(4003, '手机号不能为空!');  
            }
            if(!Ivf::isMobile($paramDates['u_mobile']))
                $this->ajaxReturn(4005, '请输入有效的11位手机号码!');   
			
			$code = $this->Session->read('mobile_code');
            if(($paramDates['u_mobile'] != $code['mobile']) || ($paramDates['u_code'] != $code['code'])) 
                    $this->ajaxReturn(4006, '手机验证码有误！');
				
            $this->loadModel('UsersAppoint');  
    		$dataList = $this->UsersAppoint->find('first',array('conditions'=>array('status'=>0,'mobile'=>$paramDates['u_mobile'],'source'=>'ivf52-Pc医院列表页')));
            if(!empty($dataList))
                $this->ajaxReturn(4007, '不能重复预约！'); 

			$user = $this->getUser();  
            $db_data = array();
            $db_data['types'] =  0; 
            $db_data['uid'] =  isset($user['id'])?$user['id']:0; 
            // $db_data['user_name'] =  isset($user['user_name'])?$user['user_name']:''; 
            $db_data['user_name'] = $paramDates['user_name']; 
            $db_data['hospital_id'] =  $paramDates['hid']; 
            $db_data['mobile'] =  $paramDates['u_mobile'];
            $db_data['source'] = "ivf52-Pc医院列表页";
            $db_data['source_url'] = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $db_data['status'] = 0; 
            $db_data['description'] = $paramDates['description'];  
            $db_data['ip'] = Ivf::getClientIp(); 
            $db_data['system'] = Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT']));
            $db_data['brand'] = Ivf::getOSBrand(strtolower($_SERVER['HTTP_USER_AGENT'])); 
            $db_data['create_time'] = date("Y-m-d H:i:s",time()); 
            if(!$this->UsersAppoint->save($db_data)) 
                    $this->ajaxReturn(4008, '预约失败！'); 
              
            $this->ajaxReturn(200, '申请成功！');   
        }
        //区域ID
        $cid = $this->rm_end_html($cid); 
        $this->loadModel('District'); 
        $conditions =$coments = array();
        $orderby=[];
        if(!empty($cid)){
            if(!is_numeric($cid)){
                //处理错误跳转
                die("非法请求");
            }
            $conditions['city_id'] = intval($cid);
        	$datas = $this->District->findById($cid);
        	if(empty($datas)){
            	//处理错误跳转 
                die("非法请求");
        	}
        } 
         
        $orderby['sort']="desc";
        $orderby['create_time']="desc";
        $conditions['status'] = 1;
        $dataList = array(); 
        $dataList = $this->Hospital->find('all',array(
            'conditions' => $conditions, 
            'limit' => 6,
            'order' => $orderby,
            )
        );
  
        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['医院列表']),'order'=>array('id'=>'desc'))); 
        $this->set('keysInfo',$keysInfo);
        $districtList = $this->District->find('all',array('order'=>array('sort'=>'desc','id'=>'asc'),'fields'=>array('id','country')));
        $this->set('dataList',$dataList);
        $this->set('districtList',$districtList); 
        $this->set('cid',$cid); 
    }
	// 医院详情页
    function info($id = 0){
        if(empty($id)){
            $this->redirect('/');
        }
        $id = $this->rm_end_html($id);
        $this->loadModel('Hospital');
        $this->loadModel('Comment'); 
        $this->loadModel('User'); 
        $this->loadModel('Doctor'); 
        $this->loadModel('Picture'); 
        $picTopic=Configure::read('PIC_TOPIC'); 
        $info = $this->Hospital->find("first",array('conditions'=>array('status'=>1,'id'=>$id),'fields'=>array('*'))); 
        if(empty($info)){
            //处理错误跳转
            die("非法请求");
        }  
        $commentTopic=Configure::read('COMMENT_TOPIC');
        $comments = $this->Comment->find('all',array('conditions'=>array('topic_type'=>$commentTopic['医院'],'parent_id'=>0,'topic_id'=>$id),'order'=>array('is_top'=>'desc','sort'=>'desc','comment_time' => 'desc'),'limit' => 5));
        if(!empty($comments)){
            foreach ($comments as $key => $value) { 
                $userInfo = $this->User->findById($value['Comment']['uid']);   
                $comments[$key]['Comment']['username']=isset($userInfo['User']['nick_name'])?$userInfo['User']['nick_name']:"保密";
                $comments[$key]['Comment']['avatar']=$userInfo['User']['avatar'];
            }
        }
        
        $orderby['sort']="desc";
        $orderby['create_time']="desc";
        $conditions['status'] = 1;
        $conditions['hospital_id'] = $info['Hospital']['id'];
        $docList = array(); 
        $docList = $this->Doctor->find('all',array(
            'conditions' => $conditions,
//            'limit' => 3,
            'order' => $orderby,
            )
        ); 
        $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['医院'],'obj_id'=>intval($id)),'fields'=>array('thumb','image'))); 
        $info['Hospital']['images']=$picData; 
        $keysInfo=[];
        $keysInfo['Keyword']['meta_title']=$info['Hospital']['cn_name'].' - 试管无忧'; 
        $keysInfo['Keyword']['meta_keywords']=$info['Hospital']['cn_name']; 
        $keysInfo['Keyword']['meta_description']=Ivf::getSubstr(strip_tags($info['Hospital']['introduction']),0,200,''); 
        $this->set('keysInfo',$keysInfo);
        $this->set('comments',$comments); 
        $this->set('info',$info); 
        $this->set('docList',$docList); 
    }

}