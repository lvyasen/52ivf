<?php
// 音频
class AudioController extends AppController {

    public $layout = 'media';

    function beforeFilter() {
        parent::beforeFilter();
        $this->set('nav','audio');  
    }

    function index(){ 
        $this->loadModel('Album');   
        $albums = $this->Album->find('all',array('conditions' => array('status'=>1)),array('order'=>array('sort'=>'desc','id'=>'asc'))); 
        $dataList=[];
        foreach ($albums as $key => $val) {
            $dataList[$val['Album']['parent_id']][] = $val['Album'];
        } 
        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['胖妈妈之声']),'order'=>array('id'=>'desc'))); 
        $this->set('keysInfo',$keysInfo);
		$this->set('dataList',$dataList);
        $this->set('cateList',Configure::read('ALBUM_CATE'));
    }
    function info($id = 0){
        if(empty($id)){
            $this->redirect('/');
        }
        $id = $this->rm_end_html($id); 
        $this->loadModel('Album');  
        $this->loadModel('Audio');
        $info = $this->Album->find('first',array('conditions'=>array('status'=>1,'id'=>intval($id))));
        if(empty($info)){
            //处理错误跳转
            die("非法请求！");
        }  
        $audio = $this->Audio->find('all',array(
            'conditions'=>array('parent_id'=>$id),
            'order'=>array('sort'=>'desc','id'=>'desc')
            )
        );
        if(empty($audio)){
            die("该专辑无音频！");
        }  
        $titles = $audio[0]['Audio']['title'];  
        $keysInfo=[];
        $keysInfo['Keyword']['meta_title']=$titles.'_胖妈妈之声 - 试管无忧社区'; 
        $keysInfo['Keyword']['meta_keywords']=$titles.",胖妈妈之声"; 
        $keysInfo['Keyword']['meta_description']=Ivf::getSubstr(strip_tags($titles),0,200,''); 
        $this->set('keysInfo',$keysInfo); 
        $this->set('album',$info);
        $this->set('audio',$audio);

    }

}