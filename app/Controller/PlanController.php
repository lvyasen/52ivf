<?php
// 社区攻略
class PlanController extends AppController {

    public $layout = 'comm';

    function beforeFilter() {
        parent::beforeFilter();
        if($this->checkLogin()){ 
            $this->user = $this->getUser();  
            $this->set('userInfo',$this->user); 
        } 
        $this->set('nav','plan');  
    }

    function index($cid = null){  
        //分类ID
        $cid = $this->rm_end_html($cid);
        $params = $this->get_params($cid); 
        $cid = $params[0]; 
        $this->loadModel('ArticleCate');
        $this->loadModel('Article');
        $this->loadModel('User');
        $this->loadModel('Picture'); 
        $this->loadModel('Hospital');   
        $this->loadModel('Agree'); 
        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $conditions =$coments = array();
        $orderby=[];
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['攻略列表']),'order'=>array('id'=>'desc'))); 
        if(!empty($cid) && $cid>0){
            $conditions['category_id'] = intval($cid);
        	$cates = $this->ArticleCate->find('first',array('conditions'=>array('status'=>1,'id'=>$cid,'type'=>0)));
        	if(empty($cates)){
            	//处理错误跳转 
                die("非法请求");
        	}  
            if(!empty($keysInfo) && !empty($cates['ArticleCate']['name']))
                $keysInfo['Keyword']['meta_title']=$cates['ArticleCate']['name']."_".$keysInfo['Keyword']['meta_title'];
        
        }
        $seletCss="";
        if(isset($params[1]) && $params[1]=='elite'){
            $conditions['is_elite'] = '1';
            $seletCss="elite";
            $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['攻略精华']),'order'=>array('id'=>'desc'))); 
        }
        else{ 
	        if(isset($params[1]) && $params[1]=='hot'){
	            //处理热门 
	            $orderby['comment_num']="desc";
                $seletCss="hot";
                $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['攻略热门']),'order'=>array('id'=>'desc'))); 
	        } 
        }
        $seachTitle="";
        if(isset($params[2]) && !empty($params[2])){ 
            $conditions["OR"]['title like'] = '%'.$params[2].'%';
            $conditions["OR"]['content like'] = '%'.$params[2].'%'; 
            $seachTitle=$params[2];
        }
        $orderby['is_top']="desc"; 
        $orderby['sort']="desc";
        $orderby['update_time']="desc"; 
        $conditions['status'] = 1;
        $topList = array(); 
        $topList = $this->Article->find('all',array(
            'conditions' => $conditions, 
            'limit' => 7,
            'order' => $orderby,
            )
        ); 

        $conditions['uid >'] = 0;
        $dataList = array(); 
        $dataList = $this->Article->find('all',array(
            'conditions' => $conditions, 
            'limit' => 10,
            'order' => $orderby,
            )
        );
        $picTopic=Configure::read('PIC_TOPIC');
        $agreeTopic=Configure::read('AGREE_TOPIC');
        $userId=intval($this->user['id']);
        if(!empty($dataList)){
            foreach ($dataList as $key => $value) { 
                $dataList[$key]['Article']['hospital']="无";
                if(intval($value['Article']['hospital_id'])>0){ 
                    $info = $this->Hospital->findById(intval($value['Article']['hospital_id']));
                    if(isset($info['Hospital']['id']))
                        $dataList[$key]['Article']['hospital']=$info['Hospital']['cn_name'];
                } 
                $agCount = $this->Agree->find('count',array('conditions'=>array('uid'=>$userId,'obj_id'=>$value['Article']['id'],'obj_type'=>$agreeTopic['攻略'])));
                 $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['攻略'],'obj_id'=>intval($value['Article']['id'])),'fields'=>array('thumb')));   
                 $userInfo=$this->User->findById(intval($value['Article']['uid']));
                 $dataList[$key]['Article']['images']=$picData;
                 $dataList[$key]['Article']['username']=isset($userInfo['User']['nick_name'])?$userInfo['User']['nick_name']:"保密";
                 $dataList[$key]['Article']['avatar']=$userInfo['User']['avatar'];
                 $dataList[$key]['Article']['myagree']=$agCount; 
                 $dataList[$key]['Article']['description']= Ivf::getLittleText($value['Article']['description'],Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT']))); 
            }
            foreach ($dataList as $key => $value) {              //从内容中取三张图片方法
                $html = $value['Article']['content'];
                preg_match_all('/<img\s+src=".*?"/', $html, $matches);
                $dataList[$key]['Article']['images'] = $matches;
            }
        }   
        if(!empty($topList)){
            foreach ($topList as $key => $value) {  
                $agCount = $this->Agree->find('count',array('conditions'=>array('uid'=>$userId,'obj_id'=>$value['Article']['id'],'obj_type'=>$agreeTopic['攻略'])));
                $topList[$key]['Article']['myagree']=$agCount;

            }
        }  
        $cateList = $this->ArticleCate->find('all',array('conditions'=>array('status'=>1,'parent_id'=>0,'type'=>0),'order'=>array('sort'=>'desc'))); 
        $countNum = $this->Article->find('count',array('conditions' => array('status' => 1))); 
        $newNum = $this->Article->find('count',array('conditions' => array('status' => 1,'create_time >=' => date('Y-m-d 00:00:00')))); 
        $randNum=Ivf::planCount();
        $this->set('keysInfo',$keysInfo);
        $this->set('dataList',$dataList);
        $this->set('cateList',$cateList);
        $this->set('topList',$topList);
        $this->set('cid',intval($cid)); 
        $this->set('seletCss',$seletCss); 
        $this->set('countNum',$countNum); 
        $this->set('newNum',$newNum+$randNum); 
        $this->set('seachTitle',$seachTitle); 
    }

    function info($id = 0){
        if(empty($id)){
            $this->redirect('/');
        }
        $id = $this->rm_end_html($id);
        $this->loadModel('Article');
        $this->loadModel('Comment'); 
        $this->loadModel('ArticleCate'); 
        $this->loadModel('User'); 
        $this->loadModel('Picture'); 
        $this->loadModel('District'); 
        $this->loadModel('Agree'); 
        $this->loadModel('Hospital'); 
        $picTopic=Configure::read('PIC_TOPIC');
        $commentTopic=Configure::read('COMMENT_TOPIC');
        $userId=intval($this->user['id']); 
        $agreeTopic=Configure::read('AGREE_TOPIC');
        $info = $this->Article->find('first',array('conditions'=>array('status'=>1,'id'=>$id)));
        if(empty($info)){
            //处理错误跳转
            die("非法请求");
        } 
        $cateInfo = $this->ArticleCate->findById($info['Article']['category_id']); 
        $cate_name='';
        if(!empty($cateInfo))
            $cate_name=$cateInfo['ArticleCate']['name'];
        $info['Article']['username']="试管无忧";
        $info['Article']['avatar']="/images/temp/head.jpg";
        $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['攻略'],'obj_id'=>intval($id)),'fields'=>array('thumb')));  
        $info['Article']['images']=$picData;
        if($info['Article']['hospital_id'] > 0){
            $hiInfo=$this->Hospital->findById($info['Article']['hospital_id']);
            $cyInfo=$this->District->findById($hiInfo['Hospital']['city_id']);
            $info['Article']['city']=isset($cyInfo['District']['country'])?$cyInfo['District']['country']:"保密";
        }
        if($info['Article']['uid']>0){
            $userInfo=$this->User->findById(intval($info['Article']['uid']));
            $info['Article']['username']=isset($userInfo['User']['nick_name'])?$userInfo['User']['nick_name']:"保密";
            $info['Article']['avatar']=$userInfo['User']['avatar'];
        }    
        $comments = $this->Comment->find('all',array('conditions'=>array('topic_type'=>$commentTopic['攻略'],'parent_id'=>0,'topic_id'=>$id),'order'=>array('is_top'=>'desc','sort'=>'desc','comment_time' => 'desc'),'limit' => 5));
        if(!empty($comments)){
            foreach ($comments as $key => $value) { 
                $userInfo = $this->User->findById($value['Comment']['uid']);      
                $childCmt = $this->Comment->find('all',array('conditions'=>array('topic_type'=>$commentTopic['攻略'],'parent_id'=>$value['Comment']['id']),'order'=>array('is_top'=>'desc','sort'=>'desc','comment_time' => 'asc'),'fields'=>array('id','comment_time','uid','reply_id','content')));
                $childAttr=[];
                if(!empty($childCmt)){
                    foreach ($childCmt as $kk => $vv) {
                        $uInfo = $this->User->findById($vv['Comment']['uid']); 
                        if(!empty($uInfo)){ 
                            $vv['Comment']['replayName']=isset($uInfo['User']['nick_name'])?$uInfo['User']['nick_name']:$uInfo['User']['mobile'];
                            if($vv['Comment']['reply_id']>0 && $vv['Comment']['reply_id']!=$value['Comment']['id']){
                                $cInfo = $this->Comment->findById($vv['Comment']['reply_id']); 
                                if(empty($cInfo)) continue; 
                                $uInfo = $this->User->findById($cInfo['Comment']['uid']); 
                                $vv['Comment']['replayName']=$vv['Comment']['replayName']." <em>回复</em> ".(isset($uInfo['User']['nick_name'])?$uInfo['User']['nick_name']:$uInfo['User']['mobile']);

                            }
                            $childAttr[]=$vv['Comment'];
                        } 
                    }
                }
                $comments[$key]['Comment']['username']=isset($userInfo['User']['nick_name'])?$userInfo['User']['nick_name']:"保密";
                $comments[$key]['Comment']['avatar']=isset($userInfo['User']['avatar'])?$userInfo['User']['avatar']:"";
                $comments[$key]['Comment']['childCmt']=$childAttr;
            }
        } 

        $conditions=$orderby=[];
        //总排行
        $orderby['browse_num']="desc";  
        $conditions['status'] = 1;
        $topList = array(); 
        $topList = $this->Article->find('all',array(
            'conditions' => $conditions, 
            'limit' => 5,
            'order' => $orderby,
            )
        );
        if(!empty($topList)){
            foreach ($topList as $key => $value) { 
                $topList[$key]['Article']['avatar']="/images/temp/head.jpg";
                if($value['Article']['uid'] > 0){
                    $userInfo = $this->User->findById($value['Article']['uid']);    
                    $topList[$key]['Article']['avatar']=isset($userInfo['User']['avatar'])?$userInfo['User']['avatar']:"";
                }
            }
        } 

        //周排行
        $startTime=date("Y-m-d",time());
        $conditions['create_time >= '] =date('Y-m-d',strtotime("$startTime - 7 days"))." 00:00:00";    
        $weekList = array();  
        $weekList = $this->Article->find('all',array(
            'conditions' => $conditions, 
            'limit' => 5,
            'order' => $orderby,
            )
        );

        if(!empty($weekList)){
            foreach ($weekList as $key => $value) { 
                $weekList[$key]['Article']['avatar']='/images/mobile/touxiang.png';
                if($value['Article']['uid'] > 0){
                    $userList = $this->User->findById($value['Article']['uid']);    
                    $weekList[$key]['Article']['avatar']=isset($userList['User']['avatar'])?$userList['User']['avatar']:'';
                }
            }
        }  
        $agCount = $this->Agree->find('count',array('conditions'=>array('uid'=>$userId,'obj_id'=>$info['Article']['id'],'obj_type'=>$agreeTopic['攻略'])));
        $info['Article']['myagree']=$agCount;
        //浏览数+1  
        $this->Article->updateAll(array('Article.browse_num'=>'`Article`.`browse_num`+1'), array('Article.id'=>$id)); 
        $keysInfo=[];
        $keysInfo['Keyword']['meta_title']=$info['Article']['title'].'_泰国试管婴儿攻略 - 试管邦社区'; 
        $keysInfo['Keyword']['meta_keywords']=$info['Article']['title'].",泰国试管婴儿攻略"; 
        $keysInfo['Keyword']['meta_description']=Ivf::getSubstr(strip_tags($info['Article']['description']),0,200,''); 
        $this->set('keysInfo',$keysInfo); 
        $this->set('info',$info); 
        $this->set('cate_name',$cate_name); 
        $this->set('comments',$comments); 
        $this->set('topList',$topList); 
        $this->set('weekList',$weekList); 
    }

    function replay(){ 
        // 发表评论   
        $this->checkLogin(1);  
        $params = $this->request->data;
        $commentTopic=Configure::read('COMMENT_TOPIC');
        $this->loadModel('Article');   
        $this->loadModel('Comment');  
        if($this->request->is('post')){ 
            if($params['cid'] <=0){
                $this->ajaxReturn(4008, '非法请求!');  
            }
            if(empty($params['content'])){
                $this->ajaxReturn(4007, '内容不能为空!');  
            }
            if(isset($params['dosubmit']) && $params['dosubmit']==1){
                if(empty($params['u_code']))
                    $this->ajaxReturn(4003, '验证码不能为空!');   
                App::uses('ImageCaptcha','Lib');
                $ImageCaptcha = new ImageCaptcha();
                $captcha=$ImageCaptcha->check_captcha_code($params['u_code']);
                if($captcha['status']=="fail") $this->ajaxReturn(4005, $captcha['msg']);   
            }
            
            $info = $this->Article->find('first',array('conditions'=>array('status'=>1,'id'=>intval($params['cid']))));
            if(empty($info))
                $this->ajaxReturn(4006, '非法提交!');   

            $cinfo = $this->Comment->find('first',array('conditions'=>array('topic_type'=>$commentTopic['攻略'],'topic_id'=>$info['Article']['id'],'reply_id'=>0,'uid'=>$this->user['id'])));
            if(!empty($cinfo))
                $this->ajaxReturn(4009, '不能重复评论!');  
            //添加评论
            $data=[];
            $data['topic_type']=$commentTopic['攻略'];
            $data['topic_id']=$info['Article']['id'];
            $data['uid'] = $this->user['id']; 
            $data['sort'] = 1100; 
            $data['content'] = $params['content'];   
            $data['comment_time']=date("Y-m-d H:i:s",time());
            $data['create_time']=date("Y-m-d H:i:s",time());
            $this->Comment->save($data); 
            $data=[];
            $data['id']=$info['Article']['id']; 
            $data['comment_num']=$info['Article']['comment_num']+1;
            $this->Article->save($data);
            $this->ajaxReturn(200, '发表成功！');   
        }
        $this->ajaxReturn(4007, '异常提交！');    
    } 

    function replay_answer(){ 
        // 发表评论   
        $this->checkLogin(1);  
        $params = $this->request->data;
        $commentTopic=Configure::read('COMMENT_TOPIC');
        $this->loadModel('Article');   
        $this->loadModel('Comment');  
        if($this->request->is('post') && $params['dosubmit']==1){ 
            if($params['cid'] <=0 || $params['reply_id'] <=0){
                $this->ajaxReturn(4008, '请选择回复评论!');  
            }
            if(empty($params['content'])){
                $this->ajaxReturn(4007, '内容不能为空!');  
            }

            $info = $this->Article->find('first',array('conditions'=>array('status'=>1,'id'=>intval($params['cid']))));
            if(empty($info))
                $this->ajaxReturn(4006, '非法提交!');  

            $rpinfo = $this->Comment->findById(intval($params['reply_id']));

            if(empty($rpinfo))
                $this->ajaxReturn(4007, '非法提交!');   

            if($rpinfo['Comment']['uid']==$this->user['id']){
                $this->ajaxReturn(4008, '不能回复自己!');  
            }  
            //添加评论
            $data=[];
            if($rpinfo['Comment']['parent_id']==0)
                $data['parent_id']=$rpinfo['Comment']['id'];
            else
                $data['parent_id']=$rpinfo['Comment']['parent_id']; 
            $data['topic_type']=$commentTopic['攻略'];
            $data['topic_id']=$info['Article']['id'];
            $data['reply_id']=intval($params['reply_id']);
            $data['uid'] = $this->user['id']; 
            $data['sort'] = 1100; 
            $data['content'] = $params['content'];   
            $data['comment_time']=date("Y-m-d H:i:s",time());
            $data['create_time']=date("Y-m-d H:i:s",time());
            $this->Comment->save($data);
            $data=[];
            $data['id']=$info['Article']['id']; 
            $data['comment_num']=$info['Article']['comment_num']+1;
            $this->Article->save($data);
            $this->ajaxReturn(200, '发表成功！');   
        }
        $this->ajaxReturn(4007, '异常提交！');    
    }  
}