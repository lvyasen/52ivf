<?php
App::uses('AdminbaseController', 'Controller'); 
class AdminajaxController extends AdminbaseController {  

    function beforeFilter() { 
        parent::beforeFilter();  
    }
  
    //切换状态
    function toggle()
    { 
        $params = $this->request->query;
        $act = $params['ajaxdata']; 
        $this->checkLogin(1);  
        $c_id = $params['id']?intval($params['id']):0; 
        $c_val = $params['val']?$params['val']:0;  
        if($c_id<=0)
        {
            $this->ajaxReturn(4001, '参数异常！'); 
        }
        $data=[];
        $data['id']=$c_id;  
        $data['status']=$c_val;  
        switch ($act) { 
            case 'account':   
                $this->admin_power('users_account_edit',1);
                $this->loadModel('User');  
                $dataList = $this->User->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '用户不存在！'); 
                if(!$this->User->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "用户状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break;
            case 'appoint':   
                $this->admin_power('users_appoint_list',1);
                $this->loadModel('UsersAppoint'); 
                $dataList = $this->UsersAppoint->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '预约不存在！'); 
                if(!$this->UsersAppoint->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "预约状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break;    
            case 'manages':  
                $this->admin_power('adminmanage_edit',1); 
                $this->loadModel('AdminUser');  
                $dataList = $this->AdminUser->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '管理员不存在！'); 
                if(!$this->AdminUser->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "管理员状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break;
            case 'mapcate':  
                $this->admin_power('map_cates_edit',1); 
                $this->loadModel('MapCate');  
                $dataList = $this->MapCate->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '分类不存在！'); 
                if(!$this->MapCate->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "地图分类状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break; 
            case 'map':  
                $this->admin_power('map_cates_edit',1); 
                $this->loadModel('Map');  
                $dataList = $this->Map->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '地图不存在！'); 
                if(!$this->Map->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "地图状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break; 
            case 'link':   
                $this->admin_power('link_edit',1);
                $this->loadModel('Link');  
                $dataList = $this->Link->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '友情链接不存在！'); 
                if(!$this->Link->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "友情链接状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break;
            case 'blackIp': 
                $this->admin_power('blackIp_edit',1); 
                $this->loadModel('BlackIp'); 
                $dataList = $this->BlackIp->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '黑名单不存在！'); 
                if(!$this->BlackIp->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "黑名单状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break;
            case 'cate':   
                $this->admin_power('article_cate_edit',1);
                $this->loadModel('ArticleCate');  
                $dataList = $this->ArticleCate->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '分类不存在！'); 
                if(!$this->ArticleCate->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "攻略分类状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break; 
            case 'news':   
                $this->admin_power('news_edit',1);
                $this->loadModel('New');
                $dataList = $this->New->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '新闻不存在！'); 
                if(!$this->New->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "新闻状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break; 
            case 'news_home':   
                $this->admin_power('news_edit',1);
                $this->loadModel('New');
                $dataList = $this->New->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '新闻不存在！'); 
                unset($data['status']);
                $data['is_home']=$c_val;  
                if(!$this->New->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "新闻状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break; 
            case 'article':   
                $this->admin_power('article_edit',1);
                $this->loadModel('Article');  
                $dataList = $this->Article->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '攻略不存在！'); 
                if(!$this->Article->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "攻略状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break; 
            case 'channel':   
                $this->admin_power('channel_edit',1);
                $this->loadModel('Yonghuqudao');  
                $dataList = $this->Yonghuqudao->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '渠道不存在！'); 
                if(!$this->Yonghuqudao->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "渠道状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break; 
            case 'article_top':   
                $this->admin_power('article_edit',1);
                $this->loadModel('Article');  
                $dataList = $this->Article->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '攻略不存在！');  
                unset($data['status']);
                $data['is_top']=$c_val;  
                $data['update_time']=date('Y-m-d H:i:s');
                if(!$this->Article->save($data)){
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "攻略推荐状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break;  
            case 'ask':   
                $this->admin_power('users_ask_edit',1);
                $this->loadModel('Share');    
                $dataList = $this->Share->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '提问不存在！'); 
                if(!$this->Share->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "提问状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break;
            case 'ask_top':   
                $this->admin_power('users_ask_edit',1);
                $this->loadModel('Share');    
                $dataList = $this->Share->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '提问不存在！'); 
                unset($data['status']);
                $data['is_top']=$c_val;  
                if(!$this->Share->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "提问置顶状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break;
            case 'share':   
                $this->admin_power('share_edit',1);
                $this->loadModel('Share');    
                $dataList = $this->Share->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '分享不存在！'); 
                if(!$this->Share->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "分享状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break; 
            case 'share_top':   
                $this->admin_power('share_edit',1);
                $this->loadModel('Share');  
                $dataList = $this->Share->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '分享不存在！');  
                unset($data['status']);
                $data['is_top']=$c_val;  
                if(!$this->Share->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "分享置顶状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break; 
            case 'article_elite':   
                $this->admin_power('article_edit',1);
                $this->loadModel('Article');  
                $dataList = $this->Article->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '攻略不存在！'); 
                unset($data['status']);
                $data['is_elite']=$c_val;
                $data['update_time']=date('Y-m-d H:i:s');
                if(!$this->Article->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "攻略精华状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break; 
            case 'video_cate':   
                $this->admin_power('video_cate_edit',1);
                $this->loadModel('VideoCate');  
                $dataList = $this->VideoCate->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '分类不存在！'); 
                if(!$this->VideoCate->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "视频分类状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break;
            case 'video_cate_top':   
                $this->admin_power('video_cate_edit',1);
                $this->loadModel('VideoCate');  
                $dataList = $this->VideoCate->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '分类不存在！'); 
                unset($data['status']);
                $data['is_top']=$c_val;  
                if(!$this->VideoCate->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "视频分类置顶状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break;
            case 'comment':   
                $this->admin_power('users_comment_edit',1);
                $this->loadModel('Comment');
                $dataList = $this->Comment->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '评论不存在！'); 
                unset($data['status']);
                $data['is_top']=$c_val;  
                if(!$this->Comment->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "评论置顶状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break;
            case 'video':   
                $this->admin_power('video_edit',1);
                $this->loadModel('Video');  
                $dataList = $this->Video->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '视频不存在！'); 
                if(!$this->Video->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "视频状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break; 
            case 'video_rec':   
                $this->admin_power('video_edit',1);
                $this->loadModel('Video');  
                $dataList = $this->Video->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '视频不存在！'); 
                unset($data['status']);
                $data['rec']=$c_val;  
                if(!$this->Video->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "视频推荐状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break; 
            case 'music':   
                $this->admin_power('music_edit',1);
                $this->loadModel('Audio');  
                $dataList = $this->Audio->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '音频不存在！'); 
                if(!$this->Audio->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "音频状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break; 
            case 'albums':   
                $this->admin_power('music_albums_edit',1);
                $this->loadModel('Album');  
                $dataList = $this->Album->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '专辑不存在！'); 
                if(!$this->Album->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "专辑状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break; 
            case 'hospital':   
                $this->admin_power('hospital_edit',1);
                $this->loadModel('Hospital');  
                $dataList = $this->Hospital->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '医院不存在！'); 
                if(!$this->Hospital->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "医院状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break;
            case 'doctor':   
                $this->admin_power('doctor_edit',1);
                $this->loadModel('Doctor');  
                $dataList = $this->Doctor->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '医生不存在！'); 
                if(!$this->Doctor->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "医生状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break;
            case 'doctor_rec':   
                $this->admin_power('doctor_edit',1);
                $this->loadModel('Doctor');  
                $dataList = $this->Doctor->findById($c_id);  
                if(empty($dataList))
                    $this->ajaxReturn(4002, '医生不存在！'); 
                unset($data['status']);
                $data['rec']=$c_val;  
                if(!$this->Doctor->save($data)){ 
                    $this->ajaxReturn(4003, '变更失败！'); 
                }
                $this->adminLog($c_id, 'edit', "医生推荐状态",$data);
                $this->ajaxReturn(200, '变更成功！',array('res' => $c_val)); 
                break;  
            default:
                break;
        }
        die();
    }  

    public function del(){ 
        $params = $this->request->query;
        $act = $params['ajaxdata']; 
        $rid = $params['rid']?intval($params['rid']):0; 
        $deltype = $params['type']?$params['type']:'single';  
        $this->checkLogin(1);  
        if(empty($act) || $rid<=0)
        {
            $this->ajaxReturn(4001, '参数异常！'); 
        }   
        switch ($act) {  
            case 'roles': 
                $this->admin_power('adminmanage_roles_edit',1);
                $this->loadModel('AdminGroup');  
                if($deltype=='single'){ 
                    //处理角色删除
                    $this->data_delete('AdminGroup',array('id' => $rid)); 
                    //角色资源删除    
                    $this->loadModel('AdminPermission');  
                    $this->AdminPermission->deleteAll(array('gid'=>$rid));  
                    $this->adminLog($rid, 'del', "角色");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break;
            case 'manages':  
                $this->admin_power('adminmanage_edit_boss',1); 
                $this->loadModel('AdminUser');  
                if($deltype=='single'){ 
                    //处理角色删除
                    $this->data_delete('AdminUser',array('id' => $rid));  
                    $this->adminLog($rid, 'del', "管理员");
                } 
                $this->ajaxReturn(200, '已删除！');  
                break;
            case 'keywords': 
                $this->admin_power('keywords_edit',1);
                $this->loadModel('Keyword');  
                if($deltype=='single'){ 
                    //处理角色删除
                    $this->data_delete('Keyword',array('id' => $rid));  
                    $this->adminLog($rid, 'del', "keywords");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break;
            case 'specials': 
                $this->admin_power('specials_edit',1);
                $this->loadModel('Special');  
                if($deltype=='single'){ 
                    //处理角色删除
                    $this->data_delete('Special',array('id' => $rid));  
                    $this->adminLog($rid, 'del', "自定义专题");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break;
            case 'account': 
                $this->admin_power('users_account_list',1);
                $this->loadModel('User');  
                $this->loadModel('Share');  
                $this->loadModel('Article');  
                $this->loadModel('Comment');  
                if($deltype=='single'){ 
                    $this->data_delete('User',array('id' => $rid));   
                    //删除对应攻略，分享，问答，评论
                    $this->Share->deleteAll(['uid =' => $rid]);
                    $this->Article->deleteAll(['uid =' => $rid]);
                    $this->Comment->deleteAll(['uid =' => $rid]); 
                    $this->adminLog($rid, 'del', "会员");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break;
            case 'site': 
                $this->admin_power('site_comments_edit',1);
                $this->loadModel('SiteComment');  
                if($deltype=='single'){ 
                    //处理角色删除
                    $this->data_delete('SiteComment',array('id' => $rid));  
                    $this->adminLog($rid, 'del', "赴泰点评");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break;  
            case 'joins': 
                $this->admin_power('join_edit',1);
                $this->loadModel('Join');  
                if($deltype=='single'){ 
                    //处理角色删除
                    $this->data_delete('Join',array('id' => $rid));   
                    $this->adminLog($rid, 'del', "招聘");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break; 
            case 'map': 
                $this->admin_power('map_edit',1); 
                $this->loadModel('Map');  
                if($deltype=='single'){  
                    $this->data_delete('Map',array('id' => $rid));  
                    $this->adminLog($rid, 'del', "地图");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break; 
            case 'channel':   
                $this->admin_power('channel_edit',1);
                $this->loadModel('Yonghuqudao');  
                if($deltype=='single'){  
                    $this->data_delete('Yonghuqudao',array('id' => $rid));  
                    $this->adminLog($rid, 'del', "渠道");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break; 
            case 'link': 
                $this->admin_power('link_edit',1); 
                $this->loadModel('Link');  
                if($deltype=='single'){  
                    $this->data_delete('Link',array('id' => $rid));  
                    $this->adminLog($rid, 'del', "友情链接");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break; 
            case 'blackIp': 
                $this->admin_power('blackIp_edit',1); 
                $this->loadModel('BlackIp');  
                if($deltype=='single'){  
                    $this->data_delete('BlackIp',array('id' => $rid));  
                    $this->adminLog($rid, 'del', "黑名单");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break; 
            case 'video': 
                $this->admin_power('video_edit',1); 
                $this->loadModel('Video');  
                if($deltype=='single'){  
                    $this->data_delete('Video',array('id' => $rid));  
                    $this->adminLog($rid, 'del', "视频");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break; 
            case 'news': 
                $this->admin_power('news_edit',1); 
                $this->loadModel('New');
                if($deltype=='single'){  
                    $this->data_delete('New',array('id' => $rid));  
                    $this->adminLog($rid, 'del', "新闻");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break;
            case 'article': 
                $this->admin_power('article_edit',1); 
                $this->loadModel('Article');
                if($deltype=='single'){  
                    $this->data_delete('Article',array('id' => $rid));  
                    $this->adminLog($rid, 'del', "攻略");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break; 
            case 'music': 
                $this->admin_power('music_edit',1); 
                $this->loadModel('Audio');    
                if($deltype=='single'){  
                    $this->data_delete('Audio',array('id' => $rid));  
                    $this->adminLog($rid, 'del', "音频");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break; 
            case 'comment': 
                $this->admin_power('users_comment_edit',1); 
                $this->loadModel('Comment');    
                if($deltype=='single'){  
                    $this->data_delete('Comment',array('id' => $rid));  
                    $this->adminLog($rid, 'del', "评论");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break;  
            case 'album': 
                $this->admin_power('music_albums_edit',1); 
                $this->loadModel('Album');    
                if($deltype=='single'){  
                    $this->data_delete('Album',array('id' => $rid));  
                    $this->adminLog($rid, 'del', "音频专辑");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break;
            case 'hospital': 
                $this->admin_power('hospital_del',1); 
                $this->loadModel('Hospital');    
                if($deltype=='single'){  
                    $this->data_delete('Hospital',array('id' => $rid));  
                    $this->adminLog($rid, 'del', "医院");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break;
            case 'doctor': 
                $this->admin_power('doctor_del',1); 
                $this->loadModel('Doctor');    
                if($deltype=='single'){  
                    $this->data_delete('Doctor',array('id' => $rid));  
                    $this->adminLog($rid, 'del', "医生");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break; 
            case 'share': 
                $this->admin_power('share_del',1); 
                $this->loadModel('Share');    
                if($deltype=='single'){  
                    $this->data_delete('Share',array('id' => $rid));  
                    $this->adminLog($rid, 'del', "分享");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break; 
            case 'answer': 
                $this->admin_power('answer_del',1); 
                $this->loadModel('Share');    
                if($deltype=='single'){  
                    $this->data_delete('Share',array('id' => $rid));  
                    $this->adminLog($rid, 'del', "问答");
                } 
                $this->ajaxReturn(200, '已删除！'); 
                break; 
            default:
                break;
        }
        die();
    }


    public function other(){  
        $params = $this->request->data;
        $act = $params['ajaxdata'];  
        $this->checkLogin(1);  
        if(empty($act))
        {
            $this->ajaxReturn(4001, '参数异常！'); 
        }   
        switch ($act) {  
            case 'cate':   
                $list = isset($params['val'])?json_decode($params['val']):[];  
                if(empty($list))
                {
                    $this->ajaxReturn(4001, '参数异常！'); 
                }  
                //检查权限
                $this->admin_power('article_cate_edit',1); 
                $this->loadModel('ArticleCate');  
                //改变结构及排序
                $sort=100; 
                foreach ($list as $key => $value) {
                    $data=array(); 
                    $data=['sort'=>$sort,'id'=>$value->id,'parent_id'=>0];    
                    $this->ArticleCate->save($data);
                    $sort++;
                    if(isset($value->children)){
                        foreach ($value->children as $kk => $vv) {
                            $data=['sort'=>$sort,'id'=>$vv->id,'parent_id'=>$value->id];  
                            $this->ArticleCate->save($data);
                            $sort++;
                        }
                    } 
                }
                $this->ajaxReturn(200, '变更成功！');  
                break;  
            case 'video_cate':   
                $list = isset($params['val'])?json_decode($params['val']):[];  
                if(empty($list))
                {
                    $this->ajaxReturn(4001, '参数异常！'); 
                }  
                //检查权限
                $this->admin_power('video_cate_edit',1); 
                $this->loadModel('VideoCate');  
                //改变结构及排序
                $sort=100; 
                foreach ($list as $key => $value) {
                    $data=array(); 
                    $data=['sort'=>$sort,'id'=>$value->id,'parent_id'=>0];    
                    $this->VideoCate->save($data);
                    $sort++;
                    if(isset($value->children)){
                        foreach ($value->children as $kk => $vv) {
                            $data=['sort'=>$sort,'id'=>$vv->id,'parent_id'=>$value->id];  
                            $this->VideoCate->save($data);
                            $sort++;
                        }
                    } 
                }
                $this->ajaxReturn(200, '变更成功！');  
                break;   
            default:
                break;
        }
        die();
    } 

    public function other_get(){  
        $params = $this->request->query;
        $act = $params['ajaxdata'];  
        $this->checkLogin(1);  
        if(empty($act))
        {
            $this->ajaxReturn(4001, '参数异常！'); 
        }   
        switch ($act) {   
            case 'num_data':   
                $startTime = isset($params['stime']) && !empty($params['stime'])?date('Y-m-d',strtotime($params['stime'])):''; 
                $endTime  = isset($params['etime']) && !empty($params['etime'])?date('Y-m-d',strtotime($params['etime'])):'';  
                  
                $this->loadModel('Rizhi');   
                $this->loadModel('User');  
                $this->loadModel('UsersAppoint');   
                $conditions=[];
                if(!empty($startTime)){
                    $startTime=date("Y-m-d 00:00:00",strtotime($startTime));
                    $conditions['godate >= '] = $startTime;   
                }
                if(!empty($endTime)){
                    $endTime=date("Y-m-d 23:59:59",strtotime($endTime)); 
                    $conditions['godate <= '] = $endTime;   
                }    
                $uvCount = $this->Rizhi->find('count', array('conditions' => $conditions)); 

                $conditions=[];
                if(!empty($startTime)){
                    $startTime=date("Y-m-d 00:00:00",strtotime($startTime));
                    $conditions['create_time >= '] = $startTime;   
                }
                if(!empty($endTime)){
                    $endTime=date("Y-m-d 23:59:59",strtotime($endTime)); 
                    $conditions['create_time <= '] = $endTime;   
                }   

                $uinfo = $this->User->find('count', array('conditions' => $conditions)); 

                $conditions=[];
                if(!empty($startTime)){
                    $startTime=date("Y-m-d 00:00:00",strtotime($startTime));
                    $conditions['create_time >= '] = $startTime;   
                }
                if(!empty($endTime)){
                    $endTime=date("Y-m-d 23:59:59",strtotime($endTime)); 
                    $conditions['create_time <= '] = $endTime;   
                }   
                $txCount = $this->UsersAppoint->find('count', array('conditions' => $conditions)); 
  
  
                $this->ajaxReturn(200, '',array('totaluv' => $uvCount,'regUser' => $uinfo,'txUser' => $txCount));  
                break; 
            case 'qudaoregtop10':   
                $startTime = isset($params['stime']) && !empty($params['stime'])?date('Y-m-d',strtotime($params['stime'])):''; 
                $endTime  = isset($params['etime']) && !empty($params['etime'])?date('Y-m-d',strtotime($params['etime'])):''; 
                $qudao  = isset($params['qudao']) && !empty($params['qudao'])?$params['qudao']:'';    

                $this->loadModel('User');  
                $where="1=1";      
                if(!empty($startTime)){
                    $startTime=date("Y-m-d 00:00:00",strtotime($startTime));
                    $where.=" AND create_time >= '{$startTime}'";  
                }
                if(!empty($endTime)){
                    $endTime=date("Y-m-d 23:59:59",strtotime($endTime)); 
                    $where.=" AND create_time <= '{$endTime}'";  
                }  
                if(!empty($qudao)) 
                    $where.=" AND source = {$qudao}";   
                else
                    $where.=" AND source >0 ";   

                $qudaoPvTop=$this->User->query("SELECT d.title,c.nums FROM (SELECT COUNT(id) as nums,source FROM ivf_users WHERE {$where}  GROUP BY source  order by nums DESC LIMIT 0,10) c INNER JOIN  ivf_yonghuqudaos d ON d.yonghuqudaohao=c.source");  
                $attrName=[];
                $attrData=[]; 
                if(!empty($qudaoPvTop)){
                    foreach ($qudaoPvTop as $key => $value) {
                        $attrName[]=$value['d']['title'];
                        $attrData[$key]['value']=$value['c']['nums'];
                        $attrData[$key]['name']=$value['d']['title'];
                    }
                }    
                $this->ajaxReturn(200, '',array('clickname' => json_encode($attrName),'clickData' => json_encode($attrData)));  
                break; 
            case 'qudaouvtop10':   
                $startTime = isset($params['stime']) && !empty($params['stime'])?date('Y-m-d',strtotime($params['stime'])):''; 
                $endTime  = isset($params['etime']) && !empty($params['etime'])?date('Y-m-d',strtotime($params['etime'])):''; 
                $qudao  = isset($params['qudao']) && !empty($params['qudao'])?$params['qudao']:'';        
                //top10  
                $this->loadModel('Rizhi');   
                $where=" `system` <> '' ";      
                if(!empty($startTime)){
                    $startTime=date("Y-m-d 00:00:00",strtotime($startTime));
                    $where.=" AND godate >= '{$startTime}'";  
                }
                if(!empty($endTime)){
                    $endTime=date("Y-m-d 23:59:59",strtotime($endTime)); 
                    $where.=" AND godate <= '{$endTime}'";  
                } 
                if(!empty($qudao)) 
                    $where.=" AND source = {$qudao}";    

                $qudaoUvTop=$this->Rizhi->query("SELECT A.nums,B.title FROM (SELECT COUNT(source) as nums,source FROM ivf_rizhis where {$where} GROUP BY source) A INNER JOIN ivf_yonghuqudaos B ON A.source=B.yonghuqudaohao ORDER BY A.nums desc LIMIT 0,10");  
                $attrName=[];
                $attrData=[];
                if(!empty($qudaoUvTop)){
                    foreach ($qudaoUvTop as $key => $value) {
                        $attrName[]=$value['B']['title'];
                        $attrData[$key]['value']=$value['A']['nums'];
                        $attrData[$key]['name']=$value['B']['title'];
                    }
                }   
                $this->ajaxReturn(200, '',array('clickname' => json_encode($attrName),'clickData' => json_encode($attrData)));  
                break; 
            case 'home':   
                $time = isset($params['time']) && !empty($params['time'])?$params['time']:''; 
                $qudao  = isset($params['qudao']) && !empty($params['qudao'])?$params['qudao']:'';   
                $startTime=$endTime="";
                $this->loadModel('Rizhi');   
                $this->loadModel('User');  
                $this->loadModel('UsersAppoint');  
                if(!empty($time)){
                    $startTime = date('Y-m-d', strtotime($time));//指定月份月初时间戳  
                    $endTime = date('Y-m-d',mktime(23, 59, 59, date('m', strtotime($time))+1, 00));//指定月份月末时间戳  
                }
                else{ 
                    $startTime=date('Y-m-d', mktime(0, 0, 0, date('m'), 1, date('Y')));
                    $endTime=date('Y-m-d', mktime(0, 0, 0, date('m')+1, 0, date('Y')));   
                }  
                //top10  
                $where=" `system` <> '' ";        
                if(!empty($startTime)){
                    $startTime=date("Y-m-d 00:00:00",strtotime($startTime));
                    $where.=" AND godate >= '{$startTime}'";  
                }
                if(!empty($endTime)){
                    $endTime=date("Y-m-d 23:59:59",strtotime($endTime)); 
                    $where.=" AND godate <= '{$endTime}'";  
                } 
                if(!empty($qudao)) 
                    $where.=" AND source = {$qudao}";    

                //平台数据趋势  
                $rizhiList=$this->Rizhi->query("SELECT COUNT(id) as count,dateTime FROM (SELECT id,substr(godate,1,10) as dateTime FROM ivf_rizhis where {$where} group by model) a  group by dateTime  LIMIT 0,31");  
                $reportUV=[];  
                if(!empty($rizhiList)){
                    foreach ($rizhiList as $key => $value) {
                        $reportUV[$key]['count']=$value[0]['count'];
                        $reportUV[$key]['dateTime']=$value['a']['dateTime'];
                    }
                }
                $where="1=1";   
                if(!empty($qudao)) 
                    $where.=" AND source = {$qudao}";   
                
                if(!empty($startTime)){
                    $startTime=date("Y-m-d 00:00:00",strtotime($startTime));
                    $where.=" AND create_time >= '{$startTime}'";  
                }
                if(!empty($endTime)){
                    $endTime=date("Y-m-d 23:59:59",strtotime($endTime)); 
                    $where.=" AND create_time <= '{$endTime}'";  
                }  
                $uList=$this->User->query("SELECT count(*) as count,substr(create_time,1,10) as dateTime FROM ivf_users where {$where} group by dateTime ASC LIMIT 0,31;");  
                $reportUserReg=[];  
                if(!empty($uList)){
                    foreach ($uList as $key => $value) {
                        $reportUserReg[$key]['count']=$value[0]['count'];
                        $reportUserReg[$key]['dateTime']=$value[0]['dateTime'];
                    }
                }
                $where="1=1";   
                if(!empty($qudao)) 
                    $where.=" AND  uid in (SELECT id FROM ivf_users WHERE source={$qudao})";   
                
                if(!empty($startTime)){
                    $startTime=date("Y-m-d 00:00:00",strtotime($startTime));
                    $where.=" AND create_time >= '{$startTime}'";  
                }
                if(!empty($endTime)){
                    $endTime=date("Y-m-d 23:59:59",strtotime($endTime)); 
                    $where.=" AND create_time <= '{$endTime}'";  
                }  
                $auList=$this->User->query("SELECT count(*) as count,substr(create_time,1,10) as dateTime FROM ivf_users_appoints where {$where} group by dateTime ASC LIMIT 0,31;");  
                $reportApply=[];  
                if(!empty($auList)){
                    foreach ($auList as $key => $value) {
                        $reportApply[$key]['count']=$value[0]['count'];
                        $reportApply[$key]['dateTime']=$value[0]['dateTime'];
                    }
                }     
                $btime = strtotime($startTime);
                $etime = strtotime($endTime);
                for ($start = $btime; $start <= $etime; $start += 24 * 3600) {
                    if(!Ivf::deep_in_array(date("Y-m-d", $start),$reportUV)){
                        $reportUV[]=  array('count' =>0 ,'dateTime'=>date("Y-m-d", $start));
                    } 
                    if(!Ivf::deep_in_array(date("Y-m-d", $start),$reportUserReg)){
                        $reportUserReg[]=  array('count' =>0 ,'dateTime'=>date("Y-m-d", $start));
                    } 
                    if(!Ivf::deep_in_array(date("Y-m-d", $start),$reportApply)){
                        $reportApply[]=  array('count' =>0 ,'dateTime'=>date("Y-m-d", $start));
                    }  
                } 
                $reportUV = Ivf::my_sort($reportUV,'dateTime');   
                $reportUserReg = Ivf::my_sort($reportUserReg,'dateTime');   
                $reportApply = Ivf::my_sort($reportApply,'dateTime');     
                
                $attrData=[];
                foreach ($reportUV as $key => $value) { 
                  $attrData['uv'][]=$value['count']; 
                  $attrData['time'][]=$value['dateTime']; 
                }
                foreach ($reportUserReg as $key => $value) { 
                  $attrData['reg'][]=$value['count']; 
                }
                foreach ($reportApply as $key => $value) { 
                  $attrData['applyProds'][]=$value['count']; 
                }  
                $this->ajaxReturn(200, '',array('qddata' => json_encode($attrData['uv']),'regData' => json_encode($attrData['reg']),'timeData'=>json_encode($attrData['time']),'prodData' => json_encode($attrData['applyProds']) ));  
                break;  
            case 'setUser':
                $type = isset($params['val'])?intval($params['val']):0; 
                if($type<=0)
                    $this->ajaxReturn(4002, '参数异常！'); 
                $this->loadModel('Share');    
                $this->loadModel('User'); 
                $this->loadModel('Article');
                $this->loadModel('Comment');
                $commentTopic=Configure::read('COMMENT_TOPIC');
                $userId=[];   
                if($type==1)
                {
                    //分享   
                    $shareUsers = $this->Share->find('all',array('conditions'=>array('type'=>0),'group' => array('uid'),'fields' => array('uid')));
                    if(!empty($shareUsers)){
                        foreach ($shareUsers as $key => $value) {
                            $userId[]=$value['Share']['uid'];
                        }
                    }  
                }else if($type==2)
                {
                    //问答   
                    $shareUsers = $this->Share->find('all',array('conditions'=>array('type'=>1),'group' => array('uid'),'fields' => array('uid')));
                    if(!empty($shareUsers)){
                        foreach ($shareUsers as $key => $value) {
                            $userId[]=$value['Share']['uid'];
                        }
                    }  
                }else if($type==3)
                {
                    //攻略   
                    $shareUsers = $this->Article->find('all',array('group' => array('uid'),'fields' => array('uid')));
                    if(!empty($shareUsers)){
                        foreach ($shareUsers as $key => $value) {
                            $userId[]=$value['Article']['uid'];
                        }
                    }  
                }else if($type==50)
                {
                    //分享评论   
                    $shareUsers = $this->Comment->find('all',array('conditions'=>array('topic_type'=>$commentTopic['分享']),'group' => array('uid'),'fields' => array('uid')));
                    if(!empty($shareUsers)){
                        foreach ($shareUsers as $key => $value) {
                            $userId[]=$value['Comment']['uid'];
                        }
                    }  
                }else if($type==60)
                {
                    //问答评论 
                    $shareUsers = $this->Comment->find('all',array('conditions'=>array('topic_type'=>$commentTopic['问答']),'group' => array('uid'),'fields' => array('uid')));
                    if(!empty($shareUsers)){
                        foreach ($shareUsers as $key => $value) {
                            $userId[]=$value['Comment']['uid'];
                        }
                    }  
                }else if($type==10)
                {
                    //攻略评论   
                    $shareUsers = $this->Comment->find('all',array('conditions'=>array('topic_type'=>$commentTopic['攻略']),'group' => array('uid'),'fields' => array('uid')));
                    if(!empty($shareUsers)){
                        foreach ($shareUsers as $key => $value) {
                            $userId[]=$value['Comment']['uid'];
                        }
                    }  
                }else if($type==20)
                {
                    //医院   
                    $this->ajaxReturn(4003, '没有可以推荐的会员！');  
                } else if($type==30)
                {
                    //医生   
                    $this->ajaxReturn(4003, '没有可以推荐的会员！');   
                } else if($type==40)
                {
                    $this->ajaxReturn(4003, '没有可以推荐的会员！');  
                }  
                if(!empty($userId)) 
                    $uid = $this->User->find('first',array('conditions'=>array('status'=>1,'test'=>1,'id !='=>$userId),'order'=>array('rand()'),'fields' => array('id')));
                else
                    $uid = $this->User->find('first',array('conditions'=>array('status'=>1,'test'=>1),'order'=>array('rand()'),'fields' => array('id')));
                if(!isset($uid['User']['id']))
                    $this->ajaxReturn(4003, '没有可以推荐的会员！');  
                $this->ajaxReturn(200, $uid['User']['id']);  

            default:
                break;
        }
        die();
    } 

    public function file() { 
        $params = $this->request->query;
        $act = $params['ajaxdata'];  
        $this->checkLogin(1);  
        $this->loadModel('Picture');  
        if(empty($act)) 
            $this->ajaxReturn(4001, '参数异常！');   
        switch ($act) {    
            case 'article':     
                $pictype=Configure::read('PIC_TOPIC');
                if (!empty($_FILES['file']['name'])) {   
                    // 上传文件 
                    $fName=explode(".", $_FILES['file']['name']);
                    $_FILES['imgFile']=$_FILES['file'];
                    $attrRes=Ivf::uploadImg('/uploads/article/');
                    if($attrRes['status']!=200)
                        $this->ajaxReturn(4002, '图片上传失败！原因:'.$attrRes['message']);   
                    //生成缩略图 
                    $php_path = dirname(dirname(dirname(__FILE__))).'/webroot';
                    //文件保存目录路径    
                    $thumb_path=$attrRes['save_url']."thumb/"; 
                    if(!file_exists($php_path.$thumb_path)){
                        mkdir($php_path.$thumb_path);
                    }
                    $new_image_path=$thumb_path.$attrRes['save_name'];  
                    Ivf::thumb_pic($php_path.$attrRes['url'],$php_path.$new_image_path);

                    $data=[];
                    $data['image'] =$attrRes['url'];   
                    $data['obj_type'] = $pictype['攻略'];  
                    $data['thumb'] =$new_image_path;  
                    $data['obj_id']=0;
                    $data['mimetype']=$fName[1];  
                    $this->Picture->save($data);
                     
                    $this->ajaxReturn(200, '上传成功！',array('id'=>$this->Picture->id,'url'=>$data['thumb']));  
                }  
                $this->ajaxReturn(4002, '上传失败！'); 
                break; 
            case 'hospit':     
                $pictype=Configure::read('PIC_TOPIC');
                if (!empty($_FILES['file']['name'])) {   
                    // 上传文件 
                    $fName=explode(".", $_FILES['file']['name']);
                    $_FILES['imgFile']=$_FILES['file'];
                    $attrRes=Ivf::uploadImg('/uploads/hospital/');
                    if($attrRes['status']!=200)
                        $this->ajaxReturn(4002, '图片上传失败！原因:'.$attrRes['message']);   
                    //生成缩略图 
                    $php_path = dirname(dirname(dirname(__FILE__))).'/webroot';
                    //文件保存目录路径    
                    $thumb_path=$attrRes['save_url']."thumb/"; 
                    if(!file_exists($php_path.$thumb_path)){
                        mkdir($php_path.$thumb_path);
                    }
                    $new_image_path=$thumb_path.$attrRes['save_name'];  
                    Ivf::thumb_pic($php_path.$attrRes['url'],$php_path.$new_image_path);

                    $data=[];
                    $data['image'] =$attrRes['url'];   
                    $data['obj_type'] = $pictype['医院'];  
                    $data['thumb'] =$new_image_path;  
                    $data['obj_id']=0;
                    $data['mimetype']=$fName[1];  
                    $this->Picture->save($data);
                     
                    $this->ajaxReturn(200, '上传成功！',array('id'=>$this->Picture->id,'url'=>$data['thumb']));  
                }  
                $this->ajaxReturn(4002, '上传失败！'); 
                break;
            case 'share':     
                $pictype=Configure::read('PIC_TOPIC');
                if (!empty($_FILES['file']['name'])) {   
                    // 上传文件 
                    $fName=explode(".", $_FILES['file']['name']);
                    $_FILES['imgFile']=$_FILES['file'];
                    $attrRes=Ivf::uploadImg('/uploads/share/');
                    if($attrRes['status']!=200)
                        $this->ajaxReturn(4002, '图片上传失败！原因:'.$attrRes['message']);   
                    //生成缩略图 
                    $php_path = dirname(dirname(dirname(__FILE__))).'/webroot';
                    //文件保存目录路径    
                    $thumb_path=$attrRes['save_url']."thumb/"; 
                    if(!file_exists($php_path.$thumb_path)){
                        mkdir($php_path.$thumb_path);
                    }
                    $new_image_path=$thumb_path.$attrRes['save_name'];  
                    Ivf::thumb_pic($php_path.$attrRes['url'],$php_path.$new_image_path);

                    $data=[];
                    $data['image'] =$attrRes['url'];   
                    $data['obj_type'] = $pictype['分享'];  
                    $data['thumb'] =$new_image_path;  
                    $data['obj_id']=0;
                    $data['mimetype']=$fName[1];  
                    $this->Picture->save($data);
                     
                    $this->ajaxReturn(200, '上传成功！',array('id'=>$this->Picture->id,'url'=>$data['thumb']));  
                }  
                $this->ajaxReturn(4002, '上传失败！'); 
                break;
            case 'answer':     
                $pictype=Configure::read('PIC_TOPIC');
                if (!empty($_FILES['file']['name'])) {   
                    // 上传文件 
                    $fName=explode(".", $_FILES['file']['name']);
                    $_FILES['imgFile']=$_FILES['file'];
                    $attrRes=Ivf::uploadImg('/uploads/answer/');
                    if($attrRes['status']!=200)
                        $this->ajaxReturn(4002, '图片上传失败！原因:'.$attrRes['message']);   
                    //生成缩略图 
                    $php_path = dirname(dirname(dirname(__FILE__))).'/webroot';
                    //文件保存目录路径    
                    $thumb_path=$attrRes['save_url']."thumb/"; 
                    if(!file_exists($php_path.$thumb_path)){
                        mkdir($php_path.$thumb_path);
                    }
                    $new_image_path=$thumb_path.$attrRes['save_name'];  
                    Ivf::thumb_pic($php_path.$attrRes['url'],$php_path.$new_image_path);

                    $data=[];
                    $data['image'] =$attrRes['url'];   
                    $data['obj_type'] = $pictype['问答'];  
                    $data['thumb'] =$new_image_path;  
                    $data['obj_id']=0;
                    $data['mimetype']=$fName[1];  
                    $this->Picture->save($data);
                     
                    $this->ajaxReturn(200, '上传成功！',array('id'=>$this->Picture->id,'url'=>$data['thumb']));  
                }  
                $this->ajaxReturn(4002, '上传失败！'); 
                break; 
            case 'del_pic': 
                $rid = $params['val']?intval($params['val']):0;  
                if(intval($rid) <= 0) 
                    $this->ajaxReturn(4002, '参数异常！');   
 
                $this->data_delete('Picture',array('id' => $rid));   
                $this->ajaxReturn(200, '已删除！');  
                break; 
            case 'add_users': 
                $this->loadModel('User');
                if(@is_uploaded_file($_FILES['file']['tmp_name'])){
                    //批量上传
                    $upfile=$_FILES["file"];  
                    $name=$upfile["name"]; 
                    $tmp_name=$upfile["tmp_name"]; 

                    /*判别是不是.xls文件，判别是不是excel文件*/ 
                    $file_types = explode ( ".", $name);
                    $file_type = $file_types[count($file_types)-1];
                    if(strtolower($file_type) != "xlsx" && strtolower($file_type) != "xls")     
                        $this->ajaxReturn(4003, '不是Excel文件，重新上传!');   
 
                    $php_path = dirname(dirname(dirname(__FILE__))).'/webroot';
                    //文件保存目录路径  
                    $save_path = $php_path . '/uploads/excel/'; 

                    if (!file_exists($save_path)) { 
                        mkdir($save_path);
                    }  
                      

                    $file_name=$save_path.'users'.date('_YmdHis').'.'.strtolower($file_type);  
                    move_uploaded_file($tmp_name,$file_name);
                    if($upfile["error"]==0)
                    { 
                        ignore_user_abort();
                        set_time_limit(0);
                        App::import('Vendor', 'Excel/PHPExcel');  
                        App::import('Vendor', 'Excel/PHPExcel/IOFactory');  
                        $objExcel = new PHPExcel(); 
   
                        if (strtolower($file_type)=='xlsx') 
                            $objReader = \PHPExcel_IOFactory::createReader('Excel2007');  
                        else 
                            $objReader = \PHPExcel_IOFactory::createReader('Excel5');//创建读取实例 
                        
                        $objPHPExcel = $objReader->load($file_name,$encode='utf-8');//加载文件 
                        $sheet = $objPHPExcel->getSheet(0);//取得sheet(0)表 
                        $highestRow = $sheet->getHighestRow(); // 取得总行数
                        $highestColumn = $sheet->getHighestColumn(); // 取得总列数   
                        $totalRow=$highestRow-1;
                        if($highestRow>1){   
                            //导入产品 
                            $attrdata=[];   
                            for($i=2;$i<=$highestRow;$i++)
                            {  
                                $data=[];   
                                $data['nick_name']=(string)$objPHPExcel->getActiveSheet()->getCell("A".$i)->getValue(); 
                                $data['mobile']=$objPHPExcel->getActiveSheet()->getCell("B".$i)->getValue(); 
                                $data['avatar']=$objPHPExcel->getActiveSheet()->getCell("C".$i)->getValue();  

                                if(!empty($data['nick_name']) && !empty($data['mobile']) && !empty($data['avatar'])){ 
                                    $dataList = $this->User->findByMobile($data['mobile']);  
                                    if(empty($dataList)){
                                        //添加
                                        $data['user_name']=(string)$data['nick_name'];
                                        $data['auth']=Ivf::random(8);;
                                        $data['password']=md5("12345678".$data['auth']);
                                        $data['status']='1';
                                        $data['test']='1';
                                        $data['create_time']=date("Y-m-d H:i:s",time()); 
                                        $data['avatar']="/uploads/avatar/image/".date("Ymd")."/".$data['avatar'];
                                        $attrdata[]=$data;
                                    } 
                                }
                            } 
                            //$entities = $this->User->newEntities($attrdata);
                            if(!empty($attrdata)){
                                $rows = $this->User->saveMany($attrdata); //返回批量保存的条数 
                                $this->ajaxReturn(200, '上传成功!');  
                                $this->adminLog('', 'add', "导入用户",$attrdata); 
                            }
                            else 
                                $this->ajaxReturn(200, '没有要上传数据!');  
                        }
                        else
                            $this->ajaxReturn(4003, '不能上传空数据!');  

                    }
                    else   
                        $this->ajaxReturn(4001, '上传失败，重新上传!');    
                }  
                $this->ajaxReturn(4005, '非法提交!');    
                break; 
            default:
                break;
        }
        die();
    }

}