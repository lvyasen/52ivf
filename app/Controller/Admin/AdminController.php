<?php
App::uses('AdminbaseController', 'Controller'); 
class AdminController extends AdminbaseController {

    public $layout = 'admin_content';  
    public $user=array();   

    function beforeFilter() { 
        parent::beforeFilter(); 
        $this->checkLogin(2); 
        $this->user = $this->getUser();  
        $this->set('userRoles',$this->user['roles']);
    }
  
    function index(){   
        $this->layout = 'admin_main';
        $this->set('userName',$this->user['user_name']);
    }
    function home(){
        $this->layout = 'admin_write';   
        $this->loadModel('Rizhi');   
        $this->loadModel('User');  
        $this->loadModel('UsersAppoint');   
        $uvCount = $this->Rizhi->find('count'); 
        $uinfo = $this->User->find('count'); 
        $txCount = $this->UsersAppoint->find('count'); 
        $endTime=date("Y-m-d",time());
        $startTime=date("Y-m-d",strtotime("$endTime -1 month"));   
        //平台数据趋势  
        $rizhiList=$this->Rizhi->query("SELECT COUNT(id) as count,dateTime FROM (SELECT id,substr(godate,1,10) as dateTime FROM ivf_rizhis where  `system` <> '' and godate >= '".date("Y-m-d 00:00:00",strtotime($startTime))."'  AND godate <= '".date("Y-m-d 23:59:59",strtotime($endTime))."' group by model) a  group by dateTime  LIMIT 0,31");  
        $reportUV=[];  
        if(!empty($rizhiList)){
            foreach ($rizhiList as $key => $value) {
                $reportUV[$key]['count']=$value[0]['count'];
                $reportUV[$key]['dateTime']=$value['a']['dateTime'];
            }
        }
        $uList=$this->User->query("SELECT count(*) as count,substr(create_time,1,10) as dateTime FROM ivf_users where create_time >= '".date("Y-m-d 00:00:00",strtotime($startTime))."'  AND create_time <= '".date("Y-m-d 23:59:59",strtotime($endTime))."' group by dateTime ASC LIMIT 0,31;");  
        $reportUserReg=[];  
        if(!empty($uList)){
            foreach ($uList as $key => $value) {
                $reportUserReg[$key]['count']=$value[0]['count'];
                $reportUserReg[$key]['dateTime']=$value[0]['dateTime'];
            }
        }
        $auList=$this->User->query("SELECT count(*) as count,substr(create_time,1,10) as dateTime FROM ivf_users_appoints where create_time >= '".date("Y-m-d 00:00:00",strtotime($startTime))."'  AND create_time <= '".date("Y-m-d 23:59:59",strtotime($endTime))."' group by dateTime ASC LIMIT 0,31;");  
        $reportApply=[];  
        if(!empty($auList)){
            foreach ($auList as $key => $value) {
                $reportApply[$key]['count']=$value[0]['count'];
                $reportApply[$key]['dateTime']=$value[0]['dateTime'];
            }
        }     
        $btime = strtotime($startTime);
        $etime = time();
        for ($start = $btime; $start <= $etime; $start += 24 * 3600) {
            if(!Ivf::deep_in_array(date("Y-m-d", $start),$reportUV)){
                $reportUV[]=  array('count' =>0 ,'dateTime'=>date("Y-m-d", $start));
            } 
            if(!Ivf::deep_in_array(date("Y-m-d", $start),$reportUserReg)){
                $reportUserReg[]=  array('count' =>0 ,'dateTime'=>date("Y-m-d", $start));
            } 
            if(!Ivf::deep_in_array(date("Y-m-d", $start),$reportApply)){
                $reportApply[]=  array('count' =>0 ,'dateTime'=>date("Y-m-d", $start));
            }  
        } 
        $reportUV = Ivf::my_sort($reportUV,'dateTime');   
        $reportUserReg = Ivf::my_sort($reportUserReg,'dateTime');   
        $reportApply = Ivf::my_sort($reportApply,'dateTime');   
  
        //top10  
        $rizhiList=$this->Rizhi->query("SELECT A.nums,B.title FROM (SELECT COUNT(source) as nums,source FROM ivf_rizhis where `system` <> '' and  godate >= '".date("Y-m-d 00:00:00",strtotime($startTime))."'  AND godate <= '".date("Y-m-d 23:59:59",strtotime($endTime))."' GROUP BY source) A INNER JOIN ivf_yonghuqudaos B ON A.source=B.yonghuqudaohao ORDER BY A.nums desc LIMIT 0,10");  
        $qudaoUvTop=[];  
        if(!empty($rizhiList)){
            foreach ($rizhiList as $key => $value) {
                $qudaoUvTop[$key]['nums']=$value['A']['nums'];
                $qudaoUvTop[$key]['title']=$value['B']['title'];
            }
        }   

        $rizhiList=$this->User->query("SELECT d.title,c.nums FROM (SELECT COUNT(id) as nums,source FROM ivf_users WHERE   create_time >= '".date("Y-m-d 00:00:00",strtotime($startTime))."'  AND create_time <= '".date("Y-m-d 23:59:59",strtotime($endTime))."'  GROUP BY source  order by nums DESC LIMIT 0,10) c INNER JOIN  ivf_yonghuqudaos d ON d.yonghuqudaohao=c.source");  
        $qudaoPvTop=[];  
        if(!empty($rizhiList)){
            foreach ($rizhiList as $key => $value) {
                $qudaoPvTop[$key]['nums']=$value['c']['nums'];
                $qudaoPvTop[$key]['title']=$value['d']['title'];
            }
        }   
        $dataList=[];
        $dataList['totaluv']=$uvCount; 
        $dataList['regUser']=$uinfo;  
        $dataList['txUser']=$txCount;    
 
        //有效渠道商 
        $this->loadModel('Yonghuqudao');   
        $qudaoList = $this->Yonghuqudao->find('all',array('conditions'=>array('status'=>1),'fields'=>array('title','yonghuqudaohao')));  
        $this->set('qudaoList',$qudaoList);
        $this->set('dataList',$dataList);
        $this->set('reportUV',$reportUV);
        $this->set('reportUserReg',$reportUserReg);
        $this->set('reportApply',$reportApply);
        $this->set('qudaoUvTop',$qudaoUvTop);
        $this->set('qudaoPvTop',$qudaoPvTop);
    }

    function logout(){
        if($this->delUser()){
            $this->redirect('/adminlogin/');
        }
    }
    function hospital()
    { 
        $this->admin_power('hospital_list',2);
        $this->loadModel('Hospital');
        $params = $this->request->query;
        $conditions = array();
        if(!empty($params['keywords'])){

            $conditions["OR"]['en_name like'] = '%'.$params['keywords'].'%';
            $conditions["OR"]['cn_name like'] = '%'.$params['keywords'].'%'; 
        } 
        if(isset($params['city_id']) && $params['city_id']>0) 
            $conditions['city_id'] = $params['city_id'];  
        
        if(!empty($params['startTime'])){
            $startTime=date("Y-m-d 00:00:00",strtotime($params['startTime']));
            $conditions['create_time >= '] = $startTime;   
        }
        if(!empty($params['endTime'])){
            $endTime=date("Y-m-d 23:59:59",strtotime($params['endTime']));
            $conditions['create_time <= '] = $endTime;   
        }  
        $this->paginate = array('conditions' => $conditions,'order'=>array('id' => 'desc'),'limit' => 10); 
        try {
            $users = $this->paginate('Hospital'); 
        } catch (NotFoundException $e) {
            $users = array();
        }   
        $this->loadModel('District');  
        $districtList = $this->District->find('all');   
        $this->set('districtList',$districtList);
        $this->set('dataList',$users);
    }
 
    function hospital_edit(){
        $this->admin_power('hospital_edit',2);  
        $params = $this->request->data;
        $uid = $this->request->query('id'); 
        $this->loadModel('Hospital');
        $this->loadModel('Picture');
        $picTopic=Configure::read('PIC_TOPIC');
        $dataList=$picData=[];
        if(intval($uid)>0){  
            $dataList = $this->Hospital->find("first",array('conditions'=>array('id'=>$uid),'fields'=>array('*')));  
            if(empty($dataList))
                $this->admin_redirect('/admin/hospital',2,'医院不存在！');
            $dataList=$dataList['Hospital'];  
            $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['医院'],'obj_id'=>intval($uid)),'fields'=>array('id','thumb')));  
        }

        if($this->request->is('post') && $params['dosubmit']==1){ 
            $params['status'] = isset($params['status'])?$params['status']:0; 
            $params['auth'] = isset($params['auth'])?$params['auth']:1; 
            $params['rule'] = !empty($params['rule'])?addslashes($params['rule']):'';   
            if(intval($uid)>0)  
                $params['id'] = $uid;    
            else
            { 
                $params['auth'] = 1; 
                $params['create_time']=date("Y-m-d H:i:s",time());
            }
            if (isset($this->request['form']['imgFile']) && is_uploaded_file($this->request['form']['imgFile']['tmp_name'])) { 
                $attrRes=Ivf::uploadImg('/uploads/hospital/');
                if($attrRes['status']!=200)
                    $this->admin_redirect('/admin/hospital',2,'图片上传失败！原因:'.$attrRes['message']);
                $params['image'] =$attrRes['url'];  
            }

            if (isset($this->request['form']['imgFile2']) && is_uploaded_file($this->request['form']['imgFile2']['tmp_name'])) { 
                $attrRes=[];
                $_FILES['imgFile']=$_FILES['imgFile2'];
                $attrRes=Ivf::uploadImg('/uploads/hospital/');
                if($attrRes['status']!=200)
                    $this->admin_redirect('/admin/hospital',2,'图片上传失败！原因:'.$attrRes['message']);
                $params['mb_image'] =$attrRes['url'];  
            }
            if(!$this->Hospital->save($params))  
                $this->admin_redirect('/admin/hospital',2,'操作失败，请稍后再试');  
            if(!empty($params['UptPicture'])){ 
                foreach ($params['UptPicture'] as $val){  
                    $data=[]; 
                    $data['id'] = $val;  
                    $data['obj_type'] = $picTopic['医院'];  
                    $data['obj_id'] = $this->Hospital->id;  
                    $this->Picture->save($data);
                }  
            }
            if($uid>0){ 
                $this->adminLog($params['cn_name'], 'edit', "医院",$params);
            }   
            else
                $this->adminLog($params['cn_name'], 'add', "医院"); 
            $this->redirect("/admin/hospital");     
        }       

        $this->loadModel('District');  
        $districtList = $this->District->find('all');   
        $this->set('dataList',$dataList);
        $this->set('districtList',$districtList);
        $this->set('picData',$picData);
        $this->set('score',Configure::read('HOSPITAL_SCORE'));
    } 
    function hospital_project_log(){ 
        $this->admin_power('hospital_project_log_list',2);
        $this->loadModel('HospitalProjectLog');
        $params = $this->request->query;
        $conditions = array();
        if(!empty($params['keywords']))
            $conditions['pro_name like'] = '%'.$params['keywords'].'%';  
        
        if(!empty($params['startTime'])){
            $startTime=date("Y-m-d 00:00:00",strtotime($params['startTime']));
            $conditions['create_time >= '] = $startTime;   
        }
        if(!empty($params['endTime'])){
            $endTime=date("Y-m-d 23:59:59",strtotime($params['endTime']));
            $conditions['create_time <= '] = $endTime;   
        }  
        $this->paginate = array('conditions' => $conditions,'order'=>array('id' => 'desc'),'limit' => 10);
        try {
            $users = $this->paginate('HospitalProjectLog'); 
        } catch (NotFoundException $e) {
            $users = array();
        } 
        $this->set('dataList',$users);

    }

    function doctor()
    { 
        $this->admin_power('doctor_list',2);
        $this->loadModel('Doctor');
        $params = $this->request->query;
        $conditions = array();
        if(!empty($params['keywords'])){

            $conditions["OR"]['en_name like'] = '%'.$params['keywords'].'%';
            $conditions["OR"]['cn_name like'] = '%'.$params['keywords'].'%'; 
        } 
        if(isset($params['hospital_id']) && $params['hospital_id']>0) 
            $conditions['hospital_id'] = $params['hospital_id'];  
        
        if(!empty($params['startTime'])){
            $startTime=date("Y-m-d 00:00:00",strtotime($params['startTime']));
            $conditions['create_time >= '] = $startTime;   
        }
        if(!empty($params['endTime'])){
            $endTime=date("Y-m-d 23:59:59",strtotime($params['endTime']));
            $conditions['create_time <= '] = $endTime;   
        }  
        $this->paginate = array('conditions' => $conditions,'order'=>array('id' => 'desc'),'limit' => 10);
        try {
            $users = $this->paginate('Doctor'); 
        } catch (NotFoundException $e) {
            $users = array();
        }  
        $this->loadModel('Hospital');   
        $hospitals = $this->Hospital->find('all',array(
            'conditions'=>array('status'=>1),
            'fields'=>array('id','cn_name'),
            )
        );  
        $this->set('hospitalList',$hospitals);
        $this->set('education',Configure::read('EDUCATION'));
        $this->set('dataList',$users);
    }
 
    function doctor_edit(){
        $this->admin_power('hospital_edit',2);  
        $params = $this->request->data;
        $uid = $this->request->query('id'); 
        $this->loadModel('Doctor');
        $dataList=[];
        if(intval($uid)>0){ 
            $dataList = $this->Doctor->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/hospital',2,'医生不存在！');
            $dataList=$dataList['Doctor'];  
        }

        if($this->request->is('post') && $params['dosubmit']==1){ 
            $params['status'] = isset($params['status'])?$params['status']:0; 
            $params['rec'] = isset($params['rec'])?$params['rec']:0; 
            $params['auth'] = isset($params['auth'])?$params['auth']:1;   
            if(intval($uid)>0)  
                $params['id'] = $uid;    
            else
            { 
                $params['auth'] = 1; 
                $params['create_time']=date("Y-m-d H:i:s",time());
            }
            if (isset($this->request['form']['imgFile']) && is_uploaded_file($this->request['form']['imgFile']['tmp_name'])) { 
                $attrRes=Ivf::uploadImg('/uploads/doctor/');
                if($attrRes['status']!=200)
                    $this->admin_redirect('/admin/hospital',2,'图片上传失败！原因:'.$attrRes['message']);
                $params['avatar'] =$attrRes['url'];  
            }
            if (isset($this->request['form']['imgFile2']) && is_uploaded_file($this->request['form']['imgFile2']['tmp_name'])) { 
                $attrRes=[];
                $_FILES['imgFile']=$_FILES['imgFile2'];
                $attrRes=Ivf::uploadImg('/uploads/hospital/');
                if($attrRes['status']!=200)
                    $this->admin_redirect('/admin/hospital',2,'图片上传失败！原因:'.$attrRes['message']);
                $params['mb_image'] =$attrRes['url'];  
            }
 
            if(!$this->Doctor->save($params))  
                $this->admin_redirect('/admin/doctor',2,'操作失败，请稍后再试');  
            if($uid>0){ 
                $this->adminLog($params['cn_name'], 'edit', "医生",$params);
            }   
            else
                $this->adminLog($params['cn_name'], 'add', "医生"); 
            $this->redirect("/admin/doctor");     
        }       

        $this->loadModel('Hospital');   
        $hospitals = $this->Hospital->find('all',array(
            'conditions'=>array('status'=>1),
            'fields'=>array('id','cn_name'),
            )
        ); 
        $this->set('hospitalList',$hospitals);
        $this->set('dataList',$dataList);  
        $this->set('education',Configure::read('EDUCATION'));
    } 
    function account_list(){
        $this->admin_power('users_account_list',2);
        $this->loadModel('User');
        $params = $this->request->query;
        $conditions = array();
        $type=isset($params['type'])?$params['type']:'0';

        if(isset($params['import']) && $params['import']==1){
            //导出    
            $xlsCell  = array(
            array('nick_name','昵称'),
            array('mobile','手机号'),
            array('avatar','头像（如1.png）')
            );   
            $xlsName="会员导入";  
            $tmpData=array(array('nick_name' => "ada", 'mobile'=>"18222222222", 'avatar'=>"1.png"));
            Ivf::exportExcel($xlsName,$xlsCell,$tmpData);
        }

        if(!empty($params['username'])){

            if (is_numeric($params['username'])) 
                $conditions['mobile like'] = '%'.$params['username'].'%'; 
            else{ 
                $conditions["OR"]['nick_name like'] = '%'.$params['username'].'%';
                $conditions["OR"]['user_name like'] = '%'.$params['username'].'%'; 
            } 
        } 
        if($type!="all"){ 
            $conditions['test']=intval($type);
        } 
        $this->paginate = array('conditions' => $conditions,'order'=>array('id' => 'desc'),'limit' => 10);
        try {
            $users = $this->paginate('User'); 
        } catch (NotFoundException $e) {
            $users = array();
        }
        $this->set('dataList',$users);
        $this->set('type',$type);
        $this->set('sex',Configure::read('USER_SEX')); 
    } 

    function account_comment(){
        $this->admin_power('users_comment_list',2);
        $this->loadModel('Comment');
        $this->loadModel('User');
        $this->loadModel('Hospital');
        $this->loadModel('Doctor');
        $this->loadModel('Article');
        $this->loadModel('Share');
        $this->loadModel('VideoCate');
        $params = $this->request->query;
        $conditions = array();
        $conditions['parent_id'] = 0; 
        if(isset($params['ctype']) && $params['ctype']>0){
            $conditions['topic_type'] = $params['ctype']; 
        }
        if(isset($params['cid']) && $params['cid']>0){
            $conditions['topic_id'] = $params['cid']; 
        }
        if(!empty($params['username'])){
            $conditions['content like'] = '%'.$params['username'].'%'; 
        }
        if(!empty($params['startTime'])){
            $startTime=date("Y-m-d 00:00:00",strtotime($params['startTime']));
            $conditions['create_time >= '] = $startTime;   
        }
        if(!empty($params['endTime'])){
            $endTime=date("Y-m-d 23:59:59",strtotime($params['endTime']));
            $conditions['create_time <= '] = $endTime;   
        }  
        $this->paginate = array('conditions' => $conditions,'order'=>array('id' => 'desc'),'limit' => 10);
        try {
            $users = $this->paginate('Comment'); 
        } catch (NotFoundException $e) {
            $users = array();
        } 
        $commentTopic=Configure::read('COMMENT_TOPIC');
        if(!empty($users)){
            foreach ($users as $key => $value) { 
                $users[$key]['Comment']['topic_title']="无";
                if($value['Comment']['uid']>0){ 
                    $dataInfo = $this->User->findById($value['Comment']['uid']);  
                    $users[$key]['Comment']['uid']= isset($dataInfo['User']['nick_name'])?$dataInfo['User']['nick_name']:"无"; 
                } 
                if($value['Comment']['topic_type']==$commentTopic['攻略']){
                    $dataInfo = $this->Article->findById($value['Comment']['topic_id']);  
                    $users[$key]['Comment']['topic_title']= isset($dataInfo['Article']['title'])?$dataInfo['Article']['title']:"无"; 
                }
                else if($value['Comment']['topic_type']==$commentTopic['医院']){
                    $dataInfo = $this->Hospital->findById($value['Comment']['topic_id']);  
                    $users[$key]['Comment']['topic_title']= isset($dataInfo['Hospital']['cn_name'])?$dataInfo['Hospital']['cn_name']:"无"; 
                }
                else if($value['Comment']['topic_type']==$commentTopic['医生']){
                    $dataInfo = $this->Doctor->findById($value['Comment']['topic_id']);  
                    $users[$key]['Comment']['topic_title']= isset($dataInfo['Doctor']['cn_name'])?$dataInfo['Doctor']['cn_name']:"无"; 
                }
                else if($value['Comment']['topic_type']==$commentTopic['视频']){
                    $dataInfo = $this->VideoCate->findById($value['Comment']['topic_id']);  
                    $users[$key]['Comment']['topic_title']= isset($dataInfo['VideoCate']['name'])?$dataInfo['VideoCate']['name']:"无"; 
                }
                else if($value['Comment']['topic_type']==$commentTopic['分享']){
                    $dataInfo = $this->Share->findById($value['Comment']['topic_id']);  
                    $users[$key]['Comment']['topic_title']= isset($dataInfo['Share']['title'])?$dataInfo['Share']['title']:"无"; 
                }
                else if($value['Comment']['topic_type']==$commentTopic['问答']){
                    $dataInfo = $this->Share->findById($value['Comment']['topic_id']);  
                    $users[$key]['Comment']['topic_title']= isset($dataInfo['Share']['content'])?$dataInfo['Share']['content']:"无"; 
                }
            }
        } 
        $this->set('dataList',$users);
        $this->set('comment_type',$commentTopic); 
    } 


    function edit_comment(){
        $this->layout = 'admin_write';
        $this->admin_power('users_comment_edit',2); 
        $params = $this->request->data;
        $uid = $this->request->query('id');   
        $this->loadModel('Comment');
        $this->loadModel('User');
        $this->loadModel('Hospital'); 
        $this->loadModel('Doctor'); 
        $this->loadModel('Article'); 
        $this->loadModel('VideoCate'); 
        $this->loadModel('Share'); 
        $dataList=[];
        if(intval($uid)>0){ 
            $dataList = $this->Comment->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/account_comment',2,'评论不存在！');
            $dataList=$dataList['Comment'];  
        }
        $commentTopic=Configure::read('COMMENT_TOPIC');
        if($this->request->is('post') && $params['dosubmit']==1){   
            $params['parent_id'] = isset($params['parent_id'])?$params['parent_id']:0;  

            if($params['topic_type']==$commentTopic['攻略']){ 
                $params['topic_id']=$params['g_id']; 
                $info = $this->Article->findById($params['g_id']); 
                $data=[];
                $data['id']=$info['Article']['id']; 
                $data['comment_num']=intval($info['Article']['comment_num'])+1; 
                $data['update_time']=date("Y-m-d H:i:s",time());  
                $this->Article->save($data); 
            }
            else if($params['topic_type']==$commentTopic['医院']){
                $params['topic_id']=$params['h_id'];
                $info = $this->Hospital->findById($params['topic_id']); 
                $data=[];
                $data['id']=$info['Hospital']['id']; 
                $data['comment_num']=intval($info['Hospital']['comment_num'])+1; 
                $this->Hospital->save($data); 
            }
            else if($params['topic_type']==$commentTopic['医生']){
                $params['topic_id']=$params['d_id'];
                $info = $this->Doctor->findById($params['topic_id']); 
                $data=[];
                $data['id']=$info['Doctor']['id']; 
                $data['comment_num']=intval($info['Doctor']['comment_num'])+1; 
                $this->Doctor->save($data); 
            }
            else if($params['topic_type']==$commentTopic['视频']){
                $params['topic_id']=$params['v_id'];
                /**
                    $info = $this->Video->findById($params['topic_id']); 
                    $data=[];
                    $data['id']=$info['Video']['id']; 
                    $data['comment_num']=intval($info['Video']['comment_num'])+1; 
                    $this->Video->save($data); 
                **/
            }
            else if($params['topic_type']==$commentTopic['分享']){
                $params['topic_id']=$params['s_id'];
                $info = $this->Share->findById($params['topic_id']); 
                $data=[];
                $data['id']=$info['Share']['id']; 
                if($info['Share']['is_replay']==0){
                    $data['is_replay']=1;
                }
                $data['comment_num']=intval($info['Share']['comment_num'])+1; 
                $data['update_time']=date("Y-m-d H:i:s",time());  
                $this->Share->save($data); 
            }
            else if($params['topic_type']==$commentTopic['问答']){
                $params['topic_id']=$params['a_id'];
                $info = $this->Share->findById($params['topic_id']); 
                $data=[];
                $data['id']=$info['Share']['id']; 
                if($info['Share']['is_replay']==0){
                    $data['is_replay']=1;
                }
                $data['comment_num']=intval($info['Share']['comment_num'])+1; 
                $data['update_time']=date("Y-m-d H:i:s",time());  
                $this->Share->save($data); 
            }
            if(empty($params['comment_time']))
                $params['comment_time']=date("Y-m-d H:i:s",time());
            else
                $params['comment_time']=date("Y-m-d H:i:s",strtotime($params['comment_time'])); 
            
            if(intval($uid)>0)  
                $params['id'] = $uid;     
            else
                $params['create_time']=date("Y-m-d H:i:s",time());
            if(!$this->Comment->save($params))  
                $this->admin_redirect('/admin/account_comment',2,'操作失败，请稍后再试');  
            if($uid>0){ 
                $this->adminLog($params['id'], 'edit', "评论",$params);
            }   
            else
                $this->adminLog($params['content'], 'add', "评论"); 
            $this->redirect("/admin/account_comment");     
        }       
        $usersDate = $this->User->find('all',array('conditions'=>array('status'=>1),'fields'=>array('id','nick_name'),'order'=>array('id' => 'desc')));
        //获取医院
        $hospitals = $this->Hospital->find('all',array('conditions'=>array('status'=>1),'fields'=>array('id','cn_name'),'order'=>array('id' => 'desc')));   
        //获取医生
        $doctors = $this->Doctor->find('all',array('conditions'=>array('status'=>1),'fields'=>array('id','cn_name'),'order'=>array('id' => 'desc')));   
        //获取攻略
        $guide = $this->Article->find('all',array('conditions'=>array('status'=>1),'fields'=>array('id','title'),'order'=>array('id' => 'desc')));   
        //获取视频
        $videos = $this->VideoCate->find('all',array('conditions' => array('status'=>1),'fields'=>array('id','name'),'order'=>array('id' => 'desc'))); 
        
        //获取分享
        $share = $this->Share->find('all',array('conditions' => array('status'=>1,'type'=>0),'fields'=>array('id','title'),'order'=>array('id' => 'desc'))); 
        //获取问答   
        $answer = $this->Share->find('all',array('conditions' => array('status'=>1,'type'=>1),'fields'=>array('id','content'),'order'=>array('id' => 'desc'))); 

        $this->set('hospitalList',$hospitals);
        $this->set('doctorList',$doctors);
        $this->set('guideList',$guide);
        $this->set('videoList',$videos);
        $this->set('shareList',$share);
        $this->set('answerList',$answer);
        $this->set('dataList',$dataList);
        $this->set('usersDate',$usersDate);
        $this->set('comment_type',$commentTopic); 
    }  
    function obj_comment(){ 
        $this->admin_power('users_comment_edit',2); 
        $params = $this->request->data;
        $id = $this->request->query('cid');  
        $type = $this->request->query('ctype');   
        $commentTopic=Configure::read('COMMENT_TOPIC');
        $this->loadModel('Comment');    
        $this->loadModel('User');     
        $this->loadModel('Share');   
        $this->loadModel('Article');   
        $info=[];
        $ajaxComment='';
        if($type==$commentTopic['分享']) { 
            $info = $this->Share->find('first',array('conditions'=>array('type'=>0,'id'=>$id))); 
            $info['Share']['username']="试管无忧";
            $picTopic=Configure::read('PIC_TOPIC');
            if($info['Share']['uid']>0){
                $userInfo=$this->User->findById(intval($info['Share']['uid'])); 
                $info['Share']['username']=isset($userInfo['User']['nick_name'])?$userInfo['User']['nick_name']:"保密";
                $info['Share']['avatar']=isset($userInfo['User']['avatar'])?$userInfo['User']['avatar']:'';  
            } 
            if(empty($info)) 
                $this->admin_redirect('/admin/users_share',2,'分享不存在！');   
            $info=$info['Share'];
            $ajaxComment='share_comment';
        }
        else if($type==$commentTopic['问答']) { 
            $info = $this->Share->find('first',array('conditions'=>array('type'=>1,'id'=>$id)));
            if(empty($info)) 
                $this->admin_redirect('/admin/users_ask',2,'问答不存在！');   

            $info['Share']['username']="试管无忧";
            $picTopic=Configure::read('PIC_TOPIC');
            if($info['Share']['uid']>0){
                $userInfo=$this->User->findById(intval($info['Share']['uid'])); 
                $info['Share']['username']=isset($userInfo['User']['nick_name'])?$userInfo['User']['nick_name']:"保密";
                $info['Share']['avatar']=isset($userInfo['User']['avatar'])?$userInfo['User']['avatar']:'';  
            } 
            $info=$info['Share'];
            $ajaxComment='answer_comment';
        }
        else if($type==$commentTopic['攻略']) { 
            $info = $this->Article->find('first',array('conditions'=>array('id'=>$id)));
            if(empty($info)) 
                $this->admin_redirect('/admin/users_ask',2,'攻略不存在！');   

            $info['Article']['username']="试管无忧";
            $picTopic=Configure::read('PIC_TOPIC');
            if($info['Article']['uid']>0){
                $userInfo=$this->User->findById(intval($info['Article']['uid'])); 
                $info['Article']['username']=isset($userInfo['User']['nick_name'])?$userInfo['User']['nick_name']:"保密";
                $info['Article']['avatar']=isset($userInfo['User']['avatar'])?$userInfo['User']['avatar']:'';  
            } 
            $info=$info['Article'];
            $ajaxComment='plan_comment';
        }
        $comments = $this->Comment->find('all',array('conditions'=>array('topic_type'=>$type,'parent_id'=>0,'topic_id'=>$id),'order'=>array('is_top'=>'desc','sort'=>'desc','comment_time' => 'desc'),'limit' => 5));
        if(!empty($comments)){
            foreach ($comments as $key => $value) { 
                $userInfo = $this->User->findById($value['Comment']['uid']);    
                $childCmt = $this->Comment->find('all',array('conditions'=>array('topic_type'=>$type,'parent_id'=>$value['Comment']['id']),'order'=>array('is_top'=>'desc','sort'=>'desc','comment_time' => 'asc'),'fields'=>array('id','comment_time','uid','reply_id','content')));
                $childAttr=[];
                if(!empty($childCmt)){
                    foreach ($childCmt as $kk => $vv) {
                        $uInfo = $this->User->findById($vv['Comment']['uid']); 
                        if(!empty($uInfo)){ 
                            $vv['Comment']['replayName']=isset($uInfo['User']['nick_name'])?$uInfo['User']['nick_name']:$uInfo['User']['mobile'];
                            if($vv['Comment']['reply_id']>0 && $vv['Comment']['reply_id']!=$value['Comment']['id']){
                                $cInfo = $this->Comment->findById($vv['Comment']['reply_id']); 
                                if(empty($cInfo)) continue; 
                                $uInfo = $this->User->findById($cInfo['Comment']['uid']); 
                                $vv['Comment']['replayName']=$vv['Comment']['replayName']." <em>回复</em> ".(isset($uInfo['User']['nick_name'])?$uInfo['User']['nick_name']:$uInfo['User']['mobile']);

                            }
                            $childAttr[]=$vv['Comment'];
                        } 
                    }
                }
                $comments[$key]['Comment']['username']=isset($userInfo['User']['nick_name'])?$userInfo['User']['nick_name']:"保密";
                $comments[$key]['Comment']['avatar']=$userInfo['User']['avatar'];
                $comments[$key]['Comment']['childCmt']=$childAttr;
            }
        } 
        $usersDate = $this->User->find('all',array('conditions'=>array('status'=>1),'fields'=>array('id','nick_name'))); 
        $this->set('info',$info);  
        $this->set('comments',$comments); 
        $this->set('usersDate',$usersDate); 
        $this->set('ajaxComment',$ajaxComment);  
        $this->set('ctype',$type);  
    } 

    function replay_comment(){
        // 发表评论   
        $this->admin_power('users_comment_edit',2);  
        $params = $this->request->data;
        $commentTopic=Configure::read('COMMENT_TOPIC');
        $this->loadModel('Share');   
        $this->loadModel('Article');   
        $this->loadModel('Comment');  
        if($this->request->is('post')){ 
            if($params['cid'] <=0 || $params['ctype']<=0){
                $this->ajaxReturn(4008, '非法请求!');  
            }
            if($params['uid'] <=0){
                $this->ajaxReturn(4006, '请选择发布会员!');  
            }
            if(empty($params['content'])){
                $this->ajaxReturn(4007, '内容不能为空!');  
            } 
            $type=$params['ctype'];
            if($type==$commentTopic['分享']) {  
                $info = $this->Share->find('first',array('conditions'=>array('type'=>0,'id'=>intval($params['cid']))));
                if(empty($info))
                    $this->ajaxReturn(4006, '非法提交!');   
                $cinfo = $this->Comment->find('first',array('conditions'=>array('topic_type'=>$commentTopic['分享'],'topic_id'=>$info['Share']['id'],'reply_id'=>0,'uid'=>$params['uid'])));
                if(!empty($cinfo))
                    $this->ajaxReturn(4009, '不能重复评论!');
                //添加评论
                $data=[];
                $data['topic_type']=$commentTopic['分享'];
                $data['topic_id']=$info['Share']['id'];
                $data['uid'] = $params['uid']; 
                $data['sort'] = isset($params['sort'])?$params['sort']:0; 
                $data['content'] = $params['content'];   
                $data['comment_time']=!empty($params['comment_time'])?date("Y-m-d H:i:s",strtotime($params['comment_time'])):date("Y-m-d H:i:s",time());
                $data['create_time']=date("Y-m-d H:i:s",time());
                $this->Comment->save($data);
                $data=[];
                $data['id']=$info['Share']['id'];
                if($info['Share']['is_replay']==0){
                    $data['is_replay']=1;
                }
                $data['comment_num']=$info['Share']['comment_num']+1;
                $this->Share->save($data);
                $this->ajaxReturn(200, '发表成功！');     
            }
            else if($type==$commentTopic['问答']) {  
                $info = $this->Share->find('first',array('conditions'=>array('type'=>1,'id'=>intval($params['cid']))));
                if(empty($info))
                    $this->ajaxReturn(4006, '非法提交!');   
                $cinfo = $this->Comment->find('first',array('conditions'=>array('topic_type'=>$commentTopic['问答'],'topic_id'=>$info['Share']['id'],'reply_id'=>0,'uid'=>$params['uid'])));
                if(!empty($cinfo))
                    $this->ajaxReturn(4009, '不能重复评论!');
                //添加评论
                $data=[];
                $data['topic_type']=$commentTopic['问答'];
                $data['topic_id']=$info['Share']['id'];
                $data['uid'] = $params['uid']; 
                $data['sort'] = isset($params['sort'])?$params['sort']:0; 
                $data['content'] = $params['content'];   
                $data['comment_time']=!empty($params['comment_time'])?date("Y-m-d H:i:s",strtotime($params['comment_time'])):date("Y-m-d H:i:s",time());
                $data['create_time']=date("Y-m-d H:i:s",time());
                $this->Comment->save($data);
                $data=[];
                $data['id']=$info['Share']['id'];
                if($info['Share']['is_replay']==0){
                    $data['is_replay']=1;
                }
                $data['comment_num']=$info['Share']['comment_num']+1;
                $this->Share->save($data);
                $this->ajaxReturn(200, '发表成功！');  
            }
            else if($type==$commentTopic['攻略']) {  
                $info = $this->Article->find('first',array('conditions'=>array('id'=>intval($params['cid']))));
                if(empty($info))
                    $this->ajaxReturn(4006, '非法提交!');   

                $cinfo = $this->Comment->find('first',array('conditions'=>array('topic_type'=>$commentTopic['攻略'],'topic_id'=>$info['Article']['id'],'reply_id'=>0,'uid'=>$params['uid'])));
                if(!empty($cinfo))
                    $this->ajaxReturn(4009, '不能重复评论!');  
                //添加评论
                $data=[];
                $data['topic_type']=$commentTopic['攻略'];
                $data['topic_id']=$info['Article']['id'];
                $data['uid'] = $params['uid']; 
                $data['sort'] = isset($params['sort'])?$params['sort']:0; 
                $data['content'] = $params['content'];   
                $data['comment_time']=!empty($params['comment_time'])?date("Y-m-d H:i:s",strtotime($params['comment_time'])):date("Y-m-d H:i:s",time());
                $data['create_time']=date("Y-m-d H:i:s",time());
                $this->Comment->save($data);
                $data=[];
                $data['id']=$info['Article']['id']; 
                $data['comment_num']=$info['Article']['comment_num']+1;
                $this->Article->save($data);
                $this->ajaxReturn(200, '发表成功！');  
            }  
        }
        $this->ajaxReturn(4007, '异常提交！');    
    }

    function replay_answer_comment(){
         // 发表评论   
        $this->admin_power('users_comment_edit',2);  
        $params = $this->request->data;
        $commentTopic=Configure::read('COMMENT_TOPIC');
        $this->loadModel('Share');   
        $this->loadModel('Comment');  
        $this->loadModel('Article');   
        if($this->request->is('post') && $params['dosubmit']==1){ 
            if($params['ctype']<=0){
                $this->ajaxReturn(4008, '非法请求!');  
            }
            if($params['cid'] <=0 || $params['reply_id'] <=0){
                $this->ajaxReturn(4008, '请选择回复评论!');  
            }
            if(empty($params['content'])){
                $this->ajaxReturn(4007, '内容不能为空!');  
            }
            if($params['uid'] <=0){
                $this->ajaxReturn(4006, '请选择发布会员!');  
            }

            $type=$params['ctype'];
            if($type==$commentTopic['分享']) {  
                $info = $this->Share->find('first',array('conditions'=>array('type'=>0,'id'=>intval($params['cid']))));
                if(empty($info))
                    $this->ajaxReturn(4006, '非法提交!');  

                $rpinfo = $this->Comment->findById(intval($params['reply_id']));

                if(empty($rpinfo))
                    $this->ajaxReturn(4007, '非法提交!');   

                if($rpinfo['Comment']['uid']==$params['uid']){
                    $this->ajaxReturn(4008, '不能回复自己!');  
                }  
                //添加评论
                $data=[];
                if($rpinfo['Comment']['parent_id']==0)
                    $data['parent_id']=$rpinfo['Comment']['id'];
                else
                    $data['parent_id']=$rpinfo['Comment']['parent_id']; 
                $data['topic_type']=$commentTopic['分享'];
                $data['topic_id']=$info['Share']['id'];
                $data['reply_id']=intval($params['reply_id']);
                $data['uid'] = $params['uid']; 
                $data['sort'] = isset($params['sort'])?$params['sort']:0; 
                $data['content'] = $params['content'];   
                $data['comment_time']=!empty($params['comment_time'])?date("Y-m-d H:i:s",strtotime($params['comment_time'])):date("Y-m-d H:i:s",time());
                $data['create_time']=date("Y-m-d H:i:s",time());
                $this->Comment->save($data);
                $data=[];
                $data['id']=$info['Share']['id'];
                if($info['Share']['is_replay']==0){
                    $data['is_replay']=1;
                }
                $data['comment_num']=$info['Share']['comment_num']+1;
                $this->Share->save($data);
                $this->ajaxReturn(200, '发表成功！'); 
            }
            else if($type==$commentTopic['问答']) {  
                $info = $this->Share->find('first',array('conditions'=>array('type'=>1,'id'=>intval($params['cid']))));
                if(empty($info))
                    $this->ajaxReturn(4006, '非法提交!');  

                $rpinfo = $this->Comment->findById(intval($params['reply_id']));

                if(empty($rpinfo))
                    $this->ajaxReturn(4007, '非法提交!');   

                if($rpinfo['Comment']['uid']==$params['uid']){
                    $this->ajaxReturn(4008, '不能回复自己!');  
                }  
                //添加评论
                $data=[];
                if($rpinfo['Comment']['parent_id']==0)
                    $data['parent_id']=$rpinfo['Comment']['id'];
                else
                    $data['parent_id']=$rpinfo['Comment']['parent_id']; 
                $data['topic_type']=$commentTopic['问答'];
                $data['topic_id']=$info['Share']['id'];
                $data['reply_id']=intval($params['reply_id']);
                $data['uid'] = $params['uid']; 
                $data['sort'] = isset($params['sort'])?$params['sort']:0; 
                $data['content'] = $params['content'];   
                $data['comment_time']=!empty($params['comment_time'])?date("Y-m-d H:i:s",strtotime($params['comment_time'])):date("Y-m-d H:i:s",time());
                $data['create_time']=date("Y-m-d H:i:s",time());
                $this->Comment->save($data);
                $data=[];
                $data['id']=$info['Share']['id'];
                if($info['Share']['is_replay']==0){
                    $data['is_replay']=1;
                }
                $data['comment_num']=$info['Share']['comment_num']+1;
                $this->Share->save($data);
                $this->ajaxReturn(200, '发表成功！'); 
            }
            else if($type==$commentTopic['攻略']) {  
                $info = $this->Article->find('first',array('conditions'=>array('id'=>intval($params['cid']))));
                if(empty($info))
                    $this->ajaxReturn(4006, '非法提交!');  

                $rpinfo = $this->Comment->findById(intval($params['reply_id']));

                if(empty($rpinfo))
                    $this->ajaxReturn(4007, '非法提交!');   

                if($rpinfo['Comment']['uid']==$params['uid']){
                    $this->ajaxReturn(4008, '不能回复自己!');  
                }  
                //添加评论
                $data=[];
                if($rpinfo['Comment']['parent_id']==0)
                    $data['parent_id']=$rpinfo['Comment']['id'];
                else
                    $data['parent_id']=$rpinfo['Comment']['parent_id']; 
                $data['topic_type']=$commentTopic['攻略'];
                $data['topic_id']=$info['Article']['id'];
                $data['reply_id']=intval($params['reply_id']);
                $data['uid'] = $params['uid']; 
                $data['sort'] = isset($params['sort'])?$params['sort']:0; 
                $data['content'] = $params['content'];   
                $data['comment_time']=!empty($params['comment_time'])?date("Y-m-d H:i:s",strtotime($params['comment_time'])):date("Y-m-d H:i:s",time());
                $data['create_time']=date("Y-m-d H:i:s",time());
                $this->Comment->save($data);
                $data=[];
                $data['id']=$info['Article']['id']; 
                $data['comment_num']=$info['Article']['comment_num']+1;
                $this->Article->save($data);
                $this->ajaxReturn(200, '发表成功！'); 
            }
              
        }
        $this->ajaxReturn(4007, '异常提交！'); 
    }

    function account_message()
    { 
        $this->admin_power('account_message',2);
        $this->loadModel('Leave');  
        $params = $this->request->query;
        $conditions = array();
        if(!empty($params['username'])){

            if (is_numeric($params['username'])) 
                $conditions['phone like'] = '%'.$params['username'].'%'; 
            else{  
                $conditions['name like'] = '%'.$params['username'].'%'; 
            }
        }
        if(!empty($params['startTime'])){
            $startTime=date("Y-m-d 00:00:00",strtotime($params['startTime']));
            $conditions['ctime >= '] = $startTime;   
        }
        if(!empty($params['endTime'])){
            $endTime=date("Y-m-d 23:59:59",strtotime($params['endTime']));
            $conditions['ctime <= '] = $endTime;   
        }  
        $limit=isset($params['import']) && $params['import']==1?1000:10;

        $this->paginate = array('conditions' => $conditions,'order'=>array('id' => 'desc'),'limit' => $limit);
        try {
            $users = $this->paginate('Leave'); 
        } catch (NotFoundException $e) {
            $users = array();
        }  
        $tmpData=[]; 
        if(!empty($users)){
            foreach ($users as $key => $value) { 
                $tmpData[]=$value['Leave']; 
            }
        }

        if(isset($params['import']) && $params['import']==1){
            //导出                     
            $xlsCell  = array( 
            array('name','真实姓名'),
            array('phone','手机号'),
            array('qq','QQ'),
            array('email','电子邮件'),
            array('content','留言内容'),
            array('ctime','留言时间')
            );   
            $xlsName="留言导出";   
            Ivf::exportExcel($xlsName,$xlsCell,$tmpData);
        }
        $this->set('dataList',$users);  
    }

    function account_appoint()
    { 
        $this->admin_power('users_appoint_list',2);
        $this->loadModel('UsersAppoint'); 
        $this->loadModel('Hospital'); 
        $this->loadModel('Doctor'); 
        $params = $this->request->query;
        $conditions = array();
        if(!empty($params['username'])){

            if (is_numeric($params['username'])) 
                $conditions['mobile like'] = '%'.$params['username'].'%'; 
            else{  
                $conditions['user_name like'] = '%'.$params['username'].'%'; 
            }
        }
        if(!empty($params['startTime'])){
            $startTime=date("Y-m-d 00:00:00",strtotime($params['startTime']));
            $conditions['create_time >= '] = $startTime;   
        }
        if(!empty($params['endTime'])){
            $endTime=date("Y-m-d 23:59:59",strtotime($params['endTime']));
            $conditions['create_time <= '] = $endTime;   
        }  
        $limit=isset($params['import']) && $params['import']==1?1000:10;

        $this->paginate = array('conditions' => $conditions,'order'=>array('id' => 'desc'),'fields'=>array('*'),'limit' => $limit);
        try {
            $users = $this->paginate('UsersAppoint'); 
        } catch (NotFoundException $e) {
            $users = array();
        } 
        $disease=Configure::read('DISEASE_TOPIC');
        $other_type=Configure::read('OTHER_TYPE');
        $tmpData=[];
        if(!empty($users)){
            foreach ($users as $key => $value) {
                $users[$key]['UsersAppoint']['hospital_id']='无';
                $users[$key]['UsersAppoint']['doctor_id']='无';
                if($value['UsersAppoint']['hospital_id']>0){ 
                    $dataInfo = $this->Hospital->findById($value['UsersAppoint']['hospital_id']);  
                    $users[$key]['UsersAppoint']['hospital_id']= isset($dataInfo['Hospital']['cn_name'])?$dataInfo['Hospital']['cn_name']:"无";
                }
                if($value['UsersAppoint']['doctor_id']>0){ 
                    $dataInfo = $this->Doctor->findById($value['UsersAppoint']['doctor_id']);  
                    $users[$key]['UsersAppoint']['doctor_id']= isset($dataInfo['Doctor']['cn_name'])?$dataInfo['Doctor']['cn_name']:"无";
                }
                if(strpos($value['UsersAppoint']['source'], '91.')===false)
                    $users[$key]['UsersAppoint']['bingzheng']=!isset($disease[$value['UsersAppoint']['types']])?'无':$disease[$value['UsersAppoint']['types']];
                else
                    $users[$key]['UsersAppoint']['bingzheng']=!isset($other_type[$value['UsersAppoint']['types']])?'无':$other_type[$value['UsersAppoint']['types']];
                $users[$key]['UsersAppoint']['create_time']=date('Y-m-d H:i:s',strtotime($value['UsersAppoint']['create_time']));
                $users[$key]['UsersAppoint']['isCall']=$value['UsersAppoint']['status']==1?'是':'否';
                $tmpData[]=$users[$key]['UsersAppoint']; 
            }
        } 
        if(isset($params['import']) && $params['import']==1){
            //导出                             
            $xlsCell  = array(
            array('bingzheng','病症'),
            array('user_name','真实姓名'),
            array('mobile','手机号'),
            array('age','年龄'),
            array('hospital_id','意向医院'),
            array('doctor_id','意向医生'),
            array('description','描述'),
            array('source','提交来源'),
            array('system','提交平台'),
            array('brand','设备'),
            array('isCall','是否联系'),
            array('ip','ip'),
            array('create_time','申请时间'),
            );   
            $xlsName="预约导出";   
            Ivf::exportExcel($xlsName,$xlsCell,$tmpData);
        }
        $this->set('dataList',$users);  
    }
    function article_cate(){
        $this->admin_power('article_cate',2);
        $this->loadModel('ArticleCate');  
        $cates = $this->ArticleCate->find('all',array('conditions'=>array('type'=>0))); 
        $newCates=[];
        if(!empty($cates)){
            foreach ($cates as $key => $value) {
                $newCates[]=$value['ArticleCate'];
            }
        }
        $dataList=empty($newCates)?array():Ivf::Totree($newCates);   
        $this->set('dataList',$dataList);
    }

    function news_cate(){
        $this->admin_power('news_cate',2);
        $this->loadModel('ArticleCate');  
        $cates = $this->ArticleCate->find('all',array('conditions'=>array('type'=>1))); 
        $newCates=[];
        if(!empty($cates)){
            foreach ($cates as $key => $value) {
                $newCates[]=$value['ArticleCate'];
            }
        }
        $dataList=empty($newCates)?array():Ivf::Totree($newCates);   
        $this->set('dataList',$dataList);
    }

    function site_comments(){
        $this->admin_power('site_comments',2);
        $this->loadModel('SiteComment');  
        $dataList = $this->SiteComment->find('all');  
        $this->set('dataList',$dataList);
    }

    function site_comments_edit(){
        $this->layout = 'admin_write';
        $this->admin_power('site_comments_edit',2);  
        $params = $this->request->data;
        $uid = $this->request->query('id');   
        $this->loadModel('SiteComment');  
        $dataList=[];
        if(intval($uid)>0){ 
            $dataList = $this->SiteComment->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/site_comments',2,'评论不存在！');
            $dataList=$dataList['SiteComment'];  
        }
        if($this->request->is('post') && $params['dosubmit']==1){     
            if(empty($params['comment_time']))
                $params['comment_time']=date("Y-m-d H:i:s",time());
            else
                $params['comment_time']=date("Y-m-d H:i:s",strtotime($params['comment_time'])); 
            
            if(intval($uid)>0)  
                $params['id'] = $uid;     
            else
                $params['create_time']=date("Y-m-d H:i:s",time());

            if(!$this->SiteComment->save($params))  
                $this->admin_redirect('/admin/site_comments',2,'操作失败，请稍后再试');  
            if($uid>0){ 
                $this->adminLog($params['mobile'], 'edit', "赴泰点评",$params);
            }   
            else
                $this->adminLog($params['mobile'], 'add', "赴泰点评"); 
            $this->redirect("/admin/site_comments");     
        }      
        $this->set('dataList',$dataList);
    }

    function news_cate_edit(){
        $this->layout = 'admin_write';
        $this->admin_power('news_cate_edit',2);  
        $params = $this->request->data;
        $uid = $this->request->query('id');   
        $this->loadModel('ArticleCate');   
        $dataList=[];
        if(intval($uid)>0){ 
            $dataList = $this->ArticleCate->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/news_cate',2,'分类不存在！');
            $dataList=$dataList['ArticleCate'];  
        }
        if($this->request->is('post') && $params['dosubmit']==1){  
            $params['status'] = isset($params['status'])?$params['status']:0;  
            $params['type'] = 1;  
            if(intval($uid)>0)  
                $params['id'] = $uid;     
            if(!$this->ArticleCate->save($params))  
                $this->admin_redirect('/admin/news_cate',2,'操作失败，请稍后再试');  
            if($uid>0){ 
                $this->adminLog($params['name'], 'edit', "新闻分类",$params);
            }   
            else
                $this->adminLog($params['name'], 'add', "新闻分类"); 
            $this->redirect("/admin/news_cate");     
        }     
        $cates = $this->ArticleCate->find('all',array('conditions'=>array('type'=>1))); 
        $newCates=[];
        if(!empty($cates)){
            foreach ($cates as $key => $value) {
                $newCates[]=$value['ArticleCate'];
            }
        }
        $cateList=empty($newCates)?array():Ivf::Totree($newCates);  
        $this->set('cateList',$cateList); 
        $this->set('dataList',$dataList);
    }
    function article_cate_edit(){
        $this->layout = 'admin_write';
        $this->admin_power('article_cate_edit',2);  
        $params = $this->request->data;
        $uid = $this->request->query('id');   
        $this->loadModel('ArticleCate');   
        $dataList=[];
        if(intval($uid)>0){ 
            $dataList = $this->ArticleCate->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/article_cate',2,'分类不存在！');
            $dataList=$dataList['ArticleCate'];  
        }
        if($this->request->is('post') && $params['dosubmit']==1){  
            $params['status'] = isset($params['status'])?$params['status']:0;  
            $params['type'] = 0;  
            if(intval($uid)>0)  
                $params['id'] = $uid;     
            if(!$this->ArticleCate->save($params))  
                $this->admin_redirect('/admin/article_cate',2,'操作失败，请稍后再试');  
            if($uid>0){ 
                $this->adminLog($params['name'], 'edit', "攻略分类",$params);
            }   
            else
                $this->adminLog($params['name'], 'add', "攻略分类"); 
            $this->redirect("/admin/article_cate");     
        }     
        $cates = $this->ArticleCate->find('all',array('conditions'=>array('type'=>0))); 
        $newCates=[];
        if(!empty($cates)){
            foreach ($cates as $key => $value) {
                $newCates[]=$value['ArticleCate'];
            }
        }
        $cateList=empty($newCates)?array():Ivf::Totree($newCates);  
        $this->set('cateList',$cateList); 
        $this->set('dataList',$dataList);
    } 

    function news(){
        $this->admin_power('news_list',2);
        $this->loadModel('New');
        $this->loadModel('ArticleCate');   
        $params = $this->request->query;
        $conditions = array();
        if(!empty($params['keywords']))
            $conditions['title like'] = '%'.$params['keywords'].'%';  
        
        if(!empty($params['category_id']))
            $conditions['category_id'] = intval($params['category_id']);  

        if(!empty($params['startTime'])){
            $startTime=date("Y-m-d 00:00:00",strtotime($params['startTime']));
            $conditions['create_time >= '] = $startTime;   
        }
        if(!empty($params['endTime'])){
            $endTime=date("Y-m-d 23:59:59",strtotime($params['endTime']));
            $conditions['create_time <= '] = $endTime;   
        }  
        $this->paginate = array('conditions' => $conditions,'order'=>array('id' => 'desc'),'limit' => 10);
        try {
            $article = $this->paginate('New'); 
        } catch (NotFoundException $e) {
            $article = array();
        } 
        if(!empty($article)){
            foreach ($article as $key => $value) {
                $article[$key]['New']['cate_name']='无';
                if($value['New']['category_id']>0){ 
                    $cates = $this->ArticleCate->findById($value['New']['category_id']);  
                    $article[$key]['New']['cate_name']= isset($cates['ArticleCate']['name'])?$cates['ArticleCate']['name']:"无";
                }
            }
        }
        $cates = $this->ArticleCate->find('all',array('conditions'=>array('type'=>1,'status'=>1))); 
        $newCates=[];
        if(!empty($cates)){
            foreach ($cates as $key => $value) {
                $newCates[]=$value['ArticleCate'];
            }
        }
        $cateList=empty($newCates)?array():Ivf::Totree($newCates); 
        $this->set('cateList',$cateList);
        $this->set('dataList',$article);
    }
    function news_edit(){
        $this->admin_power('news_edit',2); 
        $params = $this->request->data;
        $uid = $this->request->query('id'); 
        $this->loadModel('New');
        $this->loadModel('ArticleCate');    
        $dataList=[];
        if(intval($uid)>0){ 
            $dataList = $this->New->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/news',2,'新闻不存在！');
            $dataList=$dataList['New'];  
        } 
        if($this->request->is('post') && $params['dosubmit']==1){  
            $params['status'] = isset($params['status'])?$params['status']:0;  
            $params['is_home'] = isset($params['is_home'])?$params['is_home']:0;
            $params['update_time']=date("Y-m-d H:i:s",time());   
            if(intval($uid)>0)  
                $params['id'] = $uid;   
            else { 
                $params['author_id'] = $this->user['id']; 
                $params['create_time']=date("Y-m-d H:i:s",time());
            }

            if (isset($this->request['form']['imgFile']) && is_uploaded_file($this->request['form']['imgFile']['tmp_name'])) {  
                $attrRes=Ivf::uploadImg('/uploads/news/');
                if($attrRes['status']!=200)
                    $this->admin_redirect('/admin/news',2,'图片上传失败！原因:'.$attrRes['message']);
                $params['image'] =$attrRes['url'];  
            }


            if (isset($this->request['form']['imgFile2']) && is_uploaded_file($this->request['form']['imgFile2']['tmp_name'])) { 
                $attrRes=[];
                $_FILES['imgFile']=$_FILES['imgFile2'];
                $attrRes=Ivf::uploadImg('/uploads/news/');
                if($attrRes['status']!=200)
                    $this->admin_redirect('/admin/news',2,'图片上传失败！原因:'.$attrRes['message']);
                $params['mb_image'] =$attrRes['url'];  
            }
            if(empty($params['description']) && !empty($params['content'])){
                $params['description']=Ivf::getSubstr(strip_tags($params['content']),0,200); 
            } 
            if(!$this->New->save($params))  
                $this->admin_redirect('/admin/news',2,'操作失败，请稍后再试');  
            if($uid>0){ 
                $this->adminLog($params['title'], 'edit', "新闻",$params);
            }   
            else
                $this->adminLog($params['title'], 'add', "新闻"); 
            $this->redirect("/admin/news");     
        }    
        $cates = $this->ArticleCate->find('all',array('conditions'=>array('type'=>1,'status'=>1))); 
        $newCates=[];
        if(!empty($cates)){
            foreach ($cates as $key => $value) {
                $newCates[]=$value['ArticleCate'];
            }
        }
        $cateList=empty($newCates)?array():Ivf::Totree($newCates);   
        $this->set('cateList',$cateList);
        $this->set('dataList',$dataList); 
    }
    function article(){
        $this->admin_power('article_list',2);
        $this->loadModel('Article');
        $this->loadModel('ArticleCate');   
        $params = $this->request->query;
        $conditions = array();
        if(!empty($params['keywords']))
            $conditions['title like'] = '%'.$params['keywords'].'%';  
        
        if(!empty($params['category_id']))
            $conditions['category_id'] = intval($params['category_id']);  

        if(!empty($params['startTime'])){
            $startTime=date("Y-m-d 00:00:00",strtotime($params['startTime']));
            $conditions['create_time >= '] = $startTime;   
        }
        if(!empty($params['endTime'])){
            $endTime=date("Y-m-d 23:59:59",strtotime($params['endTime']));
            $conditions['create_time <= '] = $endTime;   
        }  
        $this->paginate = array('conditions' => $conditions,'order'=>array('id' => 'desc'),'limit' => 10);
        try {
            $article = $this->paginate('Article'); 
        } catch (NotFoundException $e) {
            $article = array();
        } 
        if(!empty($article)){
            foreach ($article as $key => $value) {
                $article[$key]['Article']['cate_name']='无';
                if($value['Article']['category_id']>0){ 
                    $cates = $this->ArticleCate->findById($value['Article']['category_id']);  
                    $article[$key]['Article']['cate_name']= isset($cates['ArticleCate']['name'])?$cates['ArticleCate']['name']:"无";
                }
            }
        }
        $cates = $this->ArticleCate->find('all',array('conditions'=>array('type'=>0,'status'=>1))); 
        $newCates=[];
        if(!empty($cates)){
            foreach ($cates as $key => $value) {
                $newCates[]=$value['ArticleCate'];
            }
        }
        $cateList=empty($newCates)?array():Ivf::Totree($newCates); 
        $this->set('cateList',$cateList);
        $this->set('dataList',$article);
    }
   
    function users_share(){
        $this->admin_power('users_share',2); 
        $params = $this->request->query;
        $this->loadModel('Share');
        $this->loadModel('User');   
        $this->loadModel('District');
        $conditions = array();
        $conditions['type'] = 0;
        if(!empty($params['keywords']))
            $conditions['title like'] = '%'.$params['keywords'].'%';  
        
        if(!empty($params['user_id'])){
            $conditions['uid'] = $params['user_id'];
        }
        if(!empty($params['tag'])){
            $conditions['tag'] = intval($params['tag']);
        }

        if(!empty($params['startTime'])){
            $startTime=date("Y-m-d 00:00:00",strtotime($params['startTime']));
            $conditions['create_time >= '] = $startTime;   
        }
        if(!empty($params['endTime'])){
            $endTime=date("Y-m-d 23:59:59",strtotime($params['endTime']));
            $conditions['create_time <= '] = $endTime;   
        }  

        $this->paginate = array(
            'conditions' => $conditions,
            'order' => array('id' => 'desc'),
            'limit' => 10
        );
        try {
            $shares = $this->paginate('Share');
        } catch (NotFoundException $e) {
            $shares = array();
        }
        if(!empty($shares))
        {
            foreach ($shares as $key => $value) {
                if($value['Share']['uid']>0)
                {
                    $userInfo = $this->User->findById($value['Share']['uid']); 
                    $shares[$key]['Share']['uid']=isset($userInfo['User']['nick_name'])?$userInfo['User']['nick_name']:"无";
                }

                if($value['Share']['city_id']>0)
                {
                    $ctInfo = $this->District->findById($value['Share']['city_id']); 
                    $shares[$key]['Share']['city_id']=$ctInfo['District']['country'];
                }
            }
        }
        $district = $this->District->find('all'); 
        $this->set('district', $district); 
        $this->set('share_tags',Configure::read('SHARE_TAG'));
        $this->set('share_tags2',Configure::read('SHARE_TAG_TWO'));
        $this->set('dataList',$shares);
    }
    function users_ask(){ 
        $this->admin_power('users_ask',2); 
        $params = $this->request->query;
        $this->loadModel('Share');
        $this->loadModel('User');   
        $this->loadModel('District');
        $conditions = array();
        $conditions['type'] = 1;
        if(!empty($params['keywords']))
            $conditions['content like'] = '%'.$params['keywords'].'%';  
        
        if(!empty($params['user_id'])){
            $conditions['uid'] = $params['user_id'];
        }
        if(!empty($params['tag'])){
            $conditions['tag'] = intval($params['tag']);
        }

        if(!empty($params['startTime'])){
            $startTime=date("Y-m-d 00:00:00",strtotime($params['startTime']));
            $conditions['create_time >= '] = $startTime;   
        }
        if(!empty($params['endTime'])){
            $endTime=date("Y-m-d 23:59:59",strtotime($params['endTime']));
            $conditions['create_time <= '] = $endTime;   
        }  

        $this->paginate = array(
            'conditions' => $conditions,
            'order' => array('id' => 'desc'),
            'limit' => 10
        );
        try {
            $shares = $this->paginate('Share');
        } catch (NotFoundException $e) {
            $shares = array();
        }
        if(!empty($shares))
        {
            foreach ($shares as $key => $value) { 
                if($value['Share']['city_id']>0)
                {
                    $ctInfo = $this->District->findById($value['Share']['city_id']); 
                    $shares[$key]['Share']['city_id']=$ctInfo['District']['country'];
                }
            }
        }
        $district = $this->District->find('all'); 
        $this->set('district', $district); 
        $this->set('share_tags',Configure::read('SHARE_TAG'));
        $this->set('dataList',$shares);
    }
    function users_ask_edit(){ 
        $this->admin_power('users_ask_edit',2);  
        $params = $this->request->data;
        $uid = $this->request->query('id'); 
        $this->loadModel('Share');   
        $this->loadModel('User');   
        $this->loadModel('Picture');   
        $dataList=$picData=[];
        $picTopic=Configure::read('PIC_TOPIC');
        if(intval($uid)>0){ 
            $dataList = $this->Share->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/users_ask',2,'提问不存在！');
            $dataList=$dataList['Share'];  
            $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['问答'],'obj_id'=>intval($uid)),'fields'=>array('id','thumb')));  
        } 
        if($this->request->is('post') && $params['dosubmit']==1){  
            $params['status'] = isset($params['status'])?$params['status']:0;  
            $params['is_top'] = isset($params['is_top'])?$params['is_top']:0;
            $params['update_time']=date("Y-m-d H:i:s",time());   
            if(intval($uid)>0)  
                $params['id'] = $uid;   
            else {  
                $params['create_time']=date("Y-m-d H:i:s",time());
                $params['type']=1;
            }
            if(!$this->Share->save($params))  
                $this->admin_redirect('/admin/users_ask',2,'操作失败，请稍后再试');  
            if(!empty($params['UptPicture'])){ 
                foreach ($params['UptPicture'] as $val){  
                    $data=[]; 
                    $data['id'] = $val;  
                    $data['obj_type'] = $picTopic['问答'];  
                    $data['obj_id'] = $this->Share->id;  
                    $this->Picture->save($data);
                }  
            }
            if($uid>0){ 
                $this->adminLog($params['id'], 'edit', "提问",$params);
            }   
            else
                $this->adminLog($params['content'], 'add', "提问"); 
            $this->redirect("/admin/users_ask");     
        }    
        $usersDate = $this->User->find('all',array('conditions'=>array('status'=>1),'fields'=>array('id','nick_name'))); 
        $this->loadModel('District');  
        $districtList = $this->District->find('all');   
        $this->set('picData',$picData);
        $this->set('districtList',$districtList);
        $this->set('usersDate',$usersDate); 
        $this->set('share_tags',Configure::read('SHARE_TAG'));
        $this->set('dataList',$dataList); 
    }
    function replay_share(){
        $this->layout = 'admin_write';
        $this->admin_power('share_edit',2);  
        $params = $this->request->data; 
        $this->loadModel('Comment');  
        $this->loadModel('User');  
        $this->loadModel('Share');  
        $uid = $this->request->query('id');  
        $commentTopic=Configure::read('COMMENT_TOPIC');
        if($this->request->is('post') && $params['dosubmit']==1){    
            $info = $this->Share->findById($uid);
            if(empty($info))
                $this->admin_redirect('/admin/users_share',2,'分享不存在');     
            $params['topic_type'] = $commentTopic['分享'];   
            $params['topic_id'] = $uid;   
            $params['create_time']=date("Y-m-d H:i:s",time());
            if(!$this->Comment->save($params))  
                $this->admin_redirect('/admin/users_share',2,'操作失败，请稍后再试');   
            $this->adminLog($params['content'], 'add', "分享评论"); 
            $data=[];
            $data['id']=$uid;
            if($info['Share']['is_replay']==0){
                $data['is_replay']=1;
            }
            $data['comment_num']=intval($info['Share']['comment_num'])+1; 
            $data['update_time']=date("Y-m-d H:i:s",time()); 
            $this->Share->save($data); 
            $this->redirect("/admin/users_share");     
        }  
        $usersDate = $this->User->find('all',array('conditions'=>array('status'=>1),'fields'=>array('id','nick_name')));    
        $this->set('usersDate',$usersDate); 
    }
    function replay_ask(){
        $this->layout = 'admin_write';
        $this->admin_power('users_ask_edit',2);  
        $params = $this->request->data; 
        $this->loadModel('Comment');  
        $this->loadModel('User');  
        $this->loadModel('Share');  
        $uid = $this->request->query('id'); 
        $commentTopic=Configure::read('COMMENT_TOPIC');
        if($this->request->is('post') && $params['dosubmit']==1){   
            $info = $this->Share->findById($uid);
            if(empty($info))
                $this->admin_redirect('/admin/users_ask',2,'问答不存在');     
            $params['topic_type'] = $commentTopic['问答']; 
            $params['topic_id'] = $uid;   
            $params['create_time']=date("Y-m-d H:i:s",time());
            if(!$this->Comment->save($params))  
                $this->admin_redirect('/admin/users_ask',2,'操作失败，请稍后再试');   
            $this->adminLog($params['content'], 'add', "问答评论");  
            $data=[];
            $data['id']=$uid;
            if($info['Share']['is_replay']==0){
                $data['is_replay']=1;
            }
            $data['comment_num']=intval($info['Share']['comment_num'])+1; 
            $data['update_time']=date("Y-m-d H:i:s",time()); 
            $this->Share->save($data); 

            $this->redirect("/admin/users_ask");     
        }  
        $usersDate = $this->User->find('all',array('conditions'=>array('status'=>1),'fields'=>array('id','nick_name')));    
        $this->set('usersDate',$usersDate); 
    }

    function share_edit(){
        $this->admin_power('share_edit',2);  
        $params = $this->request->data;
        $uid = $this->request->query('id'); 
        $this->loadModel('Share');   
        $this->loadModel('User');   
        $this->loadModel('Picture');   
        $dataList=$picData=[];
        $picTopic=Configure::read('PIC_TOPIC');
        if(intval($uid)>0){ 
            $dataList = $this->Share->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/users_share',2,'分享不存在！');
            $dataList=$dataList['Share'];  
            $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['分享'],'obj_id'=>intval($uid)),'fields'=>array('id','thumb')));  
        } 
        if($this->request->is('post') && $params['dosubmit']==1){  
            $params['status'] = isset($params['status'])?$params['status']:0;  
            $params['is_top'] = isset($params['is_top'])?$params['is_top']:0;
            $params['is_rec'] = isset($params['is_rec'])?$params['is_rec']:0;
            $params['update_time']=date("Y-m-d H:i:s",time());   

            if(intval($uid)>0)  
                $params['id'] = $uid;   
            else {  
                $params['create_time']=date("Y-m-d H:i:s",time());
            }
            if(empty($params['description']) && !empty($params['content'])){
                $params['description']=Ivf::getSubstr(strip_tags($params['content']),0,200); 
            } 
            if(!$this->Share->save($params))  
                $this->admin_redirect('/admin/users_share',2,'操作失败，请稍后再试');  
            if(!empty($params['UptPicture'])){ 
                foreach ($params['UptPicture'] as $val){  
                    $data=[]; 
                    $data['id'] = $val;  
                    $data['obj_type'] = $picTopic['分享'];  
                    $data['obj_id'] = $this->Share->id;  
                    $this->Picture->save($data);
                }  
            }
            if($uid>0){ 
                $this->adminLog($params['title'], 'edit', "分享",$params);
            }   
            else
                $this->adminLog($params['title'], 'add', "分享"); 
            $this->redirect("/admin/users_share");     
        }    
        $usersDate = $this->User->find('all',array('conditions'=>array('status'=>1),'fields'=>array('id','nick_name'))); 
        $this->loadModel('Hospital');   
        $this->loadModel('District');  
        $hospitals = $this->Hospital->find('all',array(
            'conditions'=>array('status'=>1),
            'fields'=>array('id','cn_name'),
            )
        );  
        $this->set('picData',$picData);
        $this->set('hospitalList',$hospitals);
        $districtList = $this->District->find('all');   
        $this->set('districtList',$districtList);
        $this->set('usersDate',$usersDate); 
        $this->set('share_tags',Configure::read('SHARE_TAG'));
        $this->set('share_tags2',Configure::read('SHARE_TAG_TWO'));
        $this->set('dataList',$dataList); 
    }
    function article_edit(){
        $this->admin_power('article_edit',2); 
        $params = $this->request->data;
        $uid = $this->request->query('id'); 
        $this->loadModel('Article');  
        $this->loadModel('ArticleCate');   
        $this->loadModel('User');   
        $this->loadModel('Picture');   
        $picTopic=Configure::read('PIC_TOPIC');
        $dataList=$picData=[];
        if(intval($uid)>0){ 
            $dataList = $this->Article->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/article',2,'攻略不存在！');
            $dataList=$dataList['Article'];  
            $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['攻略'],'obj_id'=>intval($uid)),'fields'=>array('id','thumb')));  
        } 
        if($this->request->is('post') && $params['dosubmit']==1){  
            $params['status'] = isset($params['status'])?$params['status']:0; 
            $params['is_elite'] = isset($params['is_elite'])?$params['is_elite']:0;  
            $params['is_top'] = isset($params['is_top'])?$params['is_top']:0;
            $params['update_time']=date("Y-m-d H:i:s",time());   

            if(intval($uid)>0)  
                $params['id'] = $uid;   
            else {  
                $params['author_id'] = $this->user['id']; 
                $params['create_time']=date("Y-m-d H:i:s",time());
            }
            if(empty($params['description']) && !empty($params['content'])){
                $params['description']=Ivf::getSubstr(strip_tags($params['content']),0,200); 
            } 
            if(!$this->Article->save($params))  
                $this->admin_redirect('/admin/article',2,'操作失败，请稍后再试');   
            if(!empty($params['UptPicture'])){ 
                foreach ($params['UptPicture'] as $val){  
                    $data=[]; 
                    $data['id'] = $val;  
                    $data['obj_type'] = $picTopic['攻略'];  
                    $data['obj_id'] = $this->Article->id;  
                    $this->Picture->save($data);
                }  
            }
            if($uid>0){ 
                $this->adminLog($params['title'], 'edit', "攻略",$params);
            }   
            else
                $this->adminLog($params['title'], 'add', "攻略"); 
            $this->redirect("/admin/article");     
        }   
        $cates = $this->ArticleCate->find('all',array('conditions'=>array('type'=>0,'status'=>1))); 
        $newCates=[];
        if(!empty($cates)){
            foreach ($cates as $key => $value) {
                $newCates[]=$value['ArticleCate'];
            }
        }
        $cateList=empty($newCates)?array():Ivf::Totree($newCates); 
        $usersDate = $this->User->find('all',array('conditions'=>array('status'=>1),'fields'=>array('id','nick_name')));  
        
        $this->loadModel('Hospital');   
        $hospitals = $this->Hospital->find('all',array(
            'conditions'=>array('status'=>1),
            'fields'=>array('id','cn_name'),
            )
        );  
        $this->set('hospitalList',$hospitals);
        $this->set('picData',$picData);
        $this->set('usersDate',$usersDate);
        $this->set('cateList',$cateList);
        $this->set('dataList',$dataList); 

    }

    function video_cate(){ 
        $this->admin_power('video_cate',2);
        $this->loadModel('VideoCate');  
        $cates = $this->VideoCate->find('all'); 
        $newCates=[];
        if(!empty($cates)){
            foreach ($cates as $key => $value) {
                $newCates[]=$value['VideoCate'];
            }
        }
        $dataList=empty($newCates)?array():Ivf::Totree($newCates);   
        $this->set('dataList',$dataList);
    }

    function music_albums(){ 
        $this->admin_power('music_albums',2);
        $this->loadModel('Album');  
        $cates = $this->Album->find('all');   
        $this->set('dataList',$cates);
        $this->set('cateList',Configure::read('ALBUM_CATE'));
    }
    function music_albums_edit(){ 
        $this->admin_power('music_albums_edit',2);  
        $params = $this->request->data;
        $uid = $this->request->query('id');   
        $this->loadModel('Album');  
        $dataList=[];
        if(intval($uid)>0){ 
            $dataList = $this->Album->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/music_albums',2,'专辑不存在！');
            $dataList=$dataList['Album'];  
        }
        if($this->request->is('post') && $params['dosubmit']==1){  
            $params['status'] = isset($params['status'])?$params['status']:0;  
            if(intval($uid)>0)  
                $params['id'] = $uid;   
            else
                $params['create_time']=date("Y-m-d H:i:s",time()); 

            if (isset($this->request['form']['imgFile']) && is_uploaded_file($this->request['form']['imgFile']['tmp_name'])) {  
                $attrRes=Ivf::uploadImg('/uploads/albums/');
                if($attrRes['status']!=200)
                    $this->admin_redirect('/admin/music_albums',2,'图片上传失败！原因:'.$attrRes['message']);
                $params['img'] =$attrRes['url'];  
            }

            if(!$this->Album->save($params))  
                $this->admin_redirect('/admin/music_albums',2,'操作失败，请稍后再试');  
            if($uid>0){ 
                $this->adminLog($params['name'], 'edit', "专辑",$params);
            }   
            else
                $this->adminLog($params['name'], 'add', "专辑"); 
            $this->redirect("/admin/music_albums");     
        }     
        $this->set('cateList',Configure::read('ALBUM_CATE'));
        $this->set('dataList',$dataList);
    }

    function video_cate_edit(){
        $this->layout = 'admin_write';
        $this->admin_power('video_cate_edit',2);  
        $params = $this->request->data;
        $uid = $this->request->query('id');   
        $this->loadModel('VideoCate');  
        $dataList=[];
        if(intval($uid)>0){ 
            $dataList = $this->VideoCate->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/video_cate',2,'分类不存在！');
            $dataList=$dataList['VideoCate'];  
        }
        if($this->request->is('post') && $params['dosubmit']==1){  
            $params['status'] = isset($params['status'])?$params['status']:0;  

            if (isset($this->request['form']['imgFile']) && is_uploaded_file($this->request['form']['imgFile']['tmp_name'])) {  
                $attrRes=Ivf::uploadImg('/uploads/video_cate/');
                if($attrRes['status']!=200)
                    $this->admin_redirect('/admin/video_cate',2,'图片上传失败！原因:'.$attrRes['message']);
                $params['img'] =$attrRes['url'];  
            }
            if(intval($uid)>0)  
                $params['id'] = $uid;      
            if(!$this->VideoCate->save($params))  
                $this->admin_redirect('/admin/video_cate',2,'操作失败，请稍后再试');  
            if($uid>0){ 
                $this->adminLog($params['name'], 'edit', "视频分类",$params);
            }   
            else
                $this->adminLog($params['name'], 'add', "视频分类"); 
            $this->redirect("/admin/video_cate");     
        }    
        $cates = $this->VideoCate->find('all'); 
        $newCates=[];
        if(!empty($cates)){
            foreach ($cates as $key => $value) {
                $newCates[]=$value['VideoCate'];
            }
        }
        $cateList=empty($newCates)?array():Ivf::Totree($newCates);  
        $this->set('cateList',$cateList); 
        $this->set('dataList',$dataList);
    }
    function videos(){
        $this->admin_power('video_list',2);
        $this->loadModel('Video');  
        $this->loadModel('VideoCate');  
        $dataList = $this->Video->find('all'); 
        
        if(!empty($dataList)){
            foreach ($dataList as $key => $value) {
                $dataList[$key]['Video']['cate_name']='无';
                if($value['Video']['parent_id']>0){ 
                    $cates = $this->VideoCate->findById($value['Video']['parent_id']);  
                    $dataList[$key]['Video']['cate_name']= isset($cates['VideoCate']['name'])?$cates['VideoCate']['name']:"无";
                }
            }
        }
        $this->set('dataList',$dataList);
 
    }
    function video_edit(){
        $this->admin_power('video_edit',2);
        $params = $this->request->data;
        $uid = $this->request->query('id');   
        $this->loadModel('Video');    
        $this->loadModel('VideoCate');   
        $dataList=[];
        if(intval($uid)>0){ 
            $dataList = $this->Video->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/videos',2,'视频不存在！');
            $dataList=$dataList['Video'];  
        }

        if($this->request->is('post') && $params['dosubmit']==1){  
            $params['rec'] = isset($params['rec'])?$params['rec']:0; 
            $params['status'] = isset($params['status'])?$params['status']:0;   
            $params['author_id'] = $this->user['id']; 

            if (isset($this->request['form']['imgFile']) && is_uploaded_file($this->request['form']['imgFile']['tmp_name'])) {  
                $attrRes=Ivf::uploadImg('/uploads/video/');
                if($attrRes['status']!=200)
                    $this->admin_redirect('/admin/videos',2,'图片上传失败！原因:'.$attrRes['message']);
                $params['img'] =$attrRes['url'];  
            }
            
            if(intval($uid)>0)  
                $params['id'] = $uid;  
            else
                $params['create_time']=date("Y-m-d H:i:s",time());
 
            if(!$this->Video->save($params))  
                $this->admin_redirect('/admin/videos',2,'操作失败，请稍后再试');  
            if($uid>0){ 
                $this->adminLog($params['title'], 'edit', "视频",$params);
            }   
            else
                $this->adminLog($params['title'], 'add', "视频"); 
            $this->redirect("/admin/videos");     
        }      
 
        $cates = $this->VideoCate->find('all'); 
        $newCates=[];
        if(!empty($cates)){
            foreach ($cates as $key => $value) {
                $newCates[]=$value['VideoCate'];
            }
        }
        $cateList=empty($newCates)?array():Ivf::Totree($newCates);  
        $this->set('cateList',$cateList); 
        $this->set('dataList',$dataList);
 
    }

    function music(){
        $this->admin_power('music_list',2);
        $this->loadModel('Audio');  
        $this->loadModel('Album');  
        $dataList = $this->Audio->find('all');  
        $cateList=[];
        $cates = $this->Album->find('all');   
        foreach ($cates as $key => $value) {
             $cateList[$value['Album']['id']]=$value['Album']['name'];
         }  
        $this->set('cateList',$cateList); 
        $this->set('dataList',$dataList);
 
    }
    function music_edit(){
        $this->admin_power('music_edit',2);
        $params = $this->request->data;
        $uid = $this->request->query('id');   
        $this->loadModel('Audio');      
        $this->loadModel('Album');  
        $dataList=[];
        if(intval($uid)>0){ 
            $dataList = $this->Audio->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/music',2,'音频不存在！');
            $dataList=$dataList['Audio'];  
        }

        if($this->request->is('post') && $params['dosubmit']==1){   
            $params['status'] = isset($params['status'])?$params['status']:0;    
            $params['h'] = empty($params['h']) ? '00' : $params['h'];
            $params['i'] = empty($params['i']) ? '00' : $params['i'];
            $params['s'] = empty($params['s']) ? '00' : $params['s'];
            $params['length'] = $params['h'].':'.$params['i'].':'.$params['s'];
            if(!empty($params['length'])){
                if(!Ivf::isDatetime($params['length'], 'H:i:s')){ 
                    $this->admin_redirect('/admin/music',2,'格式错误，请按 00:00:00 格式填写！');
                }
            }

            if(intval($uid)>0)  
                $params['id'] = $uid;  
            else
                $params['create_time']=date("Y-m-d H:i:s",time());
 
            if(!$this->Audio->save($params))  
                $this->admin_redirect('/admin/music',2,'操作失败，请稍后再试');  
            if($uid>0){ 
                $this->adminLog($params['title'], 'edit', "音频",$params);
            }   
            else
                $this->adminLog($params['title'], 'add', "音频"); 
            $this->redirect("/admin/music");     
        }      
  
        $cateList = $this->Album->find('all',array('conditions' => array('status'=>1)));    
        $this->set('cateList',$cateList); 
        $this->set('dataList',$dataList);
 
    }
    function link(){
        $this->admin_power('link_setting',2); 
        $this->loadModel('Link');  
        $dataList = $this->Link->find('all'); 
        $this->set('dataList',$dataList);
    }
    function link_edit(){
        $this->admin_power('link_edit',2);  
        $params = $this->request->data;
        $uid = $this->request->query('id');   
        $this->loadModel('Link');   
        $dataList=[];
        if(intval($uid)>0){ 
            $dataList = $this->Link->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/link',2,'链接不存在！');
            $dataList=$dataList['Link'];  
        }

        if($this->request->is('post') && $params['dosubmit']==1){  
            $params['status'] = isset($params['status'])?$params['status']:0;  
            if(intval($uid)>0)  
                $params['id'] = $uid;   
            if (isset($this->request['form']['imgFile']) && is_uploaded_file($this->request['form']['imgFile']['tmp_name'])) { 
                $attrRes=Ivf::uploadImg('/uploads/link/');
                if($attrRes['status']!=200)
                    $this->admin_redirect('/admin/link',2,'图片上传失败！原因:'.$attrRes['message']);
                $params['logo'] =$attrRes['url'];  
            }
 
            if(!$this->Link->save($params))  
                $this->admin_redirect('/admin/index',2,'操作失败，请稍后再试');  
            if($uid>0){ 
                $this->adminLog($params['name'], 'edit', "友情链接",$params);
            }   
            else
                $this->adminLog($params['name'], 'add', "友情链接"); 
            $this->redirect("/admin/link");     
        }      
        $this->set('dataList',$dataList);
    }

    function blackIp(){
        $this->admin_power('blackIp_list',2); 
        $this->loadModel('BlackIp');  
        $dataList = $this->BlackIp->find('all');  
        $this->set('dataList',$dataList);
    }
    function blackIp_edit(){
        $this->admin_power('blackIp_edit',2);  
        $params = $this->request->data;
        $uid = $this->request->query('id');   
        $this->loadModel('BlackIp');   
        $dataList=[];
        if(intval($uid)>0){ 
            $dataList = $this->BlackIp->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/blackIp',2,'不存在！');
            $dataList=$dataList['BlackIp'];  
        }

        if($this->request->is('post') && $params['dosubmit']==1){  
            $params['status'] = isset($params['status'])?$params['status']:0;  
            if(intval($uid)>0)  
                $params['id'] = $uid;   

            $cinfo = $this->BlackIp->find('first',array('conditions'=>array('address'=>trim($params['address'])))); 
            if(!empty($cinfo)){
                if(intval($uid)==0 || (isset($params['id']) && $cinfo['BlackIp']['id'] != intval($uid)))
                    $this->admin_redirect('/admin/blackIp',2,'不能重复添加');   
            }  
  
            if(!$this->BlackIp->save($params))  
                $this->admin_redirect('/admin/blackIp',2,'操作失败，请稍后再试');  
            if($uid>0){ 
                $this->adminLog($params['name'], 'edit', "设置黑名单",$params);
            }   
            else
                $this->adminLog($params['name'], 'add', "设置黑名单"); 
            $this->redirect("/admin/blackIp");     
        }      
        $this->set('dataList',$dataList);
    }


    function maps(){
        $this->admin_power('map_setting',2); 
        $this->loadModel('Map');  
        $this->loadModel('MapCate');   
        $dataList = $this->Map->find('all'); 
        if(!empty($dataList)){
            foreach ($dataList as $key => $value) {
                $dataList[$key]['Map']['cate_name']='无';
                if($value['Map']['parent_id']>0){ 
                    $cates = $this->MapCate->findById($value['Map']['parent_id']);  
                    $dataList[$key]['Map']['cate_name']= isset($cates['MapCate']['name'])?$cates['MapCate']['name']:"无";
                }
            }
        }
        $this->set('dataList',$dataList);
    }
    function map_edit(){
        $this->admin_power('map_edit',2);  
        $params = $this->request->data;
        $uid = $this->request->query('id');   
        $this->loadModel('Map');   
        $this->loadModel('MapCate');   
        $dataList=[];
        if(intval($uid)>0){ 
            $dataList = $this->Map->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/maps',2,'地图不存在！');
            $dataList=$dataList['Map'];  
        }

        if($this->request->is('post') && $params['dosubmit']==1){  
            $params['status'] = isset($params['status'])?$params['status']:0;  
            if(intval($uid)>0)  
                $params['id'] = $uid;    
 
            if(!$this->Map->save($params))  
                $this->admin_redirect('/admin/maps',2,'操作失败，请稍后再试');  
            if($uid>0){ 
                $this->adminLog($params['name'], 'edit', "地图",$params);
            }   
            else
                $this->adminLog($params['name'], 'add', "地图"); 
            $this->redirect("/admin/maps");     
        }    
        $cates = $this->MapCate->find('all',array('conditions'=>array('status'=>1))); 
        $newCates=[];
        if(!empty($cates)){
            foreach ($cates as $key => $value) {
                $newCates[]=$value['MapCate'];
            }
        }
        $cateList=empty($newCates)?array():Ivf::Totree($newCates);  
        $this->set('cateList',$cateList);   
        $this->set('dataList',$dataList);
    }


    function map_cates(){
        $this->admin_power('map_cates',2);
        $this->loadModel('MapCate');  
        $cates = $this->MapCate->find('all'); 
        $newCates=[];
        if(!empty($cates)){
            foreach ($cates as $key => $value) {
                $newCates[]=$value['MapCate'];
            }
        }
        $dataList=empty($newCates)?array():Ivf::Totree($newCates);   
        $this->set('dataList',$dataList);
    }

    function map_cates_edit(){
        $this->layout = 'admin_write';
        $this->admin_power('map_cates_edit',2);  
        $params = $this->request->data;
        $uid = $this->request->query('id');   
        $this->loadModel('MapCate');   
        $dataList=[];
        if(intval($uid)>0){ 
            $dataList = $this->MapCate->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/map_cates',2,'地图分类不存在！');
            $dataList=$dataList['MapCate'];  
        }
        if($this->request->is('post') && $params['dosubmit']==1){  
            $params['status'] = isset($params['status'])?$params['status']:0;   
            if(intval($uid)>0)  
                $params['id'] = $uid;     
            if(!$this->MapCate->save($params))  
                $this->admin_redirect('/admin/map_cates',2,'操作失败，请稍后再试');  
            if($uid>0){ 
                $this->adminLog($params['name'], 'edit', "地图分类",$params);
            }   
            else
                $this->adminLog($params['name'], 'add', "地图分类"); 
            $this->redirect("/admin/map_cates");     
        }     
        $cates = $this->MapCate->find('all'); 
        $newCates=[];
        if(!empty($cates)){
            foreach ($cates as $key => $value) {
                $newCates[]=$value['MapCate'];
            }
        }
        $cateList=empty($newCates)?array():Ivf::Totree($newCates);  
        $this->set('cateList',$cateList); 
        $this->set('dataList',$dataList);
    }

    function district(){
        $this->admin_power('district_list',2); 
        $this->loadModel('District');  
        $dataList = $this->District->find('all'); 
        $this->set('dataList',$dataList);
    }
    function set_keywords(){
        $this->admin_power('keywords_setting',2);  
        $this->loadModel('Keyword');  
        $dataList = $this->Keyword->find('all'); 
        $this->set('dataList',$dataList);
        $this->set('keywords',Configure::read('KEYWORDS_TYPE'));

    }
    function joins(){ 
        $this->admin_power('joins',2); 
        $this->loadModel('Join');  
        $dataList = $this->Join->find('all'); 
        $this->set('dataList',$dataList);
        $this->set('education',Configure::read('EDUCATION'));
        $this->set('experience',Configure::read('Experience'));
    }

    function join_edit(){
        $this->admin_power('join_edit',2); 
        $params = $this->request->data;
        $uid = $this->request->query('id');   
        $this->loadModel('Join'); 
        $dataList=[]; 
        if(intval($uid)>0){ 
            $dataList = $this->Join->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/join_edit',2,'招聘不存在！');
            $dataList=$dataList['Join'];  
        }
        if($this->request->is('post') && $params['dosubmit']==1){    
            if(intval($uid)>0)  
                $params['id'] = $uid;    
            else 
                $params['create_time']=date("Y-m-d H:i:s",time()); 
            if(!$this->Join->save($params))  
                $this->admin_redirect('/admin/joins',2,'操作失败，请稍后再试');  
            if($uid>0){ 
                $this->adminLog($params['title'], 'edit', "人事招聘",$params);
            }   
            else
                $this->adminLog($params['title'], 'add', "人事招聘"); 
            $this->redirect("/admin/joins");     
        }    
        $this->set('dataList',$dataList); 
        $this->set('education',Configure::read('EDUCATION'));
        $this->set('experience',Configure::read('Experience'));
    }

    function channels(){
        $this->admin_power('channels',2); 
        $this->loadModel('Yonghuqudao');  
        $dataList = $this->Yonghuqudao->find('all'); 
        $this->set('dataList',$dataList); 
    }
    function channel_edit(){
        $this->admin_power('channel_edit',2); 
        $params = $this->request->data;
        $uid = $this->request->query('id');   
        $this->loadModel('Yonghuqudao'); 
        $dataList=[]; 
        if(intval($uid)>0){ 
            $dataList = $this->Yonghuqudao->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/channel_edit',2,'渠道不存在！');
            $dataList=$dataList['Yonghuqudao'];  
        }
        if($this->request->is('post') && $params['dosubmit']==1){   
            if(intval($uid)>0)  
                $params['id'] = $uid;      
            if(!$this->Yonghuqudao->save($params))  
                $this->admin_redirect('/admin/channels',2,'操作失败，请稍后再试');  
            if($uid>0){ 
                $this->adminLog($params['title'], 'edit', "推广渠道",$params);
            }   
            else
                $this->adminLog($params['title'], 'add', "推广渠道"); 
            $this->redirect("/admin/channels");     
        }    
        $this->set('dataList',$dataList); 
    }
    function specials(){ 
        $this->admin_power('specials',2); 
        $this->loadModel('Special');   
        $dataList = $this->Special->find("all",array('fields'=>array('*')));  
        $this->set('dataList',$dataList); 
    }

    function specials_edit(){
        $this->admin_power('specials_edit',2);   
        $params = $this->request->data;
        $uid = $this->request->query('id');   
        $this->loadModel('Special'); 
        $dataList=[]; 
        if(intval($uid)>0){  
            $dataList = $this->Special->find("first",array('conditions'=>array('id'=>$uid),'fields'=>array('*')));  
            if(empty($dataList))
                $this->admin_redirect('/admin/specials',2,'专题不存在！');
            $dataList=$dataList['Special'];   
        }
        if($this->request->is('post') && $params['dosubmit']==1){  
            $params['update_time'] = date("Y-m-d H:i:s",time());   
            if(intval($uid)>0)  { 
                $params['id'] = $uid;  
            }
            else
                $params['create_time'] = date("Y-m-d H:i:s",time());    
            if(!$this->Special->save($params))  
                $this->admin_redirect('/admin/specials',2,'操作失败，请稍后再试');  
            if($uid>0){ 
                $this->adminLog($params['title'], 'edit', "自定义专题",$params);
            }   
            else
                $this->adminLog($params['title'], 'add', "自定义专题"); 
            $this->redirect("/admin/specials");     
        }    
        $this->set('dataList',$dataList); 
    }
    function keywords_edit(){
        $this->layout = 'admin_write';
        $this->admin_power('keywords_edit',2);  
        $params = $this->request->data;
        $uid = $this->request->query('id');   
        $this->loadModel('Keyword');  
        $dataList=[];
        if(intval($uid)>0){ 
            $dataList = $this->Keyword->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/set_keywords',2,'关键词不存在！');
            $dataList=$dataList['Keyword'];  
        }
        if($this->request->is('post') && $params['dosubmit']==1){   
            if(intval($uid)>0)  
                $params['id'] = $uid;      
            if(!$this->Keyword->save($params))  
                $this->admin_redirect('/admin/set_keywords',2,'操作失败，请稍后再试');  
            if($uid>0){ 
                $this->adminLog($params['key'], 'edit', "设置关键词",$params);
            }   
            else
                $this->adminLog($params['key'], 'add', "设置关键词"); 
            $this->redirect("/admin/set_keywords");     
        }    
        $this->set('dataList',$dataList);
        $this->set('keywords',Configure::read('KEYWORDS_TYPE'));

    }
    function district_edit(){
        $this->admin_power('district_edit',2);  
        $params = $this->request->data;
        $uid = $this->request->query('id'); 
        $this->loadModel('District');  
        $dataList=[];
        if(intval($uid)>0){ 
            $dataList = $this->District->findById($uid);  
            if(empty($dataList))
                $this->admin_redirect('/admin/district',2,'地区不存在！');
            $dataList=$dataList['District'];  
        }

        if($this->request->is('post') && $params['dosubmit']==1){   
            if(intval($uid)>0)  
                $params['id'] = $uid;    

            if(!$this->District->save($params))  
                $this->admin_redirect('/admin/district',2,'操作失败，请稍后再试');  
            if($uid>0){ 
                $this->adminLog($params['country'], 'edit', "地区",$params);
            }   
            else
                $this->adminLog($params['country'], 'add', "地区"); 
            $this->redirect("/admin/district");     
        }      
        $this->set('dataList',$dataList);
    }
    function users_info($mobe){
        $this->admin_power('users_account_list',2);
        $this->loadModel('User');
        $params = $this->request->query;
        $conditions = array();
        if(!empty($params['username'])){

            if (is_numeric($params['username'])) 
                $conditions['mobile like'] = '%'.$params['username'].'%'; 
            else{ 
                $conditions["OR"]['nick_name like'] = '%'.$params['username'].'%';
                $conditions["OR"]['user_name like'] = '%'.$params['username'].'%'; 
            }
        }

        $this->paginate = array('conditions' => array('mobile'=>$mobe),'order'=>array('id' => 'desc'),'limit' => 10);
        try {
            $users = $this->paginate('User'); 
        } catch (NotFoundException $e) {
            $users = array();
        }
        //预约
        $this->loadModel('Users_appoint');
        $this->loadModel('Hospital');
        $this->loadModel('Doctor');
        $yuy=$this->Users_appoint->find('all',array('conditions'=>array('mobile'=>$mobe),'order'=>array('id' => 'desc')));      
        if(!empty($yuy)){
            foreach ($yuy as $key => $value) {
                $yuy[$key]['Users_appoint']['doctor_id'] = '无';
                $yuy[$key]['Users_appoint']['hospital_id'] = '无';
                if($value['Users_appoint']['hospital_id']>0){ 
                    $dataInfo = $this->Hospital->findById(intval($value['Users_appoint']['hospital_id']));  
                    $yuy[$key]['Users_appoint']['hospital_id']= isset($dataInfo['Hospital']['cn_name'])?$dataInfo['Hospital']['cn_name']:"无";
                }
                if($value['Users_appoint']['doctor_id']>0){ 
                    $dataInfo = $this->Doctor->findById(intval($value['Users_appoint']['doctor_id']));  
                    $yuy[$key]['Users_appoint']['doctor_id']= isset($dataInfo['Doctor']['cn_name'])?$dataInfo['Doctor']['cn_name']:"无";
                }

            }
        }
        $this->set('yuy',$yuy); 

        //攻略
        $this->loadModel('Article');
        $article_info=$this->Article->find('all',array('order'=>array('id' => 'desc')));
        foreach ($article_info as $key=>$value) { 
            if(intval($value['Article']['uid'])>0){ 
                $user_info = $this->User->findById(intval($value['Article']['uid']));
                if(isset($user_info['User']['id']))
                    $article_info[$key]['Article']['uid']=$user_info['User']['mobile'];
                }
        }  
        foreach($article_info as $k=>$v){
            if($article_info[$k]['Article']['uid']==$mobe){
                $article_text[]=$v;
            }
        }
        if (empty($article_text)) {
            $article_text['0']['Article']['title']='无';
            $article_text['0']['Article']['id']='无';
            $article_text['0']['Article']['create_time']='无';
        }
        $this->set('article_text',$article_text);

        // 分享
        $this->loadModel('Share');
        $share_info=$this->Share->find('all',array('conditions'=>array('type'=>0),'order'=>array('id' => 'desc')));
        foreach ($share_info as $key=>$value) { 
        if(intval($value['Share']['uid'])>0){ 
            $user_info = $this->User->findById(intval($value['Share']['uid']));
            if(isset($user_info['User']['id']))
                $share_info[$key]['Share']['uid']=$user_info['User']['mobile'];
            } 
        }  
        foreach($share_info as $k=>$v){
            if($share_info[$k]['Share']['uid']==$mobe){
                $share_text[]=$v;
            }
        }
        if (empty($share_text)) {
            $share_text['0']['Share']['title']='无';
            $share_text['0']['Share']['id']='无';
            $share_text['0']['Share']['create_time']='无';
        }
        $this->set('share_text',$share_text);
        //问答
        $answer_info=$this->Share->find('all',array('conditions'=>array('type'=>1),'order'=>array('id' => 'desc')));
        foreach ($answer_info as $key=>$value) { 
        if(intval($value['Share']['uid'])>0){ 
            $user_info = $this->User->findById(intval($value['Share']['uid']));
            if(isset($user_info['User']['id']))
                $answer_info[$key]['Share']['uid']=$user_info['User']['mobile'];
            } 
        }  
        foreach($answer_info as $k=>$v){
            if($answer_info[$k]['Share']['uid']==$mobe){
                $answer_text[]=$v;
            }
        }
        if (empty($answer_text)) {
            $answer_text['0']['Share']['content']='无';
            $answer_text['0']['Share']['id']='无';
            $answer_text['0']['Share']['create_time']='无';
        }
        $this->set('answer_text',$answer_text);
        //评论 
        $this->loadModel('Comment');
        $comment_info=$this->Comment->find('all',array('order'=>array('id' => 'desc')));
        foreach ($comment_info as $key=>$value) { 
        if(intval($value['Comment']['uid'])>0){ 
            $user_info = $this->User->findById(intval($value['Comment']['uid']));
            if(isset($user_info['User']['id']))
                $comment_info[$key]['Comment']['uid']=$user_info['User']['mobile'];
            } 
        }  
        foreach($comment_info as $k=>$v){
            if($comment_info[$k]['Comment']['uid']==$mobe){
                $comment_text[]=$v;
            }
        }
        if (empty($comment_text)) {
            $comment_text['0']['Comment']['content']='无';
            $comment_text['0']['Comment']['id']='无';
            $comment_text['0']['Comment']['create_time']='无';
        }
        $this->set('comment_text',$comment_text);

        $this->set('dataList',$users);
        $this->set('sex',Configure::read('USER_SEX')); 
    }
}