<?php
App::uses('AdminbaseController', 'Controller');
class AdminloginController extends AdminbaseController {

    public $layout = 'admin_login';

    function beforeFilter() {
        parent::beforeFilter();
    }

    function index(){ 
        $params = $this->request->data; 
        if($this->request->is('post') && $params['dosubmit']==1){   
            if(empty($params['mail']) || empty($params['userpwd'])){
                $this->ajaxReturn(4001, '参数异常！'); 
            } 
            $cookie_time=time() + 86400*3; 
            if(isset($params['d_remenber']) && $params['d_remenber']==1){
                $cookie_time=time() + 86400 * 7;
            }  
            //检查
            $this->loadModel('AdminUser');
            $user = $this->AdminUser->find('first',array(
                'conditions' => array(
                    'email' => $params['mail'],
                    'status' => 1, 
                    )
                )
            ); 
            if(empty($user)) 
                $this->ajaxReturn(4002, '账户不存在'); 

            if($user['AdminUser']['password'] != md5($params['userpwd'].$user['AdminUser']['auth'])){ 
                $this->ajaxReturn(4003, '密码错误，请重新输入'); 
            } 
            unset($user['AdminUser']['password'],$user['AdminUser']['auth']);
            $res = $this->setUser($user['AdminUser']); 
            setcookie(self::ADMIN_USER, $user['AdminUser']['id'], $cookie_time, "/"); 
            $this->ajaxReturn(200, '登陆成功', array(),'/admin/index');
        }
        $user = $this->getUser(); 
        if(!empty($user)){
            $this->redirect('/admin/index');
        }  
    }
}