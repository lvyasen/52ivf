<?php
//社区首页面
class CommunityController extends AppController {

    public $layout = 'comm';

    function beforeFilter() {
        parent::beforeFilter();
        if($this->checkLogin()){ 
            $this->user = $this->getUser();  
            $this->set('userInfo',$this->user); 
        }  
        $this->set('nav','cindex');  
    }

    function index(){
        $this->loadModel('Article');
        $this->loadModel('Share');       
        $this->loadModel('Hospital');   
        $this->loadModel('User');   
        $this->loadModel('Picture');     
        $orderby['sort']="desc";
        $orderby['create_time']="desc";
        $conditions['status'] = 1;
        $conditions['is_top'] = 1;
        $picTopic=Configure::read('PIC_TOPIC');

        $guideList=$answerList=$shareList=[];
        //攻略 
        $guideList = array(); 
        $guideList = $this->Article->find('all',array(
            'conditions' => $conditions, 
            'limit' => 5,
            'order' => $orderby,
            )
        );

        if(!empty($guideList)){
            foreach ($guideList as $key => $value) { 
                $guideList[$key]['Article']['username']="试管无忧";
                $guideList[$key]['Article']['avatar']="/images/temp/head.jpg";
                if(intval($value['Article']['uid'])>0){ 
                     $uInfo=$this->User->findById(intval($value['Article']['uid'])); 
                     $guideList[$key]['Article']['username']=isset($uInfo['User']['nick_name'])?$uInfo['User']['nick_name']:"保密";
                     $guideList[$key]['Article']['avatar']=isset($uInfo['User']['avatar'])?$uInfo['User']['avatar']:"";
                }  
            }
        }  
        // 随机试管问答
        $allId = $this->Share->find('all',array('conditions' => array('type'=>1), 'fields'=>array('id')));

        $rand = array_rand($allId,1);
        $rand_id = $allId[$rand];
        $rand_li = $this->Share->findById($rand_id['Share']['id']);
        $this->set('rand_li',$rand_li);
        $this->set('rand_id',$rand_id);
        
        //精选问答 
        $conditions['type']=1; 
        $answerList = array(); 
        $answerList = $this->Share->find('all',array(
            'conditions' => $conditions, 
            'limit' => 4,
            'order' => $orderby,
            )
        ); 
        unset($conditions['is_top']);
        //就医分享-全部
        $conditions['type']=0;
        $shareList = array(); 
        $shareList['all'] = $this->Share->find('all',array(
            'conditions' => $conditions, 
            'limit' => 10,
            'order' => $orderby,
            )
        ); 

        $this->loadModel('Agree'); 
        $agreeTopic=Configure::read('AGREE_TOPIC');
        $userId=intval($this->user['id']);
        if(!empty($shareList['all'])){
            foreach ($shareList['all'] as $key => $value) { 
                $shareList['all'][$key]['Share']['hospital']="无";
                if(intval($value['Share']['hospital_id'])>0){ 
                    $info = $this->Hospital->findById(intval($value['Share']['hospital_id']));
                    if(isset($info['Hospital']['id']))
                        $shareList['all'][$key]['Share']['hospital']=$info['Hospital']['cn_name'];
                } 
                $agCount = $this->Agree->find('count',array('conditions'=>array('uid'=>$userId,'obj_id'=>$value['Share']['id'],'obj_type'=>$agreeTopic['分享'])));
                $shareList['all'][$key]['Share']['myagree']=$agCount; 
                 $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['分享'],'obj_id'=>intval($value['Share']['id'])),'fields'=>array('thumb'))); 
                 $shareList['all'][$key]['Share']['images']=$picData; 
                 $userInfo=$this->User->findById(intval($value['Share']['uid']));  
                 $shareList['all'][$key]['Share']['username']=isset($userInfo['User']['nick_name'])?$userInfo['User']['nick_name']:"保密";
                 $shareList['all'][$key]['Share']['content']=Ivf::getSubstr(str_replace('　　', '', $value['Share']['content']),0,100);  
                 $shareList['all'][$key]['Share']['avatar']=isset($userInfo['User']['avatar'])?$userInfo['User']['avatar']:""; 
                if(empty($value['Share']['description']) && !empty($value['Share']['content'])){
                    $value['Share']['description']=Ivf::getSubstr(strip_tags($value['Share']['content']),0,200); 
                }  
                $shareList['all'][$key]['Share']['description']= Ivf::getLittleText($value['Share']['description'],Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT'])));  

                          //从内容中取图片方法
                $html = $value['Share']['content'];
                preg_match_all('/<img\s+src=".*?"/', $html, $matches);
                $shareList['all'][$key]['Share']['cont_img'] = $matches;
                
            }
        } 
        //就医分享-初次检查
        $conditions['tag']=10;
        $shareList['frst'] = $this->Share->find('all',array(
            'conditions' => $conditions, 
            'limit' => 10,
            'order' => $orderby,
            )
        ); 

        
        if(!empty($shareList['frst'])){
            foreach ($shareList['frst'] as $key => $value) { 
                $shareList['frst'][$key]['Share']['hospital']="无";
                if(intval($value['Share']['hospital_id'])>0){ 
                    $info = $this->Hospital->findById(intval($value['Share']['hospital_id']));
                    if(isset($info['Hospital']['id']))
                        $shareList['frst'][$key]['Share']['hospital']=$info['Hospital']['cn_name'];
                } 
                 $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['分享'],'obj_id'=>intval($value['Share']['id'])),'fields'=>array('thumb'))); 
                 $shareList['frst'][$key]['Share']['images']=$picData; 
                 $userInfo=$this->User->findById(intval($value['Share']['uid'])); 
                 $shareList['frst'][$key]['Share']['username']=isset($userInfo['User']['nick_name'])?$userInfo['User']['nick_name']:"保密";
                 $shareList['frst'][$key]['Share']['content']=Ivf::getSubstr(str_replace('　　', '', $value['Share']['content']),0,100);  
                 $shareList['frst'][$key]['Share']['avatar']=isset($userInfo['User']['avatar'])?$userInfo['User']['avatar']:"";
                $agCount = $this->Agree->find('count',array('conditions'=>array('uid'=>$userId,'obj_id'=>$value['Share']['id'],'obj_type'=>$agreeTopic['分享'])));
                $shareList['frst'][$key]['Share']['myagree']=$agCount; 
                if(empty($value['Share']['description']) && !empty($value['Share']['content'])){
                    $value['Share']['description']=Ivf::getSubstr(strip_tags($value['Share']['content']),0,200); 
                }  
                $shareList['frst'][$key]['Share']['description']= Ivf::getLittleText($value['Share']['description'],Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT'])));  
                            //从内容中取图片方法
                $html_frst = $value['Share']['content'];
                preg_match_all('/<img\s+src=".*?"/', $html_frst, $frst_img);
                $shareList['frst'][$key]['Share']['cont_img'] = $frst_img;
                
            }
        } 

        //就医分享-促排阶段 
        $conditions['tag']=20;
        $shareList['cpjd'] = $this->Share->find('all',array(
            'conditions' => $conditions, 
            'limit' => 10,
            'order' => $orderby,
            )
        ); 
        if(!empty($shareList['cpjd'])){
            foreach ($shareList['cpjd'] as $key => $value) { 
                $shareList['cpjd'][$key]['Share']['hospital']="无";
                if(intval($value['Share']['hospital_id'])>0){ 
                    $info = $this->Hospital->findById(intval($value['Share']['hospital_id']));
                    if(isset($info['Hospital']['id']))
                        $shareList['cpjd'][$key]['Share']['hospital']=$info['Hospital']['cn_name'];
                } 
                 $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['分享'],'obj_id'=>intval($value['Share']['id'])),'fields'=>array('thumb'))); 
                 $shareList['cpjd'][$key]['Share']['images']=$picData; 
                 $userInfo=$this->User->findById(intval($value['Share']['uid'])); 
                 $shareList['cpjd'][$key]['Share']['username']=isset($userInfo['User']['nick_name'])?$userInfo['User']['nick_name']:"保密";
                 $shareList['cpjd'][$key]['Share']['content']=Ivf::getSubstr(str_replace('　　', '', $value['Share']['content']),0,100);  
                 $shareList['cpjd'][$key]['Share']['avatar']=isset($userInfo['User']['avatar'])?$userInfo['User']['avatar']:"";
                $agCount = $this->Agree->find('count',array('conditions'=>array('uid'=>$userId,'obj_id'=>$value['Share']['id'],'obj_type'=>$agreeTopic['分享'])));
                $shareList['cpjd'][$key]['Share']['myagree']=$agCount; 
                if(empty($value['Share']['description']) && !empty($value['Share']['content'])){
                    $value['Share']['description']=Ivf::getSubstr(strip_tags($value['Share']['content']),0,200); 
                }  
                $shareList['cpjd'][$key]['Share']['description']= Ivf::getLittleText($value['Share']['description'],Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT'])));  
                            //从内容中取图片方法
                $html_cpjd = $value['Share']['content'];
                preg_match_all('/<img\s+src=".*?"/', $html_cpjd, $cpjd_img);
                $shareList['cpjd'][$key]['Share']['cont_img'] = $cpjd_img;
            }
        } 
 
        //就医分享-取卵移植 
        $conditions['tag']=30;
        $shareList['qlyz'] = $this->Share->find('all',array(
            'conditions' => $conditions, 
            'limit' => 10,
            'order' => $orderby,
            )
        ); 

        if(!empty($shareList['qlyz'])){
            foreach ($shareList['qlyz'] as $key => $value) { 
                $shareList['qlyz'][$key]['Share']['hospital']="无";
                if(intval($value['Share']['hospital_id'])>0){ 
                    $info = $this->Hospital->findById(intval($value['Share']['hospital_id']));
                    if(isset($info['Hospital']['id']))
                        $shareList['qlyz'][$key]['Share']['hospital']=$info['Hospital']['cn_name'];
                } 
                 $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['分享'],'obj_id'=>intval($value['Share']['id'])),'fields'=>array('thumb'))); 
                 $shareList['qlyz'][$key]['Share']['images']=$picData; 
                 $userInfo=$this->User->findById(intval($value['Share']['uid'])); 
                 $shareList['qlyz'][$key]['Share']['username']=isset($userInfo['User']['nick_name'])?$userInfo['User']['nick_name']:"保密";
                 $shareList['qlyz'][$key]['Share']['content']=Ivf::getSubstr(str_replace('　　', '', $value['Share']['content']),0,100);  
                 $shareList['qlyz'][$key]['Share']['avatar']=isset($userInfo['User']['avatar'])?$userInfo['User']['avatar']:"";
                $agCount = $this->Agree->find('count',array('conditions'=>array('uid'=>$userId,'obj_id'=>$value['Share']['id'],'obj_type'=>$agreeTopic['分享'])));
                $shareList['qlyz'][$key]['Share']['myagree']=$agCount; 
                if(empty($value['Share']['description']) && !empty($value['Share']['content'])){
                    $value['Share']['description']=Ivf::getSubstr(strip_tags($value['Share']['content']),0,200); 
                }  
                $shareList['qlyz'][$key]['Share']['description']= Ivf::getLittleText($value['Share']['description'],Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT'])));  
                            //从内容中取图片方法
                $html = $value['Share']['content'];
                preg_match_all('/<img\s+src=".*?"/', $html, $img);
                $shareList['qlyz'][$key]['Share']['cont_img'] = $img;
            }
        } 
        //就医分享-保胎 
        $conditions['tag']=40;
        $shareList['bt'] = $this->Share->find('all',array(
            'conditions' => $conditions, 
            'limit' => 10,
            'order' => $orderby,
            )
        ); 
        if(!empty($shareList['bt'])){
            foreach ($shareList['bt'] as $key => $value) { 
                $shareList['bt'][$key]['Share']['hospital']="无";
                if(intval($value['Share']['hospital_id'])>0){ 
                    $info = $this->Hospital->findById(intval($value['Share']['hospital_id']));
                    if(isset($info['Hospital']['id']))
                        $shareList['bt'][$key]['Share']['hospital']=$info['Hospital']['cn_name'];
                } 
                 $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['分享'],'obj_id'=>intval($value['Share']['id'])),'fields'=>array('thumb'))); 
                 $shareList['bt'][$key]['Share']['images']=$picData; 
                 $userInfo=$this->User->findById(intval($value['Share']['uid'])); 
                 $shareList['bt'][$key]['Share']['username']=isset($userInfo['User']['nick_name'])?$userInfo['User']['nick_name']:"保密";
                 $shareList['bt'][$key]['Share']['content']=Ivf::getSubstr(str_replace('　　', '', $value['Share']['content']),0,100);  
                 $shareList['bt'][$key]['Share']['avatar']=isset($userInfo['User']['avatar'])?$userInfo['User']['avatar']:"";
                $agCount = $this->Agree->find('count',array('conditions'=>array('uid'=>$userId,'obj_id'=>$value['Share']['id'],'obj_type'=>$agreeTopic['分享'])));
                $shareList['bt'][$key]['Share']['myagree']=$agCount; 
                if(empty($value['Share']['description']) && !empty($value['Share']['content'])){
                    $value['Share']['description']=Ivf::getSubstr(strip_tags($value['Share']['content']),0,200); 
                }  
                $shareList['bt'][$key]['Share']['description']= Ivf::getLittleText($value['Share']['description'],Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT'])));  
                            //从内容中取图片方法
                $html = $value['Share']['content'];
                preg_match_all('/<img\s+src=".*?"/', $html, $img);
                $shareList['bt'][$key]['Share']['cont_img'] = $img;
            }
        } 
        //就医分享-就医经验 
        $conditions['tag']=50;
        $shareList['jyjy'] = $this->Share->find('all',array(
            'conditions' => $conditions, 
            'limit' => 10,
            'order' => $orderby,
            )
        ); 
        if(!empty($shareList['jyjy'])){
            foreach ($shareList['jyjy'] as $key => $value) { 
                $shareList['jyjy'][$key]['Share']['hospital']="无";
                if(intval($value['Share']['hospital_id'])>0){ 
                    $info = $this->Hospital->findById(intval($value['Share']['hospital_id']));
                    if(isset($info['Hospital']['id']))
                        $shareList['jyjy'][$key]['Share']['hospital']=$info['Hospital']['cn_name'];
                } 
                 $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['分享'],'obj_id'=>intval($value['Share']['id'])),'fields'=>array('thumb'))); 
                 $shareList['jyjy'][$key]['Share']['images']=$picData; 
                 $userInfo=$this->User->findById(intval($value['Share']['uid'])); 
                 $shareList['jyjy'][$key]['Share']['username']=isset($userInfo['User']['nick_name'])?$userInfo['User']['nick_name']:"保密";
                 $shareList['jyjy'][$key]['Share']['content']=Ivf::getSubstr(str_replace('　　', '', $value['Share']['content']),0,100);  
                 $shareList['jyjy'][$key]['Share']['avatar']=isset($userInfo['User']['avatar'])?$userInfo['User']['avatar']:"";
                $agCount = $this->Agree->find('count',array('conditions'=>array('uid'=>$userId,'obj_id'=>$value['Share']['id'],'obj_type'=>$agreeTopic['分享'])));
                $shareList['jyjy'][$key]['Share']['myagree']=$agCount; 
                if(empty($value['Share']['description']) && !empty($value['Share']['content'])){
                    $value['Share']['description']=Ivf::getSubstr(strip_tags($value['Share']['content']),0,200); 
                }  
                $shareList['jyjy'][$key]['Share']['description']= Ivf::getLittleText($value['Share']['description'],Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT'])));  
                            //从内容中取图片方法
                $html = $value['Share']['content'];
                preg_match_all('/<img\s+src=".*?"/', $html, $img);
                $shareList['jyjy'][$key]['Share']['cont_img'] = $img;
            }
        } 
        //就医分享-心情 
        $conditions['tag']=60;
        $shareList['xq'] = $this->Share->find('all',array(
            'conditions' => $conditions, 
            'limit' => 10,
            'order' => $orderby,
            )
        );  
        if(!empty($shareList['xq'])){
            foreach ($shareList['xq'] as $key => $value) { 
                $shareList['xq'][$key]['Share']['hospital']="无";
                if(intval($value['Share']['hospital_id'])>0){ 
                    $info = $this->Hospital->findById(intval($value['Share']['hospital_id']));
                    if(isset($info['Hospital']['id']))
                        $shareList['xq'][$key]['Share']['hospital']=$info['Hospital']['cn_name'];
                } 
                 $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['分享'],'obj_id'=>intval($value['Share']['id'])),'fields'=>array('thumb'))); 
                 $shareList['xq'][$key]['Share']['images']=$picData; 
                 $userInfo=$this->User->findById(intval($value['Share']['uid'])); 
                 $shareList['xq'][$key]['Share']['username']=isset($userInfo['User']['nick_name'])?$userInfo['User']['nick_name']:"保密";
                 $shareList['xq'][$key]['Share']['content']=Ivf::getSubstr(str_replace('　　', '', $value['Share']['content']),0,100);  
                 $shareList['xq'][$key]['Share']['avatar']=isset($userInfo['User']['avatar'])?$userInfo['User']['avatar']:"";
                $agCount = $this->Agree->find('count',array('conditions'=>array('uid'=>$userId,'obj_id'=>$value['Share']['id'],'obj_type'=>$agreeTopic['分享'])));
                $shareList['xq'][$key]['Share']['myagree']=$agCount; 
                if(empty($value['Share']['description']) && !empty($value['Share']['content'])){
                    $value['Share']['description']=Ivf::getSubstr(strip_tags($value['Share']['content']),0,200); 
                }  
                $shareList['xq'][$key]['Share']['description']= Ivf::getLittleText($value['Share']['description'],Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT'])));  
                            //从内容中取图片方法
                $html = $value['Share']['content'];
                preg_match_all('/<img\s+src=".*?"/', $html, $img);
                $shareList['xq'][$key]['Share']['cont_img'] = $img;
            }
        }  
        
        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['社区首页']),'order'=>array('id'=>'desc'))); 
        $randNum=Ivf::aideCount();
        $this->set('keysInfo',$keysInfo);
        $this->set('answerList',$answerList);  
        $this->set('guideList',$guideList);  
        $this->set('randNum',$randNum);  
        $this->set('shareList',$shareList);  
    }

    function seach($ctype = null){  
        $ctype = $this->rm_end_html($ctype);
        $params = $this->get_params($ctype); 
        $ctype = $params[0];  
        if($ctype==0)
            $this->redirect("/plan/index/0--".$params[1].".html");     
        else if($ctype==1)
            $this->redirect("/share/index/0--".$params[1].".html");     
        else
            $this->redirect("/answer/index/0--".$params[1].".html");      
    }


    function ask(){ 
        // 发表提问   
        $this->checkLogin(1);  
        $params = $this->request->data;
        $commentTopic=Configure::read('COMMENT_TOPIC');
        $this->loadModel('Share');     
        if($this->request->is('post') && $params['dosubmit']==1){ 

            if(empty($params['content'])){
                $this->ajaxReturn(4002, '内容不能为空!');  
            } 
            $conditions=[];
            $conditions['status']=1;
            $conditions['type']=1;
            $conditions['uid']=$this->user['id']; 
            $conditions['create_time >= '] = date("Y-m-d 00:00:00",time());   
            $conditions['create_time <= '] = date("Y-m-d 23:59:59",time());    
            $info = $this->Share->find('first',array('conditions'=>$conditions));
            if(!empty($info))
                $this->ajaxReturn(4003, '一天只能提问一次!');  
            //添加提问
            $data=[];
            $data['meta_title']='';
            $data['meta_keywords']='';
            $data['meta_description']='';
            $data['title']='';
            $data['description']='';
            $data['status']=1;  
            $data['type']=1;  
            $data['imgs']=''; 
            $data['tag2']=0; 
            $data['uid'] = $this->user['id']; 
            $data['content'] = $params['content'];  
            $data['tag'] = 0;    
            $data['update_time']=date("Y-m-d H:i:s",time());
            $data['create_time']=date("Y-m-d H:i:s",time());
            $this->Share->save($data); 
            $this->ajaxReturn(200, '提交成功！');   
        }
        $this->ajaxReturn(4007, '异常提交！');    
    }  
}