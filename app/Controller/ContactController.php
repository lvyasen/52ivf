<?php
// 联系我们页面
class ContactController extends AppController {

    public $layout = 'main';

    function beforeFilter() {
        parent::beforeFilter();
        $this->set('nav','contact');  
    }
    function index(){ 
        $this->loadModel('Keyword');  
        $keyTopic=Configure::read('KEYWORDS_TYPE');
        $keysInfo = $this->Keyword->find('first',array('conditions'=>array('key'=>$keyTopic['联系我们']),'order'=>array('id'=>'desc'))); 
        $this->set('keysInfo',$keysInfo);
    }

}