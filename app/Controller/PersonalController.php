<?php

// pc端个人中心
class PersonalController extends AppController {

    public $layout = 'comm';

    function beforeFilter() {
        parent::beforeFilter();
        if($this->checkLogin()){ 
            $this->user = $this->getUser();  
            $this->set('userInfo',$this->user); 
        }  
    }

    function login(){    
        //登陆   
        $params = $this->request->data;
        if($this->request->is('post') && $params['dosubmit']==1){ 
            if(empty($params['u_mobile'])){
                $this->ajaxReturn(4001, '手机号不能为空!');  
            }
            if(!Ivf::isMobile($params['u_mobile']))
                $this->ajaxReturn(4002, '请输入有效的11位手机号码!');   

            if(empty($params['u_pwd'])) 
                    $this->ajaxReturn(4003, '密码不能为空！');  

            if(empty($params['u_code'])){
                $this->ajaxReturn(4005, '验证码不能为空!');  
            }
            App::uses('ImageCaptcha','Lib');
            $ImageCaptcha = new ImageCaptcha();
            $captcha=$ImageCaptcha->check_captcha_code($params['u_code']);
            if($captcha['status']=="fail")
                $this->ajaxReturn(4006, $captcha['msg']);  

            $this->loadModel('User'); 
            $dataList = $this->User->findByMobile($params['u_mobile']);  
            if(empty($dataList))
                $this->ajaxReturn(4007, '手机号未注册！'); 
            $userInfo=$dataList['User'];
            if(md5($params['u_pwd'].$userInfo['auth'])!=$userInfo['password']) 
                $this->ajaxReturn(4008, '密码有误！');
            $ImageCaptcha->captcha_expired();
            unset($userInfo['password'],$userInfo['auth']);
            $this->setUser($userInfo);  
            $this->ajaxReturn(200, '登陆成功！');   
        }
            $this->ajaxReturn(200, '登陆成功！');    
        exit();
    }
    // 帐号管理
    function index(){    
        $this->checkLogin(2); 
        $this->loadModel('User'); 
        $params = $this->request->data;
        $userInfo=$this->user;
        if($this->request->is('post') && $params['dosubmit']==1){ 
            if(empty($params['u_nick'])) 
                $this->ajaxReturn(4002, '昵称不能为空！');  
            $userInfo['nick_name']= $params['u_nick'];    
            $userInfo['user_name']= $params['u_nick'];    
            $userInfo['sex']= $params['sex'];    
            $userInfo['age']= $params['age'];    
            if(!$this->User->save($userInfo))  
                $this->ajaxReturn(4003, '提交异常!');   
            unset($userInfo['password'],$userInfo['auth']);
            $this->setUser($userInfo);  
            $this->ajaxReturn(200, '修改成功!');  
        }
        $this->set('sex',Configure::read('USER_SEX')); 
    }

    // 密码修改
    function password(){   
         $this->checkLogin(2);  
    }
    // 我的分享
    function my_shares(){ 
        $this->checkLogin(2);  
        $this->loadModel('Share');   
        $this->loadModel('Picture');   
        // 发布分享  
        $params = $this->request->data;
        $picTopic=Configure::read('PIC_TOPIC');
        if($this->request->is('post') && $params['dosubmit']==1){ 
            if(empty($params['title'])){
                $this->ajaxReturn(4001, '标题不能为空!');  
            } 

            if(empty($params['description'])){
                $this->ajaxReturn(4002, '内容不能为空!');  
            }  
           
            if(empty($params['u_code'])){
                $this->ajaxReturn(4005, '验证码不能为空!');  
            }
            
            App::uses('ImageCaptcha','Lib');
            $ImageCaptcha = new ImageCaptcha();
            $captcha=$ImageCaptcha->check_captcha_code($params['u_code']);
            if($captcha['status']=="fail")
                $this->ajaxReturn(4006, $captcha['msg']);   

            $db_data = array();
            $db_data['title'] = $params['title'];
            $db_data['type'] = 0;
            $db_data['hospital_id'] = $params['hospital_id']; 
            $db_data['description'] = $params['description']; 
            $db_data['content'] = $params['description']; 
            $db_data['sort'] = 110; 
            $db_data['uid'] = $this->user['id']; 
            $db_data['tag'] = isset($params['tag'])?$params['tag']:0;  
            $db_data['tag2'] = 0;  
            $db_data['status'] = 1; 
            $db_data['create_time'] = date("Y-m-d H:i:s",time());  

            if(!$this->Share->save($db_data)) 
                    $this->ajaxReturn(4008, '发布失败！');  
            if(!empty($params['UptPicture'])){ 
                foreach ($params['UptPicture'] as $val){  
                    $data=[]; 
                    $data['id'] = $val;  
                    $data['obj_type'] = $picTopic['分享'];  
                    $data['obj_id'] = $this->Share->id;  
                    $this->Picture->save($data);
                }  
            }

            $ImageCaptcha->captcha_expired();
            $this->ajaxReturn(200, '发布成功！');   
        }  
        $orderby['create_time']="desc";
        $conditions['status'] = 1;
        $conditions['uid']=$this->user['id'];
        $conditions['type']=0;
        $dataList = array(); 
        $dataList = $this->Share->find('all',array(
            'conditions' => $conditions, 
            'limit' => 20,
            'order' => $orderby,
            )
        ); 
        $picTopic=Configure::read('PIC_TOPIC');
        if(!empty($dataList)){
            foreach ($dataList as $key => $value) {  
                $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['分享'],'obj_id'=>intval($value['Share']['id'])),'fields'=>array('thumb')));   
                $dataList[$key]['Share']['images']=$picData;     
                $dataList[$key]['Share']['content']=Ivf::getSubstr($value['Share']['content'],0,100);   
            }
        } 
        $this->loadModel('Hospital');   
        $hospitals = $this->Hospital->find('all',array(
            'conditions'=>array('status'=>1),
            'fields'=>array('id','cn_name'),
            )
        );   
        $this->set('hospitalList',$hospitals);
        $this->set('dataList',$dataList);
        $this->set('share_tags',Configure::read('SHARE_TAG'));
        $this->set('share_tags2',Configure::read('SHARE_TAG_TWO'));
    }
    // 我的预约
    function my_reservations(){
        // 我的预约
        $this->checkLogin(2); 
        $this->loadModel('UsersAppoint'); 
        $this->loadModel('Hospital'); 
        $this->loadModel('Doctor');  
        $userInfo=$this->user;
        $dataList = $this->UsersAppoint->find('all',array('conditions'=>array('mobile'=>$userInfo['mobile'],'source like'=>'ivf52-%'))); 
        if(!empty($dataList)){
            foreach ($dataList as $key => $value) { 
                if($value['UsersAppoint']['hospital_id']>0){ 
                    $dataInfo = $this->Hospital->findById($value['UsersAppoint']['hospital_id']);  
                    $dataList[$key]['UsersAppoint']['hospital']= isset($dataInfo['Hospital']['cn_name'])?$dataInfo['Hospital']['cn_name']:"无";
                    $dataList[$key]['UsersAppoint']['hospital_image']= isset($dataInfo['Hospital']['image'])?$dataInfo['Hospital']['image']:"";
                    $dataList[$key]['UsersAppoint']['introduction']= isset($dataInfo['Hospital']['introduction'])?$dataInfo['Hospital']['introduction']:"";
                }
                if($value['UsersAppoint']['doctor_id']>0){ 
                    $dataInfo = $this->Doctor->findById($value['UsersAppoint']['doctor_id']);  
                    $dataList[$key]['UsersAppoint']['doctor']= isset($dataInfo['Doctor']['cn_name'])?$dataInfo['Doctor']['cn_name']:"无";
                    $dataList[$key]['UsersAppoint']['doctor_image']= isset($dataInfo['Doctor']['avatar'])?$dataInfo['Doctor']['avatar']:"无";
                    $dataList[$key]['UsersAppoint']['introduction']= isset($dataInfo['Doctor']['introduction'])?$dataInfo['Doctor']['introduction']:"";
                }
            }
        }
        $this->set('dataList',$dataList); 
    }
    // 我的问答
    function my_answers (){ 
        $this->checkLogin(2);  
        $this->loadModel('Share');   
        $this->loadModel('Picture');  
        $this->loadModel('District');   
 
        // 发布问答  
        $params = $this->request->data;
        $picTopic=Configure::read('PIC_TOPIC');
        if($this->request->is('post') && $params['dosubmit']==1){  

            if(empty($params['description'])){
                $this->ajaxReturn(4002, '内容不能为空!');  
            }  
           
            if(empty($params['u_code'])){
                $this->ajaxReturn(4005, '验证码不能为空!');  
            }
            
            App::uses('ImageCaptcha','Lib');
            $ImageCaptcha = new ImageCaptcha();
            $captcha=$ImageCaptcha->check_captcha_code($params['u_code']);
            if($captcha['status']=="fail")
                $this->ajaxReturn(4006, $captcha['msg']);   

            $db_data = array(); 
            $db_data['type'] = 1;
            $db_data['city_id'] = $params['city_id']; 
            $db_data['description'] = $params['description']; 
            $db_data['content'] = $params['description']; 
            $db_data['uid'] = $this->user['id']; 
            $db_data['sort'] = 110; 
            $db_data['tag'] = isset($params['tag'])?$params['tag']:0;  
            $db_data['tag2'] = 0;  
            $db_data['status'] = 1; 
            $db_data['create_time'] = date("Y-m-d H:i:s",time());  

            if(!$this->Share->save($db_data)) 
                    $this->ajaxReturn(4008, '发布失败！');  
            if(!empty($params['UptPicture'])){ 
                foreach ($params['UptPicture'] as $val){  
                    $data=[]; 
                    $data['id'] = $val;  
                    $data['obj_type'] = $picTopic['问答'];  
                    $data['obj_id'] = $this->Share->id;  
                    $this->Picture->save($data);
                }  
            }

            $ImageCaptcha->captcha_expired();
            $this->ajaxReturn(200, '发布成功！');   
        }
        $orderby['create_time']="desc";
        $conditions['status'] = 1;
        $conditions['type']=1;
        $conditions['uid']=$this->user['id'];
        $dataList = array(); 
        $dataList = $this->Share->find('all',array(
            'conditions' => $conditions, 
            'limit' => 20,
            'order' => $orderby,
            )
        ); 
        $picTopic=Configure::read('PIC_TOPIC');
        if(!empty($dataList)){
            foreach ($dataList as $key => $value) {   
                $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['问答'],'obj_id'=>intval($value['Share']['id'])),'fields'=>array('thumb')));   
                $dataList[$key]['Share']['images']=$picData;    
                $dataList[$key]['Share']['content']=Ivf::getSubstr($value['Share']['content'],0,100);   
            }
        } 
        $districtList = $this->District->find('all',array('order'=>array('sort'=>'desc','id'=>'asc'),'fields'=>array('id','country'))); 
        $this->set('districtList',$districtList); 
        $this->set('dataList',$dataList);
        $this->set('share_tags',Configure::read('SHARE_TAG'));
    }
    
   
    //验证码图片
    function captcha(){
        $checkcode = $this->make_rand(4);
        $text = strtolower($checkcode);
        $data = array('text'=>$text, 'time'=>time());
        App::uses('ImageCaptcha','Lib');
        $ImageCaptcha = new ImageCaptcha();
        if($ImageCaptcha->init_captcha_data($data)){
            $ImageCaptcha->create_verify_code($text);
            exit();
        }
    }   
    function my_plan (){ 
        $this->checkLogin(2);  
        $this->loadModel('ArticleCate'); 
        $this->loadModel('Article');   
        $this->loadModel('Picture');  
        $this->loadModel('Hospital');   
        // 发布攻略   
        $params = $this->request->data;
        $picTopic=Configure::read('PIC_TOPIC');
        if($this->request->is('post') && $params['dosubmit']==1){ 
            if(empty($params['title'])){
                $this->ajaxReturn(4001, '标题不能为空!');  
            } 

            if(empty($params['description'])){
                $this->ajaxReturn(4002, '内容不能为空!');  
            }  
           
            if(empty($params['u_code'])){
                $this->ajaxReturn(4005, '验证码不能为空!');  
            }
            
            App::uses('ImageCaptcha','Lib');
            $ImageCaptcha = new ImageCaptcha();
            $captcha=$ImageCaptcha->check_captcha_code($params['u_code']);
            if($captcha['status']=="fail")
                $this->ajaxReturn(4006, $captcha['msg']);   

            $db_data = array();
            $db_data['title'] = $params['title'];
            $db_data['hospital_id'] = $params['hospital_id']; 
            $db_data['description'] = $params['description']; 
            $db_data['content'] = $params['description']; 
            $db_data['uid'] = $this->user['id']; 
            $db_data['sort'] = 110; 
            $db_data['category_id'] = isset($params['category_id'])?$params['category_id']:0;  
            $db_data['status'] = 1; 
            $db_data['create_time'] = date("Y-m-d H:i:s",time()); 
            $db_data['update_time'] = date("Y-m-d H:i:s",time()); 
            $db_data['meta_title'] = "";  
            $db_data['meta_keywords'] = "";  
            $db_data['meta_description'] = "";  
  
            if(!$this->Article->save($db_data)) 
                    $this->ajaxReturn(4008, '发布失败！');  
            if(!empty($params['UptPicture'])){ 
                foreach ($params['UptPicture'] as $val){  
                    $data=[]; 
                    $data['id'] = $val;  
                    $data['obj_type'] = $picTopic['攻略'];  
                    $data['obj_id'] = $this->Article->id;  
                    $this->Picture->save($data);
                }  
            }

            $ImageCaptcha->captcha_expired();
            $this->ajaxReturn(200, '发布成功！');   
        }  

        $orderby['create_time']="desc";
        $conditions['status'] = 1;
        $conditions['uid']=$this->user['id'];
        $dataList = array(); 
        $dataList = $this->Article->find('all',array(
            'conditions' => $conditions, 
            'limit' => 20,
            'order' => $orderby,
            )
        ); 
        $picTopic=Configure::read('PIC_TOPIC'); 
        if(!empty($dataList)){
            foreach ($dataList as $key => $value) {
                $dataList[$key]['Article']['cate_name']='无';
                if($value['Article']['category_id']>0){ 
                    $cates = $this->ArticleCate->findById($value['Article']['category_id']);  
                    $dataList[$key]['Article']['cate_name']= isset($cates['ArticleCate']['name'])?$cates['ArticleCate']['name']:"无";
                }
                $dataList[$key]['Article']['hospital']="无";
                if(intval($value['Article']['hospital_id'])>0){ 
                    $info = $this->Hospital->findById(intval($value['Article']['hospital_id']));
                    if(isset($info['Hospital']['id']))
                        $dataList[$key]['Article']['hospital']=$info['Hospital']['cn_name'];
                } 
                $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['攻略'],'obj_id'=>intval($value['Article']['id'])),'fields'=>array('thumb')));   
                $dataList[$key]['Article']['images']=$picData; 
            }
        }
        $cates = $this->ArticleCate->find('all',array('conditions'=>array('type'=>0))); 
        $newCates=[];
        if(!empty($cates)){
            foreach ($cates as $key => $value) {
                $newCates[]=$value['ArticleCate'];
            }
        }
        $this->loadModel('Hospital');   
        $hospitals = $this->Hospital->find('all',array(
            'conditions'=>array('status'=>1),
            'fields'=>array('id','cn_name'),
            )
        );  
        $cateList=empty($newCates)?array():Ivf::Totree($newCates);    
        $this->set('hospitalList',$hospitals);
        $this->set('cateList',$cateList);
        $this->set('dataList',$dataList);
    }
    function login_out(){
        $this->delUser();
        $this->redirect('/community');
    }
}