<?php

App ::uses('RedisServer', 'Lib');

class AjaxController extends AppController
{
    public $_redis;

    function beforeFilter()
    {
        parent ::beforeFilter();
        $this -> _redis = new RedisServer();
    }

    //未登录操作
    function other()
    {
        $params = $this -> request -> query;
        $act = $params['ajaxdata'];
        if (empty($act))
            $this -> ajaxReturn(4001, '参数异常！');
        $data = [];
        switch ($act) {
            case 'send_code':
                //发送验证码
                $this -> loadModel('User');
                $mobile = $params['m'];

                if (empty($mobile))
                    $this -> ajaxReturn(4001, '手机号不能为空！');

                if (!Ivf ::isMobile($mobile))
                    $this -> ajaxReturn(4002, '请输入有效的11位手机号码!');

                $dataList = $this -> User -> findByMobile($mobile);
                if (!empty($dataList))
                    $this -> ajaxReturn(4003, '手机号已存在！');

                $issetMobile = $this -> _redis -> exists($mobile);
                if (!empty($issetMobile)) {
                    $this -> ajaxReturn(4005, '操作过于频繁，请于60秒后点击重新发送！');
                }
                $sendMbLimit = RedisServer::REDIS_KEY_MOBILE_SEND_LIMIT . $mobile;
                $mobile_count = $this -> _redis -> get($sendMbLimit);
                if (is_numeric($mobile_count) && $mobile_count >= 5)
                    $this -> ajaxReturn(4004, '当前号码当日验证码短息已达到5条上限！');

                $expire_time = strtotime(date('Y-m-d', strtotime("+1 day"))) - time(); //当日剩余时间
                $this -> _redis -> set($sendMbLimit, intval($mobile_count) + 1, $expire_time);

                $this -> loadModel('MobileMessage');
                $code = $this -> MobileMessage -> create_code();
                $msg = sprintf(MobileMessage ::$code_msg, $code);
                if ($this -> MobileMessage -> sendSigleMsg(0, $mobile, $msg)) {
                    $this -> _redis -> set($mobile, 1, 60); //同一个手机，验证码间隔60秒
                    $this -> Session -> write('mobile_code', array('mobile' => $mobile, 'code' => $code));
                    $this -> ajaxReturn(200, '发送成功！');
                } else
                    $this -> ajaxReturn(4006, '发送失败：服务器忙，请稍后再试');
                break;
            case 'send_aide_code':
                //发送验证码
                $this -> loadModel('User');
                $mobile = $params['m'];

                if (empty($mobile))
                    $this -> ajaxReturn(4001, '手机号不能为空！');

                if (!Ivf ::isMobile($mobile))
                    $this -> ajaxReturn(4002, '请输入有效的11位手机号码!');

                $dataList = $this -> User -> findByMobile($mobile);
                if (!empty($dataList))
                    $this -> ajaxReturn(4003, '您已测试！');

                $issetMobile = $this -> _redis -> exists($mobile);
                if (!empty($issetMobile)) {
                    $this -> ajaxReturn(4005, '操作过于频繁，请于60秒后点击重新发送！');
                }
                $sendMbLimit = RedisServer::REDIS_KEY_MOBILE_SEND_LIMIT . $mobile;
                $mobile_count = $this -> _redis -> get($sendMbLimit);
                if (is_numeric($mobile_count) && $mobile_count >= 5)
                    $this -> ajaxReturn(4004, '当前号码当日验证码短息已达到5条上限！');

                $expire_time = strtotime(date('Y-m-d', strtotime("+1 day"))) - time(); //当日剩余时间
                $this -> _redis -> set($sendMbLimit, intval($mobile_count) + 1, $expire_time);

                $this -> loadModel('MobileMessage');
                $code = $this -> MobileMessage -> create_code();
                $msg = sprintf(MobileMessage ::$code_msg, $code);
                if ($this -> MobileMessage -> sendSigleMsg(0, $mobile, $msg)) {
                    $this -> _redis -> set($mobile, 1, 60); //同一个手机，验证码间隔60秒
                    $this -> Session -> write('mobile_code', array('mobile' => $mobile, 'code' => $code));
                    $this -> ajaxReturn(200, '发送成功！');
                } else
                    $this -> ajaxReturn(4006, '发送失败：服务器忙，请稍后再试');
                break;
            case 'findpwd_send_code':
                //发送验证码
                $this -> loadModel('User');
                $mobile = $params['m'];

                if (empty($mobile))
                    $this -> ajaxReturn(4001, '手机号不能为空！');

                if (!Ivf ::isMobile($mobile))
                    $this -> ajaxReturn(4002, '请输入有效的11位手机号码!');

                $dataList = $this -> User -> findByMobile($mobile);
                if (empty($dataList))
                    $this -> ajaxReturn(4003, '手机号不存在！');

                $issetMobile = $this -> _redis -> exists($mobile);
                if (!empty($issetMobile)) {
                    $this -> ajaxReturn(4005, '操作过于频繁，请于60秒后点击重新发送！');
                }
                $set_expire = false;
                $sendMbLimit = RedisServer::REDIS_KEY_MOBILE_SEND_LIMIT . $mobile;
                if (!$this -> _redis -> exists($sendMbLimit))
                    $set_expire = true;
                $mobile_count = $this -> _redis -> get($sendMbLimit);
                if (is_numeric($mobile_count) && $mobile_count >= 5)
                    $this -> ajaxReturn(4004, '当前号码当日验证码短息已达到5条上限！');
                $expire_time = null;
                if ($set_expire)
                    $expire_time = strtotime(date('Y-m-d', strtotime("+1 day"))) - time(); //当日剩余时间

                $this -> _redis -> set($sendMbLimit, intval($mobile_count) + 1, $expire_time);

                $this -> loadModel('MobileMessage');
                $code = $this -> MobileMessage -> create_code();
                $msg = sprintf(MobileMessage ::$code_msg, $code);
                if ($this -> MobileMessage -> sendSigleMsg(0, $mobile, $msg)) {
                    $this -> _redis -> set($mobile, 1, 60); //同一个手机，验证码间隔60秒
                    $this -> Session -> write('mobile_code', array('mobile' => $mobile, 'code' => $code));
                    $this -> ajaxReturn(200, '发送成功！');
                } else
                    $this -> ajaxReturn(4006, '发送失败：服务器忙，请稍后再试');
                break;
            case 'audio_play':
                $this -> loadModel('Audio');
                if (empty($params['aid'])) {
                    $this -> ajaxReturn(4001, '参数异常！');
                }
                $audio = $this -> Audio -> findById(intval($params['aid']));
                if (empty($audio)) {
                    $this -> ajaxReturn(4002, '参数异常！');
                }
                $audio = $audio['Audio'];
                $audio['play_count'] = intval($audio['play_count']) + 1;
                if (!$this -> Audio -> save($audio)) {
                    $this -> ajaxReturn(4003, '系统异常！');
                }
                $this -> ajaxReturn(200, 'sucuss！');
                break;
            case 'send_code_all':
                //发送验证码
                $this -> loadModel('User');
                $mobile = $params['m'];

                if (empty($mobile))
                    $this -> ajaxReturn(4001, '手机号不能为空！');

                if (!Ivf ::isMobile($mobile))
                    $this -> ajaxReturn(4002, '请输入有效的11位手机号码!');

                $issetMobile = $this -> _redis -> exists($mobile);
                if (!empty($issetMobile)) {
                    $this -> ajaxReturn(4005, '操作过于频繁，请于60秒后点击重新发送！');
                }
                $sendMbLimit = RedisServer::REDIS_KEY_MOBILE_SEND_LIMIT . $mobile;
                $mobile_count = $this -> _redis -> get($sendMbLimit);
                if (is_numeric($mobile_count) && $mobile_count >= 5)
                    $this -> ajaxReturn(4004, '当前号码当日验证码短息已达到5条上限！');

                $expire_time = strtotime(date('Y-m-d', strtotime("+1 day"))) - time(); //当日剩余时间
                $this -> _redis -> set($sendMbLimit, intval($mobile_count) + 1, $expire_time);

                $this -> loadModel('MobileMessage');
                $code = $this -> MobileMessage -> create_code();
                $msg = sprintf(MobileMessage ::$code_msg, $code);
                if ($this -> MobileMessage -> sendSigleMsg(0, $mobile, $msg)) {
                    $this -> _redis -> set($mobile, 1, 60); //同一个手机，验证码间隔60秒
                    $this -> Session -> write('mobile_code', array('mobile' => $mobile, 'code' => $code));
                    $this -> ajaxReturn(200, '发送成功！');
                } else
                    $this -> ajaxReturn(4006, '发送失败：服务器忙，请稍后再试');
                break;
            default:
                break;
        }
        die();
    }

    //分页操作
    public function page()
    {
        $params = $this -> request -> query;

        $act = $params['ajaxdata'];

        if (empty($params))
            $this -> ajaxReturn(4001, '参数异常！');
        $dataHtml = "";
        switch ($act) {
            case 'hospital':
                $cid = isset($params['val']) ? intval($params['val']) : 0;
                $page = $params['p'];
                $this -> loadModel('Hospital');
                $this -> loadModel('District');
                $orderby = [];
                if ($page < 2)
                    $this -> ajaxReturn(4001, '参数异常！');
                if (!empty($cid) && $cid > 0) {
                    $conditions['city_id'] = $cid;
                    $datas = $this -> District -> findById($cid);
                    if (empty($datas)) {
                        $this -> ajaxReturn(4002, '非法请求！');
                    }
                }
                $orderby['sort'] = "desc";
                $orderby['create_time'] = "desc";
                $conditions['status'] = 1;
                $dataList = array();
                $dataList = $this -> Hospital -> find('all', array(
                        'conditions' => $conditions,
                        'limit'      => 6,
                        'page'       => $page,
                        'order'      => $orderby,
                    )
                );
                if (!empty($dataList)) {
                    foreach ($dataList as $key => $value) {
                        $dataHtml .= "<li>";
                        $dataHtml .= "<input  type='hidden' id='HN_" . $value["Hospital"]["id"] . "' value='" . $value["Hospital"]["cn_name"] . "'/><input  type='hidden' id='PR_" . $value["Hospital"]["id"] . "' value='" . $value["Hospital"]["price"] . "'/>";
                        $dataHtml .= "<a href='/hospital/info/" . $value["Hospital"]["id"] . ".html' ><div class='list_left'><img alt='" . $value['Hospital']['cn_name'] . "' class='lazy' src='" . $value['Hospital']['image'] . "' data-original='" . $value['Hospital']['image'] . "'></div><div class='list_center'><p class='title'>" . $value['Hospital']['cn_name'] . "<span>（" . $value['Hospital']['en_name'] . "）</span></p><p class='tag'>口碑：<span>" . $value['Hospital']['public_praise'] . "</span>人气：<span>" . $value['Hospital']['popularity'] . "</span>活跃：<span>" . $value['Hospital']['active'] . "</span></p><p class='title_m'>" . $value['Hospital']['cn_name'] . "</p><p class='wenan'>" . $value['Hospital']['introduction'] . "</p><p class='te'>特点：<span>" . $value['Hospital']['projects'] . "</span></p><div class='score'><div class='score_img'>";
                        for ($x = 1; $x <= $value['Hospital']['score']; $x++) {
                            $dataHtml .= "<img src=/images/hospital/quan_1.jpg>";
                        }
                        $dataHtml .= "<div>" . $value['Hospital']['score'] . "/5分&nbsp;&nbsp;还可以</div></div><div class='score_w'><p>已有<span>" . $value['Hospital']['rec_num'] . "</span>位病友推介该医院</p></div></div><div class='score_m'><div>";

                        for ($x = 1; $x <= $value['Hospital']['popularity']; $x++) {
                            $dataHtml .= "<img src=/images/hospital/wujiaoxing_01.jpg>";
                        }
                        $dataHtml .= "</div><span><i>￥</i>" . intval($value['Hospital']['price']) . "起/人</span><div></div></div><p class='wenan_m'>" . $value['Hospital']['introduction'] . "</p></div></a><div class='list_right'><p><span><i>￥</i>" . intval($value['Hospital']['price']) . "</span>起/人</p><a rel='nofollow' class='pc_yu' href='javascript:void(0)' title='' onclick='_user.appoint(" . $value["Hospital"]["id"] . ");_user.showDiv(0);'>我要预约</a><a rel='nofollow' class='ph_yu' href='/mobile/order_hospital/" . $value["Hospital"]["id"] . ".html' title=''>预约</a></div>";
                        $dataHtml .= "</li>";
                    }
                }

                $this -> ajaxReturn(200, $dataHtml);
                break;
            case 'doctor':
                $cid = isset($params['val']) ? intval($params['val']) : 0;
                $page = $params['p'];
                $this -> loadModel('Hospital');
                $this -> loadModel('District');
                $this -> loadModel('Doctor');
                $orderby = [];
                if ($page < 2)
                    $this -> ajaxReturn(4001, '参数异常！');
                if (!empty($cid) && $cid > 0) {
                    $hopitalDate = $this -> Hospital -> find('all', array('conditions' => array('city_id' => $cid, 'status' => 1), 'fields' => array('id')));
                    if (!empty($hopitalDate)) {
                        foreach ($hopitalDate as $key => $value) {
                            $hosAttr[] = $value['Hospital']['id'];
                        }
                        $conditions['id'] = $hosAttr;
                    } else
                        $conditions['id'] = 1;
                }
                $orderby['sort'] = "asc";
                $orderby['rec'] = "desc";
                $orderby['create_time'] = "desc";
                $conditions['status'] = 1;
                $dataList = array();
                $dataList = $this -> Doctor -> find('all', array(
                        'conditions' => $conditions,
                        'limit'      => 6,
                        'page'       => $page,
                        'order'      => $orderby,
                    )
                );
                if (!empty($dataList)) {
                    $education = Configure ::read('EDUCATION');
                    foreach ($dataList as $key => $value) {
                        $value['Doctor']['hospital'] = "无";
                        if (intval($value['Doctor']['hospital_id']) > 0) {
                            $info = $this -> Hospital -> findById(intval($value['Doctor']['hospital_id']));
                            if (isset($info['Hospital']['id']))
                                $value['Doctor']['hospital'] = $info['Hospital']['cn_name'];
                        }

                        $dataHtml .= "<li>";
                        $dataHtml .= "<input  type='hidden' id='HN_" . $value["Doctor"]["id"] . "' value='" . $value["Doctor"]["cn_name"] . "'/>";
                        $dataHtml .= "<a  href='/doctor/info/" . $value["Doctor"]["id"] . ".html' ><div class='list_left'><img alt='" . $value['Doctor']['cn_name'] . "' class='lazy' src='" . $value['Doctor']['avatar'] . "' data-original='" . $value['Doctor']['avatar'] . "'></div><div class='list_center'><p class='title'>" . $value['Doctor']['cn_name'] . "<span>(" . $value['Doctor']['en_name'] . ")</span></p><div class='wenan'><p>所属医院：<span>" . $value['Doctor']['hospital'] . "</span></p><p>医生职务：<span>" . $value["Doctor"]["duty"] . "<span>&nbsp;从医年限：<span>" . $value["Doctor"]["job_duration"] . "</span>&nbsp;最高学历：</span>" . $education[$value["Doctor"]["highest_education"]] . "</span></p><p>擅长项目：<i>" . $value["Doctor"]["projects"] . "</i></p><p>已有<em>" . $value["Doctor"]["appoint_num"] . "</em>位病友预约该医生</p></div><div class='score_m'><div>";
                        for ($x = 1; $x <= $value['Doctor']['popularity']; $x++) {
                            $dataHtml .= "<img src=/images/hospital/wujiaoxing_01.jpg>";
                        }
                        $dataHtml .= "</div><span>" . $value['Doctor']['popularity'] . "分</span></div><p class='wenan_m'>" . $value["Doctor"]["introduction"] . "</p></div></a><div class='list_right'><p></p><a rel='nofollow' class='pc_yu' href='javascript:void(0)'  onclick='_user.doctor_appoint(" . $value["Doctor"]["id"] . ");_user.showDiv(1);'>我要远程问诊</a><a rel='nofollow' class='ph_yu' href='href='/mobile/order_doctor/" . $value["Doctor"]["id"] . ".html''>预约</a></div>";
                        $dataHtml .= "</li>";
                    }
                }
                $this -> ajaxReturn(200, $dataHtml);
                break;
            case 'news':
                $cid = isset($params['val']) ? intval($params['val']) : 0;
                $page = $params['p'];
                $this -> loadModel('New');
                $orderby = [];
                if ($page < 2)
                    $this -> ajaxReturn(4001, '参数异常！');
                $pageSize = 5;
                $orderby['sort'] = "desc";
                $orderby['create_time'] = "desc";
                $conditions['status'] = 1;
                $conditions['category_id'] = intval($cid);
                $dataList = array();
                $dataList = $this -> New -> find('all', array(
                    'conditions' => $conditions,
                    'limit'      => $pageSize,
                    'page'       => $page,
                    'order'      => $orderby,
                )
            );
                if (!empty($dataList)) {
                    foreach ($dataList as $key => $value) {
                        $dataHtml .= "<li>";
                        $dataHtml .= "<a  href='/news/info/" . $value["New"]["id"] . ".html' ><img src='" . $value['New']['image'] . "' alt=''><div class='right-box'><h5>" . $value['New']['title'] . "</h5><div class='text'>" . $value['New']['description'] . "</div><p>" . date("Y-m-d", strtotime($value['New']['create_time'])) . "</p> </div> </a>";
                        $dataHtml .= "</li>";
                    }
                }
                $this -> ajaxReturn(200, $dataHtml);
                break;
            case 'joins':
                $cid = isset($params['val']) ? intval($params['val']) : 0;
                $page = $params['p'];
                $this -> loadModel('Join');
                $orderby = [];
                if ($page < 2)
                    $this -> ajaxReturn(4001, '参数异常！');
                $pageSize = 5;
                $orderby['sort'] = "desc";
                $orderby['create_time'] = "desc";
                $dataList = [];
                $dataList = $this -> Join -> find('all', array(
                        'limit' => $pageSize,
                        'page'  => $page,
                        'order' => $orderby,
                    )
                );
                $education = Configure ::read('EDUCATION');
                $experience = Configure ::read('Experience');
                if (!empty($dataList)) {
                    foreach ($dataList as $key => $value) {
                        $dataHtml .= "<li>";
                        $dataHtml .= "<div class='li_left'>" . $value['Join']['title'] . "<br><span>" . Ivf ::formatTime($value['Join']['create_time']) . "</span></div><div class='li_cen'>";
                        $salary = "面议";
                        if (!empty($value['Join']['SalaryA']) && !empty($value['Join']['SalaryB']))
                            $salary = $value['Join']['SalaryA'] . "-" . $value['Join']['SalaryB'];
                        elseif (!empty($value['Join']['SalaryA']))
                            $salary = $value['Join']['SalaryA'];
                        elseif (!empty($value['Join']['SalaryB']))
                            $salary = $value['Join']['SalaryB'];
                        $dataHtml .= $salary . "<br><span>" . $education[$value['Join']['Education']] . "/" . $experience[$value['Join']['Experience']] . "</span></div><div class='li_right'><a href='/join/info/" . $value["Join"]["id"] . ".html' >查看职位</a></div>";
                        $dataHtml .= "</li>";
                    }
                }
                $this -> ajaxReturn(200, $dataHtml);
                break;
            case 'plan':
                $cid = isset($params['val']) ? intval($params['val']) : 0;
                $ctype = isset($params['t']) ? intval($params['t']) : '';
                $keyWord = isset($params['k']) ? $params['k'] : '';
                $page = $params['p'];
                $this -> loadModel('Article');
                $this -> loadModel('ArticleCate');
                $this -> loadModel('Hospital');
                $this -> loadModel('User');
                $this -> loadModel('Picture');
                $orderby = [];
                if ($page < 2)
                    $this -> ajaxReturn(4001, '参数异常！');
                if (!empty($cid) && $cid > 0) {
                    $conditions['category_id'] = intval($cid);
                    $cates = $this -> ArticleCate -> find('first', array('conditions' => array('status' => 1, 'id' => $cid, 'type' => 0)));
                    if (empty($cates))
                        $this -> ajaxReturn(4001, '参数异常！');
                }
                if (!empty($ctype) && $ctype == 'elite')
                    $conditions['is_elite'] = '1';
                else {
                    if (!empty($ctype) && $ctype == 'hot')
                        $orderby['comment_num'] = "desc";
                }

                if (!empty($keyWord)) {
                    $conditions["OR"]['title like'] = '%' . $keyWord . '%';
                    $conditions["OR"]['content like'] = '%' . $keyWord . '%';
                }

                $orderby['is_top'] = "desc";
                $orderby['sort'] = "desc";
                $orderby['create_time'] = "desc";
                $conditions['uid >'] = 0;
                $conditions['status'] = 1;
                $dataList = array();
                $dataList = $this -> Article -> find('all', array(
                        'conditions' => $conditions,
                        'limit'      => 10,
                        'page'       => $page,
                        'order'      => $orderby,
                    )
                );
                $picTopic = Configure ::read('PIC_TOPIC');
                $this -> loadModel('Agree');
                $agreeTopic = Configure ::read('AGREE_TOPIC');
                $userInfo = $this -> getUser();
                $userId = intval($userInfo['id']);
                if (!empty($dataList)) {
                    foreach ($dataList as $key => $value) {
                        $value['Article']['hospital'] = "无";
                        if (intval($value['Article']['hospital_id']) > 0) {
                            $info = $this -> Hospital -> findById(intval($value['Article']['hospital_id']));
                            if (isset($info['Hospital']['id']))
                                $value['Article']['hospital'] = $info['Hospital']['cn_name'];
                        }
                        // $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['攻略'],'obj_id'=>intval($value['Article']['id'])),'fields'=>array('thumb')));
                        preg_match_all('/src="(.+?)"/', $value['Article']['content'], $matches);//带引号
                        $picData = array_unique($matches[0]);//去除数组中重复的值

                        $userInfo = $this -> User -> findById(intval($value['Article']['uid']));
                        $agCount = $this -> Agree -> find('count', array('conditions' => array('uid' => $userId, 'obj_id' => $value['Article']['id'], 'obj_type' => $agreeTopic['攻略'])));
                        $value['Article']['myagree'] = $agCount;
                        $value['Article']['images'] = $picData;
                        $value['Article']['username'] = isset($userInfo['User']['nick_name']) ? $userInfo['User']['nick_name'] : "保密";
                        $value['Article']['avatar'] = isset($userInfo['User']['avatar']) ? $userInfo['User']['avatar'] : "";

                        $dataHtml .= "<li><div class='list_li'><div class='touxiang'><img src=" . $value['Article']['avatar'] . " alt=" . $value['Article']['username'] . "></div><div class='list_b_right'><a href='/plan/info/" . $value["Article"]["id"] . ".html'>";
                        $dataHtml .= "<div class='title_top'>" . Ivf ::trimall(Ivf ::getLittleText($value['Article']['description'], Ivf ::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT'])))) . "</div>";
                        // $dataHtml.="<div class='title_top'>".Ivf::trimall($value['Article']['description'])."</div>";
                        $dataHtml .= "</a><div class='bottam'><div class='b_name'>" . $value['Article']['username'] . "</div><div class='b_right'><div class='b_pic1'>" . $value['Article']['browse_num'] . "</div><div class='b_pic2'>" . $value['Article']['comment_num'] . "</div>";
                        $dataHtml .= "<div class='b_right_mm'><div><img src='/images/hospital/y_1.png' />&nbsp;&nbsp;" . $value['Article']['browse_num'] . "</div><div><img src='/images/hospital/y_2.png' />&nbsp;&nbsp;" . $value['Article']['comment_num'] . "</div><div><img src='/images/hospital/y_3.png' />&nbsp;&nbsp;" . $value['Article']['agree_num'] . "</div></div>";
                        if ($value['Article']['myagree'] == 0)
                            $dataHtml .= "<div class='b_pic3 up' onclick='adAgree(this," . $value["Article"]["id"] . ",\"agree_article\")'>";
                        else
                            $dataHtml .= "<div class='b_pic3 up add_up'>";
                        $dataHtml .= $value['Article']['agree_num'] . "</div></div></div></div></div><div class='img_b'>";
                        foreach ($value['Article']['images'] as $k => $v) {
                            if ($k <= 8) {
                                $dataHtml .= "<img " . $v . " class='demo'/>";
                            }
                        }

                        $dataHtml .= "</div></li> ";
                    }
                }
                $this -> ajaxReturn(200, $dataHtml);
                break;
            case 'share':
                $cid = isset($params['val']) ? intval($params['val']) : 0;
                $ctype = isset($params['t']) ? intval($params['t']) : '';
                $keyWord = isset($params['k']) ? $params['k'] : '';
                $page = $params['p'];
                $this -> loadModel('Share');
                $this -> loadModel('Hospital');
                $this -> loadModel('User');
                $this -> loadModel('Picture');

                $orderby = [];
                if (!empty($cid) && $cid > 0)
                    $conditions['tag2'] = intval($cid);

                if (!empty($ctype) && $ctype > 0)
                    $conditions['tag'] = intval($ctype);
                if (!empty($keyWord)) {
                    $conditions["OR"]['title like'] = '%' . $keyWord . '%';
                    $conditions["OR"]['content like'] = '%' . $keyWord . '%';
                }

                $orderby['sort'] = "desc";
                $orderby['create_time'] = "desc";
                $conditions['status'] = 1;
                $conditions['type'] = 0;
                $dataList = array();
                $dataList = $this -> Share -> find('all', array(
                        'conditions' => $conditions,
                        'limit'      => 10,
                        'page'       => $page,
                        'order'      => $orderby,
                    )
                );

                $picTopic = Configure ::read('PIC_TOPIC');
                $this -> loadModel('Agree');
                $agreeTopic = Configure ::read('AGREE_TOPIC');
                $userInfo = $this -> getUser();
                $userId = intval($userInfo['id']);
                if (!empty($dataList)) {
                    foreach ($dataList as $key => $value) {
                        $value['Share']['hospital'] = "无";
                        if (intval($value['Share']['hospital_id']) > 0) {
                            $info = $this -> Hospital -> findById(intval($value['Share']['hospital_id']));
                            if (isset($info['Hospital']['id']))
                                $value['Share']['hospital'] = $info['Hospital']['cn_name'];
                        }
                        $agCount = $this -> Agree -> find('count', array('conditions' => array('uid' => $userId, 'obj_id' => $value['Share']['id'], 'obj_type' => $agreeTopic['分享'])));
                        $value['Share']['myagree'] = $agCount;
                        // $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['分享'],'obj_id'=>intval($value['Share']['id'])),'fields'=>array('thumb')));

                        preg_match_all('/src="(.+?)"/', $value['Share']['content'], $matches);//带引号
                        $picData = array_unique($matches[0]);//去除数组中重复的值
                        $value['Share']['images'] = $picData;
                        $userInfo = $this -> User -> findById(intval($value['Share']['uid']));
                        $value['Share']['username'] = isset($userInfo['User']['nick_name']) ? $userInfo['User']['nick_name'] : "保密";
                        if (empty($value['Share']['description']) && !empty($value['Share']['content'])) {
                            $value['Share']['description'] = Ivf ::getSubstr(strip_tags($value['Share']['content']), 0, 200);
                        }
                        $value['Share']['description'] = Ivf ::trimall($value['Share']['description']);
                        $value['Share']['avatar'] = isset($userInfo['User']['avatar']) ? $userInfo['User']['avatar'] : '';

                        $dataHtml .= "<li><div class='list_li'><div class='touxiang'><img src=" . $value['Share']['avatar'] . " alt=" . $value['Share']['username'] . "></div><div class='list_b_right'><a href='/share/info/" . $value["Share"]["id"] . ".html'>";
                        $dataHtml .= "<div class='title_top'>" . Ivf ::trimall(Ivf ::getLittleText($value['Share']['description'], Ivf ::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT'])))) . "</div>";
                        $dataHtml .= "</a><div class='bottam'><div class='b_name'>" . $value['Share']['username'] . "</div><div class='b_right'><div class='b_pic1'>" . $value['Share']['browse_num'] . "</div><div class='b_pic2'>" . $value['Share']['comment_num'] . "</div>";
                        $dataHtml .= "<div class='b_right_mm'><div><img src='/images/hospital/y_1.png' />&nbsp;&nbsp;" . $value['Share']['browse_num'] . "</div><div><img src='/images/hospital/y_2.png' />&nbsp;&nbsp;" . $value['Share']['comment_num'] . "</div><div><img src='/images/hospital/y_3.png' />&nbsp;&nbsp;" . $value['Share']['agree_num'] . "</div></div>";
                        if ($value['Share']['myagree'] == 0)
                            $dataHtml .= "<div class='b_pic3 up' onclick='adAgree(this," . $value["Share"]["id"] . ",\"agree_share\")'>";
                        else
                            $dataHtml .= "<div class='b_pic3 up add_up'>";
                        $dataHtml .= $value['Share']['agree_num'] . "</div></div></div></div></div><div class='img_b'>";
                        foreach ($value['Share']['images'] as $k => $v) {
                            if ($k <= 8) {
                                $dataHtml .= "<img " . $v . " class='demo'/>";
                            }
                        }
                        $dataHtml .= "</div></li>";
                    }
                }

                $this -> ajaxReturn(200, $dataHtml);
                break;
            case 'answer':
                $cid = isset($params['val']) ? intval($params['val']) : 0;
                $keyWord = isset($params['k']) ? $params['k'] : '';
                $page = $params['p'];
                $this -> loadModel('Share');
                $this -> loadModel('User');
                $this -> loadModel('Picture');

                $orderby = [];
                if (!empty($cid) && $cid == 1) {
                    $orderby['create_time'] = "desc";
                } else if (!empty($cid) && $cid == 2) {
                    $conditions['is_replay'] = 0;
                } else {
                    $orderby['comment_num'] = "desc";
                }

                if (!empty($keyWord)) {
                    $conditions["OR"]['title like'] = '%' . $keyWord . '%';
                    $conditions["OR"]['content like'] = '%' . $keyWord . '%';
                }

                $orderby['sort'] = "desc";
                $conditions['status'] = 1;
                $conditions['is_top'] = 0;
                $conditions['type'] = 1;
                $dataList = array();
                $dataList = $this -> Share -> find('all', array(
                        'conditions' => $conditions,
                        'limit'      => 10,
                        'page'       => $page,
                        'order'      => $orderby,
                    )
                );
                $share_tags = Configure ::read('SHARE_TAG');
                $picTopic = Configure ::read('PIC_TOPIC');
                $this -> loadModel('Agree');
                $agreeTopic = Configure ::read('AGREE_TOPIC');
                $userInfo = $this -> getUser();
                $userId = intval($userInfo['id']);
                if (!empty($dataList)) {
                    foreach ($dataList as $key => $value) {
                        $userInfo = $this -> User -> findById(intval($value['Share']['uid']));
                        $value['Share']['username'] = isset($userInfo['User']['nick_name']) ? $userInfo['User']['nick_name'] : "保密";
                        $value['Share']['content'] = Ivf ::getSubstr($value['Share']['content'], 0, 110);
                        $value['Share']['avatar'] = isset($userInfo['User']['avatar']) ? $userInfo['User']['avatar'] : '';
                        $agCount = $this -> Agree -> find('count', array('conditions' => array('uid' => $userId, 'obj_id' => $value['Share']['id'], 'obj_type' => $agreeTopic['问答'])));
                        $value['Share']['myagree'] = $agCount;
                        // $picData = $this->Picture->find('all',array('conditions'=>array('obj_type'=>$picTopic['问答'],'obj_id'=>intval($value['Share']['id'])),'fields'=>array('thumb')));
                        preg_match_all('/src="(.+?)"/', $value['Share']['content'], $matches);//带引号
                        $picData = array_unique($matches[0]);//去除数组中重复的值
                        $value['Share']['images'] = $picData;
                        $tags = $value['Share']['tag'] == 0 ? "保密" : $share_tags[$value['Share']['tag']];


                        $dataHtml .= "<li><div class='list_li'><div class='touxiang'><img src=" . $value['Share']['avatar'] . " alt=" . $value['Share']['username'] . "></div><div class='list_b_right'><a href='/answer/info/" . $value["Share"]["id"] . ".html'>";
                        $dataHtml .= "<div class='title_top'>" . Ivf ::trimall(Ivf ::getLittleText($value['Share']['content'], Ivf ::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT'])), 60, 20)) . "</div>";
                        $dataHtml .= "</a><div class='bottam'><div class='b_name'>" . $value['Share']['username'] . "</div><div class='b_right'><div class='b_pic1'>" . $value['Share']['browse_num'] . "</div><div class='b_pic2'>" . $value['Share']['comment_num'] . "</div>";
                        $dataHtml .= "<div class='b_right_mm'><div><img src='/images/hospital/y_1.png' />&nbsp;&nbsp;" . $value['Share']['browse_num'] . "</div><div><img src='/images/hospital/y_2.png' />&nbsp;&nbsp;" . $value['Share']['comment_num'] . "</div><div><img src='/images/hospital/y_3.png' />&nbsp;&nbsp;" . $value['Share']['agree_num'] . "</div></div>";
                        if ($value['Share']['myagree'] == 0)
                            $dataHtml .= "<div class='b_pic3 up' onclick='adAgree(this," . $value["Share"]["id"] . ",\"agree_answer\")'>";
                        else
                            $dataHtml .= "<div class='b_pic3 up add_up'>";
                        $dataHtml .= $value['Share']['agree_num'] . "</div></div></div></div></div><div class='img_b'>";
                        // 加载更多图片
                        // foreach ($value['Share']['images'] as $k => $v){
                        //     $dataHtml.="<img src='".$v['Picture']['thumb']."' class='demo'/>";
                        // }
                        foreach ($value['Share']['images'] as $k => $v) {
                            if ($k <= 8) {
                                $dataHtml .= "<img " . $v . " class='demo'/>";
                            }
                        }
                        $dataHtml .= "</div></li> ";
                    }
                }

                $this -> ajaxReturn(200, $dataHtml);
                break;
            case 'hospital_comment':
                $cid = isset($params['val']) ? intval($params['val']) : 0;
                $page = $params['p'];
                $this -> loadModel('Comment');
                $this -> loadModel('User');

                $commentTopic = Configure ::read('COMMENT_TOPIC');
                $dataList = $this -> Comment -> find('all', array('conditions' => array('topic_type' => $commentTopic['医院'], 'parent_id' => 0, 'topic_id' => $cid), 'order' => array('is_top' => 'desc', 'sort' => 'desc', 'comment_time' => 'desc'), 'limit' => 5, 'page' => $page));

                if (!empty($dataList)) {
                    foreach ($dataList as $key => $value) {
                        $userInfo = $this -> User -> findById(intval($value['Comment']['uid']));
                        $value['Comment']['username'] = isset($userInfo['User']['nick_name']) ? $userInfo['User']['nick_name'] : "保密";
                        $value['Comment']['avatar'] = isset($userInfo['User']['avatar']) ? $userInfo['User']['avatar'] : '';

                        $dataHtml .= "<li>";
                        $dataHtml .= "<div class='pinglu'>
                                <div class='head_pic'>
                                    <img src='" . $value['Comment']['avatar'] . "'>
                                </div>
                                <div class='conte'>
                                    <div class='namec'>" . $value['Comment']['username'] . "</div>
                                    <div class='contc'>" . $value['Comment']['content'] . "</div>
                                </div>
                            </div>";
                        $dataHtml .= "</li>";

                    }
                }
                $this -> ajaxReturn(200, $dataHtml);
                break;
            case 'doctor_comment':
                $cid = isset($params['val']) ? intval($params['val']) : 0;
                $page = $params['p'];
                $this -> loadModel('Comment');
                $this -> loadModel('User');

                $commentTopic = Configure ::read('COMMENT_TOPIC');
                $dataList = $this -> Comment -> find('all', array('conditions' => array('topic_type' => $commentTopic['医生'], 'parent_id' => 0, 'topic_id' => $cid), 'order' => array('is_top' => 'desc', 'sort' => 'desc', 'comment_time' => 'desc'), 'limit' => 5, 'page' => $page));

                if (!empty($dataList)) {
                    foreach ($dataList as $key => $value) {
                        $userInfo = $this -> User -> findById(intval($value['Comment']['uid']));
                        $value['Comment']['username'] = isset($userInfo['User']['nick_name']) ? $userInfo['User']['nick_name'] : "保密";
                        $value['Comment']['avatar'] = isset($userInfo['User']['avatar']) ? $userInfo['User']['avatar'] : '';

                        $dataHtml .= "<li>";
                        $dataHtml .= "<div class='pinglu'><div class='head_pic'><img src='" . $value['Comment']['avatar'] . "'></div><div class='conte'><div class='namec'>" . $value['Comment']['username'] . "</div><div class='contc'>" . $value['Comment']['content'] . "</div><div class='timec'>" . $value['Comment']['comment_time'] . "</div></div></div>";
                        $dataHtml .= "</li>";

                    }
                }
                $this -> ajaxReturn(200, $dataHtml);
                break;
            case 'plan_comment':
                $cid = isset($params['val']) ? intval($params['val']) : 0;
                $u = isset($params['u']) ? $params['u'] : '';
                $page = $params['p'];
                $this -> loadModel('Comment');
                $this -> loadModel('User');

                $commentTopic = Configure ::read('COMMENT_TOPIC');
                $dataList = $this -> Comment -> find('all', array('conditions' => array('topic_type' => $commentTopic['攻略'], 'parent_id' => 0, 'topic_id' => $cid), 'order' => array('is_top' => 'desc', 'sort' => 'desc', 'comment_time' => 'desc'), 'limit' => 5, 'page' => $page));

                if (!empty($dataList)) {
                    foreach ($dataList as $key => $value) {
                        $userInfo = $this -> User -> findById(intval($value['Comment']['uid']));
                        $value['Comment']['username'] = isset($userInfo['User']['nick_name']) ? $userInfo['User']['nick_name'] : "保密";
                        $value['Comment']['avatar'] = isset($userInfo['User']['avatar']) ? $userInfo['User']['avatar'] : '';

                        $dataHtml .= "<li>";
                        $dataHtml .= "<div class='pinglu'><div class='head_pic'><img src='" . $value['Comment']['avatar'] . "'></div><div class='conte'><div class='namec'>" . $value['Comment']['username'] . "</div><div class='contc'>" . $value['Comment']['content'] . "</div><div class='timec'>" . Ivf ::formatTime($value['Comment']['comment_time']) . "</div></div></div>";
                        $dataHtml .= "</li>";

                    }
                }

                $this -> ajaxReturn(200, $dataHtml);
                break;
            case 'share_comment':
                $cid = isset($params['val']) ? intval($params['val']) : 0;
                $page = $params['p'];
                $this -> loadModel('Comment');
                $this -> loadModel('User');

                $commentTopic = Configure ::read('COMMENT_TOPIC');
                $dataList = $this -> Comment -> find('all', array('conditions' => array('topic_type' => $commentTopic['分享'], 'parent_id' => 0, 'topic_id' => $cid), 'order' => array('is_top' => 'desc', 'sort' => 'desc', 'comment_time' => 'desc'), 'limit' => 5, 'page' => $page));

                if (!empty($dataList)) {
                    foreach ($dataList as $key => $value) {
                        $userInfo = $this -> User -> findById(intval($value['Comment']['uid']));
                        $childCmt = $this -> Comment -> find('all', array('conditions' => array('topic_type' => $commentTopic['分享'], 'parent_id' => $value['Comment']['id']), 'order' => array('is_top' => 'desc', 'sort' => 'desc', 'comment_time' => 'asc'), 'fields' => array('id', 'comment_time', 'uid', 'reply_id', 'content')));
                        $childAttr = [];
                        if (!empty($childCmt)) {
                            foreach ($childCmt as $kk => $vv) {
                                $uInfo = $this -> User -> findById($vv['Comment']['uid']);
                                if (!empty($uInfo)) {
                                    $vv['Comment']['replayName'] = isset($uInfo['User']['nick_name']) ? $uInfo['User']['nick_name'] : $uInfo['User']['mobile'];
                                    if ($vv['Comment']['reply_id'] > 0 && $vv['Comment']['reply_id'] != $value['Comment']['id']) {
                                        $cInfo = $this -> Comment -> findById($vv['Comment']['reply_id']);
                                        if (empty($cInfo)) continue;
                                        $uInfo = $this -> User -> findById($cInfo['Comment']['uid']);
                                        $vv['Comment']['replayName'] = $vv['Comment']['replayName'] . " <em>回复</em> " . (isset($uInfo['User']['nick_name']) ? $uInfo['User']['nick_name'] : $uInfo['User']['mobile']);

                                    }
                                    $childAttr[] = $vv['Comment'];
                                }
                            }
                        }
                        $value['Comment']['username'] = isset($userInfo['User']['nick_name']) ? $userInfo['User']['nick_name'] : "保密";
                        $value['Comment']['avatar'] = isset($userInfo['User']['avatar']) ? $userInfo['User']['avatar'] : '';

                        $dataHtml .= "<li>";
                        $dataHtml .= "<div class='pinglu'><div class='head_pic'><img src='" . $value['Comment']['avatar'] . "'></div><div class='conte'><div class='namec'>" . $value['Comment']['username'] . "</div><div class='contc'>" . $value['Comment']['content'] . "</div><div class='timec'>" . Ivf ::formatTime($value['Comment']['comment_time']) . "</div><a class='creplay' href='javascript:void(0);' onclick='replayComment(" . $value['Comment']['id'] . ")'>回复</a>";
                        if (!empty($childAttr))
                            $dataHtml .= "<span class='moreHui'>更多回复</span> ";
                        $dataHtml .= "</div><div class='conteBox'>";
                        if (!empty($childAttr)) {
                            foreach ($childAttr as $kk => $vv) {
                                $dataHtml .= "<div class='conte'><div class='namec'>" . $vv['replayName'] . "</div><div class='contc'>" . $vv['content'] . "</div><div class='timec'>" . Ivf ::formatTime($vv['comment_time']) . "</div><a class='creplay' href='javascript:void(0);' onclick='replayComment(" . $vv['id'] . ")'>回复</a></div>";
                            }
                        }
                        $dataHtml .= "</div></div>";
                        $dataHtml .= "</li>";


                    }
                }

                $this -> ajaxReturn(200, $dataHtml);
                break;
            case 'answer_comment':
                $cid = isset($params['val']) ? intval($params['val']) : 0;
                $page = $params['p'];
                $this -> loadModel('Comment');
                $this -> loadModel('User');

                $commentTopic = Configure ::read('COMMENT_TOPIC');
                $dataList = $this -> Comment -> find('all', array('conditions' => array('topic_type' => $commentTopic['问答'], 'parent_id' => 0, 'topic_id' => $cid), 'order' => array('is_top' => 'desc', 'sort' => 'desc', 'comment_time' => 'desc'), 'limit' => 5, 'page' => $page));

                if (!empty($dataList)) {
                    foreach ($dataList as $key => $value) {
                        $userInfo = $this -> User -> findById(intval($value['Comment']['uid']));
                        $value['Comment']['username'] = isset($userInfo['User']['nick_name']) ? $userInfo['User']['nick_name'] : "保密";
                        $value['Comment']['avatar'] = isset($userInfo['User']['avatar']) ? $userInfo['User']['avatar'] : '';

                        $dataHtml .= "<li>";
                        $dataHtml .= "<div class='pinglu'><div class='head_pic'><img src='" . $value['Comment']['avatar'] . "'></div><div class='conte'><div class='namec'>" . $value['Comment']['username'] . "</div><div class='contc'>" . $value['Comment']['content'] . "</div><div class='timec'>" . Ivf ::formatTime($value['Comment']['comment_time']) . "</div></div></div>";
                        $dataHtml .= "</li>";

                    }
                }

                $this -> ajaxReturn(200, $dataHtml);
                break;
            case 'video_comment':
                $cid = isset($params['val']) ? intval($params['val']) : 0;
                $page = $params['p'];
                $this -> loadModel('Comment');
                $this -> loadModel('User');

                $commentTopic = Configure ::read('COMMENT_TOPIC');
                $dataList = $this -> Comment -> find('all', array('conditions' => array('topic_type' => $commentTopic['视频'], 'parent_id' => 0, 'topic_id' => $cid), 'order' => array('is_top' => 'desc', 'sort' => 'desc', 'comment_time' => 'desc'), 'limit' => 10, 'page' => $page));
                $dataHtml = "";

                $this -> ajaxReturn(200, $dataHtml);
                break;
            case 'change_news':
                $this -> loadModel('Article');
                $conditions['status'] = 1;
                $conditions['is_home'] = 1;
                $orderby['create_time'] = 'desc';
                $where['status']=1;
                $where['is_top']=1;
                $where['is_elite']=1;
                $data = $this -> random_table(5, 'Article',$where);
                $dataHtml = '';
//                $dataHtml .= '<div id="zSlider"><div id="picshow"><div id="picshow_img"><ul>';
                $str1='';
                $str2='';
                foreach ($data as $value) {
//                    $str1.=$value['Article']['id'].'    ';
//                    $str1.="<li><a href=\"/plan/info/{$value['Article']['id']}.html\" target=\"_blank\"><img src='{$value['Article']['image']}' alt='{$value['Article']['title']}'></img></a></li>";
                    $str2.="<li><a href=\"/plan/info/{$value['Article']['id']}.html\" target=\"_blank\"><h5 >{$value['Article']['title']}</h5></a></li>";
                }
                $dataHtml.=$str1;
//                $dataHtml.='</ul></div></div><div id="select_btn"><ul>';
                $dataHtml.='<ul>';
                $dataHtml.=$str2;
                $dataHtml.='</ul>';
                $this -> ajaxReturn(200, '请求成功',$dataHtml);

                break;
            default:
                break;
        }
        die();
    }

    //用户操作
    public function user()
    {
        $params = $this -> request -> query;
        $this -> loadModel('User');
        $act = $params['ajaxdata'];
        $this -> checkLogin(1);
        $userInfo = $this -> getUser();
        $data = [];
        if (empty($act))
            $this -> ajaxReturn(4001, '参数异常！');
        switch ($act) {
            case 'upt_sex':
                $val = $params['val'];
                $userInfo['sex'] = intval($val);
                if (!$this -> User -> save($userInfo))
                    $this -> ajaxReturn(4002, '提交异常!');
                $this -> setUser($userInfo);
                $this -> ajaxReturn(200, 'sucuss!');
                break;
            case 'agree_article':
                $this -> loadModel('Agree');
                $this -> loadModel('Article');
                $agreeTopic = Configure ::read('AGREE_TOPIC');
                $cid = $params['val'];
                $infos = $this -> Article -> find('first', array('conditions' => array('status' => 1, 'id' => $cid)));
                if (empty($infos))
                    $this -> ajaxReturn(4002, '参数异常！');

                $agCount = $this -> Agree -> find('count', array('conditions' => array('uid' => $userInfo['id'], 'obj_id' => $cid, 'obj_type' => $agreeTopic['攻略'])));
                if ($agCount == 0) {
                    $data = [];
                    $data['uid'] = $userInfo['id'];
                    $data['obj_type'] = $agreeTopic['攻略'];
                    $data['obj_id'] = $cid;
                    $data['create_time'] = date("Y-m-d H:i:s", time());
                    $this -> Agree -> save($data);
                    //赞数+1
                    $this -> Article -> updateAll(array('Article.agree_num' => '`Article`.`agree_num`+1'), array('Article.id' => $cid));
                } else
                    $this -> ajaxReturn(4003, '不能重复点赞！');

                $this -> ajaxReturn(200, '+1');
                break;
            case 'agree_video':
                $this -> loadModel('Agree');
                $this -> loadModel('Video');
                $agreeTopic = Configure ::read('AGREE_TOPIC');
                $cid = $params['val'];
                $infos = $this -> Video -> find('first', array('conditions' => array('status' => 1, 'id' => $cid)));
                if (empty($infos))
                    $this -> ajaxReturn(4002, '参数异常！');

                $agCount = $this -> Agree -> find('count', array('conditions' => array('uid' => $userInfo['id'], 'obj_id' => $cid, 'obj_type' => $agreeTopic['视频'])));
                if ($agCount == 0) {
                    $data = [];
                    $data['uid'] = $userInfo['id'];
                    $data['obj_type'] = $agreeTopic['视频'];
                    $data['obj_id'] = $cid;
                    $data['create_time'] = date("Y-m-d H:i:s", time());
                    $this -> Agree -> save($data);
                    //赞数+1
                    $this -> Video -> updateAll(array('Video.search_num' => '`Video`.`search_num`+1'), array('Video.id' => $cid));
                } else
                    $this -> ajaxReturn(4003, '不能重复点赞！');

                $this -> ajaxReturn(200, '+1');
                break;
            case 'agree_share':
                $this -> loadModel('Agree');
                $this -> loadModel('Share');
                $agreeTopic = Configure ::read('AGREE_TOPIC');
                $cid = $params['val'];
                $infos = $this -> Share -> find('first', array('conditions' => array('status' => 1, 'type' => 0, 'id' => $cid)));
                if (empty($infos))
                    $this -> ajaxReturn(4002, '参数异常！');

                $agCount = $this -> Agree -> find('count', array('conditions' => array('uid' => $userInfo['id'], 'obj_id' => $cid, 'obj_type' => $agreeTopic['分享'])));
                if ($agCount == 0) {
                    $data = [];
                    $data['uid'] = $userInfo['id'];
                    $data['obj_type'] = $agreeTopic['分享'];
                    $data['obj_id'] = $cid;
                    $data['create_time'] = date("Y-m-d H:i:s", time());
                    $this -> Agree -> save($data);
                    //赞数+1
                    $this -> Share -> updateAll(array('Share.agree_num' => '`Share`.`agree_num`+1'), array('Share.id' => $cid));
                } else
                    $this -> ajaxReturn(4003, '不能重复点赞！');

                $this -> ajaxReturn(200, '+1');
                break;
            case 'agree_answer':
                $this -> loadModel('Agree');
                $this -> loadModel('Share');
                $agreeTopic = Configure ::read('AGREE_TOPIC');
                $cid = $params['val'];
                $infos = $this -> Share -> find('first', array('conditions' => array('status' => 1, 'type' => 1, 'id' => $cid)));
                if (empty($infos))
                    $this -> ajaxReturn(4002, '参数异常！');

                $agCount = $this -> Agree -> find('count', array('conditions' => array('uid' => $userInfo['id'], 'obj_id' => $cid, 'obj_type' => $agreeTopic['问答'])));
                if ($agCount == 0) {
                    $data = [];
                    $data['uid'] = $userInfo['id'];
                    $data['obj_type'] = $agreeTopic['问答'];
                    $data['obj_id'] = $cid;
                    $data['create_time'] = date("Y-m-d H:i:s", time());
                    $this -> Agree -> save($data);
                    //赞数+1
                    $this -> Share -> updateAll(array('Share.agree_num' => '`Share`.`agree_num`+1'), array('Share.id' => $cid));
                } else
                    $this -> ajaxReturn(4003, '不能重复点赞！');

                $this -> ajaxReturn(200, '+1');
                break;
            default:
                break;
        }
        die();
    }


    public function file()
    {
        $params = $this -> request -> query;
        $act = $params['ajaxdata'];
        $this -> checkLogin(1);
        $this -> loadModel('Picture');
        if (empty($act))
            $this -> ajaxReturn(4001, '参数异常！');
        switch ($act) {
            case 'article':
                $pictype = Configure ::read('PIC_TOPIC');
                if (!empty($_FILES['file']['name'])) {
                    // 上传文件
                    $fName = explode(".", $_FILES['file']['name']);
                    $_FILES['imgFile'] = $_FILES['file'];
                    $attrRes = Ivf ::uploadImg('/uploads/article/');
                    if ($attrRes['status'] != 200)
                        $this -> ajaxReturn(4002, '图片上传失败！原因:' . $attrRes['message']);
                    //生成缩略图
                    $php_path = dirname(dirname(__FILE__)) . '/webroot';
                    //文件保存目录路径
                    $thumb_path = $attrRes['save_url'] . "thumb/";
                    if (!file_exists($php_path . $thumb_path)) {
                        mkdir($php_path . $thumb_path);
                    }
                    $new_image_path = $thumb_path . $attrRes['save_name'];
                    Ivf ::thumb_pic($php_path . $attrRes['url'], $php_path . $new_image_path);

                    $data = [];
                    $data['image'] = $attrRes['url'];
                    $data['obj_type'] = $pictype['攻略'];
                    $data['thumb'] = $new_image_path;
                    $data['obj_id'] = 0;
                    $data['mimetype'] = $fName[1];
                    $this -> Picture -> save($data);

                    $this -> ajaxReturn(200, '上传成功！', array('id' => $this -> Picture -> id, 'url' => $data['thumb']));
                }
                $this -> ajaxReturn(4002, '上传失败！');
                break;
            case 'share':
                $pictype = Configure ::read('PIC_TOPIC');
                if (!empty($_FILES['file']['name'])) {
                    // 上传文件
                    $fName = explode(".", $_FILES['file']['name']);
                    $_FILES['imgFile'] = $_FILES['file'];
                    $attrRes = Ivf ::uploadImg('/uploads/share/');
                    if ($attrRes['status'] != 200)
                        $this -> ajaxReturn(4002, '图片上传失败！原因:' . $attrRes['message']);
                    //生成缩略图
                    $php_path = dirname(dirname(__FILE__)) . '/webroot';
                    //文件保存目录路径
                    $thumb_path = $attrRes['save_url'] . "thumb/";
                    if (!file_exists($php_path . $thumb_path)) {
                        mkdir($php_path . $thumb_path);
                    }
                    $new_image_path = $thumb_path . $attrRes['save_name'];
                    Ivf ::thumb_pic($php_path . $attrRes['url'], $php_path . $new_image_path);

                    $data = [];
                    $data['image'] = $attrRes['url'];
                    $data['obj_type'] = $pictype['分享'];
                    $data['thumb'] = $new_image_path;
                    $data['obj_id'] = 0;
                    $data['mimetype'] = $fName[1];
                    $this -> Picture -> save($data);

                    $this -> ajaxReturn(200, '上传成功！', array('id' => $this -> Picture -> id, 'url' => $data['thumb']));
                }
                $this -> ajaxReturn(4002, '上传失败！');
                break;
            case 'answer':
                $pictype = Configure ::read('PIC_TOPIC');
                if (!empty($_FILES['file']['name'])) {
                    // 上传文件
                    $fName = explode(".", $_FILES['file']['name']);
                    $_FILES['imgFile'] = $_FILES['file'];
                    $attrRes = Ivf ::uploadImg('/uploads/answer/');
                    if ($attrRes['status'] != 200)
                        $this -> ajaxReturn(4002, '图片上传失败！原因:' . $attrRes['message']);
                    //生成缩略图
                    $php_path = dirname(dirname(__FILE__)) . '/webroot';
                    //文件保存目录路径
                    $thumb_path = $attrRes['save_url'] . "thumb/";
                    if (!file_exists($php_path . $thumb_path)) {
                        mkdir($php_path . $thumb_path);
                    }
                    $new_image_path = $thumb_path . $attrRes['save_name'];
                    Ivf ::thumb_pic($php_path . $attrRes['url'], $php_path . $new_image_path);

                    $data = [];
                    $data['image'] = $attrRes['url'];
                    $data['obj_type'] = $pictype['问答'];
                    $data['thumb'] = $new_image_path;
                    $data['obj_id'] = 0;
                    $data['mimetype'] = $fName[1];
                    $this -> Picture -> save($data);

                    $this -> ajaxReturn(200, '上传成功！', array('id' => $this -> Picture -> id, 'url' => $data['thumb']));
                }
                $this -> ajaxReturn(4002, '上传失败！');
                break;
            case 'del_pic':
                $rid = $params['val'] ? intval($params['val']) : 0;
                if (intval($rid) <= 0)
                    $this -> ajaxReturn(4002, '参数异常！');

                $this -> Picture -> delete(array('id' => $rid));
                $this -> ajaxReturn(200, '已删除！');
                break;
            case 'avatar':
                $userInfo = $this -> getUser();
                $this -> loadModel('User');
                $dataList = $this -> User -> findById($userInfo['id']);
                if (empty($dataList))
                    $this -> ajaxReturn(4001, '非法提交！');
                $dataList = $dataList['User'];
                $params = $this -> request -> data;
                if (!empty($params['imgUrl'])) {
                    $attrRes = Ivf ::cropImg($params, '/uploads/avatar/');
                    $orgPath = Ivf ::upload_img_64base('/uploads/avatar/', $params['imgUrl'], 'orange');
                    $dataList['avatar'] = $attrRes['url'];
                    $dataList['orange_avatar'] = $orgPath;
                    $dataList['id'] = $userInfo['id'];
                    if (!$this -> User -> save($dataList)) {
                        $this -> ajaxReturn('error', '保存异常！');
                    }
                    unset($dataList['password'], $dataList['auth']);
                    $this -> setUser($dataList);
                    echo json_encode($attrRes);
                    exit;
                }
                break;
            case 'mb_avatar':
                $userInfo = $this -> getUser();
                $this -> loadModel('User');
                $dataList = $this -> User -> findById($userInfo['id']);
                if (empty($dataList))
                    $this -> ajaxReturn(4001, '非法提交！');
                $dataList = $dataList['User'];
                $params = $this -> request -> data;
                if (!empty($params['imgUrl'])) {
                    $attrRes = Ivf ::upload_img_64base('/uploads/avatar/', $params['imgUrl']);
                    $orgPath = Ivf ::upload_img_64base('/uploads/avatar/', $params['orange'], 'orange');
                    $dataList['avatar'] = $attrRes;
                    $dataList['orange_avatar'] = $orgPath;
                    $dataList['id'] = $userInfo['id'];
                    if (!$this -> User -> save($dataList)) {
                        $this -> ajaxReturn(4003, '保存异常！');
                    }
                    unset($dataList['password'], $dataList['auth']);
                    $this -> setUser($dataList);
                    $this -> ajaxReturn(200, '保存成功！');
                    exit;
                }
                break;
            default:
                break;
        }
        die();
    }


}