<?php
//试管助手  pc端
class AideController extends AppController {

    public $layout = 'main';

    function beforeFilter() {
        parent::beforeFilter();
        $this->set('nav','aide'); 
    }
// pc端试管助手
    function index(){
     
    }

// 手机端试管助手
    function mobile_aide(){ 
        $params = $this->request->data;
        if($this->request->is('post') && $params['dosubmit']==1){ 
            if(empty($params['u_mobile'])){
                $this->ajaxReturn(4001, '手机号不能为空!');  
            }
            if(!Ivf::isMobile($params['u_mobile']))
                $this->ajaxReturn(4002, '请输入有效的11位手机号码!');   
            $code = $this->Session->read('mobile_code');
            if(($params['u_mobile'] != $code['mobile']) || ($params['u_code'] != $code['code'])) 
                $this->ajaxReturn(4007, '手机验证码有误！');
                
            $this->loadModel('UsersAppoint');  
    		$dataList = $this->UsersAppoint->find('first',array('conditions'=>array('status'=>0,'mobile'=>$params['u_mobile'],'source'=>'ivf52-试管助手')));
            if(!empty($dataList))
                $this->ajaxReturn(4003, '不能重复提交！'); 

            $this->loadModel('User'); 
            $dataList = $this->User->findByMobile($params['u_mobile']);  
            $userInfo=[];
            if(empty($dataList)){
                //注册登陆
                $db_data = array();
                $db_data['mobile'] = $params['u_mobile'];
                $db_data['auth'] = Ivf::random(8);
                $db_data['status'] = 1;
                $db_data['sex'] = 0;
                $db_data['password'] = md5('12345678'.$db_data['auth']);
                $db_data['avatar'] = '/images/mobile/touxiang.png';
                $db_data['ip'] = Ivf::getClientIp();; 
                $db_data['create_time'] = date("Y-m-d H:i:s",time());
                $userInfo=$this->User->save($db_data);
                $userInfo=$userInfo['User'];
                unset($userInfo['password'],$userInfo['auth']);
                $this->setUser($userInfo);    
            }
            else{
                $userInfo=$dataList['User'];
                unset($userInfo['password'],$userInfo['auth']);
                $this->setUser($userInfo);    
            }  
  
            $db_data = array();
            $db_data['types'] =  0;
            $db_data['uid'] =  $userInfo['id'];
            $db_data['user_name'] =  $params['u_mobile'];
            $db_data['mobile'] =  $params['u_mobile'];
            $db_data['source'] = "ivf52-试管助手";
            $db_data['source_url'] = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $db_data['status'] = 0;   
            $db_data['system'] = Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT']));
            $db_data['brand'] = Ivf::getOSBrand(strtolower($_SERVER['HTTP_USER_AGENT']));  
            $db_data['ip'] = Ivf::getClientIp();; 
            $db_data['create_time'] = date("Y-m-d H:i:s",time()); 
            if(!$this->UsersAppoint->save($db_data)) 
                    $this->ajaxReturn(4005, '预测失败！'); 
              
            $this->ajaxReturn(200, '预测成功！');   
        } 
    }

}