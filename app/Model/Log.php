<?php
class Log extends AppModel { 
     
    public function addLog($content,$ip) {  
        if(empty($content)) return false; 
        $dataAttr['logString']=$content; 
        $dataAttr['logDate']=date("Y-m-d",time()); 
        $dataAttr['logTime']=date("H:i:s",time()); 
        $dataAttr['logIp']=$ip; 
        $this->save($dataAttr); 
    }  
}
?>
