<?php
class MobileMessage extends AppModel {

    const UNSENT_MSG = 'unsent'; //未发送
    const SEND_SUCCESS = 'success';
    const SEND_FAIL = 'fail';
    const IS_EXPIRED = 'expired';//过期

    public static $code_msg = "您的验证码是：%s。请不要把验证码泄露给其他人。如非本人操作，可不用理会！";
    
    public function addMsg($user_id , $mobile , $msg ,$status,$errorMsg=""){
        if(empty($mobile) || empty($msg)){
            return false;
        }
        $this->create();
        $data = array(
            'user_id' => $user_id,
            'mobile' => $mobile,
            'msg' => $msg,
            'create_time' => time(),
            'sent_time' => time(),
            'status' => $status,
            'error' => $errorMsg
        );
        return $this->save($data) ? true : false;
    }
    
    function sendSigleMsg($user_id , $mobile , $msg){ 
        $MobileAuth = ClassRegistry::init('MobileAuth');
        $res = $MobileAuth->send_msg($mobile, $msg); 
        if($res['status'] == 'success'){ 
            $this->addMsg($user_id , $mobile , $msg,self::SEND_SUCCESS); 
            return true;
        }else{ 
            $this->addMsg($user_id , $mobile , $msg,self::SEND_FAIL,$res['msg']); 
            return false;
        } 
    }

    function sendMsg(){
        $msg = $this->find('all',array(
                'conditions' => array(
                    'status' => self::UNSENT_MSG,
                ),
                'limit' => 10,
            )
        );
        if(empty($msg)){
            return false;
        }
        $MobileAuth = ClassRegistry::init('MobileAuth');
        foreach($msg as &$val){
            $res = $MobileAuth->send_msg($val[$this->alias]['mobile'], $val[$this->alias]['msg']);
            if($res['status'] == 'success'){
                $val[$this->alias]['sent_time'] = time();
                $val[$this->alias]['status'] = self::SEND_SUCCESS;
            }else{
                $val[$this->alias]['error'] = $res['msg'];
                $val[$this->alias]['status'] = self::SEND_FAIL;
            }
        }
        $this->saveAll($msg);
    }
    
    function sendFailedMsg(){
        $fail_msgs = $this->find('all',array(
                'conditions'=>array(
                    'status'=>self::SEND_FAIL,
                ),
                'limit'=>10,
            )
        );
        if(empty($fail_msgs)){
            return false;
        }
        $expire_time = time() - 600;
        $MobileAuth = ClassRegistry::init('MobileAuth');
        foreach($fail_msgs as &$val){
            if($val[$this->alias]['create_time'] < $expire_time){
                $val[$this->alias]['status'] = self::IS_EXPIRED;
            }else{
                $res = $MobileAuth->send_msg($val[$this->alias]['mobile'], $val[$this->alias]['msg']);
                if($res['status'] == 'success'){
                    $val[$this->alias]['sent_time'] = time();
                    $val[$this->alias]['status'] = self::SEND_SUCCESS;
                }else{
                    $val[$this->alias]['error'] = $res['msg'];
                    $val[$this->alias]['status'] = self::SEND_FAIL;
                }
            }
        }
        $this->saveAll($fail_msgs);
    }

    function create_code($length = 6 , $numeric = 1) {
        PHP_VERSION < '4.2.0' && mt_srand((double)microtime() * 1000000);
        if($numeric) {
            $hash = sprintf('%0'.$length.'d', mt_rand(0, pow(10, $length) - 1));
        } else {
            $hash = '';
            $chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789abcdefghjkmnpqrstuvwxyz';
            $max = strlen($chars) - 1;
            for($i = 0; $i < $length; $i++) {
                $hash .= $chars[mt_rand(0, $max)];
            }
        }
        return $hash;
    }
}
?>
