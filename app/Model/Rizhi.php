<?php
class Rizhi extends AppModel {   
    /**
     * 添加用户日志
     *
     * @param string $source
     * @param string $suid  
     *
     * @return bool|int 失败返回false,成功返回插入的日志id
     */
    public function addUserLog($source, $suid,$name='',$site=0,$version='1.0')
    {
        App::uses('Ivf', 'Lib'); 
        $data = array(  
            'model'         => $suid,
            'system'         => Ivf::getOSInfo(strtolower($_SERVER['HTTP_USER_AGENT'])),
            'version'         => $version,
            'site'     => $site,
            'name'         => $name, 
            'godate'       => date("Y-m-d H:i:s", time()),
            'source'       => intval($source),
            'ua'           => $_SERVER['HTTP_USER_AGENT'], 
            'IP'           => Ivf::getClientIp(),
        );   
        $this->save($data); 
        return $this->getInsertID();
    }

     function addUVLog($userQudao="",$model="",$name=""){  
        $guid=$this->getGuid();  
        if(empty($guid))
        { 
            $logRes=0;
            if(!empty($model)){ 
                $logRes = $this->getUVLog($model); 
                $guid=$logRes['model'];
                $logRes=$logRes['id'];
            } 
            if($logRes==0){
                $guid=substr(md5(uniqid()), 0,21);
                $lasttime = time() + 86400*1;  
                if(!empty($userQudao)){
                    //查看渠道是否存在 
                    $yonghuqudao = ClassRegistry::init('yonghuqudao'); 
                    $qudaoRes = $yonghuqudao->find('first',array('conditions'=>array('status'=>0,'yonghuqudaohao'=>$userQudao))); 
                    if(empty($qudaoRes)) 
                        $userQudao=''; 
                } 
                $logRes=$this->addUserLog($userQudao,$guid,$name);
            } 
            if($logRes){
                //写cookie
                setcookie('ivf_guid', $guid, $lasttime, '/');  
                $_SESSION['byLogsId']=$logRes; 
            }
            
        } 
    } 

    function getGuid()
    { 
        $guid="";   
        if(isset($_COOKIE['ivf_guid']) && !empty($_COOKIE['ivf_guid'])) 
            $guid=$_COOKIE['ivf_guid'];  
        return $guid;
    }

    function getUVLog($guid){
        if (empty($guid))  return;    
        $list=$this->query("SELECT * FROM ivf_rizhis WHERE `model` = '{$guid}' AND godate>=date(now()) AND godate<DATE_ADD(date(now()),INTERVAL 1 DAY) LIMIT 0,1");  
        return $list ? $list[0]['ivf_rizhis'] : array();
    }


    /**
     * 处理渠道推广，记录UV ada 
     *
     * @param string $channel
     * @param string $tp
     */
    function handleSpreadChannel($channel)
    {
        if (empty($channel)) {
            return;
        }          
        $yonghuqudao = ClassRegistry::init('yonghuqudao'); 
        $yonghuqudaoInfo = $yonghuqudao->find('first',array('conditions'=>array('status'=>0,'yonghuqudaohao'=>$channel))); 
        //第一步: 获取渠道信息 
        if (empty($yonghuqudaoInfo)) {
            return;
        }    
        $guid=$this->getGuid(); 
        $logRes = [];
        if(!empty($guid))
            $logRes = $this->getUVLog($guid); 
        if (!empty($logRes)) {
            $rizhiId = $logRes['id']; 
        }   
        else{
             //新用户记录下日志 
            $guid=substr(md5(uniqid()), 0,21);
            $lasttime = time() + 86400*1; 
            $rizhiId = $this->addUserLog($channel, $guid); 
            setcookie('ivf_guid', $guid, $lasttime, '/');  
            $_SESSION['byLogsId']=$rizhiId; 
        }  
    }
}
?>
